<script type="text/javascript" language="JavaScript">
$(function() {
	$.datepicker.setDefaults($.extend({
			numberOfMonths: 2,
			showButtonPanel: false,
			minDate: new Date(),
			showAnim: 'slideDown',
			showOn: 'both',
			buttonImage: '/images/calendar_icon.png'
		}, $.datepicker.regional['']));
	$('#regbox .date_input').datepicker().focus(function() {
			$('#ui-datepicker-div').css('left', $(this).offset().left - $(this).width()*4);
	});
	$('.singlebox .date_input').datepicker().focus(function() {
		//$('#ui-datepicker-div').css('left', $(this).offset().left - $(this).width()*4);
	});
	//set the date back to english so the validator doesn't trip
	$('.date_input').datepicker('option', 'dateFormat', 'mm/dd/yy')
});
</script>