$(function(){
    //colorbox modal table
    if($.fn.colorbox){
        $(".rate-deta").colorbox({
            href : 'detail-per-night.html'	
        });		
    }
    var messages_lang = {};
    if(lang == "ES"){
        messages_lang = {
            nombre: {
                required : "Este campo es requerido"
            },
            asunto: {
                required : "Este campo es requerido"
            },
            email: {
                required : "Este campo es requerido"
            },
            pe_direccion: {
                required : "Este campo es requerido"				
            },
            contenido: {
                required : "Este campo es requerido"
            }
           
        };
    }else{
        messages_lang = {
            nombre: {
                required : "This field is required"
            },
            asunto: {
                required : "This field is required"
            },
            email: {
                required : "This field is required",
                email:"Please write an valid email."
            },
            contenido: {
                required : "This field is required"				
            }
           
        };
    }
    //validate form registrese
    $("#form-contact").validate({
        rules :{
            nombre : {
                required: true,
                nombre: true
            },
            asunto :{
                required: true,
                nombre: true	
            },
            email: {
                required: true,
                email: true,
                maxlength: 500
            },
           
            contenido: {
                required: true
                //alphanumspecial: true	
            }
        },
        messages: messages_lang
    });
    $("#form-contact").submit(function(){
        $("#form-contact").validate();
    });
});