$(function(){
    //colorbox modal table
    if($.fn.colorbox){
        $(".rate-deta").colorbox({
            href : 'detail-per-night.html'	
        });		
    }
    var messages_lang = {};
    if(lang == "ES"){
        messages_lang = {
            pe_nombre: {
                required : "Este campo es requerido"
            },
            pe_apellidos: {
                required : "Este campo es requerido"
            },
            pe_email: {
                required : "Este campo es requerido"
            },
            pe_email_confirm: {
                required : "Este campo es requerido",
                equalTo: "Ingrese el mismo correo ingresado anteriormente"	
            },
            pe_direccion: {
                required : "Este campo es requerido"				
            },
            pe_ciudad: {
                required : "Este campo es requerido"				
            },
            pe_postal:{
                alphanumsimple: "Ingrese un código postal válido."
            },
            pe_pais: {
                required : "Este campo es requerido"				
            },
            res_tarj_id: {
                required : "Este campo es requerido"								
            },
            res_numero_tarj: {
                required : "Este campo es requerido"								
            },
            res_tarj_month: {
                required : "Este campo es requerido"								
            },
            res_tarj_year: {
                required : "Este campo es requerido"								
            },
            'res_security_code[]': {
                required : "Por favor acepte los términos y condiciones"
            }
        };
    }else{
        messages_lang = {
            pe_nombre: {
                required : "This field is required"
            },
            pe_apellidos: {
                required : "This field is required"
            },
            pe_email: {
                required : "This field is required",
                email:"Please write an valid email."
            },
            pe_email_confirm: {
                required : "This field is required",
                email:"Please write an valid email.",
                equalTo: "Please agree the email again"	
            },
            pe_direccion: {
                required : "This field is required"				
            },
            pe_ciudad: {
                required : "This field is required"				
            },
            pe_postal:{
                alphanumsimple: "Agree a valid postal code."
            },
            pe_pais: {
                required : "This field is required"				
            },
            res_tarj_id: {
                required : "This field is required"								
            },
            res_numero_tarj: {
                required : "This field is required"								
            },
            res_tarj_month: {
                required : "This field is required"								
            },
            res_tarj_year: {
                required : "This field is required"								
            },
            'res_security_code[]': {
                required : "This field is required"
            }
        };
    }
    //validate form registrese
    $("#frm_guest_info").validate({
        
        rules :{
            
            pe_nombre : {
                required: true,
                nombre: true
            },
            pe_apellidos :{
                required: true,
                nombre: true	
            },
            pe_email: {
                required: true,
                email: true	
            },
            pe_email_confirm: {
                required: true,
                email: true,
                equalTo: "input[name=pe_email]"
            },
            pe_direccion: {
                required: true
                //alphanumspecial: true	
            },
            pe_ciudad:{
                required: true	
            },
            pe_postal:{
                alphanumsimple: true
            },
            phone_number:{
                phone: true
            },
            pe_pais: {
                required: true	
            },
            res_tarj_id: {
                required: true	
            },
            res_numero_tarj: {
                required: true	
            },
            res_tarj_month: {
                required: true	
            },
            res_tarj_year: {
                required: true	
            },
            'res_security_code[]': {
                required: true                
            }
        },
        messages: messages_lang
    });
    $("input[type='image']").click(function(){
         
        $("#frm_guest_info").validate();
        
    });
});