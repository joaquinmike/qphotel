
function ftLimpiar(){
    document.location = '';
}

function formatMon(v){ 
    /*v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    return (v);*/
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '.00';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
        whole = whole.replace(r, '$1' + ',' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return '-' + v.substr(1);
    }
    return v;
}

function loginDefault(){
    var user = $('#us_id').attr('value');
    var pass = $('#us_pass').attr('value');
    if(user && pass){
        $.ajax({
            async:true,
            type: "POST",
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            url: '/user/login',
            data: {'us_id' : user, 'us_pass' : pass},
            success:function(data){
                if(data.action == 1){
                    location.reload();
                }else{
                    alert(data.message);
                }
            },
            timeout:50000
        });
    }
    return false;
}

function resetFormEmpty(id){
     $(id).find(':input').each(function() {
        switch(this.type) {
            case 'password':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });
}