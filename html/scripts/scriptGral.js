// JavaScript Document
//------Funciones Generales

function closeSession(){
    alert('oksa');
}

function horaActual(){
    var hora = new Date().getHours();
    var minuto = new Date().getMinutes();
    var segundo = new Date().getSeconds();
    var complet =  (hora<10?'0'+hora.toString():hora)+':'+(minuto<10?'0'+minuto.toString():minuto)+':'+(segundo<10?'0'+segundo.toString():segundo);
    return complet;
}

function alertExtjs(tipo,msn){
    var mensaje = msn.split('-');
    var info;
    switch(tipo){
        case 'info':
            info  = Ext.MessageBox.INFO;
            break;
		
        case 'error':
            info  = Ext.MessageBox.ERROR;
            break;
		
        case 'question':
            info  = Ext.MessageBox.QUESTION;
            break;
		
        case 'warning':
            info = Ext.MessageBox.WARNING;
            break;
    }
	
    Ext.MessageBox.show({
        title: 'CIT - '+mensaje[0],
        msg: mensaje[1],
        buttons: Ext.MessageBox.OK,
        animEl: 'center',
        width:300,
        fn: '',
        icon:  info
    });
}

function deleteExtjs(){
    Ext.MessageBox.confirm('Confirm', 'Desea eliminar realmente?.', confirmMessaje);
}

function deleteForm(){
    //alert('Button Click', 'You clicked the {0} button', btn)
    Ext.MessageBox.confirm('Confirm', 'Desea elimanar realmente?.', deleteFormData);
}


function alertForm(mensaje){
    //alert('Button Click', 'You clicked the {0} button', btn)
    Ext.MessageBox.confirm('CIT', mensaje, confirmMessaje);
}


