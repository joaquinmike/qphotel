var storeFechaFin,
		events = [],
		i = 0,
		dates;
$(document).ready(function(){
	//set up the vertical bar animations and make the container a link
	$('.qp-descan-1').each(function(){
		//change the cursor to pointer
		$(this).css({'cursor':'pointer'})
		var lnk = $(this).find('a:first');
		$(this).click(function(){
			//set onclick on the container to the existing link href
			window.location = lnk[0].href;
		});
		//set up the fade in/out on hover
		$(this).hover(function(){
			$(this).find('img').each(function(){
				$(this).fadeOut('fast');
			});
		}, function(){
			$(this).find('img').each(function(){
				$(this).fadeIn('fast');
			});
		});
	});
	
//	//set up the datepickers, if there are any on this page
//	$(function(){
//		$('.date_input').datepicker({
//			numberOfMonths: 2,
//			showButtonPanel: true,
//			minDate: new Date(),
//			showAnim: 'slideDown',
//			showOn: 'both',
//			buttonImage: '/images/calendar_icon.png'
//		}).focus(function() {
//            $('#ui-datepicker-div').css('left', $(this).offset().left - $(this).width()*4);
//        });
//	});
	
	//set up the link highlights
	$(function(){
		$('a.highlight').hover(function(){
			$(this).css({'text-decoration':'underline'})
		}, function(){
			$(this).css({'text-decoration':'none'})
		});
	});
	
	if(!$.browser.msie || ($.browser.msie && parseInt($.browser.version) < 7) ){
		$('.menuitems li').each(function(){
			//make a click on the box goto the same page as it child link
			var lnk = $(this).find('h3 a:first')
			$(this).css({'cursor':'pointer'})//pointer so ppl know they can click
			//only apply the styles to boxes that are not active
			if (!$(this).hasClass('active')) {
				//get the original background color
				var obg = $(this).css('background-color');
				$(this).hover(function(){
					$(this).css({//mousein
						'background': '#d1d0ce'
					})
				}, function(){//mouseout
					$(this).css({
						'background': obg
					})
				})
			}
			$(this).click(function(){
				window.location = lnk[0].href
			})
		});
	}
	dates = $("#fecha_ini, #fecha_fin").datepicker({
					defaultDate: "+1w",
					changeMonth: true,
					numberOfMonths: 2,
					showButtonPanel: false,
					showAnim: 'slideDown',
					showOn: 'both',
					showOtherMonths: true,
					selectOtherMonths: true,
					buttonImage: url_host+'/img/hotel/images/calendar-ico.png',
					onSelect : function(selectedDate){
						//var opt = this.id == "fecha_ini" ? "minDate": "maxDate";
						var	instance = $(this).data("datepicker"),
							date = $.datepicker.parseDate(
										instance.settings.dateFormat ||
										$.datepicker._defaults.dateFormat,
										selectedDate, instance.settings
									);
						/*	dates.not(this).datepicker("option", opt, date);
						*/	
						//console.log(date);
						if(this.id == "fecha_fin"){
							storeFechaFin = date;
							events[1] = selectedDate;
							
						}else{
							events[0] = selectedDate;
							var opt = this.id == "fecha_ini" ? "minDate": "";
							if($.trim(opt)){
								dates.not(this).datepicker("option", opt, date);
							}
							if($.trim($("#fecha_fin").attr("value"))==""){
								storeFechaFin = date;	
							}
							if(date > storeFechaFin){
								$("#fecha_fin").attr("value","");
							}
						}
					},
					onClose: function(date, inst){
						dates.datepicker("refresh");
					},
					beforeShow: function(input, inst) {
						
					},	
					beforeShowDay: function(date){
						var dateaComparar =$.datepicker.formatDate("dd/mm/yy", date);
						if(events.length){
							for(var di = 0; di < events.length; di++){
								if(dateaComparar == events[di]){
									return [true, 'seleccionado',''];
								}
							}
						}
						return [true, '',''];
					}
	});
	$.datepicker.setDefaults($.extend({
			numberOfMonths: 2,
			showButtonPanel: false,
			minDate: new Date(),
			showAnim: 'slideDown',
			showOn: 'both',
			buttonImage: url_host+'/img/hotel/images/calendar-ico.png'
		}, $.datepicker.regional['']));
	$('.reserva-qp .date_input').datepicker().focus(function() {
			$('#ui-datepicker-div').css('left', $(this).offset().left - $(this).width()*4);
	});
	$('.singlebox .date_input').datepicker().focus(function() {
		$('#ui-datepicker-div').css('left', $(this).offset().left - $(this).width()*4);
	});
	//set the date back to english so the validator doesn't trip
	//$('.date_input').datepicker('option', 'dateFormat', 'mm/dd/yy')
	
	$('#s1').cycle({fx:'fade'});	
	
	$('#hidden-image img').hide()
	$('#hidden-image').hover(function(){
		$('#hidden-image img').fadeIn('slow')
	},function(){
		$('#hidden-image img').fadeOut('slow')
	}); 
	
	
});//end document.ready
