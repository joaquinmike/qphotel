// JavaScript Document
cit.formatMon = function(v){ 
    /*v = (Math.round((v-0)*100))/100;
	v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
	return (v);*/
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '.00';
    var r = /(\d+)(\d{3})/;
    while (r.test(whole)) {
        whole = whole.replace(r, '$1' + ',' + '$2');
    }
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return '-' + v.substr(1);
    }
    return v;
}
//Para interno
cit.redondeo_2 = function(v){
    //redondeo=num*100;
    //redondeo=Math.floor(redondeo);
	
    v = (Math.round((v-0)*100))/100;
    v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
    v = String(v);
    var ps = v.split('.');
    var whole = ps[0];
    var sub = ps[1] ? '.'+ ps[1] : '.00';
    var r = /(\d+)(\d{3})/;
	
    v = whole + sub;
    if(v.charAt(0) == '-'){
        return '-' + v.substr(1);
    }
    return parseFloat(v);
//return redondeo/100;
}

var winForms;
//Modale Habitaciones
function formNewHabitacion(titulo,edit,records){
    //-------------
    if(edit == 1){
        band = 'edit';
        bolean = true;	
    }else{
        band = 'save';
        bolean = false;
    }
    cit.storePisosModal = new Ext.data.JsonStore({
        fields : ['pi_id', 'pi_desc'],
        url: og.getUrl('mantenimiento-habitacion','json',{
            'case':'pisos'
        },'habitacion'),
        root: 'matchpiso'
    });
		
    cit.storeTiphabModal = new Ext.data.JsonStore({
        fields : ['tih_id', 'state'],
        url: og.getUrl('mantenimiento-habitacion','json',{
            'case':'tipHabModal'
        },'habitacion'),
        root: 'matches'
    });
		
    cit.storePrecio = new Ext.data.JsonStore({
        fields : ['hb_precio'],
        url: og.getUrl('mantenimiento-habitacion','json',{
            'case':'precioHab'
        },'habitacion'),
        root: 'matches'
    });
		
    //OnChange
    cit.changeSucursalModal = {
        select: function(combo, record, index){
            var filtro = cit.formHabitacion.getForm().getValues();
            cit.storePisosModal.load({
                params: {
                    'su_id': filtro.su_id
                    }
                });
        cit.storeTiphabModal.load({
            params: {
                'su_id': filtro.su_id
                }
            });
}
};//changePrecio
		
cit.changePrecio = {
    select: function(combo, record, index){
        var filtro = cit.formHabitacion.getForm().getValues();
        cit.storePrecio.load({
            params: {
                'su_id': filtro.su_id,
                'tih_id':filtro.tih_id
                }
            });
    cit.storePrecio.on('load', function(s, r) { 
        Ext.each(r, function(records) {
            cit.formHabitacion.form.loadRecord(records);
        });
    });
}
};
		
cit.formHabitacion = new  Ext.form.FormPanel({
    autoHeigh:true,
    width:"100%",
    bodyStyle:"padding: 5px",
    labelWidth: 68,
    plain:true,
    defaults:{
        "anchor":0
    },//autoCreate: {tag: "input", type: "text", autocomplete: "off", maxlength: 10 },
    items:[
    {
        xtype:'hidden',
        name:'edit',
        value: band
    },

    {
        xtype:'hidden',
        name:'hb_id',
        value: cit.idSelect
        },

        {
        xtype:"fieldset",
        items:[
        {
            xtype:'compositefield',
            fieldLabel:'Código ',
            combineErrors:false,
            items:[
            {
                xtype:'textfield',
                name:'hhb_id',
                allowBlank:false,
                width: 70,
                disabled : true
            },

            {
                xtype:'displayfield',
                width:20
            },

            {
                xtype:'displayfield',
                value:'Número :',
                width:55,
                style:'padding-top:+3px;'
            },

            {
                xtype:'numberfield',
                name:'hb_numero',
                allowBlank:false,
                width: 40,
                style:'text-align:center;'
            },

            {
                xtype:'displayfield',
                width:20
            },

            {
                xtype:'displayfield',
                value:'Repetir :',
                width:50,
                style:'padding-top:+3px;'
            },

            {
                xtype:'numberfield',
                name:'hb_cantidad',
                width: 40,
                disabled : bolean,
                style:'text-align:center;'
            }
            ]
            },

            {
            xtype:'textarea',
            name:'hb_desc',
            fieldLabel:'Descrip',
            allowBlank:false,
            blankText: 'Ingrese una Descipción… ',
            height: 35,
            width:310, 
            anchor: '100%'
        },

        {
            xtype:'compositefield',
            fieldLabel:'Precio ',
            combineErrors:false,
            items:[//right
            {
                xtype:'textfield',
                name:'hb_precio',
                allowBlank:false,
                width: 60,
                style:'text-align:right;',
                disabled:true
            }
						
            ]
            }
        ]
        },

        {
        xtype:'fieldset',
        items:[
					
        {
            xtype:'compositefield',
            fieldLabel:'Sucursal ',
            combineErrors:false,
            items:[
            {
                xtype:'combo', 
                width:120,
                mode:'local',
                triggerAction:'all',
                forceSelection:true,
                editable:false,
                name:'su_id',
                emptyText:'- Sucursal -',
                hiddenName:'su_id',
                displayField:'state',
                valueField:'su_id',
                store: cit.storeSucur,
                allowBlank:false,
                listeners: cit.changeSucursalModal,
                minListWidth:'160'
            },
            {
                xtype:'displayfield',
                width:30
            },

            {
                xtype:'displayfield',
                value:'Piso :',
                width:40,
                style:'padding-top:+3px;'
            },

            {
                xtype:'combo', 
                width:100,
                mode:'local',
                triggerAction:'all',
                forceSelection:true,
                editable:false,
                name:'pi_id',
                emptyText:'- Piso -',
                hiddenName:'pi_id',
                displayField:'pi_desc',
                valueField:'pi_id',
                store: cit.storePisosModal,
                allowBlank:false
            },
            {
                icon:  og.domainName+'/images/icons/add_deuda.png', 
                iconCls: 'textIcons',
                xtype: 'tbbutton', 
                handler: function (btn){
                    var filtro = cit.formHabitacion.getForm().getValues();
                    //alert(filtro.su_id);
                    if(filtro.su_id){
								
                        cit.storeDatoPisos = new Ext.data.JsonStore({
                            fields : ['su_id','su_nombre'],
                            //autoLoad: true,
                            url: og.getUrl('mantenimiento-habitacion','json',{
                                'case':'datosPiso'
                            },'habitacion'),
                            root: 'matchpiso'
                        });
								
                        cit.formHabitacionModal = new  Ext.form.FormPanel({
                            autoHeigh:true,
                            width:"100%",
                            bodyStyle:"padding: 5px",
                            labelWidth: 68,
                            plain:true,
                            defaults:{
                                "anchor":0
                            },
                            items:[
                            {
                                xtype:"fieldset",
                                items:[
                                {
                                    xtype:'hidden',
                                    name:'su_id'
                                },

                                {
                                    xtype:'textfield',
                                    name:'su_nombre',
                                    fieldLabel:'Sucursal',
                                    allowBlank:false,
                                    width: 100,
                                    disabled : true
                                },

                                {
                                    xtype:'textfield',
                                    name:'pi_desc',
                                    fieldLabel:'Piso',
                                    allowBlank:false,
                                    width: 100,
                                    maxLength: 32
                                },

                                {
                                    xtype:'numberfield',
                                    name:'pi_numero',
                                    fieldLabel:'Nº',
                                    allowBlank:false,
                                    width: 30,
                                    maxLength: 3
                                }
                                ]
                                }
                            ],
                            buttons: [{
                                text:'Guardar',
                                handler: function(){
                                    if (cit.formHabitacionModal.form.isValid()) {
                                        cit.formSave = cit.formHabitacionModal.form.getValues();
                                        og.openLink(og.getUrl('mantenimiento-habitacion','ajax',{
                                            'case':'saveNewPiso'
                                        },'habitacion'), {
                                            hideLoading: false,
                                            hideErrors: false,
                                            preventPanelLoad: true, 
                                            post: cit.formSave,
                                            onSuccess:cit.loadFomsModal2
                                        });
                                    //cit.hide();
                                    }
                                }
                            },{
                                text: 'Close',
                                handler: function(){
										
                                    cit.formHabitacionModal.form.reset();
                                    winModal.hide();
                                }
                            }]
                        });
                        cit.storeDatoPisos.load({
                            params: {
                                'su_id': filtro.su_id
                                }
                            });
                    cit.storeDatoPisos.on('load', function(s, r) { 
                        Ext.each(r, function(records) {
                            cit.formHabitacionModal.form.loadRecord(records);
                        });
                    });
								
                    winModal = new Ext.Window({
                        width: 300,
                        height:190,
                        minWidth: 300,
                        minHeight: 150,
                        layout : 'fit',
                        closeAction: 'hide',
                        autoDestroy:true,
                        plain: true,
                        modal:true,
                        defaultType: 'textfield',
                        title  : 'Nuevo Piso',
                        items  : [ cit.formHabitacionModal ]
                    });
                    winModal.show();
                }//endif Sucursal
            }
        }
						
    ]
    },
{
    xtype:'compositefield',
    fieldLabel:'Tipo Hab ',
    combineErrors:false,
    items:[
    {
        xtype:'combo', 
        width:120,
        mode:'local',
        triggerAction:'all',
        forceSelection:true,
        editable:false,
        allowBlank:false,
        name:'tih_id',
        emptyText:'- Tipo Hab -',
        hiddenName:'tih_id',
        displayField:'state',
        valueField:'tih_id',
        store: cit.storeTiphabModal,
        listeners: cit.changePrecio,
        minListWidth:'160'
    }
						
						
    ]
    }
]
}
],
buttons: [{
    text:'Guardar',
    handler: function(){
				
        if (cit.formHabitacion.form.isValid()) {
            cit.formSave = cit.formHabitacion.form.getValues();
            og.openLink(og.getUrl('mantenimiento-habitacion','ajax',{
                'case':'saveHabitacion'
            },'habitacion'), {
                hideLoading: false,
                hideErrors: false,
                preventPanelLoad: true, 
                post: cit.formSave,
                onSuccess:cit.loadFormHab,
                timeout : 990000
            });
        //cit.hide();
        }
					   
				
    }
},{
    text: 'Close',
    handler: function(){
        cit.formHabitacion.form.reset();
        winForms.hide();
    }
}]
	
});
		
//Comprobando si existen datos
switch(edit){
    case 1:
        cit.formHabitacion.form.loadRecord(records);
        cit.storeTiphabModal.load({
            params: {
                'su_id': records.data.su_id
            }
        });
    cit.storeTiphabModal.on('load', function(s, r) { 
        //Ext.each(r, function(recordsi) {
        cit.formHabitacion.getForm().setValues({
            tih_id : records.data.tih_id
        });
    //});
    });
    cit.storePisosModal.load({
        params: {
            'su_id': records.data.su_id
        }
    });
cit.storePisosModal.on('load', function(s, r) { 
    //Ext.each(r, function(recordsi) {
    cit.formHabitacion.getForm().setValues({
        pi_id : records.data.pi_id
    });
//});
});
				
break;
			
default:
    cit.storeNew = new Ext.data.JsonStore({
        fields : ['hb_id','hhb_id'],
        autoLoad: true,
        url: og.getUrl('mantenimiento-habitacion','json',{
            'case':'newIDhabit'
        },'habitacion'),
        root: 'matchhab'
    });
    cit.storeNew.on('load', function(s, r) { 
        Ext.each(r, function(records) {
            cit.formHabitacion.form.loadRecord(records);
        });
    });
				
    if(edit == 3){
        cit.formHabitacion.form.loadRecord(records);
        cit.storeTiphabModal.load({
            params: {
                'su_id': records.data.su_id
            }
        });
    cit.storeTiphabModal.on('load', function(s, r) { 
        //Ext.each(r, function(recordsi) {
        cit.formHabitacion.getForm().setValues({
            tih_id : records.data.tih_id,
            hb_numero : parseInt(records.data.tih_stock)+1
        });
    //});
    });
    cit.storePisosModal.load({
        params: {
            'su_id': records.data.su_id
        }
    });
}
break;
}
		
		

winForms = new Ext.Window({
    width: 520,
    height:300,
    minWidth: 300,
    minHeight: 150,
    layout : 'fit',
    closeAction: 'hide',
    autoDestroy:true,
    plain: true,
    modal:true,
    defaultType: 'textfield',
    title  : titulo,
    items  : [ cit.formHabitacion ]
});
return winForms;
	
}
	
//Modal Detalle de Reserva
function modalDetalleRes(reserva){
    //Cargando Grids
    cit.fieldModal =[
        { name : 'lleva' },
        { name : 'red_detalle'},
        { name : 'red_fecini' },
        { name : 'red_fecfin' },
        { name : 'red_dias' },
        { name : 'red_precio' },
        { name : 'pro_id' },
        { name : 'red_tipo'}
    ];
	
    cit.proxyModal=new Ext.data.HttpProxy({
        url:og.getUrl('reserva-consulta','json',{
            'case':'verDetalleReserva'
        },'reserva'),
        method:'POST'
    });
		
    cit.readerMod=new Ext.data.JsonReader({
        fields: cit.fieldModal, //['NEMO', 'nomcomp'],
        root:'matches'
    });
		
    cit.storeMod = new Ext.data.Store({
        proxy:cit.proxyModal,
        reader:cit.readerMod
    //autoLoad: true
    });
		 
    cit.columnMod = [
        new Ext.grid.RowNumberer(),
        {
            dataIndex: 'red_detalle', 
            header: 'Descripci&oacute;n', 
            width: 180,
            locked: true,
            align:'left'
        },

        {
            dataIndex: 'red_fecini', 
            header: 'Ini', 
            width: 70, 
            align:'center',
            renderer: function(v, params, record){            			 
                var fecha = og.fechaRender(v, params, record);
                record.data.res_fecini = fecha;
                return fecha;
            }
        },
        {
            dataIndex: 'red_fecfin', 
            header: 'Fin', 
            width: 70, 
            align:'center',
            renderer: function(v, params, record){            			 
                var fecha = og.fechaRender(v, params, record);
                record.data.res_fecfin = fecha;
                return fecha;
            }
        },
        {
            dataIndex: 'red_dias', 
            header: 'Días', 
            width: 50, 
            align:'center'
        },

        {
            dataIndex: 'red_precio', 
            header: 'Subtotal', 
            width: 80,
            locked: true,
            align:'right',
            renderer: function(v, params, record){
                record.data.ac_precio = v;
                return cit.formatMon(v);
            }
        },
        {
            dataIndex: 'red_tipo', 
            header: 'Tipo', 
            width: 80
        }
			
    ];
		  
    cit.gridMod = new Ext.grid.EditorGridPanel({
        //renderTo: 'contiene',
        store: cit.storeMod,         
        columns: cit.columnMod,
        clicksToEdit: 1,      
        width:'100%',
        height:250, 
        frame: false,
        stripeRows: true,
        columnLines: true,
        trackMouseOver: true,
        viewConfig: {
            forceFit: true
        },
        bodyStyle:'border:0px;border-top:solid 1px #ccc;'
    //loadMask: true
    });
		
    cit.storeMod.load({
        params: {
            'res_id':reserva
        }
    });
		
			
var winModal;
cit.formModal = new  Ext.form.FormPanel({
    autoHeigh:true,
    width:"100%",
    bodyStyle:"padding: 5px",
    labelWidth: 68,
    plain:true,
    defaults:{
        "anchor":0,
        border:0
    },//autoCreate: {tag: "input", type: "text", autocomplete: "off", maxlength: 10 },
    items:[
    {
        items : [cit.gridMod]
    }  
    ],
    buttons: [{
        text: 'Close',
        handler: function(){
            cit.formModal.form.reset();
            winModal.hide();
        }
    }]
	
});
		

winModal = new Ext.Window({
    width: 620,
    height:320,
    minWidth: 400,
    minHeight: 250,
    //bodyStyle:'padding:5px;',
    layout : 'fit',
    closeAction: 'hide',
    autoDestroy:true,
    defaultType: 'textfield',
    plain: true,
    modal:true,
    title  : 'Detalle de la Resersa Nº '+reserva,
    items  : [ cit.formModal ]
});
return winModal;
	
}
	


function cadenaGrid(records){
    //var records = sm.getSelections();
    var datos = '';
    $.each(records, function(i){
        datos = datos + records[i].data.lleva;
        datos = datos+'**';
    });
    datos =  datos.substring(0,datos.length-2);	
	
    return datos;
}