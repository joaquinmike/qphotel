
cit.procesarAjax = function(data,url)
{
    $.ajax({
        async:true,
        type: "POST",
        dataType: "html",
        contentType: "application/x-www-form-urlencoded",
        url:url,
        data:data,
        beforeSend:cit.inicio,
        success:cit.llegadaAction,
        timeout:80000,
        error:cit.problemas
    }); 
    return false;
}
cit.inicio = function()
{
    og.loading();
    $("#"+cit.divs).html('');
    
}
cit.llegadaAction = function(datos)
{
    var dist = datos. split('errorCode')
    if(dist[1]){
        data = Ext.util.JSON.decode(datos);
        if (data.errorCode == 2009) {		
            og.LoginDialog.show();
        }
        og.hideLoading();
        return false
    }
    og.hideLoading();
    $("#"+cit.divs).html(datos);
    cit.divs ='';
}
cit.problemas = function()
{
    og.hideLoading();
    $("#"+cit.divs).text('Problemas en el servidor.');
    cit.divs='';
}
    
////////
cit.numberDefault = function(not){
    if(not=='09')
        not = 9;
    if(not=='08')
        not = 8;
    return not;
}
/////////    
function selectthis(id,sis){
    var dat = id.id.split('-');
    var name =Array('S','SL','LE','AT');
    var i=0;
    var band=0; 
	
    /******************************/
    for(band=1;band<=4;band++){
        if(name[band]!=dat[0])
            $('#'+name[band]+'-ONE-'+dat[2]+'-'+sis).attr('checked',false);
        else
            $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',true);
    }
    /****************************/
    if(dat[0]=='S'){
        if($(id).attr('checked'))
            $('#SL-ONE-'+dat[2]+'-'+sis).attr('checked',true);
        else
            $('#SL-ONE-'+dat[2]+'-'+sis).attr('checked',false);
    }
    /****************************/
    if($(id).attr('checked')){
        for(i=0;i<=20;i++){	
            $('#'+dat[0]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
            var bnd=0;
            /*****************************/
            for(bnd=1;bnd<=4;bnd++){
                if(name[bnd]!=dat[0])
                    $('#'+name[bnd]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            }
            /***************************/
            //$('#'+dat[0]+'-TWO-'+dat[2]+'-'+i).attr('checked',true);
            /*desactiva*/
            if(dat[0]=='S')
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
            else
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);

			
            var j = 0;
            for(j=0;j<=10;j++){	
                $('#'+dat[0]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
					
                for(bnd=1;bnd<=4;bnd++){
                    if(name[bnd]!=dat[0])
                        $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
                }
					
                if(dat[0]=='S')
                    $('#SL-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
                else
                    $('#S-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
					
            }		
        }
    }
    else{		
        $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',false);		
        for(i=0;i<=20;i++){	
            $('#'+dat[0]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
			
            var bnd=0;
            /*****************************/
            for(bnd=1;bnd<=4;bnd++){
                if(name[bnd]!=dat[0])
                    $('#'+name[bnd]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            }
            /***************************/
            //$('#'+dat[0]+'-TWO-'+dat[2]+'-'+i).attr('checked',true);
            /*desactiva*/
            if(dat[0]=='S')
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            else
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);

			
            var j = 0;
            for(j=0;j<=10;j++){	
                $('#'+dat[0]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
					
                for(bnd=1;bnd<=4;bnd++){
                    if(name[bnd]!=dat[0])
                        $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
                }
					
                if(dat[0]=='S')
                    $('#SL-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
                else
                    $('#S-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
					
            }		
        }
		
		
    }
}
function selecttwo(id,sis){
    var dat = id.id.split('-');
    var name =Array('S','SL','LE','AT');
    var i =dat[3];
    var j = 0;
    var band =0;
	

    /****************************/
	
		
    if($(id).attr('checked')){	
		
		
        for(band=1;band<=4;band++){				
            if(name[band]==dat[0]){
					
                $('#'+name[band]+'-ONE-'+dat[2]+'-'+sis).attr('checked',true);
                $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',true);
            }
            else{
                $('#'+name[band]+'-ONE-'+dat[2]+'-'+sis).attr('checked',false);
					
            }
        }
			
        for(band=1;band<=4;band++){
            if(name[band]!=dat[0])
                $('#'+name[band]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            else
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
        }
        /****************************/
        if(dat[0]=='S'){
            if($(id).attr('checked')){
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
                $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',true);
                $('#SL-ONE-'+dat[2]+'-'+sis).attr('checked',true);
            }
            else
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
        }
	
	
        for(j=0;j<=10;j++){	
            $('#'+dat[0]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
				
            for(bnd=1;bnd<=4;bnd++){
                if(name[bnd]!=dat[0])
                    $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
            }
				
            if(dat[0]=='S')
                $('#SL-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
            else
                $('#S-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',true);
				
        }	
		
    }else{
		
		
		
        for(band=1;band<=4;band++){
            if(name[band]!=dat[0])
                $('#'+name[band]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            else
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
        }
        /****************************/
        if(dat[0]=='S'){
            if($(id).attr('checked'))
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            else{
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            /*$('#S-ONE-'+dat[2]).attr('checked',false);
					$('#SL-ONE-'+dat[2]).attr('checked',false);*/
            }
        }
	
	
        for(j=0;j<=10;j++){	
            $('#'+dat[0]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
				
            for(bnd=1;bnd<=4;bnd++){
                if(name[bnd]!=dat[0])
                    $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
            }
				
            if(dat[0]=='S')
                $('#SL-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);
            else
                $('#S-TRE-'+dat[2]+'-'+i+'-'+j+'-'+sis).attr('checked',false);				
        }	
		
		
    }
	
}
function selecttre(id,sis){
    var dat = id.id.split('-');
    var name =Array('S','SL','LE','AT');
    var i =dat[3];
    var j = 0;
    var band =0;
	
    if($(id).attr('checked')){		
        for(band=1;band<=4;band++){				
            if(name[band]==dat[0]){
					
                $('#'+name[band]+'-ONE-'+dat[2]+'-'+sis).attr('checked',true);
                $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',true);
            }
            else{
                $('#'+name[band]+'-ONE-'+dat[2]+'-'+sis).attr('checked',false);
					
            }
        }
			
        for(band=1;band<=4;band++){
				
            if(name[band]==dat[0]){					
                $('#'+name[band]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
            }
            else{
                $('#'+name[band]+'-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
            }
        }
        /****************************/
        if(dat[0]=='S'){
            if($(id).attr('checked')){
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
                $('#S-ONE-'+dat[2]+'-'+sis).attr('checked',true);
                $('#S-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',true);
                $('#SL-ONE-'+dat[2]+'-'+sis).attr('checked',true);
            }
            else
                $('#SL-TWO-'+dat[2]+'-'+i+'-'+sis).attr('checked',false);
        }
        for(bnd=1;bnd<=4;bnd++){
            if(name[bnd]!=dat[0]){
                $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+dat[4]+'-'+sis).attr('checked',false);
            }
        }
        if(dat[0]=='S')
            $('#SL-TRE-'+dat[2]+'-'+i+'-'+dat[4]+'-'+sis).attr('checked',true);
        $('#S-TRE-'+dat[2]+'-'+i+'-'+dat[4]+'-'+sis).attr('checked',true);
    }else{		
        $('#S-TRE-'+dat[2]+'-'+i+'-'+dat[4]+'-'+sis).attr('checked',false);
        for(bnd=1;bnd<=4;bnd++){
            $('#'+name[bnd]+'-TRE-'+dat[2]+'-'+i+'-'+dat[4]+'-'+sis).attr('checked',false);
        }
    }
	
}

function recalcula(tipo,numero){	
    if(tipo){
        var original=parseFloat(numero);
        var result=Math.round(original);
        return result;
    }
    else{
        var result=parseInt(numero);
        return result;
    } 
}
 
function cambia(val,gride,storeGride){  
    var icons = $('#nextitem').attr('title');		
    if(val ==13){
        var pos = gride.getSelectionModel().getSelectedCell();
        if(icons=='left'){					 
            var posi = pos[0]+1; 
            var posj = pos[1];				 
            if(posi >=  storeGride.getCount()){
                posi = 0;
                posj = posj +1;
            }
            //alert(posi+'-'+posj)
            if(posj < gride.colModel.config.length){
                gride.getSelectionModel().select(posi,posj);  		    		 
                gride.startEditing(posi,posj);  
            }	 		    		 		 
        }
        else{ 
            // alert()	    			
            var posi = pos[0]; 
            var posj = pos[1]+1;	
   					 			 
            if(posj >= gride.colModel.config.length-1){
                posi = posi + 1;
                posj = 3;
            }
            if(posi < storeGride.getCount()){
                gride.getSelectionModel().select(posi,posj); 
                gride.startEditing(posi,posj);
            }
        }
    }
}
cit.estados=function(v){
    switch(v){
        case 'P':
            envio = '<img title="Promovido" src="'+og.domainName+'/images/icons/fam/zpromovido.gif" width="10px"></img>';
            break;
        case 'J':
            envio = '<img title="Repitente" src="'+og.domainName+'/images/icons/fam/zrepitente.gif" width="10px"></img>';
            break;
        case 'R':
            envio = '<img title="Retirado" src="'+og.domainName+'/images/icons/fam/zretirado.gif" width="10px"></img>';
            break;
        case 'T':
            envio = '<img title="Trasladado" src="'+og.domainName+'/images/icons/fam/ztrasladado.gif" width="10px"></img>';
            break;
        case 'V':
            envio = '<img title="Vigente" src="'+og.domainName+'/images/icons/fam/zvigente.gif" width="10px"></img>';
            break;
        case 'M':
            envio = '<img title="Evaluaci�n Postergada" src="'+og.domainName+'/images/icons/fam/zpostergado.gif" width="10px"></img>';
            break;
        case 'S':
            envio = '<img title="Requiere Recuperaci�n" src="'+og.domainName+'/images/icons/fam/zrecuperacion.gif" width="10px"></img>';
            break;
        case 'E':
            envio = '<img title="Ex-Alumno" src="'+og.domainName+'/images/icons/fam/zexalumno.gif" width="10px"></img>';
            break;
        default:
            envio = '';
            break;
    }
    return envio;
}
