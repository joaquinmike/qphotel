/////////////////////HABILITAR CONEXIONES REMOTAS ///////////
Paso 1:

Editar el archivo postgresql.conf, este archivo se encuentra en diferentes sitios dependiendo del sistema operativo y la distribucion de Linux que se este usando. Estas son algunas posibles rutas: Windows (C:\PostgreSQL\9.1\data), CentOS (/var/lib/pgsql/data/), Ubuntu ( /etc/postgresql/8.2/main/).

Una vez que se abre el archivo:

Paso1.1 Se debe ubicar la siguiente linea:
?
1
	
#listen_addresses = 'localhost'

Cambiar por la siguiente linea:
?
1
	
listen_addresses = '*'

Paso 1.2 Se debe ubicar la siguiente linea y le quitamos el comentario:
?
1
	
#password_encryption = on

Para quitar el comentario, le quitamos el # al principio de la linea:
?
1
	
password_encryption = on

Paso 1.3 Guardar y cerrar el archivo.

Paso 1.4 Reinciar el postgres esto dependiendo de sus sistemas operativo se puede realizar de varias formas.

Linux (Ubuntu)
?
1
	
sudo /etc/init.d/postgresql-8.2 restart

Linux (CentOS)
?
1
	
service postgresql restart

Windows
Por la consola de servicios se reinicia.


Paso 2

Se debe modificar  lista de acceso, la cual permite establecer relaciones de confianza para ciertos equipos  y redes.

Paso 2.1

Editar el fichero pg_hba.conf. este archivo se encuentra en diferentes sitios dependiendo del sistema operativo y la distribucion de linux que se este usando estas son algunas posibles rutas Windows (C:\PostgreSQL\9.1\data), CentOS (/var/lib/pgsql/data/), Ubuntu ( /etc/postgresql/8.2/main/):

Una vez que se abre el archivo:

Al final del archivo debemos agregar la siguiente linea:
?
1
	
host all all 0.0.0.0 0.0.0.0 md5

Paso 2.2 Guardar y cerrar el archivo.

Paso 2.3 Reinciar el postgres esto dependiendo de sus sistemas operativo se puede realizar de varias formas.

Linux (Ubuntu)
?
1
	
sudo /etc/init.d/postgresql-8.2 restart

Linux (CentOS)
?
1
	
service postgresql restart


//////////////////////////cambiar contrasena

 $sudo su postgres
 $psql
 alter user postgres with password 'contrasena';

para salir de ahi 
 \q   luego exit

probando la conexion 

psql -h localhost -U postgres -W





