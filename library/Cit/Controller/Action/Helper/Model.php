<?php
/**
 * Enter description here...
 *
 */
class Cit_Controller_Action_Helper_Model extends Zend_Controller_Action_Helper_Abstract
{

    public function direct ($object)
    {
    	$db = Zend_Db_Table_Abstract::getDefaultAdapter();
    	$tables = $db->listTables();
		$name =  array();
		foreach($tables as $table)
		{
			$name['Form'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)))] = 'Form'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
			$name['Db'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)))] = 'Db'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
			$name['Cit'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)))] = 'Cit'.str_replace(' ', '', ucwords(str_replace('_', ' ', $table)));
		}
        return new $name[$object];
    }

}

?>