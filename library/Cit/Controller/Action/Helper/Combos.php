<?php

/**
 * Enter description here...
 *
 */
class Cit_Controller_Action_Helper_Combos extends Zend_Controller_Action_Helper_Abstract {

    public function direct($object, array $data = array()) {
        if (!empty($data['mask'])) {
            $id = $data['mask'];
        } else {
            $id = $data['id'];
        }
        $code = '';
        $i = 0;
        if (is_array($object)) {

            $code = 'new Ext.data.SimpleStore({';
            $code .= "fields: ['{$id }', 'state'],
    				data :[ ";
            foreach ($object as $indice => $value) {
                $i++;
                $code .= "['{$indice}', '{$value}'],";
            }
            if (!empty($i))
                $code = substr($code, 0, strlen($code) - 1);
            $code .= ' ]});';
        }
        else {

            if (empty($data['where']))
                $data['where'] = null;
            if (empty($data['order']))
                $data['order'] = null;
            $dataobj = $object->fetchAll($data['where'], $data['order'])->toArray();
            $code = 'new Ext.data.SimpleStore({';
            $code .= "fields: ['{$id}', 'state'],
    				data : [";
            foreach ($dataobj as $value) {
                $i++;
                $code .= "['{$value[$data['id']]}', '{$value[$data['desc']]}'],";
            }
            if (!empty($i))
                $code = substr($code, 0, strlen($code) - 1);
            $code .= ']});';
        }
        return $code;
    }

    /**
     * Helper para crear combos desde un array
     *
     * @param  $data array,$campo nombres [id,desc],
     * $blanco aumenta un registro [T->todos,E->espacio blanco] 
     * @return array combo extjs
     */
    public function consulta(array $data = Array(), $campo, $blanco = '') {
        $combo = '';
        $combo .= 'new Ext.data.SimpleStore({fields:[';
        $combo .= "'" . $campo['id'] . "','state'],";
        switch (@$blanco):
            case 'T':
                $combo .= "data : [ [ 'T' , '-General-'],";
                break;
            case 'E':
                $combo .= "data : [ [ '' , '&nbsp;'],";
                break;
            default:
                $combo .= "data : [";
                break;
        endswitch;

        foreach ($data as $value):
            $combo .= " ['{$value[$campo['id']]}','{$value[$campo['desc']]}'],";
        endforeach;
        $combo = substr($combo, 0, strlen($combo) - 1);
        $combo .= ']})';
        return $combo;
    }

    //$id="",$object=null,$condi='',$valor='',$opcion='',$compara='',$order=null
    /**
     * Combo para Html
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function html($table, $where, $item = array(), $default = '') {
        $combo = '';
        if (is_array($table)):
            $data = $table;

            foreach ($data as $value):
                if ($value[$item['id']] == $default)
                    $select = ' selected="selected" ';
                else
                    $select = '';
                $combo.="<option " . $select . " value='" . $value[$item['id']] . "'>" . $value[$item['desc']] . "</option>";
            endforeach;
        else:
            $citObj = new CitDataGral();
            $data = $citObj->getDataGral($table, $item, $where);
            //var_dump($data); exit;
            foreach ($data as $value):
                if ($value['id'] == $default)
                    $select = ' selected="selected" ';
                else
                    $select = '';
                $combo .= '<option ' . $select . " value='" . $value['id'] . "'>" . $value['desc'] . '</option>';
            endforeach;
        endif;

        return $combo;
    }

}

?>