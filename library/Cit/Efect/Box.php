<?php 
class Cit_Efect_Box{
	private $_async = true;
	
	private $_type = 'POST';
	
	private $_dataType = 'html';
	
	private $_contentType = 'application/x-www-form-urlencoded';
	
	private $_url;
	
	private $_data;
	
	private $_success;
	
	private $_timeout=4000;
	
	private $_error;
	
	private $_name;
	
	private $_code;	
	
	private $_option;
	
	private $_return = false;
	
	private $_div;
	
	private $_param;
	 
	private $_windonw;
	
	private $_tittle;
	
	private $_element;
	
	private $_valida;
	
	
	//private 
	
	public function __construct($name='',array $option = array(), array $data = array(),$div=''){
		if(!empty($div)){
			$this->_return = true;
			$this->_div=$div;
		}
		$this->setName($name);
		$this->setData($data);
		$this->setOption($option);		
		
		//$this->_url;			
	}
	public function option(){
		$data=array('async'=>true,
			   'type'=>'"POST"',
			   'dataType'=> '"html"',
			   'contentType'=>' "application/x-www-form-urlencoded"',
			   'url'=>'"/',
			   'beforeSend'=>'before'.$this->getName(),
			   'success'=>'success'.$this->getName(),
			   'timeout'=>4000,
			   'error'=>'error'.$this->getName());
		
		$datamin=array('async'=>true,
			   'type'=>'"POST"',
			   'dataType'=> '"html"',
			   'contentType'=>' "application/x-www-form-urlencoded"',
			   'url'=>'"/',
			   'timeout'=>4000);
		if($this->_return)
			return $data;
		else 
			return $datamin;
	}
	
	public function setOption($option){
		
		$this->_option = $option;
	}
	public function getOption(){
		return $this->_option;		
	}
	
	public function configura(){
		$data=$this->option();$date='';
		foreach($this->getOption() as $indice => $value){
			foreach($this->option() as $indiceone => $valueone):
				if($indice==$indiceone):
					$data[$indice]=$value;					
				endif;				
			endforeach;			
		}
		foreach($data as $indice => $value):
			$date.=$indice.':'.$value.',';
		endforeach;
		$date = substr($date,0,strlen($date)-1);	
		return $date;
	}	
	public function setAsync($async){
		$this->_async = $async;
		
	}
	public function getAsync(){
		return $this->_async;
	}
	public function setType($type){
		$this->_type=$type;
	}
	public function getType(){
		return $this->_type;
	}
	public function setDataType($dataType){
		$this->_type=$dataType;
	}
	public function getDataType(){
		return $this->_dataType;
	}
	public function setContentType($contentType){
		$this->_type=$contentType;
	}
	public function getContentType(){
		return $this->_contentType;
	}
	
	public function setData($data){
		$this->_data=$data;
	}
	public function getData(){
		return $this->_data;
	}
	public function data(){
		$data='';
		foreach($this->getData()as $indice => $value){
			$data.=$indice.'='.$value.'&';
		}	
		return substr($data,0,strlen($data)-1);
	}
	
	public function setSuccess($success){
		$this->_data=$success;
	}
	public function getSuccess(){
		return $this->_success;
	}
	
	public function setError($error){
		$this->_data=$error;
	}
	public function getError(){
		return $this->_error;
	}
	public function setName($name){
		$this->_name = $name;
	}
	public function getName(){
		return $this->_name;
	}
	public function setParam(array $data=array()){
		$this->_param=$data;		
	}
	public function getParam(){	
		return $this->_param;		
	}
	public function param($forma='code'){
		switch($forma){
			case 'comas': 
				$datec='';
				if(is_array($this->getParam())){				
					foreach($this->getParam() as $indice=>$value):
						$datec.= $indice.',';
					endforeach;
				}
				return substr($datec,0,strlen($datec)-1);
				break;
			default : 
				$dated='';
				if(is_array($this->getParam())){
					foreach($this->getParam() as $indice=>$value):
						$dated.= '&'.$indice.'='.'"+'.$value.'+" ';
					endforeach;
				}
				return $dated;
				break;
		}
	}
	public function setElement(array $element= array()){
		$this->_element=$element;
	}
	public function getElement(){
		$code='';
		if(!empty($this->_element)){
			foreach($this->_element as $value){
				$code.='var '.$value.'=$("#'.$value.'").attr("value"); ';
			}
		}
		 return $code;
	}
	public function setConfigWindonw(array $windonw=array()){
		$this->_windonw=$windonw;
	}
	public function getConfigWindonw(){
		$code="
			autoOpen: true,
			show: 'blind',
			hide: 'explode',
			modal: 'true',
			width: 680, 
			height: 330,";
			
		if(!empty($this->_windonw)){
			foreach($this->_windonw as $indice=>$value){
				$code.=$indice.' : '.$value.',';
			}
			//$code = substr($option,0,strlen($option)-1);	
			return $code;
		}
		else{
			return $code;
		}
	}
	public function setTittle($tittle = 'New Ventana'){
		$this->_tittle = $tittle;
	}
	public function getTittle(){
		if(empty($this->_tittle))
			$this->setTittle();
		return $this->_tittle;
	}
	public function setCode(){	
		$this->_code.='
		<div id="'.$this->_name.'box" title="'.$this->getTittle().'">
			<div id="'.$this->_name.'">
			</div>
		</div>';	
		$this->_code.='<script> ';	
		$this->_code.=' function '.$this->getName().'('.$this->param('comas').'){				
		'.$this->getElement().
		"$('#".$this->_name."box').dialog({
			".$this->getConfigWindonw()."			
			open:".'
				$.ajax({';
				$this->_code.='data:"'.$this->data().$this->param().'",'.$this->configura();
				$this->_code.=' })
				'."	});
				$('#".$this->_name."box').dialog('open');
		".' 
		}';
		$this->_code.=$this->optionfunction();
		$this->_code.=' </script>';		
	}	
	public function getCode(){
		$this->setCode();
		return $this->_code;
	}
	public function optionfunction(){
		$data='';
		if($this->_return){
			$data.='function before'.$this->getName().'(){$("#'.$this->_div.'").html("<img src='."'/images/cms/cargando.gif'".'>");}';	
			$data.='function success'.$this->getName().'(datos){$("#'.$this->_div.'").html(datos);}';
			$data.='function error'.$this->getName().'(){ $("#'.$this->_div.'").text("Problemas en el servidor.");}';
		}
		return $data;
	}
	public function render(){
		
	}
}

