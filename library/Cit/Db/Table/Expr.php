<?php 
class Cit_Db_Table_Expr {
	
	private $_fields;
	
	protected  $_expression;
	
	private $_adapter;
	
	private $_opcion;
	
	private $_delimiter;
	
	static $angle=0;
	
	public  function __construct($opcion = '',array $fields = array(),$delimiter=''){
		$this->_opcion = $opcion;
		$this->_delimiter = $delimiter;
		 $this->_fields =  $fields;
		 $this->render();
	}
	
	private function getAdapters(){		
		$this->_adapter = Cit_Init::config()->database->adapter;
		return  $this->_adapter;
	}	
	private function concat(){
		switch($this->_adapter){
			case 'Firebird':
				$this->_expression .='('; 
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= $value."||'$this->_delimiter'||";		
			 		}
			 	$this->_expression = substr($this->_expression,0,strlen($this->_expression)-strlen("||'$this->_delimiter'||"));
				$this->_expression .=')'; 
				break;
			case 'Pdo_Mysql':
				$this->_expression .='concat('; 
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= $value.",'$this->_delimiter',";		
			 		}
			 		$this->_expression = substr($this->_expression,0,strlen($this->_expression)-strlen(",'$this->_delimiter',"));
				$this->_expression .=')'; 
		  case 'Pdo_Pgsql':
				$this->_expression .='('; 
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= $value."||'$this->_delimiter'||";		
			 		}
			 	$this->_expression = substr($this->_expression,0,strlen($this->_expression)-strlen("||'$this->_delimiter'||"));
				$this->_expression .=')'; 
				break;
				break;
			
		}	
		//return  (string)	$this->_expression;
	}
	private function lenght(){
		switch($this->_adapter){
			case 'Firebird':				 
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "CHAR_LENGTH($value)";		
			 		}			
				break;
			case 'Pdo_Mysql':				
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "LENGTH($value)";		
			 		} 
				break;
				case 'Pdo_Mssql':				
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "LENGTH($value)";		
			 		} 
				break;
			   case 'Pdo_Pgsql':
					foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "CHAR_LENGTH($value)";		
			 		} 
				break;
			
		}	
		//return  (string)	$this->_expression;
	}
	private function substr(){
		switch($this->_adapter){
			case 'Firebird':				 
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "SUBSTRING($value)";		
			 		}			
				break;
			case 'Pdo_Mysql':				
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= "SUBSTR($value)";		
			 		} 
				break;
			case 'Pdo_Mssql':				
				foreach($this->_fields as $indice => $value){
			 			$this->_expression .= " SUBSTRING ($value)";		
			 		} 
				break;
		case 'Pdo_Pgsql':
					foreach($this->_fields as $indice => $value){
			 			$this->_expression .= " SUBSTRING ($value)"	;
			 		} 
				break;
			
		}	
	}
	//array[0] -> dato de la BD
	//array[1] -> fecha hasta donde se calcula
	private function substr_iff(){
		switch($this->_adapter){
			case 'Firebird':				 
			 		//'ALUCOD','valor','compara'
			 		if($this->_fields[2] == 'null')
						$this->_expression = 'IIF('.$this->_fields[0].' is '.$this->_fields[2].",'{$this->_fields[1]}',{$this->_fields[0]})";
					else	//CASE WHEN expr1 THEN expr_if_true ELSE expr_if_false END
						$this->_expression = 'IIF('.$this->_fields[0]." is '{$this->_fields[2]}','{$this->_fields[1]}',{$this->_fields[0]})";		
				break;
			case 'Pdo_Mysql':				
			
				break;
			case 'Pdo_Mssql':				
				
				break;
			case 'Pdo_Pgsql':
					if($this->_fields[2] == 'null')
						$this->_expression = '(case when '.$this->_fields[0].' is '.$this->_fields[2]." then '{$this->_fields[1]}' else {$this->_fields[0]} end)";
					else	//CASE WHEN expr1 THEN expr_if_true ELSE expr_if_false END
						$this->_expression = '(case when '.$this->_fields[0]." is '{$this->_fields[2]}' then '{$this->_fields[1]}' else {$this->_fields[0]} end)";
				break;
			
		}	
	}
	//Menor -> fecha mas antigua
	//Mayor fecha ultima o actual
	private function numDias(){
		switch($this->_adapter){
			case 'Firebird':
			 	$this->_expression = 'dateiff(day from '.$this->_fields[0]." to cast('{$this->_fields[1]}' as date)";
				break;
			case 'Pdo_Mysql':				
					
				break;
			case 'Pdo_Mssql':				
				
				break;
			case 'Pdo_Pgsql':
				switch($this->_fields['tipo']):
					case 'menor':
						if(!empty($this->_fields['mayor']))
							$this->_expression = "'{$this->_fields['mayor']}' - {$this->_fields['menor']}::timestamp";
						else
					 		$this->_expression = "current_date - {$this->_fields['menor']}::timestamp";
						break;
					
				endswitch;
				
				//echo $this->_expression;exit;
				
				break;
			
		}	
	}
 	private function render(){
 		$this->getAdapters();
 		switch($this->_opcion){
 			case 'concat':$this->concat();break;
 			case 'lenght':$this->lenght();break;
 			case 'substr':$this->substr();break;
 			case 'iff':$this->substr_iff();break;
 			case 'date':$this->numDias();break;
 		} 
 	}
	
    public function __toString()
    {
        return (string) $this->_expression;
    }
    static function lower($data){
   		$temp = array();
    	foreach ($data as $indice=>$value){
    		$temp[strtolower($indice)] = $value;	
    	}
    	//var_dump($temp);exit;
    	return $temp;
    }
	static function upper($data){
   		$temp = array();
    	foreach ($data as $indice=>$value){
    		$temp[strtoupper($indice)] = $value;	
    	}
    	return $temp;
    }
    
    
    
 	static function RotatedText($x,$y,$txt,$angle,$pdf){
   		self::Rotate($angle,$x,$y,$pdf);		
		$pdf->Text($x,$y,$txt);	
		self::Rotate(0,'','',$pdf);
    }
    static function Rotate($angle,$x=-1,$y=-1,$pdf){
		//echo $angle;exit;
		if($x==-1)
			$x=$pdf->GetX();
		if($y==-1)
			$y=$pdf->GetY();
		if(self::$angle!=0) $pdf->_out('Q');
		
		self::$angle=$angle; 
		if(self::$angle!=0){
			self::$angle*=M_PI/180;
			$c=cos(self::$angle);
			$s=sin(self::$angle);
			$cx=$x*$pdf->k;
			$cy=($pdf->h-$y)*$pdf->k;;
			$pdf->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
		}
	}  
}