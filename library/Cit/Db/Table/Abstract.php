<?php

class Cit_Db_Table_Abstract extends Zend_Db_Table_Abstract {

    /**
     * Inserta un nuevo registro en la tabla
     *
     * @param array $data
     * @return Object
     */
    public function createNewRow(array $data) {
        return $this->createRow($data)->save();
    }

    public function createNewRowData(array $data) {
        $datanew = array();
        $info = $this->info();
        foreach ($info['metadata'] as $indice => $value) {
            $datanew[$indice] = @$data[$indice];
            //if($value['DATA_TYPE']=='date'){
            if (empty($data[$indice]))
                $datanew[$indice] = null;
            //}
        }
        //var_dump($datanew);exit;
        return $this->createRow($datanew)->save();
    }

    public function json(array $data = array(), $root = 'matches') {
        return json_encode(array('totalCount' => count($data), $root => $data));
    }

    public function correlativo($n = 8, $anio = '') {
        $fetch = $this->select()->limit(1)->order($this->_primary[1] . ' DESC');
        $var = $this->fetchAll($fetch)->toArray();
        if (!empty($anio))
            $var[0][$this->_primary[1]] = substr_replace($var[0][$this->_primary[1]], $anio, 0, 4);

        //var_dump($var[0][$this->_primary[1]], $this->_primary[1]);exit;
        if (empty($var))
            return str_pad(@$var[0][$this->_primary[1]] + 1, $n, '0', STR_PAD_LEFT);
        else
            return str_pad($var[0][$this->_primary[1]] + 1, $n, '0', STR_PAD_LEFT);
    }

    /**
     * Actualiza un registro en la tabla
     * Inserta solo los valores pertencientes a la tabla lo demas lo omite  
     *
     * @param int|string $id
     * @param array $data
     * @return object
     */
    public function updateRow($id, array $data) {
        $info = $this->info();
        /* var_dump($info);
          exit; */
        $cols = array_combine($info['cols'], $info['cols']);
        $dataUpdate = array_intersect_key($data, $cols);

        return $this->find($id)->current()->setFromArray($dataUpdate)->save();
    }

    public function updateRows(array $data, $where) {
        $info = $this->info();

        $cols = array_combine($info['cols'], $info['cols']);
        $dataUpdate = array_intersect_key($data, $cols);
        $datanew = array();
        foreach ($info['metadata'] as $indice => $value) {
            if (array_key_exists($indice, $dataUpdate)) {
                $datanew[$indice] = @$dataUpdate[$indice];
                if (empty($data[$indice]) or $data[$indice] == 'null')
                    $datanew[$indice] = null;
            }
        }
        //var_dump($datanew,$where);exit;
        if (!empty($datanew))
            return $this->update($datanew, $where);
    }

    /**
     * Genera uan consulta
     *
     * @param array $cols
     * @param string $where
     * @param string $order
     * @return array
     */
    public function fetchPairs(array $cols = array('*'), $where = null, $order = null) {
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        $select = $db->select();

        $select->from($this->_name, $cols);

        if (!empty($where)) {
            $select->where($where);
        }
        if (!empty($order)) {
            $select->order($order);
        }

        $rows = $db->fetchPairs($select);
        return $rows;
    }

    public function dependentRows($row = null) {
        if (is_null($row)) {
            throw new Zend_Db_Table_Row_Exception("varible row is not defined");
        }

        if ($this->_dependentTables) {
            foreach ($this->_dependentTables as $value) {
                $rowset = $row->findDependentRowset($value);
                $numRows = count($rowset->toArray());
                if ($numRows > 0) {
                    return array('table' => $value, 'numRows' => $numRows);
                }
            }
        } else {
            return false;
        }
    }

}

?>