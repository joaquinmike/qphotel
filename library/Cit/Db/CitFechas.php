<?php

//include_once('calendar.php');
class Cit_Db_CitFechas {

    private $_formater;
    private $_tamanio;
    private $_dbadapter;
    private $_fecha;
    //private $_fechas;
    private $_data;
    private $_isrray;
    private $_mes;

    public function __construct($fecha = '', $formato = 'yyyy/mm/dd', array $data = array()) {
        $this->setFormater($formato);
    }

    private function setFormater($formato) {
        $this->_formater = $formato;
    }

    private function getFormater() {
        return $this->_formater;
    }

    /* public function setFecha($date){
      $this->_fecha =	$date;
      } */

    private function getFecha() {
        return $this->_fecha;
    }

    public function setFecha($date) {
        $this->_fecha = $date;
        if (is_array($date)):
            $this->_isrray = true;
        else:
            $this->_isrray = false;
        endif;
    }

    /* public function getFechas(){
      return $this->_fechas ;
      } */

    private function getAdapters() {

        $db = Cit_Init::config()->database->adapter;
        return $db;
    }

    public function setData($data) {
        $this->_data = $data;
        $this->setFecha($data);
    }

    public function getData() {
        return $this->_data;
    }

    public function renders($state = 'open') {
        switch ($state):
            case 'open':
                return $this->cliente();
                break;

            case 'save':
                return $this->servidor();
                break;

        endswitch;
        return $this->_data;
    }

    static function null($date) {
        if (is_null($date))
            return chr(0);
        else
            return $date;
    }

    public function servidor() {
        $fecha = '';
        if ($this->_isrray):
            //Cuando Pasa un Array;
            $bands = $this->getFecha();
            var_dump($bands);
            exit;
            if (!empty($bands)) {
                foreach ($bands as $indice => $value) {
                    //Validando si es fecha
                    $isfecha = strpos($value, '/');
                    if ($isfecha):
                        if (!empty($value)) {
                            if ($this->getAdapters() == 'Pdo_Mysql') {
                                $f = explode('/', $value);
                                $fecha = $f[2] . '-' . $f[1] . '-' . $f[0];
                                $this->_data[$indice] = $fecha;
                            }
                            if ($this->getAdapters() == 'Firebird') {
                                $f = explode('/', $value);
                                $fecha = $f[0] . '.' . $f[1] . '.' . $f[2];
                                $this->_data[$indice] = $fecha;
                            }
                            if ($this->getAdapters() == 'Pdo_Pgsql') {
                                $f = explode('/', $value);
                                $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                                $this->_data[$indice] = $fecha;
                            }
                            if ($this->getAdapters() == 'Pdo_Mssql') {
                                $f = explode('/', $value);
                                $fecha = $f[0] . '-' . $f[1] . '-' . $f[2];
                                $this->_data[$indice] = $fecha;
                            }
                        }
                    endif;
                }
            }
            return $this->_data;
        else:
            //Cuando Pasa solo un dato;
            $band = $this->getFecha();
            if (!empty($band) and strlen($band) == 10) {
                switch ($this->getAdapters()):
                    case 'Pdo_Mysql':
                        $f = explode('/', $this->getFecha());
                        $fecha = $f[2] . '-' . $f[1] . '-' . $f[0];
                        break;
                    case 'Firebird':
                        $valor = strpos($this->getFecha(), '-');
                        if ($valor)
                            $f = explode('-', $this->getFecha());
                        else
                            $f = explode('/', $this->getFecha());

                        $fecha = $f[0] . '.' . $f[1] . '.' . $f[2];
                        break;

                    case 'Pdo_Pgsql':
                        $valor = strpos($this->getFecha(), '-');
                        if ($valor)
                            $f = explode('-', $this->getFecha());
                        else
                            $f = explode('/', $this->getFecha());

                        $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                        break;
                    case 'Pdo_Mssql':
                        $valor = strpos($this->getFecha(), '-');
                        if ($valor)
                            $f = explode('-', $this->getFecha());
                        else
                            $f = explode('/', $this->getFecha());

                        $fecha = $f[0] . '-' . $f[1] . '-' . $f[2];
                        break;
                endswitch;

                return $fecha;
            }else {
                $re = @count($bands);
                if ($re > 0)
                    return '';
            }
        endif;
    }

    public function cliente() {
        $fecha = '';

        if ($this->_isrray):
            //Validando si es fecha

            $bands = $this->getFecha();
            if (!empty($bands)) {
                foreach ($bands as $indice => $value) {
                    if (!empty($value)) {
                        if ($this->getAdapters() == 'Pdo_Mysql') {
                            $f = explode('-', $value);
                            $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                            $this->_data[$indice] = $fecha;
                        }

                        if ($this->getAdapters() == 'Firebird') {
                            $f = explode('-', $value);

                            $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                            $this->_data[$indice] = $fecha;
                        }
                        if ($this->getAdapters() == 'Pdo_Pgsql') {
                            $valor = strpos($value, '-');
                            if ($valor)
                                $f = explode('-', $value);
                            else
                                $f = explode('/', $value);

                            //$f = explode('-',$value); 						
                            $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                            $this->_data[$indice] = $fecha;
                        }
                        if ($this->getAdapters() == 'Pdo_Mssql') {

                            $valor = strpos($value, '-');


                            /* :::::::::::::::::::::::::: */
                            if (strlen($value) > 10) {
                                $jj = explode(' ', $value);
                                $hh = explode('-', $jj[0]);
                                $hh[0] = str_pad($hh[0], 2, '0', STR_PAD_LEFT);
                                $hh[1] = str_pad($hh[1], 2, '0', STR_PAD_LEFT);
                                $value = $hh[2] . '-' . $hh[1] . '-' . $hh[0];
                            }
                            //echo $value;exit;							
                            /* :::::::::::::::::::::::::: */


                            if ($valor) {
                                $f = explode('-', $value);
                            } else {
                                $f = explode('/', $value);
                            }
                            $fecha = $f[0] . '/' . $f[1] . '/' . $f[2];
                            $this->_data[$indice] = $fecha;
                        }
                    }
                }
            }

            return $this->_data;
        else:
            //Cuando Pasa solo un dato;
            $band = $this->getFecha();
            if (!empty($band)) {
                switch ($this->getAdapters()):
                    case 'Pdo_Mysql':
                        $f = explode('-', $this->getFecha());
                        $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                        break;

                    case 'Firebird':
                        $valor = strpos($this->getFecha(), '.');
                        if ($valor)
                            $f = explode('.', $this->getFecha());
                        else
                            $f = explode('-', $this->getFecha());

                        $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                        break;

                    case 'Pdo_Pgsql':

                        $valor = strpos($this->getFecha(), '-');
                        if ($valor)
                            $f = explode('-', $this->getFecha());
                        else
                            $f = explode('/', $this->getFecha());

                        $fecha = $f[2] . '/' . $f[1] . '/' . $f[0];
                        break;
                    case 'Pdo_Mssql':
                        $fechita = $this->getFecha();
                        /* :::::::::::::::::::::::::::::::::::::: */
                        if (strlen($fechita) > 10) {
                            $vali_fec = strpos($fechita, '-');
                            if ($vali_fec) {
                                $f = explode('-', $fechita);
                            } else {
                                $f = explode('/', $fechita);
                            }
                            if (!empty($f[1])) {
                                $f[0] = str_pad($f[0], 2, '0', STR_PAD_LEFT);
                                $f[1] = str_pad($f[1], 2, '0', STR_PAD_LEFT);
                                $f[2] = str_pad($f[2], 2, '0', STR_PAD_LEFT);

                                $fechita = $f[0] . '/' . $f[1] . '/' . $f[2];
                                $fechita = substr($fechita, 0, 10);
                            }
                        }
                        /* :::::::::::::::::::::::::::::::::::::: */
                        if (!empty($fechita)) {
                            $valor = strpos($fechita, '-');
                            if ($valor)
                                $f = explode('-', $fechita);
                            else
                                $f = explode('/', $fechita);

                            $fecha = $f[0] . '/' . $f[1] . '/' . $f[2];
                        }
                        else
                            $fecha = '';
                        break;
                endswitch;
                //var_dump($fecha);exit;
                return $fecha;
            }
        endif;
        //var_dump($band);exit;
    }

//HOtel ---------------- -+-
    function restaFechas($desde, $hasta) {
//        $dFecIni = str_replace("-", "", $dFecIni);
        try{
            $objDesde = new Zend_Date($desde);
            $objHasta = new Zend_Date($hasta);
            $dias = $objHasta->getDate()->get(Zend_Date::TIMESTAMP) - $objDesde->getDate()->get(Zend_Date::TIMESTAMP);
            $dias = (int) ($dias / (60 * 60 * 24));
            return $dias;
        }  catch (Exception $e){
            echo $e->getMessage(); exit;
        }
       
    }
    
//    public static function diasEntreFechas(Zend_Date $desde, Zend_Date $hasta)
//    {
//        $dias = $hasta->toString(Zend_Date::TIMESTAMP) - $desde->toString(Zend_Date::TIMESTAMP);
//        $dias = (int) ($dias / (60 * 60 * 24));
//
//        return $dias;
//    }

    function getMonthDays($Month, $Year) {
        $Month = (int) $Month;
        $Year = (int) $Year;
        //Si la extensión que mencioné está instalada, usamos esa. 
        //var_dump($Month, $Year,CAL_GREGORIAN); exit;
        if (is_callable("cal_days_in_month")) {
            return @cal_days_in_month(CAL_JULIAN, $Month, $Year);
        } else {
            //Lo hacemos a mi manera. 
            return date("d", mktime(0, 0, 0, $Month + 1, 0, $Year));
        }
    }

    function nameDate($fecha = '', $idioma = 'ES') {//formato: 00/00/0000
        $fecha = empty($fecha) ? date('d/m/Y') : $fecha;

        if ($idioma == 'ES')
            $dias = array('domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sábado');
        else
            $dias = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');

        $dd = explode('/', $fecha);
        $ts = mktime(0, 0, 0, $dd[1], $dd[0], $dd[2]);
        return $dias[date('w', $ts)];
    }

    function suma_fechas($fecha, $ndias) {
        if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/", $fecha))
            list($dia, $mes, $año) = split("/", $fecha);

        if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/", $fecha))
            list($dia, $mes, $año) = split("-", $fecha);

        $nueva = mktime(0, 0, 0, $mes, $dia, $año) + $ndias * 24 * 60 * 60;
        $nuevafecha = date("d/m/Y", $nueva);

        return ($nuevafecha);
    }

    function getFormatDate($fecha, $format = 'R', $idioma = 'ES') {
        $mesObj = new DbHtCmsMeses();
        $data = explode('/', $fecha);
        switch ($format):
            case 'D'://Lista de los días de una reserva
                $name = $this->nameDate($fecha, $idioma);
                $mes = $mesObj->getMesNombre($data[1], $idioma);
                $date = ucfirst(substr($name, 0, 3)) . ' ' . $mes['mes_abr'] . ' ' . $data[0] . ', ' . $data[2];
                break;
            case 'R'://Fecha de inicio y fin de una reserva
                $name = $this->nameDate($fecha, $idioma);
                $date = ucfirst($name) . ' ' . $data[0] . ', ' . $data[2];
                break;
            case 'P'://Sat 17 Mar -> default/proceso-reserva/boxdtail
                $name = $this->nameDate($fecha, $idioma);
                $mes = $mesObj->getMesNombre($data[1], $idioma);
                $date = ucfirst($name) . ' ' . $data[0] . ', ' . $mes['mes_abr'];
                break;
        endswitch;

        return $date;
    }

    /**
     * General Cadena de Fechas
     * Devuelde en cadena todas las fechas de la reserva
     *
     * @param $info array, array donde se encuentra la fecha Ini y Fin
     * return $cadens, string
     */
    public function getChainDates($info) {
        $cadens = '(';
        $ini = $info['fecha_ini'];
        for ($i = 1; $i <= $info['dias']; $i++):
            $ftcha = explode('/', $ini);
            if ($i == 1):
                $fech['mon'] = $ftcha['1'];
                $fech['mday'] = $ftcha['0'];
                $fech['year'] = $ftcha['2'];
            else:
                $hoy = strtotime($ftcha['2'] . '-' . $ftcha['1'] . '-' . $ftcha['0']);
                //Fecha Acutal
                $ftcha = strtotime("+1 day", $hoy);
                $fech = getdate($ftcha);

                $ini = str_pad($fech['mday'], 2, '0', STR_PAD_LEFT) . '/' . str_pad($fech['mon'], 2, '0', STR_PAD_LEFT) . '/' . $fech['year'];
            endif;
            $this->setData($ini);
            $ini = $this->renders('save');
            $cadens .= "'{$ini}',";
        endfor;

        $cadens = substr($cadens, 0, strlen($cadens) - 1);
        $cadens .= ')';

        return $cadens;
    }

}