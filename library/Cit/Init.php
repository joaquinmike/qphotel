<?php
/**
 * Cit_init
 * 
 * @author 	 	Cusi Guizado Cirilo jared
 * @version 
 */
final class Cit_Init
{
	/*Direccion  raiz*/
	private static $_root;
	/*Configuracion */
	private static $_config=null;
	/*configuracion de envio*/
	private static $_env;
	/*config root*/
	private static $_traslate;
	//private static $_root;
	static function setRoot($root=null){
		if(empty($root)){
			require_once 'Cit/Exception.php';
            throw new Cit_Exception('root path is not set');
		}
		self::$_root=$root;
	}
	static function root(){
		return self::$_root;
	}
	static function setEnv($env)
	{
		self::$_env=$env;
	}
	public static function setConfig($filename, $section = null, $options = false, $type = 'ini')
	{
		//echo $filename;
		if(empty($filename))
		{
			require_once 'Zend/Config/Exception.php';
            throw new Zend_Config_Exception('Filename is not set');
		}
		if(empty($section)){
			$section=self::$_env;
		}
		if($type=='ini'){
			//echo self::root().$filename;
			//exit;
			self::$_config= new Zend_Config_Ini(self::root().$filename,$section,$options);
			//var_dump(self::root().$filename);
		}
		if($type=='xml'){
			self::$_config= new Zend_Config_Xml(self::root().$filename,$section,$options);
		}
		
	}
	public static function config()
	{
		if(empty(self::$_config)){
			require_once 'Cit/Exception.php';
			throw new Cit_Eception('Config not init');
		}
		return self::$_config;
	}
	public static function setTranslate ($locale='es')
    {    	 
        self::$_traslate = new Zend_Translate(array(
		        'adapter' => 'csv',
		        'content' => self::root().'/languages/'.strtolower($locale),
		        'locale'  => strtolower($locale),
		        'delimiter' => ';'
		    ));
    }
	public static function _($value)
    {
        if(empty(self::$_traslate)){
            /**
             * @see Zend_Config_Exception
             */
            require_once 'Cit/Exception.php';
            throw new Cit_Exception('traslate object not initialized');
        }
     return self::$_traslate->_($value);        
    }
}