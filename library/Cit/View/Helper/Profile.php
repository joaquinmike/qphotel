<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * Profile helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Cit_View_Helper_Profile extends Zend_View_Helper_Abstract
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    /**
     *  
     */
    public function profile ()
    {
        // TODO Auto-generated Zend_View_Helper_Profile::profile() helper 
        return $this;
    }

    public function timers ()
    {
        if (Cit_Init::config()->profiler->timer) {
            $timers = Cit_Profiler::getTimers();
            
            $html = '<pre>';
            $html .= '<table cellpadding="0" cellspacing="0">'."\n";
            if (! empty($timers)) {
                foreach ($timers as $key => $value) {
                    $html .= '<tr>';
                    $html .= '<td colspan="2">' . $key . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td width="100"> Start </td>';
                    $html .= '<td>' . $value['start'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Count </td>';
                    $html .= '<td>' . $value['count'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Sum </td>';
                    $html .= '<td>' . $value['sum'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Realmem </td>';
                    $html .= '<td>' . $value['realmem'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Emalloc </td>';
                    $html .= '<td>' . $value['emalloc'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Realmem Start </td>';
                    $html .= '<td>' . $value['realmem_start'] . '</td>';
                    $html .= '</tr>';
                    $html .= '<tr>';
                    $html .= '<td> Emalloc Start </td>';
                    $html .= '<td>' . $value['emalloc_start'] . '</td>';
                    $html .= '</tr>';
                }
            }
            $html .= '</table>';
            $html .= '</pre>';
            
            echo $html;
        }
    }

    public function sqlProfiler ($showquerys = true)
    {
        $profiler = Cit_Profiler::getSqlProfiler(Zend_Db_Table_Abstract::getDefaultAdapter());
        
        if (is_array($profiler) && ! empty($profiler)) {
            $html = '<pre>';
            $html .= '<table cellpadding="0" cellspacing="0">'."\n";;
            $html .= '<tr>';
            $html .= '<td width="150"> Executed </td>';
            $html .= '<td>' . $profiler['executed'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr>';
            $html .= '<td> Average Query Length </td>';
            $html .= '<td>' . $profiler['average_query_length'] . '</td>';
            $html .= '</tr>';
            
            $html .= '<tr>';
            $html .= '<td> Queries Per Second </td>';
            $html .= '<td>' . $profiler['queries_per_second'] . '</td>';
            $html .= '</tr>';
            
            $html .= '<tr>';
            $html .= '<td> Longest Qquery Length </td>';
            $html .= '<td>' . $profiler['longest_query_length'] . '</td>';
            $html .= '</tr>';
            
            $html .= '<tr>';
            $html .= '<td> Longest Query </td>';
            $html .= '<td>' . $profiler['longest_query'] . '</td>';
            $html .= '</tr>';
            
            if ($showquerys) {
                foreach ($profiler['show_queries'] as $query) {
                    $html .= '<tr>';
                    $html .= '<td colspan="2"  style="color:#cc0000">' . $query . '</td>';
                    $html .= '</tr>';
                }
            }
            $html .= '</table>';
            $html .= '</pre>';
            echo $html;
        }
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
