<?php
/**
 *
 * @author Cusi
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * HtmlHead helper
 *
 * @uses viewHelper Gnbit_View_Helper
 */
class Cit_View_Helper_HtmlHead
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    /**
     *  
     */
    public function htmlHead ()
    {
        // TODO Auto-generated Cit_View_Helper_HtmlHead::htmlHead() helper 
        return $this;
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
    
     public function js(){
     	$redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
     	return $this->urlSite().'/scripts';
     	//return $redirect->getFrontController()->getBaseUrl().'/scripts';
    } 
    
    public function css(){
        return $this->urlSite().'/styles';
    }    
    
    public function img(){
        return $this->urlSite().'/images';
    }
    
    public function urlSite(){
         return Cit_Init::config()->domain;  
    }
	public function urlSiteSubdomain(){
         return Cit_Init::config()->subdomain;  
    }
	public function getImageUrl(){
         return Cit_Init::config()->domain.'/assets/themes/default/images';  
    }
    
	public function getJs(){
		$script='';
		$data = array(
		'og/traductor.js',
		'wztooltip/wz_tooltip.js',
		//'extjs/adapter/ext/ext-base.js',
		//'extjs/ext-all.js',
		'extfix.js',
		'og/Helpers.js',
		'og/og.js',
		'og/tasks/main.js',
		'og/tasks/addTask.js',
		'og/tasks/drawing.js',
		'og/tasks/TasksTopToolbar.js',
		'og/tasks/TasksBottomToolbar.js',
		'og/tasks/print.js',
		'og/time/main.js',
		'og/time/drawing.js',
		'og/HttpProvider.js',
		'og/GooProxy.js',
		'og/WorkspaceChooser.js',
		'og/QuickAdd.js',
		'og/Permissions.js',
		'og/WorkspaceUtils.js',
		'og/CalendarDatePicker.js',
		'og/ConfirmDialog.js',
		'og/MessageManager.js',
		'og/MailManager.js',
		'og/WebpageManager.js',
		'og/UserMenu.js',
		'og/CalendarToolbar.js',
		'og/CalendarFunctions.js',
		'og/ContactManager.js',
		'og/OverviewManager.js',
		'og/TrashCan.js',
		'og/FileManager.js',
		'og/ReportingManager.js',
		'og/ReportingFunctions.js',
		'og/CalendarManager.js',
		'og/swfobject.js',
		'og/ImageChooser.js',
		'og/Sound.js',
		'og/GooPlayer.js',
		'og/ObjectPicker.js',
		'og/CSVCombo.js',
		'og/EmailCombo.js',
		'og/LoginDialog.js',
		'og/HtmlPanel.js',
		'og/WorkspacePanel.js',
		'og/TagPanel.js',
		'og/EmailAccountMenu.js',
		'og/TagMenu.js',
		'og/ContentPanelLayout.js',
		'og/ContentPanel.js',
		'og/HelpPanel.js',
		'og/layout.js',
		'og/EventPopUp.js',
		'modules/addTaskForm.js',
		'modules/addMessageForm.js',
		'modules/addContactForm.js',
		'modules/addFileForm.js',
		'modules/addProjectForm.js',
		'modules/addUserForm.js',
		'modules/massmailerForm.js',
		'modules/updatePermissionsForm.js',
		'modules/updateUserPermissions.js',
		'modules/linkObjects.js',
		'modules/linkToObjectForm.js',
		'slimey/slimey.js',
		'slimey/functions.js',
		'slimey/stack.js',
		'slimey/editor.js',
		'slimey/navigation.js',
		'slimey/actions.js',
		'slimey/tools.js',
		'slimey/toolbar.js',
		'slimey/integration.js',
		'og/DateField.js',
		'og/AjaxQuery.js',
		'og/FormsJM.js'
		);
		foreach($data as $value){
			$script.='<script src="'.$this->urlSite().'/assets/javascript/'.$value.'"></script>
			';
		}
		return $script;
	} 
	public function getJsPubli(){
		$script='';
		$data = array(
		'og/traductor.js',
		'wztooltip/wz_tooltip.js',
		//'extjs/adapter/ext/ext-base.js',
		//'extjs/ext-all.js',
		'extfix.js',
		'og/Helpers.js',
		'og/ogs.js',
		'og/tasks/main.js',
		'og/tasks/addTask.js',
		'og/tasks/drawing.js',
		'og/tasks/TasksTopToolbar.js',
		'og/tasks/TasksBottomToolbar.js',
		'og/tasks/print.js',
		'og/time/main.js',
		'og/time/drawing.js',
		'og/HttpProvider.js',
		'og/GooProxy.js',
		'og/WorkspaceChooser.js',
		'og/QuickAdd.js',
		'og/Permissions.js',
		'og/WorkspaceUtils.js',
		'og/CalendarDatePicker.js',
		'og/ConfirmDialog.js',
		'og/MessageManager.js',
		'og/MailManager.js',
		'og/WebpageManager.js',
		'og/UserMenu.js',
		'og/CalendarToolbar.js',
		'og/CalendarFunctions.js',
		'og/ContactManager.js',
		'og/OverviewManager.js',
		'og/TrashCan.js',
		'og/FileManager.js',
		'og/ReportingManager.js',
		'og/ReportingFunctions.js',
		'og/CalendarManager.js',
		'og/swfobject.js',
		'og/ImageChooser.js',
		'og/Sound.js',
		'og/GooPlayer.js',
		'og/ObjectPicker.js',
		'og/CSVCombo.js',
		'og/EmailCombo.js',
		'og/LoginDialog.js',
		'og/HtmlPanel.js',
		'og/WorkspacePanel.js',
		'og/TagPanel.js',
		'og/EmailAccountMenu.js',
		'og/TagMenu.js',
		'og/ContentPanelLayout.js',
		'og/ContentPanel.js',
		'og/HelpPanel.js',
		'og/layouts.js',
		'og/EventPopUp.js',
		'modules/addTaskForm.js',
		'modules/addMessageForm.js',
		'modules/addContactForm.js',
		'modules/addFileForm.js',
		'modules/addProjectForm.js',
		'modules/addUserForm.js',
		'modules/massmailerForm.js',
		'modules/updatePermissionsForm.js',
		'modules/updateUserPermissions.js',
		'modules/linkObjects.js',
		'modules/linkToObjectForm.js',
		'slimey/slimey.js',
		'slimey/functions.js',
		'slimey/stack.js',
		'slimey/editor.js',
		'slimey/navigation.js',
		'slimey/actions.js',
		'slimey/tools.js',
		'slimey/toolbar.js',
		'slimey/integration.js',
		'og/DateField.js',
		'og/AjaxQuery.js',
		'og/FormsJM.js'
		);
		foreach($data as $value){
			$script.='<script src="'.$this->urlSite().'/assets/javascript/'.$value.'"></script>
			';
		}
		return $script;
	}
	
	
}
