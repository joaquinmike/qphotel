<?php
/**
 *
 * @author Cusi
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * HtmlHead helper
 *
 * @uses viewHelper Gnbit_View_Helper
 */
class Cit_View_Helper_HtmlIndex {
    /**
     * @var Zend_View_Interface 
     */
    public $view;
    public $_citObj;
    public $_sesion;
    
    function __construct() {
        $this->_citObj = new CitDataGral();
        $this->_sesion = new Zend_Session_Namespace('web');
    }

    /**
     *  
     */
    public function htmlIndex() {
        // TODO Auto-generated Cit_View_Helper_HtmlHead::htmlHead() helper 
        return $this;
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView(Zend_View_Interface $view) {
        $this->view = $view;
    }

    public function js() {
        $redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
        return $this->urlSite() . '/scripts';
        //return $redirect->getFrontController()->getBaseUrl().'/scripts';
    }

    public function urlSite() {
        return Cit_Init::config()->domain;
    }

    public function css() {
        return $this->urlSite() . '/styles';
    }

    public function img() {
        return $this->urlSite() . '/img';
    }

    public function sesion_user($data) {
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        //$sesion = new Zend_Session_Namespace('web');
        $html = '';
        if (empty($this->_sesion->usid)):
            $pass = $this->_citObj->getDataGral('ht_seg_usuario', array('us_password'), "us_login = '{$data['us_id']}'", 'U');
            if (!empty($pass)):
                if (md5($data['us_pass']) == $pass['us_password'] and !empty($data['us_id'])):
                    $select->from(array('t1' => 'ht_seg_usuario'), array('us_login', 'us_id'));
                    $select->join(array('t2' => 'ht_seg_persona_hotel'), 't1.pe_id = t2.pe_id and t1.ho_id = t2.ho_id', array(''));
                    $select->join(array('t3' => 'ht_seg_persona'), 't1.pe_id = t3.pe_id', array('pe_id', 'pe_nomcomp'));
                    $select->where('t1.ho_id =?', htID);
                    $select->where('us_login =?', $data['us_id']);
                    $select->where('us_password =?', md5($data['us_pass']));
                    $select->where('per_id =?', '0003');
                    //echo $select; exit;
                    $dtaLogin = $this->_citObj->fetchAll($select)->toArray();

                    foreach ($dtaLogin as $value):
                        $this->_sesion->penomcomp = $value['pe_nomcomp'];
                        $this->_sesion->uslogin = ucfirst($value['us_login']);
                        $this->_sesion->peid = $value['pe_id'];
                        $this->_sesion->usid = $value['us_id'];
                    endforeach;
                    header('location: ' . $_SERVER['REQUEST_URI']);
                endif;

                $html .= '<p style="margin-top:25px;" class="texto_login">' . $this->_sesion->uslogin . '</p>';
                $html .= '';
                $html .= '<p style="margin-top:20px;"><a href="' . $this->htmlIndex()->urlSite() . '/close-web"><img src="' . $this->htmlIndex()->urlSite() . '/images/icons/close.png" /></a></p>';
            else:
            //echo $_SERVER['REQUEST_URI'];
            endif;
        else:
            $html .= '<p style="margin-top:25px;" class="texto_login">' . $this->_sesion->uslogin . '</p>';
            $html .= '';
            $html .= '<p style="margin-top:20px;"><a href="' . $this->htmlIndex()->urlSite() . '/close-web"><img src="' . $this->htmlIndex()->urlSite() . '/images/icons/close.png" /></a></p>';
        endif;

        return $html;
    }

    public function menu_horizontal($suid = '00000001') {//20110001
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo_categoria_sucursal'), array('art_id', 'art_titulo', 'art_link'));
        $select->where("su_id = '{$this->_sesion->su_id}' and cat_id = '20110009' and art_est = '1' and id_id = '{$this->_sesion->lg}'");
        $select->order('art_orden');
        $select->limit(4);
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();

        $html = '';
        if (!empty($dta)):
            foreach ($dta as $value):
                if (strlen($value['art_titulo']) > 9)
                    $value['art_titulo'] = substr($value['art_titulo'], 0, 9);
                //<li><a class="highlight" href="/">Home</a></li>
                if (empty($value['art_id'])):
                    $html.= '<li><a href="#">' . $value['art_titulo'] . '</a></li>';
                else:
                    if (empty($value['art_link']))
                        $url = $this->htmlIndex()->urlSite() . '/articulos/index/cat_id/20110009/art_id/' . @$value['art_id'];
                    else
                        $url = $this->htmlIndex()->urlSite() . $value['art_link'];

                    $html.= '<li><a href="' . $url . '">' . $value['art_titulo'] . '</a></li><li><span>|</span></li>';
                endif;
            endforeach;
        endif;
        $html = substr($html, 0, strlen($html) - 23);
        return $html;
    }

    public function menu_categorias($cat_id, $controller = 'index') {//20110001
        //var_dump($suid,$cat); exit;
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_sucursal_categoria'), array('cat_id'));
        //$select->join(array('t2'=>'cms_categoria_articulo'),"t1.cat_id = t2.cat_id",array('art_orden','art_est'));
        $select->join(array('t2' => 'vht_cms_categoria'), "t1.cat_id = t2.cat_id and id_id = '{$this->_sesion->lg}'", array('cat_titulo', 'cat_estado', 'cat_div'));
        $select->where("su_id = '{$this->_sesion->su_id}' and t2.cat_estado = '1' and cat_public  = 'P'");
        $select->order('cat_orden');
        $select->limit(4);
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        //<li class="active"><h3>	<a href="/lima?feature=news-events#hotel_detail">News &amp; Events</a></h3></li>
        $i = 1;
        foreach ($dta as $value):
            if (empty($cat_id)):
                $cat_id = $value['cat_id'];
            endif;

            if (!empty($value['cat_div']))
                $html.= '<div class="menu-eventos p'.$i.'"><h3><a href="' . $this->htmlIndex()->urlSite() . $value['cat_div'] . '">' . $value['cat_titulo'] . '</a></h3></div>';
            else
                $html.= '<div class="menu-eventos p'.$i.'"><h3><a href="' . $this->htmlIndex()->urlSite() . '/articulos/index/cat/' . $value['cat_id'] . '">' . $value['cat_titulo'] . '</a></h3></div>';
            /*
              switch ($value['cat_id']):
              case $cat_id:
              $html.= '<li class="active"><h3><a href="#">'.$value['cat_titulo'].'</a></h3></li>';
              break;
              default:
              $html.= '<li><h3><a href="'.$this->htmlIndex()->urlSite().'/'.$controller.'/index/suc/'.$sesion->su_id.'/cat/'.$value['cat_id'].'">'.$value['cat_titulo'].'</a></h3></li>';
              break;
              endswitch; */
            //<li><h3><a href="/lima?feature=deals#hotel_detail">Deals</a></h3></li>
            $i++;
        endforeach;

        return $html;
    }

       
    public function combo_sucursal($sucursal) {//20110001
       $cboObj = new Cit_Controller_Action_Helper_Combos;
       $cboSucursal = $cboObj->html('ht_hb_sucursal', array('where' => "ho_id = '" . htID . "'", 'order' => 'su_id'), array('id' => 'su_id', 'desc' => 'su_nombre'), $sucursal);
       return $cboSucursal;
    }

    public function menu_articulos($cat_id) {//20110001
        //$cat = $this->_citObj->getDataGral('ht_cms_sucursal_categoria', array('cat_id'=>new Zend_Db_Expr('')))
        if (empty($cat_id)):
            $select2 = $this->_citObj->select()->setIntegrityCheck(false);
            $select2->from(array('t1' => 'ht_cms_sucursal_categoria'), array('cat_id'));
            $select2->join(array('t2' => 'vht_cms_categoria'), "t1.cat_id = t2.cat_id and id_id = '{$this->_sesion->lg}'", array('cat_titulo', 'cat_estado'));
            $select2->where("su_id = '{$this->_sesion->su_id}' and t2.cat_estado = '1' and cat_public  = 'P'");
            $select2->order('cat_orden');
            $select2->limit(1);
            //echo $select2; exit;
            $dtaCat = $this->_citObj->fetchAll($select2)->toArray();
            $cat_id = @$dtaCat[0]['cat_id'];
        endif;

        $html = '';
        if (!empty($cat_id)):
            $select = $this->_citObj->select()->setIntegrityCheck(false);
            $select->from(array('t1' =>'cms_categoria_articulo'), array('su_id', 'art_id'));
            $select->join(array('t2' =>'ht_cms_idioma_art_cont'), 't1.su_id = t2.su_id and t1.cat_id = t2.cat_id and t1.art_id = t2.art_id', array('art_contenido'));
            $select->join(array('t3' =>'vht_cms_articulo'), 't1.art_id = t3.art_id and t2.id_id = t3.id_id', array('art_titulo'));
            $select->where("t1.su_id = '{$this->_sesion->su_id}' and t1.cat_id = '{$cat_id}' and t2.id_id = '{$this->_sesion->lg}'");
            $select->limit(2);
            //echo $select; exit;
            $dta = $this->_citObj->fetchAll($select)->toArray();
            //var_dump($dta); exit;
            $i = 1;
            foreach ($dta as $value):
                if (strlen($value['art_contenido']) >= 300)
                    $contenido = substr($value['art_contenido'], 0, 300) . '...';
                else
                    $contenido = $value['art_contenido'];

                switch ($i):
                    case 1:
                        $html.= '<div class="block322" style="border-right:1px solid #d1d0ce;">';
                        $html.= '<h4><a href="' . $this->htmlIndex()->urlSite() . '/articulos/index/cat_id/' . $cat_id . '/art_id/' . @$value['art_id'] . '">' . strtoupper(@$value['art_titulo']) . '</a></h4>';
                        $html.= '<p>' . @$contenido . '</p>';
                        $html.= '</div>';
                        break;
                    case 2:
                        $html.= '<div class="block322">';
                        $html.= '<h4><a href="' . $this->htmlIndex()->urlSite() . '/articulos/index/cat_id/' . $cat_id . '/art_id/' . @$value['art_id'] . '">' . strtoupper(@$value['art_titulo']) . '</a></h4>';
                        $html.= '<p>' . @$contenido . '</p>';
                        $html.= '</div>';
                        break;
                endswitch;
                $i++;
            endforeach;
        endif;
        return $html;
    }

    public function banner_imagen($suid = '00000001') {//20110001
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'cms_categoria_articulo'), array('su_id', 'cat_id', 'art_id')); //
        $select->join(array('t2' => 'cms_imagenes_articulo'), 't1.cat_id = t2.cat_id and t1.su_id = t2.su_id and t1.art_id = t2.art_id', array('im_link'));
        $select->join(array('t3' => 'ht_cms_imagenes'), 't2.im_id = t3.im_id', array('im_image4'));
        $select->where("t1.su_id = '00000001' and t1.cat_id = '20110003'");
        $select->order('im_orden');
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        foreach ($dta as $value):
            //<a href="http://www.google.com"><img src="<?php echo $this->htmlHead()->urlSite()>/img/hotel/img/slide1.jpg" /></a>
            if (!empty($value['im_link']))
                $link = $this->htmlIndex()->urlSite() . '/' . $value['im_link'];
            else
                $link = $this->htmlIndex()->urlSite() . '/articulos/index/cat_id/' . $value['cat_id'] . '/art_id/' . $value['art_id']; //$link = '/#';

            $img = $this->htmlIndex()->img() . '/' . $value['im_image4'];
            $html .= '<img src="' . $img . '" width="540" />';
        endforeach;

        return $html;
    }

    public function banner_sucursal($suid = '00000001') {//20110001
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_menu_interno'), array('cat_id', 'cat_titulo', 'cat_orden')); //
        $select->where("id_id = '{$this->_sesion->lg}'");
        $select->order('cat_orden');
        //echo $select; exit;
        $dtaCateg = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        $i == 1;
        foreach ($dtaCateg as $value):
            //<a href="http://www.google.com"><img src="<?php echo $this->htmlIndex()->urlSite()>/img/hotel/img/slide1.jpg" /></a>
            $select2 = $this->_citObj->select()->setIntegrityCheck(false);
            //$select2->from(array('t1'=>'ht_hb_sucursal_tipo_habitacion'),array('t1.tih_id'));
            $select2->join(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id', 'tih_desc', 'cat_orden'));
            $select2->join(array('t2' => 'ht_hb_tipo_habitacion_imagen'), 't1.tih_id = t2.tih_id', array('im_id'));
            $select2->join(array('t3' => 'ht_cms_imagenes'), 't2.im_id = t3.im_id', array('im_image2'));
            $select2->where("cat_id = '{$value['cat_id']}' and id_id = '{$this->_sesion->lg}'");
            $select2->order(array(new Zend_Db_Expr('random()')));
            $select2->limit(1);
            //echo $select2;exit;
            $dtatipohab = $this->_citObj->fetchAll($select2)->toArray();
            $result = @$dtatipohab[0];
            //if (!empty($result)):
                $class = 'vbar_' . $value['cat_orden'];
                $html .= ' <div class="qp-descan-1 fondo-descance'.$value['cat_orden'].' color-letter'.$value['cat_orden'].'">';

                if (!empty($result['im_image2']))
                    $img = $this->htmlIndex()->img() . '/' . $result['im_image2'];
                else
                    $img = $this->htmlIndex()->urlSite() . '/images/default/Alert.png';

                $html .= '<a href="' . $this->htmlIndex()->urlSite() . '/categoria/index/suc/' . $this->_sesion->su_id . '/interno/' . $value['cat_id'] . '"><img src="' . $img . '" alt="Stay at qp hotels" width="105"></a>';
                $html .= '<p><a href="' . $this->htmlIndex()->urlSite() . '/categoria/index/suc/' . $this->_sesion->su_id . '/interno/' . $value['cat_id'] . '">' . $value['cat_titulo'] . '</a></p>';
                $html .= '</div>';
            //endif;
            $i++;
        endforeach;
        /* $html .= '<div id="hidden-image">
          <img alt="s" src="img/red/sucursal.jpg" style="display:none;">
          </div>'; */

        return $html;
    }

    //Controller Categoria
    public function menu_publicacion($controller) {//20110001
        $sesion = new Zend_Session_Namespace('web');

        if($sesion->su_id == '00000001'){
            $sucursal = '00000002';
        }else{
            $sucursal = $sesion->su_id;
        }

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_menu_interno'), array('cat_id', 'cat_titulo')); //
        $select->where("id_id = '{$sesion->lg}'");
        $select->order('cat_orden');
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        /* if($controller=='proceso-reserva'){
          $controller = 'categoria';
          } */
        foreach ($dta as $value):

            //$link = $this->htmlIndex()->urlSite().'/'.$controller.'/index/suc/'.$sesion->su_id.'/interno/'.$value['cat_id'];
            $link = $this->htmlIndex()->urlSite() . '/categoria/index/suc/' . $sucursal . '/interno/' . $value['cat_id'];
            switch ($value['cat_id']):
                case 'H'://4
                    //$html .= '<p><a href="'.$this->htmlIndex()->urlSite().'/categoria/index/suc/'.$sesion->su_id.'/interno/'.$value['cat_id'].'">'.$value['cat_titulo'].'</a></p>';
                    $html .= '<li class="enjoy-sub-menu enjoy-back-1 color-enjoy1"><a href="' . $link . '"><span class="titulo-banner-1">' . $value['cat_titulo'] . '</span><img src="'.$this->htmlIndex()->urlSite().'/img/hotel/images/descance-1.png"/></a></li>';
                    break;

                case 'D':
                    $html .= '<li class="enjoy-sub-menu enjoy-back-2 color-enjoy2"><a href="' . $link . '"><span class="titulo-banner-2">' . $value['cat_titulo'] . '</span><img src="'.$this->htmlIndex()->urlSite().'/img/hotel/images/disfrute-1.png"/></a></li>';
                    break;

                case 'W':
                    $html .= '<li class="enjoy-sub-menu enjoy-back-3 color-enjoy3"><a href="' . $link . '"><span class="titulo-banner-3">' . $value['cat_titulo'] . '</span><img src="'.$this->htmlIndex()->urlSite().'/img/hotel/images/negocie-1.png"/></a></li>';
                    break;

                case 'E'://1
                    $html .= '<li class="enjoy-sub-menu enjoy-back-4 color-enjoy4"><a href="' . $link . '"><span class="titulo-banner-4">' . $value['cat_titulo'] . '</span><img src="'.$this->htmlIndex()->urlSite().'/img/hotel/images/explore-1.png"/></a></li>';
                    break;
            endswitch;
        endforeach;

        return $html;
    }

    //CategoriaController
    public function categoria_tipohab($interno = '') {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        switch ($interno):
            case 'H':
                $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'))//
                        ->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('tih_desc', 'tih_coment', 'cat_id'))
                        ->joinleft(array('t3' => 'ht_hb_idioma_suc_tiphab'), 't1.su_id = t3.su_id and t1.tih_id = t3.tih_id and t2.id_id = t3.id_id', array('contenido' => 'tih_desc'))
                        ->where("t2.id_id = '{$sesion->lg}' and t1.su_id = '{$sesion->su_id}' and cat_id = '$interno'")
                        ->where('t2.tih_estado = ?',1)
                        ->order('tih_orden')->limit(4);
                        //echo $select; exit;
                break;
            default:
                $select->from(array('t1' => 'ht_cms_sucursal_menu_interno'), array('tih_id')) //
                    ->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id and t1.cat_id = t2.cat_id', array('tih_desc', 'tih_coment', 'cat_id'))
                    ->where("id_id = '{$sesion->lg}' and su_id = '{$sesion->su_id}' and t1.cat_id = '$interno'")
                    ->where('t2.tih_estado = ?',1)
                    ->order('tih_orden')->limit(4);
                //echo $select; exit;
                break;
        endswitch;
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();

        $html = '';
        foreach ($dta as $value):

            $reserva = $this->htmlIndex()->urlSite() . '/proceso-reserva?ho_id=' . $sesion->su_id . '&fecha_ini=' . date('d/m/Y') . '&fecha_fin=' . date('d/m/Y') . '&num_adults=1';

            $value['tih_coment'] = (strlen(@$value['contenido']) > 0) ? $value['contenido'] : $value['tih_coment'];


            if (strlen($value['tih_coment']) > 98) {
                $coment = substr($value['tih_coment'], 0, 105) . '...';
            } else {
                if (empty($value['tih_coment']))
                    $coment = 'Sin Coment.';
                else
                    $coment = $value['tih_coment'];
            }


            $coment = str_replace("\\", '', $coment);
            switch ($interno):
                case 'H':
                    $link = $this->htmlIndex()->urlSite() . '/room/index/interno/' . $interno . '/tih/' . $value['tih_id'];
                    break;

                default:
                    $link = $this->htmlIndex()->urlSite() . '/diversion/index/interno/' . $interno . '/tih/' . $value['tih_id'];
                    break;
            endswitch;
            $html .= '<div class="main-descance">';
            $html .= '<h3><a href="' . $link . '">' . $value['tih_desc'] . '</a></h3>';
            $html .= '<p>' . $coment . '</p>';
            $html .= '<div class="boton-descance"><a href="' . $link . '">' . Cit_Init::_('RES_MASDET') . '</a>';
            //var_dump($value['cat_id']); exit;
            if ($value['cat_id'] == 'H')
                $html .= '<p><input type="button" onclick="getReservar();" value="' . Cit_Init::_('RES_ROOM') . '" class="button"/></p>';
            $html .= '</div></div>';
        endforeach;
        //var_dump($html); exit;
        return $html;
    }

    //CategoriaController
    public function tipohab_imagen($interno = '', $su_id = '00000001') {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id', 'tih_desc'))
            ->join(array('t2' => 'ht_hb_tipo_habitacion_imagen'), 't1.tih_id = t2.tih_id', array('im_id'))
            ->join(array('t3' => 'ht_cms_imagenes'), 't2.im_id = t3.im_id', array('im_image4'))
            ->where('cat_id =?', $interno)
            ->where('su_id =?', $su_id)
            ->where('id_id =?', $sesion->lg);
        //$select->where("cat_id = '$interno' and id_id = '{$sesion->lg}'");
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        if (!empty($dta)):
            foreach ($dta as $value):
                //<img src="<?php echo $this->htmlHead()->urlSite()>/img/hotel/img/slide1.jpg" width="645" height="350"/>  
                $html .= '<img src="' . $this->htmlIndex()->img() . '/' . $value['im_image4'] . '" width="685" height="300"/>';
            endforeach;
        endif;
        return $html;
    }

    //RoomController
    public function habitacion_imagen($tih_id = '') {//20110001
        $sesion = new Zend_Session_Namespace('web');
        
        /*$select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id', 'tih_desc'));
        $select->join(array('t2' => 'ht_hb_tipo_habitacion_imagen'), 't1.tih_id = t2.tih_id', array('im_id'));
        $select->join(array('t3' => 'ht_cms_imagenes'), 't2.im_id = t3.im_id', array('im_image4'));
        $select->where("cat_id = 'H' and tih_id = '$tih_id' and id_id = '{$sesion->lg}'");*/
        
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_tipo_habitacion_imagen'), array(''))
        //$select->join(array('t2' => 'ht_hb_tipo_habitacion_imagen'), 't1.hb_id = t2.hb_id', array('im_id'));
            ->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('im_image4'))
            ->where('tih_id =?', $tih_id)
            ->where('su_id =?', $sesion->su_id);
//        $select->where("tih_id = '$tih_id' and su_id = '{$sesion->su_id}'");
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        foreach ($dta as $value):
            //<img src="<?php echo $this->htmlHead()->urlSite()/img/hotel/img/slide1.jpg" width="645" height="350"/>    
            $html .= '<img src="' . $this->htmlIndex()->img() . '/' . $value['im_image4'] . '" width="685" height="300"/>';
        endforeach;
        
        if(empty($html))
            $html .= '<img src="' . $this->htmlIndex()->urlSite() . '/images/default/room-sin-imagen.png" />';
        return $html;
    }

    //RoomController
    public function habitacion_lista($tih_id = '', $interno) {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'));
        $select->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id and id_id = ' . "'{$sesion->lg}'", array('tih_desc'));

        $select->where("cat_id = '$interno' and su_id = '{$sesion->su_id}'");
        $select->order('tih_orden');
        $select->limit(4);
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';

        if (!empty($dta)):
            foreach ($dta as $value):
                //$value['tih_desc'] = substr($value['tih_desc'], 0, strlen($value['tih_desc']) - 6);
                switch ($value['tih_id']):
                    case $tih_id:
                        $html .= ' <li id="activado">' . $value['tih_desc'] . '</li>';
                        break;

                    default:
                        $link = $this->htmlIndex()->urlSite() . '/room/index/interno/' . $interno . '/tih/' . $value['tih_id'];
                        $html .= ' <li><a href="' . $link . '">' . $value['tih_desc'] . '</a></li>';
                        break;
                endswitch;
            endforeach;
        endif;


        return $html;
    }

    //RoomController
        //SEWEController
    public function SEWE_lita($tih_id = '', $interno) {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_sucursal_menu_interno'), array('tih_id'));
        $select->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id and t1.cat_id = t2.cat_id and id_id = ' . "'{$sesion->lg}'", array('tih_desc'));

        $select->where("t1.cat_id = '$interno' and su_id = '{$sesion->su_id}'");
        $select->order('tih_orden');
        //$select->limit(4);
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';

        if (!empty($dta)):
            foreach ($dta as $value):
                //$value['tih_desc'] = substr($value['tih_desc'], 0, strlen($value['tih_desc']) - 6);
                switch ($value['tih_id']):
                    case $tih_id:
                        $html .= ' <li id="activado">' . $value['tih_desc'] . '</li>';
                        break;

                    default:
                        $link = $this->htmlIndex()->urlSite() . '/diversion/index/interno/' . $interno . '/tih/' . $value['tih_id'];
                        $html .= ' <li><a href="' . $link . '">' . $value['tih_desc'] . '</a></li>';
                        break;
                endswitch;
            endforeach;
        endif;

        return $html;
    }
    //
    public function habitacion_accesorios($tih_id = '',$limit = null) {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false)
            ->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'))
            ->join(array('t2' => 'ht_hb_accesorio_sucursal_tiphab'), 't1.tih_id = t2.tih_id and t1.su_id = t2.su_id', array(''))
            ->join(array('t3' => 'vht_hb_accesorios'), 't2.ac_id = t3.ac_id', array('ac_desc'))
            ->where("t1.tih_id = '$tih_id' and t1.su_id = '{$sesion->su_id}' and id_id = '{$sesion->lg}'");
        //$select->order('cat_orden');
        if(!empty($limit))
            $select->limit($limit);
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';

        foreach ($dta as $value):
            $html .= '<li>' . $value['ac_desc'] . '</li>';
        endforeach;
        return $html;
    }

    public function combo_reserva($su_id) {
        if(empty($su_id)):
            $sesion = new Zend_Session_Namespace('web');
            $su_id = $sesion->su_id;
        endif;
        $cboObj = new Cit_Controller_Action_Helper_Combos;
        $cboSucursal = $cboObj->html('ht_hb_sucursal', array('where' => "ho_id = '" . htID . "' and su_portal = 'N'", 'order' => 'su_id'), array('id' => 'su_id', 'desc' => 'su_nombre'),$su_id);
        //var_dump($cboSucursal);exit;
        return $cboSucursal;
    }  
    public function articulos_imagenes(array $data = Array()) {

        $sesion = new Zend_Session_Namespace('web');
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'cms_imagenes_articulo'), array('im_link'));
        $select->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('im_image2', 'im_image4'));
        $select->where("t1.su_id = '{$sesion->su_id}' and t1.cat_id = '{$data['cat_id']}' and art_id = '{$data['art_id']}'");
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();

        $html = '';
        $i = 0;

        if (!empty($dta)):
            foreach ($dta as $value):
                if ($i % 5 == 0):
                    if ($i > 0) {
                        $html .= '</div>';
                    }
                    $html .= '<div style="clear:both">';
                    $html .= '<div style="float:left; margin-left:5px;">';
                    $html .= '<a href="' . $this->htmlIndex()->img() . '/' . $value['im_image4'] . '" rel="lightbox"><img src="' . $this->htmlIndex()->img() . '/' . $value['im_image2'] . '"/></a>';
                    $html .= '</div>';
                else:
                    $html .= '<div style="float:left; margin-left:5px;">'; //style="margin-left:5px;"
                    $html .= '<a href="' . $this->htmlIndex()->img() . '/' . $value['im_image4'] . '" rel="lightbox"><img src="' . $this->htmlIndex()->img() . '/' . $value['im_image2'] . '"/></a>';
                    $html .= '</div>';
                endif;
                //<img src="<?php echo $this->htmlHead()->urlSite()/img/hotel/img/slide1.jpg" width="645" height="350"/>    
                $i++;
            endforeach;
        endif;
        return $html;
    }

    public function promo_imagenes(array $data = Array()) {

        $sesion = new Zend_Session_Namespace('web');
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promo_imagen'), array(''));
        $select->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('images' => 'im_image5'));
        $select->where("t1.su_id = '{$data['su_id']}' and t1.an_id = '{$data['an_id']}' and pro_id = '{$data['pro_id']}'");
        //echo $select;exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        //$html = '<div class="slide"';
        $i = 0;

        if (!empty($dta)):
            foreach ($dta as $value):
                $html .= '<div class="slide1">
  				<img src="' . $this->htmlIndex()->img() . '/' . $value['images'] . '" alt="" width="615"/>
                          </div>';
            /*
              if($i % 5 == 0):
              if($i > 0){$html .= '</div>';}
              $html .= '<div style="clear:both">';
              $html .= '<div style="float:left; margin-left:5px;">';
              $html .= '<img src="'.$this->htmlIndex()->img().'/'.$value['im_image2'].'"/>';
              $html .= '</div>';
              else:
              $html .= '<div style="float:left; margin-left:5px;">';
              $html .= '<img src="'.$this->htmlIndex()->img().'/'.$value['im_image2'].'"/>';
              $html .= '</div>';
              endif;
              $i++; */
            endforeach;
        endif;
        //$html .= '</div>';
        return $html;
    }

    public function public_images($codigo) {
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_public_imagen'), array(''));
        $select->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('images' => 'im_image4'));
        $select->where("pub_id = '{$codigo}'");
        $select->order(new Zend_Db_Expr('random()'));
        $select->limit(1);
        //echo $select;exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        //$html = '<div class="slide"';
        $i = 0;

        if (!empty($dta)):
            $html = $this->htmlIndex()->img() . '/' . $dta[0]['images'];
        endif;
        //$html .= '</div>';
        return $html;
    }

    public function promo_img_small(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('web');
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promo_imagen'), array(''));
        $select->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('im_image1'));
        $select->where("t1.su_id = '{$data['su_id']}' and t1.an_id = '{$data['an_id']}' and pro_id = '{$data['pro_id']}'");
        $dta = $this->_citObj->fetchAll($select)->toArray();

        $html = '';
        //$html = '<div class="slide"';
        $i = 0;

        if (!empty($dta)):
            foreach ($dta as $value):
                $html .= '<li class="menuItem"><a href="">
  							<img src="' . $this->htmlIndex()->img() . '/' . $value['im_image1'] . '" alt="" />
 						  </a></li>';
            /*
              if($i % 5 == 0):
              if($i > 0){$html .= '</div>';}
              $html .= '<div style="clear:both">';
              $html .= '<div style="float:left; margin-left:5px;">';
              $html .= '<img src="'.$this->htmlIndex()->img().'/'.$value['im_image2'].'"/>';
              $html .= '</div>';
              else:
              $html .= '<div style="float:left; margin-left:5px;">';
              $html .= '<img src="'.$this->htmlIndex()->img().'/'.$value['im_image2'].'"/>';
              $html .= '</div>';
              endif;
              $i++; */
            endforeach;
        endif;
        //$html .= '</div>';
        return $html;
    }

    public function promoIndex($suid = '') {
        $fecha = date('d/m/Y');
        $fecObj = new Cit_Db_CitFechas();

        $fecObj->setFecha($fecha);
        $fecha = $fecObj->renders('open');
        $sesion = new Zend_Session_Namespace('web');
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_promocion'), array('an_id', 'pro_id', 'pro_titulo', 'pro_desc'))
                //->join(array('t2'=>'ht_cms_idioma_promocion'),'t1.an_id = t2.an_id and t1.pro_id = t2.pro_id and id_id = '."'{$sesion->lg}'", array('pro_titulo','pro_desc'))
                ->join(array('t3' => 'ht_cms_promocion_sucursal'), 't1.tih_id = t3.tih_id and t1.pro_id = t3.pro_id and t1.an_id = t3.an_id', array('su_id', 'tih_id'))
                //->joinLeft(array('t4'=>'ht_cms_promo_imagen'),'t1.an_id = t4.an_id and t1.pro_id = t4.pro_id and t1.tih_id = t4.tih_id',array(''))
                //->joinLeft(array('t5'=>'ht_cms_imagenes'),'t4.im_id = t5.im_id',array()) //ht_cms_imagenes
                ->where("'$fecha' <= pro_fecfin and id_id = '{$sesion->lg}' and t1.pro_estado = 'S'");
        if (!empty($suid))
            $select->where("su_id = '{$suid}'");
        $select->order(new Zend_Db_Expr('random()'));
        $select->limit(3);
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        if (!empty($dta)):
            foreach ($dta as $value):
                $datos = $this->returnImagen($value);
        
                $html .= '<div class="promocion-img">';
                $html .= '<a href="' . $datos['link'] . '"><img src="' . $datos['image'] . '" /></a>';
                $html .= '</div>';
            /*
              if(strlen($value['pro_desc'])> 90)
              $coment = substr($value['pro_desc'], 0,87).'...';
              else
              $coment = $value['pro_desc'];

              $link =  $this->htmlIndex()->urlSite().'/promociones/index/cod/'.$value['su_id'].'-'.$value['an_id'].'-'.$value['pro_id'];
              $html .= '<div class="block322 hauto"><div class="inner">';
              $html .= '<h3><a href="'.$link.'">'.strtoupper($value['pro_titulo']).'</a></h3>';
              $html .= '<p>'.$coment.'</p>';
              $html .= '</div></div>'; */
            endforeach;
        else:
            $html .= '<div class="block322"></div>
                    <div class="block322"></div>
                    <div class="block322"></div>';
        endif;
        return $html;
    }

    //PromocionController
    public function returnImagen(array $data = Array(), $table = '') {
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promo_imagen'), array(''))
                ->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('images' => 'im_image3')) //ht_cms_imagenes
                ->where("t1.tih_id = '{$data['tih_id']}' and t1.an_id = '{$data['an_id']}' and t1.pro_id = '{$data['pro_id']}' and t1.su_id = '{$data['su_id']}'")
                //->order(new Zend_Db_Expr('random()'))
                ->limit(1);
        //echo $select;// exit;	
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $image = '';
        if (empty($dta[0])):
            $image = $this->htmlIndex()->urlSite() . '/images/default/noimage.png';
            $link = '#';
        else:
            $image = $this->htmlIndex()->urlSite() . '/img' . $dta[0]['images'];
            $link = $this->htmlIndex()->urlSite() . '/promociones/index/cod/' . $data['su_id'] . '*' . $data['an_id'] . '*' . $data['pro_id'];
        endif;
        return array('link' => $link, 'image' => $image);
    }

    public function promo_lista() {
        $sesion = new Zend_Session_Namespace('web');
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_promocion'), array('pro_id', 'pro_titulo', 'pro_desc', 'pro_fecalta', 'pro_fecini', 'pro_fecfin', 'pro_dscto', 'tih_id', 'an_id'))
                ->where("pro_estado = 'S' and an_id = '" . date('Y') . "' and id_id = '{$sesion->lg}'")
                ->order(array('tih_id', 'pro_id'))
                ->limit(10);

        $dta = $this->_citObj->fetchAll($select)->toArray();
        foreach ($dta as $value):
            $datos = $this->returnImagen($value);
            //var_dump($datos);
            $html .= '<div class="fila_zoe">';
            $html .= '<div class="left_zoe"><img src="' . $datos['image'] . '" /></div>';
            $html .= '<div class="left_zoe">';
            $html .= '<div class="fila_zoe">' . $value['pro_titulo'] . '</div>';
            $html .= '<div class="fila_zoe">' . $value['pro_desc'] . '</div>';
            $html .= '</div>';
            $html .= '</div>';
        /*
          if(strlen($value['pro_desc'])> 90)
          $coment = substr($value['pro_desc'], 0,87).'...';
          else
          $coment = $value['pro_desc'];

          $link =  $this->htmlIndex()->urlSite().'/promociones/index/cod/'.$value['su_id'].'-'.$value['an_id'].'-'.$value['pro_id'];
          $html .= '<div class="block322 hauto"><div class="inner">';
          $html .= '<h3><a href="'.$link.'">'.strtoupper($value['pro_titulo']).'</a></h3>';
          $html .= '<p>'.$coment.'</p>';
          $html .= '</div></div>'; */

        endforeach;
        //exit;
        return $html;
    }

    //DiversionController
    public function interno_imagen($interno, $tih_id = '') {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_sucursal_menu_interno'), array('tih_id'));
        $select->join(array('t2' => 'ht_hb_tipo_habitacion_imagen'), 't1.tih_id = t2.tih_id', array('im_id'));//  and t1.su_id = t2.su_id
        $select->join(array('t3' => 'ht_cms_imagenes'), 't2.im_id = t3.im_id', array('im_image4'));
        $select->where("cat_id = '{$interno}' and t1.tih_id = '$tih_id' and t1.su_id = '{$sesion->su_id}'");
        //$select->order('cat_orden');
        //echo $select; exit;
        $dta = $this->_citObj->fetchAll($select)->toArray();
        $html = '';
        if (!empty($dta)):
            foreach ($dta as $value):
                //<img src="<?php echo $this->htmlHead()->urlSite()/img/hotel/img/slide1.jpg" width="645" height="350"/>    
                $html .= '<img src="' . $this->htmlIndex()->img() . '/' . $value['im_image4'] . '" width="670" />';
            endforeach;
        endif;


        return $html;
    }
    
    public function getDescripciones($id = '',$idioma = ''){
        $desc = '';
        $where = '';
        $idioma = (empty($idioma))?$this->_sesion->lg:$idioma;
        
        $dtaVerde = $this->_citObj->getDataGral('vht_cms_descripciones', array('des_contenido'),"des_id = '{$id}' and id_id = '{$idioma}'",'U');
        $desc = str_replace('\"', '"', $dtaVerde['des_contenido']);
        $desc = str_replace("\'", "'", $desc);
		
		return $desc;
		
        
    }
	public function getDescripciones2($id = '',$idioma = ''){
        $desc = '';
        $where = '';
        $idioma = (empty($idioma))?$this->_sesion->lg:$idioma;
        
        $dtaVerde = $this->_citObj->getDataGral('vht_cms_descripciones', array('des_contenido'),"des_id = '{$id}' and id_id = '{$idioma}'",'U');
        $desc = str_replace('\"', '"', $dtaVerde['des_contenido']);
        $desc = str_replace("\'", "'", $desc);
		if(strlen($desc)<110){
		    return $desc;
		}else{
            return substr($desc,0,30);
        }
        
    }
    /**
     * Devolver Contenido de un tipo de habitaciones
     * de una sucursal
     *
     * @param $id string id->tipo de habitacion
     * @param $suid id->sucursal
     * @param $suid id->idioma
     * @return array de datos
     */
    public function getContenidoTipHabSuc($id,$suid,$lg = 'ES'){
        
        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_idioma_tipo_habitacion'), array('tih_coment'))
            ->joinleft(array('t2' => 'ht_hb_idioma_suc_tiphab'), 't1.tih_id = t2.tih_id and t1.id_id = t2.id_id and su_id = '."'{$suid}'", array('tih_desc'))
            ->where("t1.tih_id = '{$id}' and t1.id_id = '{$lg}'");
      
       $data = $this->_citObj->fetchRow($select)->toArray();  
        
        if(empty($data['tih_desc']))
            $texto = $data['tih_coment'];
        else
            $texto = $data['tih_desc'];
        
        return $texto;
    }
    
     /**
     * Devolver las imagenes de Tipo de Habitacion
     * de una sucursal
     *
     * @param $id string id->tipo de habitacion
     * @return array de datos
     */
    public function p_image_tiphab($tihid) {//20110001
        $sesion = new Zend_Session_Namespace('web');

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_tipo_habitacion_imagen'), array('im_id'))
            ->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array('im_image3'))
            ->where("tih_id = '$tihid'")
            ->limit(1);
        //$select->order('cat_orden');
       $data = $this->_citObj->fetchAll($select)->toArray();
        
        if(empty($data['im_image3']))
            $html = $this->htmlIndex()->urlSite() . '/images/default/sin-imagen-prewiew.png';
        else{
            
            $html = $this->htmlIndex()->img() . '/' . $data[0]['im_image3'];
        }  
        return $html;
    }

    /**
     * Devolver la lista de servicios 
     * de una sucursal
     *
     * @param $suid id->sucursal
     * @param $idioma id->idioma
     * @return array de datos
     */
    public function p_servicios_tiphab($suid, $additional = array(), $idioma = 'ES') {//20110001

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_ser_servicio'), array('ser_id','ser_titulo','ser_desc','ser_precio'))
            ->join(array('t2' => 'ht_ser_servicio_sucursal'), 't1.ser_id = t2.ser_id', array(''))
            ->where("su_id = '{$suid}' and id_id = '{$idioma}'")
            ->order('ser_id');
            
        $data = $this->_citObj->fetchAll($select)->toArray();
        
        $html = ''; $i = 1;
        foreach ($data as $value):
            $price = (float)$value['ser_precio'];
            $id = 'additional-' . $i;
            if(in_array($value['ser_id'], $additional['id']))
                $check = 'checked="checked"';
            else
                $check = '';
            $html .= '<li><input type="checkbox" id="' . $value['ser_id'] . 
                    '" name="additional[' . $value['ser_id'] . ']" value="'. 
                    round($value['ser_precio'],2) .'" '. $check .' onClick="sumServices(this)" />' . 
                    $value['ser_titulo'] . ' <span>'. $value['ser_desc'] .
                    '</span><span class="price right">$'. number_format($value['ser_precio'], 2) . 
                    ' USD</span>' . '</li>';
            //<li><input type="checkbox" name="additional" id="additional-1" value="35"/>* TAXI PRICE <span>FROM THE AIRPORT TO HOTEL:</span><span class="price right">$35.00 USD</span></li>
            $i++;
        endforeach;
        
        return $html;
    }
    
    /**
     * Devolver la lista de servicios 
     * de una sucursal
     *
     * @param $suid id->sucursal
     * @param $idioma id->idioma
     * @return array de datos
     */
    public function p_accesorios_tiphab($suid, $additional = array(), $tipohab = '',$idioma = 'ES', $noches) {//20110001

        $select = $this->_citObj->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_accesorios'), array('ac_id','ac_desc','ac_precio'))
            ->join(array('t2' => 'ht_hb_accesorio_sucursal_tiphab'), 't1.ac_id = t2.ac_id and t2.tih_id = ' . "'{$tipohab}'", array(''))
            ->where('su_id =?', $suid )
            ->where('id_id =?' ,$idioma )
            ->where('ac_precio > 0')
            ->order('ac_id');
        //echo $select; exit;
        $data = $this->_citObj->fetchAll($select)->toArray();
        
        $html = ''; $i = 1;
        foreach ($data as $value):
            $id = 'additional-' . $i;
             if(in_array($value['ac_id'], $additional['id']))
                $check = 'checked="checked"';
            else
                $check = '';
            
            $html .= '<li><input type="checkbox" id="' . $value['ac_id'] . 
                    '" name="amenities[' . $value['ac_id'] . ']" value="'. 
                    round($value['ac_precio'] * $noches,2) .'" '. $check .' onClick="sumServices(this)" />' . 
                    $value['ac_desc'] . '<span class="price right">$'. number_format($value['ac_precio'], 2) . 
                    ' USD</span>' . '</li>';
            //<li><input type="checkbox" name="additional" id="additional-1" value="35"/>* TAXI PRICE <span>FROM THE AIRPORT TO HOTEL:</span><span class="price right">$35.00 USD</span></li>
            $i++;
        endforeach;
        
        return $html;
    }
}
