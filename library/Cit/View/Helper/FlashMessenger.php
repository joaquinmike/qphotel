<?php
/**
 * Enter description here...
 *
 */
class Cit_View_Helper_FlashMessenger
{
    private $_types = array( 
        Cit_Controller_Action_Helper_GFlashMessenger::ERROR, 
        Cit_Controller_Action_Helper_GFlashMessenger::WARNING, 
        Cit_Controller_Action_Helper_GFlashMessenger::NOTICE, 
        Cit_Controller_Action_Helper_GFlashMessenger::SUCCESS 
    ); 

    /**
     * Enter description here...
     *
     * @return unknown
     */
    public function flashMessenger ()
    {
        $flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('GFlashMessenger');
        
        $html = null;
        

        foreach ($this->_types as $type) {
            $messages = $flashMessenger->getMessages($type);
            if (! $messages) {
                $messages = $flashMessenger->getCurrentMessages($type);
            }
            if ($messages) {
                $html .= '<div class="messages">';
                $html .= '<div class="' . strtolower($type) . '">';
                $html .= '<ul style="color: red; list-style-type: none; ">';
                foreach ($messages as $message) {
                    $html .= '<li style="color: red; list-style-type: none;">';
                   // $html .= '<img src="/images/alert/' . strtolower($type) . '.jpg">';
                    $html .= $message->messageText;
                    $html .= '</li>';
                }
                $html .= '</ul>';
                $html .= '</div>';
                $html .= '</div>';
            }
        }
        
        return $html;
    }
}

