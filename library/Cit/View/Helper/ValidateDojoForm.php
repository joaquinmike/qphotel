<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * ValidateDojoForm helper
 *
 * @uses viewHelper Gnbit_View_Helper
 */
class Gnbit_View_Helper_ValidateDojoForm
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    /**
     *  
     */
    public function validateDojoForm ($formId)
    {
        $this->view->dojo()->javascriptCaptureStart(); ?>
         
	function validateForm() {
      	var frm = dijit.byId("<?php echo $formId; ?>");
       	if(!dijit.byId(frm).validate()){
			dijit.byId('dialog1').show()
			//alert('error');
	 	}else{
			dijit.byId(frm).submit();	
		}
	}	
        <?php $this->view->dojo()->javascriptCaptureEnd();
        
        $this->reset($formId);
        $this->gnform();
    }

    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
	
	private function gnform(){
		$this->view->dojo()->onLoadCaptureStart();?>
function() {
dojo.forEach(zendDijits, function(info) {
		
	   	if(info.params.dojoType == 'dijit.form.FilteringSelect'){
			if(info.params.autocomplete=='false'){
				dojo.byId(info.id).readOnly = 'readonly';
			}
			if(dijit.byId(info.id).attr('value')  ==''){
				dijit.byId(info.id).attr('value', '');
			}
	   	}

	   	if(dojo.byId(info.id).readOnly){
	   		dojo.style(dojo.byId(info.id),'background','#D7D7D7');
	   		dojo.style(dojo.byId(info.id),'color','#900');
		}

		/*if(info.params.required){
			dojo.style(dojo.byId(info.id),'background','#F9F7BA');
				
		}*/
   });
}
		
		<?php $this->view->dojo()->onLoadCaptureEnd();
	}
	
	private function reset($formId)
	{
	    $this->view->dojo()->javascriptCaptureStart(); ?>    
	    
	function resetForm(){
		dijit.byId("<?php echo $formId; ?>").reset();	
	}
	      <?php $this->view->dojo()->javascriptCaptureEnd();
	}
	
}
