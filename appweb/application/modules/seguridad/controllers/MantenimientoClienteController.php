<?php

/**
 * MantenimientoClienteController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoClienteController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_alumnoOservacion;
    public $_control;
    public $_ubigeo;
    public $_tbadetalle;
    public $_ano;
    public $_cliente;
    public $_sesion;
    public $_colegioObj;

    function init() {

        $this->_ubigeo = new DbHtSegUbigeo();

        $this->_tbadetalle = new DbVtablaDetalle();

        $this->_cliente = new DbHtSegCliente();

        $this->_sesion = new Zend_Session_Namespace('login');
    }

    public function formAction() {
        $this->view->sexo = $this->_helper->Combos(array('M' => 'Masculino', 'F' => 'Femenino'), array('id' => 'pe_sexo'));

        $this->view->petipdoc = $this->_helper->Combos($this->_tbadetalle, array('id' => 'tad_id', 'desc' => 'tad_desc', 'where' => "tba_id = '011' and id_id = '{$this->_sesion->lg}'"));
        //echo $this->view->petipdoc ;exit; 
        $this->view->pepais = $this->_helper->Combos($this->_tbadetalle, array('id' => 'tad_id', 'desc' => 'tad_desc', 'where' => "tba_id = '022' and id_id = '{$this->_sesion->lg}'"));

        $this->view->penacionalidad = $this->_helper->Combos($this->_tbadetalle, array('id' => 'tad_id', 'desc' => 'tad_desc', 'where' => "tba_id = '001' and id_id = '{$this->_sesion->lg}'"));

        $this->view->pedepartamento = $this->_helper->Combos($this->_ubigeo, array('id' => 'ub_id', 'mask' => 'ub_id_dep', 'desc' => 'ub_desc', 'where' => "substr(ub_id,3,4)='0000'", 'order' => 'ub_desc'));

        $this->view->clestado = $this->_helper->Combos(array('1' => 'Activo', '0' => 'Inactivo'), array('id' => 'cl_estado'));

        $this->view->idclient = $this->_cliente->correlativo(8);
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $post = $this->getRequest()->getParams();
        switch ($case) {
            case 'save':
                $this->_helper->Alert($this->_cliente->saveData($post));

                break;
            case 'delete':
                $post['action'] = 'delete';
                $post['anio'] = $this->_sesion->anio;
                $post['cos_id'] = $this->_sesion->cosid;
                $this->_helper->Alert($this->_cliente->deleteCliente($post), '', '', '', 'El Alumno Tiene Datos Relacionados');
                break;
            case 'saveimage':
                //error_reporting(NULL); 
                $post['anio'] = $this->_sesion->anio;
                $post['cos_id'] = $this->_sesion->cosid;
                $this->_alumno->saveImagen($post);
                echo json_encode(array());
        }
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        //var_dump($_SESSION);exit;
        switch ($case) {
            case 'clientes':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);

                $select->from(array('t1' => 'vht_seg_cliente'), array('*'))
                        ->join(array('t2' => 'ht_seg_persona_hotel'), 't1.pe_id=t2.pe_id', array('cl_id_id' => 't1.cl_id'))
                        ->where("t2.ho_id = '{$this->_sesion->hoid}'")
                        ->order('t1.pe_nomcomp');
                //echo $select;exit;
                $data = $select->query()->fetchAll();
                foreach ($data as $indice => $value) {
                    $value['cl_estado'] = (int) $value['cl_estado'];
                    $value['ub_id_dep'] = str_pad(substr($value['ub_id'], 0, 2), 6, '0', STR_PAD_RIGHT);
                    $value['ub_id_prov'] = str_pad(substr($value['ub_id'], 0, 4), 6, '0', STR_PAD_RIGHT);

                    $data[$indice] = $value;
                }

                if (!empty($start)) {
                    $pagina = (int) (($start / $limit) + 1);
                }
                else
                    $pagina = 1;
                $paginator = Zend_Paginator::factory($data);
                $paginator->setCurrentPageNumber($pagina)
                        ->setItemCountPerPage($limit);
                foreach ($paginator->getCurrentItems() as $value) {
                    $dat[] = $value;
                }
                if (!empty($dat)) {
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(Array('totalCount' => $total, 'matches' => $dat, 'page' => $pagina, 'total' => $paginator->getTotalItemCount(), 'records' => $records, "success" => 'true'));
                } else {
                    echo $this->_cliente->json(array());
                }
        }
    }

}
