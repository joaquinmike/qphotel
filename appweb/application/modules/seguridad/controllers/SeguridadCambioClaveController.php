<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class SeguridadCambioClaveController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;
	public $_usuarioObj;
	function init(){
	
		$this->_usuarioObj = new DbUsuario();
	    $this->_sesion = new Zend_Session_Namespace('login');
	}
    public function formAction() 
    {
		$select = $this->_helper->DBAdapter()->select();
    	$arrayTipo = array();
  		$select->from(array('vtipo_usuario'),array('tip_id','tip_desc'))
					->where("id_id='{$this->_sesion->lg}'")
					->order('tip_desc');
  		$tipousuario = $select->query()->fetchAll();
    	foreach ($tipousuario as $value){
  			$arrayTipo[$value['tip_id']]=$value['tip_desc'];
  		}
  		//var_dump($arrayTipo );exit;
	
		$this->view->listaTipoUsuario = $this->_helper->Combos($arrayTipo,array('id'=>'tip_id','desc' => 'tip_desc','order'=>'tip_desc'));
		
		$this->view->nombreCompleto= $this->_sesion->penomcomp;
		$this->view->uslogin= $this->_sesion->uslogin;
		
		
    }
    
    
    public function jsonAction(){
    	//Desabilitar la capa
		$this->_helper->layout->disableLayout();
		//Para no Cargar la vista
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$select = $this->_helper->DBAdapter()->select();
		
		switch ($case):
    		case 'datoPersonal':
    		
	    		
				break;
	    
		endswitch;	
	}
	
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):		
	    	case 'save':
	    		$usuarioObj=new DbUsuario();
					    	
	    		$errNum = $usuarioObj->updateClave($_POST['data']);
	    		break;
	    	
		endswitch;
		$this->_helper->Alert($errNum,'','Registro grabado Correctamente','Registro Existente','Registro con un solo Grupo o Grupos Existentes');		
	}	      
}
