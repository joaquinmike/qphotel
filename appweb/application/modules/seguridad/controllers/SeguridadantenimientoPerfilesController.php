<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class SeguridadMantenimientoPerfilesController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;
	public $_tipoUsuarioObj;
	public $_idiomaTipoUsuarioObj;
	function init(){
	
		$this->_tipoUsuarioObj = new DbTipoUsuario();
		$this->_idiomaTipoUsuarioObj = new DbIdiomaTipoUsuario();
	    $this->_sesion = new Zend_Session_Namespace('login');
	}
    public function formAction() 
    {

		
    }
    
    
    public function jsonAction(){
    	//Desabilitar la capa
		$this->_helper->layout->disableLayout();
		//Para no Cargar la vista
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$select = $this->_helper->DBAdapter()->select();
		
		switch ($case):
    		case 'lista':
    		
    			//$lleva = new Cit_Db_Table_Expr('concat',array('t1.us_id','t1.cos_id','t3.pe_id','t1.tip_id'),'##');
				
    			
    			$select->from(array('t1'=>'vtipo_usuario'),array('t1.tip_id','t1.tip_desc'));
				$select->where("t1.id_id='{$this->_sesion->lg}'");
			   	$select->order('t1.tip_id');
	   			$data=$select->query()->fetchAll();
				echo $this->_tipoUsuarioObj->json($data);
	    		break;
	    
		endswitch;	
	}
	
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):		
	    	case 'save':
	       		$idiomaTipoUsuario = explode('**',$_POST['data']);
	    	  	$errNum = $this->_idiomaTipoUsuarioObj->saveData($idiomaTipoUsuario);
	    		break;
	    	case 'delete':
	    		$errNum = 	$this->_tipoUsuarioObj->deleteData($_POST['id']);
	    		break;
	    	case 'insert':
	    		
	    		$errNum = $this->_tipoUsuarioObj->insertData($_POST);
	    		break;
	    	
		endswitch;
		//var_dump($errNum); exit;
		$this->_helper->Alert($errNum,'','Registro grabado Correctamente','Registro Existente','Registro con un solo Grupo o Grupos Existentes');		
	}	      
}
