<?php
/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */
require_once 'Zend/Controller/Action.php';

class SeguridadNivelAccesoUsuarioController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	private $_sesion;
	public $_usuarioObj;
	public $_menuObj;
	public $_vmenuObj;
	public $_sistemaObj;
	public $_colegioSucursalObj;
	public $_usuarioMenuObj;
	public $_combos;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');
		$this->_usuarioObj = new DbHtSegUsuario();
		$this->_menuObj = new DbHtSegMenu();
		$this->_vmenuObj = new DbVHtSegMenu();
		$this->_usuarioMenuObj = new DbHtSegUsuarioMenu();
		$this->_combos = new CitCombos();
	} 
   
	public function formAction() 
    {
    	$select = $this->_helper->DBAdapter()->select();    	
    	$this->view->tipousuario=$this->_combos->combo('0002',new DbVHtSegPerfil(),"id_id='{$this->_sesion->lg}'",'per_id','per_desc','0002','per_desc');
    	
    	$this->view->sucursal=$this->_combos->combo($this->_sesion->suid,new DbHtHbSucursal(),"su_id!='00000001'",'su_id','su_nombre',$this->_sesion->suid,'su_nombre');
    	
    	$this->view->tipuse = '0002';
    	$this->view->su_id = $this->_sesion->suid;
    	
    }
    
 	public function permisosAction() 
    {
    	$this->_helper->layout->disableLayout();
    	$select = $this->_helper->DBAdapter()->select();
 
    	$this->view->menus = $this->_vmenuObj->menuComparado($_POST['us_id'],$_POST['su_id']);
    	$this->view->suid = $_POST['su_id'];
    	$this->view->usid = $_POST['us_id'];	
    	$this->view->perid = $_POST['per_id'];	
    	$this->view->peid = $_POST['pe_id'];

    	
    }
    
    public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$select = $this->_helper->DBAdapter()->select();
		$case = $this->getRequest()->getParam('case',0);
	  	switch ($case):
	    	case 'usuario':	    		    		
	    		$select->from(array('t1'=>'ht_seg_usuario'),array('lleva'=>'us_id','us_id','us_login'));
	    		$select->join(array('t2'=>'ht_seg_persona'),'t1.pe_id= t2.pe_id',array('pe_id','pe_nomcomp'));
	    		$select->where("t1.per_id='{$_POST['per_id']}' and (t1.su_id='{$_POST['su_id']}' or t1.su_id is null)");
	    		$select->order('pe_nomcomp');
	    		
	    		//echo $select;exit;
	    		$data = $select->query()->fetchAll();
	    		
	    		$limit = $this->getRequest()->getParam('limit',500);
	  			$start = $this->getRequest()->getParam('start',0);
    			if(!empty($start)){
			  			$pagina =(int)(($start/$limit)+1); 
			  		} 
			  		else
			  			$pagina=1;			  		
			  	$paginator = Zend_Paginator::factory ( $data );
				$paginator->setCurrentPageNumber( $pagina )
				->setItemCountPerPage($limit);				
	  			foreach ($paginator->getCurrentItems() as $value){
			  		$dat[]=$value;
			  	}	
			  	if(!empty($dat)){
			  		$totalpages = $paginator->getPages()->pageCount;
			  		$total = $paginator->getTotalItemCount();
			  		$records = $paginator->getPages()->totalItemCount;			  		
					
					echo json_encode(Array('totalCount' => $total,'matches' =>$dat,'page'=>$pagina,'total'=>$paginator->getTotalItemCount(),'records'=>$records, "success"=>'true'));
				}else{
					echo $this->_usuarioMenuObj->json(array());
				}
	    		//echo $this->_usuarioObj->json($count);			   
	    		break;
	  	endswitch;
    }

	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		switch ($case){
	  		case 'save':
	  			$evalua = $this->_usuarioMenuObj->saveAccess($_POST);
	  			$this->_helper->Alert->direct($evalua,'','Permisos Actualizados correctamente','Existen los registros.','');
	  			break;
		}		
    }
}
