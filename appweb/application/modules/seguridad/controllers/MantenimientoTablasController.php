<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoTablasController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_tbasisObj;
    private $_tablaObj;
    public $_sesion;
    public $_fecOjb;
    public $_citDataObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_tbasisObj = new DbTablaSistema();
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_fecOjb = new Cit_Db_CitFechas();
        $this->_tablaObj = new DbTabla();
        $this->_citDataObj = new CitDataGral();
    }

    public function formAction() {
        //blank
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        $tipo = $this->getRequest()->getParam('tipo', 0);
        switch ($tipo):
            case 'combos':
                switch ($case):
                    case 'tipohab'://Niveles
                        $select = array('ht_tih_id' => 'tih_id', 'tih_desc');
                        $datatipo = $this->_citDataObj->getDataGral('vht_hb_tipo_habitacion', $select, "id_id = '{$this->_sesion->lg}'");
                        $newdata = array();
                        foreach ($datatipo as $value) {
                            $newdata[] = array('ht_tih_id' => $value['ht_tih_id'], 'state' => $value['tih_desc']);
                        }
                        echo $this->_citDataObj->json($newdata, 'matchtih');
                        break;
                endswitch;
                break;

            case 'modulo':
                $select->from(array('t1' => 'vtabla_sistema'), array('lleva' => 'tas_id', 'tas_desc'));
                $select->where("id_id = '{$this->_sesion->lg}'");
                $select->order('t1.tas_orden');
                //echo $select;exit;
                $rowset = $select->query();
                $items = $rowset->fetchAll();
                //var_dump($items); exit;
                echo $this->_tbasisObj->json($items);
                break;

            case 'detalle':
                $codigo = $this->getRequest()->getParam('cod', 0);
                //$lleva = new Cit_Db_Table_Expr('concat', array('t1.tba_id', 't1.tad_id'), '##');
                $select->from(array('t1' => 'vtabla_detalle'), array('lleva' => 'tad_id', 'tad_id', 'tad_desc'));////new Zend_Db_Expr($lleva)
                $select->where('tba_id =?', $codigo);
                $select->where('id_id =?', $this->_sesion->lg);
                $select->order('tad_id');
                $items = $select->query()->fetchAll();
                //var_dump($items); exit;
                echo $this->_tbasisObj->json($items);

                break;

            case 'unico':
                $codigo = $this->getRequest()->getParam('lleva', 0);
                switch ($case):
                    case 'tiposerv'://Tipo de Servicio
                        $select = array('lleva' => 'set_id', 'set_id', 'set_titulo', 'set_desc', 'set_estado');
                        $where = array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'set_id');
                        $items = $this->_citDataObj->getDataGral('vht_ser_tipo', $select, $where);
                        //$select->from(array('t1'=>'ht_hab_tipo_habitacion'),array('lleva'=>'ni_id','ni_id','ni_desc'));
                        //var_dump($items); exit;
                        echo $this->_tbasisObj->json($items);
                        break;
                    case 'tarifa'://Tarifas
                        //$tipoHabitObj = new DbHtHabTipoHabitacion();
                        $select = array('lleva' => 'ta_id', 'ta_id', 'ta_desc');
                        $where = array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'ta_id');
                        $items = $this->_citDataObj->getDataGral('vht_hb_tarifa', $select, $where);
                        //$select->from(array('t1'=>'ht_hab_tipo_habitacion'),array('lleva'=>'ni_id','ni_id','ni_desc'));
                        //var_dump($items); exit;
                        echo $this->_tbasisObj->json($items);
                        break;
                    case 'categoria'://Tarifas
                        //$tipoHabitObj = new DbHtHabTipoHabitacion();
                        $select = array('lleva' => 'cat_id', 'cat_id', 'cat_titulo', 'cat_desc', 'cat_orden');
                        $where = array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'cat_orden');
                        $items = $this->_citDataObj->getDataGral('vht_cms_menu_interno', $select, $where);

                        //$select->from(array('t1'=>'ht_hab_tipo_habitacion'),array('lleva'=>'ni_id','ni_id','ni_desc'));
                        //var_dump($items); exit;
                        echo $this->_tbasisObj->json($items);
                        break;
                    case 'year'://Años
                        //$tipoHabitObj = new DbHtHabTipoHabitacion();
                        $select = array('lleva' => 'an_id', 'an_id', 'an_abr', 'an_desc');
                        $where = array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'an_id');
                        $items = $this->_citDataObj->getDataGral('vht_res_anio', $select, $where);

                        //$select->from(array('t1'=>'ht_hab_tipo_habitacion'),array('lleva'=>'ni_id','ni_id','ni_desc'));
                        //var_dump($items); exit;
                        echo $this->_tbasisObj->json($items);
                        break;
                    case 'tablas':
                        //$lleva = new Cit_Db_Table_Expr('concat',array('t1.tba_id','t1.tad_id'),'**');	
                        $select = array('lleva' => 'tba_id', 'tba_id', 'tba_desc');
                        $where = array('where' => "id_id = '{$this->_sesion->lg}'");
                        $items = $this->_citDataObj->getDataGral('vtabla', $select, $where);
                        //echo $select; exit;
                        //var_dump($items); exit;
                        echo $this->_tbasisObj->json($items);
                        break;

                endswitch;

                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($form):
            case 'save':

                switch ($case):
                    case 'tiposerv':
                        $data = explode('**', $_POST['data']);
                        $evalua = $this->_tablaObj->saveMantenTablas(new DbHtSerTipo(), $data, array('cod' => 'set_id', 'desc' => 'set_titulo', 'abr' => 'set_desc', 'check' => 'set_estado'), new DbHtSerIdiomaTipo());
                        break;
                    case 'tarifa':
                        $data = explode('**', $_POST['data']);
                        $evalua = $this->_tablaObj->saveMantenTablas(new DbHtHbTarifa(), $data, array('cod' => 'ta_id', 'desc' => 'ta_desc'), new DbHtHbIdiomaTarifa());
                        break;
                    case 'tabla':
                        $tabla = $this->getRequest()->getParam('tba', 0);
                        $data = explode('**', $_POST['data']);
                        $evalua = $this->_tablaObj->saveMantenTbaDet($tabla, $data);
                        break;
                    case 'categ':
                        $data = explode('**', $_POST['data']);
                        $evalua = $this->_tablaObj->saveMantenTablas(new DbHtCmsMenuInterno(), $data, array('cod' => 'cat_id', 'desc' => 'cat_titulo', 'abr' => 'cat_desc'), new DbHtCmsIdiomaMenuInterno());
                        break;
                    case 'anio':
                        $data = explode('**', $_POST['data']);
                        $aniObj = new DbHtResAnio();
                        $evalua = $aniObj->saveData($data, new DbHtResIdiomaAnio());
                        break;

                endswitch;
                $this->_helper->Alert->direct($evalua, '', 'Transacción se ha realizado con éxito.', 'Existen los registros.', '');

                break;

            case 'delete':

                switch ($case):
                    case 'tiposerv':
                        $tipHatObj = new DbHtSerTipo();
                        $evalua = $tipHatObj->deleteData($_POST);
                        break;
                    case 'tarifa':
                        $tarifaObj = new DbHtHbTarifa();
                        $evalua = $tarifaObj->deleteData($_POST);
                        break;
                    case 'tablas':
                        $tabla = $this->getRequest()->getParam('tba', 0);
                        list($data['tba_id'], $data['tad_id']) = explode('**', $_POST['id']);
                        $taDetObj = new DbTablaDetalle();
                        $evalua = $taDetObj->deleteData($data);
                        break;

                endswitch;

                $this->_helper->Alert->direct($evalua, '', 'Registro eliminado correctamente', 'Existen los registros.', '');
                break;

            case 'saveContenidoTH':
                $tipHabObj = new DbHtCmsIdiomaMenuInterno();
                $evalua = $tipHabObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', 'Transacción se ha realizado con éxito', 'Existen los registros.', '');
                break;
        endswitch;
    }

}
