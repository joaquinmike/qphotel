<?php
/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */
require_once 'Zend/Controller/Action.php';

class SeguridadNivelAccesoPerfilController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	private $_sesion; public $_citObj;
	public $_usuarioObj;
	public $_menuObj;
	public $_vmenuObj;
	public $_sistemaObj;
	public $_colegioSucursalObj;
	public $_usuarioMenuObj;
	public $_combos;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');
		$this->_usuarioObj = new DbHtSegUsuario();
		$this->_menuObj = new DbHtSegMenu();
		$this->_vmenuObj = new DbVHtSegMenu();
		$this->_usuarioMenuObj = new DbHtSegUsuarioMenu();
		$this->_combos = new CitCombos();
		$this->_citObj = new CitDataGral();
	} 
   
	public function formAction() 
    {
    	$select = $this->_helper->DBAdapter()->select();    	
    	$this->view->tipousuario=$this->_combos->combo('0001',new DbVHtSegPerfil(),null,'per_id','per_desc','0001','per_desc');
    	$this->view->tipuse = '0001';
    	
    }
    
 	public function permisosAction() 
    {
    	$this->_helper->layout->disableLayout();
    	$select = $this->_helper->DBAdapter()->select();
 
    	$this->view->menus = $this->_vmenuObj->menuComparado($_POST['per_id'],null);
    	
    	$this->view->usid = $_POST['per_id'];	

    	
    }
    
    public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$select = $this->_helper->DBAdapter()->select();
		$case = $this->getRequest()->getParam('case',0);
	  	switch ($case):
	    	case 'perfil':
	    		$data = $this->_citObj->getDataGral('vht_seg_perfil', array('lleva'=>'per_id','per_id','per_desc','per_estado'),"id_id = '{$this->_sesion->lg}'");
	    		echo $this->_citObj->json($data);			   
	    		break;
	  	endswitch;
    }

	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		switch ($case){
	  		case 'save':
	  			$evalua = $this->_usuarioMenuObj->saveAccess($_POST);
	  			$this->_helper->Alert->direct($evalua,'','Permisos Actualizados correctamente','Existen los registros.','');
	  			break;
		}		
    }
}
