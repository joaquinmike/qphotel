<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class SeguridadUsuarioPasswordController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;
	public $_usuarioObj;
	function init(){
	
		$this->_usuarioObj = new DbHtSegUsuario();
		$this->_citObj = new CitDataGral();
	    $this->_sesion = new Zend_Session_Namespace('login');
	}
    public function formAction() 
    {

    }
    
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):		
	    	case 'cambiar':
	    		$action = $this->_usuarioObj->saveCambioPassword($_POST,$this->_sesion->usid);
	    		break;
		endswitch;
		//var_dump($errNum); exit;
		$this->_helper->Alert($action,'','Registro grabado Correctamente','La clave actual no es correcta','Registro con un solo Grupo o Grupos Existentes');		
	}	      
}
