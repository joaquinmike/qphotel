<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */

require_once 'Zend/Controller/Action.php';

class SeguridadPermisoOpcionesController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	

	private $_sesion;

	public $_menuObj;
	public $_vmenuObj;
	public $_sistemaObj;
	public $_menuconfObj;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');

		$this->_menuObj = new DbMenu();
		$this->_vmenuObj = new DbVMenu();
		$this->_sistemaObj = new DbSistema();
		$this->_menuconfObj = new DbMenuConfiguracion();
	} 
   
	public function formAction() 
    {  
    	$select = $this->_helper->DBAdapter()->select();
    	$select->from(array('t1'=>'sistema'),array('lleva'=>'si_id','si_id','si_desc'));
    	$select->join(array('t2'=>'sistema_sucursal'),'t1.si_id= t2.si_id',array(''));
    	$select->where("cos_id='{$this->_sesion->cosid}'");
    	$select->distinct();
    	$datos = $select->query()->fetchAll();
    	foreach ($datos as $value){
    		$this->view->$value['si_desc'] = $this->_vmenuObj->menuOption('',$value['si_id']);
    	}    
    	//$this->view->usid = $_POST['us_id'];	
    	$this->view->sistemas = $datos;
    }
    
 	public function permisosAction() 
    {
    	$this->_helper->layout->disableLayout();
    	$select = $this->_helper->DBAdapter()->select();
    	$select->from(array('t1'=>'vmenu'),array('me_id','me_desc','me_link','me_estado','me_orden'));
    	$select->joinleft(array('t2'=>'menu_configuracion'),'t1.si_id= t2.si_id and t1.me_id=t2.me_id and t1.cos_id=t2.cos_id',array('mec_conf'));
    	$select->where("t1.me_id='{$_POST['me_id']}' and t1.si_id='{$_POST['si_id']}' and t1.cos_id='{$_POST['cos_id']}' and t1.id_id='{$this->_sesion->lg}'");
    	$this->view->data = $select->query()->fetchAll();
    	
    	$this->view->me_id = $_POST['me_id'];
    	$this->view->si_id = $_POST['si_id'];
    	$this->view->cos_id = $_POST['cos_id'];
    	
    }
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		switch ($case){
	  		case 'save':	 
	  			$_POST['id_id']=$this->_sesion->lg; 
	  			$_POST['cos_cos_id']=$this->_sesion->cosid; 			
	  			$evalua = $this->_menuconfObj->saveData($_POST);
	  			$this->_helper->Alert->direct($evalua,'','Registros Actualizados correctamente','Existen los registros.','');
	  			break;
		}		
    }
}
