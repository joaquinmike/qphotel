<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */

require_once 'Zend/Controller/Action.php';

class SeguridadPerfilesController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	

	private $_sesion;
	public $_pefil;
	public $_menuObj;
	public $_vmenuObj;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');
		$this->_menuObj = new DbHtSegMenu();
		$this->_vmenuObj = new DbVHtSegMenu();
		$this->_perfil = new DbHtSegPerfil();
	} 
   
	public function formAction() 
    {
    	
    } 	
    public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$select = $this->_helper->DBAdapter()->select();
		$case = $this->getRequest()->getParam('case',0);
	  	switch ($case):
	    	case 'perfil':	    		    		
	    		$select->from(array('t1'=>'vht_seg_perfil'),array('lleva'=>'per_id','per_id','per_desc'));
	    		$select->where("id_id='{$this->_sesion->lg}'");
	    		$count = $select->query()->fetchAll();
	    		echo $this->_perfil->json($count);			   
	    		break;
	  	endswitch; 	
    }

	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		switch ($case){
	  		case 'save':
	  			$_POST['id_id'] = $this->_sesion->lg;
	  			$evalua = $this->_perfil->saveData($_POST);
	  			$this->_helper->Alert->direct($evalua,'','Permisos Actualizados correctamente','Existen los registros.','');
	  			break;
	  		case 'saveSegPerfil':
	  			$_POST['id_id'] = $this->_sesion->lg;
	  			$evalua = $this->_perfil->saveSegPerfil($_POST);
	  			$this->_helper->Alert->direct($evalua,'','Permisos Guardados correctamente','Existen los registros.','');
	  			break;
	  		case 'deleteSegPerfil':
        		$evalua = $this->_perfil->deleteSegPerfil($_POST);
				$this->_helper->Alert->direct($evalua,'','','Existen los registros.','');
        		break;
		}		
    }
}
