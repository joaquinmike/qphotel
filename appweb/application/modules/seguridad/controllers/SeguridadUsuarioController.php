<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class SeguridadUsuarioController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_usuarioObj;
    public $_combos;

    function init() {

        $this->_usuarioObj = new DbHtSegUsuario();
        $this->_ubigeo = new DbHtSegUbigeo();
        $this->_citObj = new CitDataGral();
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_combos = new CitCombos();
    }

    public function formAction() {

        $select = $this->_helper->DBAdapter()->select();
        $select2 = $this->_helper->DBAdapter()->select();
        $arrayTipo = array();
        $select->from(array('vht_seg_perfil'), array('per_id', 'per_desc'))
                ->where("id_id='{$this->_sesion->lg}'")
                ->order('per_desc');
        $tipousuario = $select->query()->fetchAll();
        foreach ($tipousuario as $value) {
            $arrayTipo[$value['per_id']] = $value['per_desc'];
        }

        //$this->view->tipousuario=$this->_combos->combo('0001',new DbHtSegPerfil(),null,'per_id','per_desc','0001','per_desc');
        $select2 = array('per_id', 'per_desc');
        $where2 = array('where' => "id_id = '{$this->_sesion->lg}'");
        $sertipo = $this->_citObj->getDataGral('vht_seg_perfil', $select2, $where2);

        $sertipo2 = $this->_citObj->getDataGral('vtabla_detalle', array('tad_id', 'tad_desc'), "tba_id='022' and id_id='{$this->_sesion->lg}'");
        $this->view->listaPais = $this->_helper->Combos->consulta($sertipo2, array('id' => 'tad_id', 'desc' => 'tad_desc'), 1);

        $arSucursal = $this->_citObj->getDataGral('ht_hb_sucursal', array('su_id', 'su_nombre'), "ho_id='" . htID . "' and su_portal = 'N'");
        $this->view->cboSucursal = $this->_helper->Combos->consulta($arSucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $this->view->tipuse = '0001';

        $this->view->listaTipoUsuario = $this->_helper->Combos($arrayTipo, array('id' => 'per_id', 'desc' => 'per_desc', 'order' => 'per_desc'));
        $this->view->pedepartamento = $this->_helper->Combos($this->_ubigeo, array('id' => 'ub_id', 'mask' => 'ub_id_dep', 'desc' => 'ub_desc', 'where' => "substr(ub_id,3,4)='0000'", 'order' => 'ub_desc'));
        //$this->view->tipousuario  = $this->_helper->Combos->consulta($sertipo, array('id'=>'per_id','desc' => 'per_desc'), 1);
        $this->view->tipousuario = $this->_helper->Combos->html($sertipo, '', array('id' => 'per_id', 'desc' => 'per_desc'));
    }

    public function jsonAction() {
        //Desabilitar la capa
        $this->_helper->layout->disableLayout();
        //Para no Cargar la vista
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $select = $this->_helper->DBAdapter()->select();

        switch ($case):
            case 'lista':

                $lleva = new Cit_Db_Table_Expr('concat', array('t1.us_id', 't3.pe_id', 't1.per_id'), '##');
                $select->from(array('t1' => 'ht_seg_usuario'), array('lleva' => "$lleva", 't1.us_login', 't1.per_id'));
                $select->join(array('t2' => 'vht_seg_perfil'), 't1.per_id = t2.per_id', array('t2.id_id'));
                $select->join(array('t3' => 'ht_seg_persona'), 't3.pe_id = t1.pe_id ', array('t3.pe_paterno', 't3.pe_materno', 't3.pe_nombre'));
                $select->where("t2.id_id='{$this->_sesion->lg}'");
                $select->order('t1.us_login');
                if (!empty($_POST['per_id']) and $_POST['per_id'] != 'T')
                    $select->where("t1.per_id='{$_POST['per_id']}'");

                //echo $select;exit;
                $data = $select->query()->fetchAll();

                $limit = $this->getRequest()->getParam('limit', 500);
                $start = $this->getRequest()->getParam('start', 0);
                if (!empty($start))
                    $pagina = (int) (($start / $limit) + 1);
                else
                    $pagina = 1;
                $paginator = Zend_Paginator::factory($data);
                $paginator->setCurrentPageNumber($pagina)
                        ->setItemCountPerPage($limit);
                foreach ($paginator->getCurrentItems() as $value) {
                    $dat[] = $value;
                }
                if (!empty($dat)) {
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(Array('totalCount' => $total, 'matches' => $dat, 'page' => $pagina, 'total' => $paginator->getTotalItemCount(), 'records' => $records, "success" => 'true'));
                } else {
                    echo $this->_usuarioObj->json(array());
                }
                break;

        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $post = $this->getRequest()->getParams();
        switch ($case):
            case 'save':
                $usuario = explode('**', $post['data']);
                $errNum = $this->_usuarioObj->saveData($usuario);
                break;
            case 'delete':
                //$usuarioObj = new DbUsuario();
                $errNum = $this->_usuarioObj->deleteData($post['id']);
                break;
            case 'insertUsuario':
                //$usuarioObj=new DbUsuario();

                $errNum = $this->_usuarioObj->saveSegUsuario($post);
                break;
            case 'newarid':
                $cursoObj = new DbArea();
                $errNum = $cursoObj->getNewId($post);
                break;
        endswitch;
        //var_dump($errNum); exit;
        $this->_helper->Alert($errNum, '', 'Registro grabado Correctamente', 'Usuario Existente', 'Registro con un solo Grupo o Grupos Existentes');
    }

}
