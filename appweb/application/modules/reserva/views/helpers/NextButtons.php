<?php
/**
 *
 * @author karito
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * FormButtons helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_NextButtons
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    
    private $_formId;
    private $_button;
    /**
     *  
     */
    public function nextButtons ()
    {
        return $this;
    }

    
    public function setId($id)
    {
        $this->_formId = $id;
    }   
    private function back($back=0,$home=0)
    {
    $redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    $action=$redirect->getRequest()->getActionName();
    
        if($action=='form'){
	        if($redirect->getRequest()->getParam('page',0)){
	        	if(!empty($back))
	        		echo '<a href="'.$redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/form/edit/'.$back.'/page/'.$redirect->getRequest()->getParam('page',1).'"><img class="efect_buton" alt="Anterior" src="/images/cms/icons/direction_left.png">&nbsp;</a>'.PHP_EOL;
				else
					echo'<img class="efect_buton"  alt="Anterior" src="/images/cms/icons/direction_left.png">';
	        }else{
	        	if(!empty($back))
	     		  echo '<a class="efect_buton" href="'.$redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/form/edit/'.$back.'/page/'.$redirect->getRequest()->getParam('page',1).'"><img class="efect_buton" alt="Anterior" src="/images/cms/icons/direction_left.png">&nbsp;</a>'.PHP_EOL;
	        	else
	        		echo '<img class="efect_buton" alt="Anterior" src="/images/cms/icons/direction_left.png">';
	        }
        }
     
    }
	private function next($next=0,$end=0)
    {
    $redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    $action=$redirect->getRequest()->getActionName();
  
        if($action=='form'){
	        if($redirect->getRequest()->getParam('page',0)){
	        	if(!empty($next))
	        		echo '<a href="'.$redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/form/edit/'.$next.'/page/'.$redirect->getRequest()->getParam('page',1).'"><img class="efect_buton" alt="Siguiente" src="/images/cms/icons/direction_right.png">&nbsp;</a>'.PHP_EOL;
				else
	        		echo '<img class="efect_buton" alt="Siguiente" src="/images/cms/icons/direction_right.png">';
	        }else{
	        	if(!empty($next))
	       			echo '<a href="'.$redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/form/edit/'.$next.'/page/'.$redirect->getRequest()->getParam('page',1).'"><img class="efect_buton" alt="Siguiente" src="/images/cms/icons/direction_right.png">&nbsp;</a>'.PHP_EOL;
	        	else
	        		echo '<img class="efect_buton"  alt="Siguiente" src="/images/cms/icons/direction_right.png">';
	        }
        }
   
    }  
    public function show ()
    {        
		
    	$redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
		$edit=$redirect->getRequest()->getParam('edit',0);
		$session= new Zend_Session_Namespace('paginator');
		//echo $session->page;
		if($redirect->getRequest()->getActionName()=='form' and !empty($edit) ){
		
		if(!empty($session->pages)){
			$cons=substr($session->pages,0,strpos($session->pages,"LIMIT"));
			$db=Zend_Db::factory(Cit_Init::config()->database);
			$date=$db->fetchAll($cons);
			$id=0;
			$back=0;
			$next=0;
			foreach($date as $indice=>$value){
				foreach($value as $ind=>$val){
					if($val==$edit)	{
						
						if($indice>0)
							$back=$date[$indice-1][$ind];
						if($indice<(count($date)-1))
							$next=$date[$indice+1][$ind];
						//echo $back.'-'.$next;									
					}					
					break;		
				}
				if(!empty($back))
				 break;
			}
				    						
			
				echo '<table style="padding-top:3px;" width="100%" height="15px"><tr><td align="center">';
				$this->back($back,$home=0);
				echo'&nbsp;&nbsp;&nbsp;';	        	
				$this->next($next,$end=0);	
				echo '</td></tr></table>';
			} 
		}   
    }
    
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
