<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * HeadBar2 helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_HeadBar2
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    private $_buttons = array();
    /**
     *  
     */
    public function headBar2 ()
    {
       return $this;
       
    }

    public function button ($button)
    {
        $this->_buttons[] = $button; 
    }
    
    public function show ()
    {
        $this->placeholder('bar2')->captureStart();
        foreach ($this->_buttons as $value) {
            echo $value;
        }
        $this->placeholder('bar2')->captureEnd();
    }
    
    
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
