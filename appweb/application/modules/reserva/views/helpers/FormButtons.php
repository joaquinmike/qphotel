<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * FormButtons helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_FormButtons
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    
    private $_formId;
    private $_button;
    private $_icons;
    private $_configura;
    /**
     *  
     */
    public function formButtons ()
    {
        return $this;
    }

    
    public function setId($id)
    {
        $this->_formId = $id;
        
    }
    public function setIcons($data = array()){
    	
    	$this->_icons=$data;
    }
 	public function setConfigura($data = array()){
    	$this->_configura = $data;
    	
    }
    private function submit ()
    {
    	$redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    	$action=$redirect->getRequest()->getActionName();
        if($action=='form'){
	    	echo '<img style = "cursor:pointer" onclick="save();" class="efect_buton" alt="Guardar Cambios" title="Guardar Cambios" src="/images/cms/icons/diskette.png"  />';
	        echo '<script>
	        function save(){
	        $(document).ready(function() {
				// SUCCESS AJAX CALL, replace "success: false," by:     success : function() { callSuccessFunction() }, 
				
				$("#'.$this->_formId.'").validationEngine()
				
				
				//$.validationEngine.loadValidation("#date")
				//alert($("#formID").validationEngine({returnIsValid:true}))
				//$.validationEngine.buildPrompt("#date","This is an example","error")	 		 // Exterior prompt build example								 // input prompt close example
				//$.validationEngine.closePrompt(".formError",true) 							// CLOSE ALL OPEN PROMPTS
			});
			$("#'.$this->_formId.'").submit();
	        }
	        </script>';
        }       
    }
    
    private function reset()
    {
       echo '<img class="efect_buton" alt="Cancelar" src="/images/cms/icons/document_delete.png">';
	}
    
    private function back()
    {
    $redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    $action=$redirect->getRequest()->getActionName();
        if($action=='form'){
	        if($redirect->getRequest()->getParam('page',0)){
	        	$dir = $redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/list/page/'.$redirect->getRequest()->getParam('page',1);
	        }else{
	      	 	$dir = $redirect->getFrontController()->getBaseUrl().'/'.$redirect->getRequest()->getModuleName().'/'.$redirect->getRequest()->getControllerName().'/list/page/'.$redirect->getRequest()->getParam('page',1);
	        }
	        echo '<img class="efect_buton" onClick="redirect('."'$dir'".')" style = "cursor:pointer"  alt="Pagina Anterior" src="/images/cms/icons/direction_left.png" />';
       	}      
    }    
     private function news()
     {
     	$redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    	$controllername=$redirect->getRequest()->getControllerName();
     	echo '<img class="efect_buton" style = "cursor:pointer" onClick="news('."'/pensiones/$controllername/form'".')" class="barra" title="Agregar" alt="Nuevo" src="/images/cms/icons/file_add.png">'; 	
		
     }
	private function printer()
     {
     	$redirect = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
    	$controllername=$redirect->getRequest()->getControllerName();
     	echo '<img style = "cursor:pointer;margin-right:5px" onClick="print()" class="barra" title="Agregar" alt="Nuevo" src="/images/cms/icons/print.gif">'; 	
		
     }
     private function icons(array $data = array()){
     	$code='';
     	
     	foreach($data  as $indice=>$value):
     		$code.='<img ';
     		foreach($value as $indicetowo=>$valuetwo):
     			$code.=$indicetowo.'="'.$valuetwo.'" ';     		
     		endforeach; 
     		$code.='/>';    		
     	endforeach;
     	echo $code;
     }
     
	private function configura(array $data = array()){
     	
		foreach($data  as $indice=>$value):
     			
     		if($indice == 'back' and !empty($value)){     			
     			$this->back();
     		}
     		if($indice == 'add' and !empty($value)){     			
     			$this->submit();
     		}
			if($indice == 'reset' and !empty($value)){     			
     			$this->reset();
     		} 
     		if($indice == 'news' and !empty($value)){     			
     			$this->news();
     		}  	
     		if($indice == 'print' and !empty($value)){     			
     			$this->printer();
     		}  		
     	endforeach;
     }
     
    public function show() 
    {        
 	  
		if(!empty($this->_icons)){
			$this->icons($this->_icons);	
		}
    	if(!empty($this->_configura)){
			$this->configura($this->_configura);	
		}
    	/*$this->news();
    	$this->back();
        $this->reset();
        $this->submit();*/
        
       
    }
    
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
