<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaConsultaController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
    }

    public function formAction() {
        //Sucursal

        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'anulaReserva':
                $res_id = $this->getRequest()->getParam('res_id', 0);
                $action = $this->_reservaObj->anulaReserva($res_id);
                $this->_helper->Alert->direct($action, '', 'La Reserva se anulo Correctamente');
                break;
        endswitch;
    }
    
    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        $post = $this->getRequest()->getParams();
        switch ($case):
            case 'listaReservas':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);

                if (!empty($start))
                    $pag = (int) (($start / $limit) + 1);
                else
                    $pag = 1;

                $select->from(array('t1' => 'ht_res_reserva'), array('t1.res_id', 't1.cl_id', 'res_fecha', 'res_total', 'res_promo','res_estado','res_codigo'))
                    ->join(array('t2' => 'ht_hb_sucursal'), 't1.su_id = t2.su_id', array('su_nombre'))
                    ->join(array('t3' => 'ht_seg_cliente'), 't1.cl_id = t3.cl_id', array(''))
                    ->join(array('t4' => 'ht_seg_persona'), 't3.pe_id = t4.pe_id', array('pe_nomcomp'))
                    ->join(array('t5' => 'ht_cms_idioma'), 't1.id_id = t5.id_id', array('id_desc'));
                    //->where('res_estado = ?','1');

                if (!empty($post['su_id']))
                    $select->where('t1.su_id =?',$post['su_id']);

                if (!empty($post['fec_ini']) and !empty($post['fec_fin'])):
                    $this->_fecObj->setData($post['fec_ini']);
                    $fecini = $this->_fecObj->renders('save');

                    $this->_fecObj->setData($post['fec_fin']);
                    $fecfin = $this->_fecObj->renders('save');

                    $select->where("res_fecha BETWEEN '$fecini' and '$fecfin'");
                endif;

                if (!empty($post['query'])):
                    $nombre = strtoupper($post['query']);
                    $select->where("upper(pe_nomcomp) like '%{$nombre}%' or t1.res_id like '%{$post['query']}%'");
                endif;

                $select->order(array('res_fecha desc', 'pe_nomcomp'));
                $paginator = Zend_Paginator::factory($select);
                $paginator->setCurrentPageNumber($pag)->setItemCountPerPage($limit);

                foreach ($paginator->getCurrentItems() as $value) {
                    $this->_fecObj->setData($value['res_fecha']);
                    $value['res_fecha'] = $this->_fecObj->renders('open');
                    $items[] = $value;
                }

                if (!empty($items)):
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(array('matches' => $items, 'page' => $pag, 'total' => $paginator->getTotalItemCount(), 'records' => $records, 'success' => 'true'));
                else:
                    echo $this->_citObj->json(array());
                endif;
                //echo $this->_citObj->json($dtaLista);
                break;

            case 'verDetalleReserva':
                if (!empty($post))
                    $dtaLista = $this->_reservaObj->getReservasDetalle($post['res_id'],$this->_sesion->lg);
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;
        endswitch;
    }

}
