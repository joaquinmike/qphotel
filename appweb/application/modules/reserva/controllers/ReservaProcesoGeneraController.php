<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaProcesoGeneraController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    private $_reservaObj;
    private $_fecObj;
    private $_estado;
    private $_kardexObj;
    private $_meses;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
        $this->_habitacionObj = new DbHtHbHabitacion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_estado = array('0' => 'Inactivo', '1' => 'Activo', 'R' => 'Reservados');
        $this->_meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre',
            '12' => 'Diciembre');
    }

    public function formAction() {

        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;

        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
        /* $meses = $this->_citObj->returnMesesActivos($this->_meses);
          $this->view->meses  = $this->_helper->Combos($meses,array('id'=>'mes_id'));
          $this->view->mesfin  = $this->_helper->Combos($meses,array('id'=>'mes_fin')); */
        $this->view->mesid = date('m');

        $array[$this->_sesion->anio] = $this->_sesion->anio;
        $anio = $this->_sesion->anio;
        for ($i = 0; $i < 2; $i++):
            $anio = (int) $anio + 1;
            $array[$anio] = $anio;
        endfor;

        $this->view->cboanio = $this->_helper->Combos($array, array('id' => 'an_id'));
        $this->view->anio = $this->_sesion->anio;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'iniMes':
                if (empty($_POST['an_id']) or $_POST['an_id'] == $this->_sesion->anio)
                    $meses = $this->_citObj->returnMesesActivos($this->_meses);
                else
                    $meses = $this->_meses;


                echo $this->_citObj->json($this->_citObj->returnArrayCombo($meses, array('id' => 'mes_id')));

                break;
            case 'finMes':
                if (empty($_POST['an_id']) or $_POST['an_id'] == $this->_sesion->anio)
                    $meses = $this->_citObj->returnMesesActivos($this->_meses);
                else
                    $meses = $this->_meses;

                echo $this->_citObj->json($this->_citObj->returnArrayCombo($meses, array('id' => 'mes_fin')));
                break;
            case 'categoria':

                $dtaHabit = $this->_kardexObj->getTipoHabSucursal($_POST, 'T');
                echo $this->_citObj->json($dtaHabit);
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        set_time_limit(0);
        switch ($case):
            case 'saveGenera':
                //var_dump($_POST);exit;
                $action = $this->_kardexObj->generaCalendarioKardex($_POST);
                $this->_helper->Alert->direct($action['id'], '', '', @$action['desc']);
                break;
        endswitch;
    }

}
