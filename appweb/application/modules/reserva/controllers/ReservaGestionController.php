<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaGestionController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_fechas;
    public $_kardexObj;
    public $_tarifaObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fechas = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
        $this->_habitacionObj = new DbHtHbHabitacion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_tarifaObj = new DbVHtHbTarifa();
    }

    public function formAction() {
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;

        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal <> 'S'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Novienbre',
            '12' => 'Diciembre');

        $this->view->meses = $this->_helper->Combos($meses, array('id' => 'mes_id'));

        $array[$this->_sesion->anio] = $this->_sesion->anio;
        $anio = $this->_sesion->anio;
        for ($i = 0; $i < 2; $i++):
            $anio = (int) $anio + 1;
            $array[$anio] = $anio;
        endfor;

        $this->view->cboanio = $this->_helper->Combos($array, array('id' => 'an_id'));
        $this->view->anio = $this->_sesion->anio;
    }

    public function renderAction() {
        if (empty($_POST['an_id']))
            $year = date('Y');
        else
            $year = $_POST['an_id'];

        $this->_helper->layout->disableLayout();
        $dias = $this->_fechas->getMonthDays($_POST['mes_id'], $year);
        $fieldgrid = '';
        $fiel = '';
        for ($i = 1; $i <= $dias; $i++) {
            $namedia = $this->_fechas->nameDate("$i/{$_POST['mes_id']}/{$year}");
            $str = '';
            if ($namedia == 'viernes' or $namedia == 'sábado') {
                $str = ",css:'background-color: #D0DEF0;'";
            }
            $fiel.=",{ name : 'd$i'}";
            $fieldgrid.=",{dataIndex: 'd$i', header: '$i $namedia', width: 90,align:'center' $str}";
        }

        $this->view->field = $fiel;
        $this->view->fieldgrid = $fieldgrid;
    }

    public function render1Action() {
        $year = date('Y');
        $this->_helper->layout->disableLayout();
        $dias = $this->_fechas->getMonthDays($_POST['mes_id'], $year);

        //$this->view->habitaciones = $this->_habitacionObj->getDatosHabit(array('where'=>"t2.tih_id='{$_POST['tih_id']}' and t4.su_id='{$_POST['su_id']}' and t2.id_id='{$this->_sesion->lg}'"));

        $day = array('domingo' => 0, 'lunes' => 1, 'martes' => 2, 'miércoles' => 3, 'jueves' => 4, 'viernes' => 5, 'sábado' => 6);
        $this->view->diasd = $day;
        $raydia = array();
        $demo = array();
        //$compare = $this->_kardexObj->getDatosHabit(array('where'=>"t2.tih_id='{$_POST['tih_id']}' and t2.su_id='{$_POST['su_id']}' and Extract(month from t2.ka_fecha)='{$_POST['mes_id']}' and  Extract(year from t2.ka_fecha) ='$year'"));
        //$this->view->compare=$compare;
        for ($i = 1; $i <= $dias; $i++) {
            $hab = $this->_kardexObj->getDatosHabit(array('where' => "t2.tih_id='{$_POST['tih_id']}' and t2.su_id='{$_POST['su_id']}' and t2.ka_fecha='$i/{$_POST['mes_id']}/{$year}'"));
            //var_dump($hab);exit;
            $raydia[] = array('num' => $i, 'desc' => $this->_fechas->nameDate("$i/{$_POST['mes_id']}/{$year}"), 'hab' => $hab);
        }
        for ($i = 0; $i < $day[$raydia[0]['desc']]; $i++) {
            $demo[] = array('num' => '', 'desc' => '');
        }
        foreach ($raydia as $value) {
            $demo[] = $value;
        }
        //var_dump($demo);exit;
        $this->view->dias = $demo;
    }

    public function jsonAction() {
        $fecObj = new Cit_Db_CitFechas();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaReservas':

                $dtaLista = $this->_reservaObj->getListaReservas('', array('t1.ac_desc'));
                echo $this->_citObj->json($dtaLista);
                break;

            case 'dias':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);
                $datatar = $this->_tarifaObj->fetchAll("id_id='{$this->_sesion->lg}'")->toArray();
                $year = date('Y');
                $dias = $this->_fechas->getMonthDays($_POST['mes_id'], $year);
                $data = array();
                $sit = array();
                for ($i = 1; $i <= $dias; $i++) {
                    $dia = str_pad($i, 2, '0', STR_PAD_LEFT);
                    $fecha = $dia . '/' . $_POST['mes_id'] . '/' . $year;
                    $fecObj->setData($fecha);
                    $fecha = $fecObj->renders('save');
                    //var_dump($fecha);exit;
                    $dataw = $this->_kardexObj->getDatosHabit(array('where' => "t2.ka_situacion!='R' and t2.tih_id='{$_POST['tih_id']}' and t2.su_id='{$_POST['su_id']}' and t2.ka_fecha='{$fecha}'"));

                    foreach ($dataw as $indiceone => $valueone) {
                        //$sit['d'.$i] =$valueone['ka_situacion']; 		    			
                        if ($valueone['ka_situacion'] == '1') {
                            $chk = ' checked="checked" ';
                        } else {
                            $chk = ' ';
                        }
                        $tarifa = $this->_helper->Combos->html($datatar, '', array('id' => 'ta_id', 'desc' => 'ta_desc'), $valueone['ta_id']);
                        $data[$valueone['hb_id']]['tih_id'] = $valueone['tih_id'];
                        $data[$valueone['hb_id']]['su_id'] = $valueone['su_id'];
                        $data[$valueone['hb_id']]['ka_fecha'] = $valueone['ka_fecha'];
                        $data[$valueone['hb_id']]['hb_id'] = $valueone['hb_id'];
                        $data[$valueone['hb_id']]['hb_numero'] = $valueone['hb_numero'];
                        $data[$valueone['hb_id']]['hb_desc'] = $valueone['hb_desc'];
                        $data[$valueone['hb_id']]['d' . $i] = '<p style="height:35px;"><input type="checkbox" id="b' . $valueone['hb_numero'] . 'd' . $i . '" name="ka_situacion[' . $valueone['hb_id'] . '**' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $valueone['ka_fecha'] . ']" value="' . $valueone['ka_situacion'] . '" ' . $chk . ' />' . '<br><select name="ta_id[' . $valueone['hb_id'] . '**' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $valueone['ka_fecha'] . ']">' . $tarifa . '</select></p>';
                    }
                    //var_dump($data);exit;	
                }


                if (!empty($start)) {
                    $pagina = (int) (($start / $limit) + 1);
                }
                else
                    $pagina = 1;
                $paginator = Zend_Paginator::factory($data);
                $paginator->setCurrentPageNumber($pagina)
                        ->setItemCountPerPage($limit);
                foreach ($paginator->getCurrentItems() as $value) {
                    $dat[] = $value;
                }
                if (!empty($dat)) {
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(Array('totalCount' => $total, 'matches' => $dat, 'page' => $pagina, 'total' => $paginator->getTotalItemCount(), 'records' => $records, "success" => 'true'));
                } else {
                    echo $this->_reservaObj->json(array());
                }

                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'save':
                //echo urldecode($_POST['data']);
                //var_dump($_POST);exit;
                $evalua = $this->_kardexObj->saveKardex($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'saveGestionMes':
                $evalua = $this->_kardexObj->saveGeneraMes($_POST);
                $this->_helper->Alert->direct($evalua, '', 'Mes generado correctamente', 'El mes a generar no afecta a las reservas(mes antiguo).');
                break;
        endswitch;
    }

}
