<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class ReservaRoomAvailController extends Zend_Controller_Action 
{
    public $_sesion;
    public $_reservaObj;
    public $_kardexObj;
    public $_kardexCitObj;
    public $_array;
    public $_objSucursal;
    public $_days = array();

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_reservaObj = new DbHtResReserva();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_kardexCitObj = new CitHtResKardex();
        $this->_kardexObj = new DbHtResKardex();
        $this->_objSucursal = new DbHtHbSucursal();
        
    }
    
    public function formAction(){
        
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = 'and su_id = '. "'{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;
        $this->view->fecha = date('d/m/Y');
        $sucursal = $this->_objSucursal->fetchAll("su_portal ='N'" . $admin)->toArray();
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
    }
    
    public function renderAction() {
        $this->_helper->layout->disableLayout();
        $post = $this->getRequest()->getParams();
        if (empty($post['mes_id']))
            $post['mes_id'] = date('m');
        if (empty($post['an_id']))
            $post['an_id'] = date('Y');
        $dtaGrid = $this->_kardexObj->getRoomAvail($post, date('d'), 1);
        $grid = $this->_kardexCitObj->formKardexInventario($dtaGrid['datos'], $dtaGrid['mes'], $dtaGrid['avail']);
        
        $this->view->form = $grid;

        $this->view->data = $post;
    }
    
    public function jsonAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'lista':
                $post = $this->getRequest()->getParams();
                //$dtaGrid = $this->_kardexObj->getArrayInventario($_POST);
                //var_dump($dtaGrid); exit;
                $dtaLista = $this->_kardexObj->getTipoHabSucursal($post);
                $data = $this->getJsonRoom($dtaLista);
                //var_dump($data); exit;
                echo $this->_kardexObj->json($data);
                break;
            case 'tarifa':
                $objTarifa = new DbVHtHbTarifa();
                $dtatarifa = $objTarifa->fetchAll(array('id_id =?' => $this->_sesion->lg))->toArray();
                echo $this->_kardexObj->json($dtatarifa);
                break;
        endswitch;
    }
    
    public function ajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'roomAvail':
                $post = $this->getRequest()->getParams();
                $action = $this->_kardexObj->saveCambioTarifa($post);
                $this->_helper->Alert->direct($action, '', 'El cambio de tarifa se realizo satisfactoriamente.', '');
                break;
        endswitch;
    }
    
    public function getJsonRoom($data){
        foreach ($data as $item => $value):
            for ($i = 1; $i<=7; $i++):
                $data[$item]['day'.$i] = true;
            endfor;
        endforeach;
        return $data;
    }
}