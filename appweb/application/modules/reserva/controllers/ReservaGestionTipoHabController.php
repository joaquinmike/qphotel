<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaGestionTipoHabController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_fecObj;
    public $_kardexObj;
    public $_tarifaObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
        $this->_habitacionObj = new DbHtHbHabitacion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_tarifaObj = new DbVHtHbTarifa();
    }

    public function formAction() {
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        //$this->view->suid = $this->_sesion->suid;
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Novienbre',
            '12' => 'Diciembre');

        $meses = $this->_citObj->returnMesesActivos($meses);
        $this->view->meses = $this->_helper->Combos($meses, array('id' => 'mes_id'));
        $this->view->mesid = date('m');
    }

    public function renderAction() {
        $year = date('Y');
        $this->_helper->layout->disableLayout();
        $dias = $this->_fecObj->getMonthDays($_POST['mes_id'], $year);
        $fieldgrid = '';
        $fiel = '';
        for ($i = 1; $i <= $dias; $i++) {
            $namedia = $this->_fecObj->nameDate("$i/{$_POST['mes_id']}/{$year}");
            $str = '';
            if ($namedia == 'viernes' or $namedia == 'sábado') {
                $str = ",css:'background-color: #D0DEF0;height:44px;'";
            }
            $fiel.=",{ name : 'd$i'}";
            $fieldgrid.=",{dataIndex: 'd$i', header: '$i $namedia', width: 90,align:'center' $str}";
        }
        $this->view->field = $fiel;
        $this->view->fieldgrid = $fieldgrid;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaReservas':
                $dtaLista = $this->_reservaObj->getListaReservas('', array('t1.ac_desc'));
                echo $this->_citObj->json($dtaLista);
                break;

            case 'dias':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);
                $datatar = $this->_tarifaObj->fetchAll("id_id='{$this->_sesion->lg}'")->toArray();
                //var_dump($tarifa);exit;
                $year = date('Y');
                $dias = $this->_fecObj->getMonthDays($_POST['mes_id'], $year);
                $data = array();
                $sit = array();
                //Corregido-------------------------------- -+-
                $dtatipHab = $this->_kardexObj->getGestionTipoHabKardex(array('where' => "t1.su_id = '{$_POST['su_id']}'", 'order' => 't1.tih_id'));
                foreach ($dtatipHab as $indiceone => $valueone):
                    for ($i = 1; $i <= $dias; $i++):
                        $dia = str_pad($i, 2, '0', STR_PAD_LEFT);

                        $ka_fecha = $dia . '/' . $_POST['mes_id'] . '/' . $year;
                        $this->_fecObj->setFecha($ka_fecha);
                        $this->_fecObj->setData($ka_fecha);
                        $ka_fecha = $this->_fecObj->renders('save');

                        $sel = $this->_kardexObj->select()->limit(1)->where("su_id='{$_POST['su_id']}' and ka_fecha='$ka_fecha' and tih_id='{$valueone['tih_id']}'");
                        $datati = $this->_kardexObj->fetchAll($sel)->toArray();
                        if (!empty($datati)) {
                            $dataka = $this->_kardexObj->getDatosHabit(array('where' => "t2.ka_situacion != 'R'  and t2.su_id='{$_POST['su_id']}' and t2.ka_fecha='$ka_fecha' and t2.tih_id='{$valueone['tih_id']}'", 'order' => 't3.tih_id'));
                            $conmas = 0;
                            $commenos = 0;
                            foreach ($dataka as $valuetwo) {
                                if ($valuetwo['ka_situacion'] == 1)
                                    $commenos--;
                                if ($valuetwo['ka_situacion'] == 0)
                                    $conmas++;
                            }

                            //echo $commenos.'--'.$conmas;exit;
                            $tarifa = $this->_helper->Combos->html($datatar, '', array('id' => 'ta_id', 'desc' => 'ta_id'), $datati[0]['ta_id']);
                            $save = $this->_citObj->getDataGral('ht_hb_habitacion', array('stock' => new Zend_Db_Expr('count(*)')), "su_id = '{$_POST['su_id']}' and tih_id = '{$valueone['tih_id']}'", 'U');
                            $data[$valueone['tih_id']]['tih_stock'] = $save['stock'];
                            $data[$valueone['tih_id']]['tih_desc'] = $valueone['tih_desc'];
                            $data[$valueone['tih_id']]['tih_id'] = $valueone['tih_id'];
                            $data[$valueone['tih_id']]['cn'] = $valueone['cn'];

                            $selecting = '<select name="ka_situacion[' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $ka_fecha . ']">';
                            for ($a = $commenos; $a <= $conmas; $a++) {
                                $se = '';
                                if ($a == 0)
                                    $se = ' selected="selected" ';
                                $selecting .='<option ' . $se . ' value="' . $a . '">' . $a . '</option>';
                            }
                            $selecting .='</select>';
                            //$selecting = '<select name="ta_id['.$valueone['tih_id'].'**'.$valueone['su_id'].'**'.$ka_fecha.']">'.$tarifa.'</select>';
                            $data[$valueone['tih_id']]['d' . $i] = '<p style="height:35px;">' . $selecting . '<br><select name="ta_id[' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $ka_fecha . ']">' . $tarifa . '</select></p>';
                        }

                    endfor;
                endforeach;


                /* if(!empty($start)){
                  $pagina =(int)(($start/$limit)+1);
                  }
                  else
                  $pagina=1;
                  $paginator = Zend_Paginator::factory ( $data );
                  $paginator->setCurrentPageNumber( $pagina )
                  ->setItemCountPerPage($limit); */
                foreach ($data as $value):
                    $dat[] = $value;
                endforeach;
                if (!empty($data)) {
                    //$totalpages = $paginator->getPages()->pageCount;
                    //$total = $paginator->getTotalItemCount();
                    //$records = $paginator->getPages()->totalItemCount;			  		
                    //'total'=>$paginator->getTotalItemCount(),'records'=>$records, "success"=>'true','page'=>$pagina
                    echo json_encode(Array('totalCount' => count($data), 'matches' => $dat));
                } else {
                    echo $this->_reservaObj->json(array());
                }

                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'save':
                //echo urldecode($_POST['data']);
                //var_dump($_POST);exit;
                $evalua = $this->_kardexObj->saveKardexTipo($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'saveGestionMes':
                $evalua = $this->_kardexObj->saveGeneraMes($_POST);
                $this->_helper->Alert->direct($evalua, '', 'Mes generado correctamente', 'El mes a generar no afecta a las reservas(mes antiguo).');
                break;
        endswitch;
    }

}
