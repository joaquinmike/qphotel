<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_sesion;
	public $_combos;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');
		$this->_combos = new CitCombos();
	}
    public function indexAction() 
    {
		$this->view->login = $this->_sesion->uslogin;	
		$this->view->tipdesc = $this->_sesion->tipdesc;
		$this->view->penomcomp = $this->_sesion->penomcomp;		
		
		$select = $this->_helper->DBAdapter()->select();		
		$select->from(array('t1'=>'usuario_menu'),array('si_id','us_id'));
		$select->join(array('t2'=>'sistema'),'t1.si_id=t2.si_id',array('si_desc','si_image2'));
	    $select->where("t1.us_id = '{$this->_sesion->usid}'");
	    $select->distinct();
	    $this->view->data = $select->query()->fetchAll();
	    //var_dump($this->view->data);exit;
	    $this->view->module = $this->getRequest()->getModuleName();
	    
	    $this->view->cosnombre = $this->_sesion->cosnombre;
	    $this->view->imgimg1 = $this->_sesion->imgimg1;
	    
	    $this->view->anio=$this->_combos->combo($this->_sesion->anio,new DbAnio(),null,'an_id','an_id',$this->_sesion->anio,'an_id desc');
	    
	    //echo $this->view->module;exit;
    }
	public function loadAction() 
    {
    	
		$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	echo '{"events":[],"contents":{},"current":{"type":"html","data":"Hola Cit"
		,"actions":null,"panel":"overview-panel","notbar":false,"preventClose":false,
		"replace":false,"url":""},
		"errorCode":0,"errorMessage":"","notbar":false,"preventClose":false,"replace":false}';	   	
    }
    public function listtagAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	echo '{"events":[],"contents":{},"current":false,"errorCode":0,
		"errorMessage":"","notbar":false,"preventClose":false,"replace":false,"tags":[]}';
    }
    
    public function ajaxAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	$case = $this->getRequest()->getParam('case',0);
    	
    	$bonusObj = new DbHtResBonos();
    	switch ($case):
    		case 'savePuntosDolar':
    			$evalua = $bonusObj->saveData($_POST);
    			$this->_helper->Alert->direct($evalua,'','Registros agregados correctamente','Registros modificados correctamente.','');
    			break;
    	endswitch;
    	
    }
    
	public function jsonAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	$case = $this->getRequest()->getParam('case',0);
    	$citObj = new CitDataGral();
    	$bonusObj = new DbHtResBonos();
    	switch ($case):
    		case 'datosPuntos':
    			$data = $citObj->getDataGral('ht_res_bonos', array('hbo_dolar'=>'bo_dolar','hbo_puntos'=>'bo_puntos'));
    			echo $citObj->json($data);
    			break;
    	endswitch;
    	
    }
      
}
