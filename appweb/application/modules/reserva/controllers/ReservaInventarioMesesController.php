<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaInventarioMesesController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_kardexObj;
    public $_kardexCitObj;
    public $_array;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_reservaObj = new DbHtResReserva();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_kardexCitObj = new CitHtResKardex();
        $this->_kardexObj = new DbHtResKardex();
    }

    public function formAction() {
        //Sucursal
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 'T');
        $this->view->mes_id = date('m');

        //array_merge($array1)
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'lista':
                $dtaGrid = $this->_kardexObj->getArrayInventario($_POST);
                $dtaLista = $this->_kardexObj->getInventario3Meses($dtaGrid['datos'], $dtaGrid['mes'], $_POST);
                echo $this->_citObj->json($dtaLista);
                break;
        endswitch;
    }

    public function renderAction() {
        $this->_helper->layout->disableLayout();

        if (empty($_POST['mes_id']))
            $_POST['mes_id'] = date('m');

        $dtaGrid = $this->_kardexObj->getArrayInventario($_POST);
        //var_dump($dtaGrid['mes']); exit;
        $grid = $this->_kardexCitObj->formKardexInventario($dtaGrid['datos'], $dtaGrid['mes']);

        $this->view->form = $grid;

        $this->view->data = $_POST;
    }

}
