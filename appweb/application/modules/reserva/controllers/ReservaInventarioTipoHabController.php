<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ReservaInventarioTipoHabController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_fecObj;
    public $_estado;
    public $_kardexObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
        $this->_habitacionObj = new DbHtHbHabitacion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_estado = array('0' => 'Inactivo', '1' => 'Activo', 'R' => 'Reservados');
    }

    public function formAction() {
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal <> 'S'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Novienbre',
            '12' => 'Diciembre');

        $this->view->meses = $this->_helper->Combos($meses, array('id' => 'mes_id'));
        $this->view->mesid = date('m');
        $this->view->anio = $this->_sesion->anio;
    }

    public function renderAction() {
        set_time_limit(0);
        $post = $this->getRequest()->getParams();
        if (empty($post['an_id']))
            $year = date('Y');
        else
            $year = $_POST['an_id'];
        $this->_helper->layout->disableLayout();
        $dias = $this->_fecObj->getMonthDays($post['mes_id'], $year);
        $this->view->estado = $this->_estado;
        //$this->view->habitaciones = $this->_habitacionObj->getDatosHabit(array('where'=>"t2.tih_id='{$_POST['tih_id']}' and t4.su_id='{$_POST['su_id']}' and t2.id_id='{$this->_sesion->lg}'"));

        $day = array('domingo' => 0, 'lunes' => 1, 'martes' => 2, 'miercoles' => 3, 'jueves' => 4, 'viernes' => 5, 'sábado' => 6);
        $this->view->diasd = $day;
//        $raydia = array();
        $demo = array();
        //$compare = $this->_kardexObj->getDatosHabit(array('where'=>"t2.tih_id='{$_POST['tih_id']}' and t2.su_id='{$_POST['su_id']}' and Extract(month from t2.ka_fecha)='{$_POST['mes_id']}' and  Extract(year from t2.ka_fecha) ='$year'"));
        //$this->view->compare=$compare;
        $ini = $year . '-' . $post['mes_id'] . '-' . '01';
        $fin = $year . '-' . $post['mes_id'] . '-' . $dias;
        
        $data = $this->_kardexObj->getInventaryRooms($post,$ini,$fin,$year,$this->_sesion->lg);
        $this->_fecObj->setData($data[0]['ka_fecha']);
        $fecha = $this->_fecObj->renders('open');
        $i = 0;
        $arrayGral = array(); $subarray = array();
        $grupo = $data[0]['ka_fecha'];
        $arrayGral[$i]['ta_id'] = $data[0]['tad_id'];
        $arrayGral[$i]['ka_fecha'] = $fecha;
        $arrayGral[$i]['desc'] = $this->_fecObj->nameDate($fecha);
        $arrayGral[$i]['num'] = $i + 1 ;
        
        foreach ($data as $value) :
            //Agrupando ----------------- -+-
            if($grupo == $value['ka_fecha']){
                
            }else{
                $this->_fecObj->setData($value['ka_fecha']);
                $fecha = $this->_fecObj->renders('open');
                
                $arrayGral[$i]['data'] = $subarray;
                $grupo = $value['ka_fecha'];
                $subarray  = array(); $i++;
                $arrayGral[$i]['ta_id'] = $value['tad_id'];
                $arrayGral[$i]['fecha'] = $fecha;
                $arrayGral[$i]['desc'] = $this->_fecObj->nameDate($fecha);
                $arrayGral[$i]['cantidad'] = 0;
                $arrayGral[$i]['num'] = $i + 1;
            }
            
            if(!empty($value['promo'])){
                $value['hb_precio'] = $value['promo'];
                $value['promo'] = 1;
            }
            unset($value['tad_desc']);
            unset($value['tad_id']);
            $subdata = $this->_kardexObj->getInventarioContRooms($post['su_id'],$value['tih_id'],$value['ka_fecha']);
            unset($value['ka_fecha']);
            $subarray[] = array_merge($value,$subdata);
        endforeach;
        
        $arrayGral[$i]['data'] = $subarray;
            
//        for ($i = 1; $i <= $dias; $i++) {
//            $dia = str_pad($i, 2, '0', STR_PAD_LEFT);
//            $fecha = $dia . '/' . $post['mes_id'] . '/' . date('Y');
//            //$this->_fecObj->setFecha($fecha);
//            $this->_fecObj->setData($fecha);
//            $post['fecha'] = $this->_fecObj->renders('save');
//            $cantidad = $this->_kardexObj->getInventarioTipohab($post);
//            //var_dump($cantidad);exit;
//            $raydia[] = array('num' => $i, 'desc' => $this->_fecObj->nameDate("$i/{$post['mes_id']}/{$year}"), 'fecha' => $dia . '/' . $post['mes_id'] . '/' . $year, 'count' => $cantidad);
//        }
        for ($i = 0; $i < $day[$arrayGral[0]['desc']]; $i++) {
            $demo[] = array('num' => '', 'desc' => '');
        }
        foreach ($arrayGral as $value) {
            $demo[] = $value;
        }
        $this->view->dias = $demo;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'listaReservas':
                $dtaLista = $this->_reservaObj->getListaReservas('', array('t1.ac_desc'));
                echo $this->_citObj->json($dtaLista);
                break;

            case 'verDetalleReserva':
                if (!empty($_POST))
                    $dtaLista = $this->_reservaObj->getReservasDetalle("t1.res_id = '{$_POST['res_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;

            case 'mesActivo':
                if (!empty($_POST))
                    $dtaLista = $this->_reservaObj->getCalendarGestion($_POST);
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;
            case 'reservasDia':
                if (!empty($_POST))
                    $dtaLista = $this->_reservaObj->getReservasDia($_POST);
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'save':
                //echo urldecode($_POST['data']);
                var_dump($_POST);
                exit;
                $evalua = $this->_kardexObj->saveKardex($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'saveGestionMes':
                $evalua = $this->_kardexObj->saveGeneraMes($_POST);
                $this->_helper->Alert->direct($evalua, '', 'Mes generado correctamente', 'El mes a generar no afecta a las reservas(mes antiguo).');
                break;
        endswitch;
    }

}
