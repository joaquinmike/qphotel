<?php

/* * **
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class ReservaBloqueoRangoController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_reservaObj;
    public $_fecObj;
    public $_estado;
    public $_kardexObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_reservaObj = new DbHtResReserva();
        $this->_habitacionObj = new DbHtHbHabitacion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_estado = array('0' => 'Inactivo', '1' => 'Activo', 'R' => 'Reservados');
    }

    public function formAction() {

        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;

        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $meses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre',
            '12' => 'Diciembre');

        $this->view->hoy = date("m-d-Y");
        $this->view->mesid = date('m');
    }

    public function jsonAction() {
        $fecObj = new Cit_Db_CitFechas();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        $fecha_fin = $this->getRequest()->getParam('fecha_fin', 0);
        $fecha_ini = $this->getRequest()->getParam('fecha_ini', 0);
        $ka_situacion = $this->getRequest()->getParam('ka_situacion', 0);
        $su_id = $this->getRequest()->getParam('su_id', 0);
        $tih_id = $this->getRequest()->getParam('tih_id', 0);
        set_time_limit(0);
        switch ($case):
            case 'cantHab':
                if ($ka_situacion == '0')
                    $ka_sit = '1';
                else
                    $ka_sit = '0';

                if ($tih_id == 'T') {
                    $tih = '';
                    $tihUpd = '';
                } else {
                    $tih = "and t1.tih_id = '$tih_id'";
                    $tihUpd = "and tih_id = '$tih_id'";
                }
//                $fecObj->setFecha($fecha_ini);
                $fecObj->setData($fecha_ini);
                $fecha_ini = $fecObj->renders('save');
//                $fecObj->setFecha($fecha_fin);
                $fecObj->setData($fecha_fin);
                $fecha_fin = $fecObj->renders('save');

                $where = "t1.ka_situacion = '$ka_sit' and t1.ka_fecha between '$fecha_ini' and '$fecha_fin' and t1.su_id = '$su_id' " . $tih;
                $whereUpd = "ka_situacion = '$ka_sit' and ka_fecha between '$fecha_ini' and '$fecha_fin' and su_id = '$su_id' " . $tihUpd;

                $select = $this->_helper->DBAdapter()->select();
                $select->from(array('t1' => 'ht_res_kardex'), array('tih_cant' => '(count(*))'))
                        ->join(array('t2' => 'vht_hb_tipo_habitacion'), "t1.tih_id = t2.tih_id and t2.id_id = '{$this->_sesion->lg}'", array('tih_desc'))
                        ->where($where)
                        ->group('t2.tih_desc');
                //echo $select;exit;
                $dtaLista = $select->query()->fetchall();
                echo $this->_citObj->json($dtaLista);
                break;
            case 'listaHabDisp':
                $num_dias = (int) $fecObj->restaFechas($fecha_ini, $fecha_fin) + 1;
                $fecObj->setData($fecha_ini);
                $fecha_ini = $fecObj->renders('save');
//                $fecObj->setFecha($fecha_fin);
                $fecObj->setData($fecha_fin);
                $fecha_fin = $fecObj->renders('save');
                if ($tih_id == 'T') {
                    $tih = '';
                } else {
                    $tih = " and t1.tih_id = '$tih_id'";
                }
//                $where = "t1.ka_situacion = '1' and t1.ka_fecha between '$fecha_ini' and '$fecha_fin' and t1.su_id = '$su_id' "."and t1.tih_id = '$tih_id'"." and t2.id_id = '{$this->_sesion->lg}'";
                $select->from(array('t1' => 'ht_res_kardex'), array('hb_id', 'tih_id', 'su_id'))
                        ->join(array('t2' => 'ht_hb_idioma_habitacion'), "t1.hb_id = t2.hb_id and t2.id_id = '{$this->_sesion->lg}'", array('hb_desc'))
                        ->join(array('t3' => 'ht_hb_idioma_tipo_habitacion'), "t1.tih_id = t3.tih_id and t3.id_id = '{$this->_sesion->lg}'", array('tih_abr'))
                        ->where("t1.ka_situacion = '1' and t1.ka_fecha between '$fecha_ini' and '$fecha_fin' and t1.su_id = '$su_id' " . " and t2.id_id = '{$this->_sesion->lg}'" . $tih)
                        ->group(array('t1.hb_id', 't1.tih_id', 't1.su_id', 't2.hb_desc', 't3.tih_abr'))
                        ->having("count(t1.hb_id) = " . $num_dias)
                        ->order('t1.hb_id');
                $dtaLista = $select->query()->fetchall();
                echo $this->_citObj->json($dtaLista);
                break;
            default:
                break;
        endswitch;
    }

    public function ajaxAction() {
        $fecObj = new Cit_Db_CitFechas();
        //$this->_fecObj = new Cit_Db_CitFechas();
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', '');
        $fecha_fin = $this->getRequest()->getParam('fecha_fin', '');
        $fecha_ini = $this->getRequest()->getParam('fecha_ini', '');
        $ka_situacion = $this->getRequest()->getParam('ka_situacion', '');
        $su_id = $this->getRequest()->getParam('su_id', '');
        $tih_id = $this->getRequest()->getParam('tih_id', '');
        $hb_id = $this->getRequest()->getParam('cadhbid', '');

        set_time_limit(0);
        switch ($case):
            case 'actBloqueo':
                if ($ka_situacion == '0')
                    $ka_sit = '1';
                else
                    $ka_sit = '0';

                if ($tih_id == 'T') {
                    $tih = '';
                    $tihUpd = '';
                } else {
                    $tih = "and tih_id = '$tih_id'";
                    $tihUpd = "and tih_id = '$tih_id'";
                }
                $hb_id = str_replace('\\', '', $hb_id);

                $fecObj->setFecha($fecha_ini);
                $fecObj->setData($fecha_ini);
                $fecha_ini = $fecObj->renders('save');
                $fecObj->setFecha($fecha_fin);
                $fecObj->setData($fecha_fin);
                $fecha_fin = $fecObj->renders('save');
                if ($ka_situacion == '0') {
                    $where = "ka_situacion = '$ka_sit' and ka_fecha between '$fecha_ini' and '$fecha_fin' and su_id = '$su_id' " . $tih . " and hb_id in($hb_id)";
                    $evalua = $this->_kardexObj->updateEstado(array('ka_situacion' => $ka_situacion), $where);
                } else {
                    $where = "ka_situacion = '$ka_sit' and ka_fecha between '$fecha_ini' and '$fecha_fin' and su_id = '$su_id' " . $tih;
                    $evalua = $this->_kardexObj->updateEstado(array('ka_situacion' => $ka_situacion), $where);
                }
                $this->_helper->Alert->direct($evalua, '', 'Transacción realizada con éxito', 'No es posible realizar la transacción', '');
                break;
            default :
                break;
        endswitch;
    }

}
