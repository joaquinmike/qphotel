<?php
/**
 * MantenimientoServicioController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';
class MantenimientoServicioController extends Zend_Controller_Action
{
    public $_aulaObj;

	//public $_gradObj;
	public $_sesion;
	public $_citObjt;
	public $_ubigeo;
	public $_htsermantServicioObj;
	
	//public $_profeSalonObj;
	function init(){
		
		$this->_ubigeo = new DbHtSegUbigeo();
		$this->_citObj = new CitDataGral();
		$this->_htsermantServicioObj = new DbHtSerServicio();
		//$this->_ht
	    $this->_sesion = new Zend_Session_Namespace('login');
	}
    public function formAction() 
    {
		$select = array('set_id','set_titulo');
    	$where = array('where'=>"id_id = '{$this->_sesion->lg}'");
		$sertipo = $this->_citObj->getDataGral('vht_ser_tipo',$select,$where);
		$this->view->tiposerv  = $this->_helper->Combos->consulta($sertipo, array('id'=>'set_id','desc' => 'set_titulo'), 1);
		$this->view->tiposervf = $this->_helper->Combos->consulta($sertipo, array('id'=>'set_id','desc' => 'set_titulo'), 'T');
    	
    	$this->view->serestado = $this->_helper->Combos(array('1'=>'Activo','0'=>'Inactivo'),array('id'=>'ser_estado'));
    	$this->view->servdepartamento = $this->_helper->Combos($this->_ubigeo,array('id'=>'ub_id','mask'=>'ub_id_dep','desc' => 'ub_desc','where'=>"substr(ub_id,3,4)='0000'",'order'=>'ub_desc'));
    	$this->view->idserv = $this->_htsermantServicioObj->correlativo(8);
    	//var_dump('ok'); exit;
  		//$this->view->listaArea = $this->_helper->Combos(new DbArea(),array('id'=>'ar_id','desc' => 'ar_desc','order'=>'ar_desc'));
    }
    
    
    public function jsonAction(){
    	//$this->_citObjt->getDataGral($table, $data);
   		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$select = $this->_helper->DBAdapter()->select();
		switch ($case){
	    	case 'servicio':
	    		if ($_POST['set_id'] == 'T')
	    			$wheresetid = '';
	    		else
	    			$wheresetid = "and t1.set_id = '{$_POST['set_id']}'";
	    				    		
	    		
				$select->from(array('t1'=>'ht_ser_servicio'), array('hser_id'=>'ser_id','ser_id', 'set_id', 'ser_razsocial','ser_ruc','ser_direccion','ser_telefono1','ser_telefono2','ser_web','ser_correo','ser_precio','ser_estado','ub_id'))
					   ->join(array('t2'=>'ht_ser_idioma_servicio'),'t1.ser_id = t2.ser_id', array('ser_titulo', 'ser_desc'))
					   ->join(array('t3'=>'ht_seg_ubigeo'), 't1.ub_id = t3.ub_id', array('ub_desc'))
					   ->join(array('t4'=>'ht_ser_idioma_tipo'), 't1.set_id = t4.set_id', array('set_titulo'))
					   ->where("t2.id_id = '{$this->_sesion->lg}' and t4.id_id = '{$this->_sesion->lg}'".$wheresetid);
				//echo $select;exit;
	  
				$listadoSer = $select->query()->fetchAll();
				//var_dump($listadoSuc);exit;
				foreach ($listadoSer as $indice => $value){
		  				
		  				 $value['ub_id_dep'] = str_pad(substr($value['ub_id'], 0,2),6,'0',STR_PAD_RIGHT);  		
						 $value['ub_id_prov'] = str_pad(substr($value['ub_id'], 0,4),6,'0',STR_PAD_RIGHT);
		  				
		  				$listadoSer[$indice]=$value;
				}
		  			//echo $listadoSuc[$indice];exit;
				echo $this->_htsermantServicioObj->json($listadoSer);
				
			break;
			case 'newserv':	
					echo json_encode(array('ser_id'=>$this->_htsermantServicioObj->correlativo(8)));
			break;
    	}	
	}
	
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		$_POST['id_id'] = $this->_sesion->lg;
		switch ($case){ 
	  		case 'save':	  			
	  			$result = $this->_htsermantServicioObj->saveData($_POST);
	  			$this->_helper->Alert($result,'','','','Existen datos relacionados.');
	  			break;
	  		case 'delete':	  			
	  			$_POST['action'] = 'delete';
	  			$this->_helper->Alert($this->_htsermantServicioObj->deleteSer($_POST),'','','','Existen datos relacionados');
	  			break;
		}		
	}	      
}
