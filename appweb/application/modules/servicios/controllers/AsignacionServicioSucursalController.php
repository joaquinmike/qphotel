<?php
/**
 * AsignacionServicioSucursalController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';
class AsignacionServicioSucursalController extends Zend_Controller_Action
{
    /**
     * The default action - show the home page
     */
     
    public $_aulaObj;

	//public $_gradObj;
	public $_sesion;
	public $_hthbHotelObj;
	public $_hthbSucursalObj;
	public $_citObj;
	public $_ubigeo;
    public $_servicioSucursalObj;
    public $_htserServicioSucursal;
    
    function init(){
		$this->_citObj = new CitDataGral();
		$this->_ubigeo = new DbHtSegUbigeo();
		$this->_hthbHotelObj = new DbHtHbHotel();
		$this->_hthbSucursalObj = new DbHtHbSucursal();
        $this->_servicioSucursalObj = new DbHtSrvServicioSucursal(); 
        $this->_htserServicioSucursal = new DbHtSerServicio();
	    $this->_sesion = new Zend_Session_Namespace('login');
	}
    
    public function formAction ()
    {
        // TODO Auto-generated AsignacionServicioSucursalController::indexAction() default action
        $select = array('set_id','set_titulo');
    	$where = array('where'=>"id_id = '{$this->_sesion->lg}'",'order'=>'set_titulo');
		$tipoServicio = $this->_citObj->getDataGral('vht_ser_tipo',$select,$where);
		$this->view->tipoServicio  = $this->_helper->Combos->consulta($tipoServicio,array('id'=>'set_id','desc' => 'set_titulo'),1);
    }
    
    public function jsonAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$select = $this->_helper->DBAdapter()->select();
		switch ($case){
	    	case 'sucursal':
				$dtaListaSuc = $this->_hthbSucursalObj->getListaSucursal(array('where'=>"t1.ho_id = '{$this->_sesion->hoid}' and su_estado = '1'",'order'=>'su_nombre'));
				echo $this->_citObj->json($dtaListaSuc);

			break;
            
            case 'listaServicioSucursal':
    			if(!empty($_POST))
					$dtaListaSucServ = $this->_servicioSucursalObj->getServicioSuc("su_id = '{$_POST['su_id']}'",'ser_titulo');
				else
					$dtaListaSucServ = array();
				echo $this->_citObj->json($dtaListaSucServ,'matchacc');
 			break;
            
            case 'selServicio'://Seleccionar accesorios
    			if(!empty($_POST))// and su_id = '{$_POST['su_id']}'
					$dtaListaServicio = $this->_htserServicioSucursal->selListaAsignarServicio($_POST);
				else
					$dtaListaServicio = array();
				echo $this->_citObj->json($dtaListaServicio,'matchacc');
    			break;
			case 'newsucur':	
					echo json_encode(array('su_id'=>$this->_hthbSucursalObj->correlativo(8)));
			break;
    	}
    }
    
    public function ajaxAction(){
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
    	switch ($case):
	    	case 'saveServSucursal':
	    		$_POST['grid'] = explode('**',$_POST['grid']);
        		$evalua = $this->_servicioSucursalObj->saveData($_POST);
        		$this->_helper->Alert->direct($evalua,'','Servicios asignados correctamente','','');
	    		break;
	    	case 'delServSucursal':
	    		$_POST['grid'] = explode('**',$_POST['grid']);
        		$evalua = $this->_servicioSucursalObj->deleteData($_POST);
        		$this->_helper->Alert->direct($evalua['id'],'',@$evalua['desc'],@$evalua['desc'],'');
	    		break;
    	endswitch;
    }
	
}
