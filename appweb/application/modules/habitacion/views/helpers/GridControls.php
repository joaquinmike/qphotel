<?php
/**
 *
 * @author karito 
 * @version 
 */

/**
 * GridControls helper
 *
 * @uses helper Zend_View_Helper
 */
class Zend_View_Helper_GridControls
{
    /**
     * @var Zend_View_Interface 
     */
    public $view;
    /**
     *  
     */
    public function gridControls (array $options = array())
    {
        $form = new Zend_Form(); 
        $form->setname('frm_grid_actions');
        $form->setAction('')->setMethod('post');
        $ctrl1 = new Cit_Form_Element_Comment('link1', array('value'=>'<a href="javascript:checkAll(true)" >Marcar Todos</a> |'));
        
        $ctrl2 = new Cit_Form_Element_Comment('link2', array('value'=>'<a href="javascript:checkAll(false)">Desmarcar Todos</a> |'));

        $ctrl3 = new Zend_Form_Element_Select('action_grid', array('style'=>'float:none','onChange'=>'gridActions(this)'));
        $ctrl3->setLabel('Acciones :')->addMultiOptions($options);

        $ctrl4 = new Zend_Form_Element_Hidden('grid_ids');
        $ctrl5 = new Zend_Form_Element_Hidden('grid_order');

        $form->addElements(array($ctrl1,$ctrl2, $ctrl3, $ctrl4,$ctrl5));

		$form->clearDecorators();
		$form->addDecorator('FormElements')
		     ->addDecorator('Form');
		$form->setElementDecorators(array(
		     array('ViewHelper'),
		     array('Label', array('style'=>'float:none')),
		));
		
		return $form;
    }
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
