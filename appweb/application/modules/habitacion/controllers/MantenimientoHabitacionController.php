<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoHabitacionController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_habitObj;
    public $_citObj;
    public $_pisObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_habitObj = new DbHtHbHabitacion();
        $this->_citObj = new CitDataGral();
        $this->_pisObj = new DbHtHbPiso();
    }

    public function formAction() {
        //Sucursal
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'", 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
        //Tipo de Habitacion
        $select = array('tih_id', 'tih_desc');
        $where = array('where' => '', 'order' => 'tih_desc');
        $sucursal = $this->_citObj->getDataGral('vht_hb_tipo_habitacion', $select, $where);
        //var_dump($sucursal);exit;
        $this->view->tipohab = $this->_helper->Combos->consulta($sucursal, array('id' => 'tih_id', 'desc' => 'tih_desc'), 1);
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'newIDhabit'://Devolver el ID para una nueva habitacion
                $dtaHab = $this->_citObj->returnCorrelativo('ht_hb_habitacion', array('hb_id' => 'max("hb_id")'), 8);
                $dtaHab[0]['hhb_id'] = $dtaHab[0]['hb_id'];
                echo $this->_citObj->json($dtaHab, 'matchhab');
                break;

            case 'datosPiso'://Devolver los datos del Modal Piso
                $dtaSucursal = $this->_citObj->getDataGral('ht_hb_sucursal', array('su_id', 'su_nombre'), "su_id = '{$_POST['su_id']}'");
                echo $this->_citObj->json($dtaSucursal, 'matchpiso');
                break;
            ////tipHabModal
            case 'tipHabModal'://Devolver los datos del Modal Piso
                $dtaSucursal = $this->_habitObj->getSucursalTipohab("su_id = '{$_POST['su_id']}'");
                echo $this->_citObj->json($dtaSucursal, 'matches');
                break;
            case 'listaHabitacion':
                $where = '';
                $i = 0;
                if (!empty($_POST['su_id'])):
                    $where .= "t1.su_id = '{$_POST['su_id']}'";
                    $i++;
                endif;
                if (!empty($_POST['tih_id'])):
                    if ($i == 1)
                        $where .= ' and ';
                    $where .= "t1.tih_id = '{$_POST['tih_id']}'";
                    $i++;
                endif;
                if (!empty($_POST['pi_id'])):
                    if ($i > 0)
                        $where .= ' and ';
                    $where .= "t4.pi_id = '{$_POST['pi_id']}'";
                endif;
                //var_dump($where); exit;
                $dtaHab = $this->_habitObj->getDatosHabit($where, array('t1.su_id', 't4.pi_id'));

                echo $this->_citObj->json($dtaHab);
                break;

            case 'pisos':
                $select = array('pi_id', 'pi_desc');
                if (!empty($_POST['su_id'])):
                    $dtaHab = $this->_citObj->getDataGral('vht_hb_piso', $select, "su_id = '{$_POST['su_id']}' and id_id = '{$this->_sesion->lg}'");
                else:
                    $dtaHab = array();
                endif;

                echo $this->_citObj->json($dtaHab, 'matchpiso');
                break;
            case 'precioHab':
                $select = array('hb_precio');
                if (!empty($_POST)):
                    $dtaHab = $this->_citObj->getDataGral('ht_hb_habitacion_tarifa', $select, "su_id = '{$_POST['su_id']}' and tih_id = '{$_POST['tih_id']}'");
                    $array = array();
                    foreach ($dtaHab as $value):
                        $array['hb_precio'] = number_format($value['hb_precio'], 2);
                        break;
                    endforeach;
                    $dtaPrecio[] = $array;
                else:
                    $dtaPrecio = array();
                endif;

                echo $this->_citObj->json($dtaPrecio);
                break;
            case 'imagen':
                $select->from(array('t1' => 'ht_cms_imagenes'), array('*'));
                $select->joinleft(array('t3' => 'ht_hb_habitacion_imagen'), 't1.im_id=t3.im_id', array(''));
                $select->where("t3.hb_id='{$_POST['hb_id']}'");
                //$select->where("t3.tih_id='{$_POST['tih_id']}'");
                //echo $select;exit;
                $dataimagen = $select->query()->fetchAll();
                //var_dump($dataimagen); exit;
                $newdata = array();
                foreach ($dataimagen as $value) {
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else{
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image2'];
                    }
                    $newdata['images'][] = array('name' => 'ti', 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
                }
                if (empty($newdata)) {
                    $newdata['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'im_id' > '');
                }
                echo json_encode($newdata);


                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        set_time_limit(0);
        switch ($case):
            case 'delImgHbt':
                $_POST['ho_id'] = $this->_sesion->hoid;
                $evalua = $this->_habitObj->deleteImagen($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'No se puede eliminar imagen.', '');
                break;
            case 'saveHabitacion':
                $evalua = $this->_habitObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'saveNewPiso':
                $evalua = $this->_pisObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'delHabitacion':
                $evalua = $this->_habitObj->deleteHabitacion($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen datos relacionados.', '');
                break;
            case 'saveimage':
                 try {
                    //error_reporting(NULL);
                    $_POST['ho_id'] = $this->_sesion->hoid;
                     
                    //copy($_FILES['imgtipo']['tmp_name'], MP.'/nv.jpg');
                    
                    $this->_habitObj->saveImagen($_POST, 'room');
                    echo json_encode(array());
                } catch (Zend_Exception $e) {
                    
                    Zend_Registry::get('Log')->log($e->getMessage(), Zend_Log::CRIT);            
                    //throw new Zend_Db_Statement_Exception($e->getMessage());
                }
                break;
        endswitch;
    }

}
