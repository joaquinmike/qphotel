<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionAccesorioTipoHabController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_accesObj = new DbHtHbAccesorios();
        $this->_citObj = new CitDataGral();
        $this->_tipohabObj = new DbHtHbTipoHabitacion();
        $this->_accesTipohab = new DbHtHbAccesorioSucursalTiphab();
        $this->_tihSucursalObj = new DbHtHbSucursalTipoHabitacion;
    }

    public function formAction() {
        //Sucursal
        $select = array('su_id', 'su_nombre');
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');

        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $this->view->suid = $this->_sesion->suid;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaTipohab'://Lista de tipo de habitaciones por sucursal
                if ($_POST['su_id']):
                    $dtaLista = $this->_tipohabObj->getDatosSucurTipohab("su_id = '{$_POST['su_id']}'");
                else:
                    $dtaLista = array();
                endif;
                echo $this->_citObj->json($dtaLista);
                break;

            case 'selSucursalTipohab'://
                if (!empty($_POST))// and su_id = '{$_POST['su_id']}'
                    $dtaLista = $this->_tihSucursalObj->selTipohabSucursal("su_id = '{$_POST['su_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;

            case 'listaAccesxTipohab':
                if (!empty($_POST))
                    $dtaLista = $this->_accesTipohab->getAccesTipohab("tih_id = '{$_POST['tih_id']}' and su_id = '{$_POST['su_id']}'", 'ac_desc');
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista, 'matchacc');
                break;

            case 'selAccesorios'://Seleccionar accesorios
                if (!empty($_POST))// and su_id = '{$_POST['su_id']}'
                    $dtaLista = $this->_accesObj->selListaAsignarAccesorio("tih_id = '{$_POST['tih_id']}' and su_id = '{$_POST['su_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista, 'matchacc');
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout(); //-/form/00002
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'saveAccesTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_accesTipohab->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'delAccesTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_accesTipohab->deleteData($_POST);
                $this->_helper->Alert->direct($evalua['id'], '', @$evalua['desc'], @$evalua['desc'], '');
                break;
            case 'saveSucursalTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_tihSucursalObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;

            case 'delTipohabSucursal'://Eliminar Tipode habitaciones x Sucursal
                $evalua = $this->_tihSucursalObj->deleteData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen datos relacionados.', '');
                break;
        endswitch;
    }

}
