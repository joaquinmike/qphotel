<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoAccesoriosController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_pisObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_accesObj = new DbHtHbAccesorios();
        $this->_citObj = new CitDataGral();
        $this->_pisObj = new DbHtHbPiso();
    }

    public function formAction() {
        //Sucursal
        $newID = $this->_citObj->returnCorrelativo('ht_hb_accesorios', array('ac_id' => 'max("ac_id")'), 4, 'U');
        $this->view->ac_id = $newID['ac_id'];
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaAccesorio':
                $dtaLista = $this->_accesObj->getDatosAccesorio('', array('t1.ac_desc'));
                echo $this->_citObj->json($dtaLista);
                break;

            case 'newIDAcces':
                $dtaNew = $this->_citObj->returnCorrelativo('ht_hb_accesorios', array('ac_id' => 'max("ac_id")'), 4);
                $dtaNew[0]['hac_id'] = $dtaNew[0]['ac_id'];
                echo $this->_citObj->json($dtaNew, 'matchacc');
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'saveAccesorios':
                $evalua = $this->_accesObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'delAccesorios':
                $evalua = $this->_accesObj->deleteData($_POST);
                $this->_helper->Alert->direct($evalua, '', 'Accesorio eliminado correctamente', 'No es posible eliminar, existen accesorios ya asignado a un Tipo de Habitación.', '');
                break;
        endswitch;
    }

}
