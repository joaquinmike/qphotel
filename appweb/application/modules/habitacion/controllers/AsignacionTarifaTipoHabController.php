<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class AsignacionTarifaTipoHabController extends Zend_Controller_Action 
{
    /**
     * The default action - show the home page
     */
    public $_sesion; public $_accesObj;
    public $_citObj; public $_tipohabObj;
    public $_accesTipohab; public $_tihSucursalObj;
    public $_tarifaObj;
    private $_ConteTipHab;
	
    function init(){
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_accesObj = new DbHtHbAccesorios();
        $this->_citObj = new CitDataGral();
        $this->_tipohabObj = new DbHtHbTipoHabitacion();
        $this->_accesTipohab = new DbHtHbAccesorioSucursalTiphab();
        $this->_tihSucursalObj = new DbHtHbSucursalTipoHabitacion;
        $this->_tarifaObj = new DbHtHbHabitacionTarifa();
        $this->_ConteTipHab = new DbHtHbIdiomaSucTiphab();
    }
	
    public function formAction() {
    	//Sucursal
    	$select = array('su_id','su_nombre');
    	$where = array('where'=>"ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'",'order'=>'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal',$select,$where);
        $this->view->sucursal  = $this->_helper->Combos->consulta($sucursal,array('id'=>'su_id','desc' => 'su_nombre'),1);

        $tarifa = $this->_citObj->getDataGral('vht_hb_tarifa',array('ta_id','ta_desc'),array('where'=>"id_id = '{$this->_sesion->lg}'",'order'=>'ta_id'));
        $this->view->tarifa  = $this->_helper->Combos->consulta($tarifa,array('id'=>'ta_id','desc' => 'ta_desc'),1);
        $this->view->form = $this->_tarifaObj->getFormularioPrecios($tarifa);
    }
    
    public function jsonAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();	
        $case = $this->getRequest()->getParam('case',0);
        switch ($case):
            case 'listaSucursal'://Lista de tipo de habitaciones por sucursal
                $where = "ho_id = '{$this->_sesion->hoid}' and su_estado = '1'";
                //var_dump($this->_sesion->usid); exit;
                if($this->_sesion->usid != '00000001')
                    $where .= "and su_id = '{$this->_sesion->suid}'";
                else
                    $where .= "and su_id <> '00000001'";

                $dtaLista = $this->_citObj->getDataGral('ht_hb_sucursal', array('su_id','su_nombre','su_estado'),$where);

                echo $this->_citObj->json($dtaLista);
                break;

            case 'listaTipohab'://Lista de tipo de habitaciones por sucursal
                if(!empty($_POST['su_id'])):
                    $dtaLista = $this->_tipohabObj->getDatosSucurTipohab("su_id = '{$_POST['su_id']}'");
                else:
                    $dtaLista = array();
                endif;
                echo $this->_citObj->json($dtaLista);
                break;

        case 'precio'://Lista de tipo de habitaciones por sucursal
            if($this->getRequest()->isPost()):
                $post = $this->getRequest()->getParams();
                $dtaLista = $this->_tarifaObj->getPreciosCapacidad($post);
            else:
                $dtaLista = array();
            endif;
            echo $this->_citObj->json($dtaLista);
            break;
            
        case 'contenido'://Lista de tipo de habitaciones por sucursal
            $items = array();
            if($this->getRequest()->isPost()):
                $post = $this->getRequest()->getParams();
                $objSucTipRoom = new DbVHtHbIdiomaSucTipoHabitacion();
                //$where = "su_id = '{$_POST['su_id']}' and tih_id = '{$_POST['tih_id']}' and id_id = '{$this->_sesion->lg}'";
                $dtaLista = $objSucTipRoom->fetchAll(
                        array('su_id =?' => $post['su_id'],
                        'tih_id =?' => $post['tih_id'],
                        'id_id =?' => $this->_sesion->lg)
                        )->toArray();
                foreach ($dtaLista as $value):
                    $value['tih_desc']=str_replace("\\", '', $value['tih_desc']);
                    $items[] = $value;
                endforeach;
           endif;
           echo $this->_citObj->json($items);
           break;
        case 'getImages':
//            if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getParams();
            $dtaLista = $this->_tihSucursalObj->getImagesTipHabSucursal($post);
            echo json_encode($dtaLista);
            break;
        endswitch;
    }

    public function ajaxAction() {
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case',0);
        $form = $this->getRequest()->getParam('form',0);
        switch($case):
            case 'saveSucursalTipha':
                $_POST['grid'] = explode('**',$_POST['grid']);
                $evalua = $this->_tihSucursalObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua,'','','Existen los registros.','');
                break;

            case 'delTipohabSucursal'://Eliminar Tipode habitaciones x Sucursal
                $_POST['grid'] = explode('**',$_POST['grid']);
                $evalua = $this->_tihSucursalObj->deleteDataArray($_POST);
                $this->_helper->Alert->direct($evalua['id'],'Existen reservas hechas con este tipo de habitación. Puede cambiar el estado.','');
                break;

            case 'editTipohabSucursal':
                $_POST['grid'] = explode('**',$_POST['grid']);
                $evalua = $this->_tihSucursalObj->editTipoHabitaciones($_POST);
                $this->_helper->Alert->direct($evalua,'','Punto agregado correctamente','Puntos modificados correctamente.','');
                break;

            case 'savePrecio':
                $evalua = $this->_tarifaObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua,'','Precio asignado Correctamente','No se asignaron los precios');
                break;
            case 'saveContenido':
                $_POST['id_id'] = $this->_sesion->lg;                
                $evalua = $this->_ConteTipHab->saveData($_POST);
                $this->_helper->Alert->direct($evalua,'','Contenido agregado a una Sucursal','No se asignaron los precios');
                break;
            case 'saveImagen':
                $post = $this->getRequest()->getParams();
                $data['ho_id'] = '01';
                $data['su_id'] = $post['su_id'];
                $data['tih_id'] = $post['tih_id'];
                $action = $this->_tihSucursalObj->saveImages($data);
                if($action == 1)
                    echo Zend_Json::encode(array("success" => true, 'data' => array("msge" => 'correctamente')));
                else
                    echo Zend_Json::encode(array("success" => true, 'data' => array("msge" => 'Fail')));
                
                break;
                
            case 'deleteImagen':
                $post = $this->getRequest()->getParams();
                $post['su_id'] = $this->_sesion->suid;
                $evalua = $this->_tihSucursalObj->deleteImages($post);
                $this->_helper->Alert->direct($evalua, '', '', 'No se puede eliminar imagen.', '');
                break;
            
            case 'changeState':
                $post = $this->getRequest()->getParams();
                $post['grid'] = explode('**',$post['grid']);
                $evalua = $this->_tihSucursalObj->stateChangeTypeRoom($post);
                $this->_helper->Alert->direct($evalua, '', '');
                break;
        endswitch;
    }
}
