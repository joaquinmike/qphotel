<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */

require_once 'Zend/Controller/Action.php';

class JsonController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_vtabladetalle;
	
	public $_control;
	
	public $_ubigeo;
	
	public $_familia;
	
	public $_grado;
	
	public $_seccion;
	
	public $_sesion;
	
	public $_aulaObj;
	

	
	function init(){
		$this->_vtabladetalle = new DbVtablaDetalle();
		
		$this->_control = new DbControl(); 
		
		$this->_ubigeo = new DbUbigeo(); 
		
		$this->_familia = new DbFamilia(); 
		
		$this->_grado = new DbGrado();
		
		$this->_seccion = new DbSeccion();
		
		$this->_sesion = new Zend_Session_Namespace('login');
		
		$this->_aulaObj = new DbAula();
		
		$this->_vaulaObj = new DbVAula();
		
	
		
		
	} 
	public function tabladetalleAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();

	  	 $dataubigeo = $this->_vtabladetalle->fetchAll(null,'tba_id')->toArray();
  		 echo  	$this->_ubigeo->json($dataubigeo);			
  		
	}
	
   
    public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();	    			
		$case = $this->getRequest()->getParam('case',0);
		$depart = $this->getRequest()->getParam('departamento',0);
		$prov = $this->getRequest()->getParam('provincia',0);
		$alid = $this->getRequest()->getParam('al_id',0);
		$faid = $this->getRequest()->getParam('fa_id',0);
		$tba_id = $this->getRequest()->getParam('tba_id',0);
		
	  	switch ($case){
	  		case 'tabla_detalle':	  			
	  			$select->from(array('t1'=>'vtabla_detalle'),array('tad_id','tad_desc'))
	  				   ->where("t1.tba_id = '$tba_id' and t1.id_id='".$this->_sesion->lg."'")
	  				   ->order('t1.tad_desc');
	  			$dataTablaDetalle = $select->query()->fetchAll();
	  			echo  $this->_familia->json($dataTablaDetalle, 'matches');
				break;
	  		case 'familia':
	  			$select->from(array('t1'=>'familia'),array('fa_id','fa_des'));
	  			$select->joinleft(array('t2'=>'valumno'),'t1.fa_id = t2.fa_id',array('pe_nomcomp'));
	  			$select->where("t1.fa_id='$faid'");
	  			$datafamilia = $select->query()->fetchAll();	  			 
	  			 echo $this->_familia->json($datafamilia);  				
	  			 break;
	  		case 'familias':
	  			$select->from(array('t1'=>'familia'),array('fa_id','state'=>'fa_des'));	  			
	  			//$datacontrol= $this->_control->getControl(array('con_id'=>'alucod','an_id'=>$this->_sesion->anio));
	  			//$select->joinleft(array('t2'=>'valumno'),'t1.fa_id = t2.fa_id',array('pe_nomcomp'));
	  			//$select->where("t1.fa_id='$faid'");
	  			$datafamilia = $select->query()->fetchAll();
	  				  			 
	  			 echo $this->_familia->json($datafamilia);  				
	  			 break;	 
	  		case 'departamento':
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,3,4)='0000'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('name'=>$value['ub_desc'],'value'=>$value['ub_id']);  		
	  			 }	 
	  			 echo  	$this->_ubigeo->json($newdata,'rows');			
	  			 break;
	  			 
	  		case 'provincia':
	  			$depart = substr($depart,0,2);
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,1,2)='$depart' and substr(ub_id,5,2)='00' and substr(ub_id,3,4)!='0000'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('name'=>$value['ub_desc'],'value'=>$value['ub_id']);  		
	  			 }	
	  			  echo $this->_ubigeo->json($newdata,'rows');  				
	  			 break;
	  			 
	  		case 'distrito':
	  			$prov = substr($prov,0,4);
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,1,4)='$prov' and substr(ub_id,5,2)!='00'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('name'=>$value['ub_desc'],'value'=>$value['ub_id']);  		
	  			 }	 
	  			  echo  $this->_ubigeo->json($newdata,'rows');				
	  			 break;
	  		case 'alumno':
	  			
	  			$select->from(array('t1'=>'valumno'),array('lleva'=>'al_id','al_id','pe_nomcomp','al_situacion'));
	  			
				$dataalumno = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($dataalumno);
	  			break;
	  			
	  		case 'alumnoid':
	  			//var_dump($_POST);exit;
	  			$select->from(array('t1'=>'valumno'),array('lleva'=>'al_id','al_id','pe_nomcomp','al_situacion','pe_appat'));
	  			$select->where("al_id='{$alid}'");
				$dataalumno = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($dataalumno); 
	  			break; 
	  			
	  		case 'alumnoob':
	  			$dataalumnoobservacion = $this->_alumnoOservacion->fetchAll("al_id = '{$_POST['al_id']}'")->toArray();
	  			echo  $this->_ubigeo->json($dataalumnoobservacion); 
	  			break; 
	  			
	  		case 'newalumno':
	  			$datacontrol= $this->_control->getControl(array('con_id'=>'alucod','an_id'=>$this->_sesion->anio));
	  			echo  $this->_ubigeo->json($datacontrol);
	  			break; 
	  		case 'grado':
	  			//echo "ni_id='{$_POST['ni_id']}'";exit;
	  			if(empty($_POST['ni_id']))$_POST['ni_id']=0;
	  			$datacontrol= $this->_grado->fetchAll("ni_id='{$_POST['ni_id']}'")->toArray();
	  			
	  			//var_dump($datacontrol);
	  			echo  $this->_grado->json($datacontrol);
	  			break; 
  			case 'aula':
	  		
				if(empty($_POST['au_tipcal']))
  				{
	  				$_POST['au_tipcal']=0;
	  				$datosaula=$this->_aulaObj->getAulas();
  				}else
	  				$datosaula=$this->_aulaObj->getAulas("au_tipcal='{$_POST['au_tipcal']}'");
				$dataaula=Array();
	  			$dataaula= $this->_aulaObj->getComboAulas($datosaula);
	  			echo  $this->_grado->json($dataaula,'matchesaulas');
	  			break; 
	  			
	  		case 'datosaula':
	  			$lleva = $this->getRequest()->getParam('lleva',0);
	  			if(empty($lleva))
	  			$lleva=0;
	  			$datosaula=$this->_aulaObj->getDatosAulas("lleva='{$lleva}'");
			    echo  $this->_grado->json($datosaula,'matchedatau');
	  			break; 
	  			
	  		case 'area':
	  			$lleva = $this->getRequest()->getParam('lleva',0);
	  			$ni_id= $this->getRequest()->getParam('ni_id',0);
	  			$select = $this->_helper->DBAdapter()->select();
	  			$select->from(array('t1'=>'varea'),array('t1.ar_id','t1.ar_desc'));
	  			$select->join(array('t2'=>'grado_area'),'t1.ar_id=t2.ar_id',array(''));
	  			$select->where("t2.an_id='{$this->_sesion->anio }'");
	  			$select->where("t1.id_id='{$this->_sesion->lg }'");
	  			$select->order("t1.ar_desc");
	  			if(!empty($lleva))
	  			$select->where("t2.gr_id='{$lleva}'");
	  			if(!empty($ni_id))
	  			$select->where("t2.ni_id='{$ni_id}'");
	  			
	  			
	  			$data=$select->query()
    				->fetchAll();
    			//echo $select:exit;
				echo  $this->_grado->json($data,'matches');
	  			break;
	  				
	  		case 'criterio':
	  			$lleva = $this->getRequest()->getParam('lleva',0);
	  			$au_id2= $this->getRequest()->getParam('au_id2',0);
	  			if(empty($lleva))
	  			$lleva=0;
	  			$sesion = new Zend_Session_Namespace('login');
				$select = $this->_helper->DBAdapter()->select();
				$select->from('varea_logro',array('arl_id','arl_desc'));
				$select->where("id_id='$sesion->lg'");
				$select->where("ar_id='$lleva'");
				$select->where("au_id2='$au_id2' and (bi_id='1' or bi_id='0')");
				//echo $select;exit;
				$data=$select->query()
    						->fetchAll();
    			
				echo  $this->_grado->json($data,'matches');
	  			break;			
	  		case 'nivel':
	  			$select->from(array('nivel'),array('ni_id', 'ni_desc'));
	  			$datanivel = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($datanivel);
	  			break; 	
	  		case 'turno':
	  			$select->from(array('turno'),array('tu_id', 'tu_nombre'));
	  			$dataturno = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($dataturno);
	  			break;		  			
	  		case 'seccion':
	  			$select->from(array('seccion'),array('se_id', 'se_desc'));
	  			$dataseccion = $select->query()->fetchAll();
	  			//var_dump($dataseccion);
	  			//exit;
	  			echo  $this->_ubigeo->json($dataseccion);
	  			break;
	  		case 'periodo':
	  			$select->from(array('periodo'),array('per_id', 'per_desc', 'per_num'));
	  			$dataperiodo = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($dataperiodo);
	  			break;
	  		case 'bimestre':
	  			$datab = $this->_vaulaObj->fetchAll("au_id2 = '{$_POST['au_id2']}' and id_id='{$this->_sesion->lg}'")->toArray();
	  			$newdata = array();
	  			for($i =1;$i<=$datab[0]['per_num'];$i++){
	  				$newdata[$i-1]['bi_id'] = $i;
					$newdata[$i-1]['bi_desc'] = $i.' Bimestre';
	  			}
	  			echo  $this->_ubigeo->json($newdata);
	  			break;
	  		
	  		case 'profesor':
	  			$select->from(array('t1'=>'personal'),array('t1.pr_id'))
	  				   ->join(array('t2'=>'persona'),'t1.pe_id=t2.pe_id',array('t2.pe_nomcomp'))
	  				   ->join(array('t3'=>'personal_cargo'),'t1.pr_id=t3.pr_id', array())
	  				   ->where('t3.ca_id = ?', '0001');
	  			$dataprofesor = $select->query()->fetchAll;
	  			echo  $this->_ubigeo->json($dataprofesor);
	  			break;
	  		default:
	  		break;
	  	}
	  	  	
	}
	public function gruposAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();	    			
    			$select->from(array('t1'=>'profesor_salon'),array())
    				   ->join(array('t2'=>'vgrupo'),'t1.gp_id = t2.gp_id',array('gp_id','gp_desc'))
    				   ->where("t1.au_id2 = '{$_POST['au_id2']}' and t1.ar_id = '{$_POST['ar_id']}' and t2.id_id = '".$this->_sesion->lg."'")
    				   ->order('t2.gp_desc');
    			$dataGrupo = $select->query()->fetchall();
    			echo $this->_aulaObj->json($dataGrupo);
	}
}
