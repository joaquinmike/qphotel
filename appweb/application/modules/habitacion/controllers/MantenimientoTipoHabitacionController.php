<?php

/**
 * MantenimientoClienteController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoTipoHabitacionController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_alumnoOservacion;
    public $_sesion;
    public $_citObj;
    public $_control;
    public $_tbadetalle;
    public $_ano;
    public $_objTipRoom;
    public $_colegioObj;

    function init() {
        $this->_citObj = new CitDataGral();
        $this->_ubigeo = new DbHtSegUbigeo();
        $this->_tbadetalle = new DbVtablaDetalle();
        $this->_objTipRoom = new DbHtHbTipoHabitacion();
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_habitimgtipo = new DbHtHbTipoHabitacionImagen();
    }

    public function formAction() {
        $this->view->idtipo = $this->_objTipRoom->correlativo(2);
        $this->view->suestado = $this->_helper->Combos(array('1' => 'Activo', '0' => 'Inactivo'), array('id' => 'tih_estado'));

//        $select = array('cat_id', 'cat_titulo');
//        $where = array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'cat_id');
//        $dtaSewe = $this->_citObj->getDataGral('vht_cms_menu_interno', $select, $where);
        //var_dump($sucursal);exit;
        $objMenuInterno = new DbVHtCmsMenuInterno();
        $dtaSewe = $objMenuInterno->fetchAll(array('id_id =?' => $this->_sesion->lg),'cat_id')->toArray();
        $this->view->cboCategory = $this->_helper->Combos->consulta($dtaSewe, array('id' => 'cat_id', 'desc' => 'cat_titulo'), 1);
        
        $objSucursal = new DbHtHbSucursal();
        $dtaSucursal = $objSucursal->fetchAll(null, 'su_id')->toArray();
        $this->view->cboSucursal = $this->_helper->Combos->consulta($dtaSucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case) {
            case 'delImgTipHab':
                $_POST['ho_id'] = $this->_sesion->hoid;
                $evalua = $this->_habitimgtipo->deleteImagen($_POST);
                $this->_helper->Alert($evalua, '', '', '', '');
                break;
            case 'save':
                $_POST['id_id'] = $this->_sesion->lg;
                //var_dump($_POST); exit;
                $this->_helper->Alert($this->_objTipRoom->saveData($_POST));
                break;
            case 'delete':
                $this->_helper->Alert($this->_objTipRoom->deleteCliente($_POST));
                break;
            case 'saveimage':
                //error_reporting(NULL); 
                $post = $this->getRequest()->getParams();
                $post['ho_id'] = $this->_sesion->hoid;
                //copy($_FILES['imgtipo']['tmp_name'], MP.'/nv.jpg');
                $this->_objTipRoom->saveImagen($post, 'room');
                if($action == 1)
                    echo Zend_Json::encode(array("success" => true, 'data' => array("msge" => 'correctamente')));
                else
                    echo Zend_Json::encode(array("success" => true, 'data' => array("msge" => 'Fail')));
                break;
        }
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case) {
            case 'tipo':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);
                $post = $this->getRequest()->getParams();
                $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id_id' => 'tih_id', 't1.*'));
                $select->where("id_id = '{$this->_sesion->lg}'");
                if (!empty($post['cat_id']))
                    $select->where('cat_id =?',$post['cat_id']);
                   
                $select->order('tih_orden');
                //echo $select;exit;
                $data = $select->query()->fetchAll();

                if (!empty($start))
                    $pagina = (int) (($start / $limit) + 1);
                else
                    $pagina = 1;
                $paginator = Zend_Paginator::factory($data);
                $paginator->setCurrentPageNumber($pagina)
                        ->setItemCountPerPage($limit);

                foreach ($paginator->getCurrentItems() as $value) {
                    $value['tih_comen'] = str_replace("\\", '', $value['tih_comen']);
                    $value['tih_estado'] = (int) $value['tih_estado'];
                    $dat[] = $value;
                }

                if (!empty($dat)) {
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(Array('totalCount' => $total, 'matches' => $dat, 'page' => $pagina, 'total' => $paginator->getTotalItemCount(), 'records' => $records, "success" => 'true'));
                } else {
                    echo $this->_objTipRoom->json(array());
                }
                break;

            case 'newtipo':
                echo json_encode(array('tih_id' => $this->_objTipRoom->correlativo(2)));
                break;

            case 'imagen':
                $tih_id = $this->getRequest()->getParam('tih_id', 0);
                $su_id = $this->getRequest()->getParam('su_id', '00000001');
                
                $select->from(array('t1' => 'ht_cms_imagenes'), array('*'))
                    ->joinleft(array('t3' => 'ht_hb_tipo_habitacion_imagen'), 't1.im_id=t3.im_id', array(''))
                    ->where('tih_id =?', $tih_id)
                    ->where('su_id =?', $su_id);
                
                //echo $select;exit;
                $dataimagen = $select->query()->fetchAll();
                $newdata = array();
                foreach ($dataimagen as $value) {
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];
                    $newdata['images'][] = array('name' => $value['im_id'], 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
                }
                if (empty($newdata)) {
                    $newdata['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'idimg' => 0);
                }
                echo json_encode($newdata);
                break;
        }
    }

}
