<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionHabitacionTarifasController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_tipohabObj;
    public $_fecOjb;
    public $_tarifahabObj;
    public $_tarifaObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');

        $this->_fecOjb = new Cit_Db_CitFechas();
        $this->_citObj = new CitDataGral();
        $this->_tipohabObj = new DbHtHbTipoHabitacion();
        $this->_tarifahabObj = new DbHtHbHabitacionTarifa();
        $this->_tarifaObj = new DbHtHbHabitacionTarifa();
    }

    public function formAction() {
        //Sucursal
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}'  and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);

        $this->view->suid = $this->_sesion->suid;

        $tarifa = $this->_citObj->getDataGral('vht_hb_tarifa', array('ta_id', 'ta_desc'), array('where' => "id_id = '{$this->_sesion->lg}'", 'order' => 'ta_id'));
        $this->view->form = $this->_tarifaObj->getFormularioPrecios($tarifa);
        $field = '';
        $column = '';
        foreach ($tarifa as $value):
            $id = 'hb_precio_' . $value['ta_id'];
            $field .= ",'$id'";
            $column .= ",{dataIndex: '{$id}', header: 'Precio {$value['ta_id']}', width: 80,locked: true,align:'right',
						renderer: function(v, params, record){
							record.data.$id = v;
							return cit.formatMon(v);
						}
					}";
        endforeach;
        $grid['field'] = $field;
        $grid['column'] = $column;
        $this->view->grid = $grid;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'listaTipohab'://Lista de tipo de habitaciones por sucursal
                if ($_POST['su_id']):
                    $dtaLista = $this->_tipohabObj->getDatosSucurTipohab("su_id = '{$_POST['su_id']}'");
                    $i = 0;
                    foreach ($dtaLista as $value):
                        $save = $this->_citObj->getDataGral('ht_hb_habitacion', array('stock' => new Zend_Db_Expr('count(*)')), "su_id = '{$_POST['su_id']}' and tih_id = '{$value['tih_id']}'", 'U');
                        $dtaLista[$i]['tih_stock'] = $save['stock'];
                        $i++;
                    endforeach;
                else:
                    $dtaLista = array();
                endif;
                echo $this->_citObj->json($dtaLista);

                break;

            case 'listaTarifas':
                if (!empty($_POST))
                    $dtaLista = $this->_tarifahabObj->getHabitacionTarifas("t1.su_id = '{$_POST['su_id']}' and t1.tih_id = '{$_POST['tih_id']}'");
                else
                    $dtaLista = array();

                echo $this->_citObj->json($dtaLista);
                break;

        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'savePromoSucursal':
                $evalua = $this->_promosuObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;


        endswitch;
    }

}
