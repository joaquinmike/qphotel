<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class ProcesoReservaController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_kardexObj;
    public $_fecObj;
    public $_hotel;
    private $_tarifaObj;
    private $_tipohabObj;
    private $_promObj;
    private $_cache;
    private $_dbImagenesArticulo;
    private $_promo;

    function init() {
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->view->headTitle($this->_sesion->datahotel[0]['su_reserva'] . ' - ' . Cit_Init::_('PAGE_RESERVA_I'));
        
        $this->_tarifaObj = new DbHtHbTarifa();
        $this->_hotel = new DbHtHbSucursal();
        $this->_kardexObj = new DbHtResKardex();
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_promObj = new DbHtCmsPromocion();
        $this->_tipohabObj = new DbHtHbIdiomaTipoHabitacion();
        $this->_dbImagenesArticulo = new DbCmsImagenesArticulo();
        $this->_mesObj = new DbHtCmsMeses();         
        $frontendOptions = array(
            'lifetime' => 3600, // cache lifetime of 2 hours
            'automatic_serialization' => true
        );
        
        $backendOptions = array(
            'cache_dir' => dirname(MP) . '/appweb/var/cache/'  // Directory where to put the cache files
        );
        // getting a Zend_Cache_Core object
        $this->_cache = Zend_Cache::factory('Core',
            'File',
            $frontendOptions,
            $backendOptions);
        
        $this->_promo = $this->getRequest()->getParam('promo');
    }

    public function indexAction() {
        //Elimando si hay reservas
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        $carrito->removeAll();
        unset( $this->_sesion->reserva_price);
        
        $post = $this->getRequest()->getParams();
        
        if(!empty($post['ho_id'])):
            
            $form = new Zend_Form();
           // $dta = $form->isValid($post);
           // var_dump($dta); exit;
            $this->_sesion->su_id = $post['ho_id'];

            $this->_sesion->datahotel = $this->_hotel->fetchAll("su_id='{$post['ho_id']}'")->toArray();
            $this->view->suc = $this->_sesion->datahotel;

            $this->view->fechaini = str_replace('/', '-', $post['fecha_ini']);
            $this->view->fechafin = str_replace('/', '-', $post['fecha_fin']);
            $fechainis = explode('/',$post['fecha_ini']);            
            $mes = $fechainis[1];
            $dataMes = $this->_mesObj->getMesNombre("$mes",$this->_sesion->lg);
            $fechafin = explode('/',$post['fecha_fin']);
            //print_r($dataMes);exit;
            $this->view->fechainis2 = ($this->_sesion->lg=='ES')? $fechainis[0].' '
                                    .$dataMes['mes_desc'].' '.$fechainis[2] :
                                     $dataMes['mes_desc'].'/'.$fechainis[0]
                                    .'/'.$fechainis[2] ;
            
            $mes = $fechafin[1];
            $dataMes2 = $this->_mesObj->getMesNombre("$mes",$this->_sesion->lg);
            $this->view->fechafins2 = ($this->_sesion->lg=='ES')? $fechafin[0].' '
                                    .$dataMes2['mes_desc'].' '.$fechafin[2]:
                                     $dataMes2['mes_desc'].'/'.$fechafin[0]
                                    .$fechafin[2];
            
            $this->view->fechainis = $post['fecha_ini'];                        
            $this->view->fechafins = $post['fecha_fin'];
            $this->_sesion->reserve_info = $post;

            $this->view->adultos = $post['num_adults'];
            $this->view->children = $this->_sesion->reserve_info['num_ninios'];
            $dias = $this->_fecObj->restaFechas($post['fecha_ini'], $post['fecha_fin']);
            $this->_sesion->reserve_info['fec_dias'] = $this->_fecObj->suma_fechas($post['fecha_ini'], $dias - 1);
            $post['fecha_fin'] = $this->_sesion->reserve_info['fec_dias'];
           
            $this->_sesion->reserve_info['dias'] = $dias;

            //$this->_fecObj->setFecha($post['fecha_ini']);
            $this->_fecObj->setData($post['fecha_ini']);
            $post['fecha_ini'] = $this->_fecObj->renders('save');

            //$this->_fecObj->setFecha($post['fecha_fin']);
            $this->_fecObj->setData($post['fecha_fin']);
            $post['fecha_fin'] = $this->_fecObj->renders('save');
            $this->_sesion->reserve_info['reserva'] = 1;
        else:
            if(empty($this->_sesion->reserve_info['reserva'])){
                $this->_redirect(Cit_Init::config()->subdomain);
            }
            
            $this->view->suc = $this->_sesion->datahotel;
            
            //$post['ho_id'] = $this->_sesion->su_id;
            $post['fecha_fin'] = $this->_sesion->reserve_info['fec_dias'];
            $dias = $this->_sesion->reserve_info['dias'];
            
            $this->view->fechaini = str_replace('/', '-', $this->_sesion->reserve_info['fecha_ini']);
            $this->view->fechafin = str_replace('/', '-', $this->_sesion->reserve_info['fecha_fin']);
            $this->view->adultos = $this->_sesion->reserve_info['num_adults'];
            $this->view->children = $this->_sesion->reserve_info['num_ninios'];
            
            $this->view->fechainis = $this->_sesion->reserve_info['fecha_ini'];
            $this->view->fechafins = $this->_sesion->reserve_info['fecha_fin'];
            
            //
            $fechainis = explode('/',$this->_sesion->reserve_info['fecha_ini']);            
            $mes = $fechainis[1];
            $dataMes = $this->_mesObj->getMesNombre("$mes",$this->_sesion->lg);
            $fechafin = explode('/',$this->_sesion->reserve_info['fecha_fin']);
            //print_r($dataMes);exit;
            $this->view->fechainis2 = ($this->_sesion->lg=='ES')? $fechainis[0].' '
                                    .$dataMes['mes_desc'].' '.$fechainis[2] :
                                     $dataMes['mes_desc'].'/'.$fechainis[0]
                                    .'/'.$fechainis[2] ;
            
            $mes = $fechafin[1];
            $dataMes2 = $this->_mesObj->getMesNombre("$mes",$this->_sesion->lg);
            $this->view->fechafins2 = ($this->_sesion->lg=='ES')? $fechafin[0].' '
                                    .$dataMes2['mes_desc'].' '.$fechafin[2]:
                                     $dataMes2['mes_desc'].'/'.$fechafin[0]
                                    .$fechafin[2];
            //
            
            //$this->_fecObj->setFecha($this->_sesion->reserve_info['fecha_ini']);
            $this->_fecObj->setData($this->_sesion->reserve_info['fecha_ini']);
            $post['fecha_ini'] = $this->_fecObj->renders('save');

            //$this->_fecObj->setFecha($post['fec_dias']);
            $this->_fecObj->setData( $this->_sesion->reserve_info['fec_dias']);
            $post['fecha_fin'] = $this->_fecObj->renders('save');
            
        endif;
        
        $this->_sesion->cache['rooms'] = 'qphotel_rooms_' . $this->_sesion->su_id . 
                '_' . str_replace('-', '', $this->view->fechaini) .
                '_' . str_replace('-', '', $this->view->fechafin) . 
                '_' . $this->_sesion->reserve_info['num_adults'] . 
                '_' . $this->_sesion->lg;
        
        if(!$newdata = $this->_cache->load($this->_sesion->cache['rooms'])){
            $where = "t1.su_id='{$this->_sesion->su_id}'";
             $where .= " and t3.su_id = '{$this->_sesion->su_id}'";
            if(!empty($post['pro_tih_id'])):
                $where .= " and t1.tih_id = '{$post['pro_tih_id']}'";
            endif;
            $dataw = $this->_kardexObj->getDatosHabitTipoPubli(
                    array(
                    'where' => $where,
                    'order' => array('tih_orden','t3.im_id')
                    ), 
                    '',
                    $this->_sesion->reserve_info['num_adults']
            );

            $newdata = array();
            foreach ($dataw as $value) {
                $newdata[$value['tih_id']] = $value;
            }
            $i = 1;
            foreach ($newdata as $indice => $value) {
                $data = $this->_kardexObj->getReservaHabitacion($post, $this->_sesion, $indice);
                
                $promo = $this->_kardexObj->updateTarifaPromocion($post,$this->_sesion,$indice);
                //$promo = array('pro_dscto' => '50');
                
                if(empty($promo)):
                    $newdata[$indice]['hb_precio'] = $data['hb_precio'];
                    $newdata[$indice]['pro_id'] = '';
                    $newdata[$indice]['pro_name'] = '';
                else:
                    $newdata[$indice]['hb_precio'] = $data['hb_precio'] - $data['hb_precio'] * (float)$promo['pro_dscto'] / 100;
                    $newdata[$indice]['pro_id'] = $promo['pro_id'] . '*' . $promo['an_id']*17;
                    $newdata[$indice]['pro_name'] = $promo['pro_dscto'] . '%';
                endif;
                
                if (!empty($data))
                    $newdata[$indice]['cn'] = $data['count'];
                else
                    $newdata[$indice]['cn'] = 0;
                
                $i++;
            }
            $this->_cache->save($newdata, $this->_sesion->cache['rooms']);
        }
        
        $this->view->data = $newdata;
        $this->view->noches = $dias;

        //Listar las Promos
         $this->_sesion->cache['promo'] = 'qphotel_promos_' . $this->_sesion->su_id . 
                '_' . str_replace('-', '', $this->view->fechaini) .
                '_' . str_replace('-', '', $this->view->fechafin) . 
                '_' . $this->_sesion->reserve_info['num_adults'];
         
        if(!$promos = $this->_cache->load($this->_sesion->cache['promo'])){
            $this->_fecObj->setData(date('d/m/Y'));
            $fecha = $this->_fecObj->renders('save');
            $promos = $this->_promObj->getListPromotions($fecha, $this->_sesion->su_id);
            
            $this->_cache->save($promos, $this->_sesion->cache['promo']);
        }
        $this->view->promotions = $promos;
       // Zend_Debug::dump($this->view->data);exit;
        $dtaLista = $this->_dbImagenesArticulo->getListaImagenes(array('art_id' => '00000012', 'cat_id' => '20110014', 'su_id' => $this->_sesion->su_id));
        $this->view->images = $dtaLista[0];       
    }

    public function cargarAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        
        $post = $this->getRequest()->getParams();
        //Promocion
        if(!empty($post['promo'])):
            list($promo['id'], $anio) = explode('*',$post['promo']);
            $promo['anio'] = $anio/17;
            $sql = $this->_promObj->fetchRow("pro_id = '{$promo['id']}' and (an_id = '{$promo['anio']}' or (extract(year from pro_fecini)) = '{$promo['anio']}')");
            if(empty($sql)){
                echo Cit_Init::config()->subdomain . '/proceso-reserva';
                exit;
            }else{
                $this->_sesion->promo = $sql->toArray();
            }
        endif;
        
        $data = $this->_tarifaObj->getPrecioHabitacion($post, $this->_sesion->reserve_info,$promo);
        $this->_sesion->reserve_info['hb_id'] = $data['hb_id'];
        $this->_sesion->reserve_info['img'] = $_POST['img'];
        $rnd = rand(0, 1000000);
        //echo substr($_POST['monto'][$indice],1,strlen($_POST['monto'][$indice]));exit;
        $rnd = str_pad($rnd, 6, '0', STR_PAD_LEFT);        
        $desc = $this->_sesion->reserve_info['num_adults'] . '**' . $this->_sesion->reserve_info['fecha_ini'] . '**' . $this->_sesion->reserve_info['fecha_fin'] .
                '**' . $this->_sesion->reserve_info['dias'] . '**' .$this->_sesion->reserve_info['img']. '**' . $post['id'] . '**' .
                $this->_sesion->reserve_info['ho_id'] . '**' . $this->_sesion->reserve_info['num_adults'];
        //Descontando la promocion
        $data['hb_precio'] = $data['hb_precio'] - $data['promo'];
        $pd = new Core_Store_Product($rnd, $post['desc'], $desc, round($data['hb_precio'], 2), null);
        $item = new Core_Store_Cart_Item($pd, 1);
        $carrito->addCart($item);
        
        //Agregando Promocion ------------------------------------------ -+-
        if($data['promo'] > 0):
            $id = 'P**' . $promo['id'] . '**' . $promo['anio'];
            $pr = new Core_Store_Product($id, '', $data['promo'], 0,$data['promo']);
            $items = new Core_Store_Cart_Item($pr,1);
            $carrito->addCart($items); 
        endif;
        
        $this->_sesion->reserve_info['tih_id'] = $data['tih_id'];
        echo Cit_Init::config()->subdomain . '/proceso-reserva-one';
        /* Antiguos ------ -+
          foreach ($_POST['tipos'] as $indice => $value) {
          if (!empty($value)) {
          //$_POST['monto'][$indice] = round($_POST['monto'][$indice],2);
          $rnd = rand(0, 1000000);
          //echo substr($_POST['monto'][$indice],1,strlen($_POST['monto'][$indice]));exit;
          $rnd = str_pad($rnd, 6, '0', STR_PAD_LEFT);
          //echo var_dump($value);exit;
          $desc = $value . '**' . $this->_sesion->reserve_info['fecha_ini'] . '**' . $this->_sesion->reserve_info['fecha_fin'] . '**' . $this->_sesion->reserve_info['dias'] .
         * '**' . $_POST['imagen'][$indice] . '**' . $_POST['tih_id'][$indice] . '**' . $this->_sesion->reserve_info['ho_id'] . '**' . $this->_sesion->reserve_info['num_adults'];
          $pd = new Core_Store_Product($rnd, $_POST['tih_desc'][$indice], $desc, round(substr($_POST['monto'][$indice], 1, strlen($_POST['monto'][$indice])), 2), null);
          $item = new Core_Store_Cart_Item($pd, 1);
          $carrito->addCart($item);
          }
          }
          //var_dump($carrito);exit;
         */
        //$this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-one');
    }

    public function boxdetailAction() {
        $this->_helper->layout->disableLayout();
        $id = $this->getRequest()->getParam('id', '');
        $proid = $this->getRequest()->getParam('pro', '');
        if(!empty($proid)):
            list($promo['pro_id'], $anio) = explode('-',$proid);
            $promo['an_id'] = $anio/17;
        endif;
        $this->view->info = $this->_sesion->datahotel[0];

        $data = $this->_kardexObj->getDetailRooms($this->_sesion->reserve_info, $id, $this->_sesion->lg, $promo);

        $tipoHab = $this->_tipohabObj->fetchRow(array('tih_id = ?' => $id, 'id_id =?' => $this->_sesion->lg));
        $this->view->nameRoom = ucwords(strtolower($tipoHab['tih_desc']));
        $this->view->detail = $data['detail'];
        $this->view->total = $data['total'];
        
        $this->view->fechaini = str_replace('-', '/', $this->_sesion->reserve_info['fecha_ini']);
        $this->view->fechafin = str_replace('-', '/', $this->_sesion->reserve_info['fecha_fin']);
        $this->view->dias = $this->_sesion->reserve_info['dias'];
        $this->view->adultos = $this->_sesion->reserve_info['num_adults'];
        $this->view->children = $this->_sesion->reserve_info['num_ninios'];
    }

    public function boximagesAction(){
        $this->_helper->layout->disableLayout();
        $id = $this->getRequest()->getParam('id', '');
        $select = $this->_helper->DBAdapter()->select();
        
        $select->from(array('t1' => 'ht_cms_imagenes'), array('im_image4'))
            ->joinleft(array('t5' => 'ht_hb_tipo_habitacion_imagen'), 't1.im_id = t5.im_id', 
                array(''))
            ->where('tih_id = ?', $id)
            ->where('su_id = ?', $this->_sesion->su_id)
            ->order('t1.im_id');
            //->limit(3);
        //echo $select; exit;
        $dataimagen = $select->query()->fetchAll();
        $this->view->images = $dataimagen;
        
    }
}
