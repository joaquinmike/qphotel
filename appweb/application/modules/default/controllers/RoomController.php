<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class RoomController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;
    private $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
    }

    public function indexAction() {
        $interno = $this->getRequest()->getParam('interno', 0);
        $tih_id = $this->getRequest()->getParam('tih', 0);
        $select = $this->_helper->DBAdapter()->select();
        $dtaInterno = $this->_citObj->getDataGral('vht_cms_menu_interno', array('cat_id', 'cat_titulo', 'cat_desc'), "cat_id = '{$interno}' and id_id = '{$this->_sesion->lg}'", 'U');
        $this->view->interno = $dtaInterno;
        //$items = $this->_citObj->getDataGral('vht_hb_tipo_habitacion', array('tih_id','tih_desc','tih_coment'),"id_id = '{$this->_sesion->lg}' and tih_id = '{$tih_id}'",'U');
        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'))//
                ->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('tih_desc', 'tih_coment'))
                ->joinleft(array('t3' => 'ht_hb_idioma_suc_tiphab'), 't1.su_id = t3.su_id and t1.tih_id = t3.tih_id and t2.id_id = t3.id_id', array('contenido' => 'tih_desc'))
                ->where("t2.id_id = '{$this->_sesion->lg}' and t1.su_id = '{$this->_sesion->su_id}'  and t1.tih_id = '{$tih_id}' and cat_id = '$interno'")
                ->limit(1);
        $dtaHab = $select->query()->fetchAll();
        $items = Array();
        foreach ($dtaHab as $value):
            if (!empty($value['contenido']))
                $items['tih_coment'] = $value['contenido'];
            else
                $items['tih_coment'] = $value['tih_coment'];
            
            $items['tih_desc'] = $value['tih_desc'];
            $items['tih_id'] = $value['tih_id'];
        endforeach;

        $items['contenido'] = str_replace("\\", '', $value['contenido']);
        $this->view->headTitle($this->_sesion->datahotel[0]['su_reserva'] . ' - ' . $items['tih_desc']);
        $this->view->habitacion = $items;
        $html = '';

        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);

        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);

        if ($this->_sesion->su_id == '00000001') {
            $this->view->hotel = '00000002';
        } else {
            $this->view->hotel = $this->_sesion->su_id;
        }

        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);
        /* $dtaLista = $this->_citObj->getDataGral('vht_hb_tipo_habitacion', array('cat_id','tih_desc','tih_coment'),"cat_id = '{$interno}'",'U');
          foreach($dtaLista as $value):
          switch ($value['tih_id']):
          case $tih_id:
          $html = ' <li>'.$value['tih_desc'].'</li>';
          break;

          default:
          $html = ' <li><a href="/room_type/lima/show/en/Complimentary+bottle+of+Champagne+upon+arrival+">'.$value['tih_desc'].'</li>';
          break;
          endswitch;
          endforeach; */
        $this->view->lista_tih = $html;
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}