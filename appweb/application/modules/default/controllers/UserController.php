<?php
/**
 * LoginController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class UserController extends Zend_Controller_Action {
    
    private $_sesion;
    
    function init() {
        $this->_sesion = new Zend_Session_Namespace('web');
    }
    
    public function loginAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $objUser = new DbHtSegUsuario();
        try{
            if ($this->_request->isPost()) {
                $data = $this->getRequest()->getParams();
                //$citObj = new CitDataGral();
                //$pass = $citObj->getDataGral('ht_seg_usuario', array('us_password'), "us_login = '{$data['us_id']}'", 'U');
                $dtaUser = $objUser->fetchRow(array('us_login =?' => $data['us_id']));
                if (!empty($dtaUser['us_password'])):
                    if (md5($data['us_pass']) == $dtaUser['us_password'] and !empty($data['us_id'])):
                        $select = $this->_helper->DBAdapter()->select();
                        $select->from(array('t1' => 'ht_seg_usuario'), array('us_login', 'us_id', 'us_puntos'));
                        $select->join(array('t2' => 'ht_seg_persona_hotel'), 't1.pe_id = t2.pe_id and t1.ho_id = t2.ho_id', array(''));
                        $select->join(array('t3' => 'ht_seg_persona'), 't1.pe_id = t3.pe_id', array('pe_id', 'pe_nomcomp'));
                        $select->where('t1.ho_id =?', htID);
                        $select->where('us_login =?', $data['us_id']);
                        $select->where('us_password =?', md5($data['us_pass']));
                        $select->where('per_id =?', '0003');
                        //echo $select; exit;
                        $dtaLogin = $select->query()->fetch();

                        $this->_sesion->penomcomp = $dtaLogin['pe_nomcomp'];
                        $this->_sesion->uslogin = ucfirst($dtaLogin['us_login']);
                        $this->_sesion->peid = $dtaLogin['pe_id'];
                        $this->_sesion->usid = $dtaLogin['us_id'];
                        $this->_sesion->uspuntos = $dtaLogin['us_puntos'];
                        $this->_sesion->clid = $dtaLogin['cl_id'];
                        
                        $action  = array('action' => 1, 'message' => 'Good.');
                    else:
                        $action  = array('action' => 0, 'message' => Cit_Init::_('LOGIN_FAIL'));
                    endif;
                else:
                    $action  = array('action' => 0, 'message' => Cit_Init::_('LOGIN_FAIL'));
                endif;

            }
            echo json_encode($action);
        }  catch (Exception $e){
            echo $e->getMessage(); exit;
        }
        
    }
}