<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $errors = $this->_getParam('error_handler');

        if (!$errors) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:

                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'La pagina no existe';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $this->view->message = 'Application Error';// $errors->exception
                break;
        }
            $data = array('params'=>$errors->request->getParams(),
                'mensaje' => $errors->exception->getMessage(),
                'traza'=>$errors->exception->getTraceAsString());
            ob_start();
            var_dump($data);
            $mensaje = ob_get_contents();
            ob_end_clean();
            Zend_Registry::get('Log')->log($mensaje, Zend_Log::CRIT);

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        Zend_Mail::setDefaultTransport($transport);
        //Initialize needed variables
        $name_from = 'Error Qphotel';
        $name = $this->getRequest()->getControllerName();
        $asunto_from = utf8_decode('QP - ' .$name);
        //$email_from = $data['us_email'];
        //$user = md5('CD' . $data['us_usuario']);

        $mail = new Zend_Mail();

        $email_to = 'perucit@gmail.com';
        $name_to = 'perucit@gmail.com';

        //$mail->setReplyTo($email_from, $name_from);
        $html = '<p>' . 
                'Fecha : ' . date('d/m/Y')
                . '</p>';
        $html .= $errors->exception;
        $mail->setFrom('perucit@gmail.com', $name_from)
            ->addTo('perucit@gmail.com', $name_to)
            ->setSubject($asunto_from)
            ->setBodyHtml($html)
            ->send();

        //$this->view->request = $errors->request;

    }

    public function getLog()
    {
        $bootstrap = $this->getInvokeArg('bootstrap');


        return $log;
    }

}

