<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class ProcesoReservaTwoController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_sesionCard;
    public $_idioma;
    public $_hotel;
    private $_fecha;
    private $_kardexObj;
    private $_dbImagenesArticulo;
    private $_aniObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');        
        $this->view->headTitle($this->_sesion->datahotel[0]['su_reserva'] . ' - ' . Cit_Init::_('PAGE_RESERVA_III'));
        $this->_hotel = new DbHtHbSucursal();
        $this->_fecha = new Cit_Db_CitFechas();
        $this->_kardexObj = new DbHtResKardex();
        $this->_aniObj = new DbHtResAnio();
        $this->_dbImagenesArticulo = new DbCmsImagenesArticulo();
    }

    public function indexAction() {
        $this->view->usid = $this->_sesion->usid;
        $this->view->idioma = $this->_sesion->lg;

        $this->view->suc = $this->_sesion->datahotel;
        $this->view->reserve_info = $this->_sesion->reserve_info;

        $this->view->fecini = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_ini'], 'R', $this->_sesion->lg);
        $this->view->fecfin = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_fin'], 'R', $this->_sesion->lg);

        $this->view->usid = $this->_sesion->usid;
        $this->view->idioma = $this->_sesion->lg;
        //$this->view->cboDpto = $this->_helper->Combos->html('ht_seg_ubigeo', "substr(ub_id,3,4)='0000'", array('id' => 'ub_id', 'desc' => 'ub_desc'));
        $this->view->cboPais = $this->_helper->Combos->html('vtabla_detalle', "tba_id = '022' and id_id = '{$this->_sesion->lg}'", array('id' => 'tad_id', 'desc' => 'tad_desc'));
        $this->view->cboTarjetas = $this->_helper->Combos
            ->html('vtabla_detalle', "tba_id = '012' and id_id = '{$this->_sesion->lg}'"
            , array('id' => 'tad_id', 'desc' => 'tad_desc'));

        $anio = date('Y');
        $select = $this->_helper->DBAdapter()->select();
        $select->from(array('t1' => 'ht_res_anio'), array('an_id', 'an_abr'))
                ->where("an_id >= '{$anio}' ")
                ->limit(4);
        $dtaanio = $select->query()->fetchall();

        $this->view->cboAnio = $this->_helper->Combos->html($dtaanio, '', array('id' => 'an_id', 'desc' => 'an_abr'));
        $this->view->cboMeses = $this->_helper->Combos->html('vht_cms_meses', "id_id = '{$this->_sesion->lg}'", array('id' => 'mes_id', 'desc' => 'mes_desc'));

        //$this->view->cboTarjetas = $this->_helper->Combos->html('vtabla_detalle', "tba_id = '012' and id_id = '{$this->_sesion->lg}'", array('id' => 'tad_id', 'desc' => 'tad_desc'));

        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        $this->view->data = $carrito->getContents()->getIterator();

        $this->view->total = $carrito->getTotal();
        $service = array();
        foreach ($carrito->getContents()->getIterator() as $value) :
            if (strpos($value->getId(), '**')){
                $datos = explode('**', $value->getId());
                if($datos[0] != 'P'){
                    $service[] = array(
                        'id' => $datos[1],
                        'name' => ucfirst(strtolower($value->getName())),
                        'price' => $value->getPrice()
                    );
                }
            }else{
                $reserva = $this->_kardexObj->getPromocionDias($value->getId(), 'new');
            }
        endforeach;
        $this->view->services = $service;
        //var_dump($reserva); exit;
        $this->view->detalle = $reserva;
        
        $dtaLista = $this->_dbImagenesArticulo->getListaImagenes(array('art_id' => '00000012', 'cat_id' => '20110014', 'su_id' => $this->_sesion->su_id));
        $this->view->images = $dtaLista[0];    
                          
        $sessionCard = new Zend_Session_Namespace('creditCard');        
        if(count($sessionCard->getIterator())>1){
            $this->view->dataOld=array(
                'pe_direccion'=>$sessionCard->pe_direccion
                ,'pe_nombre'=>$sessionCard->pe_nombre
                ,'pe_apellidos'=>$sessionCard->pe_apellidos
                ,'pe_email'=>$sessionCard->pe_email
                ,'pe_ciudad'=>$sessionCard->pe_ciudad
                ,'pe_pais'=>$sessionCard->pe_pais
                ,'phone_number'=>$sessionCard->phone_number
                ,'res_tarj_id'=>$sessionCard->res_tarj_id
                ,'pe_postal'=>$sessionCard->pe_postal
                ,'msjError'=>$sessionCard->msjError
            );            
        }else{
            $this->view->dataOld=array(
                'pe_direccion'=>''
                ,'pe_nombre'=>''
                ,'pe_apellidos'=>''
                ,'pe_email'=>''
                ,'pe_ciudad'=>''
                ,'pe_pais'=>''
                ,'phone_number'=>''
                ,'res_tarj_id'=>''
                ,'pe_postal'=>''
                ,'msjError'=>'');
        }
        Zend_Session::namespaceUnset('creditCard');
    }

    public function confirmationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if ($this->_request->isPost()) :
           
            $post = $this->getRequest()->getParams();             
            $datos=array('id_id'=>$this->_sesion->lg,'tba_id'=>'012'
                ,'tad_id'=>$post['res_tarj_id']);
            $DbIdiomaTablaDet = new DbIdiomaTablaDet();
            $tarjetaData = $DbIdiomaTablaDet->getDataId2($datos);            
            $post['res_nomb_tarj'] = $tarjetaData[0]['tad_desc'];
            //$post['res_cod_tarj'] = '321';            
            $post['id_id'] = $this->_sesion->lg;            
            $cardCreditValid= new Core_Utils_CreditCard();
            if($cardCreditValid->checkCreditCard($post["res_numero_tarj"]
                ,$post['res_nomb_tarj'],$this->_sesion->lg,$errornumber,$errortext)){                                
            }else{                               
                //$this->_sesionCard->setExpirationSeconds(900);
                $sesionCard = new Zend_Session_Namespace('creditCard');
                $sesionCard->setExpirationSeconds(380);
                $sesionCard->pe_nombre=$post['pe_nombre'];
                $sesionCard->pe_apellidos=$post['pe_apellidos'];
                $sesionCard->pe_email=$post['pe_email'];
                $sesionCard->pe_direccion=$post['pe_direccion'];
                $sesionCard->pe_ciudad=$post['pe_ciudad'];
                $sesionCard->pe_postal=$post['pe_postal'];
                $sesionCard->pe_pais=$post['pe_pais'];
                $sesionCard->phone_number=$post['phone_number'];
                $sesionCard->res_tarj_id=$post['res_tarj_id'];
                $sesionCard->msjError=$errortext;
                
                $this->_redirect('/proceso-reserva-two/');
            };
            $reserva = new DbHtResReserva();
            $result = $reserva->saveData($post);
            switch ($result['action']):
                case 2:
                    echo '<p>Usuario no esta permitido hacer reservas</p>';
                    break;
                case 'failSend':
                    $this->view->sendError = true;
                    break;
                case 1:                                        
                    $dataMail['data_hotel']=$this->_sesion->datahotel;
                    $dataMail['reserve_info']=$this->_sesion->reserve_info;                   
                    $dataMail['newUser']=$post;
                    $dataMail['newDataUser']=$result;
                    $this->sendMailPostReserva($dataMail);
                    if(!empty($result['session'])):
                        $this->_sesion->penomcomp = $result['session']['pe_nomcomp'];
                        $this->_sesion->us_login = $result['session']['us_login'];
                        $this->_sesion->pe_id = $result['session']['pe_id'];
                        $this->_sesion->us_id = $result['session']['us_id'];
                    endif;
                    $this->_redirect( Cit_Init::config()->subdomain.'/receipt/index/res/' . $result['res_id']); 
                    die();
                    break;
            endswitch;
        endif;
    }

    // Antiguos ----------------------------------------- -+-
    
    public function valoresagregadosAction() {
        $suid = $this->getRequest()->getParam('suid', 0);
        $data = explode('**', $suid);
        $str = '';
        foreach ($data as $value) {
            if (!empty($value)) {
                $str.="'$value',";
            }
        }
        $str = substr($str, 0, strlen($str) - 1);
        //var_dump($_SESSION);
        $select = $this->_helper->DBAdapter()->select();
        $select->from(array('t1' => 'vht_hb_accesorios'), array('*'));
        $select->join(array('t2' => 'ht_hb_accesorio_sucursal_tiphab'), 't1.ac_id=t2.ac_id', '');
        $select->join(array('t3' => 'vht_hb_tipo_habitacion'), 't2.tih_id=t3.tih_id', array('tih_desc', 'tih_id'));
        $select->where("t1.id_id='{$this->_sesion->lg}' and t2.su_id='{$this->_sesion->reserve_info['ho_id']}'");
        $select->where("t2.tih_id in ($str) and t3.id_id='{$this->_sesion->lg}'");
        $select->order('t2.tih_id');

        $this->view->data = $select->query()->fetchall();
    }

    public function addserviciosAction() {
        $dtaSer = $this->_serviciObj->getServicioSucursal($this->_sesion->reserve_info['ho_id']);
        $this->view->data = $dtaSer;
    }

    public function puntosAction() {
        $bonos = new DbHtResBonos();
        $data = $bonos->fetchAll()->toArray();
        $puntos = $data[0]['bo_puntos'];
        $dolar = $data[0]['bo_dolar'];
        $razon = $dolar / $puntos;
        $this->_sesion->bonus = $razon * $_POST['us_puntos'];
        $this->_sesion->puntos = $_POST['us_puntos'];

        $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-two');
    }

    public function promocionAction() {
        $date = date('d/m/Y');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $select->from(array('t1' => 'vht_cms_promocion'), array('*'));
        $select->where("pro_id='{$_POST['pro_id']}' and id_id='{$this->_sesion->lg}'"); // and pro_fecini>'$date' and pro_fecfin<'$date'");	
        //echo $select; exit;
        $data = $select->query()->fetchAll();
        if (!empty($data)):
            $this->_sesion->pro_id = $_POST['pro_id'];
            $this->_sesion->pro_anio = $_POST['an_id'];
            $this->_sesion->pro_dscto = $data[0]['pro_dscto'];
            $this->_fecObj->setData($data[0]['pro_fecini']);
            $this->_sesion->pro_ini = $this->_fecObj->renders('open');

            $this->_fecObj->setData($data[0]['pro_fecfin']);
            $this->_sesion->pro_fin = $this->_fecObj->renders('open');

            $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');


            $dtaPromo = $this->_kardexObj->getPromocionDias2($data[0], 'sum');
            $this->_sesion->pro_monto = $dtaPromo['monto'];
        endif;
        $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-two');
    }

    public function reservarAction() {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $reserva = new DbHtResReserva();
        $post = $this->getRequest()->getParams();
        $result = $reserva->saveData($post);
        if ($result) {
            $this->_redirect(Cit_Init::config()->subdomain . '/receipt');
        }
    }

    public function boxdetalleAction() {
        $id = $this->getRequest()->getParam('id', 0);

        $name_dia = array(1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado', 7 => 'Domingo');
        $this->_helper->layout->disableLayout();

        $dtaPromo = $this->_kardexObj->getPromocionDias($id);

        //var_dump($dtaPromo);
        $this->view->data = $dtaPromo;
    }
    public function sendMailPostReserva($data = Array()){
        
        $nombre = $data['newUser']['pe_nombre'].' '.$data['newUser']['pe_apellidos'];
        $asunto = Cit_Init::_('CONFIRM_RESERVA');
        $email = $data['newUser']['pe_email'];
        
        $smtpHost = 'smtp.gmail.com';
        $smtpConf = array(
            'auth' => 'login',
            'ssl' => 'ssl',
            'port' => '465',
            'username' => 'perucit@gmail.com',
            'password' => 'perucit2012'
        );
        $transport = new Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        
        try {
            $mail = new Zend_Mail(); 
            $email_to = $email;
            $name_to = "Qp Hotels";
            $this->_objReserve = new DbHtResReserva();
            $datareserva = $this->_objReserve->fetchRow(array('res_id = ?' =>$data['newDataUser']['res_id']))->toArray();
            
            $content = utf8_decode('<table align="center" width="631" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                <tr>
                    <td width="469" height="180" valign="top">
                    <img src="'.Cit_Init::config()->subdomain.'/img/email/firma.jpg" width="469" height="180"></td>
                    <td width="4" valign="top">
                    <p>&nbsp;</p></td>
                    <td width="150" valign="top"><br>
                    <br>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                    '.$data['data_hotel'][0]['su_direccion'].'-'.$data['data_hotel'][0]['su_nombre'].'</span><br>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                        '.$data['data_hotel'][0]['su_telefono1'].'</span><br>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px">
                        '.$data['data_hotel'][0]['su_telefono2'].'</span><br>
                    <a href="'.Cit_Init::config()->subdomain.'">'.Cit_Init::config()->subdomain.'</a></td>
                </tr>
                <tr>
                    <td height="17" rowspan="8">
                        <img src="'.Cit_Init::config()->subdomain.'/img/'.$this->urlImg($data['reserve_info']['img']).'" 
                         style="width: 475px; height: 413px;"/>
                    </td>
                    <td rowspan="2" bordercolor="#FFFFFF" bgcolor="#FFFFFF">&nbsp;    	 </td>
                    <td height="17" bordercolor="#E84828" bgcolor="#E42924">&nbsp;</td>
                </tr>
                <tr>
                    <td height="85" valign="middle" bordercolor="#E84828" bgcolor="#E84828">
                    <div align="center" style="color: #FFFFFF; font-family: Arial, Helvetica, sans-serif;
                    font-weight: bold;">Descanse</div>    </td>
                </tr>
                <tr>
                    <td rowspan="2" valign="top" bordercolor="#E6DDCE" bgcolor="#FFFFFF">        </td>
                    <td height="17" valign="top" bordercolor="#E6DDCE" bgcolor="#E6DDCE"></td>
                </tr>
                <tr>
                    <td height="84" valign="middle" bordercolor="#CAC0B4" bgcolor="#CAC0B4">
                        <div align="center" style="color: #A02D28; font-family: Arial, Helvetica, sans-serif;
                        font-weight: bold;">Disfrute</div>    </td>
                </tr>
                <tr>
                    <td rowspan="2" bordercolor=""cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">&nbsp;  </td>
                    <td height="17" bordercolor="#4D3E37" bgcolor="#4D3E37">&nbsp;</td>
                </tr>
                <tr>
                    <td height="83" bordercolor="#7E7673" bgcolor="#7E7673" valign="middle">
                    <div align="center" style="color: #FFFFFF;
                            font-weight: bold;
                            font-family: Arial, Helvetica, sans-serif;">
                            Explore         </div>       </td>
                </tr>
                <tr>
                    <td rowspan="2" bordercolor="#EA612D" bgcolor="#FFFFFF">         </td>
                    <td height="17" bordercolor="#EA612D" bgcolor="#EA612D"></td>
                </tr>
                <tr>
                    <td height="87" valign="middle" bordercolor="#EB7F59" bgcolor="#EB7F59">
                    <div align="center" style="color: #A02D28;
                            font-weight: bold;
                            font-family: Arial, Helvetica, sans-serif;">
                            Negocie        </div>        </td>
                </tr>

                <tr>
                    <td height="44" colspan="3">
                    </td>
                </tr>
                <tr>
                    <td height="18" colspan="3"><div align="center"></div></td>
                </tr>

                </table>
                <br>
                 '.Cit_Init::_('REG_EMAIL_WELCOME').'
                <br>
                <table align="center" width="631" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                <tr>
                <td width="20" >&nbsp;</td>
                <td colspan="2" align="center">'.Cit_Init::_('CONFIRM_NUMBER').
                    ' '.$data['newDataUser']['res_id'].' </td>
                <td width="20"></td>
                <td width="21"></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td width="200">'.Cit_Init::_('REGISTER_BOOK').'</td>
                <td width="358" align="center">&nbsp;</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>'.Cit_Init::_('CONFIRM_ARRIVAL').'</td>
                <td >'.$data['reserve_info']['fecha_ini'].'</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>'.Cit_Init::_('CONFIRM_DEPARTURE').'</td>
                <td >'.$data['reserve_info']['fecha_fin'].'</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>N° '. Cit_Init::_('PRO_GRID_ROOM').'</td>
                <td >1</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>'.Cit_Init::_('PRO_ADULTS').'</td>
                <td >'.$data['reserve_info']['num_adults'].'</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>'.Cit_Init::_('PRO_ADULTS').'</td>
                <td >N° '.Cit_Init::_('RES_CHILDREN').'</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>'.Cit_Init::_('PRO_TWO_DESCRIPTION').'</td>
                <td >&nbsp;</td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td>Total</td>
                <td aling="center"> USD$ '.number_format($datareserva['res_total'],2).'</td>
                <td > </td>
                <td></td>
                </tr>
                <tr>
                <td >&nbsp;</td>
                <td colspan="2"></td>
                <td></td>
                <td></td>                
                </table>
                <br /><br />
                <table align="center" width="631" border="0" cellpadding="0" cellspacing="0" bordercolor="#FFFFFF">
                <tr>
                <td width="20" >&nbsp;</td>
                <td colspan="2" align="center">'.Cit_Init::_('CONFIRM_HORARIOS').'</td>
                <td width="20"></td>
                <td width="21"></td>
                </tr>
                <tr>
                    <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td align="center" >
                        <table width="300" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="74" height="27">Destination</td>
                            <td width="111" align="center">Check-in</td>
                            <td width="107" align="center">Check-out</td>
                        </tr>
                        <tr>
                            <td>Lima</td>
                            <td align="center"> 14:00</td>
                            <td align="center">12:00</td>
                        </tr>
                        <tr>
                            <td>Arequipa</td>
                            <td align="center">12:00</td>
                            <td align="center">12:00</td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                    <td >&nbsp;</td>
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td></td>
                <td></td>
                </tr>
                </table>
 <br />
 <table width="631" cellspacing="0" cellpadding="0" bordercolor="#FFFFFF" border="0" align="center">
    <tbody>    
    <tr>
        <td>&nbsp;</td>
        <td><p align="justify"><strong>Políticas de  Check in &amp; Check out:</strong><br>
          <br>
          
          Early Departures: Se cobrara el 100%  de la tarifa otorgada<br>
          Late Check out: Sin cobro hasta las  15:00 horas previa coordinación, se cobrara el 50% de la tarifa otorgada hasta  las 18:00 horas, se cobrará el 100% de la tarifa después de las 18:00 horas,  sujeto a variación sin previo aviso de acuerdo a disponibilidad.<br>
          <br>
  <strong>Nuestras  tarifas incluyen:</strong></p>
          <ul>
            <li>Desayuno Buffet servido en  nuestro Café Lounge de 07:00 a 10:00 horas.</li>
            <li>WI-FI en nuestras  instalaciones y línea dedicada en las habitaciones.</li>
            <li>Uso de las instalaciones  de nuestro gimnasio y piscina con hidromasajes.</li>
            <li>Cocktail de Bienvenida.</li>
            <li>Atención de Emergencias  las 24 horas.</li>
            <li>Descuentos y promociones  para largas estadías.</li>
          </ul>
          <p align="justify"><strong>Políticas de  pago:</strong><br>
            <br>
            Las tarifas mencionadas son  confidenciales y están sujetas al 19% I.G.V, validas para una o dos personas y  están expresadas en dólares americanos. De acuerdo a D.L personas naturales no  residentes están exoneradas de par el I.G.V presentando su pasaporte vigente  y/o carné de extranjería con su tarjeta de migración debidamente sellada TAM y  su permanencia en el país no exceda los 60 días. <br>
            <br>
  <strong>Términos y  Condiciones:</strong></p>
          <ul>
            <li>Tarjetas de Crédito:  Aceptamos pagos en efecto o con las siguientes tarjetas de crédito: VISA,  MASTERCARD, AMERICAN EXPRESS, DINERS CLUB.</li>
            <li>Política de niños: Solo se  permite un niño (igual o menor a 6 años de edad) compartiendo habitación con  dos adultos de manera gratuita.</li>
            <li>Anulación: No se penalizan  si se realizan 48 horas antes del Check-in de su reserva. Como penalidad se  cargaran el total de las habitaciones y noches anuladas que estén dentro de las  48 horas siguientes a la fecha de anulación. </li>
          </ul>
          <p align="justify"><strong>Políticas de  No Show:</strong><br>
            <br>
            Toda anulación de reservas deberá  realizarse con 48 horas de anticipación y por escrito, caso contrario se  procederá a facturar la primera noche de alojamiento por concepto de <em>NO SHOW</em><strong>. </strong>Si la reserva fuese anulada con 24 horas de anticipación, se  cargará el 50% de la primera noche; si la anulación se hace el mismo día, se  cargara el 100% de la primera noche.<br>
          </p>
  <strong>Política de Transfer:</strong>
          <p align="justify">Las tarifas incluyen en los cargos los posibles  peajes. <br>
            Se realizará un cargo adicional de un 25% por hora de  espera. Con respecto al tiempo de espera, este comenzará después de los 30  minutos del recojo programado.</p>
          <p><strong>Otras Políticas:</strong></p>
          <p>No esta permitido el ingreso de mascotas.<br>
        Todas las habitaciones del hotel son de No-Fumadores.</p></td>
      <td></td>
        <td></td>
    </tr>
    <tr>
            <td width="20">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td width="20">&nbsp;</td>
            <td width="21"></td>
    </tr>
    <tr>
       <td>&nbsp;</td>
       <td>
                <strong>Con respecto a su privacidad </strong>
                      <div align="justify"><br>
                  Rogamos por favor que tenga en cuenta que por razones de seguridad, al registrarse deberá presentar un documento de identificación emitido por una autoridad pertinente, con fotografía.
                Validez de la Tarifa y la Reserva
                Tenga siempre en cuenta que nosotros le facilitamos las confirmaciones de reservas digitales única y exclusivamente para su mayor comodidad y practicidad. Es importante señalar que guardamos registros oficiales de nuestras transacciones de reservas, incluida la información de las fechas del hospedaje así como también las tarifas de las habitaciones. De ser el caso que ocurran algunas discrepancias, cambios, modificaciones, y/o variaciones entre la presente confirmación y nuestros registros oficiales, prevalecerán estos últimos. Manipular de alguna manera la presente confirmación con el fin de modificar la tarifa de habitación, o cualquier otro aspecto  concerniente a la reserva queda estrictamente prohibido y puede tener repercusiones legales.
                Finalmente, lo invitamos a que visite nuestras redes sociales para que pueda acceder a mucha mas información de nuestro hotel. Esperamos poder verlo ahí .               </div></td>
                <td></td>
                <td></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> </td>
                  <td></td>
                    <td></td>
                </tr>                
                <tr>
                    <td>&nbsp;</td>
                    <td>Visítenos en: </td>
                  <td></td>
                    <td></td>
                </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td></td>
                  <td></td>
                    <td></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                    <td><div align="left"><a target="_blank" href="http://www.facebook.com/qpHotelsPeru">
                      <img src="http://qphotel.com/img/hotel/images/facebook.png" width="21" height="20" border="0">
                      </a>
                      <a target="_blank" href="https://twitter.com/qphotelsperu">
                        <img src="http://qphotel.com/img/hotel/images/twitter.jpg" width="21" height="20" border="0">                        </a>
                      <a target="_blank" href="http://pinterest.com/qphotels">
                        <img src="http://qphotel.com/img/hotel/images/pinterest-logo.jpg" width="21" height="20" border="0">                                </a>                       
                       <a target="_blank" href="https://es.foursquare.com/v/qp-hotels/4cba2916a33bb1f768c68cfd">
                         <img src="http://qphotel.com/img/hotel/images/foursquare-logo.jpg" width="21" height="20" border="0">                                </a>
                       <a target="_blank" href="http://www.youtube.com/qphotelslima">
                         <img src="http://qphotel.com/img/hotel/images/youtube.jpg" width="21" height="20" border="0">                       </a> </div></td>
                  <td></td>
                    <td></td>
                </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><p style="font-size: 14px"><em>Este correo  electrónico es únicamente para emitir mensajes. Por favor, no responda este  mensaje.</em></p></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>
        
        </td>
        <td>&nbsp;</td>
      <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><div align="center"><img width="267" height="87" src="http://qphotel.com/img/email/firma.jpg"></div></td>
        <td></td>
        <td> </td>
    </tr>
    </tbody>
 </table>                 
 ');
            $mail->setReplyTo($email, $nombre);
            $mail->setFrom($email, $nombre);            
            $mail->addTo($email_to, $name_to);
            $mail->setSubject($asunto);
            $mail->setBodyHtml($content);
            //Send
            $sent = true;
            $mail->send($transport);
        } catch (Exception $e) {
            echo $e->getMessage();
            $sent = false;
        }
        return $sent;
    }
   public function urlImg($cad)
   {
       $ext= substr($cad,-(strlen($cad)-strrpos($cad,'.')));   
       return $urlImg= substr($cad,0,(strlen($cad)-strlen($ext))-1).'4'.$ext;
   }
}