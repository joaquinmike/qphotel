<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_usuarioObj;
    public $_sucursalObj;
    public $_sesion;
    public $_idioma;
    public $_citObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->view->headTitle('Welcome to QP Hotels Peru:: Quality Place Hotels :. Menu Principal');
        $this->_idioma = new DbHtCmsIdioma();
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        //$this->_colegioObj = new DbColegioSucursal();
    }

    public function indexAction() {
        unset( $this->_sesion->reserva_info);
        $categoria = $this->getRequest()->getParam('cat', 0);
        //$cboSucursal = $cboObj->html('ht_hb_sucursal',"ho_id = '".htID."' and su_portal = 'N'",array('id'=>'su_id','desc'=>'su_nombre'));
        //$this->view->data = $_POST;
        $this->view->cat_id = $categoria;
        /* $select = $this->_helper->DBAdapter()->select();
          $select->from(array('t1'=>'vht_cms_articulo_categoria_sucursal'),array('art_id','art_titulo'));
          $select->where("su_id = '00000001' and cat_id = '20110009' and art_est = '1'");
          $select->order('sca_orden');
          $select->limit(4);
          echo $select; exit;
          /*$dta = $select->query()->fetchAll();
          $html = '';
          foreach ($dta as $value):
          //<li><a class="highlight" href="/">Home</a></li>
          $html.= '<li><a class="highlight" href="#'.$value['art_id'].'">'.$value['art_titulo'].'</a></li>';
          endforeach;
          echo $html; exit; */

        //var_dump($this->_sesion->lg);
        $dtaDesc = $this->_citObj->getDataGral('ht_hb_sucursal', array('su_desc'), "su_id = '{$this->_sesion->su_id}'", 'U');

        $desc = str_replace('\"', '', $dtaDesc['su_desc']);
        $desc = str_replace('=""', '', $desc);

        $this->view->desc_home = $desc;
    }

}
