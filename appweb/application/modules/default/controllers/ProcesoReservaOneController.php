<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class ProcesoReservaOneController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_idioma;
    public $_hotel;
    private $_fecha;
    private $_kardexObj;
    private $_dbImagenesArticulo;
    private $_carrito;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->view->headTitle($this->_sesion->datahotel[0]['su_reserva'] . ' - ' . Cit_Init::_('PAGE_RESERVA_II'));
        $this->_personaObj = new DbHtSegPersona();
        $this->_dbImagenesArticulo = new DbCmsImagenesArticulo();
        $this->_hotel = new DbHtHbSucursal();
        $this->_fecha = new Cit_Db_CitFechas();
        $this->_kardexObj = new DbHtResKardex();
        $this->_carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
    }

    public function indexAction() {
        if ($this->_request->isPost()) :
            $post = $this->getRequest()->getParams();            
        else:
            $this->view->additional = $this->_sesion->reserva_price;
            
            //$this->_sesion->datahotel = $this->_hotel->fetchAll("su_id='{$this->_sesion->su_id}'")->toArray();
            $this->view->suc = $this->_sesion->datahotel;
            $this->view->reserve_info = $this->_sesion->reserve_info;            
            $this->view->fecini = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_ini'],'R',$this->_sesion->lg);
            $this->view->fecfin = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_fin'],'R',$this->_sesion->lg);
            $this->view->usid = $this->_sesion->usid;
            $this->view->idioma = $this->_sesion->lg;
            $this->view->cboPais = $this->_helper->Combos->html('vtabla_detalle', "tba_id = '022' and id_id = '{$this->_sesion->lg}'", array('id' => 'tad_id', 'desc' => 'tad_desc'));
            
            $this->_carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
            $this->_carrito->calculateTotals();
            $this->view->data = $this->_carrito->getContents()->getIterator();            
            foreach ($this->_carrito->getContents()->getIterator() as $value) :
                $reserva = $this->_kardexObj->getPromocionDias($value->getId(),'new');
                break;
            endforeach;
            $this->view->total = $this->_carrito->getTotal();
            $this->view->detalle = $reserva;
            if(!empty($this->_sesion->promo['pro_id']))
                $this->view->promo = $this->_sesion->promo['pro_id'] . '-' . $this->_sesion->promo['an_id'] * 17;
            else
                $this->view->promo = '';
            
        endif;
       
        $this->view->noches = $this->_sesion->reserve_info['dias'];
        $this->view->domain= Cit_Init::config()->subdomain;
        
        $dtaLista = $this->_dbImagenesArticulo->getListaImagenes(array('art_id' => '00000012', 'cat_id' => '20110014', 'su_id' => $this->_sesion->su_id));
        $this->view->images = $dtaLista[0];     
        
    }

    public function loginAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $data = $this->getRequest()->getParams();;
        if(empty($data['us_id'])):
            $data['us_id'] = $data['us_id_l'];
            $data['us_pass'] = $data['us_pass_l'];
        endif;
            
        if (!empty($data)) {
            $citObj = new CitDataGral();
            $select = $citObj->select()->setIntegrityCheck(false);
            $sesion = new Zend_Session_Namespace('web');
            $html = '';
            
            $pass = $citObj->getDataGral('ht_seg_usuario', array('us_password'), "us_login = '{$data['us_id']}'", 'U');
            if (!empty($pass)):
                if (md5($data['us_pass']) == $pass['us_password'] and !empty($data['us_id'])):
                    $select->from(array('t1' => 'ht_seg_usuario'), array('us_login', 'us_id', 'us_puntos'));
                    $select->join(array('t2' => 'ht_seg_persona_hotel'), 't1.pe_id = t2.pe_id and t1.ho_id = t2.ho_id', array(''));
                    $select->join(array('t3' => 'ht_seg_persona'), 't1.pe_id = t3.pe_id', array('pe_id', 'pe_nomcomp'));
                    $select->where('t1.ho_id =?', htID);
                    $select->where('us_login =?', $data['us_id']);
                    $select->where('us_password =?', md5($data['us_pass']));
                    $select->where('per_id =?', '0003');
                    //echo $select; exit;
                    $dtaLogin = $citObj->fetchAll($select)->toArray();

                    foreach ($dtaLogin as $value):
                        $sesion->penomcomp = $value['pe_nomcomp'];
                        $sesion->uslogin = ucfirst($value['us_login']);
                        $sesion->peid = $value['pe_id'];
                        $sesion->usid = $value['us_id'];
                        $sesion->uspuntos = $value['us_puntos'];
                        $sesion->clid = $value['cl_id'];
                    endforeach;

                endif;

            else:
                $this->_redirect('/proceso-reserva-one');
            endif;
        }
        $this->_redirect('/proceso-reserva-one');
    }
    
    public function addservicesAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        if($this->_sesion->reserva_price['sum'] > 0):
            foreach ($this->_sesion->reserva_price['item'] as $items):
               $this->_carrito->remove($items);
            endforeach;
        endif;
       
        if ($this->_request->isPost()) :
            $sum = 0; $id = array(); $add = array();
            $post = $this->getRequest()->getParams();
            foreach ($post['additional'] as $item => $value):
                $select = $this->_helper->DBAdapter()->select();
                $select->from(array('t1' => 'vht_ser_servicio'),array('ser_id','ser_titulo','ser_precio'))
                    ->where('ser_id =?',$item)
                    ->where('id_id =?', $this->_sesion->lg);
                $data = $select->query()->fetch();
                $sum = $sum + $data['ser_precio'];
                $id[] = $data['ser_id'];
                $add[] = 'S**' . $data['ser_id'];
                $pd = new Core_Store_Product('S**'.$data['ser_id'],$data['ser_titulo'],'',$data['ser_precio'],null);
                $item = new Core_Store_Cart_Item($pd,1);
                $this->_carrito->addCart($item); 
            endforeach;
            foreach ($post['amenities'] as $items => $values):
                $select = $this->_helper->DBAdapter()->select();
                $select->from(array('t1' => 'vht_hb_accesorios'),
                        array('ac_id','ac_desc', 
                            'ac_precio' => new Zend_Db_Expr('ac_precio * ' . $this->_sesion->reserve_info['dias'])))
                    ->where('ac_id =?',$items)
                    ->where('id_id =?', $this->_sesion->lg);
                $data = $select->query()->fetch();
                $sum = $sum + $data['ac_precio'];
                $id[] = $data['ac_id'];
                $add[] = 'A**' . $data['ac_id'];
                $pd = new Core_Store_Product('A**'.$data['ac_id'],$data['ac_desc'],'',$data['ac_precio'],null);
                $item = new Core_Store_Cart_Item($pd,1);
                $this->_carrito->addCart($item); 
                
            endforeach;
            $this->_sesion->reserva_price['sum'] = $sum;
            $this->_sesion->reserva_price['id'] = $id;
            $this->_sesion->reserva_price['item'] = $add;
            $this->_carrito->calculateTotals();
            $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-two');
        endif;
    }

    public function procesarAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if (!empty($_POST['pe_nombre'])) {
            $code = $this->_personaObj->saveData($_POST);
            //exit;
            if ($code)
                $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-two');
            else
                $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-one');
        }
        else {
            $this->_redirect(Cit_Init::config()->subdomain . '/proceso-reserva-two');
        }
    }

}