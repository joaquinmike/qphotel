<?php

/**
 * FranchiseController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */

require_once 'Zend/Controller/Action.php';

class FranchiseController extends Zend_Controller_Action 
{
    private $_sesion;
    
    function init(){
        $this->view->headTitle('Welcome to QP Hotels - FRANCHISE');
    }
    
    public function indexAction(){
        $this->_redirect('/franchise/terms');
    }
    public function termsAction()
    {
        $this->_sesion = new Zend_Session_Namespace('web');        
        $modelArticulos = new DbHtCmsArticulo();
        $params= $this->_getAllParams();    
        $idArticulo=isset($params['cod'])?$params['cod']:'';
        $this->view->titulos=$modelArticulos->getDataArticuloCategoria('20110016',$this->_sesion->lg);
        
        if(!empty($this->view->titulos)){
            if(empty($idArticulo)){
                $this->view->contenido = $contenido= $modelArticulos
                    ->getOneContainer('20110016',$this->_sesion->lg);
            }else{
                $this->view->contenido=$contenido= $modelArticulos
                    ->getOneContainer('20110016',$this->_sesion->lg,$idArticulo);
            }
        }

    }
}