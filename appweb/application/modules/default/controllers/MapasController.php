<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class MapasController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->view->headTitle('Welcome to QP Hotels - ' . Cit_Init::_('PAGE_MAP'));
    }

    public function indexAction() {
        
    }

}