<?php
/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';
class ReceiptController extends Zend_Controller_Action 
{
    /**
        * The default action - show the home page
        */
    public $_sesion;
    public $_idioma;
    public $_hotel;
    private $_fecha;
    private $_kardexObj;
    private $_aniObj;
    private $_cache;
    private $_objReserve;
    private $_dbImagenesArticulo;
    
    function init(){
        $this->view->headTitle($this->_sesion->datahotel[0]['su_reserva'] . ' - ' . Cit_Init::_('PAGE_CONFIRM'));
        $this->_personaObj = new DbHtSegPersona();
        $this->_objReserve = new DbHtResReserva();
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_fecha = new Cit_Db_CitFechas();
        $this->_kardexObj = new DbHtResKardex();
        $this->_aniObj = new DbHtResAnio();
        $this->_dbImagenesArticulo = new DbCmsImagenesArticulo();
        
        $this->_sesion = new Zend_Session_Namespace('web');        
            //$this->_colegioObj = new DbColegioSucursal();
         $frontendOptions = array(
            'lifetime' => 3600, // cache lifetime of 2 hours
            'automatic_serialization' => true
        );
        
        $backendOptions = array(
            'cache_dir' => dirname(MP) . '/appweb/var/cache/'  // Directory where to put the cache files
        );
        // getting a Zend_Cache_Core object
        $this->_cache = Zend_Cache::factory('Core',
            'File',
            $frontendOptions,
            $backendOptions);
    }

    public function indexAction()    
    {
    	$this->view->persona = $this->_sesion->penomcomp;
        $this->view->idioma = $this->_sesion->lg;
        
        $this->view->suc = $this->_sesion->datahotel;
        $this->view->reserve_info = $this->_sesion->reserve_info;

        $this->view->fecini = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_ini'],'D',$this->_sesion->lg);
        $this->view->fecfin = $this->_fecha->getFormatDate($this->_sesion->reserve_info['fecha_fin'],'D',$this->_sesion->lg);

        $this->view->usid = $this->_sesion->usid;
        $this->view->idioma = $this->_sesion->lg;
        
        $res_id = $this->getRequest()->getParam('res', '');
        $this->view->dtareserva = $this->_objReserve->fetchRow(array('res_id = ?' => $res_id))->toArray();
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        $this->view->data = $carrito->getContents()->getIterator();
        $this->view->total = $carrito->getTotal();
        $this->view->data = $carrito->getContents()->getIterator();
        foreach ($carrito->getContents()->getIterator() as $value) :
            if (strpos($value->getId(), '**')):
                $datos = explode('**', $value->getId());
                if($datos[0] != 'P'){
                    $service[] = array(
                        'id' => $datos[1],
                        'name' => ucfirst(strtolower($value->getName())),
                        'price' => $value->getPrice()
                    );
                }
                //okas
                $okas;

            else:
                $objDesde = new Zend_Date($this->_sesion->reserve_info['fecha_ini']);
                $objHasta = new Zend_Date($this->_sesion->reserve_info['fecha_fin']);
                $dias = $objHasta->getDate()->get(Zend_Date::TIMESTAMP) - $objDesde->getDate()->get(Zend_Date::TIMESTAMP);
                $dias = (int) ($dias / (60 * 60 * 24));
                //$dias = ($dias > 1)?$dias-1:$dias;
     
                $reserva = $this->_kardexObj->getPromocionDias($value->getId(), 'new');
                $this->view->roomName = $value->getName();
                $this->view->roomPrice = $value->getPrice() / $dias;
            endif;
        endforeach;
        $this->view->services = $service;
        $this->view->detalle = $reserva;
        
        $dtaLista = $this->_dbImagenesArticulo->getListaImagenes(array('art_id' => '00000012', 'cat_id' => '20110014', 'su_id' => $this->_sesion->su_id));
        $this->view->images = $dtaLista[0];
        $carrito->removeAll();
        $this->_cache->remove($this->_sesion->cache['rooms']);
        unset($this->_sesion->cache);
        unset($this->_sesion->promo);
  
    }
}