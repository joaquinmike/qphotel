<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ArticulosController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
    }

    public function indexAction() {
        //Categorias
        $select = $this->_helper->DBAdapter()->select();
        $data['cat_id'] = $this->getRequest()->getParam('cat', 0);
        if (empty($data['cat_id'])):
            $data['cat_id'] = $this->getRequest()->getParam('cat_id', 0);
            $data['art_id'] = $this->getRequest()->getParam('art_id', 0);

            $select->from(array('t1' => 'vht_cms_articulo'), array('art_id', 'art_titulo'));
            $select->join(array('t2' => 'ht_cms_idioma_art_cont'), 't1.art_id = t2.art_id and t1.id_id = t2.id_id', array('su_id', 'cat_id', 'art_contenido'));
            $select->where("cat_id = '{$data['cat_id']}' and t1.art_id = '{$data['art_id']}' and t1.id_id = '{$this->_sesion->lg}' and su_id = '{$this->_sesion->su_id}'");
        //echo $select; exit;
        else:
            $select->from(array('t1' => 'vht_cms_articulo'), array('art_id', 'art_titulo'));
            $select->join(array('t2' => 'ht_cms_idioma_art_cont'), 't1.art_id = t2.art_id and t1.id_id = t2.id_id', array('su_id', 'cat_id', 'art_contenido'));
            $select->where("cat_id = '{$data['cat_id']}' and t1.id_id = '{$this->_sesion->lg}' and su_id = '{$this->_sesion->su_id}'");
        //echo $select; exit;

        endif;

        $data = $select->query()->fetchAll();
        $this->view->data = $data;

        /* $helper = new Cit_View_Helper_HtmlIndex();
          $this->view->imagenes = $helper->articulos_imagenes($data); */
    }

}