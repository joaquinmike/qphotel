<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class SucursalController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
    }

    public function indexAction() {
        $categoria = $this->getRequest()->getParam('cat', 0);
        $this->view->cat_id = $categoria;

        $dtaDesc = $this->_citObj->getDataGral('ht_hb_sucursal', array('su_desc','su_reserva'), "su_id = '{$this->_sesion->su_id}'", 'U');
        $this->view->headTitle('Welcome to ' . $dtaDesc['su_reserva'] . ' - Principal');
        
        $desc = str_replace('\"', '', $dtaDesc['su_desc']);
        $desc = str_replace('=""', '', $desc);
        $this->view->desc_home = $desc;

        switch ($this->_sesion->su_id) {
            case '00000002':
                $this->view->sucursal = 'logo-qp-205';
                break;
            case '00000003':
                $this->view->sucursal = 'logo-qp-205-1';
                break;
        }
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}