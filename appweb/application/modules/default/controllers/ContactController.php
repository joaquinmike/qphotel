<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class ContactController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->view->headTitle('Welcome to QP Hotels - ' . Cit_Init::_('PAGE_CONTACT'));
    }

    public function indexAction() {
        if ($this->_sesion->su_id == '00000001') {
            $this->view->hotel = '00000002';
        } else {
            $this->view->hotel = $this->_sesion->su_id;
        }
        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);
        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);
    }

    public function sendAction() {
        //Initialize needed variables
        /* Recivir datos del formulario */
        $post = $this->getRequest()->getParams();
        if ($this->getRequest()->isPost()) :
            $nombre = $post['nombre'];
            $asunto = $post['asunto'];
            $email = $post['email'];
            $contenido = $post['contenido'];
            /* Fin de datos */
            $your_email = 'info@qphotels.com'; //Or your_email@gmail.com for Gmail
    //        $your_password = 'perucit2012';
            //SMTP server configuration
            $smtpHost = 'smtp.gmail.com';
            $smtpConf = array(
                'auth' => 'login',
                'ssl' => 'ssl',
                'port' => '465',
                'username' => 'perucit@gmail.com',
                'password' => 'perucit2012'
            );
            $transport = new Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
            try {
                //Create email
                $mail = new Zend_Mail();
                $email_to = 'info@qphotels.com';
                $name_to = "Qp Hotels";

                $content = utf8_decode('<table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <p style="font-size: 16px;"><strong>' . $asunto . '</strong></p>
                                <strong>Usuario : </strong><br />
                                    <span style="margin-left:40px;">' . $nombre . ' (' . $email . ')</span><br /><br />
                                <strong>Asunto : </strong><br />
                                    <span style="margin-left:40px;">' . $contenido . '</span>
                            </td>
                        </tr>
                    </table>
                    ');

                $mail->setReplyTo($email, $nombre);
                $mail->setFrom($email, $nombre);
                $mail->addTo($email_to, $name_to);
                $mail->setSubject($asunto);
                $mail->setBodyHtml($content);

                //Send
                $sent = true;

                $mail->send($transport);
            } catch (Exception $e) {
                $sent = false;
            }
        else:
            $sent = false;
        endif;
        
        //Return boolean indicating success or failure
        $this->view->sendEmail = $sent;
    }

}
