<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class CloseController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;

    function init() {

        $this->_sesion = new Zend_Session_Namespace('login');
    }

    public function indexAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->_sesion->unsetAll();
        $this->_redirect(Cit_Init::config()->subdomain);
    }

}
