<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class PublicacionController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
    public $_sesion;
    public $_citObj;
    public $_fecObj;

    function init(){
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
    }
    public function indexAction() 
    {
    	$tipo = $this->getRequest()->getParam('tipo',0);
    	$cod = $this->getRequest()->getParam('cod',0);
    	$this->view->codigo = $cod;
    	$select = $this->_helper->DBAdapter()->select();
    	
    	if(empty($cod)){
            $limit = $this->getRequest()->getParam('limit',10);
            $start = $this->getRequest()->getParam('page',0);

            if(!empty($start))
                    $pag =(int)(($start/$limit)+1); 
            else
                    $pag = 1;

            $select->from(array('t1'=>'vht_cms_publicacion'),array('pub_id', 'pub_titulo', 'pub_contenido', 'pub_fecha'))
                    ->where("pub_estado = '1' and t1.id_id = '{$this->_sesion->lg}' and pub_tipo = '{$tipo}'")
                    ->order(array('pub_fecha desc','pub_titulo'));
            //echo $select; exit;	
            
            $paginator = Zend_Paginator::factory ( $select );
            $paginator->setCurrentPageNumber( $pag )->setItemCountPerPage($limit);

            $this->view->paginator = $paginator; 
    	}else{
    		$dtaPublic = $this->_citObj->getDataGral('vht_cms_publicacion', array('pub_id','pub_titulo','pub_contenido', 'pub_fecha'),array('where'=>"id_id ='{$this->_sesion->lg}' and pub_id = '{$cod}'"),'U');
    		$this->view->data = $dtaPublic;
    	}

        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy,1);
        
        $this->view->fechas = array('hoy'=>$fecHoy,'man'=>$fecMan);

        if ($this->_sesion->su_id == '00000001'){
            $this->view->hotel = '00000002';
        }else{
            $this->view->hotel = $this->_sesion->su_id;
        }
    	
    }	
}