<?php

/**
 * GuaranteedController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class GuaranteedController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
  

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
    }

    public function indexAction() {
        
    }
}