<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class CategoriaController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;
    public $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
    }

    public function indexAction() {
        $interno = $this->getRequest()->getParam('interno', 0);
        $select = $this->_helper->DBAdapter()->select();
        $data = $this->_citObj->getDataGral('vht_cms_menu_interno', array('cat_id', 'cat_titulo', 'cat_desc'), "cat_id = '{$interno}' and id_id = '{$this->_sesion->lg}'", 'U');
        $this->view->headTitle('Welcome to ' . $this->_sesion->datahotel[0]['su_reserva'] . ' - ' . ucfirst($data['cat_titulo']));
        
        $this->view->interno = $data;

        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);

        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);

        if ($this->_sesion->su_id == '00000001') {
            $this->view->hotel = '00000002';
        } else {
            $this->view->hotel = $this->_sesion->su_id;
        }
        
        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);
        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}