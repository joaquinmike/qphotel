<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class DiversionController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    //public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        //$this->_citObj = new CitDataGral();
    }

    public function indexAction() {
        $interno = $this->getRequest()->getParam('interno', 0);
        $tih_id = $this->getRequest()->getParam('tih', 0);
        $objMenuInterno = new DbVHtCmsMenuInterno();
        $objTipRoom = new DbVHtHbTipoHabitacion();
        $dtaInterno = $objMenuInterno->fetchRow(array('cat_id =?' => $interno,'id_id =?' => $this->_sesion->lg))->toArray();
        //$dtaInterno = $this->_citObj->getDataGral('vht_cms_menu_interno', array('cat_id', 'cat_titulo', 'cat_desc'), "cat_id = '{$interno}' and id_id = '{$this->_sesion->lg}'", 'U');
        $this->view->interno = $dtaInterno;
        $dtaHab = $objTipRoom->fetchRow(array('tih_id =?' => $tih_id,'id_id =?' => $this->_sesion->lg))->toArray();
        //$dtaHab = $this->_citObj->getDataGral('vht_hb_tipo_habitacion', array('tih_id', 'tih_desc', 'tih_coment'), "id_id = '{$this->_sesion->lg}' and tih_id = '{$tih_id}'", 'U');
        $this->view->habitacion = $dtaHab;
        $html = '';
        /* $dtaLista = $this->_citObj->getDataGral('vht_hb_tipo_habitacion', array('cat_id','tih_desc','tih_coment'),"cat_id = '{$interno}'",'U');
          foreach($dtaLista as $value):
          switch ($value['tih_id']):
          case $tih_id:
          $html = ' <li>'.$value['tih_desc'].'</li>';
          break;

          default:
          $html = ' <li><a href="/room_type/lima/show/en/Complimentary+bottle+of+Champagne+upon+arrival+">'.$value['tih_desc'].'</li>';
          break;
          endswitch;
          endforeach; */
        $this->view->lista_tih = $html;
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}