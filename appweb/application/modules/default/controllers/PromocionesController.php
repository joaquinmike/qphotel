<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class PromocionesController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->view->headTitle('Welcome to QP Hotels - ' . Cit_Init::_('PAGE_PROMOTIONS'));
        $this->_citObj = new CitDataGral();
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_fecObj = new Cit_Db_CitFechas();
    }

    public function indexAction() {
        $cod = $this->getRequest()->getParam('cod', 0);
        $this->view->codigo = $cod;

        $select = $this->_helper->DBAdapter()->select();

        if (empty($cod)) {
            //Lista de Todos...............
            $limit = $this->getRequest()->getParam('limit', 10);
            $page = $this->getRequest()->getParam('page', 1);

            $select->from(array('t1' => 'vht_cms_promocion'), 
                    array('pro_id', 'pro_desc', 'pro_titulo', 'pro_fecalta', 'pro_fecini', 'pro_fecfin', 'pro_dscto', 'tih_id', 'an_id'))
                    ->join(array('t2' => 'vht_hb_tipo_habitacion'), 
                        "t1.tih_id = t2.tih_id and cat_id = 'H' and t1.id_id = t2.id_id", 
                        array('tih_abr'))
                    ->joinLeft(array('t3' => 'ht_cms_promocion_sucursal'), 
                        't1.pro_id = t3.pro_id and t1.an_id = t3.an_id and t1.tih_id = t3.tih_id', 
                        array('promo' => new Zend_Db_Expr('CASE WHEN pro_fecfin > current_date THEN 1 ELSE 0 END')))
                    ->joinLeft(array('t5' => 'ht_cms_idioma_promsucursal'), 
                        't1.id_id = t5.id_id and t1.an_id = t5.an_id and t1.pro_id = t5.pro_id and t1.tih_id = t5.tih_id and t3.su_id = t5.su_id ', 
                        array('desc' => 'pro_desc'))
                    ->join(array('t4' => 'ht_hb_sucursal'), 't3.su_id = t4.su_id', 
                        array('su_id', 'su_nombre'))
                    ->where("pro_estado = 'S'")
                    ->where('t1.id_id = ?', $this->_sesion->lg)
                    ->where('t1.pro_fecfin > ?', new Zend_Db_Expr('current_date'))
                    //->where('t1.an_id = ?', date('Y'))
                    ->order(array('pro_fecfin desc', 'tih_id', 'pro_id'));
            //echo $select; exit;	
            //$dtaPromo = $select->query()->fetchAll();
            $paginator = Zend_Paginator::factory($select);
            $paginator->setCurrentPageNumber($page)->setItemCountPerPage($limit);

            $this->view->data = $paginator;
        }else {
            //Unico..........................
            list($sucursal, $anio, $promo) = explode('*', $cod);

            $data['su_id'] = $sucursal;
            $data['an_id'] = $anio;
            $data['pro_id'] = $promo;

            $select->from(array('t1' => 'vht_cms_promocion'), array('pro_id', 'pro_titulo','tih_id', 'pro_desc','pro_fecini', 'pro_fecfin'))
                    ->joinleft(array('t2' => 'ht_cms_idioma_promsucursal'), 
                        't1.id_id = t2.id_id and t1.an_id = t2.an_id and t1.pro_id = t2.pro_id and t1.tih_id = t2.tih_id and t2.su_id = ' . "'{$sucursal}'", 
                        array('desc' => 'pro_desc'))
                    ->joinleft(array('t3' => 'ht_cms_promo_imagen'), 
                        't2.an_id = t3.an_id and t2.pro_id = t3.pro_id and t2.su_id = t3.su_id and t2.tih_id = t3.tih_id', 
                        array(''))
                    ->joinleft(array('t4' => 'ht_cms_imagenes'), 't3.im_id = t4.im_id ', array('im_image2'))
                    ->joinleft(array('t5' => 'ht_hb_sucursal'), 't5.su_id = t2.su_id ', array('su_nombre'))
                    ->where('t1.id_id = ?', $this->_sesion->lg)
                    ->where('t1.an_id = ?', $anio)
                    ->where('t1.pro_id = ?' , $promo);
            //echo $select; exit;
            $dta = $select->query()->fetch();
            if (!empty($dta)):
                $data['titulo'] = $dta['pro_titulo'];
                $data['nombre'] = $dta['su_nombre'];
                $data['idpromo'] = $dta['pro_id'];
                $data['tih_id'] = $dta['tih_id'];
                $this->_fecObj->setFecha($dta['pro_fecini']);
                $data['fecha_ini'] = $this->_fecObj->renders('open');
                $this->_fecObj->setFecha($dta['pro_fecfin']);
                $data['fecha_fin'] = $this->_fecObj->renders('open');
                if (!empty($dta['desc']))
                    $data['contenido'] = $dta['desc'];
                else
                    $data['contenido'] = $dta['pro_desc'];

            else:
                $data['titulo'] = 'NO exite Contenido';
                $data['contenido'] = '';
            endif;
            $this->view->promo = $data;
            
        }


        $dtaVerde = $this->_citObj->getDataGral('vht_cms_descripciones', array('des_contenido'), "des_id = 'VER'", 'U');
        //str_replace($search, $replace, $subject)

        $desc = str_replace('\"', '"', $dtaVerde['des_contenido']);
        $desc = str_replace("\'", "'", $desc);

        $this->view->desc = $desc;

        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);

        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);

        if ($this->_sesion->su_id == '00000001') {
            $this->view->hotel = '00000002';
        } else {
            $this->view->hotel = $this->_sesion->su_id;
        }
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}