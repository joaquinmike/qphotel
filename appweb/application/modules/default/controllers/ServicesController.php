<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class ServicesController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursalObj;
    private $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        $this->_fecObj = new Cit_Db_CitFechas();
    }

    public function indexAction() {
        
    }

    public function jsonAction() {
        
    }

    public function ajaxAction() {
        
    }

}