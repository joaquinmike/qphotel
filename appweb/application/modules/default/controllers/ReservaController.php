<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_usuarioObj;
    public $_sucursalObj;
    public $_sesion;
    public $_idioma;

    function init() {
        $this->_usuarioObj = new DbHtSegUsuario();
        $this->_sucursalObj = new DbHtHbSucursal();
        $this->_idioma = new DbHtCmsIdioma();
        $this->_sesion = new Zend_Session_Namespace('login');
        //$this->_colegioObj = new DbColegioSucursal();
    }

    public function indexAction() {

        $categoria = $this->getRequest()->getParam('cat', 0);

        $this->view->sucursal = $this->_helper->Combos->html('ht_hb_sucursal', "ho_id = '" . htID . "' and su_portal = 'N'", array('id' => 'su_id', 'desc' => 'su_nombre'));
        $this->view->cat_id = $categoria;
        
        $post = $this->getRequest()->getParams();
        $this->view->data = $post;
        /* $select = $this->_helper->DBAdapter()->select();
          $select->from(array('t1'=>'vht_cms_articulo_categoria_sucursal'),array('art_id','art_titulo'));
          $select->where("su_id = '00000001' and cat_id = '20110009' and art_est = '1'");
          $select->order('sca_orden');
          $select->limit(4);
          echo $select; exit;
          /*$dta = $select->query()->fetchAll();
          $html = '';
          foreach ($dta as $value):
          //<li><a class="highlight" href="/">Home</a></li>
          $html.= '<li><a class="highlight" href="#'.$value['art_id'].'">'.$value['art_titulo'].'</a></li>';
          endforeach;
          echo $html; exit; */

        //var_dump($this->_sesion->lg);
    }

}
