<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class AboutController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_fecObj;
    private $_citObj;

    function init() {
        $this->view->headTitle('Welcome to QP Hotels - ' . Cit_Init::_('PAGE_NOSOTROS'));
        
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_fecObj = new Cit_Db_CitFechas();
        $this->_citObj = new CitDataGral();
        //$this->_colegioObj = new DbColegioSucursal();
    }

    public function indexAction() {
        $desc = $this->_citObj->getDataGral('vht_cms_descripciones', array('des_contenido', 'des_nombre'), "des_id = 'NOS' and id_id = '{$this->_sesion->lg}'", 'U');
        $this->view->texto = $desc['des_contenido'];
        $this->view->textonom = $desc['des_nombre'];
        
        if ($this->_sesion->su_id == '00000001') {
            $this->view->hotel = '00000002';
        } else {
            $this->view->hotel = $this->_sesion->su_id;
        }
        $fecHoy = date('d/m/Y');
        $fecMan = $this->_fecObj->suma_fechas($fecHoy, 1);
        $this->view->fechas = array('hoy' => $fecHoy, 'man' => $fecMan);
    }

}