<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class CloseWebController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;

    function init() {

        $this->_sesion = new Zend_Session_Namespace('web');
    }

    public function indexAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        $this->_sesion->unsetAll();
        $carrito->removeAll();
        $this->_redirect(Cit_Init::config()->subdomain);
    }

}
