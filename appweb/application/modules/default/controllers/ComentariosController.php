<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class ComentariosController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    private $_citObj;
    const TIPO_EVENTO = 'A';

    function init() {
        $this->_sesion = new Zend_Session_Namespace('web');
        $this->_citObj = new CitDataGral();
        $this->view->headTitle('Welcome to ' . $this->_sesion->datahotel[0]['su_reserva'] . ' - ' . ucfirst($data['cat_titulo']));
        $this->view->headTitle('Welcome to QP Hotels - ' . Cit_Init::_('PAGE_NOSOTROS'));
    }

    public function indexAction() {
        
    	$cod = $this->getRequest()->getParam('cod',0);
    	
    	$select = $this->_helper->DBAdapter()->select();
    	$this->view->codigo = $cod;
    	if (empty($cod)) {
            $limit = $this->getRequest()->getParam('limit',10);
            $start = $this->getRequest()->getParam('page',0);

            if(!empty($start))
                    $pag =(int)(($start/$limit)+1); 
            else
                    $pag = 1;

            $select->from(array('t1'=>'vht_cms_publicacion'),array('pub_id', 'pub_titulo', 'pub_contenido', 'pub_fecha'))
                ->where("pub_estado = '1'")
                ->where('t1.id_id = ?', $this->_sesion->lg)
                ->where('pub_tipo = ?', self::TIPO_EVENTO)
                ->order(array('pub_fecha desc','pub_titulo'));
            //echo $select; exit;	

            $paginator = Zend_Paginator::factory ( $select );
            $paginator->setCurrentPageNumber( $pag )->setItemCountPerPage($limit);

            $this->view->paginator = $paginator; 
        }else{
            $dtaPublic = $this->_citObj->getDataGral('vht_cms_publicacion', array('pub_id', 'pub_titulo', 'pub_contenido', 'pub_fecha'), array('where' => "id_id ='{$this->_sesion->lg}' and pub_id = '{$cod}'"), 'U');
            $this->view->data = $dtaPublic;
           
        }
            
        
    	
    }

}
