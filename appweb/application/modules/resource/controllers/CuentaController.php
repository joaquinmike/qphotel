<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */

require_once 'Zend/Controller/Action.php';

class CuentaController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;

	
	function init(){	
		$this->_sesion = new Zend_Session_Namespace('login');
	}
 	public function indexAction()    
    {     	
    	$select = $this->_helper->DBAdapter()->select();
    	$select->from(array('t1'=>'usuario'),array('*'))
    				   ->join(array('t2'=>'persona'),'t1.pe_id = t2.pe_id',array('*'))
    				   ->where("t1.us_id = '{$this->_sesion->usid}'");
    	 $datauser = $select->query()->fetchall();
    	 $this->view->data = $datauser[0];
    }
  	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
				
    }
}
