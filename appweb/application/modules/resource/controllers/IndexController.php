<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */

require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;

	
	function init(){	
		$this->_sesion = new Zend_Session_Namespace('login');
	}
 	public function indexAction()    
    {     	
    	$select = $this->_helper->DBAdapter()->select();		
		$select->from(array('t1'=>'ht_seg_usuario_menu'),array('t1.su_id','t1.us_id'));
		$select->join(array('t2'=>'ht_hb_sucursal'),'t1.su_id=t2.su_id',array('su_nombre'));
	    $select->where("t1.us_id = '{$this->_sesion->usid}'");
	    $select->distinct();
	    $this->view->data = $select->query()->fetchAll();
	    exit;
    }
  	public function ajaxAction(){
		
				
    }
}
