<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */

require_once 'Zend/Controller/Action.php';

class PublicidadController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_sesion;

	
	function init(){
		Cit_Init::setConfig('/config/publicidad.ini');		
		$this->_sesion = new Zend_Session_Namespace('login');
	}
 	public function indexAction()    
    {     	
    	$db=Zend_Db::factory(Cit_Init::config()->database);
    	Zend_Db_Table_Abstract::setDefaultAdapter($db);
    	$select = $db->select();
    	$select->from(array('t1'=>'publicidad'),array('t1.*'));
    	$select->where("pu_estado='1'");
    	
    	//$select->join($name, $cond);
    	
    	$data = $select->query()->fetchAll();
    	
    	
    	//var_dump($_SESSION);exit;
       $db->beginTransaction ();		
        try {
        	$newdata = array();
	    	$indices = array_rand ( $data , 5 );
	    	foreach ($indices as $indice => $value){
	    		$newdata[$indice] = $data[$value];	    		
	    		$select1 = $db->select();
		    	$select1->from(array('t1'=>'accesos'),array('ac_id'))->limit(1)->order('ac_id desc');
		    	$datacl = $select1->query()->fetchAll();
		    	//var_dump($datacl);
		    	if(empty($datacl))
		    		$clid = 1;
		    	else 
		    		$clid = $datacl[0]['ac_id']+1;		    	
		    	$db->insert('accesos', array('ac_id'=>$clid,'ac_ip'=>$_SERVER['REMOTE_ADDR'],'pu_id'=>$data[$value]['pu_id'],'ac_session'=>$this->_sesion->usid));
	    	}
			$this->view->data = $newdata;
        	$db->commit();
        } catch ( Zend_Exception $e ) {
        	$db->rollBack ();
        	throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }
  	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case = $this->getRequest()->getParam('case',0);
		$db=Zend_Db::factory(Cit_Init::config()->database);
    	Zend_Db_Table_Abstract::setDefaultAdapter($db);
    	$select = $db->select();
		switch ($case){
	  		case 'click':
	  			$select1 = $db->select();
		    	$select1->from(array('t1'=>'click'),array('cl_id'))->limit(1)->order('cl_id desc');
		    	$datacl = $select1->query()->fetchAll();
		    	if(empty($datacl))
		    		$clid = 1;
		    	else 
		    		$clid = $datacl[0]['cl_id']+1;		    	
		    	$db->insert('click', array('cl_id'=>$clid,'cl_ip'=>$_SERVER['REMOTE_ADDR'],'pu_id'=>$_POST['pu_id'],'cl_session'=>$this->_sesion->usid));
	  			break;	  			
		}			
    }
}
