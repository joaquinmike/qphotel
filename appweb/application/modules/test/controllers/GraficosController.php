<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';
//echo dirname(BP). DS . 'library'.DS.'Includes'.DS.'FusionCharts.php';exit;
require_once dirname(BP). DS . 'library'.DS.'Includes'.DS.'FusionCharts.php';
class GraficosController extends Zend_Controller_Action 
{
	/** 
	 * The default action - show the home page
	 */
	
    public function indexAction() 
    {
    	FC_SetDataFormat("json");
    	//echo 'aaa';exit;    	
    }
 	public function jsonAction() 
    {
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		echo '{
  "chart":{ 
    "caption":"Monthly Unit Sales", "xaxisname":"Month", 
	 "yaxisname":"Units", "showvalues":"0", 
	 "formatnumberscale":"0", "showborder":"1"  },
  "data":[
    { "label":"Jan", "value":"462","link":"n-DemoLinkPages/DemoLink1.html"  },
    { "label":"Feb", "value":"857"  },
    { "label":"Mar", "value":"671"  },
	 { "label":"Apr", "value":"494"  },
    { "label":"May", "value":"761"  },
    { "label":"Jun", "value":"960"  },
    { "label":"Jul", "value":"629"  },
    { "label":"Aug", "value":"622"  },
	 { "label":"Sep", "value":"376"  },
    { "label":"Oct", "value":"494"  },
    { "label":"Nov", "value":"761"  },
    { "label":"Dec", "value":"960"  }
  ]
}';    	
    }
}