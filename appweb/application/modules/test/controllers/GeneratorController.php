<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class GeneratorController extends Zend_Controller_Action 
{
	/** 
	 * The default action - show the home page
	 */
	
    public function indexAction() 
    { 
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	$temp_path='C:/AppServ/www/cit/appweb/application/';
    	$db = $this->_helper->DBAdapter ();
    	$code = new Cit_Generator_Code($db,$temp_path);
    	
    	$code -> generateCode();   	
    }
}