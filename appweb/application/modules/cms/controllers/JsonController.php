<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version  
 */

require_once 'Zend/Controller/Action.php';

class JsonController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_vtabladetalle;
	
	public $_control;
	
	public $_ubigeo;
	
	public $_familia;
	
	public $_grado;
	
	public $_seccion;
	
	public $_sesion;
	
	public $_aulaObj;
	
	public $_aniObj;
	
	function init(){
		$this->_vtabladetalle = new DbVtablaDetalle();
		
		$this->_ubigeo = new DbHtSegUbigeo(); 	
		
		$this->_sesion = new Zend_Session_Namespace('login');
		
		$this->_aniObj = new DbHtResAnio();

	} 
	public function tabladetalleAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();

	  	 $dataubigeo = $this->_vtabladetalle->fetchAll(null,'tba_id')->toArray();
  		 echo  	$this->_ubigeo->json($dataubigeo);			
  		
	}
	
   
    public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();	    			
		$case = $this->getRequest()->getParam('case',0);
		$depart = $this->getRequest()->getParam('departamento',0);
		$prov = $this->getRequest()->getParam('provincia',0);
		$tba_id = $this->getRequest()->getParam('tba_id',0);
		
	  	switch ($case){
	  		case 'tabla_detalle':	  			
	  			$select->from(array('t1'=>'vtabla_detalle'),array('tad_id','tad_desc'))
	  				   ->where("t1.tba_id = '$tba_id' and t1.id_id='".$this->_sesion->lg."'")
	  				   ->order('t1.tad_desc');
	  			$dataTablaDetalle = $select->query()->fetchAll();
	  			echo  $this->_ubigeo->json($dataTablaDetalle, 'matches');
				break;
	  			 
	  		case 'departamento':
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,3,4)='0000'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('state'=>$value['ub_desc'],'ub_id_dep'=>$value['ub_id']);  		
	  			 }	 
	  			 echo  	$this->_ubigeo->json($newdata);			
	  			 break;
	  			 
	  		case 'provincia':
	  			$depart = substr($depart,0,2);
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,1,2)='$depart' and substr(ub_id,5,2)='00' and substr(ub_id,3,4)!='0000'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('state'=>$value['ub_desc'],'ub_id_prov'=>$value['ub_id']);  		
	  			 }	
	  			  echo $this->_ubigeo->json($newdata);  				
	  			 break;
	  			 
	  		case 'distrito':
	  			$prov = substr($prov,0,4);
	  			 $dataubigeo = $this->_ubigeo->fetchAll("substr(ub_id,1,4)='$prov' and substr(ub_id,5,2)!='00'",'ub_desc')->toArray();
	  			 $newdata = array();
	  			 foreach($dataubigeo as $value){
	  			 	$newdata[] = array('ub_desc'=>$value['ub_desc'],'ub_id'=>$value['ub_id']);  		
	  			 }	 
	  			  echo  $this->_ubigeo->json($newdata);				
	  			 break;
	  			 
	  	    case 'anio':
	  	    	
	  			 $dtaYear = $this->_aniObj->fetchAll(null,'an_id')->toArray();
	  			 $newdata = array();
	  			 foreach($dtaYear as $value){
	  			 	$newdata[] = array('an_id'=>$value['an_id'],'state'=>$value['an_abr']);  		
	  			 }	 
	  			 echo  $this->_ubigeo->json($newdata);				
	  			 break;
	  	
	  		break;
	  	}
	  	  	
	}
}
