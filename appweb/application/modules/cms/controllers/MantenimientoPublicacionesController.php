<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoPublicacionesController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_citObj;
    public $_sesion;
    public $_publicObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_publicObj = new DbHtCmsPublicacion();
    }


    public function formAction() {
        //$this->_ht_hb_sucursalObj = new DbHtHbSucursal();
        $this->view->idarticulo = $this->_publicObj->correlativo(10);
        $this->view->estado = $this->_helper->Combos(array('1' => 'Activo', '0' => 'Inactivo'), array('id' => 'pub_estado'));
        $this->view->categoria = $this->_helper->Combos(new DbVHtCmsCategoria(), array('id' => 'cat_id', 'desc' => 'cat_titulo', 'order' => 'cat_titulo', 'where' => "id_id='{$this->_sesion->lg}'"));
    }

    public function jsonAction() {
        //Desabilitar la capa
        $this->_helper->layout->disableLayout();
        //Para no Cargar la vista
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $art_id = $this->getRequest()->getParam('art_id', 0);

        switch ($case):
            case 'getPublic':
                $where = '';
                if (!empty($_POST['pub_tipo']))
                    $where = ' and pub_tipo = ' . "'{$_POST['pub_tipo']}'";
                $dtaPublic = $this->_citObj->getDataGral('vht_cms_publicacion', array('pub_id', 'pub_titulo', 'pub_estado','pub_contenido','pub_fecha','pub_tipo'), array('where' => "id_id ='{$this->_sesion->lg}'" . $where, 'order' => 'pub_fecha desc'), 'T');
                echo $this->_publicObj->json($dtaPublic);
                break;
                
            case 'newart':
                echo json_encode(array('pub_id' => $this->_publicObj->correlativo(8)));
                break;
            case 'imagen':
                $select = $this->_helper->DBAdapter()->select();
                $select->from(array('t1' => 'ht_cms_imagenes'), array('*'));
                $select->joinleft(array('t3' => 'ht_cms_public_imagen'), 't1.im_id=t3.im_id', array(''));
                $select->where("t3.pub_id='{$_POST['idevento']}'");
                //$select->where("t3.tih_id='{$_POST['tih_id']}'");
                //echo $select;exit;
                $dataimagen = $select->query()->fetchAll();
                $newdata = array();
                foreach ($dataimagen as $value) {
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];
                    $newdata['images'][] = array('name' => $value['im_id'], 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
                }
                if (empty($newdata)) {
                    $newdata['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'idimg' => 0);
                }
                echo json_encode($newdata);
            default:
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'save':
                $_POST['id_id'] = $this->_sesion->lg;
                $errNum = $this->_publicObj->saveData($_POST);
                $this->_helper->Alert($errNum, '', '', '');
                break;
            case 'delete':
                $action = $this->_publicObj->deleteData($_POST);
                $this->_helper->Alert($action, '', '', 'No es posible eliminar, existen datos relacionados');
                break;
            case 'delImgeve':
                $errNum = $this->_publicObj->deleteImagePublic($_POST);
                $this->_helper->Alert($errNum, '', '', 'No es posible eliminar, existen datos relacionados');
                break;
            
            case 'saveimage';
                error_reporting(NULL);
                
                $_POST['ho_id'] = $this->_sesion->hoid;
                //copy($_FILES['imgtipo']['tmp_name'], MP.'/nv.jpg');
                
                $this->_publicObj->saveImagen($_POST);
                echo json_encode(array('success'=>true));
                break;
        endswitch;
        //var_dump($errNum); exit;
        //echo $errNum; exit;
    }


}

