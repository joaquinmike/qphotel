<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionAlumnosGruposController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_aulaObj;
    //public $_gradObj;
    public $_sesion;
    public $_grupoAlumno;

    //public $_profeSalonObj;
    function init() {
        $this->_aulaObj = new DbAula();
        //$this->_gradObj = new DbGrado();
        $this->_grupoAlumno = new DbGrupoAlumno();
        //$this->_profeSalonObj = new DbProfesorSalon();
        $this->_sesion = new Zend_Session_Namespace('login');
    }

    public function formAction() {

        $arrayAula = array();
        $arrayProfesor = array();
        $arrayGrupo = array();
        $arrayTurno = array();
        $select = $this->_helper->DBAdapter()->select();
        $select1 = $this->_helper->DBAdapter()->select();
        $select2 = $this->_helper->DBAdapter()->select();
        $select3 = $this->_helper->DBAdapter()->select();

        $select->from(array('t1' => 'vaula'), array('lleva', 'audesc'))
                ->where("id_id = '" . $this->_sesion->lg . "'")
                ->order('ni_id')->order('gr_id')->order('se_id');
        $dataAula = $select->query()->fetchAll();
        foreach ($dataAula as $value) {
            $arrayAula[$value['lleva']] = $value['audesc'];
        }

        $select1->from(array('t1' => 'vprofesor'), array('pr_id', 'pe_nomcomp'))
                ->order('pe_nomcomp');
        $dataProfesor = $select1->query()->fetchAll();
        foreach ($dataProfesor as $value) {
            $arrayProfesor[$value['pr_id']] = $value['pe_nomcomp'];
        }

        $select2->from(array('vgrupo'), array('gp_id', 'gp_desc'));
        $select2->where("id_id='{$this->_sesion->lg}'");
        $dataGrupo = $select2->query()->fetchAll();
        foreach ($dataGrupo as $value) {
            $arrayGrupo[$value['gp_id']] = $value['gp_desc'];
        }

        $select3->from(array('t1' => 'turno'), array('tu_id', 'tu_nombre'));
        $dataTurno = $select3->query()->fetchAll();
        foreach ($dataTurno as $value) {
            $arrayTurno[$value['tu_id']] = $value['tu_nombre'];
        }

        $this->view->listaAula = $this->_helper->Combos($arrayAula, array('id' => 'lleva'));
        $this->view->listaGrupo = $this->_helper->Combos($arrayGrupo, array('id' => 'gp_id'));
        $this->view->listaProfesor = $this->_helper->Combos($arrayProfesor, array('id' => 'id_pr'));
        $this->view->listaNivel = $this->_helper->Combos(new DbNivel(), array('id' => 'ni_id', 'desc' => 'ni_desc', 'order' => 'ni_desc'));
        $this->view->listaSeccion = $this->_helper->Combos(new DbSeccion(), array('id' => 'se_id', 'desc' => 'se_desc', 'order' => 'se_desc'));
        $this->view->listaTurno = $this->_helper->Combos($arrayTurno, array('id' => 'tu_id'));
        //$this->view->listaArea = $this->_helper->Combos(new DbArea(),array('id'=>'ar_id','desc' => 'ar_desc','order'=>'ar_desc'));
    }

    public function jsonAction() {
        //Desabilitar la capa
        $this->_helper->layout->disableLayout();
        //Para no Cargar la vista
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $select = $this->_helper->DBAdapter()->select();
        $select0 = $this->_helper->DBAdapter()->select();
        $au_id2 = $this->getRequest()->getParam('au_id2', 0);
        $ar_id = $this->getRequest()->getParam('ar_id', 0);
        $gp_id = $this->getRequest()->getParam('gp_id', 0);
        $ni_id = $this->getRequest()->getParam('ni_id', 0);
        $gr_id = $this->getRequest()->getParam('gr_id', 0);
        $se_id = $this->getRequest()->getParam('se_id', 0);
        $tu_id = $this->getRequest()->getParam('tu_id', 0);
        $bi_id = $this->getRequest()->getParam('bi_id', 0);

        $cursoObj = new DbGrado();
        switch ($case):
            case 'getArea':
                $select->from(array('t1' => 'profesor_salon'), array('ar_id'))
                        ->join(array('t2' => 'varea'), 't1.ar_id = t2.ar_id', array('ar_desc'))
                        //->join(array('t3'=>'grupo'),'t1.gp_id=t3.gp_id',array(''))
                        ->where("t1.au_id2 = '$au_id2' and t2.id_id = '" . $this->_sesion->lg . "' and t1.gp_id!='T'")
                        ->order('t2.ar_desc')
                        ->group(array('t1.ar_id', 't2.ar_desc'));
                //echo $select;exit;
                $dataArea = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataArea);
                break;
            case 'getGrupo':
                $select->from(array('t1' => 'profesor_salon'), array())
                        ->join(array('t2' => 'vgrupo'), 't1.gp_id = t2.gp_id', array('gp_id', 'gp_desc'))
                        ->where("t1.au_id2 = '$au_id2' and t1.ar_id = '$ar_id' and t2.id_id = '" . $this->_sesion->lg . "'")
                        ->order('t2.gp_desc');
                $dataGrupo = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataGrupo);
                break;
            case 'getAlumnoGrado' :
//    		and al_id not in (select al_id from grupo_alumno where au_id2='$au_id2' and ar_id='$ar_id'" 
                $select->from(array('t1' => 'aula_alumno'), array('al_id'))
                        ->join(array('t2' => 'valumno'), 't1.al_id = t2.al_id', array('pe_sexo', 'pe_nomcomp'))
                        ->where("t1.au_id2 = '$au_id2' and t2.al_id not in (select al_id from grupo_alumno where au_id2='$au_id2' and ar_id='$ar_id' and bi_id = '$bi_id')")
                        ->order('t2.pe_nomcomp');
                //echo $select;exit;
                $dataAlumnoGrado = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataAlumnoGrado);
                break;
            case 'getAlumnoGrupo' :
                $select->from(array('t1' => 'grupo_alumno'), array())
                        ->join(array('t2' => 'valumno'), 't1.al_id = t2.al_id', array('al_id', 'pe_nomcomp', 'pe_sexo'))
                        ->where("t1.au_id2 = '" . $au_id2 . "' and t1.gp_id = '" . $gp_id . "' and t1.ar_id = '" . $ar_id . "' and t1.bi_id = '" . $bi_id . "'")
                        ->order('t2.pe_nomcomp');
                $dataAlumnoGrupo = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataAlumnoGrupo);
                break;
            case 'getBimestre' :
                $select0->from(array('t1' => 'aula'), array('per_id'))
                        ->where("au_id2 = '" . $au_id2 . "'");
                $dataPeriodo = $select0->query()->fetchall();
                foreach ($dataPeriodo as $value) {
                    $per_id = $value['per_id'];
                }
                $select->from(array('t1' => 'bimestre'), array('bi_id', 'bi_desc'))
                        ->where("per_id = '" . $per_id . "'")
                        ->order('bi_id');
                $dataBimestre = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataBimestre);
                break;
            case 'getAulaId':
                $select->from(array('t1' => 'aula'), array('au_id2'))
                        ->where("t1.ni_id = '" . $ni_id . "' and t1.gr_id = '" . $gr_id . "' and t1.se_id = '" . $se_id . "' and t1.tu_id = '" . $tu_id . "' and an_id = '" . $this->_sesion->anio . "'");
                $dataAulaId = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataAulaId);
                break;
            case 'getBimestre':
                $select->from(array('t1' => 'bimestre'), array('bi_id', 'bi_desc'))
                        ->join(array('t2' => 'aula'), 't1.per_id = t2.per_id', array())
                        ->where("t1.per_id= '" . $per_id . "' and t2.au_id2 = '" . $au_id2 . "'")
                        ->order('t1.bi_desc');
                $dataBimestre = $select->query()->fetchall();
                echo $this->_aulaObj->json($dataBimestre);
                break;
            case 'getProfes':
                $select->from(array('t1' => 'profesor_salon'), array(''))
                        ->join(array('t2' => 'vprofesor'), 't1.pr_id = t2.pr_id', array('pr_id', 'pe_nomcomp'))
                        ->where("t1.ar_id= '{$_POST['ar_id']}'")
                        ->order('t2.pe_nomcomp');
                $data = $select->query()->fetchall();
                echo $this->_aulaObj->json($data);
                break;

            default:
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'save':
                $errNum = $this->_grupoAlumno->saveData($_POST);
                break;
        endswitch;
        //var_dump($errNum); exit;
        $this->_helper->Alert($errNum, '', 'Registro grabado Correctamente', 'Registro Existente');
    }

}
