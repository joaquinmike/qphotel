<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';
require_once dirname(BP). DS . 'library'.DS.'Includes'.DS.'FusionCharts.php';
class HelpController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_sesion;
	function init(){
		$this->_sesion = new Zend_Session_Namespace('login');
	}
    public function indexAction() 
    {
    	$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender();
		   	
    }	
    
    public function graficoAction(){	 	
    	FC_SetDataFormat("json");
    	
    }
	public function jsonAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	  	$select = $this->_helper->DBAdapter()->select();	
	  	$case = $this->getRequest()->getParam('case',0);
		switch ($case):
	    	case 'data'://Devolver el ID para una nueva habitacion
	    		
	    		$select->from(array('t1'=>'ht_res_kardex'),array('label'=>'t2.tih_desc','value'=>'count(*)'));
	    		$select->join(array('t2'=>'vht_hb_tipo_habitacion'),'t1.tih_id=t2.tih_id',array());
	    		$select->where("t2.id_id='{$this->_sesion->lg}' and ka_situacion='R'");
	    		$select->group('t2.tih_desc');
	    		//echo $select; exit;
	    		$data = $select->query()->fetchall();

	    		echo json_encode(array("chart"=>
		    		array( 
					    "caption"=>"Cuadro de reservas por tipo de habitacion", "xaxisname"=>"Tipos de Habitacion", 
						 "yaxisname"=>"reservas", "showvalues"=>"0", 
						 "formatnumberscale"=>"0", "showborder"=>"1"),
					 	 "data"=>$data
				  ));   	
	    		break;
	    	case 'data1'://Devolver el ID para una nueva habitacion
	    		
	    		$select->from(array('t1'=>'ht_res_kardex'),array('label'=>'t2.tih_desc','value'=>'count(*)'));
	    		$select->join(array('t2'=>'vht_hb_tipo_habitacion'),'t1.tih_id=t2.tih_id',array());
	    		$select->where("t2.id_id='{$this->_sesion->lg}' and ka_situacion='R'");
	    		$select->group('t2.tih_desc');
	    		//echo $select; exit;
	    		$data = $select->query()->fetchall();

	    		echo json_encode(array("chart"=>
		    		array( 
					    "caption"=>"Cuadro de reservas por tipo de habitacion", "xaxisname"=>"Tipos de Habitacion", 
						 "yaxisname"=>"reservas", "showvalues"=>"0", 
						 "formatnumberscale"=>"0", "showborder"=>"1"),
					 	 "data"=>$data
				  ));   	
	    		break;
	  	endswitch;
	} 
    
    
}
