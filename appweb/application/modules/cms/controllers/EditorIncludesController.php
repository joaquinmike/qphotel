<?php
/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class EditorIncludesController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_citObj;
	public $_sesion;
	public $_articuloObj;
	

	function init(){
		
	    $this->_sesion = new Zend_Session_Namespace('login');
	    $this->_citObj = new CitDataGral();
	    $this->_articuloObj = new DbHtCmsArticulo();
	}
    public function formAction() 
    {
    	$this->_ht_hb_sucursalObj = new DbHtHbSucursal();
  		$this->view->listaSucursal = $this->_helper->Combos(new DbHtHbSucursal(),array('id'=>'su_id','desc' => 'su_nombre','order'=>'su_nombre','where'=>"ho_id = '{$this->_sesion->hoid}' and su_estado != 0"));
    }
    
    
    public function jsonAction(){
    	//Desabilitar la capa
		$this->_helper->layout->disableLayout();
		$select = 
		//Para no Cargar la vista
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$su_id = $this->getRequest()->getParam('su_id',0);
		$cat_id = $this->getRequest()->getParam('cat_id',0);
		switch ($case):
    		case 'getListaArticulos':
    			$dataArticulos = $this->_citObj->getDataGral('vht_cms_articulo', array('art_est','art_id','art_titulo'),array('where'=>"id_id ='{$this->_sesion->lg}' and su_id = '$su_id' and cat_id = '$cat_id'",'order'=>'art_id'),'T');
    			echo $this->_articuloObj->json($dataArticulos);
    			break;
    		case 'getListaCategorias':
    			$dataCategoria = $this->_citObj->getDataGral('vht_cms_categoria', array('cat_id','cat_titulo'),array('where'=>"id_id = '{$this->_sesion->lg}' and su_id = '$su_id' and cat_estado != 0"));
    			echo $this->_articuloObj->json($dataCategoria);
    			break;
    		default:
    			break;
		endswitch;	
	}
	
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):		
	    	case 'save':
	    		$errNum = $this->_grupoAlumno->saveData($_POST);
	    		break;
		endswitch;
		//var_dump($errNum); exit;
		$this->_helper->Alert($errNum,'','Registro grabado Correctamente','Registro Existente');		
	}	      
}
