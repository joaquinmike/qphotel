<?php
/**
 * MantenimientoHotelController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';
class MantenimientoHotelController extends Zend_Controller_Action
{
    /**
     * The default action - show the home page
     */
    public $_aulaObj;

    //public $_gradObj;
    public $_sesion;
    public $_hthbHotelObj;
    public $_hthbSucursalObj;
    public $_citObj;
    public $_grupoAlumno;
    public $_control;
    public $_ubigeo;
    public $_tbadetalle;
    public $_ano;
    public $_cliente;
    //public $_profeSalonObj;
    
    function init(){
        //$this->_aulaObj = new DbAula();
        //$this->_gradObj = new DbGrado();
        //$this->_grupoAlumno = new DbGrupoAlumno();
        //$this->_profeSalonObj = new DbProfesorSalon();
        $this->_citObj = new CitDataGral();
        $this->_ubigeo = new DbHtSegUbigeo();
        $this->_tbadetalle = new DbVtablaDetalle();
        $this->_hthbHotelObj = new DbHtHbHotel();
        $this->_hthbSucursalObj = new DbHtHbSucursal();
        $this->_sesion = new Zend_Session_Namespace('login');
    }
    
    public function formAction() 
    {
        $select = array('ho_ruc');
        $select1 = array('ho_nombre');
    	$where = array('where'=>"ho_estado = 1");
        $numeroRuc = $this->_citObj->getDataGral('ht_hb_hotel',$select,$where,'U');
        $nombreHot = $this->_citObj->getDataGral('ht_hb_hotel',$select1,$where,'U');
        $this->view->numRuc = $numeroRuc['ho_ruc'];
        $this->view->nomHot = $nombreHot['ho_nombre'];

        $this->view->sucdepartamento = $this->_helper->Combos($this->_ubigeo,array('id'=>'ub_id','mask'=>'ub_id_dep','desc' => 'ub_desc','where'=>"substr(ub_id,3,4)='0000'",'order'=>'ub_desc'));
        $this->view->suestado = $this->_helper->Combos(array('1'=>'Activo','0'=>'Inactivo'),array('id'=>'su_estado'));
        $this->view->idsucur = $this->_hthbSucursalObj->correlativo(8);
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal',array('su_idcpy'=>'su_id','su_nombre'),array('where'=>"ho_id = '{$this->_sesion->hoid}'",'order'=>'su_nombre'));
        $this->view->sucursal  = $this->_helper->Combos->consulta($sucursal,array('id'=>'su_idcpy','desc' => 'su_nombre'),1);
    }
    
    public function jsonAction(){
    	$this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case=$this->getRequest()->getParam('case',0);
        $select = $this->_helper->DBAdapter()->select();
        switch ($case){
            case 'sucursal':
                $select->from(array('t1'=>'ht_hb_sucursal'), array('hsu_id'=>'su_id','su_id', 'ho_id', 'su_nombre','su_estado','su_telefono1','su_telefono2','su_ruc','su_direccion','su_desc'))
                       ->join(array('t2'=>'ht_seg_ubigeo'), 't1.ub_id = t2.ub_id', array('ub_desc', 'ub_id'))
                       ->where("t1.ho_id = '{$this->_sesion->hoid}'");
                //echo $select;exit;

                $listadoSuc = $select->query()->fetchAll();
                //var_dump($listadoSuc);exit;
                foreach ($listadoSuc as $indice => $value) {
                     $value['su_desc']=str_replace("\\", '', $value['su_desc']);
                     $value['ub_id_dep'] = str_pad(substr($value['ub_id'], 0,2),6,'0',STR_PAD_RIGHT);  		
                     $value['ub_id_prov'] = str_pad(substr($value['ub_id'], 0,4),6,'0',STR_PAD_RIGHT);

                    $listadoSuc[$indice]=$value;
                }
                        //echo $listadoSuc[$indice];exit;
                echo $this->_hthbSucursalObj->json($listadoSuc);

                break;
            case 'newsucur':	
                echo json_encode(array('su_id'=>$this->_hthbSucursalObj->correlativo(8)));
                break;
    	}
    }
	
    public function ajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case',0);
        $_POST['ho_id'] = $this->_sesion->hoid;
        switch ($case){ 
            case 'save':	  			
                $this->_helper->Alert($this->_hthbSucursalObj->saveData($_POST));
                break;
            case 'delete':	  			
                $_POST['action'] = 'delete';
                $this->_helper->Alert($this->_hthbSucursalObj->deleteSuc($_POST),'','','','El Alumno Tiene Datos Relacionados');
                break;
        }		
    }	      
}
