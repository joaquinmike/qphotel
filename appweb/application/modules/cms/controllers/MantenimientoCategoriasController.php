<?php

/**
 * MantenimientoClienteController
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoCategoriasController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_categoria;
    public $_sesion;

    function init() {
        $this->_categoria = new DbHtCmsCategoria();
        $this->_sesion = new Zend_Session_Namespace('login');
    }

    public function formAction() {
        $this->view->categoria = $this->_categoria->correlativo(8);
        $this->view->suestado = $this->_helper->Combos(array('1' => 'Activo', '0' => 'Inactivo'), array('id' => 'cat_estado'));
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case) {
            case 'save':
                $_POST['id_id'] = $this->_sesion->lg;
                $this->_helper->Alert($this->_categoria->saveData($_POST));
                break;
            case 'delete':
                $this->_helper->Alert($this->_categoria->deleteCategoria($_POST), '', '', 'Existen datos relacionados');
                break;
        }
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        //var_dump($_SESSION);exit;
        switch ($case) {
            case 'categorias':
                $limit = $this->getRequest()->getParam('limit', 0);
                $start = $this->getRequest()->getParam('start', 0);

                $select->from(array('t1' => 'vht_cms_categoria'), array('cat_id_id' => 'cat_id', 't1.*'))
                        ->where("id_id='{$this->_sesion->lg}'")
                        ->order('cat_titulo');

                $data = $select->query()->fetchAll();

                if (!empty($start)) {
                    $pagina = (int) (($start / $limit) + 1);
                }
                else
                    $pagina = 1;
                $paginator = Zend_Paginator::factory($data);
                $paginator->setCurrentPageNumber($pagina)
                        ->setItemCountPerPage($limit);
                foreach ($paginator->getCurrentItems() as $value) {
                    $value['cat_estado'] = (int) $value['cat_estado'];
                    $dat[] = $value;
                }
                if (!empty($dat)) {
                    $totalpages = $paginator->getPages()->pageCount;
                    $total = $paginator->getTotalItemCount();
                    $records = $paginator->getPages()->totalItemCount;

                    echo json_encode(Array('totalCount' => $total, 'matches' => $dat, 'page' => $pagina, 'total' => $paginator->getTotalItemCount(), 'records' => $records, "success" => 'true'));
                } else {
                    echo $this->_categoria->json(array());
                }
                break;
            case 'newtipo':
                echo json_encode(array('cat_id' => $this->_categoria->correlativo(8)));
                break;
            case 'imagen':

                $select->from(array('t1' => 'ht_cms_imagenes'), array('*'));
                $select->joinleft(array('t3' => 'ht_hb_tipo_habitacion_imagen'), 't1.im_id=t3.im_id', array(''));
                $select->where("t3.tih_id='{$_POST['tih_id']}'");
                //$select->where("t3.tih_id='{$_POST['tih_id']}'");
                //echo $select;exit;
                $dataimagen = $select->query()->fetchAll();
                $newdata = array();
                foreach ($dataimagen as $value) {
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];
                    $newdata['images'][] = array('name' => 'ti', 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000');
                }
                echo json_encode($newdata);


                break;
        }
    }

}
