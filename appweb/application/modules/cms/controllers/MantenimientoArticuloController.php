<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class MantenimientoArticuloController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_citObj;
	public $_sesion;
	public $_articuloObj;
	//public $_gradObj;
	
	//	public $_grupoAlumno;
	//public $_profeSalonObj;

	function init(){
		//$this->_aulaObj = new DbAula();
		//$this->_gradObj = new DbGrado();
		//$this->_grupoAlumno = new DbGrupoAlumno();
		//$this->_profeSalonObj = new DbProfesorSalon();
	    $this->_sesion = new Zend_Session_Namespace('login');
	    $this->_citObj = new CitDataGral();
	    $this->_articuloObj = new DbHtCmsArticulo();
	}
    public function formAction() 
    {
    	//$this->_ht_hb_sucursalObj = new DbHtHbSucursal();
    	$this->view->idarticulo = $this->_articuloObj->correlativo(8);
    	$this->view->estado = $this->_helper->Combos(array('1'=>'Activo','0'=>'Inactivo'),array('id'=>'art_est'));
  		$this->view->categoria = $this->_helper->Combos(new DbVHtCmsCategoria(),array('id'=>'cat_id','desc' => 'cat_titulo','order'=>'cat_titulo','where'=>"id_id='{$this->_sesion->lg}'"));
    }
    
    
    public function jsonAction(){
    	//Desabilitar la capa
		$this->_helper->layout->disableLayout(); 
		//Para no Cargar la vista
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		$su_id = $this->getRequest()->getParam('su_id',0);
		$cat_id = $this->getRequest()->getParam('cat_id',0);
		$art_id = $this->getRequest()->getParam('art_id',0);
		switch ($case):
    		case 'getListaArticulos':
    			$dataArticulos = $this->_citObj->getDataGral('vht_cms_articulo', array('art_est','art_id','art_titulo', 'ho_id', 'su_id','cat_id'),array('where'=>"id_id ='{$this->_sesion->lg}' and su_id = '$su_id' and cat_id = '$cat_id'",'order'=>'art_id'),'T');
    			echo $this->_articuloObj->json($dataArticulos);
    			break;
    		case 'getListaCategorias':
    			$dataCategoria = $this->_citObj->getDataGral('vht_cms_categoria', array('cat_id','cat_titulo'),array('where'=>"id_id = '{$this->_sesion->lg}' and su_id = '$su_id' and cat_estado != 0"));
    			echo $this->_articuloObj->json($dataCategoria);
    			break;
    		case 'getDataArt':
    			$dataArticulos = $this->_citObj->getDataGral('vht_cms_articulo', array('art_est','art_id','art_titulo'),array('where'=>"id_id ='{$this->_sesion->lg}'",'order'=>'art_id'),'T');
    			
    			//select art_id, art_titulo from vht_cms_articulo where id_id = 'ES'
    			//!!! NO OLVIDARSE ENVIAR PARAMETRO DE IDIOMA...!!!
//    			$dataArticulos = $this->_articuloObj->getDataArt(array('cat_id'=>$cat_id, 'id_id'=>'ES'));
				//$dataArticulos = $this->_citObj->getDataGral('vht_cms_articulo', array('art_est','art_id','art_titulo'),array('where'=>"id_id ='{$this->_sesion->lg}' and su_id = '$su_id' and cat_id = '$cat_id'",'order'=>'art_id'),'T');
				echo $this->_articuloObj->json($dataArticulos);
    			break;
    		case 'newart':
    			echo json_encode(array('art_id'=>$this->_articuloObj->correlativo(8)));
    			break;
    		default:
    			break;
		endswitch;	
	} 
	
	public function ajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):		
	    	case 'save':
	    		$_POST['id_id'] = $this->_sesion->lg;
	    		$errNum = $this->_articuloObj->saveData($_POST);
	    		$this->_helper->Alert($errNum,'','','');
	    		break;
	    	case 'delete':	  			
	  			$errNum = $this->_articuloObj->deleteArticulo($_POST);
	  			$this->_helper->Alert($errNum,'','','No es posible eliminar, existen datos relacionados');
	  			break;
		endswitch;
		//var_dump($errNum); exit;
		//echo $errNum; exit;
				
	}	      
}
