<?php
/**
 * MantenimientoDescripciones
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';
class MantenimientoDescripcionesController extends Zend_Controller_Action
{
    /**
     * The default action - show the home page
     */
	public $_citObj;
	public $_descripObj;
	public $_sesion;
	
    function init(){

        $this->_descripObj = new DbHtCmsDescripciones();

        $this->_citDataObj = new CitDataGral();

        $this->_sesion = new Zend_Session_Namespace('login');
	
    }
    
    public function formAction ()
    {
        $this->view->suestado = $this->_helper->Combos(array('1'=>'Activo','0'=>'Inactivo'),array('id'=>'des_estado'));
	
    }
    public function ajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case',0);
        switch ($case){ 
            case 'save':	
                $_POST['id_id'] = $this->_sesion->lg;
                $this->_helper->Alert($this->_descripObj->saveData($_POST));
                break;
            case 'delete':	  			
                $this->_helper->Alert($this->_descripObj->deleteData($_POST),'','','Existen datos relacionados');
                break;
        }		
    }
    public function jsonAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();	    			
        $case = $this->getRequest()->getParam('case',0);
        //var_dump($_SESSION);exit;
        switch ($case){
            case 'lista':	
                $select = array('lleva'=>'des_id','des_id','des_contenido','des_estado','des_nombre');
                $where = array('where'=>"id_id = '{$this->_sesion->lg}'",'order'=>'des_id');
                $data = $this->_citDataObj->getDataGral('vht_cms_descripciones',$select,$where);
                
                foreach ($data as $value):
                    $value['des_contenido']=str_replace("\\", '', $value['des_contenido']);
                    $items[] = $value;
                endforeach;
                
                echo $this->_citDataObj->json($items);
                break;
            
            case 'newtipo':	
                echo json_encode(array('cat_id'=>$this->_categoria->correlativo(8)));
                break;
            
            case 'imagen':
                $select->from(array('t1'=>'ht_cms_imagenes'),array('*'));
                $select->joinleft(array('t3'=>'ht_hb_tipo_habitacion_imagen'),'t1.im_id=t3.im_id',array(''));
                $select->where("t3.tih_id='{$_POST['tih_id']}'");
                //$select->where("t3.tih_id='{$_POST['tih_id']}'");
                //echo $select;exit;
                $dataimagen = $select->query()->fetchAll();
                $newdata = array();
                foreach ($dataimagen as $value){
                    if(empty($value['im_image2']))
                        $value['im_image1']=Cit_Init::config()->subdomain.'/img/imgblak.jpg'; 
                    else 
                        $value['im_image1'] = Cit_Init::config()->subdomain.'/img'.$value['im_image1'];
                    
                    $newdata['images'][]=array('name'=> 'ti','url'=> $value['im_image1'],"size"=>'2477','lastmod'=>'1291155496000');
                }
                echo json_encode($newdata);			

                break;	
        }	  	  	
    }    
}
