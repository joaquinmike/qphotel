<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionArticuloCategoriaSucursalController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_tipohabObj;
    public $_accesTipohab;
    public $_tihSucursal;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_dbCategoria = new DbHtCmsCategoria();
        $this->_dbArticulo = new DbHtCmsArticulo();
        $this->_dbCategoriaArticulo = new DbCmsCategoriaArticulo();
        $this->_dbSucursalCategoria = new DbHtCmsSucursalCategoria();
        $this->_dbImagenesArticulo = new DbCmsImagenesArticulo();
        $this->_dbContenidoArticulo = new DbHtCmsIdiomaArtCont();
    }

    public function formAction() {
        //Sucursal

        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
        $this->view->suid = $this->_sesion->suid;
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $su_id = $this->getRequest()->getParam('su_id', 0);
        $cat_id = $this->getRequest()->getParam('cat_id', 0);
        $cad_art = $this->getRequest()->getParam('cad_art', 0);
        $cad_cat = $this->getRequest()->getParam('cad_cat', 0);
        $art_id = $this->getRequest()->getParam('art_id', 0);
        switch ($case):
            case 'getContArticulo':
                $dtaLista = $this->_citObj->getDataGral('ht_cms_idioma_art_cont', array('art_id', 'id_id', 'su_id', 'cat_id', 'art_contenido'), array('where' => "id_id = '{$this->_sesion->lg}' and art_id = '$art_id' and su_id = '$su_id' and cat_id = '$cat_id'"));
                echo $this->_dbCategoria->json($dtaLista);
                break;
            case 'getListaImages':
                $dtaLista = $this->_dbImagenesArticulo->getListaImagenes(array('art_id' => $art_id, 'cat_id' => $cat_id, 'su_id' => $su_id));
                $newdata = array();
                foreach ($dtaLista as $value) {
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];
                    $newdata['images'][] = array('name' => $value['im_id'], 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
                }
                if (empty($newdata)) {
                    $newdata['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'idimg' => 0);
                }
                echo json_encode($newdata);
                break;
            case 'getListaCatxSuc':
                $cad_cat = str_replace("\\", '', $cad_cat);
                $dtaLista = $this->_citObj->getDataGral('vht_cms_categoria', array('cat_id', 'cat_titulo'), array('where' => "id_id = '{$this->_sesion->lg}' and cat_id not in ($cad_cat)", 'order' => 'cat_titulo'));
                echo $this->_dbCategoria->json($dtaLista);
                break;
            case 'getListaCategoria':
                $dtaLista = $this->_citObj->getDataGral('vht_cms_categoria_sucursal', array('cat_id', 'cat_titulo', 'cat_estado'), array('where' => "su_id = '$su_id' and id_id = '{$this->_sesion->lg}'", 'order' => 'cat_titulo'));
                echo $this->_dbCategoria->json($dtaLista);
                break;
            case 'getListaArticulos':
                $dtaLista = $this->_citObj->getDataGral('vht_cms_articulo_categoria_sucursal', array('art_est', 'art_id', 'art_titulo', 'cat_id', 'su_id', 'art_orden', 'art_link'), array('where' => "id_id = '{$this->_sesion->lg}' and cat_id = '$cat_id' and su_id = '$su_id'", 'order' => 'art_titulo'));
                //categoria articulo
                //$dtaLista = $this->_dbCategoriaArticulo->getListaArtxCat(array('lg'=>$this->_sesion->lg, 'su_id'=>$su_id, 'cat_id'=>$cat_id));
                //$dtaLista = $this->_citObj->getDataGral('vht_cms_articulo_categoria_sucursal', array('art_est', 'art_id', 'art_titulo', 'cat_id', 'su_id'),array('where'=>"id_id = '{$this->_sesion->lg}' and cat_id = '$cat_id' and su_id = '$su_id'", 'order'=>'art_titulo'));
                echo $this->_dbCategoria->json($dtaLista);
                break;
            case 'getListaArtxCatySuc':
                $dtaLista = $this->_dbArticulo->getDataArtxCatySuc(array('id_id' => $this->_sesion->lg, 'su_id' => $su_id, 'cat_id' => $cat_id));
                echo $this->_dbArticulo->json($dtaLista);
                break;
            case 'getListaArtNoAsignados':
                $cad_art = str_replace("\\", '', $cad_art);
                $dtaLista = $this->_citObj->getDataGral('vht_cms_articulo', array('art_id', 'id_id', 'art_est', 'art_titulo'), array('where' => "art_id not in ($cad_art) and id_id = '{$this->_sesion->lg}' and art_est not in (0)"));
                echo $this->_dbArticulo->json($dtaLista);
                break;
        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'updateOrdenArt':
                $evalua = $this->_dbCategoriaArticulo->updateOrden($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'No es posible actualizar datos.', '');
                break;
            case 'saveContArticulo':
                $_POST['id_id'] = $this->_sesion->lg;                
                $evalua = $this->_dbContenidoArticulo->saveContenido($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'No se pueden almacenar el contenido.', '');
                break;
            case 'delImgArtCatSuc':
                $evalua = $this->_dbImagenesArticulo->deleteImagen($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'No se pueden eliminar las imágenes.', '');
                break;
            case 'saveImage':
                error_reporting(NULL);
                $_POST['ho_id'] = $this->_sesion->hoid;
                $this->_dbImagenesArticulo->saveImagen($_POST, 'banner');
                echo '{success:true, file:' . json_encode($_FILES['articulo']['name']) . '}';
                break;
            case 'delArtCatSuc':
                $_POST['artids'] = str_replace("\\", '', $_POST['artids']);
                $evalua = $this->_dbCategoriaArticulo->deleteData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen dependencia de los registros.', '');
                break;
            case 'delCatSuc':
                $evalua = $this->_dbSucursalCategoria->deleteCatSuc($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen dependencia de los registros.', '');
                break;
            case 'addCatSuc':
                $evalua = $this->_dbSucursalCategoria->asignCatSuc($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'addArtCatSuc':
                $evalua = $this->_dbCategoriaArticulo->asignArticulo($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'saveAccesTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_accesTipohab->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
            case 'delAccesTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_accesTipohab->deleteData($_POST);

                $this->_helper->Alert->direct($evalua['id'], '', @$evalua['desc'], @$evalua['desc'], '');
                break;
            case 'saveSucursalTipha':
                $_POST['grid'] = explode('**', $_POST['grid']);
//                var_dump($_POST);
//                exit;
                $evalua = $this->_accesTipohab->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;
        endswitch;
    }

}
