<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionPromocionSucursalController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_citObj;
    public $_promosuObj;
    public $_fecOjb;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_promosuObj = new DbHtCmsPromocionSucursal();
        $this->_promoObj = new DbHtCmsPromocion();
        $this->_kardexObj = new DbHtResKardex();
        $this->_fecOjb = new Cit_Db_CitFechas();
        $this->_citObj = new CitDataGral();
    }

    public function formAction() {
        //Sucursal
        $admin = '';
        if (PADMIN != $this->_sesion->perid) {
            $admin = " and su_id='{$this->_sesion->suid}'";
        }
        $this->view->suid = $this->_sesion->suid;
        $select = array('su_id', 'su_nombre');
        $where = array('where' => "ho_id = '{$this->_sesion->hoid}' and su_portal = 'N'" . $admin, 'order' => 'su_nombre');
        $sucursal = $this->_citObj->getDataGral('ht_hb_sucursal', $select, $where);
        $this->view->sucursal = $this->_helper->Combos->consulta($sucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaTipohab'://Lista de tipo de habitaciones por sucursal
                if ($_POST['su_id']):
                    $dtaLista = $this->_promosuObj->getDatosSucurTipohab("su_id = '{$_POST['su_id']}'");
                    $i = 0;
                    foreach ($dtaLista as $value):
                        $save = $this->_citObj->getDataGral('ht_hb_habitacion', array('stock' => new Zend_Db_Expr('count(*)')), "su_id = '{$_POST['su_id']}' and tih_id = '{$value['tih_id']}'", 'U');
                        $dtaLista[$i]['tih_stock'] = $save['stock'];
                        $i++;
                    endforeach;
                else:
                    $dtaLista = array();
                endif;
                echo $this->_citObj->json($dtaLista);
                break;

            case 'listaPromoSucursal'://Segundo Grid
                if (!empty($_POST))
                    $dtaLista = $this->_promosuObj->getPromoSucursal("t2.su_id = '{$_POST['su_id']}' and t2.tih_id = '{$_POST['tih_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;

            case 'selPromocion'://
                $fecha = date('d/m/Y');
                $this->_fecOjb->setFecha($fecha);
                $fecha = $this->_fecOjb->renders('save');

                if (!empty($_POST))// and su_id = '{$_POST['su_id']}'ht_cms_idioma_promsucursal
                    $dtaLista = $this->_promosuObj->selPromoSucursal("'$fecha' < pro_fecfin and tih_id = '{$_POST['tih_id']}'", "su_id = '{$_POST['su_id']}' and tih_id = '{$_POST['tih_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;

            case 'Contenido':
                if (!empty($_POST))// and su_id = '{$_POST['su_id']}'
                    $dtaLista = $this->_citObj->getDataGral('ht_cms_idioma_promsucursal', array('pro_desc'), "su_id = '{$_POST['su_id']}' and tih_id = '{$_POST['tih_id']}' and pro_id = '{$_POST['pro_id']}'");
                else
                    $dtaLista = array();
                echo $this->_citObj->json($dtaLista);
                break;

            case 'getImgPromo':
                $dtaLista = $this->_promosuObj->getPromoImagen($_POST);
                $array = array();
                foreach ($dtaLista as $value):
                    if (empty($value['im_image2']))
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
                    else
                        $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];

                    $array['images'][] = array('name' => $value['im_id'], 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
                endforeach;
                ;
                if (empty($array))
                    $array['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'idimg' => 0);

                echo json_encode($array);
                break;

        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'savePromoSucursal':
                $datPro = explode(',', $_POST['grid']);
                $_POST['grid'] = explode('**', $_POST['grid']);

                $evalua = $this->_promosuObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen los registros.', '');
                break;

            case 'delPromoSucursal':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_promosuObj->deleteData($_POST);
                $this->_helper->Alert->direct($evalua['id'], '', @$evalua['desc'], @$evalua['desc'], '');
                break;

            case 'saveContenido':
                $post = $this->getRequest()->getParams();
                $evalua = $this->_promosuObj->saveContenidoLocal($post,$this->_sesion->lg);
                $this->_helper->Alert->direct($evalua, '', 'Contenido actualizado Correctamente', 'Algun error', '');
                break;
            case 'saveImagen':
                error_reporting(0);
                $_POST['ho_id'] = $this->_sesion->suid;
                $this->_promosuObj->saveImagenPromo($_POST, 'promo');
                echo Zend_Json::encode(array("success" => true, 'data' => array("msge" => 'correctamente')));
                break;
            case 'deleteImagen':
                $_POST['ho_id'] = $this->_sesion->suid;
                //$this->_promosuObj->deleteImagenPromo($_POST);
                //$_POST['ho_id']=$this->_sesion->hoid;
                $evalua = $this->_promosuObj->deleteImagenPromo($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'No se puede eliminar imagen.', '');
                break;
            default:
                break;
        endswitch;
    }

}
