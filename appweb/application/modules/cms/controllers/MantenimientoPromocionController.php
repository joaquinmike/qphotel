<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class MantenimientoPromocionController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_tipohabObj;
    public $_citObj;
    public $_promocionObj;
    public $_fecObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_promocionObj = new DbHtCmsPromocion();
        $this->_citObj = new CitDataGral();
        $this->_tipohabObj = new DbHtHbTipoHabitacion();
        $this->_fecObj = new Cit_Db_CitFechas();
    }

    public function formAction() {
        $this->view->fecha = date('d/m/Y');
        $this->view->estado = $this->_helper->Combos(array('S' => 'Activo', 'N' => 'Inactivo'), array('id' => 'pro_estado'));
        $dtaArticulo = $this->_citObj->returnCorrelativo('ht_cms_promocion', array('pro_id' => 'max("pro_id")'), 8, 'U');
        //var_dump($dtaArticulo); exit;
        $this->view->newID = $dtaArticulo["pro_id"];
        $dtaTipohab = $this->_tipohabObj->getCombo();
        $this->view->tipohab = $this->_helper->Combos->consulta($dtaTipohab, array('id' => 'tih_id', 'desc' => 'tih_desc'));
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):
            case 'listaPromociones'://Lista de tipo de habitaciones por sucursal
                $data = array('hpro_id' => 'pro_id', 'an_id', 'pro_id', 'tih_id', 'pro_estado', 'pro_titulo', 'pro_desc', 'pro_dscto', 'pro_fecini', 'pro_fecfin');
                $dtaLista = $this->_citObj->getDataGral('vht_cms_promocion', $data, "id_id = '{$this->_sesion->lg}' and ho_id = '{$this->_sesion->hoid}'"); //("su_id = '{$_POST['su_id']}'");
                $i = 0;
                foreach ($dtaLista as $value):
                    //$this->_fecObj->setFecha($value['pro_fecini']);	
                    $this->_fecObj->setData($value['pro_fecini']);
                    $dtaLista[$i]['pro_fecini'] = $this->_fecObj->renders('open');

                    //$this->_fecObj->setFecha($value['pro_fecfin']);	
                    $this->_fecObj->setData($value['pro_fecfin']);
                    $dtaLista[$i]['pro_fecfin'] = $this->_fecObj->renders('open');

                    $i++;
                endforeach;
                echo $this->_citObj->json($dtaLista);
                break;

            case 'newID'://
                $dtaLista = $this->_tihSucursalObj->selTipohabSucursal("su_id = '{$_POST['su_id']}'");
                echo $this->_citObj->json($dtaLista);
                break;

        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):
            case 'savePromocion':
                $evalua = $this->_promocionObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existe el COD de la promoción', '');
                break;
            case 'deletePromocion';
                $evalua = $this->_promocionObj->deletePromocion($_POST,$this->_sesion->anio);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen reservas con la promoción seleccionada', '');
                break;
        endswitch;
    }

}
