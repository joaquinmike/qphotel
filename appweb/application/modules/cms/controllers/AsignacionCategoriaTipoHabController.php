<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */
require_once 'Zend/Controller/Action.php';

class AsignacionCategoriaTipoHabController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_accesObj;
    public $_citObj;
    public $_menuInterObj;

    function init() {
        header("Content-type: text/html; charset=utf-8");
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_citObj = new CitDataGral();
        $this->_menuInterObj = new DbHtCmsSucursalMenuInterno();
    }

    public function formAction() {
        //Sucursal
      
        $objSucursal = new DbHtHbSucursal();
        $dtaSucursal = $objSucursal->fetchAll(array('ho_id =?' => $this->_sesion->hoid, 'su_portal =?' => 'N'),'su_nombre')->toArray();
        $this->view->sucursal = $this->_helper->Combos->consulta($dtaSucursal, array('id' => 'su_id', 'desc' => 'su_nombre'), 1);
    }

    public function jsonAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();
        $case = $this->getRequest()->getParam('case', 0);
        switch ($case):

            case 'listaCategoria'://Lista de tipo de habitaciones por sucursal
                $where['where'] = "id_id = '{$this->_sesion->lg}' and cat_id <> 'H'";
                $where['order'] = 'cat_orden';
                //var_dump($this->_sesion->usid); exit;
                $dtaLista = $this->_citObj->getDataGral('vht_cms_menu_interno', array('cat_id', 'cat_titulo', 'cat_desc'), $where);

                echo $this->_citObj->json($dtaLista);
                break;

            case 'listaTipohab'://Lista de tipo de habitaciones por sucursal
                if (!empty($_POST['su_id']))
                    $dtaLista = $this->_menuInterObj->getDatosSucurMenuInterno($_POST);
                else
                    $dtaLista = array();

                echo $this->_citObj->json($dtaLista);
                break;


            case 'selSucursalMenuinter':
                $dtaLista = $this->_menuInterObj->selMenuInterno($_POST);
                echo $this->_citObj->json($dtaLista);
                break;

        endswitch;
    }

    public function ajaxAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $case = $this->getRequest()->getParam('case', 0);
        $form = $this->getRequest()->getParam('form', 0);
        switch ($case):

            case 'saveMenuInterno':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_menuInterObj->saveSucursalCategorias($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen datos relacionados.', '');
                break;
            case 'deleteMenuInterno':
                $_POST['grid'] = explode('**', $_POST['grid']);
                $evalua = $this->_menuInterObj->deleteSucursalCategorias($_POST);
                $this->_helper->Alert->direct($evalua, '', '', 'Existen datos relacionados.', '');
                break;

        endswitch;
    }

}
