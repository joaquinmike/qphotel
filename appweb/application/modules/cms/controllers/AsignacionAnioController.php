<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class AsignacionAnioController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	public $_anioObj;
    public $_sesion;
	function init(){
		
		$this->_anioObj = new DbAnio();
 		$this->_sesion = new Zend_Session_Namespace('login');
	}
    public function formAction() 
    {
	//
    	$this->view->anio = $this->_helper->Combos(new DbAnio(),array('id'=>'an_id','desc' => 'an_id','order'=>'an_id desc'));
    	$this->view->anioactual = $this->_sesion->anio; 
    }
   
	public function ajaxAction() 
    {
      //  var_dump($_POST);exit();
    	$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		$case=$this->getRequest()->getParam('case',0);
		switch ($case):
		
	    	case 'save'://Update
	    		
	    		$this->_sesion->anio = $_POST['an_id'];
	    		break;
			
		endswitch;
    	//->0,1,2,3,
		$this->_helper->Alert(1,'','');
    	
    }
}
