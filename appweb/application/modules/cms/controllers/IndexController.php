<?php

/**
 * IndexController - The default controller class
 * 
 * @author
 * @version 
 */

require_once 'Zend/Controller/Action.php';


class IndexController extends Zend_Controller_Action 
{
    /**
     * The default action - show the home page
     */
    public $_sesion;
    public $_combos; public $_citObj;
    private $_bonusObj;
    
    function init(){
        //var_dump($redirect); exit;
        $this->_sesion = new Zend_Session_Namespace('login');
        $this->_combos = new CitCombos();
        $this->_citObj = new CitDataGral();
        $this->_hotel = new DbHtHbHotel();
        $this->_bonusObj = new DbHtResBonos();
    }
    
    public function indexAction() 
    {
        $this->view->login = $this->_sesion->uslogin;	
        $this->view->tipdesc = $this->_sesion->tipdesc;
        $this->view->penomcomp = $this->_sesion->penomcomp;		

        $select = $this->_helper->DBAdapter()->select();		
        $select->from(array('t1'=>'ht_seg_usuario_menu'),array('t1.su_id','t1.us_id'));
        $select->join(array('t2'=>'ht_hb_sucursal'),'t1.su_id=t2.su_id',array('su_nombre'));
        $select->where("t1.us_id = '{$this->_sesion->usid}'");
        $select->distinct();
       // echo $select; exit;
        $this->view->data = $select->query()->fetchAll();
        //var_dump($this->view->data);exit;
        $this->view->module = $this->getRequest()->getModuleName();

        $hotel = $this->_hotel->fetchAll("ho_id='{$this->_sesion->hoid}'")->toArray();
        $this->view->sunombre = $hotel['0']['ho_nombre'];

       // $dtaidioma = $this->_citObj->getDataGral('ht_cms_idioma', array('id_id','id_desc',));
        $this->view->idioma  = $this->_helper->Combos->html('ht_cms_idioma',"id_estado = '1'",array('id'=>'id_id','desc'=>'id_desc'),$this->_sesion->lg);
        $this->view->sucursal=$this->_combos->combo($this->_sesion->suid,new DbHtHbSucursal(),"su_id!='00000001'",'su_id','su_nombre',$this->_sesion->suid,'su_nombre');
        //echo $this->view->module;exit;
    }
    
    public function loadAction() 
    {
        $this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	echo '{"events":[],"contents":{},"current":{"type":"html","data":"Hola Cit"
		,"actions":null,"panel":"overview-panel","notbar":false,"preventClose":false,
		"replace":false,"url":""},
		"errorCode":0,"errorMessage":"","notbar":false,"preventClose":false,"replace":false}';	   	
    }
    
    public function listtagAction(){
    	$this->_helper->layout->disableLayout();
    	$this->_helper->viewRenderer->setNoRender();
    	echo '{"events":[],"contents":{},"current":false,"errorCode":0,
		"errorMessage":"","notbar":false,"preventClose":false,"replace":false,"tags":[]}';
    } 
	 
    public function jsonAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();	
        $case = $this->getRequest()->getParam('case',0);
        switch ($case):
            case 'idioma'://Devolver el ID para una nueva habitacion
                $this->_sesion->lg = $_POST['id_id'];

                echo $this->_citObj->json(array('esatdo'=>1));
                break;
            
            case 'bonus'://Devolver el ID para una nueva habitacion
                $data = $this->_citObj->getDataGral('ht_res_bonos', array('bo_dolar','bo_puntos'), "bo_id = 'DO'");
                echo $this->_citObj->json($data);
                break;
            
            case 'form'://Devolver el ID para una nueva habitacion
                exit;
                break;
        endswitch;
    }
    
    public function ajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $select = $this->_helper->DBAdapter()->select();	
        $case = $this->getRequest()->getParam('case',0);
        switch ($case):
            case 'saveBonus'://Devolver el ID para una nueva habitacion
                $evalua = $this->_bonusObj->saveData($_POST);
                $this->_helper->Alert->direct($evalua,'','Puntos asignados correctamente por Dolar.');
                break;
            
            
        endswitch;
    }
}
