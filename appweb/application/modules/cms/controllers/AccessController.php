<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */

require_once 'Zend/Controller/Action.php';

class AccessController extends Zend_Controller_Action 
{
	/**
	 * The default action - show the home page
	 */
	
	public $_usuarioObj;
	
	public $_anioObj;
	
	public $_sesion;
	
	public $_idioma;
	
	function init(){
				
		$this->_usuarioObj = new DbUsuario();

		$this->_anioObj= new DbAnio();
		
		$this->_idioma= new DbIdioma();
		
		$this->_sesion = new Zend_Session_Namespace('login');
	}
 	public function reloginAction()    
    {    
	$this->_helper->layout->disableLayout();
	$this->_helper->viewRenderer->setNoRender();
	$_POST['us_login'] = $_POST['login']['username'];
    $_POST['us_password'] = $_POST['login']['password'];
		if(!empty($_POST)){			
    		$resultuser = $this->_usuarioObj->fetchAll("us_login='{$_POST['us_login']}'")->toArray();    
    			   			
    		if( !empty($resultuser)){   
				if( trim(md5($_POST['us_password'])) == trim($resultuser[0]['us_password']) and !empty($_POST['us_login'])){
	    			$select=$this->_helper->DBAdapter()->select();
					$select->from(array('t1'=>'usuario'),array('us_login','us_id'));
					$select->join(array('t2'=>'vtipo_usuario'),'t1.tip_id = t2.tip_id',array('tip_desc','tip_id','id_id'));
					$select->join(array('t3'=>'persona'),'t1.pe_id = t3.pe_id and t1.cos_id = t3.cos_id',array('pe_id','pe_nomcomp','cos_id'));
					$select->where('t1.us_login=?',$_POST['us_login']);				
					$select->where('t2.id_id=?', $this->_sesion->lg);
								
					$datausuario = $select->query()->fetchAll();

					//var_dump($datausuario,$_POST['us_password'],$resultuser[0]['us_password'],$_POST['us_password']);exit;
					foreach($datausuario as $value){					
						$this->_sesion->penomcomp = $value['pe_nomcomp']; 
						$this->_sesion->uslogin = $value['us_login'];
						$this->_sesion->tipdesc = $value['tip_desc'];
						$this->_sesion->tipcod = $value['tip_id'];
						$this->_sesion->peid = $value['pe_id'];
						$this->_sesion->cosid = $value['cos_id'];
						$this->_sesion->usid = $value['us_id'];						
					}	
					
					$dataanio = $this->_anioObj -> fetchAll("an_vigente = 'S'") -> toArray();
					$this->_sesion->anio = $dataanio[0]['an_id']; 			
					echo '{"events":[],"contents":{},"current":{"type":"html","data":"Hola Cit"
						,"actions":null,"panel":"overview-panel","notbar":false,"preventClose":false,
						"replace":false,"url":""},
						"errorCode":"0",
						"errorMessage":"","notbar":false,
						"preventClose":false,"replace":false}';

					
				}			
			}
		}
   }
}
