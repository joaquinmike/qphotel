<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * FormButtons helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_FormGridExt
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    
    private $_formId;
    private $_button;
    private $_icons;
    private $_configura;
    private $_urls;
    private $_view;
    private $_store;
    /**
     *  
     */
    public function formGridExt()
    {
        return $this;
    }

    
    public function setId($id)
    {
        $this->_formId = $id;
        
    }
    public function setIcons($data = array()){
    	
    	$this->_icons=$data;
    }
 	public function setConfigura($data = array()){
    	$this->_configura = $data;
    }
    public function setUrls($data){
    	$this->_urls = $data;
    }
    
    private function submit ()
    {
    	$view = new Zend_View_Helper_BaseUrl();
    	$cont=0; $delete = false;
    	echo "<script>
    	var elimina; var indice;
    	var win;
    	var tb = new Ext.Toolbar();
		var p = new Ext.Panel({
		   // title: 'My Panel',
		    //collapsible:true,
		    renderTo: 'bodytoolbar',
		    width:'100%',
		    height:30,
		    tbar:tb    
		});";
		
    	if(!empty($this->_view['save'])):
    		$cont++;
    		echo"
    		tb.add('-',
			{
			    icon: '{$view->baseUrl()}/images/icons/save.gif', // icons can also be specified inline
			    cls: 'x-btn-icon',
			    tooltip: '<b>Quick Tips</b><br/>Icon only button with tooltip',
			    handler: function (btn) {
		    		var datos = dataSave();
		    		if(datos == 0)
		    			return false;
		    		
					og.openLink(og.getUrl('{$this->_urls}','ajax/case/save'), {
						hideLoading: true,
						hideErrors: true,
						preventPanelLoad: true, 
						hideLoading:false, 
						post:'data='+datos
					});
		      }}";
    	endif;
    	
    	if(!empty($this->_view['delete'])):
    		if(!empty($cont))
    			echo ",'-',";
    		else
    			echo "tb.add('-',";
    		$delete = true;	
    		$cont++;
    		echo "
    		{
			    icon: '{$view->baseUrl()}/images/icons/fam/cross.gif', // icons can also be specified inline
			    cls: 'x-btn-icon',
			    tooltip: '<b>Quick Tips</b><br/>Icon only button with tooltip',
				handler: function (btn) {
					
					if (!elimina){
				    	alertExtjs('info','Seleccionar-Aun no ha elegido un registro.');  
				    	return false;
			    	}
			    	deleteExtjs();
				}
			}";
    	endif;
			    
		if(!empty($this->_view['new'])):
			if(!empty($cont))
    			echo ",'-',";
    		else
    			echo "tb.add('-',";	
    		$cont++;
			echo "
			{
			    icon: '{$view->baseUrl()}/images/icons/fam/add.gif', // icons can also be specified inline
			    cls: 'x-btn-icon',
			    tooltip: '<b>Quick Tips</b><br/>Icon only button with tooltip',
				handler: function (btn) {
					insertData();
				  }
			}";
		endif;
		//Si no encuentra ninguno
		if($cont > 0):
			echo ",'-');";
		endif;
		//Si esta activo el Eliminar
		if($delete):
			echo "
			function acceptDel(btn){
				og.openLink(og.getUrl('{$this->_urls}','ajax/case/delete'), {
					hideLoading: true,
					hideErrors: true,
					preventPanelLoad: true, 
					hideLoading:false, 
					post:'id='+elimina
				});
				$this->_store.load();
				elimina = '';
			}</script>";
		else:
			echo '</script>';
		endif;
		
		//exit;
    }        
    public function show($url,array $view = Array(),$store = 'grid.store') 
    {
        $this->_urls = $url;
        $this->_view = $view;
        $this->_store = $store;
    	$this->submit();       
    }
    
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
