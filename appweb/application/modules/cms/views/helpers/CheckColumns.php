<?php
/**
 *
 * @author Benjamin
 * @version 
 */
require_once 'Zend/View/Interface.php';

/**
 * FormButtons helper
 *
 * @uses viewHelper Zend_View_Helper
 */
class Zend_View_Helper_CheckColumns
{
    
    /**
     * @var Zend_View_Interface 
     */
    public $view;

    
    private $_formId;
    private $_button;
    private $_icons;
    private $_configura;
    private $_case;
    /**
     *  
     */
    public function checkColumns()
    {
        return $this;
    }

    
    public function setId($id)
    {
        $this->_formId = $id;
        
    }
    public function setIcons($data = array()){
    	
    	$this->_icons=$data;
    }
 	public function setConfigura($data = array()){
    	$this->_configura = $data;
    }
    
    private function submit ()
    {
    	$view = new Zend_View_Helper_BaseUrl();
    	switch ($this->_case):
	    	case 'column':
	    		echo 
	    		"Ext.ux.grid.CheckColumn = function(config){
					this.addEvents({
						click: true
					});
					
					Ext.ux.grid.CheckColumn.superclass.constructor.call(this);
				
					Ext.apply(this, config, {
						init : function(grid){
							this.grid = grid;
							this.grid.on('render', function(){
								var view = this.grid.getView();
							view.mainBody.on('mousedown', this.onMouseDown, this);
							}, this);
						},
				
						onMouseDown : function(e, t){
							if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1){
								e.stopEvent();
								var index = this.grid.getView().findRowIndex(t);
								var record = this.grid.store.getAt(index);
								record.set(this.dataIndex, !record.data[this.dataIndex]);
								this.fireEvent('click', this, e, record);
							}
						},
				
						renderer : function(v, p, record){
							p.css += ' x-grid3-check-col-td';
							return '<div class=".'"'."x-grid3-check-col"."'+(v?'-on':'')+' x-grid3-cc-'+this.id+'".'"'."> </div>';
						}
					});
				
					if(!this.id){
						this.id = Ext.id();
					}
					
					this.renderer = this.renderer.createDelegate(this);
				};
				
				// register ptype
				Ext.preg('checkcolumn', Ext.ux.grid.CheckColumn);
				
				// backwards compat
				Ext.grid.CheckColumn = Ext.ux.grid.CheckColumn;
				
				Ext.extend(Ext.grid.CheckColumn, Ext.util.Observable);
				//and then this type of implementation:";
			    break;
			    
    	endswitch;
    	
    }        
    public function show($case = 'column') 
    {
        $this->_case = $case;
     	//var_dump($this->_case);
    	$this->submit();       
    }
    
    /**
     * Sets the view field 
     * @param $view Zend_View_Interface
     */
    public function setView (Zend_View_Interface $view)
    {
        $this->view = $view;
    }
}
