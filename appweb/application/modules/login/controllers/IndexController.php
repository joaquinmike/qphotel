<?php

/**
 * IndexController - The default controller class
 * 
 * @author cit
 * @version  0.1
 */
require_once 'Zend/Controller/Action.php';

class IndexController extends Zend_Controller_Action {

    /**
     * The default action - show the home page
     */
    public $_usuarioObj;
    public $_sucursalObj;
    public $_sesion;
    public $_idioma;

    function init() {

        $this->_usuarioObj = new DbHtSegUsuario();

        $this->_sucursalObj = new DbHtHbSucursal();

        $this->_idioma = new DbHtCmsIdioma();

        $this->_sesion = new Zend_Session_Namespace('login');

        //$this->_colegioObj = new DbColegioSucursal();
    }

    public function loadAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        echo '{"events":[],"contents":{},"current":{},
		"errorCode":2009,
		"errorMessage":"","notbar":false,
		"preventClose":false,"replace":false}';
    }

    public function indexAction() {
        //$modulo = $this->getRequest()->getModuleName();
        //var_dump(Cit_Init::config()->domain.'/cms'); exit;
        if (!empty($this->_sesion->uslogin)) {
            $this->_redirect('/cms');
        }

        $this->view->idioma = $this->_idioma->fetchAll()->toArray();
        $this->view->idid = $this->_sesion->lg;


        if (!empty($_POST)) {
            //var_dump($_POST); exit;
            //$this->_redirect ( Cit_Init::config()->domain.'/admin');
            //echo "us_login='{$_POST['us_login']}'";exit;
            $resultuser = $this->_usuarioObj->fetchAll("us_login='{$_POST['us_login']}'")->toArray();
            //var_dump($resultuser);exit;
            if (!empty($resultuser[0]['su_id'])) {
                $dat = $this->_sucursalObj->fetchAll("su_id = '{$resultuser[0]['su_maestra']}'")->toArray();
                $this->_sesion->suid = $resultuser[0]['su_id'];
                $this->_sesion->sunombre = $dat[0]['su_nombre'];
            } else {
                $dat = $this->_sucursalObj->fetchAll("su_maestra = 'S'")->toArray();
                $this->_sesion->suid = $dat[0]['su_id'];
                $this->_sesion->sunombre = $dat[0]['su_nombre'];
            }

            if (!empty($resultuser)) {

                if (trim(md5($_POST['us_password'])) == trim($resultuser[0]['us_password']) and !empty($_POST['us_login'])) {
                    $select = $this->_helper->DBAdapter()->select();
                    $select->from(array('t1' => 'ht_seg_usuario'), array('us_login', 'us_id'));
                    $select->join(array('t2' => 'vht_seg_perfil'), 't1.per_id = t2.per_id', array('per_desc', 'per_id', 'id_id'));
                    $select->join(array('t3' => 'ht_seg_persona'), 't1.pe_id = t3.pe_id', array('pe_id', 'pe_nomcomp'));
                    $select->join(array('t4' => 'ht_seg_persona_hotel'), 't1.pe_id = t4.pe_id', array(''));
                    $select->join(array('t5' => 'ht_hb_sucursal'), 't4.ho_id = t5.ho_id', array('su_id', 'ho_id'));
                    $select->where('t1.us_login=?', $_POST['us_login']);
                    $select->where('t2.id_id=?', $this->_sesion->lg);

                    if (!empty($this->_sesion->suid))
                        $select->where('t5.su_id=?', $this->_sesion->suid);

                    //echo $select;exit;

                    $datausuario = $select->query()->fetchAll();

                    foreach ($datausuario as $value) {
                        $this->_sesion->penomcomp = $value['pe_nomcomp'];
                        $this->_sesion->uslogin = $value['us_login'];
                        $this->_sesion->tipdesc = $value['per_desc'];
                        $this->_sesion->perid = $value['per_id'];
                        $this->_sesion->tipcod = $value['per_id'];
                        $this->_sesion->peid = $value['pe_id'];
                        $this->_sesion->usid = $value['us_id'];
                        //$this->_sesion->suid = $value['su_id'];
                        $this->_sesion->hoid = $value['ho_id'];
                    }
                    $this->_sesion->anio = date('Y');

                    /*
                      $selectc=$this->_helper->DBAdapter()->select();
                      $selectc->from(array('t1'=>'colegio_sucursal'),array('cos_nombre','cos_pagweb','cos_id','cos_abrev'));
                      $selectc->joinleft(array('t2'=>'colegio_sucursal_imagen'),"t1.cos_id = t2.cos_id and t2.csi_tipo='1'",array(''));
                      $selectc->joinleft(array('t3'=>'imagenes'),'t2.img_id = t3.img_id',array('img_img1'));
                      $selectc->where("t1.cos_id = '{$this->_sesion->cosid}'");
                      ///echo $selectc;exit;
                      $datacolegio = $selectc->query()->fetchAll();
                      foreach($datacolegio as $value){
                      $this->_sesion->cosnombre=$value['cos_nombre'];
                      $this->_sesion->cospagweb=$value['cos_pagweb'];
                      if(empty($value['img_img1']))
                      $this->_sesion->imgimg1=Cit_Init::config()->domain.'/favicon.ico';
                      else
                      $this->_sesion->imgimg1=Cit_Init::config()->subdomain.'/images'.$value['img_img1'];
                      $this->_sesion->cosabrev=$value['cos_abrev'];
                      }
                      //var_dump($datacolegio);exit; */
                    $this->_redirect(Cit_Init::config()->domain . '/cms');
                } else {
                    $this->_redirect(Cit_Init::config()->subdomain);
                }
            } else {
                $this->_redirect(Cit_Init::config()->subdomain);
            }
        }
    }

}
