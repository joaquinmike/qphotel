<?php

/**
 * My new Zend Framework project
 * 
 * @author  
 * @version 
 */
require_once 'Zend/Controller/Plugin/Abstract.php';
require_once 'Zend/Controller/Front.php';
//require_once 'Zend/Controller/Front.php';
require_once 'Zend/Controller/Request/Abstract.php';
require_once 'Zend/Controller/Action/HelperBroker.php';
require_once 'Zend/Loader.php';
require_once 'Zend/Loader/Autoloader.php';
require_once 'Cit/Profiler.php';

/**
 * 
 * Initializes configuration depndeing on the type of environment 
 * (test, development, production, etc.)
 *  
 * This can be used to configure environment variables, databases, 
 * layouts, routers, helpers and more
 *   
 */
class Initializer extends Zend_Controller_Plugin_Abstract {

    
    protected  $_ipCliente;
    protected static $_config;

    /**
     * @var string Current environment
     */
    protected $_env;
    /*
     * almacen de cache
     * */ 
    public static $_cache = null;

    /**
     * @var Zend_Controller_Front
     */
    protected $_front;

    /**
     * @var string Path to application root
     */
    protected $_nameModule = 'default';
    /*
     * name module 
     * */
    protected $_root;

    /**
     * Constructor
     *
     * Initialize environment, root path, and configuration.
     * 
     * @param  string $env 
     * @param  string|null $root 
     * @return void
     */
    public function __construct($env, $root = null) {
       
        isset($_SERVER["HTTP_CLIENT_IP"])?
            $this->_ipCliente=$_SERVER["HTTP_CLIENT_IP"]:
            $this->_ipCliente=$_SERVER["REMOTE_ADDR"];
        $writer = new Zend_Log_Writer_Stream(BP
                . '/var/log/application.log');//, ,'a+'
        $log = new Zend_Log();
        $log->addWriter($writer);
        $log->setTimestampFormat('Ymd H:i:s');        
        Zend_Registry::set('Log', $log);      
        
        $lg = new Zend_Session_Namespace('login');

        error_reporting(0);
        $this->_setEnv($env);
        if (null === $root) {
            $root = realpath(dirname(__FILE__) . '/../');
        }
        $this->_root = $root;

        $this->initPhpConfig();

        $this->_front = Zend_Controller_Front::getInstance();
        // set the test environment parameters        
        Zend_Session::start();

        Cit_Init::setRoot($root);

        Cit_Init::setEnv($this->_env);
        
        Zend_Date::setOptions(array('format_type' => 'php'));
        
        Cit_Init::setConfig('/config/hotel.ini', $this->_env);
        @define('MP', dirname(BP) . '/html');
        @define('PADMIN', '0001');
        @define('htID', '01');
        Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
        
    }

    /**
     * Initialize environment
     * 
     * @param  string $env 
     * @return void
     */
    protected function _setEnv($env) {
        $this->_env = $env;
    }

    /**
     * Initialize Data bases
     * 
     * @return void
     */
    public function initPhpConfig() {
        
    }

    /**
     * Route startup
     * 
     * @return void
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request) {

        $this->initHelpers();
        $this->initView();
        $this->initPlugins();
        $this->initRoutes();
        $this->initControllers();
        //$this->initDb();
    }

    /**
     * Initialize data bases
     * 
     * @return void
     */
    public function preDispatch() {
        $nameModule = $this->getRequest()->getModuleName();
        $nameController = $this->getRequest()->getControllerName();
        switch ($nameModule):
            case 'default':
                $lg = new Zend_Session_Namespace('web');
                if ($nameController == 'index')
                    $lg->su_id = '00000001';
                
                break;
            default:
                $lg = new Zend_Session_Namespace('login');
                break;
        endswitch;
        if (empty($lg->su_id)) {
            $lg->su_id = '00000001';
        }
        $paramss = $this->getRequest()->getParam('suc', 'null');
        if ($paramss != 'null') {
            $lg->su_id = $paramss;
        }
        if ($lg->su_id == 'img') {
            $lg->su_id = $_SESSION['login']['historials'];
        } else {
            $_SESSION['login']['historials'] = $lg->su_id;
        }

        /** ******************************* */    
        if (empty($lg->lg)) {
            $lg->lg = 'ES';//$this->getLenguaje($this->_ipCliente);
        }
        $params = $this->getRequest()->getParam('lg', 'null');
        if ($params != 'null') {
            $lg->lg = $params;
        }
        if ($lg->lg == 'img') {
            $lg->lg = $_SESSION['login']['historial'];
        } else {
            $_SESSION['login']['historial'] = $lg->lg;
        }
        Cit_Init::setTranslate($lg->lg);
        $this->initDb();
        $this->initControllers(strtolower($nameModule));
        $this->initView(strtolower($nameModule));
        
//        $frontendOptions = array(
//            'lifetime' => 7200, // cache lifetime of 2 hours
//            'automatic_serialization' => true
//        );
//
//        $backendOptions = array(
//            'cache_dir' => dirname(MP) . '/appweb/var/cache/'  // Directory where to put the cache files
//        );
//        // getting a Zend_Cache_Core object
//        $cache = Zend_Cache::factory('Core',
//            'File',
//            $frontendOptions,
//            $backendOptions);
//
//        Zend_Registry::set('Cache', $cache);
//        var_dump($cache); exit;
    }

    public function initDb() {
        /* $nameModule = $this->getRequest()->getModuleName(); 
          switch ($nameModule){
          case 'publicidad' :Cit_Init::setConfig('/config/publicidad.ini',$this->_env);break;
          } */


        $db = Zend_Db::factory(Cit_Init::config()->database);
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
  
    }

    /**
     * Initialize action helpers
     * 
     * @return void
     */
    public function initHelpers() {

        Zend_Controller_Action_HelperBroker::addPrefix('Cit_Controller_Action_Helper');
    }

    /**
     * Initialize view 
     * 
     * @return void
     */
    public function initView($nameModule = 'default') {

        // Bootstrap layouts

        Zend_Layout::startMvc(array(
            'layoutPath' => $this->_root . '/application/modules/' . $nameModule . '/layouts',
            'layout' => 'main'
        ));
    }

    /**
     * Initialize plugins 
     * 
     * @return void
     */
    public function initPlugins() {

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->initView();
        $viewRenderer->view->addHelperPath('Zend/View/Helper/', 'Zend_View_Helper');
        $viewRenderer->view->addHelperPath('Cit/View/Helper/', 'Cit_View_Helper');
    }

    /**
     * Initialize routes
     * 
     * @return void
     */
    public function initRoutes() {
        
    }

    /**
     * Initialize Controller paths 
     * 
     * @return void
     */
    public function initControllers($nameModule = 'default') {

        $this->_front->addModuleDirectory($this->_root . '/application/modules');
        $this->_front->setDefaultModule($nameModule);
        $this->_front->registerPlugin(new Plugin_View());
        $this->_front->addControllerDirectory($this->_root . '/application/modules/' . $nameModule . '/controllers', 'default');
        $this->_front->throwExceptions(false);
    }
   /**
    *var Inicial Pais 
    */
   function getLenguaje($ip)
   {
       
       $ch = curl_init("http://api.hostip.info/country.php?ip=".$ip);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       $country_code = curl_exec($ch);        
       $ingles = array('AU','HK','GB','US','CA');       
       if(in_array($country_code,$ingles)) {
           $lg ='EN';
       }else{
           if($country_code=='XX'){
               $colIps=array('190.114.248.27');
                if(in_array($ip,$colIps)) {
                    $lg ='ES';
                }else{
                    $lg ='EN';
                }
           }else{
               $lg='ES';
           }
           
       }
       return $lg;
   }
//obtención de código de país:
  //  $pais = strtolower(getCountry($ip));
}

