<?php
/**
 * Ci -Karelinda..Sextion
 * 
 * @category 	CIT
 * @author 	 	
 * @copyright	
 * @version 
 */

date_default_timezone_set('America/Los_Angeles');

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);
define('BP', dirname(dirname(__FILE__))); //Base Path aplicacion 
//echo BP.' -';
//define('MP', dirname(BP) .'/html'); //Media Path, files js,css, img, etc

//$environment = parse_ini_file(BP . '/application/config/application.ini');
//define('ENV', $environment['environment']);
$paths[] = get_include_path();
$paths[] = BP . DS . 'application' . DS . 'modules';
$paths[] = BP . DS . 'application' . DS . 'core' . DS . 'models';
$paths[] = BP . DS . 'application' . DS . 'core' . DS . 'models'. DS . 'cit';
$paths[] = BP . DS . 'application' . DS . 'core' . DS . 'models'. DS . 'db';

$paths[] = BP . DS . 'application' . DS . 'core';
$paths[] = BP . DS . 'application' . DS . 'core' . DS . 'forms';

$paths[] = dirname(BP). DS . 'library';
$paths[] = dirname(BP). DS . 'library'.DS.'Cit';
//$paths[] = BP . DS . 'library'.DS.'Spreadsheet';
$paths[] = dirname(BP). DS . 'library'.DS.'Zend';
//$paths[] = BP . DS . 'library/Doctrine';

$app_path = implode(PS, $paths);

set_include_path($app_path);

require_once 'Initializer.php';
Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
//Zend_Controller_Front::getInstance();
$frontController = Zend_Controller_Front::getInstance(); 

 
// Change to 'production' parameter under production environemtn
$frontController->registerPlugin(new Initializer('development'));    

// Dispatch the request using the front controller. 
$frontController->dispatch(); 
