<?php 
class CitMenu  extends  Cit_Db_Table_Abstract
{
	public $_mnu;
	public $_menuObj;
	public function menu(){
			$login = new Zend_Session_Namespace('login');
			$this->_menuObj = new DbHtSegMenu();
			$select = $this->_menuObj->select()->setIntegrityCheck(false);  
			
	    	$select->from(array('t1'=>'vht_seg_menu'));
	    	$select->join(array('t2'=>'menu_sucursal'), 't1.me_id = t2.me_id',array(''));
	    	$select->join(array('t3'=>'ht_seg_usuario_menu'), 't2.me_id = t3.me_id  and t2.su_id = t3.su_id',array(''));
	    	$select->where("t1.ht_me_id is null or t1.ht_me_id = ''");
	    	$select->where("t1.id_id = '{$login->lg}' and t2.su_id='{$login->suid}' and t3.us_id='$login->usid' and me_estado = '1'");
	    	//$select->where("t2.su_id = '$login->suid' and t2.us_id='$login->usid' and t1.id_id='$login->lg' and t1.me_estado='1'");
	    	$select->order('t1.me_orden');
	    	//$select->order('t1.me_desc');
			//echo $select; exit;
	    	$data = $this->_menuObj->fetchAll($select)->toArray();	    	
	    	$workspace = array();$i=0;$parent=0;$depth=1;
	    	
			foreach ($data as $indice=>$value){						
						$i++;
						$parent = $i;
						$depth++;
						$workspace[] =array(
							"id" =>$i,
							"link"=>$value['me_link'],
							"name" => $value['me_desc'],
							"color" => '4',
							"parent" => '0',
							"realParent" => '0',
							"depth" => $indice
						); 
						$select = $this->_menuObj->select(); 
				    	$select->setIntegrityCheck(false);
				    	$select->from(array('t1'=>'vht_seg_menu'));
				    	$select->join(array('t2'=>'menu_sucursal'), 't1.me_id = t2.me_id ',array('su_id'));
				    	$select->join(array('t3'=>'ht_seg_usuario_menu'), 't2.me_id = t3.me_id  and t2.su_id = t3.su_id',array(''));
				    	$select->where("ht_me_id ='{$value['me_id']}' and t1.id_id = '{$login->lg}' and t2.su_id='{$login->suid}' and t3.us_id='$login->usid' and me_estado = '1'");
				    	//$select->where("t2.su_id = '$login->suid' and t2.us_id='$login->usid' and t1.id_id='$login->lg' and t1.me_estado='1'");	    	   	
				    	$select->order('t1.me_orden');
	    				//$select->order('t1.me_desc');
	    				//echo $select; exit;
				    	$data1 = $this->_menuObj->fetchAll($select)->toArray();
						foreach($data1 as $indiceone => $valueone){
							$i++;
							$parenttwo = $i;
							$workspace[] =array(
							"id" => $i,
							"link"=>$valueone['me_link'],
							"name" => $valueone['me_desc'],
							"color" => '1',
							"parent" => $parent,
							"realParent" => $parent,
							"depth" => $indice,
							"codigo" => $valueone['me_id'].'-'.$valueone['su_id']
							); 	
								
							$select = $this->_menuObj->select(); 
					    	$select->setIntegrityCheck(false);
					    	$select->from(array('t1'=>'vht_seg_menu'));
					    	$select->join(array('t2'=>'menu_sucursal'), 't1.me_id = t2.me_id ',array('su_id'));
				    		$select->joinleft(array('t3'=>'ht_seg_usuario_menu'), 't2.me_id = t3.me_id  and t2.su_id = t3.su_id',array(''));
					    	$select->where("ht_me_id ='{$valueone['me_id']}'  and t1.id_id = '{$login->lg}'");
					    	$select->where("t3.su_id = '$login->suid' and t3.us_id='$login->usid' and t1.id_id='$login->lg' and t1.me_estado='1'");
					    	$select->order('t1.me_orden');
	    					//$select->order('t1.me_desc');  	    	
						    	$data2 = $this->_menuObj->fetchAll($select)->toArray();
						    	if(!empty($data2)){
						    		
									foreach($data2 as $indicetwo => $valuetwo){
										$i++;
										$workspace[] =array(
										"id" => $i,
										"link"=>$valuetwo['me_link'],
										"name" => $valuetwo['me_desc'],
										"color" => '2',
										"parent" => $parenttwo,
										"realParent" => $parenttwo,
										"depth" => $indice,
										"codigo" => $valuetwo['me_id'].'-'.$valuetwo['su_id']
										); 	
									}
						    	}
						}
						
						
					}
		
	    	return $workspace;
}	
	
public function menupubli(){
	$login = new Zend_Session_Namespace('login');
			$select = $this->select()->setIntegrityCheck(false);    	
	    	$select->from(array('t1'=>'vmenu'));
	    	//$select->join(array('t2'=>'usuario_menu'), 't1.me_id = t2.me_id and t1.si_id = t2.si_id',array(''));	    		    	    	
	    	$select->where("(t1.me_me_id is null or t1.me_me_id = '') and t1.si_id = '$login->siid'");
	    	$select->where("t1.id_id='$login->lg' and t1.me_estado='1'");
	    	//echo $select;exit;
	    	$data = $this->fetchAll($select)->toArray();	    	
	    	$workspace = array();$i=0;$parent=0;$depth=1;
			foreach ($data as $indice=>$value){						
						$i++;
						$parent = $i;
						$depth++;
						$workspace[] =array(
							"id" =>$i,
							"link"=>$value['me_link'],
							"name" => $value['me_desc'],
							"color" => '4',
							"parent" => '0',
							"realParent" => '0',
							"depth" => $indice
						); 						
					}
					
	    	return $workspace;
}	
	
public function menuv2($siscod='01',$menupad='menuu.menupad IS NULL',$mnu='',$usucod='SIE',$estado='S',$colcod='0000'){
	    	$select = $this->select(); 
	    	$select->setIntegrityCheck(false)    	
	    	->join('usu_menu','usu_menu.menucod=menuu.menucod','')
	    	->join('menuu','usu_menu.menucod=menuu.menucod',
	    	array('menuu.menucod','menuu.siscod',
	    	'menuu.menunom','w_im_menu.url','w_im_menu.nom_imagen',
	    	'menuu.colcod','menuu.menupad'))
	    	->join('usuario','usuario.usucod=usu_menu.usucod','')
	    	->join('tip_usu','usuario.siscod=tip_usu.siscod','')
	    	->joinLeft('w_im_menu','w_im_menu.menucod=menuu.menucod AND
	    	w_im_menu.siscod=menuu.siscod and
	    	w_im_menu.colcod=menuu.colcod ','')
	    	->where("usu_menu.usucod = '$usucod' AND usu_menu.estado = '$estado' ")
	    	->where("menuu.siscod ='$siscod' and $menupad and (menuu.colcod = '$colcod' or menuu.colcod = '0000' )")
	    	->where("menuu.TIPO= '0'")
	    	->where("usu_menu.siscod ='$siscod'")
	    	->distinct('menuu.menucod') 	
	    	->order('menuu.menucod');
	    	/*echo $select;
	    	exit;*/
	    	
	    	$row=$this->fetchAll($select)->toArray();	  
	    	//var_dump($row); exit;
	    	if(!empty($row)){
		    		foreach($row as $value){ 						
		    			$this->_mnu.='		    			
		    			<li class="top">  
		    				<a class="top_link" href="'.$value['url'].'/cod/'.$value['menucod'].$value['siscod'].$value['colcod'].'">		    			
		    				 <span class="down"><img border="0px" class="img_iko" src ="'.Cit_Init::config()->webhost.'/images/menu/'.$value['nom_imagen'].'" />'.$value['menunom'].'</span></a>';	
		    						    					    					
		    					$selectone = $this->select(); 
	    						$selectone->setIntegrityCheck(false)    	
						    	->join('usu_menu','usu_menu.menucod=menuu.menucod','')
						    	->join('menuu','usu_menu.menucod=menuu.menucod',array('menuu.menucod','menuu.siscod','menuu.menunom','w_im_menu.url','w_im_menu.nom_imagen','menuu.colcod','menuu.menupad'))
						    	->join('usuario','usuario.usucod=usu_menu.usucod','')
						    	->join('tip_usu','usuario.siscod=tip_usu.siscod','')
						    	->joinLeft('w_im_menu','w_im_menu.menucod=menuu.menucod AND
						    	w_im_menu.siscod=menuu.siscod and
						    	w_im_menu.colcod=menuu.colcod ','')
						    	->where("usu_menu.usucod = '$usucod' AND usu_menu.estado = '$estado' ")
						    	->where("menuu.siscod ='$siscod' and menuu.menupad = '{$value['menucod']}' and (menuu.colcod = '$colcod' or menuu.colcod = '0000' )")
						    	->where("menuu.TIPO= '0'")
						    	->where("usu_menu.siscod ='$siscod'")->distinct('menuu.menucod')->order('menuu.menucod');;	
						    	
						    	$rowone=$this->fetchAll($selectone)->toArray();			    				
		    					 
		    					if(!empty($rowone)){ 		    						
		    						$this->_mnu.='<ul class="sub">';
				    					foreach($rowone as $valueone){	
				    						
						    					$selectwo = $this->select(); 
					    						$selectwo->setIntegrityCheck(false)    	
										    	->join('usu_menu','usu_menu.menucod=menuu.menucod','')
										    	->join('menuu','usu_menu.menucod=menuu.menucod',array('menuu.menucod','menuu.siscod','menuu.menunom','w_im_menu.url','w_im_menu.nom_imagen','menuu.colcod','menuu.menupad'))
										    	->join('usuario','usuario.usucod=usu_menu.usucod','')
										    	->join('tip_usu','usuario.siscod=tip_usu.siscod','')
										    	->joinLeft('w_im_menu','w_im_menu.menucod=menuu.menucod AND
										    	w_im_menu.siscod=menuu.siscod and
										    	w_im_menu.colcod=menuu.colcod ','')
										    	->where("usu_menu.usucod = '$usucod' AND usu_menu.estado = '$estado' ")
										    	->where("menuu.siscod ='$siscod' and menuu.menupad = '{$valueone['menucod']}' and (menuu.colcod = '$colcod' or menuu.colcod = '0000' )")
										    	->where("menuu.TIPO= '0'")
										    	->where("usu_menu.siscod ='$siscod'")
										    	->distinct('menuu.menucod');
										    	
										    	$rowtwo=$this->fetchAll($selectwo)->toArray();	
										    	if(!empty($rowtwo))
										    		$fli =' class="fly" ';
										    	else
										    		$fli='class="transicion"';										    	
										    		
									    		$this->_mnu.='<li><a '.$fli.'  href="'.$valueone['url'].'/cod/'.$valueone['menucod'].$valueone['siscod'].$valueone['colcod'].'">  		    			
					    				 		<span class="down">'.$valueone['menunom'].'</span></a>';											    	
										    	if(!empty($rowtwo)){
											    	$this->_mnu.='<ul>';
											    		foreach($rowtwo as $valuetwo){
											    			$this->_mnu.='<li  ><a  href="'.$valuetwo['url'].'/cod/'.$valuetwo['menucod'].$valuetwo['siscod'].$valuetwo['colcod'].'">  		    			
										    				 <span class="down">'.$valuetwo['menunom'].'</span></a>';	
										    				$this->_mnu.='</li>';
											    		}
											    	 $this->_mnu.='</ul>';
				    							}
										   $this->_mnu.='</li>';
				    					}
				    					
		    						$this->_mnu.='</ul>';
		    					}		    				
		    		}	
		    	$this->_mnu.='</li>';	    	
	    	}
	    	   	
    	return $this->_mnu;
	}
    
    public function menuMobile()
    {
        $loginMob = new Zend_Session_Namespace('login');
        $menu = '';
        $select = $this->select()->setIntegrityCheck(false);    	
	    	$select->from(array('t1'=>'vmenu'));
	    	$select->join(array('t2'=>'usuario_menu'), 't1.me_id = t2.me_id and t1.si_id = t2.si_id',array(''));	    		    	    	
	    	$select->where("(t1.me_me_id is null or t1.me_me_id = '') and t1.si_id = '$loginMob->siid'");
	    	$select->where("t2.cos_id='$loginMob->cosid' and t2.us_id='$loginMob->usid' and t1.id_id='$loginMob->lg'");
	    	//echo $select;exit;
	    	$data = $this->fetchAll($select)->toArray();	    	
	    	$workspace = array();
            //var_dump($data);
			foreach ($data as $indice=>$value){															
				$select = $this->select(); 
		    	$select->setIntegrityCheck(false);
		    	$select->from(array('t1'=>'vmenu'));
		    	$select->join(array('t2'=>'usuario_menu'), 't1.me_id = t2.me_id and t1.si_id = t2.si_id',array(''));	    		    	    	
		    	$select->where("me_me_id ='{$value['me_id']}' and t1.si_id = '$loginMob->siid'");
		    	$select->where("t2.cos_id='$loginMob->cosid' and t2.us_id='$loginMob->usid' and t1.id_id='$loginMob->lg'");	    	   	
		    	//echo $select;exit;	    	    	
		    	$data1 = $this->fetchAll($select)->toArray();
                //var_dump($data1); exit;
				foreach($data1 as $indiceone => $valueone){
                    $menu .= '{ xtype:"button", text: "'.$valueone['me_desc'].'", plugins:[new simfla.ux.plugins.iPhoneDesktopButton({image: "'.Cit_Init::config()->domain.'/images'.$valueone['me_image'].'"})],';
                    $menu .= 'handler: function(b){ b.plugins[0].onTap(function(){ location.href=domainName+"'.$valueone['me_link'].'";});}}';
                    $menu .= ','; 
              }
        }
        if(empty($menu))
            $menu = '{}';
        else
            $menu = substr($menu, 0, strlen($menu) - 1);
        return $menu;
    }
}


