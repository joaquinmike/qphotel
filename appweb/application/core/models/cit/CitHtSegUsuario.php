<?php
/**
 * HtSegUsuario model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSegUsuario
{

    protected $_data = array(
        'us_id' => true,
        'per_id' => true,
        'pe_id' => true,
        'ho_id' => true,
        'us_login' => true,
        'us_password' => true
        );


}
