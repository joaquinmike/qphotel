<?php
/**
 * HtSerServicio model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSerServicio
{

    protected $_data = array(
        'ser_id' => true,
        'set_id' => true,
        'ser_estado' => true,
        'ser_razsocial' => true,
        'ser_ruc' => true,
        'ser_direccion' => true,
        'ser_telefono1' => true,
        'ser_telefono2' => true,
        'ser_web' => true,
        'ser_correo' => true,
        'ser_precio' => true
        );


}
