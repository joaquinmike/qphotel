<?php
/**
 * HtCmsImagenes model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtCmsImagenes
{

    protected $_data = array(
        'im_id' => true,
        'im_image1' => true,
        'im_image2' => true,
        'im_image3' => true,
        'im_image4' => true,
        'im_image5' => true,
        'im_estado' => true
        );


}
