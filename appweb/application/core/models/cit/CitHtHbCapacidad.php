<?php
/**
 * HtHabCapacidad model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHabCapacidad
{

    protected $_data = array(
        'hbc_id' => true,
        'tih_id' => true,
        'hbc_capacidad' => true,
        'hbc_precio' => true
        );


}
