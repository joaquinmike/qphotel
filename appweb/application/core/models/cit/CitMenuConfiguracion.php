<?php
/**
 * MenuConfiguracion model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitMenuConfiguracion
{

    protected $_data = array(
        'mec_id' => true,
        'me_id' => true,
        'si_id' => true,
        'cos_id' => true,
        'mec_conf' => true
        );
	
    public function getConfig($data){
    	//$obj = new DbMenuConfiguracion();
    	$usuariomenu = new DbHtSegUsuarioMenu();
    	//$datapermiso = $usuariomenu->fetchAll("me_id='{$data['me_id']}' and si_id='{$data['si_id']}' and cos_id='{$data['cos_cos_id']}' and us_id='{$data['us_id']}'")->toArray();
    	
    	//$dataconf = $obj->fetchAll(/*"me_id='{$data['me_id']}' and si_id='{$data['si_id']}' and cos_id='{$data['cos_id']}' and cos_cos_id='{$data['cos_cos_id']}'"*/)->toArray();
    	
   		//if($data['si_id']=='01'){
    		//$dataconf = $obj->fetchAll("me_id='{$data['me_id']}' and si_id='{$data['si_id']}' and cos_id='{$data['cos_id']}' and cos_cos_id='{$data['cos_cos_id']}'")->toArray();
    	//}
    	$newarray = array(); 
    	if(!empty($dataconf[0]['mec_conf'])){
	    	$xml = simplexml_load_string($dataconf[0]['mec_conf']);
	    			
	    	$newarray['news'] = (string)$xml->Option->news;
	    	$newarray['save'] = (string)$xml->Option->save;
	    	$newarray['print'] = (string)$xml->Option->print;
	    	$newarray['deletes'] = (string)$xml->Option->deletes;
	    	$newarray['seach'] = (string)$xml->Option->seach;
	    	$newarray['pesos'] = (string)$xml->Option->pesos;
	    	$newarray['coment'] = (string)$xml->Option->coment;
	    	$newarray['next'] = (string)$xml->Option->next;
    	}
    	else{
    		$newarray['news'] = 'true';
	    	$newarray['save'] = 'true';
	    	$newarray['print'] = 'true';
	    	$newarray['deletes'] = 'true';
	    	$newarray['seach'] = 'true';
	    	$newarray['pesos'] = 'true';
	    	$newarray['coment'] = 'true';
	    	$newarray['next'] = 'true';
    	} 
    	switch (@$datapermiso[0]['usm_permiso']){
	    		case 1:	$newarray['news'] = 'false';
	    				$newarray['save'] = 'false';
	    				$newarray['deletes'] = 'false';
	    				break;
	    		case 2:	$newarray['deletes'] = 'false';
	    				break;
	    		default:	
	    				break;		
	    	}  	
    	return $newarray;
    	
    }

}