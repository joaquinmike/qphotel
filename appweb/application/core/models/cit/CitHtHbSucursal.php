<?php
/**
 * HtHbSucursal model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHbSucursal
{

    protected $_data = array(
        'su_id' => true,
        'ho_id' => true,
        'uscb_id' => true,
        'su_nombre' => true,
        'su_estado' => true,
        'su_telefono1' => true,
        'su_telefono2' => true,
        'su_ruc' => true,
        'su_direccion' => true,
        'su_desc' => true
        );


}
