<?php
/**
 * HtHabIdiomaHabitacion model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHabIdiomaHabitacion
{

    protected $_data = array(
        'hb_id' => true,
        'id_id' => true,
        'ht_hab_desc' => true
        );


}
