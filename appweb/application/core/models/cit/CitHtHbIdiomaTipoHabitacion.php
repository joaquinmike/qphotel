<?php
/**
 * HtHabIdiomaTipoHabitacion model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHabIdiomaTipoHabitacion
{

    protected $_data = array(
        'tih_id' => true,
        'id_id' => true,
        'tih_desc' => true
        );


}
