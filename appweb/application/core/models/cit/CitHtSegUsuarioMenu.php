<?php
/**
 * HtSegUsuarioMenu model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSegUsuarioMenu
{

    protected $_data = array(
        'us_id' => true,
        'per_id' => true,
        'pe_id' => true,
        'su_id' => true,
        'me_id' => true,
        'usm_permiso' => true,
        'usm_estado' => true
        );


}
