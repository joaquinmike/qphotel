<?php
/**
 * HtSrvIdiomaTipo model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSrvIdiomaTipo
{

    protected $_data = array(
        'set_id' => true,
        'id_id' => true,
        'tip_titulo' => true,
        'tip_desc' => true
        );


}
