<?php
/**
 * HtResReservaDetalle model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtResReservaDetalle
{

    protected $_data = array(
        'red_id' => true,
        'res_id' => true,
        'cl_id' => true,
        'hb_id' => true,
        'ser_id' => true,
        'res_fecha_inicio' => true,
        'res_fecha_fin' => true,
        'res_precio' => true,
        'ta_id' => true,
        'ba_id' => true,
        'res_estado' => true
        );


}
