<?php
/**
 * HtSegPersona model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSegPersona
{

    protected $_data = array(
        'pe_id' => true,
        'uscb_id' => true,
        'pe_nombre' => true,
        'pe_paterno' => true,
        'pe_materno' => true,
        'pe_fec_nacimiento' => true,
        'pe_lugar_nacimiento' => true,
        'pe_estado' => true,
        'pe_dni' => true,
        'pe_pais' => true,
        'pe_nacionalidad' => true
        );


}
