<?php
/**
 * HtHabTipoHabitacion model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHabTipoHabitacion
{

    protected $_data = array(
        'tih_id' => true,
        'ht__tih_id' => true,
        'tih_estado' => true,
        'tih_stock' => true
        );


}
