<?php

/**
 * HtResKardex model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtResKardex {

    protected $_data = array(
        'ka_id' => true,
        'hb_id' => true,
        'ka_dia' => true,
        'ka_mes' => true,
        'ka_anio' => true
    );

    public function formKardexInventario(array $data = Array(), $meses, $avail) {
        $field = '';
        $colum = '';
        $group = '';
        foreach ($data as $value):
            $name = 'dia_' . $value['mes'] . '_' . $value['num'];
            $field .= "{ name : '{$name}'},";

            $hoy = date('d/m/Y');
            if ($hoy == $value['fecha']):
                $css = ",css:'background-color: #C6E6FD;'";
                $value['num'] = '<b>' . $value['num'] . '</b>';
            else:
                if ($value['dia'] == 'viernes' or $value['dia'] == 'sábado')
                    $css = ",css:'background-color: #FEDAD8;'"; //style:'padding-top:+3px; margin-left:20px'
                else
                    $css = '';
            endif;

            //{dataIndex: 'pe_nomcomp', header: 'Nombre', width: 190,locked: true,align:'left'},
            $colum .= "{dataIndex: '{$name}', header: '{$value['num']}', width: 25,align:'center' {$css}},";
        endforeach;

        $form['field'] = substr($field, 0, strlen($field) - 1);
        $form['colum'] = substr($colum, 0, strlen($colum) - 1);
        $i = 0;

        foreach ($meses as $result):
            if (empty($i)):
                $form['anio'] = $result['an_id'];
                $form['mes_id'] = $result['mes_id'];
                $i++;
            endif;
            $group .= "{header: '{$result['name']}', colspan: {$result['num']}, align: 'center'},";
            $anio = $result['an_id'];
        endforeach;

        $form['header'] = substr($group, 0, strlen($group) - 1);

        return $form;
    }

}
