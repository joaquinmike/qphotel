<?php
/**
 * HtSrvIdiomaServicio model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSrvIdiomaServicio
{

    protected $_data = array(
        'ser_id' => true,
        'id_id' => true,
        'ser_titulo' => true,
        'ser_desc' => true
        );


}
