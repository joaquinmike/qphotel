<?php
/**
 * HtCmsIdiomaArticulo model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtCmsIdiomaArticulo
{

    protected $_data = array(
        'id_id' => true,
        'art_id' => true,
        'art_titulo' => true,
        'art_contenido' => true
        );


}
