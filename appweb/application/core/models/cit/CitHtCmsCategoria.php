<?php
/**
 * HtCmsCategoria model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtCmsCategoria
{

    protected $_data = array(
        'cat_id' => true,
        'me_id' => true,
        'su_id' => true,
        'cat_estado' => true,
        'cat_tipo' => true
        );


}
