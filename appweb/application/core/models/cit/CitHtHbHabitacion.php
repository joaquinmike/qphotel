<?php
/**
 * HtHbHabitacion model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHbHabitacion
{

    protected $_data = array(
        'hb_id' => true,
        'tih_id' => true,
        'su_id' => true,
        'pi_id' => true,
        'hb_capacidad' => true,
        'hb_numero' => true,
        'hb_estado' => true,
        'hb_precio' => true,
        'hb_desc' => true
        );


}
