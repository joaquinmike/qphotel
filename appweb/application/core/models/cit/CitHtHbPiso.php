<?php
/**
 * HtHbPiso model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHbPiso
{

    protected $_data = array(
        'pi_id' => true,
        'su_id' => true,
        'pi_desc' => true,
        'pi_numero' => true
        );


}
