<?php
/**
 * HtHabAccesorios model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtHabAccesorios
{

    protected $_data = array(
        'ac_id' => true,
        'ac_estado' => true,
        'ac_precio' => true,
        'ac_tipo' => true
        );


}
