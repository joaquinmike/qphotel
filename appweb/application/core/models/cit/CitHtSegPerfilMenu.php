<?php
/**
 * HtSegPerfilMenu model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSegPerfilMenu
{

    protected $_data = array(
        'me_id' => true,
        'su_id' => true,
        'per_id' => true,
        'pem_permiso' => true,
        'pem_estado' => true
        );


}
