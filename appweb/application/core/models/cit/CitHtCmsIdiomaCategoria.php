<?php
/**
 * HtCmsIdiomaCategoria model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtCmsIdiomaCategoria
{

    protected $_data = array(
        'id_id' => true,
        'cat_id' => true,
        'cat_titulo' => true
        );


}
