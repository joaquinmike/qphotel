<?php
/**
 * HtSegPerfil model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class CitHtSegPerfil
{

    protected $_data = array(
        'per_id' => true,
        'per_desc' => true,
        'per_estado' => true
        );


}
