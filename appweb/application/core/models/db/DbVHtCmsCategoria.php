<?php
/**
 * Anio Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbVHtCmsCategoria extends Cit_Db_Table_Abstract
{

    protected $_name = 'vht_cms_categoria';

    protected $_primary = array('cat_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function getDataId()
    {
        $select = $this->select();
        if(!empty($data['where'])){
        	$select->where("");
        }
        if(!empty($data['order'])){
        	$select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }


}
