<?php

/**
 * HtCmsArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsPromocion extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_promocion';
    protected $_primary = array('pro_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    const PATH_PROMO = 'promocion';
    
    /**
     * Devolver Datos Cms Articulo
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

    public function saveData(array $data = array()) {
        $idiomaObj = new DbHtCmsIdiomaPromocion();
        $sesion = new Zend_Session_Namespace('login');
        $fecOjb = new Cit_Db_CitFechas();
        $this->_db->beginTransaction();
        $citObj = new CitDataGral();
        //var_dump($data);exit;	
        try {
            $data['pro_id'] = strtoupper($data['pro_id']);

            /* if($data['pro_fecfin'] < $data['pro_fecini'])
              return 2; */
            if (!empty($data['pro_fecini'])) {
                $fecOjb->setFecha($data['pro_fecini']);
                $fecOjb->setData($data['pro_fecini']);
                $data['pro_fecini'] = $fecOjb->renders('save');
            }
            if (!empty($data['pro_fecfin'])) {
                $fecOjb->setFecha($data['pro_fecfin']);
                $fecOjb->setData($data['pro_fecfin']);
                $data['pro_fecfin'] = $fecOjb->renders('save');
            }

            switch ($data['action']):
                case 'add':
                    unset($data['action']);
                    unset($data['hpro_id']);

                    //$dtaExiste = $this->fetchAll("pro_id = '{$data['pro_id']}'")->toArray();
                    //if(!empty($dtaExiste)):
                    //$this->_db->rollBack ();
                    //return 3;
                    //endif;
                    $fecha = date('d/m/Y');
                    $data['ho_id'] = $sesion->hoid;

                    $fecOjb->setFecha($fecha);
                    $fecOjb->setData($fecha);
                    $data['pro_fecalta'] = $fecOjb->renders('save');
                    $data['an_id'] = date('Y');
                    //var_dump($data);exit;
                    $this->createNewRowData($data);
                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $idiomaObj->createNewRow(array('id_id' => $idioma['id_id'], 'an_id' => $data['an_id'], 'pro_id' => $data['pro_id'], 'pro_titulo' => $data['pro_titulo'], 'pro_desc' => $data['pro_desc']));
                    }
                    //$idiomaObj->createNewRow(array('id_id'=>$sesion->lg,'pro_id'=>$data['pro_id'],'pro_titulo'=>$data['pro_titulo'],'pro_desc'=>$data['pro_desc']));
                    $item = 1;
                    break;
                case 'edit':
                    //var_dump($data);exit;
                    $id = $data['hpro_id'];
                    unset($data['action']);
                    unset($data['hpro_id']);
                    unset($data['pro_id']);
                    $this->updateRows($data, "pro_id = '$id'");
                    //echo "cat_id='{$data['cat_id']}' and id_id='{$data['id_id']}'";exit;
                    $save = array('pro_titulo' => $data['pro_titulo'], 'pro_desc' => $data['pro_desc']);
                    $idiomaObj->update($save, "pro_id = '$id' and id_id = '{$sesion->lg}'");
                    $item = 1;
                    break;
            endswitch;
            $this->_db->commit();
            return $item;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
        return false;
    }

    /**
     * Devolver Árticulos por promocion
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getArtPromocion($where = '') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_promocion'), array('pro_estado', 'pro_titulo'));
        $select->where("t1.pro_estado = '1' and ");
        if (!empty($where))
            $select->where($where);
        //echo $select; exit;
        $array = $this->fetchAll($select)->toArray();
        return $array;
    }

    public function deletePromocion(array $data = array(),$anio) {
        $objPromoSuc = new DbHtCmsPromocionSucursal();
        $objPromoSucIdioma = new DbHtCmsIdiomaPromSucursal();
        $objPromoIdioma = new DbHtCmsIdiomaPromocion();
        $objPromoImage = new DbHtCmsPromoImagen();
        $objImage = new DbHtCmsImagenes();
        
        $objReserva = new DbHtResReserva();
        
        $this->_db->beginTransaction();
        try {
            $where = "pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'";
            $dataExis = $objReserva->returnReservaDetalle($where);
            var_dump($dataExis); exit;
            if (empty($dataExis)):
                $pro_delete = $data['pro_id'] . '-' . $data['an_id'];
                
                $dtaPromo = $objPromoImage->fetchAll("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'")->toArray();
                
                $objPromoImage->delete("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'");
                foreach ($dtaPromo as $value):
                    $objImage->delete("im_id = '{$value['im_id']}'");
                endforeach;
                
                $objPromoSucIdioma->delete("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'");
                $objPromoSuc->delete("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'");
                $objPromoIdioma->delete("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'");
                $this->delete("pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'");
                
                $this->_db->commit();
                
                foreach ($dtaPromo as $value):
                    unlink(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_1.jpg');
                    unlink(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_2.jpg');
                    unlink(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_3.jpg');
                    unlink(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_4.jpg');
                    unlink(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_5.jpg');

                    rmdir(MP. '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete . '/' . $value['im_id']); // . $value['im_id']
                    
                    rmdir(MP . '/img/' . self::PATH_PROMO . '/' . $value['su_id'] . '/' . $pro_delete); // . $value['im_id']
                endforeach;
                return 1;
            else:
                return 2;
            endif;
           
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 2;
            throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function getDataArt($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo'), array('art_id', 'art_titulo', 'id_id', 'art_est', 'art_id_id' => 'art_id'))
                ->join(array('t2' => 'cms_categoria_articulo'), "t1.art_id = t2.art_id", array('cat_id'))
                //->leftjoin(array('t3'=>'cms_imagenes_articulo'),'t2.art_id = t3.art_id and t2.su_id = t3.su_id and t2.cat_id = t3.cat_id',array())
                // ->letjoin(array('t4'=>'ht_cms_imagenes'),'t3.im_id = t4.im_id',array('im_id','im_image1','im_image2','im_image3','im_image4','im_image5','im_estado'))
                ->where("t2.cat_id = '{$data['cat_id']}' and t1.id_id = '{$data['id_id']}'");
        //echo $select;exit;	   
        $dataArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dataArticulo);
    }

    public function getDataArtxCatySuc($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'cms_categoria_articulo'), array('cat_id'))
                ->join(array('t2' => 'ht_cms_sucursal_categoria'), 't1.cat_id = t2.cat_id', array('su_id'))
                ->join(array('t4' => 'cms_imagenes_articulo'), 't2.su_id = t4.su_id AND t2.cat_id = t4.cat_id', array())
                ->join(array('t5' => 'ht_cms_imagenes'), 't4.im_id = t5.im_id', array('im_id', 'im_image1'))
                ->join(array('t3' => 'vht_cms_articulo'), 't1.art_id = t3.art_id', array('art_id', 'id_id', 'art_est', 'art_titulo'))
                ->where("t3.id_id = '{$data['id_id']}' and t2.cat_id = '{$data['cat_id']}' and t2.su_id = '{$data['su_id']}'");
        $dtaArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dtaArticulo);
    }

    public function getDataArticulo($data) {    /// para eliminar ...
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo'), array('ho_id', 'su_id', 'cat_id', 'art_id', 'art_titulo', 'id_id', 'art_est'))
                ->join(array('t2' => 'ht_cms_idioma_art_cont'), "t1.su_id = t2.su_id and t1.art_id = t2.art_id and t1.cat_id = t2.cat_id and t2.id_id = '{$data['id_id']}'", array('art_contenido'))
                ->join(array('t3' => 'cms_imagenes_articulo'), 't2.art_id = t3.art_id and t2.su_id = t3.su_id and t2.cat_id = t3.cat_id', array())
                ->join(array('t4' => 'ht_cms_imagenes'), 't3.im_id = t4.im_id', array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5', 'im_estado'))
                ->where("t1.su_id='{$data['su_id']}' and t1.cat_id = '{$data['cat_id']}' and t1.art_id = '{$data['art_id']}' and t1.id_id = '{$data['id_id']}'");
        $dataArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dataArticulo);
    }

    /*
     * select distinct t1.pro_id,t2.tih_id,im_image2 from ht_cms_promocion t1
      inner join ht_cms_promocion_sucursal t2 on t1.tih_id = t2.tih_id and t1.pro_id = t2.pro_id
      left join ht_cms_promo_imagen t3 on t2.pro_id = t3.pro_id and t2.tih_id = t2.tih_id and t2.su_id = t3.su_id
      left join ht_cms_imagenes t4 on t3.im_id = t4.im_id
     */

    /**
     * Default | proceso-reserva/index
     * Lista de las promos activos
     * 
     * @param  $fecha date, fecha actual
     * @param $suid string, ID de sucursal 
     * @return array de la consulta
     */
    public function getListPromotions($fecha, $suid) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promocion'), array('pro_id', 'tih_id'))
                ->join(array('t2' => 'ht_cms_promocion_sucursal'),
                    't1.tih_id = t2.tih_id and t1.pro_id = t2.pro_id', array('su_id','an_id'))
                ->joinLeft(array('t3' => 'ht_cms_promo_imagen'), 't2.pro_id = t3.pro_id and t2.tih_id = t2.tih_id and t2.su_id = t3.su_id', array(''))
                ->joinLeft(array('t4' => 'ht_cms_imagenes'), 't3.im_id = t4.im_id', array('image' => 'im_image3'))
                ->where('t2.su_id = ?', $suid)
                ->where('pro_fecfin > ?', $fecha)
                ->where('pro_estado = ?', 'S')
                ->order('pro_fecfin'); //
        //echo $select; exit;
        $dtaLista = $this->fetchAll($select)->toArray();
        return $dtaLista;
    }

}

