<?php
/**
 * HtSegMenu Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbVHtSegMenu extends Cit_Db_Table_Abstract
{

    protected $_name = 'vht_seg_menu';

    protected $_primary = array('me_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	public function menulista($me_me_id="(ht_me_id IS null  or ht_me_id ='')",$suid){
		$login = new Zend_Session_Namespace('login');	
		$select = $this->select();
		$select->setIntegrityCheck(false);
		$select->from(array('t1'=>'vht_seg_menu'),array('me_id','me_desc'))
		->join(array('t2'=>'menu_sucursal'),'t1.me_id=t2.me_id',array(''));
		$select->where("$me_me_id and su_id='$suid' and t1.id_id='{$login->lg}'"); 
		
		$data = $this->fetchAll($select)->toArray();
    	return $data;
    	
	}
	public function menuComparado($coduser = '',$suid){
		$login = new Zend_Session_Namespace('login');	
		$coduser=trim($coduser);
		$cad ='';$cad .='<ul>';	$i=0;
		$select = $this->select();
		$select->setIntegrityCheck(false);
	
		$select->from(array('t1'=>'menu_sucursal'),array('me_id'));
		$select->join(array('t2'=>'vht_seg_menu'),'t1.me_id = t2.me_id and id_id = '."'{$login->lg}'",array('me_desc'));
		$select->join(array('t3'=>'ht_seg_usuario_menu'),'t1.me_id = t3.me_id and t1.su_id = t3.su_id',array('usm_estado','usm_permiso'));
		$select->join(array('t4'=>'ht_seg_usuario'),'t3.us_id = t4.us_id', array('us_id'));
		$select->where("t3.us_id='".$coduser."' and t3.su_id='$suid'");
		//echo $select; exit;
		$mnuser=$this->fetchAll($select)->toArray();
		
		$mnu = $this->menulista("(ht_me_id IS NULL or ht_me_id ='') and id_id = '{$login->lg}'",$suid);
		foreach($mnu as $value){
				$checkt ='';$check1 ='';$check2 ='';$check3 ='';
				foreach($mnuser as $svalue){
					if($value['me_id'] == $svalue['me_id']){
						switch($svalue['usm_permiso']){							
							case 1: $check1 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$value['me_id'].'**1" name="SLA[]"/>' ;break;	
							case 2: $check2 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$value['me_id'].'**2" name="LEA[]"/>' ;break;	
							case 3: $check3 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$value['me_id'].'**3" name="ATA[]"/>' ;break;	
																	
						}
						switch($svalue['usm_estado']){							
							case 'S': $checkt = ' checked="checked" ' ;  break;
							case 'N': $checkt = '' ;break;												
						}							
					break;
					}									
				}
				$cad .='<li style="padding-top:5px;height:20px;padding-left:10px;color:#15428B;font-weight :bold;background:#DFE8F6; " >'.$value['me_desc'].'
				<div style="float:right">
				<input style="padding-left:5px;" type="checkbox" '.$check3.' value="'.$value['me_id'].'**3" name="AT[]" id="AT-ONE-'.$i.'-'.'"  onclick="selectthis(this,'."''".')"  style="float:right;padding:0px;width:12px" />
				<input style="padding-left:5px;" type="checkbox" '.$check2.' value="'.$value['me_id'].'**2" name="LE[]" id="LE-ONE-'.$i.'-'.'"  onclick="selectthis(this,'."''".')" style="float:right;padding:0px;width:12px" />
				<input style="padding-left:5px;" type="checkbox" '.$check1.' value="'.$value['me_id'].'**1" name="SL[]" id="SL-ONE-'.$i.'-'.'"  onclick="selectthis(this,'."''".')" style="float:right;padding:0px;width:12px" />
				<input style="padding-left:5px;" type="checkbox" '.$checkt.' value="'.$value['me_id'].'**S" name="S[]" id="S-ONE-'.$i.'-'.'"  onclick="selectthis(this,'."''".')" style="float:right;padding:0px;width:12px" />
				</div>
				</li><div class="cleaner"></div>';
				$data2=$this->menulista("ht_me_id='".$value['me_id']."' and id_id = '{$login->lg}'",$suid);
				if(!empty($data2)){
					$cad .='<ul>';
					$j=0;
					foreach($data2 as $valueone):						
						$checkt ='';$check1 ='';$check2 ='';$check3 ='';
						foreach($mnuser as $svalue){
							if($valueone['me_id'] == $svalue['me_id']){
								switch($svalue['usm_permiso']){								
									case 1: $check1 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$valueone['me_id'].'**1" name="SLA[]"/>' ;break;	
									case 2: $check2 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$valueone['me_id'].'**2" name="LEA[]"/>' ;break;	
									case 3: $check3 = ' checked="checked" ' ; $cad .='<input type="hidden"  value="'.$valueone['me_id'].'**3" name="ATA[]"/>' ;break;													
								}	
								switch($svalue['usm_estado']){							
									case 'S': $checkt = ' checked="checked" ' ;  break;
									case 'N': $checkt = '' ;break;												
								}							
							break;
							}									
						}
						$cad .='<li style="height:20px;margin-left:30px;">'.$valueone['me_desc'].'
						<div style="float:right">
						<input  style="padding-left:5px;"  type="checkbox" '.$check3.' value="'.$valueone['me_id'].'**3" name="AT[]" id="AT-TWO-'.$i.'-'.$j.'-'.'" onclick="selecttwo(this,'."''".')"  style="float:right;padding:0px;width:12px" />
						<input  style="padding-left:5px;"  type="checkbox" '.$check2.' value="'.$valueone['me_id'].'**2" name="LE[]"  id="LE-TWO-'.$i.'-'.$j.'-'.'" onclick="selecttwo(this,'."''".')"  style="float:right;padding:0px;width:12px" />
						<input  style="padding-left:5px;"  type="checkbox" '.$check1.' value="'.$valueone['me_id'].'**1" name="SL[]"  id="SL-TWO-'.$i.'-'.$j.'-'.'" onclick="selecttwo(this,'."''".')" style="float:right;padding:0px;width:12px" />
						<input  style="padding-left:5px;"  type="checkbox" '.$checkt.' value="'.$valueone['me_id'].'**S" name="S[]"  id="S-TWO-'.$i.'-'.$j.'-'.'" onclick="selecttwo(this,'."''".')" style="float:right;padding:0px;width:12px" />
						</div>
						</li><div class="cleaner"></div>';
						
						
						$j++;					
					endforeach;
					$cad .='</ul>';	
				}
				$i++;
		}
		$cad .='</ul>';	
		return $cad;
	}

}
