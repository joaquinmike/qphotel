<?php
/**
 * DbHtCmsVmeses Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsVmeses extends Cit_Db_Table_Abstract
{
    protected $_name = 'vht_cms_meses';

    protected $_primary = array(
        'mes_id','id_id'
    );

    protected $_dependentTables = array();

    protected $_referenceMap = array();
    
    
}