<?php

/**
 * DbHtResCambio Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtResCambio extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_cambio';
    protected $_primary = array(
        'cam_fecha'
    );

    public function saveData(array $data = array()) {
        $fecObj = new Cit_Db_CitFechas();
        $this->_db->beginTransaction();
        try {
            $fecObj->setData($data['cam_fecha']);
            $data['cam_fecha'] = $fecObj->renders('save');
//            var_dump($data); exit;
            $this->createNewRow($data);
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}