<?php
/**
 * HtCmsArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaPromSucursal extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_promsucursal';

    protected $_primary = array('id_id','pro_id', 'tih_id', 'su_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

}

