<?php

class DbHtResAnio extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_anio';
    protected $_primary = array('an_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = Array(), $object) {
        $sesion = new Zend_Session_Namespace('login');
        $this->_db->beginTransaction();
        try {
            foreach ($data as $value):
                $save = Array();
                $temp = substr($value, 0, 7);
                if ($temp == 'tipo::N')
                    $tipo = 'save';
                else
                    $tipo = 'update';
                //tipo::N,,se_id::E,,se_desc::Seccion E
                //lleva::D,,se_id::D,,se_desc::Sección Ds
                $data2 = explode(',,', $value);
                foreach ($data2 as $value2):
                    $final = explode('::', $value2);
                    $save[$final[0]] = $final[1];
                endforeach;

                switch ($tipo):
                    case 'update':
                        $cod = $save['lleva'];
                        unset($save['lleva']);
                        //var_dump($save); exit;
                        $this->updateRows($save, "an_id = '$cod'");
                        $object->updateRows($save, "an_id = '{$cod}' and id_id = '{$sesion->lg}'");
                        break;

                    case 'save':
                        unset($save['tipo']);
                        $this->createNewRowData($save);
                        //Idioma
                        $save['id_id'] = $sesion->lg;
                        $object->createNewRowData($save);
                        break;
                endswitch;
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}