<?php
/**
 * HtCmsIdiomaArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaArtCont extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_art_cont';
    protected $_primary = array(
    	'art_id',
    	'id_id',
    	'su_id',
    	'cat_id'
        );

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function saveData()
    {
        $this->_db->beginTransaction ();		
        					try {
        						
        					
        						$this->_db->commit();
        						return $id;
        					} catch ( Zend_Exception $e ) {
        						$this->_db->rollBack ();
        						throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        					}
        					return false;
    }

    public function getDataId()
    {
        $select = $this->select();
        					if(!empty($data['where'])){
        						$select->where("");
        					}
        					if(!empty($data['order'])){
        						$select->order($data['order']);
        					}
        					return $this->fetchAll($select)->toArray();
    }
    
    public function  saveContenido(array $data = array()){
    	$this->_db->beginTransaction();		
        try {
        	/*$where = "id_id = '{$data['id_id']}' and su_id = '{$data['su_id']}' and cat_id = '{$data['cat_id']}' and art_id = '{$data['art_id']}'";
        	$dtaExis = $this->fetchAll($where)->toArray();
        	if(empty($dtaExis))
        		$this->createNewRow($data);
        	else*/
//            $where = array("id_id" => $data['id_id'],"su_id" 
//                =>$data['su_id'],"cat_id" =>$data['cat_id'],
//                "art_id" =>$data['art_id']);
	        $this->update(array('art_contenido'=>$data['art_contenido']),
                " id_id = '{$data['id_id']}' and  su_id = '{$data['su_id']}'
                 and cat_id = '{$data['cat_id']}' and art_id = '{$data['art_id']}' ");	    
	        	    
        	$this->_db->commit();
			return 1;
		} catch ( Zend_Exception $e ) {
			$this->_db->rollBack ();
			throw new Zend_Db_Statement_Exception ( $e->getMessage () );
		}
    }
    


}
