<?php
/**
 * CmsCategoriaArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaMenuInterno extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_menu_interno';

    protected $_primary = array('cat_id','id,id' );

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	/**
	* Devolver Datos Categoria
	*
	* @param  $data string
	* @param $where string SQL WHERE clause(s),
	* @param $tipo string se quiere solo un dato (U) o varios (T).
	* @return array de la consulta
	*/
    public function getDataGral($data,$where = '',$tipo = 'T'){
    	//var_dump($data);
    	$select = $this->select()->setIntegrityCheck(false);
    	$select->from($this->_name,array(new Zend_Db_Expr($data)));
    	$select->where($where);
    	//echo $select; exit;
    	$dtaAnio = $this->fetchAll($select)->toArray();
    	if(!empty($dtaAnio[0]))	
    		$array = $dtaAnio[0];
    	else
    		 $array = array();
    	
    	if($tipo == 'T')
    		return $array;
    	else
    		return $array[0];
    }
    
	public function saveData(array $data = Array()){
		$sesion = new Zend_Session_Namespace('login');
    	$this->_db->beginTransaction ();		
        try {
        	$this->update(array('cat_desc'=>$data['cat_desc']), "cat_id = '{$data['cat_id']}' and id_id = '{$sesion->lg}'");
        	$this->_db->commit();
        	return 1;
        } catch ( Zend_Exception $e ) {
        	$this->_db->rollBack ();
        	throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }
}
