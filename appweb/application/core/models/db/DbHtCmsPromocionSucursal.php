<?php

/**
 * HtCmsArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsPromocionSucursal extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_promocion_sucursal';
    protected $_primary = array('pro_id' , 'an_id' , 'tih_id' , 'su_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    const PATH_PROMO = 'promocion';
    /**
     * Guardar un registro
     *
     * @param  $data array [registro que se guardan]
     * @return [0,1,2]
     */
    public function saveData(array $data = Array()) {
        $citObj = new CitDataGral();
        $objPromo = new DbHtCmsPromocion();
        $objKardex = new DbHtResKardex();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');
            $save['tih_id'] = $data['tih_id'];
            $save['su_id'] = $data['su_id'];

            foreach ($data['grid'] as $value):
                list($save['an_id'], $save['pro_id']) = explode(',,', $value);
//                $promo = $objPromo->fetchRow(array(
//                    'an_id = ?' => $save['an_id'],
//                    'pro_id = ?' => $save['pro_id'], 
//                    'tih_id = ?' => $save['tih_id']
//                ))->toArray();
                $this->createNewRow($save);
//                
//                $objKardex->updateTarifaPromocion($save, $promo);
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asigancion-promocion-sucursal
     * Elimina las promociones por sucursal
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function deleteData(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $objReserva = new DbHtResReserva();
        $objPromSucIdioma = new DbHtCmsIdiomaPromSucursal();
        $objPromoSucur = new DbHtCmsPromocionSucursal();
        $objPromImage = new DbHtCmsPromoImagen();
        $objImage = new DbHtCmsImagenes();
        $this->_db->beginTransaction();
        try {
            $i = 0;
            $cont = 0;
            foreach ($data['grid'] as $value):
                list($anio, $promo) = explode(',,', $value);
            
                $pro_delete = $promo . '-' . $anio;
                
                $where = "t1.su_id = '{$data['su_id']}' and pro_id = '{$promo}' and an_id = '{$anio}'";
                $dataExis = $objReserva->returnReservaDetalle($where);
                if (empty($dataExis)):
                    //var_dump("pro_id = '$value' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'");exit;
                    $dtaPromo = $objPromImage->fetchAll("pro_id = '$promo' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'  and an_id = '{$anio}'")->toArray();
                    $objPromImage->delete("pro_id = '$promo' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'  and an_id = '{$anio}'");
                    foreach ($dtaPromo as $value):
                        $objImage->delete("im_id = '{$value['im_id']}'");
                    endforeach;
                    
                    $objPromSucIdioma->delete("pro_id = '$promo' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'  and an_id = '{$anio}'");
                    $objPromoSucur->delete("pro_id = '$promo' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'  and an_id = '{$anio}'");
                    $this->delete("pro_id = '$promo' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'  and an_id = '{$anio}'");
                    
                    foreach ($dtaPromo as $value):
                        unlink(dirname(BP). '/html/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_1.jpg');
                        unlink(dirname(BP). '/html/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_2.jpg');
                        unlink(dirname(BP). '/html/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_3.jpg');
                        unlink(dirname(BP). '/html/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_4.jpg');
                        unlink(dirname(BP). '/html/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id'] . '/' . $value['im_id'] . '_5.jpg');
                        
                        rmdir(MP. '/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id']); // . $value['im_id']
                    endforeach;
                    
                    rmdir(MP . '/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete); // . $value['im_id']
                    
                    $i++;
                endif;
                $cont++;
            endforeach;
            if ($i == $cont):
                $array['id'] = 1;
                $array['desc'] = 'Eliminación completa de los promociones seleccionados.';
            else:
                if ($i == 0) {
                    $array['id'] = 2;
                    $array['desc'] = 'Existen habitaciones reservadas con esta promocion.';
                } else {
                    $array['id'] = 1;
                    $array['desc'] = 'Eliminación parcial, existen datos relacionados.';
                }
            endif;

            $this->_db->commit();

            return $array;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * Devolver Datos de Promociones pro Sucursal
     * que tengan ese tipo
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDatosSucurTipohab($where = '', $tipo = 'T') {
        //var_dump($data);
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('lleva' => 't1.tih_id', 't1.tih_id', 'tih_desc', 'tih_estado'))
            ->join(array('t2' => 'ht_hb_sucursal_tipo_habitacion'), 't1.tih_id = t2.tih_id', array(''))
            ->join(array('t3' => 'ht_cms_promocion'), 't1.tih_id = t3.tih_id', array())
            ->where('tih_estado =?', '1')
            ->where('id_id =?', $sesion->lg)
            ->where('t1.cat_id =?', 'H')
            ->distinct();
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    /**
     * Lista de Promociones asignadosa una sucursal
     * que tengan ese tipo
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getPromoSucursal($where = '', $tipo = 'T') {
        //var_dump($data);
        $anio = date('Y');
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $lleva = new Cit_Db_Table_Expr('concat', array('t1.an_id', 't1.pro_id'), ',,');
        $select->from(array('t1' => 'vht_cms_promocion'), array('lleva' => new Zend_Db_Expr($lleva), 'an_id', 't1.pro_id', 'pro_titulo', 'pro_estado', 'pro_dscto', 'pro_fecini', 'pro_fecfin'));
        $select->join(array('t2' => 'ht_cms_promocion_sucursal'), 't1.pro_id = t2.pro_id', array(''))
        //$select->join(array('t3'=>'ht_cms_promocion'),'t1.tih_id = t3.tih_id',array());
            ->where('id_id =?',$sesion->lg)
            ->where('pro_estado =?','S')
            ->where("(t1.an_id = '{$anio}' or (extract(year from pro_fecini)) = '{$anio}')");;
        $select->distinct();
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }

        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    /**
     * Lista de Promociones para asignar al tipo de de habitacion
     * a una sucursal
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function selPromoSucursal($whereLista = '', $where = '') {
        //var_dump($data);
        $anio = date('Y');
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $lleva = new Cit_Db_Table_Expr('concat', array('t1.an_id', 't1.pro_id'), ',,');
        $select->from(array('t1' => 'vht_cms_promocion'), array('lleva' => new Zend_Db_Expr($lleva), 'pro_id', 'pro_titulo', 'pro_dscto'))
                ->where('id_id =?',$sesion->lg)
                ->where('pro_estado =?','S')
                ->where("(an_id = '{$anio}' or (extract(year from pro_fecini)) = '{$anio}')");
        if (!empty($whereLista)):
            $select->where($whereLista);
        endif;
        $select->order('pro_id');
        //echo $select; exit; 
        $dataAccesorio = $this->fetchAll($select)->toArray();

        $select2 = $this->select()->setIntegrityCheck(false);
        $select2->from(array('t1' => 'ht_cms_promocion_sucursal'), array('pro_id'));
        $dataAccesorio = $this->fetchAll($select)->toArray();
        if (is_array($where)) {
            if (!empty($where['where']))
                $select2->where($where['where']);
        }else {
            if (!empty($where))
                $select2->where($where);
        }
        $select2->order('pro_id');
        //echo $select2; exit; 
        $array = array();
        $dataExiste = $this->fetchAll($select2)->toArray();
        $existe = array();
        foreach ($dataExiste as $result):
            $existe[] = $result['pro_id'];
        endforeach;
        //$i = 0;
        foreach ($dataAccesorio as $value):
            //foreach ($dataExiste as $result):
            if (!in_array($value['pro_id'], $existe)):
                $array[] = $value;
            endif;

        //endforeach;
        endforeach;
        //var_dump($dataAccesorio,$existe); exit;
        return $array;
    }

    /**
     * Guardar el contenido de la promocion por Sucursal
     * para administradores locales
     *
     * @param  $data string
     * @return array [0,1,2]
     */
    public function saveContenidoLocal($data, $language = 'ES') {
        //var_dump($data);
        $sesion = new Zend_Session_Namespace('login');
        $objIdioma = new DbHtCmsIdiomaPromSucursal();
        $this->_db->beginTransaction();
        try {
            //"su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and pro_id = '{$data['pro_id']}' and an_id = '{$data['an_id']}'"
            $dtaPromo = $objIdioma->fetchRow( array('su_id =?' => $data['su_id'],'tih_id =?' => $data['tih_id'], 'pro_id =?' => $data['pro_id'],'an_id =?' => $data['an_id']));
            if(empty($dtaPromo)):
                $objIdioma->insert(array(
                    'id_id' => $language,
                    'su_id' => $data['su_id'],
                    'tih_id' => $data['tih_id'],
                    'pro_id' => $data['pro_id'],
                    'an_id' => $data['an_id'],
                    'pro_desc' => $data['pro_desc']
                ));
            else:
                $objIdioma->update(
                    array(
                        'pro_desc' => $data['pro_desc']), 
                        array('su_id =?' => $data['su_id'],'tih_id =?' => $data['tih_id'], 'pro_id =?' => $data['pro_id'],'an_id =?' => $data['an_id'])
                    );
            endif;
            
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asignacion-promocion-sucursal
     * Lista las imagenes que pertenecen a la promocion
     *
     * @param  $data string
     * @return array de la consulta
     */
    public function getPromoImagen(array $data = Array()) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promo_imagen'), array('im_id'))
                ->joinleft(array('t2' => 'ht_cms_promocion_sucursal'), 't1.pro_id=t2.pro_id and t1.an_id = t2.an_id and t1.su_id = t2.su_id', array('an_id', 'pro_id', 'su_id', 'tih_id'))
                ->joinleft(array('t3' => 'ht_cms_imagenes'), 't1.im_id=t3.im_id', array('im_image1', 'im_image2', 'im_image3', 'im_image4'))
                ->where("t2.pro_id='{$data['pro_id']}' and t2.an_id='{$data['an_id']}' and t2.su_id = '{$data['su_id']}' and t2.tih_id = '{$data['tih_id']}'");
        //echo $select;exit;
        $dataimagen = $select->query()->fetchAll();
        return $dataimagen;
    }

    /** Asignacion-promocion-sucursal
     * Guarda las imagenes de una promocion
     *
     * @param  $data string
     * @return [0,1,2]
     */
    public function saveImagenPromo(array $data = array(), $format = 'default') {

        $tipohabimage = new DbHtCmsPromoImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'promocion';
        $imagen->_codigo = 'pro_id';

        $this->_db->beginTransaction();
        try {
            $pro_id = $data['pro_id'];
            $data['pro_id'] = $data['pro_id'] . '-' . $data['an_id'];// . '-' . rand(0, 1000000);
            
            $datanew = $imagen->ruteo($data, 'im_id', $format);

            $imagen->createNewRow($datanew);
            $tipohabimage->createNewRow(array('pro_id' => $pro_id, 'an_id' => $data['an_id'], 'tih_id' => $data['tih_id'], 'su_id' => $data['su_id'], 'im_id' => $datanew['im_id']));
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteImagenPromo(array $data = array()) {
        $select = $this->select()->setIntegrityCheck(false);
        $objPromoImage = new DbHtCmsPromoImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'promocion';
        $imagen->_codigo = 'pro_id';
        $this->_db->beginTransaction();
        $pro_delete = $data['pro_id'] . '-' . $data['an_id'];
        try {
            $data['imgids'] = str_replace('\\', '', $data['imgids']);
            $select->from(array('t1' => 'ht_cms_imagenes'), array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5'))
                    ->where("im_id in ({$data['imgids']})");
            $dataImg = $select->query()->fetchall();
            $objPromoImage->delete("pro_id = '{$data['pro_id']}' and su_id = '{$data['su_id']}' and an_id = '{$data['an_id']}' and tih_id = '{$data['tih_id']}' and im_id in ({$data['imgids']})");
            $imagen->delete("im_id in ({$data['imgids']})");
            $this->_db->commit();
            foreach ($dataImg as $value) {
                unlink(MP . '/img' . $value['im_image1']);
                unlink(MP . '/img' . $value['im_image2']);
                unlink(MP . '/img' . $value['im_image3']);
                unlink(MP . '/img' . $value['im_image4']);
                unlink(MP . '/img' . $value['im_image5']);
                rmdir(MP . '/img/' . self::PATH_PROMO . '/' . $data['su_id'] . '/' . $pro_delete . '/' . $value['im_id']); // . $value['im_id']
            }
            
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}

