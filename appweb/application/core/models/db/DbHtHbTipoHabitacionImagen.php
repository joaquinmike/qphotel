<?php
/**
 * HtHabTipoHabitacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbTipoHabitacionImagen extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_hb_tipo_habitacion_imagen';

    protected $_primary = array('tih_id','im_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();
	
	/**
     * Devolver Datos Tipo de Habitación
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
     
    public function  deleteImagen(array $data = array()){
    	$imagen = new DbHtCmsImagenes(); 
        $imagen->_carpeta ='tipohabitacion';
    	$this->_db->beginTransaction ();
        $select = $this->select()->setIntegrityCheck(false);
        try {
            $data['imgids'] = str_replace("\\", '', $data['imgids']);
            $select->from(array('t1'=>'ht_cms_imagenes'), array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5'))
                   ->where("im_id in ({$data['imgids']})");
                   //echo $select; exit;
            $dataImg = $select->query()->fetchall();
            //delete from ht_hb_tipo_habitacion_imagen where tih_id = '08'
                    //delete from ht_cms_imagenes where im_id in ('00000062', '0000063')
            
            $this->delete("tih_id = '{$data['tih_id']}' and im_id in ({$data['imgids']})"); //!!-- Por que elimnar todo??????
            $imagen->delete("im_id in ({$data['imgids']})");
            $this->_db->commit();
            
            foreach ($dataImg as $value) {
                unlink(MP.'/img' . $value['im_image1']);
                unlink(MP.'/img' . $value['im_image2']);
                unlink(MP.'/img' . $value['im_image3']);
                unlink(MP.'/img' . $value['im_image4']);
                unlink(MP.'/img' . $value['im_image5']);
            }
            return 1;
        } catch ( Zend_Exception $e ) {
                $this->_db->rollBack ();
               // return 2;
                throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

}
