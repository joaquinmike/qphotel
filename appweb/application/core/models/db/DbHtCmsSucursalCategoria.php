<?php

/**
 * HtCmsCategoria Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsSucursalCategoria extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_sucursal_categoria';
    protected $_primary = array('su_id', 'cat_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Cms Categoria
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

    public function saveData(array $data = array()) {
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function asignCatSuc($data) {
        //var_dump($data);exit;
        $this->_db->beginTransaction();
        try {
            $art_id = explode('**', $data['catids']);
            $dataCat['su_id'] = $data['su_id'];
            foreach ($art_id as $value) {
                $dataCat['cat_id'] = $value;
                $this->createNewRowData($dataCat);
            }
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function deleteCatSuc($data) {
        //var_dump($data);exit;
        $this->_db->beginTransaction();
        $dbIdiomaArtCont = new DbHtCmsIdiomaArtCont();
        $dbImagenesArticulo = new DbCmsImagenesArticulo();
        $dbCategoriaArticulo = new DbCmsCategoriaArticulo();
        try {
            $this->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
            $dbIdiomaArtCont->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
            $dbImagenesArticulo->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
            $dbCategoriaArticulo->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

}
