<?php

/**
 * HtResReserva Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtResReserva extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_reserva';
    protected $_sesion;
    protected $_primary = array(
        'res_id',
        'cl_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
        $this->_db->beginTransaction();
        $this->_sesion = new Zend_Session_Namespace('web');
        $cl_id = '';
        $message = @$data['res_additional'];
        try {
            //Guardando a la Persona
            
            if (empty($this->_sesion->usid)):
                $personaObj = new DbHtSegPersona();
                $dtaUser = $personaObj->saveData($data);
                if(!$dtaUser['cl_id']):
                     return array('action' => 'failSend');
                endif;
                $cl_id = $dtaUser['cl_id'];
                $send['name'] = $dtaUser['session']['pe_nomcomp'];
                $send['email'] = $data['email'];
            else:
                $objCliente = new DbHtSegCliente();
                $dtaCliente = $objCliente->fetchRow(array('us_id = ?' => $this->_sesion->usid));
                $cl_id = $dtaCliente['cl_id'];
                $send['name'] = $this->_sesion->penomcomp;
                $send['email'] = $this->_sesion->us_login;
            endif;
            //Usuario Nuevo Registra ahora guardando la reserva
            //Modelos
            $kardex = new DbHtResKardex();
            $usuario = new DbHtSegUsuario();
            $accesorio = new DbHtResReservaAccesorio();
            $reservadetalle = new DbHtResReservaDetalle();
            $resbonos = new DbHtResBonos();
            $servicio = new DbHtResReservaServicio();
            $cliente = new DbHtSegCliente();
            //end Modelos
            $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
            //$this->_sesion->reserve_info;
            $itemsrecord = $carrito->getContents()->getIterator();
            $carrito->calculateTotals();
            //var_dump($datacliente);
            
            $data = array(
                'cl_id' => $cl_id,
                'su_id' => $this->_sesion->reserve_info['ho_id'],
                'res_fecha' => date('Y/m/d'),
                'res_promo' => $data['res_promo'],
                'res_tarj_id' => $data['res_tarj_id'],
                'res_nomb_tarj' => $data['res_nomb_tarj'],
                'res_numero_tarj' => $data['res_numero_tarj'],
                'res_tarj_caducidad' => $data['expiration_date']['month'] . '/' . $data['expiration_date']['year'],
                'res_cod_tarj' => $data['res_cod_tarj'],
                'id_id' => $this->_sesion->lg,
                'res_codigo' => strtoupper(uniqid())
            );
            $data['res_id'] = $this->correlativo(10);
            $data['res_total'] = $carrito->getTotal();
            //$data['res_monto_promo'] = $this->_sesion->promocion;
            $data['res_monto_bonos'] = $this->_sesion->bonus;
            $bono = $resbonos->fetchRow();
            if(!empty($bono)):
                $bono = $resbonos->fetchRow()->toArray();
                $punt = round($data['res_total'] / ($bono['bo_puntos']) * $bono['bo_dolar'], 0);
                $usuario->update(array('us_puntos' => $punt), "us_id='{$this->_sesion->usid}'");
            endif;
            
            $data['res_persona'] = $this->_sesion->reserve_info['num_adults'];//Numero de Personas
            //unset($data['res_persona']);
            $this->insert($data);
            $datares = array();
            $item = 1;
            foreach ($itemsrecord as $indiceitem => $value) {
                //echo $indiceitem;
                if (!strpos($value->getId(),'**')) {
                    //Reserva Habitacion
                    $dat = explode('**', $value->getDescription());
                    //$limit = $dat[0];
                    $one = explode('/', $dat[1]);
                    $fecini = $dat[1] = $one[2] . '/' . $one[1] . '/' . $one[0];
                    $hb_capacidad = $dat[7];
                    $two = explode('/', $dat[2]);
                    $fecfin = $dat[2] = $two[2] . '/' . $two[1] . '/' . $two[0];

                    $fecZend = new Zend_Date($dat[2], 'Y/m/d');
                    $fecZend->subDay(1);
                    $dat[2] = $fecZend->get('Y/m/d');

                    $selecs = $this->select()->setIntegrityCheck(false);
                    $selecs->from(array('t10' => 'ht_res_kardex'), array('hb_id' => 'distinct(t10.hb_id)', 'count' => 'count(*)'));
                    $selecs->join(array('t20' => 'ht_hb_habitacion_tarifa'), 't10.su_id=t20.su_id and t10.tih_id=t20.tih_id and t10.ta_id=t20.ta_id', array(''));
                    $selecs->where("t10.tih_id='{$dat[5]}' and  t10.su_id='{$dat[6]}' and t10.ka_situacion='1' and t20.hb_capacidad = '$hb_capacidad'
                                    and (t10.ka_fecha BETWEEN '{$dat[1]}' and '{$dat[2]}')")->limit(1)
                            ->group('t10.hb_id')->order('2 desc');
                    //echo $selecs;exit;
                    $resumen = $selecs->query()->fetchall();

                    $node = array();
                    foreach ($resumen as $valueh) {
                        $node[] = "'" . $valueh['hb_id'] . "'";
                    }
                    $code = implode(',', $node);

                    $select = $this->select()->setIntegrityCheck(false);
                    $select->from(array('t1' => 'ht_res_kardex'), array('hb_id', 'ka_fecha', 'su_id', 'tih_id'));
                    $select->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', array('hb_precio'));
                    $select->where(" t1.tih_id='{$dat[5]}' and  t1.su_id='{$dat[6]}' and t2.hb_capacidad = '$hb_capacidad' and (t1.ka_fecha BETWEEN '{$dat[1]}' and '{$dat[2]}')   and t1.ka_situacion='1' ");
                    $select->where("t1.hb_id in($code)");
                    $dates = $select->query()->fetchall();
                        //echo $select;exit;
                    $rest = array();
                    foreach ($dates as $indice => $valueone) {
                        $rest[$valueone['hb_id']]['totals'] += $valueone['hb_precio'];
                        $rest[$valueone['hb_id']]['hb_id'] = $valueone['hb_id'];
                        $rest[$valueone['hb_id']]['res_id'] = $data['res_id'];
                        $rest[$valueone['hb_id']]['red_fecini'] = $dat[1];
                        $rest[$valueone['hb_id']]['red_fecfin'] = $dat[2];
                        $rest[$valueone['hb_id']]['su_id'] = $dat[6];

                        //echo$rest[$valueone['hb_id']]['totals'].'--';					    		
                        $kardex->update(
                            array('ka_situacion' => 'R', 'res_id' => $data['res_id']), 
                            "hb_id='{$valueone['hb_id']}' and  ka_fecha ='{$valueone['ka_fecha']}' and su_id ='{$valueone['su_id']}' and  tih_id='{$valueone['tih_id']}'"
                        );
                    }
                    foreach ($rest as $valuei) {
                        $reservadetalle->insert(array(
                            'red_item' => $item, 'res_id' => $data['res_id'],
                             'hb_id' => $valuei['hb_id'],
                            'red_fecini' => $fecini, 'red_fecfin' => $fecfin,
                            'red_precio' => $value->getPrice())//$valuei['totals'] ese no no hay promo ahi
                        );
                    }
                }else{
                    //Servicios ----------------------------- -++-
                    $additional = explode('**', $value->getId());
                    switch ($additional[0]):
                        case 'S':
                            $reservadetalle->insert(array(
                                'red_item' => $item, 'res_id' => $data['res_id'],
                                'ser_id' => $additional[1],
                                'red_fecini' => $fecini, 'red_fecfin' => $fecfin,
                                'red_precio' => $value->getPrice(), 'red_tipo' => 'S')
                            );
                            break;
                        case 'A':
                            $reservadetalle->insert(array(
                                'red_item' => $item, 'res_id' => $data['res_id'],
                                'ac_id' => $additional[1],
                                'red_fecini' => $fecini, 'red_fecfin' => $fecfin,
                                'red_precio' => $value->getPrice(), 'red_tipo' => 'A')
                            );
                            break;
                        case 'P':
                            
                            $reservadetalle->insert(array(
                                'red_item' => $item, 'res_id' => $data['res_id'],
                                'pro_id' => $additional[1], 'an_id' => $additional[2],
                                'red_fecini' => $this->_sesion->promo['pro_fecini'], 
                                'red_fecfin' => $this->_sesion->promo['pro_fecfin'],
                                'pro_dias' => (int)$this->_sesion->promo['dias'],
                                'red_precio' => (float)$value->getDescription(), 'red_tipo' => 'P')
                            );
                            break;
                    endswitch;
                }
                $item++;
            }
            
            $send['res_id'] = $data['res_id']; 
            
            if(!empty($message)):
                $this->sendEmailAdicional($send,$message);
            endif;
            
            
            $this->_db->commit(); 
            
            return array('action' => 1,'res_id' => $data['res_id'], 'session' => $dtaUser['session']);
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());

        }
    }
    
    public function sendEmailAdicional($data,$message = ''){
        $asunto = utf8_decode(Cit_Init::_('RES_EMAIL_SUBJECT'));
        $email = $data['email'];
        
        $smtpHost = 'smtp.gmail.com';
        $smtpConf = array(
            'auth' => 'login',
            'ssl' => 'ssl',
            'port' => '465',
            'username' => 'perucit@gmail.com',
            'password' => 'perucit2012'
        );
        $transport = new Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        try {
            //Create email
            $mail = new Zend_Mail();
            $email_to = 'reservations@qphotels.com';
            $name_to = "Qp Hotels";
            $content = utf8_decode('
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <p>' . Cit_Init::_('RES_EMAIL_RESERVE') . ': ' . $data['res_id'] . '</p>' .
                            '<p>' . Cit_Init::_('RES_EMAIL_NAME') . ': ' . $data['name'] . '<p>'
                            
                    . '<p>Mensaje : </p>
                            <p>' . $message . '</p>
                        </td>
                    </tr>
                </table>
            ');
            
            $mail->setReplyTo($email, $nombre);
            $mail->setFrom($email, $nombre);
            $mail->addTo($email_to, $name_to);
            $mail->setSubject($asunto);
            $mail->setBodyHtml($content);
            //Send
            $sent = true;
            $mail->send($transport);
        } catch (Exception $e) {
            echo $e->getMessage();
            $sent = false;
        }
        return $sent;
    }
    
    /** Asigancion-accesrio-tipo-hab
     * Ver si existe accesorios por tipo de habitacion ya en reserva
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function returnReservaAccesorio($where = '') {
        $sesion = new Zend_Session_Namespace('login');

        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_reserva'), array('su_id'))
                ->join(array('t2' => 'ht_res_reserva_detalle'), 't1.res_id = t2.res_id', array(''))
                ->join(array('t3' => 'ht_hb_habitacion'), 't1.su_id = t3.su_id and t3.hb_id = t2.hb_id', array(''))
                ->where('red_tipo = ?', 'A');
        //$select->join(array('t2' => 'ht_res_reserva_accesorio'), 't1.res_id = t2.res_id', array(''));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
       // echo $select; exit;
        $array = $this->fetchAll($select)->toArray();
        return $array;
    }

//returnServicioReserva

    /** Asigancion-sucursal-servicios
     * Ver si existe promociones en la reserva
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function returnReservaDetalle($where = '') {
        $sesion = new Zend_Session_Namespace('login');

        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_reserva'), array('su_id'))
                ->join(array('t2' => 'ht_res_reserva_detalle'), 't1.res_id = t2.res_id', 
                        array(''));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        //echo $select; exit;
        $array = $this->fetchAll($select)->toArray();
        return $array;
    }

    /** Reserva-consulta
     * Lista de las reservas en general
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getListaReservas($where = '', $tipo = 'N') {
        $sesion = new Zend_Session_Namespace('login');
        $fecObj = new Cit_Db_CitFechas();
        /*
         * select t1.res_id,t1.cl_id,res_fecha,res_total,t2.su_nombre,t4.pe_nomcomp
          from ht_res_reserva t1
          inner join ht_hb_sucursal t2 on t2.su_id = t1.su_id
          inner join ht_seg_cliente t3 on t1.cl_id = t3.cl_id
          inner join ht_seg_persona t4 on t3.pe_id = t4.pe_id
         */
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_reserva'), array('t1.res_id', 't1.cl_id', 'res_fecha', 'res_total', 'res_promo'));
        $select->join(array('t2' => 'ht_hb_sucursal'), 't1.su_id = t2.su_id', array('su_nombre'));
        $select->join(array('t3' => 'ht_seg_cliente'), 't1.cl_id = t3.cl_id', array(''));
        $select->join(array('t4' => 'ht_seg_persona'), 't3.pe_id = t4.pe_id', array('pe_nomcomp'));

        switch ($tipo):
            case 'N'://??? No me acuerdo


                if (is_array($where)) {
                    if (!empty($where['where']))
                        $select->where($where['where']);
                    if (!empty($where['order']))
                        $select->order($where['order']);
                }else {
                    if (!empty($where))
                        $select->where($where);
                }

                break;

            case 'R'://Rserva-Consulta

                if (!empty($where['su_id']))
                    $select->where("t1.su_id = '{$where['su_id']}'");

                if (!empty($where['fec_ini']) and !empty($where['fec_fin'])):
                    $fecObj->setData($where['fec_ini']);
                    $fecini = $fecObj->renders('save');

                    $fecObj->setData($where['fec_fin']);
                    $fecfin = $fecObj->renders('save');

                    $select->where("res_fecha BETWEEN '$fecini' and '$fecfin'");
                endif;

                if (!empty($where['query'])):
                    $nombre = strtoupper($where['query']);
                    $select->where("upper(pe_nomcomp) like '%{$nombre}%' or t1.res_id like '%{$where['query']}%'");
                endif;

                $select->order(array('res_fecha desc', 'pe_nomcomp'));

                break;
        endswitch;
        $array = $this->fetchAll($select)->toArray();
        $i = 0;
        foreach ($array as $value):
            //$fecObj->setFecha($value['res_fecha']);	
            $fecObj->setData($value['res_fecha']);
            $array[$i]['res_fecha'] = $fecObj->renders('open');
            $i++;
        endforeach;
        return $array;
    }

    /** Reserva-consulta-Detalle
     * Lista del de talle de una reserva reservas en general
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getReservasDetalle($resid,$idioma = 'ES') {
        $sesion = new Zend_Session_Namespace('login');
        $fecObj = new Cit_Db_CitFechas();
        
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_reserva_detalle'), array('res_id','red_item','red_fecini','red_fecfin','red_precio','pro_dias',
                'red_dias' => new Zend_Db_Expr('t1.red_fecfin::timestamp without time zone - t1.red_fecini::timestamp without time zone'),'red_tipo')) //'pro_id'  
            ->joinLeft(array('t2' => 'vht_hb_habitacion'), 't1.hb_id = t2.hb_id and t2.id_id = ' . "'{$idioma}'", 
                    array('hb_id','hb_desc')) 
            ->joinLeft(array('t3' => 'vht_cms_promocion'), 't1.pro_id = t3.pro_id and t1.an_id = t3.an_id and t3.id_id = ' . "'{$idioma}'", 
                    array('pro_id','pro_titulo')) 
            ->joinLeft(array('t4' => 'vht_ser_servicio'), 't1.ser_id = t4.ser_id and t4.id_id = ' . "'{$idioma}'",  
                    array('ser_id','ser_razsocial'))
            ->joinLeft(array('t5' => 'vht_hb_accesorios'), 't1.ac_id = t5.ac_id and t5.id_id = ' . "'{$idioma}'",  
                    array('ac_id','ac_desc'))
            ->where('res_id =?', $resid);
        
        $dtaLista = $this->fetchAll($select)->toArray();
        $item = array();
        foreach ($dtaLista as $value):
            $array = array();
            $array = $value;
            
            switch ($value['red_tipo']):
                case 'S':
                    $array['red_tipo'] = 'Servicio';
                    $array['red_detalle'] = $value['ser_razsocial'];
                    break;
                case 'A':
                    $array['red_tipo'] = 'Accesorio';
                    $array['red_detalle'] = $value['ac_desc'];
                    break;
                case 'P':
                    $array['red_tipo'] = 'Promoción';
                    $array['red_detalle'] = $value['pro_titulo'];
                    $array['red_dias'] = $value['pro_dias'];
                    break;
                default:
                    $array['red_tipo'] = 'Reserva';
                    $array['red_detalle'] = $value['hb_desc'];
                    break;
            endswitch;
            
            $array['red_dias'] = (int) $array['red_dias'];
            if (empty($array['red_dias']))
                $array['red_dias'] = '';

            if (!empty($value['red_fecini'])):
                //$fecObj->setFecha($value['red_fecini']);
                $fecObj->setData($value['red_fecini']);
                $array['red_fecini'] = $fecObj->renders('open');
            endif;
            if (!empty($value['red_fecfin'])):
                //$fecObj->setFecha($value['red_fecfin']);
                $fecObj->setData($value['red_fecfin']);
                $array['red_fecfin'] = $fecObj->renders('open');
            endif;

            $item[] = $array;
        endforeach;
        //var_dump($item); exit;
        return $item;
    }

    /** Reserva-Gestion
     * Verificar si existe los registro de habitaciones en kardex
     *
     * @param  $data string $_POST
     * @return array de la consulta
     */
    public function getCalendarGestion($data) {
        $sesion = new Zend_Session_Namespace('login');
        $fecObj = new Cit_Db_CitFechas();

        $fecObj->setFecha($where['fec_ini']);
        $fecObj->setData($where['fec_ini']);
        $fecini = $fecObj->renders('save');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('su_id', 'ka_fecha'));
        $select->where("su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and extract(month from ka_fecha) = '{$data['mes_id']}'"); //select extract(month from pro_fecini)
        //echo $select; exit;
        $dtaLista = $this->fetchAll($select)->toArray();
        $i = 0;
        foreach ($dtaLista as $value):
            $i++;
        endforeach;

        if ($i > 0)
            $array['res_item'] = 'S';
        else
            $array['res_item'] = 'N';

        return array($array); //
    }

    /** Reserva-Incentario
     * Verificar si existe los registro de habitaciones en kardex
     *
     * @param  $data string $_POST
     * @return array de la consulta
     */
    public function getReservasDia($data) {
        $sesion = new Zend_Session_Namespace('login');
        $fecObj = new Cit_Db_CitFechas();
        $fecObj->setFecha($data['fecha']);
        $fecObj->setData($data['fecha']);
        $fecha = $fecObj->renders('save');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('su_id', 'ka_fecha'));
        $select->distinct();
        $select->join(array('t2' => 'ht_res_reserva'), 't1.res_id = t2.res_id and t1.su_id = t2.su_id', array('res_id', 'res_fecha', 'res_total', 'res_promo'));
        $select->join(array('t3' => 'ht_seg_cliente'), 't2.cl_id = t3.cl_id', array(''));
        $select->join(array('t4' => 'ht_seg_persona'), 't3.pe_id = t4.pe_id', array('pe_nomcomp'));
        $select->where("t1.su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ka_fecha = '$fecha'"); //select extract(month from pro_fecini)
        //echo $select; exit;
        $dtaLista = $this->fetchAll($select)->toArray();
        $i = 0;
        /* foreach ($dtaLista as $value):
          $i++;
          endforeach;

          if($i > 0)
          $array['res_item'] = 'S'; // para generar S:ya tiene N:genera
          else
          $array['res_item'] = 'N'; */

        return $dtaLista; //getReservasDia
    }
    
    public function anulaReserva($res_id){
        $objResDetail = new DbHtResReservaDetalle();
        $objResKardex = new DbHtResKardex();
        try {
            $this->_db->beginTransaction();
            
            $select = $this->select()->setIntegrityCheck(false);
            $select->from(array('t1' => 'ht_res_reserva'), array('su_id', 'res_id'))
                    ->join(array('t2' => 'ht_res_reserva_detalle'), 't1.res_id = t2.res_id', array('red_fecini','red_fecfin'))
                    ->joinLeft(array('t3' => 'ht_hb_habitacion'), 't2.hb_id = t3.hb_id and t1.su_id = t3.su_id',array('tih_id','hb_id'))
                    ->where('t1.res_id =?', $res_id)
                    ->where('red_tipo =?', 'R');
            
            $dtaReserva = $this->fetchRow($select)->toArray();
//            $dtaRes = $this->fetchRow(array('res_id =?' => $res_id))->toArray();
//            $dtaResDetail = $objResDetail->fetchRow(array('res_id =?' => $res_id,'red_tipo =?' => 'R'))->toArray();
            //Kardex
            $objDesde = new Zend_Date($dtaReserva['red_fecini']);
            $objHasta = new Zend_Date($dtaReserva['red_fecfin']);
            $dias = $objHasta->getDate()->get(Zend_Date::TIMESTAMP) - $objDesde->getDate()->get(Zend_Date::TIMESTAMP);
            $dias = (int) ($dias / (60 * 60 * 24));
            $date = new Zend_Date();
            //Day One
            $objResKardex->update(array('ka_situacion' => 1), 
                array(
                    'hb_id =?' => $dtaReserva['hb_id'],
                    'tih_id =?' => $dtaReserva['tih_id'],
                    'su_id =?' => $dtaReserva['su_id'],
                    //'ta_id =?' => $dtaResDetail['ta_id'],
                    'res_id =?' => $dtaReserva['res_id'],
                    'ka_fecha =?' => $objDesde->get('Y-m-d')
                    )
                );
            //Day Two or more
            for ($i = 1; $i < $dias; $i++ ) :
                $objDesde->addDay(1);
                $objResKardex->update(array('ka_situacion' => 1), 
                array(
                    'hb_id =?' => $dtaReserva['hb_id'],
                    'tih_id =?' => $dtaReserva['tih_id'],
                    'su_id =?' => $dtaReserva['su_id'],
                    //'ta_id =?' => $dtaResDetail['ta_id'],
                    'res_id =?' => $dtaReserva['res_id'],
                    'ka_fecha =?' => $objDesde->get('Y-m-d')
                    )
                );
            endfor;
            $this->update(array('res_estado' => '0'), array('res_id = ?' => $res_id));
            $objResDetail->delete(array('res_id = ?' => $res_id,'red_tipo != ?' => 'R'));
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
