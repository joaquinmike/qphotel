<?php

/**
 * HbHabitacionTarifao Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbHabitacionTarifa extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_habitacion_tarifa';
    protected $_primary = array('su_id', 'tih_id', 'ta_id', 'hb_capacidad');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = Array()) {
        $citObj = new CitDataGral();
        $sesion = new Zend_Session_Namespace('login');
        $this->_db->beginTransaction();
        try {
            $i = 0;
            $tarifa = $citObj->getDataGral('vht_hb_tarifa', array('ta_id', 'ta_desc'), array('where' => "id_id = '{$sesion->lg}'", 'order' => 'ta_id'));
            foreach ($tarifa as $value):
                $id = 'hb_precio_' . $value['ta_id'];
                $data['hb_precio'] = $data[$id];
                unset($data[$id]);
                if (!empty($data['hb_precio'])) {
                    $dtaExiste = $this->fetchAll("su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = '{$value['ta_id']}' and hb_capacidad = '{$data['hb_capacidad']}'")->toArray();
                    if (empty($dtaExiste)):
                        $data['ta_id'] = $value['ta_id'];
                        $this->createNewRow($data);
                    else:
                        $this->update(array('hb_precio' => $data['hb_precio']), "su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = '{$value['ta_id']}' and hb_capacidad = '{$data['hb_capacidad']}'");
                    endif;
                    $i++;
                }
            endforeach;
            /* //Lunes a Viernes
              if(!empty($data['hb_precio'])){
              $dtaExiste = $this->fetchAll("su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = 'D' and hb_capacidad = '{$data['hb_capacidad']}'")->toArray();
              if(empty($dtaExiste)):
              $data['ta_id'] = 'D';
              $this->createNewRow($data);
              else:
              $this->update(array('hb_precio'=>$data['hb_precio']), "su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = 'D' and hb_capacidad = '{$data['hb_capacidad']}'");
              endif;
              $i++;
              }
              //Sabados y Domingos
              if(!empty($tarifaF)){
              $dtaExiste = $this->fetchAll("su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = 'F' and hb_capacidad = '{$data['hb_capacidad']}'")->toArray();
              if(empty($dtaExiste)):
              $data['hb_precio'] = $tarifaF;
              $data['ta_id'] = 'F';
              $this->createNewRow($data);
              else:
              $this->update(array('hb_precio'=>$tarifaF), "su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}' and ta_id  = 'F' and hb_capacidad = '{$data['hb_capacidad']}'");
              endif;
              $i++;
              } */
            if ($i == 0):
                $this->_db->rollBack();
                return 2;
            endif;

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getHabitacionTarifas($where = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion_tarifa'), array('tih_id', 't1.ta_id', 't1.hb_capacidad', 'hb_precio'));
        $select->join(array('t2' => 'ht_hb_sucursal_tipo_habitacion'), 't1.tih_id = t2.tih_id and t1.su_id = t2.su_id', array('su_id'));
        $select->join(array('t3' => 'vht_hb_tipo_habitacion'), "t1.tih_id = t3.tih_id and id_id = '{$sesion->lg}'", array('tih_desc'));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        $select->order(array('t1.tih_id', 't1.hb_capacidad'));
        //echo $select; exit;
        $data = $this->fetchAll($select)->toArray();
        $array = array();
        $item = array();
        $capacidad = '';
        $i = 0;
        foreach ($data as $value):
            if (empty($i)):
                $id = 'hb_precio_' . $value['ta_id'];
                $array = $value;
                $array[$id] = $value['hb_precio'];
                unset($array['hb_precio']);

            else:
                if ($value['hb_capacidad'] == $capacidad):
                    $id = 'hb_precio_' . $value['ta_id'];
                    $array[$id] = $value['hb_precio'];
                else:
                    $item[] = $array;
                    $array = array();
                    $id = 'hb_precio_' . $value['ta_id'];
                    $array = $value;
                    $array[$id] = $value['hb_precio'];
                    unset($array['hb_precio']);
                endif;
            endif;

            $capacidad = $value['hb_capacidad'];
            $i++;
        endforeach;
        if (!empty($array))
            $item[] = $array;

        return $item;
    }

    public function getPreciosCapacidad($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion_tarifa'), array('tih_id', 't1.ta_id', 't1.hb_capacidad', 'hb_precio'))
                ->where('su_id =?', $data['su_id'])
                ->where('tih_id =?', $data['tih_id'])
                ->where('hb_capacidad =?', $data['hb_capacidad'])
                ->order(array('t1.tih_id', 't1.hb_capacidad'));
        
        $data = $this->fetchAll($select)->toArray();
        $array = array();
        $tipo = '';
        $capacidad = '';
        //var_dump($data); exit;
        foreach ($data as $value):
            $id = 'hb_precio_' . $value['ta_id'];
            $array[$id] = $value['hb_precio'];
        //$cont++;
        endforeach;
        $item[] = $array;
        //var_dump($item); exit;
        return $item;
    }

    public function getFormularioPrecios(array $data = Array()) {
        $citObj = new CitDataGral();

        //Formulario
        $form = '';
        $field = '[';
        $i = 0;
        foreach ($data as $value):
            //{xtype:'textfield',name:'hb_precio',fieldLabel:'Lun-Viernes ',allowBlank:false,width: 100,style:'text-align:right;'},
            //{xtype:'textfield',name:'hb_precio_f',fieldLabel:'Sab - Dom ',width: 100,style:'text-align:right;'}
            $id = 'hb_precio_' . $value['ta_id'];

            if ($i == 0)
                $allow = 'allowBlank:false,';
            else
                $allow = '';

            $field .= "'$id',";
            $form .= "{xtype:'textfield',name:'{$id}',fieldLabel:'{$value['ta_desc']}',{$allow} width: 100,style:'text-align:right;'},";
            $i++;
        endforeach;
        $form = substr($form, 0, strlen($form) - 1);
        $field = substr($field, 0, strlen($field) - 1);
        $field .= ']';
        return array('form' => $form, 'fields' => $field);
        //Json Devolver
    }

}
