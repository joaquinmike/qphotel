<?php
/**
 * HtSegIdiomaMenu Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegIdiomaPerfil extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_seg_idioma_perfil';

    protected $_primary = array(
        'id_id',
        'per_id'
        );

    protected $_dependentTables = array();

    protected $_referenceMap = array();

   
}
