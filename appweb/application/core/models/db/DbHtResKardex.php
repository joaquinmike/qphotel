<?php

/* * *
 * HtResKardex Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */

class DbHtResKardex extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_kardex';
    protected $_primary = array('ka_fecha', 'hb_id', 'tih_id', 'su_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function getDatosHabit($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_habitacion'), array('hb_id', 'hb_numero', 'hb_estado', 'hb_desc'));
        $select->join(array('t2' => 'ht_res_kardex'), 't1.hb_id=t2.hb_id and t1.su_id=t2.su_id and t1.tih_id = t2.tih_id and t1.id_id = ' . "'$sesion->lg'", array('hb_id', 'su_id', 'hb_capacidad', 'ta_id', 'res_id', 'ka_fecha', 'ka_situacion'));
        $select->join(array('t3' => 'vht_hb_tipo_habitacion'), 't3.tih_id = t2.tih_id and t3.id_id = ' . "'$sesion->lg'", array('tih_desc', 't2.tih_id'));

        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit; 
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    public function getDatosHabitTipo($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t2' => 'ht_res_kardex'), array('cn' => 'count(distinct(t2.hb_id))'));
        $select->join(array('t1' => 'vht_hb_habitacion'), 't1.hb_id=t2.hb_id and t1.su_id=t2.su_id and t1.tih_id = t2.tih_id and t1.id_id = ' . "'$sesion->lg'", array('t2.su_id'));
        //$select->join(array('t4'=>'ht_hb_sucursal_tipo_habitacion'),'t2.su_(id = t4.su_id',array(''));
        $select->join(array('t3' => 'vht_hb_tipo_habitacion'), 't3.tih_id = t2.tih_id and t3.id_id = ' . "'$sesion->lg'", array('tih_desc', 't2.tih_id'));
        $select->group(array('t2.su_id', 't3.tih_desc', 't2.tih_id'));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit; 
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    public function retornar($tih_id, $su_id, $fechaini, $fechafin, $value) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('hb_precio' => 'sum(hb_precio)'));
        $select->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', array(''));
        $select->where(" t1.tih_id='$tih_id' and  t1.su_id='$su_id' and (t1.ka_fecha BETWEEN '$fechaini' and '$fechafin') and t1.ka_situacion='1'  ");
        $select->where("t1.hb_id in(
				 select distinct(hb_id) as hb_id from ht_res_kardex where  
				 tih_id='$tih_id' and  su_id='$su_id' 
				 and (ka_fecha BETWEEN '$fechaini' and '$fechafin') AND ka_situacion='1'  limit $value )");
        return $this->fetchAll($select)->toArray();
    }

    public function getDatosHabitTipoPubli($where = '', $order = '', $adulto = 1) {
        $sesion = new Zend_Session_Namespace('web');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'))
            ->join(array('t2' => 'vht_hb_tipo_habitacion') , 
                't1.tih_id = t2.tih_id and t2.id_id = ' . "'{$sesion->lg}'", 
                array('tih_desc','tih_coment'))
            ->joinleft(array('t3' => 'ht_hb_tipo_habitacion_imagen'), 't1.tih_id=t3.tih_id ', 
                array('t3.tih_id'))
            ->joinleft(array('t4' => 'ht_cms_imagenes'), 't3.im_id=t4.im_id ', 
                array('im_image2', 'im_image3'))
            ->where('t2.tih_estado = ?', 1)
            ->where('tih_state = ?', 1);
        
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit; 
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }
    
    public function getReservaHabitacion($post,$_sesion,$tihid = ''){
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('hb_id', 'count' => 'count(*)'))
            ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 
                    't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', 
                    array('hb_precio'))
            ->where("t1.tih_id = '$tihid' and  ka_situacion='1'")
            ->where('t1.su_id =?', $_sesion->su_id)
            ->where('t2.hb_capacidad = ?', $_sesion->reserve_info['num_adults'])
            ->where("t1.ka_fecha BETWEEN '{$post['fecha_ini']}' and '{$post['fecha_fin']}'")
            ->group(array('t1.hb_id','t2.hb_precio'))
            ->order('hb_id');
        $data = $this->fetchAll($select)->toArray();
        
        $objDesde = new Zend_Date($post['fecha_ini']);
        $objHasta = new Zend_Date($post['fecha_fin']);
        $dias = $objHasta->getDate()->get(Zend_Date::TIMESTAMP) - $objDesde->getDate()->get(Zend_Date::TIMESTAMP);
        $dias = (int) ($dias / (60 * 60 * 24)) + 1;
        
        $array = Array(); $i = 1;
        foreach ($data as $value):
            $array[$value['hb_id']]['hb_id'] = $value['hb_id'];
            $array[$value['hb_id']]['hb_precio'] = $value['hb_precio']  + (float)$array[$value['hb_id']]['hb_precio'];
            $array[$value['hb_id']]['count'] = (int)$array[$value['hb_id']]['count'] + $value['count'];
            $array[$value['hb_id']]['sum'] = (int)$array[$value['hb_id']]['sum'] + 1;
        endforeach;
        
        $validate = Array();
        foreach ($array as $value):
            if($value['count'] == $dias):
                $validate = array(
                    'hb_id' => $value['hb_id'],
                    'count' => $value['count'],
                    'hb_precio' => round($value['hb_precio']/$value['sum'], 2)
                );
                break;
            endif;
        endforeach;
        return $validate;
    }

    /**
     * reserva-gestion
     * Generar kardex de un mes
     *
     * @param  $data string, datos de una habitación
     */
    public function saveGeneraMes(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $mes = date('m');
            if (date('m') <= $data['mes_id']):
                $habitacionObj = new DbHtHbHabitacion(); // Lo mismo en la linea 161
                $dtaHab = $habitacionObj->getDatosHabit("t1.su_id = '{$data['su_id']}' and t1.tih_id = '{$data['tih_id']}'");
                foreach ($dtaHab as $value):
                    $data['hb_id'] = $value['hb_id'];
                    $this->saveKardexHabitacion($data, $data['mes_id']);
                endforeach; //end Lo mismo en la linea 161
            else:
                $this->_db->rollBack();
                return 2;
            endif;


            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

//

    /** Generacion de Calendario
     * Devolver los tipo de una tipo de hab
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getTipoHabSucursal(array $data = Array(), $default = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('lleva' => 't1.tih_id', 't1.tih_id', 'tih_desc', 'tih_abr', 'tih_estado'));
        $select->join(array('t2' => 'ht_hb_sucursal_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('su_id'));
        $select->where("id_id = '{$sesion->lg}' and tih_estado = '1' and cat_id = 'H'");
        $select->where("su_id = '{$data['su_id']}'");
        //echo $select; exit;
        $dtaHabit = $select->query()->fetchall();
        if ($default == 'T')
            $dtaHabit[] = array('tih_id' => 'T', 'tih_desc' => ' - General - ');

        return $dtaHabit;
    }

    /**
     * matenimiento - habitacion
     * Generar kardex de un mes indicado
     *
     * @param  $data string, datos de una habitación
     * @param  $mes string mes a pasar 
     */
    public function saveKardexHabitacion(array $data = Array(), $mes = '01') {
        $fecOjb = new Cit_Db_CitFechas();
        $numDias = $fecOjb->getMonthDays($mes, $data['an_id']);

        $save = array();
        $save['hb_id'] = $data['hb_id']; //$save[''] = $data[''];
        $save['tih_id'] = $data['tih_id'];
        $save['su_id'] = $data['su_id'];
        $save['ka_situacion'] = '1'; //1->Habilitado 0->inactivo (de un día)

        for ($i = 1; $i <= $numDias; $i++):
            $dia = str_pad($i, 2, '0', STR_PAD_LEFT);
            $fecha = $dia . '/' . $mes . '/' . $data['an_id'];
            //Nombre
            $name = $fecOjb->nameDate($fecha);
            if ($name == 'viernes' or $name == 'sábado')
                $save['ta_id'] = 'F';
            else
                $save['ta_id'] = 'D';
            //Guardar
            $fecOjb->setFecha($fecha);
            //$fecOjb->setData($fecha);	
            $save['ka_fecha'] = $fecOjb->renders('save');
            $dtaExiste = $this->fetchAll("ka_fecha = '{$save['ka_fecha']}' and hb_id = '{$save['hb_id']}' and tih_id = '{$save['tih_id']}' and su_id = '{$save['su_id']}'")->toArray();
            if (empty($dtaExiste[0]))
                $this->createNewRow($save);
        endfor;
    }

    /**
     * reserva-proceso-genera
     * Generar kardex de un mes o varios meses
     *
     * @param  $data string, datos de una habitación
     */
    public function generaCalendarioKardex(array $data = Array()) {
        if ($data['tih_id'] == 'T'):
            $array = $this->getTipoHabSucursal($data);
        else:
            $array[] = $data;
            $verifica = $this->returnKardexverifica($data);
        endif;

        $this->_db->beginTransaction();
        try {
            foreach ($array as $result):
                $query = array('tih_id' => $result['tih_id'], 'su_id' => $data['su_id']);
                $verifica = $this->returnKardexverifica($query);
                if (!empty($verifica)):
                    $habitacionObj = new DbHtHbHabitacion();
                    if (empty($data['mes_fin'])):
                        unset($data['mes_fin']);
                        //aqui
                        $dtaHab = $habitacionObj->getDatosHabit("t1.su_id = '{$data['su_id']}' and t1.tih_id = '{$result['tih_id']}'");
                        if (empty($dtaHab)):
                            $this->_db->rollBack();
                            return array('id' => 2, 'desc' => 'No existe Habitaciones.');
                        endif;

                        foreach ($dtaHab as $value):
                            $datos = $data;
                            $datos['hb_id'] = $value['hb_id'];
                            $datos['tih_id'] = $result['tih_id'];
                            $this->saveKardexHabitacion($datos, $data['mes_id']);
                        endforeach;
                    else:
                        $ini = (int) $data['mes_id'];
                        $fin = (int) $data['mes_fin'];
                        for ($i = $ini; $i <= $fin; $i++):
                            $mes = str_pad($i, 2, '0', STR_PAD_LEFT);
                            $dtaHab = $habitacionObj->getDatosHabit("t1.su_id = '{$data['su_id']}' and t1.tih_id = '{$result['tih_id']}'");
                            foreach ($dtaHab as $value):
                                $datos = $data;
                                $datos['hb_id'] = $value['hb_id'];
                                $datos['tih_id'] = $result['tih_id'];
                                $this->saveKardexHabitacion($datos, $mes);
                            endforeach;
                        endfor;
                    endif;
                else:
                    if ($data['tih_id'] != 'T') {
                        $this->_db->rollBack();
                        return array('id' => 2, 'desc' => 'No existe los precios del tipo de habitación seleccionado.');
                    }
                endif;
            endforeach; //End $result

            $this->_db->commit();
            return array('id' => 1);
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * reserva-proceso-genera
     * Verifica si los datos de generacion existen en
     * habitacion_tarifa
     *
     * @param  $data string, datos de una habitación
     */
    public function returnKardexverifica(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion_tarifa'), array('hb_capacidad', 'hb_precio', 'ta_id'));
        $select->where("su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'");

        $dtaPrecio = $this->fetchAll($select)->toArray();

        if (!empty($dtaPrecio[0]['hb_precio']))
            return 1;
        else
            return 0;
    }

    public function saveKardex(array $data = array()) {

        $this->_db->beginTransaction();
        $kardexObj = new DbHtResKardex();
        try {

            $i = 1;
            $hab = $kardexObj->getDatosHabit(array('where' => "t2.tih_id='{$data['tih_id']}' and t2.su_id='{$data['su_id']}'"));

            foreach ($hab as $valueone) {
                if (!array_key_exists($valueone['hb_id'] . '**' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $valueone['ka_fecha'], $data['ka_situacion'])) {
                    if ($valueone['ka_situacion'] == '1') {
                        $this->update(array('ka_situacion' => '0'), "hb_id='{$valueone['hb_id']}' and tih_id='{$valueone['tih_id']}' and su_id='{$valueone['su_id']}' and ka_fecha='{$valueone['ka_fecha']}'");
                    }
                } elseif ($valueone['ka_situacion'] == '0') {
                    //echo "hb_id='{$valueone['hb_id']}' and tih_id='{$valueone['tih_id']}' and su_id='{$valueone['su_id']}' and ka_fecha='{$valueone['ka_fecha']}'";exit;
                    $this->update(array('ka_situacion' => '1'), "hb_id='{$valueone['hb_id']}' and tih_id='{$valueone['tih_id']}' and su_id='{$valueone['su_id']}' and ka_fecha='{$valueone['ka_fecha']}'");
                }
            }
            //var_dump($data['ta_id']);exit;
            foreach ($data['ta_id'] as $indice => $value) {
                $ind = explode('**', $indice);
                //echo "hb_id='{$ind[0]}' and tih_id='{{$ind[1]}' and su_id='{{$ind[2]}' and ka_fecha='{{$ind[3]}'";exit;
                $this->update(array('ta_id' => $value), "hb_id='{$ind[0]}' and tih_id='{$ind[1]}' and su_id='{$ind[2]}' and ka_fecha='{$ind[3]}'");
            }

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }
    
    public function saveKardexTipo(array $data = array()) {

        $this->_db->beginTransaction();
        $kardexObj = new DbHtResKardex();
        try {
            //var_dump($data['ka_situacion']);exit;
            foreach ($data['ka_situacion'] as $indice => $value) {
                if ($value != 0) {
                    $ind = explode('**', $indice);

                    if ($value > 0)
                        $situcion = 0;
                    if ($value < 0) {
                        $situcion = 1;
                        $value = $value * (-1);
                    }

                    //$value=(int)$value;
                    //var_dump($value);exit;  
                    //	echo "ka_situacion='$situcion' and su_id='{$ind[1]}' and tih_id='{$ind[0]}' and ka_fecha='{$ind[2]}'";exit;      		
                    $sel = $this->select()->where("ka_situacion='$situcion' and su_id='{$ind[1]}' and tih_id='{$ind[0]}' and ka_fecha='{$ind[2]}'")->limit($value);

                    $datahabitaciones = $this->fetchAll($sel)->toArray();
                    //svar_dump($datahabitaciones);exit;
                    if (!empty($datahabitaciones)) {
                        if ($situcion == 1)
                            $sit = 0;
                        else
                            $sit = 1;
                        foreach ($datahabitaciones as $valueone) {
                            $this->update(array('ka_situacion' => $sit), "hb_id='{$valueone['hb_id']}' and tih_id='{$ind[0]}' and su_id='{$ind[1]}' and ka_fecha='{$ind[2]}'");
                        }
                    }
                }
            }
            //exit;
            foreach ($data['ta_id'] as $indice => $value) {
                $ind = explode('**', $indice);          //echo "hb_id='{$ind[0]}' and tih_id='{{$ind[1]}' and su_id='{{$ind[2]}' and ka_fecha='{{$ind[3]}'";exit;
                //echo "tih_id='{$ind[0]}' and su_id='{$ind[1]}' and ka_fecha='{$ind[2]}'";exit;
                $this->update(array('ta_id' => $value), "tih_id='{$ind[0]}' and su_id='{$ind[1]}' and ka_fecha='{$ind[2]}'");
            }

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * reserva-inventatio
     * Ver el inventario de las habitaciones x (sucursal,tipo hab,mes)
     *
     * @param  $where string, SQL WHERE clause(s)
     */
    public function getInventarioHabit(array $where = Array()) {
        $estado = array('0', '1', 'R');
        foreach ($estado as $value):
            $count = $this->countHabitacionEstado($where, $value);
            $array[$value] = $count['count'];
        //var_dump($count);
        endforeach;
        //var_dump($array); exit;
        return $array;
    }

    /** Reserva-Inventario
     * Cuantas habitaciones existe con un estado
     *
     * @param $where string SQL WHERE clause(s)
     * @return array de la consulta
     */
    public function countHabitacionEstado($where = '', $estado = '0') {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion'), array('count' => new Zend_Db_Expr('count(*)')));
        $select->join(array('t2' => 'ht_res_kardex'), 't1.hb_id=t2.hb_id and t1.su_id=t2.su_id and t1.tih_id = t2.tih_id', array(''));
        $select->where("hb_estado = '1' and ka_situacion = '$estado'");
        if (!empty($where)):
            if (is_array($where)) {
                if (!empty($where['where']))
                    $select->where($where['where']);
                if (!empty($where['order']))
                    $select->order($where['order']);
            }else
                $select->where($where);
        endif;

        $dtaCount = $this->fetchAll($select)->toArray();

        if (empty($dtaCount[0]))
            $array = array('count' => 0);
        else
            $array = $dtaCount[0];
        return $array;
    }

    /**
     * reserva-inventatio-tipo-hab
     * Ver el inventario de habitaciones x tipo de habitacion de un mes
     *
     * @param  $data string, datos de una habitación
     */
    public function getInventaryRooms($data = array(),$fecini, $fecfin,$year,$idioma) {
        
        $datos = $this->getDefaultAdapter()->fetchAll(
            "select * from sp_reserva_inverntario_rooms('{$year}','{$data['su_id']}','{$fecini}','{$fecfin}','{$idioma}') AS (ka_fecha date, hb_precio numeric(10,2), tad_id character(1), promo numeric, tih_id character varying(2), tih_abr character varying(64) )");
        
        return $datos;
    
    }
    
     /**
     * reserva-inventatio-tipo-hab
     * Ver el inventario de habitaciones x tipo de habitacion de un mes
     *
     * @param  $data string, datos de una habitación
     */
    public function getInventarioContRooms($suid, $tih_id, $fecha) {
        //Reservados
        $count = array();
        $count = $this->counTipohabEstado("t1.su_id = '{$suid}' and t1.tih_id = '{$tih_id}' and ka_fecha = '{$fecha}'", 'R');
        $dtaLista['reserva'] = $count['count'];
        $total += $count['count'];
        //Inactivos
        $count = array();
        $count = $this->counTipohabEstado("t1.su_id = '{$suid}' and t1.tih_id = '{$tih_id}' and ka_fecha = '{$fecha}'");
        $dtaLista['inactivo'] = $count['count'];
        $total += $count['count'];
        //Disponibles
        $count = $this->counTipohabEstado("t1.su_id = '{$suid}' and t1.tih_id = '{$tih_id}' and ka_fecha = '{$fecha}'", '1');
        $dtaLista['count'] = $count['count'];
        $total += $count['count'];
        //Total
        $dtaLista['total'] = $count['count'];
        //var_dump($count);
        $i++;
        
        //var_dump($dtaLista); exit;
        return $dtaLista;
    }

    /** Reserva-Inventario-tipo-hab
     * Cuantas habitaciones existe con un estado
     *
     * @param $where string SQL WHERE clause(s)
     * @return array de la consulta
     */
    public function counTipohabEstado($where = '', $estado = '0') {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion'), array('count' => new Zend_Db_Expr('count(*)')));
        $select->join(array('t2' => 'ht_res_kardex'), 't1.hb_id=t2.hb_id and t1.su_id=t2.su_id and t1.tih_id = t2.tih_id', array(''));
        if($estado == '1'):
            $select->join(array('t3' => 'ht_hb_habitacion_tarifa'),
                    "t1.su_id = t3.su_id and t1.tih_id = t3.tih_id and t2.ta_id = t3.ta_id and t3.hb_capacidad = '1'",
                    array('hb_precio'))
                ->group('t3.hb_precio');  
        endif;
        $select->where("hb_estado = '1' and ka_situacion = '$estado'");
        if (!empty($where)):
            if (is_array($where)) {
                if (!empty($where['where']))
                    $select->where($where['where']);
                if (!empty($where['order']))
                    $select->order($where['order']);
            }else
                $select->where($where);
        endif;
        //echo $select; exit;
        $dtaCount = $this->fetchRow($select);

        if (empty($dtaCount))
            $array = array('count' => 0, 'hb_precio' => 0);
        else
            $array = $dtaCount;
        
        return $array;
    }

    /** Reserva-Inventario-Meses
     * formando el grid de 3 meses(en la columna)
     * 
     * @param  $data array del POST
     * @return array de la consulta
     */
    public function getArrayInventario(array $data = array(),$for = 3) {
        $sesion = new Zend_Session_Namespace('login');
        if (empty($data['an_id']))
            $data['an_id'] = $sesion->anio;
        $fecObj = new Cit_Db_CitFechas();
        $nameMeses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre',
            '12' => 'Diciembre');

        //$mes = array($data['mes_id'],$data['mes_id']+1,$data['mes_id']+2);
        $mes = (int) $data['mes_id'];
        for ($i = 1; $i <= $for; $i++):
            $temp = array();
            if (12 < (int) $mes):
                $mes = 1;
                $data['an_id']++;
                $temp['mes_id'] = $mes;
                $temp['an_id'] = $data['an_id'];
            else:
                $temp['mes_id'] = $mes;
                $temp['an_id'] = $data['an_id'];
            endif;
            $arrayMeses[] = $temp;
            $mes = (int) $mes + 1;
        endfor;

        //$arrayMeses = array($data['mes_id'],$data['mes_id']+1,$data['mes_id']+2);
        $arrayDias = array();
        $field = '';
        $cont = 0;
        foreach ($arrayMeses as $value):
            $value['mes_id'] = str_pad($value['mes_id'], 2, '0', STR_PAD_LEFT);
            $numDias = $fecObj->getMonthDays($value['mes_id'], $value['an_id']);

            $arrayDias[$value['mes_id']] = array('num' => $numDias, 'name' => $nameMeses[$value['mes_id']] . ' ' . $value['an_id'], 'an_id' => $value['an_id'], 'mes_id' => $value['mes_id']);

            for ($i = 1; $i <= $numDias; $i++):
                $dia = str_pad($i, 2, '0', STR_PAD_LEFT);
                $fecha = $dia . '/' . $value['mes_id'] . '/' . $value['an_id'];
                $array[$cont]['num'] = $i;
                $array[$cont]['fecha'] = $fecha;
                $array[$cont]['mes'] = $value['mes_id'];
                $array[$cont]['dia'] = $fecObj->nameDate($fecha);
                $cont++;
            endfor;
        endforeach;

        //var_dump($array); exit;
        return array('datos' => $array, 'mes' => $arrayDias);
    }

    /** Reserva-Inventario-Meses
     * Reporte de las habitaciones disponibles en 3 meses
     *
     * @param  $array array de los campos del grid
     * @param  $mes array los meses que se muestran
     * @param  $data array POST
     * @return array de la consulta
     */
    public function getInventario3Meses(array $lista = Array(), $mes, array $data = Array()) {
        $fecObj = new Cit_Db_CitFechas();
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id', 'tih_desc', 'tih_abr'));
        $select->join(array('t2' => 'ht_hb_sucursal_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('su_id'));
        $select->join(array('t3' => 'ht_hb_sucursal'), "t2.su_id = t3.su_id and su_portal = 'N'", array('su_nombre'));
        $select->where("tih_estado = '1' and su_portal = 'N' and id_id = '{$sesion->lg}' and ho_id = '" . htID . "'");
        if ($sesion->tipcod != '0001')
            $select->where("t2.su_id = '{$sesion->suid}'");
        if (!empty($data['su_id']) and $data['su_id'] != 'T')
            $select->where("t2.su_id = '{$data['su_id']}'");
        $select->order(array('su_id', 'tih_id'));
        $dtaHabit = $this->fetchAll($select)->toArray();
        $array = array();
        $items = array();
        $i = 0;

        foreach ($dtaHabit as $value):
            $array['su_nombre'] = $value['su_nombre'];
            $array['tih_desc'] = $value['tih_abr'];
            foreach ($lista as $result):
                $name = 'dia_' . $result['mes'] . '_' . $result['num'];
                $fecObj->setFecha($result['fecha']);
                //$fecObj->setData($result['fecha']);	
                $fecha = $fecObj->renders('save');

                $cont = $this->countHabitacionEstado("t1.tih_id = '{$value['tih_id']}' and t1.su_id = '{$value['su_id']}' and ka_fecha = '{$fecha}'", 1);
                //var_dump($cont); exit;
                if (empty($cont['count']))
                    $cont['count'] = '';
                $array[$name] = $cont['count'];
            endforeach;

            $items[] = $array;
            $array = array();
        endforeach;

        return $items; //getReservasDia
    }
    
    /**
     * reserva-gestion-tip-hab
     * obtener los tipo de habitaciones de una sucursal
     * que este generado su kardex
     *
     * @param  $where string, SQL WHERE clause(s)
     */
    public function getGestionTipoHabKardex(array $where = Array()) {
        $citObj = new CitDataGral();
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);

        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('su_id', 'tih_id'));
        $select->distinct();
        $select->join(array('t2' => 'vht_hb_tipo_habitacion'), "t1.tih_id = t2.tih_id and cat_id = 'H' and id_id = '{$sesion->lg}'", array('tih_desc', 'tih_abr'));
        $select->join(array('t3' => 'ht_res_kardex'), 't1.su_id = t3.su_id and t1.tih_id = t3.tih_id', array(''));
        $select->group('t1.su_id');
        $select->group('t1.tih_id');
        $select->group('tih_desc');
        $select->group('tih_abr');
        if (!empty($where))://t2.su_id='{$data['su_id']}' 
            if (is_array($where)) {
                if (!empty($where['where']))
                    $select->where($where['where']);
                if (!empty($where['order']))
                    $select->order($where['order']);
            }else
                $select->where($where);
        endif;
        $dtaTipHab = $this->fetchAll($select)->toArray();
        $array = Array();
        foreach ($dtaTipHab as $value):
            $wheres = "su_id = '{$value['su_id']}' and tih_id = '{$value['tih_id']}' and ka_situacion in ('0','1')";
            $count = $citObj->getDataGral('ht_res_kardex', array('count' => new Zend_Db_Expr('count(*)')), $wheres, 'U');
            $value['cn'] = $count['count'];
            $array[] = $value;
        endforeach;

        return $array;
    }

    public function updateEstado(array $ka_situacion = Array(), $where = '') {
        //var_dump($ka_situacion); exit;
        $select = $this->select()->setIntegrityCheck(false);
        $this->_db->beginTransaction();
        try {
            $this->update($ka_situacion, $where);
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
        }
    }
    public function updateTarifaPromocion($data,$_sesion,$tihid)
    {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_promocion_sucursal'), array('pro_id','an_id'))
            ->join(array('t2' => 'ht_cms_promocion'), 
                    't1.tih_id = t2.tih_id and t1.pro_id = t2.pro_id and t1.an_id = t2.an_id', 
                    array('pro_dscto'))
            ->where('su_id = ?', $_sesion->su_id)
            ->where('t1.tih_id = ?', (string)$tihid)
            ->where("'{$data['fecha_ini']}' BETWEEN pro_fecini and  pro_fecfin or " . 
            "'{$data['fecha_fin']}' BETWEEN pro_fecini and pro_fecfin");
        // AND (pro_fecini >= '2012/07/26' or pro_fecfin >=  '2012/07/27')
        $promo = $this->fetchRow($select);
        return $promo;
           
    } 
    /**
     * Default -> Proceso-reserva-one y two
     * Ver el detalle de la reserva
     *
     * @param $id string, Id del carrito (solo muestra el detalle de un tipo)
     * @param $format string, el tipo de venta que se va ha mostrar
     * [box -> se mostraba en un modal y se cambio]
     * [new -> es el nueva forma que se muestra el detalle]
     */
    public function getPromocionDias($id = '', $format = 'box') {
        $sesion = new Zend_Session_Namespace('web');
        $fecObj = new Cit_Db_CitFechas();
        $name_dia = array(1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado', 7 => 'Domingo');
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');
        $value = $carrito->getContents()->getItem($id);
        //var_dump($value->getDescription()); exit;
        //foreach($carrito->getContents()->getItem() as $value):
        list($cantidad, $array['fecha_ini'], $array['fecfin'], $array['num'], $url, $array['tih_id'], $sucursal, $array['capacidad']) = explode('**', $value->getDescription());
        $array['dias'] = $array['num'];
        $fecha = $fecObj->getChainDates($array);
//        $select = $this->select()->setIntegrityCheck(false);
//        $select->from(array('t1' => 'ht_res_kardex'), array('ta_id', 'hb_id', 'ka_fecha'))
//                ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.ta_id = t2.ta_id' .
//                        ' and t1.su_id = t2.su_id and t1.tih_id = t2.tih_id', array('hb_precio' => new Zend_Db_Expr('t2.hb_precio*' . (int) $cantidad)))
//                ->join(array('t3' => 'vht_hb_tarifa'), 't1.ta_id = t3.ta_id and id_id = ' .
//                        "'{$sesion->lg}'", array('ta_desc'))
//                ->where("t1.su_id = '{$sucursal}' and t1.tih_id = '{$array['tih_id']}'")
//                ->where("t2.hb_capacidad = {$array['capacidad']}")
//                ->where("t1.ka_fecha in $fecha")
//                ->order(array('hb_id', 'ka_fecha'))
//                ->limit($array['num']);
//        echo $select; exit;
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('t2.hb_precio','ka_fecha'))
            ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', array(''))
            ->joinLeft(array('t3' => 'ht_cms_promocion_sucursal'), 
                "t1.su_id = t3.su_id  and t1.tih_id = t3.tih_id and t3.an_id = '{$sesion->promo['an_id']}' and t3.pro_id = '{$sesion->promo['pro_id']}'", 
                array(''))
            ->joinLeft(array('t4' => 'ht_cms_promocion'), 
                't3.pro_id = t4.pro_id and t3.an_id = t4.an_id', 
                array('promo' => 
                    new Zend_Db_Expr('CASE WHEN (ka_fecha BETWEEN pro_fecini and pro_fecfin) THEN (hb_precio*pro_dscto/100) ELSE 0 END ')))
            ->where('t1.tih_id = ?',$array['tih_id'])
            ->where('t1.su_id = ?', $sucursal)
            ->where('t1.ka_fecha in ?',new Zend_Db_Expr($fecha))
            ->where("t1.ka_situacion='1' and t2.hb_capacidad = {$array['capacidad']}")
            //->where("t1.hb_id = ?", $sesion->reserve_info['hb_id'])
            ->group(array('hb_precio','ka_fecha','pro_fecini','pro_fecfin','pro_dscto'))
            ->order('ka_fecha');
           //echo $select; exit;
        $dtaDias = $select->query()->fetchAll();
        $i = 0;
        foreach ($dtaDias as $result):
            $return = $result;
            $fecObj->setData($result['ka_fecha']);
            $result['ka_fecha'] = $fecObj->renders('open');
//            if (!empty($sesion->promo['pro_id'])):
//                $listSql = explode('/', $result['ka_fecha']);
//                $listProm1 = explode('/', $sesion->promo['pro_fecini']);
//                $listProm2 = explode('/', $sesion->promo['pro_fecfin']);
//                $dateSql = mktime(0, 0, 0, $listSql[1], $listSql[0], $listSql[2]);
//                $dateProm1 = mktime(0, 0, 0, $listProm1[1], $listProm1[0], $listProm1[2]);
//                $dateProm2 = mktime(0, 0, 0, $listProm2[1], $listProm2[0], $listProm2[2]);
//                if ($dateProm1 <= $dateSql and $dateSql <= $dateProm2) {
//                    $monto = $result['hb_precio'] - (float) $result['hb_precio'] * ($sesion->promo['pro_dscto'] / 100);
//                    //$return['hb_precio'] = number_format((float)$result['hb_precio'] - $monto,2);
//                    //$promo += $monto;
//                    $return['dscto'] = number_format($monto, 2);
//                    $return['hb_precio'] = number_format($monto, 2);
//                    $return['promo'] = 1;
//                    var_dump($return); exit;
//                }
//            endif; //End Promo
            switch ($format):
                case 'box':
                    $ftcha = explode('/', $result['ka_fecha']);

                    $fech['mon'] = $ftcha['1'];
                    $fech['mday'] = $ftcha['0'];
                    $fech['year'] = $ftcha['2'];

                    $tipo = date('N', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));

                    if ($tipo == 5 or $tipo == 6)
                        $return['css'] = 'class="texto_feriado"';
                    else
                        $return['css'] = 'class="texto"';

                    switch ($sesion->lg):
                        case 'ES':
                            $dia = date('N', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));
                            $dia = $name_dia[$dia];
                            break;

                        case 'EN':
                            $dia = date('l', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));
                            break;
                    endswitch;
                    $return['total'] = number_format($value->getPrice(), 2);
                    $return['cant'] = $array['capacidad'];
                    $return['fecha'] = $result['ka_fecha'];
                    $return['dia'] = $dia;

                    $items[] = $return;
                    break;
                case 'new':
                    $return['promo'] = round($result['promo'], 2);
                    if($return['promo'] > 0)
                        $i++;
                    $return['hb_precio'] = $result['hb_precio'] - $result['promo'];//number_format($value->getPrice(), 2);
                    $return['total'] = $result['hb_precio'];//number_format($value->getPrice(), 2);
                    $return['cant'] = $array['capacidad'];
                    $return['fecha'] = $fecObj->getFormatDate($result['ka_fecha'], 'D', $sesion->lg);
                    $items[] = $return;
                    break;
                default:
                    $items['monto'] = $result['promo'];
                    break;
            endswitch;

        endforeach;
        $sesion->promo['dias'] = $i;
        return $items;
    }

    public function getPromocionDias2($data, $format = 'box') {
        $sesion = new Zend_Session_Namespace('web');
        $fecObj = new Cit_Db_CitFechas();
        $name_dia = array(1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado', 7 => 'Domingo');
        $carrito = Core_Store_Cart_Factory::createInstance('StandardFinal');


        $promo = 0;
        foreach ($carrito->getContents()->getItem() as $value):
            list($cantidad, $array['fecini'], $array['fecfin'], $array['num'], $url, $array['tih_id'], $sucursal, $array['capacidad']) = explode('**', $value->getDescription());

            $fecha = $fecObj->getChainDates($array);

            $select = $this->select()->setIntegrityCheck(false);

            $select->from(array('t1' => 'ht_res_kardex'), array('ta_id', 'hb_id', 'ka_fecha'))
                    ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.ta_id = t2.ta_id' .
                            ' and t1.su_id = t2.su_id and t1.tih_id = t2.tih_id', array('hb_precio' => new Zend_Db_Expr('t2.hb_precio*' . (int) $cantidad)))
                    ->join(array('t3' => 'vht_hb_tarifa'), 't1.ta_id = t3.ta_id and id_id = ' .
                            "'{$sesion->lg}'", array('ta_desc'))
                    ->where("t1.su_id = '{$sucursal}' and t1.tih_id = '{$array['tih_id']}'")
                    ->where("t2.hb_capacidad = {$array['capacidad']}")
                    ->where("t1.ka_fecha in $fecha")
                    ->order(array('hb_id', 'ka_fecha'))
                    ->limit($array['num']);
            //echo $select; exit;
            $dtaDias = $select->query()->fetchAll();

            $i = 0;
            foreach ($dtaDias as $result):

                $return = $result;
                $fecObj->setData($result['ka_fecha']);

                $result['ka_fecha'] = $fecObj->renders('open');

                if (!empty($sesion->pro_id)):
                    $listSql = explode('/', $result['ka_fecha']);
                    $listProm1 = explode('/', $sesion->pro_ini);
                    $listProm2 = explode('/', $sesion->pro_fin);
                    $dateSql = mktime(0, 0, 0, $listSql[1], $listSql[0], $listSql[2]);
                    $dateProm1 = mktime(0, 0, 0, $listProm1[1], $listProm1[0], $listProm1[2]);
                    $dateProm2 = mktime(0, 0, 0, $listProm2[1], $listProm2[0], $listProm2[2]);

                    if ($dateProm1 <= $dateSql and $dateSql <= $dateProm2) {
                        $monto = (float) $result['hb_precio'] * ($sesion->pro_dscto / 100);
                        $return['hb_precio'] = number_format((float) $result['hb_precio'] - $monto, 2);
                        $promo += (float) $monto;
                        $return['promo'] = 1;
                    }
                endif; //End Promo
                switch ($format):
                    case 'box':
                        $ftcha = explode('/', $result['ka_fecha']);

                        $fech['mon'] = $ftcha['1'];
                        $fech['mday'] = $ftcha['0'];
                        $fech['year'] = $ftcha['2'];

                        $tipo = date('N', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));

                        if ($tipo == 5 or $tipo == 6)
                            $return['css'] = 'class="texto_feriado"';
                        else
                            $return['css'] = 'class="texto"';

                        switch ($sesion->lg):
                            case 'ES':
                                $dia = date('N', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));
                                $dia = $name_dia[$dia];
                                break;

                            case 'EN':
                                $dia = date('l', mktime(0, 0, 0, $fech['mon'], $fech['mday'], $fech['year']));
                                break;
                        endswitch;
                        $return['total'] = number_format($value->getPrice(), 2);
                        $return['cant'] = $array['capacidad'];
                        $return['fecha'] = $result['ka_fecha'];
                        $return['dia'] = $dia;

                        $items[] = $return;
                        break;
                    default:
                        //var_dump($promo);
                        $items['monto'] = $promo;
                        break;
                endswitch;
            endforeach;
        endforeach;
        return $items;
    }

    /**
     * Default -> Proceso-reserva
     * Ver el detalle de un tipode habitación
     *
     * @param $info string, Datos de la busqueda para reservar
     * @param $id string, Id de un tipo de habitación
     */
    public function getDetailRooms($info, $id, $idioma, $promo) {
        $fecObj = new Cit_Db_CitFechas();
        $cadens = $fecObj->getChainDates($info);
//        $select = $this->select()->setIntegrityCheck(false);
//        $select->from(array('t1' => 'ht_res_kardex'), array('ta_id', 'hb_id', 'ka_fecha'))
//                ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.ta_id = t2.ta_id' .
//                        ' and t1.su_id = t2.su_id and t1.tih_id = t2.tih_id', array('hb_precio' => new Zend_Db_Expr('t2.hb_precio*' . (int) $info['num_adults'])))
//                ->join(array('t3' => 'vht_hb_tarifa'), 't1.ta_id = t3.ta_id and id_id = ' .
//                        "'{$idioma}'", array('ta_desc'))
//                ->where('t1.su_id = ?', $info['ho_id'])
//                ->where('t1.tih_id = ?', $id)
//                ->where('t2.hb_capacidad = ?', $info['num_adults'])
//                ->where("t1.ka_fecha in $cadens")
//                ->order(array('hb_id', 'ka_fecha'))
//                ->limit($info['dias']);
        //echo $select; exit;
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('t2.hb_precio','ka_fecha','ta_id'))
            ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', array(''))
            ->join(array('t5' => 'vht_hb_tarifa'), 't1.ta_id = t5.ta_id and id_id = ' .
                        "'{$idioma}'", array('ta_desc'))
            ->joinLeft(array('t3' => 'ht_cms_promocion_sucursal'), 
                "t1.su_id = t3.su_id  and t1.tih_id = t3.tih_id and t3.an_id = '{$promo['an_id']}' and t3.pro_id = '{$promo['pro_id']}'", 
                array(''))
            ->joinLeft(array('t4' => 'ht_cms_promocion'), 
                't3.pro_id = t4.pro_id and t3.an_id = t4.an_id', 
                array('promo' => 
                    new Zend_Db_Expr('CASE WHEN (ka_fecha BETWEEN pro_fecini and pro_fecfin) THEN (hb_precio*pro_dscto/100) ELSE 0 END ')))
            ->where('t1.tih_id = ?', $id)
            ->where('t1.su_id = ?', $info['ho_id'])
            ->where('t1.ka_fecha in ?', new Zend_Db_Expr($cadens))
            ->where("t1.ka_situacion='1'")
            ->where('t2.hb_capacidad = ?', $info['num_adults'])
            //->where("t1.hb_id = ?", $sesion->reserve_info['hb_id'])
            ->group(array('hb_precio','ka_fecha','pro_fecini','pro_fecfin','pro_dscto','ta_desc','t1.ta_id'))
            ->order('ka_fecha');
//        echo $select; exit;
        $dtaDias = $select->query()->fetchAll();
        $detail = array();
        $total = 0;
        foreach ($dtaDias as $value) :
            //Obteniendo el nombre de la fecha
            $fecObj->setData($value['ka_fecha']);
            $fecha = $fecObj->renders('open');
            $data['dia'] = $fecObj->getFormatDate($fecha, 'P', $idioma);
            //formando el array
            $data['hb_precio'] = $value['hb_precio'] - round($value['promo'], 2);
            $data['ta_id'] = $value['ta_id'];
            $total = $total + (float) $data['hb_precio'];
            $detail[] = $data;
        endforeach;

        return array('detail' => $detail, 'total' => $total);
    }

    /** Reserva-Room-Avail
     * genrando el grid (Colum,header) un solo mes
     * 
     * @param  $data array del POST
     * @return array de la consulta
     */
    public function getRoomAvail(array $data = array(),$for = 1) {
        $sesion = new Zend_Session_Namespace('login');
        
        $fecObj = new Cit_Db_CitFechas();
        $nameMeses = array('01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo',
            '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Setiembre', '10' => 'Octubre', '11' => 'Noviembre',
            '12' => 'Diciembre');

        //$mes = array($data['mes_id'],$data['mes_id']+1,$data['mes_id']+2);
       
        //$arrayMeses = array($data['mes_id'],$data['mes_id']+1,$data['mes_id']+2);
        $arrayDias = array();
        $field = '';
        $cont = 1;
        $numDias = $fecObj->getMonthDays($data['mes_id'], $data['an_id']);
        $daysAvail = $numDias - date('d');
        $arrayDias[$value['mes_id']] = array('num' => $numDias, 'name' => $nameMeses[$data['mes_id']] . ' ' . $data['an_id'], 'an_id' => $data['an_id'], 'mes_id' => $data['mes_id']);
        //$data[$valueone['hb_id']]['d' . $i] = '<p style="height:35px;"><input type="checkbox" id="b' . $valueone['hb_numero'] . 'd' . $i . '" name="ka_situacion[' . $valueone['hb_id'] . '**' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $valueone['ka_fecha'] . ']" value="' . $valueone['ka_situacion'] . '" ' . $chk . ' />' . '<br><select name="ta_id[' . $valueone['hb_id'] . '**' . $valueone['tih_id'] . '**' . $valueone['su_id'] . '**' . $valueone['ka_fecha'] . ']">' . $tarifa . '</select></p>';

        for ($i = date('d'); $i <= $numDias; $i++):
            $dia = str_pad($i, 2, '0', STR_PAD_LEFT);
            $fecha = $dia . '/' . $value['mes_id'] . '/' . $value['an_id'];
            $array[$cont]['num'] = $i;
            $array[$cont]['fecha'] = $fecha;
            $array[$cont]['mes'] = $data['mes_id'];
            $array[$cont]['dia'] = $fecObj->nameDate($fecha);
            $cont++;
        endfor;
       
        //var_dump($array); exit;
        return array('datos' => $array, 'mes' => $arrayDias, 'avail' => $daysAvail);
    }

    /** Reserva-Room-Avail
     * Guardar ela nueva tarifa por un rango de fechas
     * 
     * @param  $data array del POST
     * @return array de la consulta
     */
    public function saveCambioTarifa(array $data = array()) {
        $objFecha = new Cit_Db_CitFechas();
        //var_dump($ka_situacion); exit;
        $select = $this->select()->setIntegrityCheck(false);
        $this->_db->beginTransaction();
        try {
            list($ta_id,$day[1],$day[2],$day[3],$day[4],$day[5],$day[6],$day[7]) = explode('**', $data['grid']);
            unset($data['grid']);
            $array = array();
            foreach ($day as $item => $value):
                if($value == 'true'):
                    $array[$item] = $item;
                endif;
            endforeach;
            
            $dtaRange = $this->getDataRangeKardex($data);
            
            foreach ($dtaRange as $value):
                $objFecha->setData($value['ka_fecha']);
                $fecha = $objFecha->renders('open');
                
                $dd = explode('/', $fecha);
                $ts = mktime(0, 0, 0, $dd[1], $dd[0], $dd[2]);
                $dia = date('N',$ts);
                if(in_array($dia, $array)):
                    $where = array('ka_fecha = ?' => $value['ka_fecha'], 'su_id = ?' => $value['su_id'], 'tih_id = ?' => $value['tih_id'], 'hb_id = ?' => $value['hb_id']);
                    $this->update(array('ta_id' => $ta_id),$where);
                endif;
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //throw new Zend_Db_Statement_Exception($e->getMessage());
            return 2;
        }
    }
    
    /**
     * Obtener los registros entre los rangos de fechas
     * por tipo de habitacion y sucursal
     * @param type $data 
     */
    public function getDataRangeKardex($data = Array()){
        $objFecha = new Cit_Db_CitFechas();
        
        $objFecha->setData($data['fec_ini']);
        $date_ini = $objFecha->renders('save');
        
        $objFecha->setData($data['fec_fin']);
        $date_end = $objFecha->renders('save');
        
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_res_kardex'), array('*'))
                ->where('tih_id = ?', $data['tih_id'])
                ->where('su_id = ?', $data['su_id'])
                //->where('ka_situacion = ?', '1')
                ->where("ka_fecha between '{$date_ini}' and '{$date_end}'");//
        //echo $select; exit;       
        $dtaKardex = $select->query()->fetchAll();
        
        return $dtaKardex;
    }
}

