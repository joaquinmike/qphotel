<?php

/**
 * HtHbPiso Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbPiso extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_piso';
    protected $_primary = array('pi_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = Array()) {
        $citObj = new CitDataGral();
        $idiomaObj = new DbHtCmsIdioma();
        $this->_db->beginTransaction();
        try {
            if (empty($data['pi_id'])):
                $dtaPiso = $citObj->returnCorrelativo($this->_name, array('pi_id' => new Zend_Db_Expr('max("pi_id")')), 4, 'U');
                $data['pi_id'] = $dtaPiso['pi_id'];
            endif;

            $save = $data;
            unset($data['pi_desc']);
            unset($save['su_desc']);
            unset($save['su_id']);
            $this->createNewRow($data);
            $idiomaObj->saveTablas(new DbHtHbIdiomaPiso(), $save);
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
