<?php
/**
 * HtCmsCategoria Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsCategoriaArticulo extends Cit_Db_Table_Abstract
{

    protected $_name = 'cms_categoria_articulo';

    protected $_primary = array('cat_id','art_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

}
