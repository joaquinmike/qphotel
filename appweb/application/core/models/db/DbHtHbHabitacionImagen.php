<?php

/**
 * HtHabHabitacionImagen Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbHabitacionImagen extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_habitacion_imagen';
    protected $_primary = array(
        'hb_id',
        'im_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    /* public function  deleteImagen(array $data = array()){
      $imagen = new DbHtCmsImagenes();
      $this->_db->beginTransaction ();
      try {

      //delete from ht_hb_tipo_habitacion_imagen where tih_id = '01' and im_id = '00001'
      $data['imgids'] = str_replace("\\", '', $data['imgids']);
      $imagen->delete("im_id in ({$data['imgids']})");
      $this->delete("tih_id = '{$data['tih_id']}' and im_id in ({$data['imgids']})");

      $this->_db->commit();
      return 1;
      } catch ( Zend_Exception $e ) {
      $this->_db->rollBack ();
      return 2;
      throw new Zend_Db_Statement_Exception ( $e->getMessage () );
      }
      } */
}
