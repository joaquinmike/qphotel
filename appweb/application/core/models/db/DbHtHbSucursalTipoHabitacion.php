<?php

/**
 * HtHabSucursalTipoHabitacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbSucursalTipoHabitacion extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_sucursal_tipo_habitacion';
    protected $_primary = array(
        'tih_id',
        'su_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    const PATH_IMG = 'tipohabitacion';
    /**
     * Guardar un registro
     *
     * @param  $data array [registro que se guardan]
     * @return [0,1,2]
     */
    public function saveData(array $data = Array()) {
        $idiomObj = new DbHtHbIdiomaAccesorios();
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');
            $save['su_id'] = $data['su_id'];
            foreach ($data['grid'] as $value):
                $save['tih_id'] = $value;
                $this->createNewRow($save);
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asigancion-accesrio-tipo-hab
     * Elimina las accesorios por tipo de habitacion
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function deleteData(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $resObj = new DbHtResReserva();
        $this->_db->beginTransaction();
        $accesSucTiphabObj = new DbHtHbAccesorioSucursalTiphab();
        try {
            $accesSucTiphabObj->delete("su_id = '{$data['su_id']}' and tih_id = '{$data['id']}'");
            $this->delete("su_id = '{$data['su_id']}' and tih_id = '{$data['id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    /** Asigancion-sucursal-tipo-hab
     * Registra las accesorios por tipo de habitacion
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function deleteDataArray(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $resObj = new DbHtResReserva();
        $this->_db->beginTransaction();
        try {
            $i = 0;
            $cont = 0;
            //var_dump($data);	exit;
            foreach ($data['grid'] as $value):
                $where = "t1.su_id = '{$data['su_id']}' and t3.tih_id = '{$value}'";
                $dataExis = $resObj->returnReservaAccesorio($where);
                if (empty($dataExis)):
                    $accesSucTiphabObj = new DbHtHbAccesorioSucursalTiphab();
                    //var_dump("su_id = '{$data['su_id']}' and tih_id = '{$value}'");	
                    $accesSucTiphabObj->delete("su_id = '{$data['su_id']}' and tih_id = '{$value}'");
                    $this->delete("su_id = '{$data['su_id']}' and tih_id = '{$value}'");
                    $i++;
                endif;
                $cont++;
            endforeach;
            //exit;
            if ($i == $cont):
                $array['id'] = 1;
                $array['desc'] = 'Eliminación completa de los accesorios seleccionados';
            else:
                if ($i == 0) {
                    $array['id'] = 2;
                    $array['desc'] = 'Todos tienes datos relacionados';
                } else {
                    $array['id'] = 1;
                    $array['desc'] = 'Eliminación parcial, existen datos relacionados';
                }
            endif;
            $this->_db->commit();
            return $array;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    /** Asigancion-sucursal-tipo-hab
     * Edita los tipos de habitaciones por sucursal
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function editTipoHabitaciones(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $resObj = new DbHtResReserva();
        try {
            $this->_db->beginTransaction();
            foreach ($data['grid'] as $value):
                $dtaLista = explode(',,', $value);
                foreach ($dtaLista as $result):
                    $final = explode('::', $result); //lleva::01;
                    $save[$final[0]] = $final[1];
                endforeach;
                $id = $save['lleva'];
                unset($save['lleva']);
                unset($save['tih_desc']);
                $this->update($save, "su_id = '{$data['su_id']}' and tih_id = '$id'");
            //var_dump($save, "su_id = '{$data['su_id']}' and tih_id = '$id'");
            endforeach;
            //exit;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asigancion-accesrio-tipo-hab
     * Muestra las tipo de habitacion(que no estan seleccionados) por sucursal
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function selTipohabSucursal($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('lleva' => 'tih_id', 'tih_id', 'tih_desc'));
        $select->where("id_id = '$sesion->lg' and tih_estado = '1' and cat_id = 'H'");
        $select->order('tih_id');
        $dataAccesorio = $this->fetchAll($select)->toArray();

        $select2 = $this->select()->setIntegrityCheck(false);
        $select2->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array('tih_id'));
        $dataAccesorio = $this->fetchAll($select)->toArray();
        if (is_array($where)) {
            if (!empty($where['where']))
                $select2->where($where['where']);
        }else {
            if (!empty($where))
                $select2->where($where);
        }
        $select2->order('tih_id');
        //echo $select2; exit;
        $array = array();
        $dataExiste = $this->fetchAll($select2)->toArray();
        $existe = array();
        foreach ($dataExiste as $result):
            $existe[] = $result['tih_id'];
        endforeach;
        //$i = 0;
        foreach ($dataAccesorio as $value):
            //foreach ($dataExiste as $result):
            if (!in_array($value['tih_id'], $existe)):
                $array[] = $value;
            endif;

        //endforeach;
        endforeach;
        //var_dump($dataAccesorio,$existe); exit;
        return $array;
    }
    
    public function getImagesTipHabSucursal($data = array()){
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_imagenes'), array('*'))
            ->joinleft(array('t3' => 'ht_hb_tipo_habitacion_imagen'), 't1.im_id = t3.im_id', array(''))
            ->where('t3.tih_id = ?', $data['tih_id'])
            ->where('t3.su_id = ?', $data['su_id']);
        
        //echo $select;exit;
        $dtaImage = $this->fetchAll($select)->toArray();
        $array = array();
        foreach ($dtaImage as $value) {
            if(empty($value['im_image1']))
                $value['im_image1'] = Cit_Init::config()->subdomain . '/img/imgblak.jpg';
            else  
                $value['im_image1'] = Cit_Init::config()->subdomain . '/img' . $value['im_image1'];
            
            $array['images'][] = array('name' => $value['im_id'], 'url' => $value['im_image1'], "size" => '2477', 'lastmod' => '1291155496000', 'im_id' => $value['im_id']);
        }
        if (empty($array)) 
            $array['images'][] = array('name' => 'ti', 'url' => '', "size" => '2477', 'lastmod' => '1291155496000', 'idimg' => 0);
        
        return $array;
    }
    
    public function saveImages($data, $format = 'default'){
        $objImgTipHab = new DbHtHbTipoHabitacionImagen();
        $objImage = new DbHtCmsImagenes();
        $objImage->_carpeta = 'tipohabitacion';
        $objImage->_codigo = 'tih_id';
        try {
            $this->_db->beginTransaction();
            $datanew = $objImage->ruteo($data, 'im_id', $format);
            $objImage->createNewRow($datanew);
            $objImgTipHab->createNewRow(array('im_id' => $datanew['im_id'], 'tih_id' => $data['tih_id'], 'su_id' => $data['su_id']));
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
        }
    }
    
    public function deleteImages(array $data = array()){
        $objImgTipHab = new DbHtHbTipoHabitacionImagen();
        $objImage = new DbHtCmsImagenes();
        $objImage->_carpeta = 'tipohabitacion';
        $objImage->_codigo = 'tih_id';
        try {
            $this->_db->beginTransaction();
            $data['imgids'] = str_replace('\\', '', $data['imgids']);
            $select = $this->select()->setIntegrityCheck(false);
            $select->from(array('t1' => 'ht_cms_imagenes'), array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5'))
                    ->where("im_id in ({$data['imgids']})");
            $dataImg = $this->fetchAll($select)->toArray();
            $objImgTipHab->delete(array('tih_id =?' => $data['tih_id'], 'su_id =?' => $data['su_id'], 'im_id in ?' => new Zend_Db_Expr('(' . $data['imgids'] . ')')));
            $objImage->delete("im_id in ({$data['imgids']})");
            $this->_db->commit();
            foreach ($dataImg as $value) {
                unlink(MP . '/img' . $value['im_image1']);
                unlink(MP . '/img' . $value['im_image2']);
                unlink(MP . '/img' . $value['im_image3']);
                unlink(MP . '/img' . $value['im_image4']);
                unlink(MP . '/img' . $value['im_image5']);
                rmdir(MP . '/img/' . self::PATH_IMG . '/' . $data['su_id'] . '/' . $value['tih_id'] . '/' . $data['im_id']); // . $value['im_id']
            }
            return 1;
            
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }
    
    public function stateChangeTypeRoom($data){
      
        try {
            $this->_db->beginTransaction();
            foreach ($data['grid'] as $value):
                $dtaLista = explode(',,', $value);
                foreach ($dtaLista as $result):
                    $this->update(array('tih_state' => new Zend_Db_Expr('case tih_state when 1 then 0 else 1 end')), 
                        array('su_id =?' => $data['su_id'],'tih_id =?' => $result));
                endforeach;
                
            //var_dump($save, "su_id = '{$data['su_id']}' and tih_id = '$id'");
            endforeach;
            $this->_db->commit();
            return 1;
            
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
