<?php

/**
 * Tabla Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbTabla extends Cit_Db_Table_Abstract {

    protected $_name = 'tabla';
    protected $_primary = array('tba_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Guardar Tablas independientes
     *  by @jmike410
     * @param $object Objteto de la tabla a guardar
     * @param $data array datos a guardar
     * @param $item array nombre de los campo de Idioma,
     * @param $objGrIdiom Objteto objeto de la tabla relacionada ah idioma.
     * @param $fechas array todas los registros tipos fechas
     * @return [0,1,2]
     */
    public function saveMantenTablas($object, array $data = Array(), array $item = Array(), $objGrIdiom = '', array $fechas = Array()) {
        $fecOjb = new Cit_Db_CitFechas();
        $this->_db->beginTransaction();
        try {
            $tipo = '';
            $id = $item['cod'];
            if (!empty($item['desc']))
                $desc = $item['desc'];
            if (!empty($item['anio']))
                $anio = 1;
            else
                $anio = 0;
            foreach ($data as $value):
                $save = Array();
                $temp = substr($value, 0, 7);
                if ($temp == 'tipo::N')
                    $tipo = 'save';
                else
                    $tipo = 'update';
                //tipo::N,,se_id::E,,se_desc::Seccion E
                //lleva::D,,se_id::D,,se_desc::Sección Ds
                $data2 = explode(',,', $value);
                foreach ($data2 as $value2):
                    $final = explode('::', $value2);
                    $save[$final[0]] = $final[1];
                endforeach;

                if (!empty($fecha)) {
                    foreach ($fechas as $value):
                        if (!empty($save[$value])) {
                            $fecOjb->setFecha($save[$value]);
                            $fecOjb->setData($save[$value]);
                            $save[$value] = $fecOjb->renders('save');
                        }
                    endforeach;
                    ;
                }

                switch ($tipo):
                    case 'update':
                        $cod = $save['lleva'];
                        unset($save['lleva']);
                        if ($cod != @$save[$id] and !empty($save[$id])):
                            $datos = $object->fetchAll("$id = '$save[$id]'")->toArray();
                            if (!empty($datos))
                                return 2;
                        endif;
                        //var_dump($save,"$id = '$cod'"); exit;
                        /* if(!empty($item['check'])):
                          $check = $item['check'];
                          if($save[$check] == 'true')
                          $save[$check] = '1';
                          else
                          $save[$check] = '0';
                          endif; */


                        $object->updateRows($save, "$id = '$cod'");
                        if (is_object($objGrIdiom)):

                            if (!empty($item['abr'])):
                                $abr = $item['abr'];
                                $idiomaObj = new DbHtCmsIdioma();
                                $idiomaObj->updateTablas($objGrIdiom, array($desc => $save[$desc], $id => $save[$id], $abr => $save[$abr]), "$id = '$cod'", @$anio);
                            else:
                                $idiomaObj = new DbHtCmsIdioma();
                                $idiomaObj->updateTablas($objGrIdiom, array($desc => $save[$desc], $id => $save[$id]), "$id = '$cod'", @$anio);
                            endif;

                        endif;
                        break;

                    case 'save':
                        unset($save['tipo']);
                        //var_dump("am_id = '{$save['am_id']}'"); exit;
                        $datos = $object->fetchAll("$id = '{$save[$id]}'")->toArray();
                        if (!empty($datos))
                            return 2;
                        //var_dump($save); exit;
                        /* if(!empty($item['check'])):
                          $check = $item['check'];
                          if($save[$check] == 'true')
                          $save[$check] = '1';
                          else
                          $save[$check] = '0';
                          endif; */
                        //var_dump($save); exit;
                        $object->createNewRowData($save);

                        if (is_object($objGrIdiom)):
                            if (!empty($item['abr'])):
                                $abr = $item['abr'];
                                $idiomaObj = new DbHtCmsIdioma();
                                $idiomaObj->saveTablas($objGrIdiom, array($desc => $save[$desc], $id => $save[$id], $abr => $save[$abr]), $anio);
                            else:
                                $idiomaObj = new DbHtCmsIdioma();
                                $idiomaObj->saveTablas($objGrIdiom, array($desc => $save[$desc], $id => $save[$id]), $anio);
                            endif;

                        endif;

                        break;
                endswitch;
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    /**
     * Guardar Tablas de tablas de detalle
     *  by @jmike410
     * @param $tbaid string  id del campo tba_id
     * @param $data array datos a guardar
     * @param $fechas array todas los registros tipos fechas
     * @return [0,1,2]
     */
    public function saveMantenTbaDet($tbaid, array $data = Array(), array $fechas = Array()) {
        $fecOjb = new Cit_Db_CitFechas();
        $tbaDetObj = new DbTablaDetalle();
        $sesion = new Zend_Session_Namespace('login');
        $this->_db->beginTransaction();
        try {
            $tipo = '';
            foreach ($data as $value):
                $save = Array();
                $temp = substr($value, 0, 7);
                if ($temp == 'tipo::N')
                    $tipo = 'save';
                else
                    $tipo = 'update';
                //lleva::D,,se_id::D,,se_desc::Sección Ds
                $data2 = explode(',,', $value);
                foreach ($data2 as $value2):
                    $final = explode('::', $value2);
                    $save[$final[0]] = $final[1];
                endforeach;
                /* if(!empty($fecha)){
                  foreach ($fechas as $value):
                  if(!empty($save[$value])){
                  $fecOjb->setFechas($save[$value]);
                  $fecOjb->setData($save[$value]);
                  $save[$value] = $fecOjb->renders('save');
                  }
                  endforeach;;
                  } */
                switch ($tipo):
                    case 'update':
                        $cod = $save['lleva'];
                        unset($save['lleva']);
                        if ($cod != @$save['tad_id'] and !empty($save['tba_id'])):
                            $datos = $tbaDetObj->fetchAll("tad_id = '{$save['tad_id']}' and tba_id = '$tbaid'")->toArray();
                            if (!empty($datos))
                                return 2;
                        endif;
                        $tbaDetObj->updateRows($save, "tba_id = '$cod' and tba_id = '$tbaid'");
                        $idiomaObj = new DbHtCmsIdioma();
                        $idiomaObj->updateTablas(new DbIdiomaTablaDet(), array('tad_desc' => $save['tad_desc'], 'tad_id' => $save['tad_id']), "tad_id = '$cod' and tba_id = '$tbaid' and id_id = '{$sesion->lg}'");

                        break;

                    case 'save':
                        unset($save['tipo']);
                        //var_dump("am_id = '{$save['am_id']}'"); exit;
                        $datos = $tbaDetObj->fetchAll("tad_id = '{$save['tad_id']}' and tba_id = '$tbaid'")->toArray();
                        if (!empty($datos))
                            return 2;
                        $save['id_id'] = $sesion->lg;
                        $save['tba_id'] = $tbaid;
                        $tbaDetObj->createNewRowData($save);
                        $idiomaObj = new DbHtCmsIdioma();
                        $idiomaObj->saveTablas(new DbIdiomaTablaDet(), array('tad_desc' => $save['tad_desc'], 'tad_id' => $save['tad_id'], 'tba_id' => $tbaid, 'id_id' => $sesion->lg));


                        break;
                endswitch;
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
