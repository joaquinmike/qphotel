<?php

/**
 * HtSegUsuario Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegUsuario extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_seg_usuario';
    protected $_primary = array(
        'us_id',
        'per_id',
        'pe_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
        $this->_db->beginTransaction();
        $personaObj = new DbHtSegPersona();
        $usuarioObj = new DbhtsegUsuario();
        try {
            //	var_dump($data);exit;
            foreach ($data as $value):
                list($lleva, $savet) = explode('::', $value);
                list($savePersona['pe_paterno'], $savePersona['pe_materno'], $savePersona['pe_nombre'], $saveUsuario['per_id']) = explode('##', $savet);
                list($us_id, $pe_id, $per_id) = explode('##', $lleva);
                $savePersona['pe_nomcomp'] = $savePersona['pe_paterno'] . ' ' . $savePersona['pe_materno'] . ', ' . $savePersona['pe_nombre'];

                //var_dump($savePersona);exit;
                $personaObj->updateRows($savePersona, "pe_id='$pe_id'");



            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function saveSegUsuario(array $data = array()) {
        $citDataGralObj = new CitDataGral();
        $personaObj = new DbHtSegPersona();
        $personaHotelObj = new DbHtSegPersonaHotel();
        $usuarioMenuObj = new DbHtSegUsuarioMenu();
        $sesionObj = new Zend_Session_Namespace('login');

        $this->_db->beginTransaction();
        try {
            $validar = $citDataGralObj->getDataGral('ht_seg_usuario', array('us_id'), "us_login='" . $data['us_login'] . "'", 'U');

            if (empty($validar['us_id'])) {
                $pe_id = $citDataGralObj->returnCorrelativo("ht_seg_persona", array('pe_id' => "max(pe_id)"), 10, "U");
                $inputArray['pe_id'] = $pe_id['pe_id'];
                if (!empty($data['ub_id'])) {
                    $inputArray['ub_id'] = $data['ub_id'];
                }
                $inputArray['pe_nombre'] = ucwords(strtolower($data['pe_nombre']));
                $inputArray['pe_paterno'] = strtoupper($data['pe_appat']);
                $inputArray['pe_materno'] = strtoupper($data['pe_apmat']);
                $inputArray['pe_nomcomp'] = strtoupper($data['pe_appat']) . ' ' . strtoupper($data['pe_apmat']) . ', ' . ucwords(strtolower($data['pe_nombre']));
                if (!empty($data['pe_dni'])) {
                    $inputArray['pe_dni'] = $data['pe_dni'];
                }
                if (!empty($data['pe_pais'])) {
                    $inputArray['pe_pais'] = $data['pe_pais'];
                }
                $personaObj->createNewRow($inputArray);

                $personaHotelObj->createNewRow(array('pe_id' => $pe_id['pe_id'], 'ho_id' => htID));

                $us_id = $citDataGralObj->returnCorrelativo("ht_seg_usuario", array('us_id' => "max(us_id)"), 8, "U");
                $inputArray2['us_id'] = $us_id['us_id'];
                $inputArray2['per_id'] = $data['per_id'];
                $inputArray2['pe_id'] = $pe_id['pe_id'];
                $inputArray2['ho_id'] = htID;
                $inputArray2['us_login'] = $data['us_login'];
                $inputArray2['su_id'] = $data['su_id'];
                $inputArray2['us_password'] = md5($data['us_password']);

                $this->createNewRow($inputArray2);

                $select = $this->select()->setIntegrityCheck(false);
                $select->from(array('t1' => 'ht_seg_perfil_menu'), array('me_id', 'su_id', 'per_id', 'pem_permiso'));
                $select->where("per_id = '" . $data['per_id'] . "'");
                $dataExiste = $this->fetchAll($select)->toArray();
                foreach ($dataExiste as $result):
                    $inputArray3['us_id'] = $us_id['us_id'];
                    $inputArray3['per_id'] = $data['per_id'];
                    $inputArray3['pe_id'] = $pe_id['pe_id'];
                    $inputArray3['su_id'] = $result['su_id'];
                    $inputArray3['me_id'] = $result['me_id'];
                    $inputArray3['usm_permiso'] = 3;
                    $inputArray3['usm_estado'] = 1;
                    $usuarioMenuObj->createNewRow($inputArray3);
                endforeach;
                //var_dump($dataExiste);
            }else {
                $this->_db->rollBack();
                return 2;
            }

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            return 0;
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function saveCambioPassword(array $data = Array(), $_user) {
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            $dtaUser = $citObj->getDataGral('ht_seg_usuario', array('us_password'), "us_id = '{$_user}'", 'U');

            if (empty($dtaUser)):
                $this->_db->rollBack();
                return 2;
            endif;

            if ($dtaUser['us_password'] == md5($_POST['us_pass'])):
                $this->update(array('us_password' => md5($_POST['us_password'])), "us_id = '{$_user}'");
            endif;

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
