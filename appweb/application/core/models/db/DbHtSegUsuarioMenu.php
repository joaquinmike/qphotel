<?php

/**
 * HtSegUsuarioMenu Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegUsuarioMenu extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_seg_usuario_menu';
    protected $_primary = array(
        'us_id',
        'per_id',
        'pe_id',
        'su_id',
        'me_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function saveAccess(array $data = array()) {
        $login = new Zend_Session_Namespace('login');
        $this->_db->beginTransaction();
        try {
            //$dbs = new HyoUsuMenu();
            $cont = array('SL', 'LE', 'AT');
            $conta = array('SLA', 'LEA', 'ATA');
            foreach ($cont as $value) {
                if (empty($data[$value . 'A'])) {
                    $data[$value . 'A'] = array();
                }
                if (empty($data[$value])) {
                    $data[$value] = array();
                }

                if (!empty($data[$value . 'A'])) {
                    //var_dump($data[$value.'A']);exit;
                    foreach ($data[$value . 'A'] as $valuecerone) {
                        if (!in_array($valuecerone, $data[$value])) {
                            $extd = explode('**', $valuecerone);
                            $this->delete("su_id='{$data['su_id']}' and us_id='{$data['us_id']}' and me_id='{$extd[0]}'");
                        }
                    }
                }
            }
            //exit;
            foreach ($cont as $value) {
                if (empty($data[$value . 'A'])) {
                    $data[$value . 'A'] = array();
                }
                if (empty($data[$value])) {
                    $data[$value] = array();
                }
                if (!empty($data[$value])) {
                    //$dbsa = new HyoUsuMenu();	
                    //var_dump($data[$value],$data[$value.'A']);exit;							
                    foreach ($data[$value] as $valueone) {
                        if (!in_array($valueone, $data[$value . 'A'])) {
                            $ext = explode('**', $valueone);
                            $datause = $this->fetchAll("su_id='{$data['su_id']}' and us_id='{$data['us_id']}' and me_id='{$ext[0]}'")->toArray();

                            if (empty($datause))
                                $this->createNewRow(array('pe_id' => $data['pe_id'], 'per_id' => $data['per_id'], 'us_id' => $data['us_id'], 'me_id' => $ext[0], 'su_id' => $data['su_id'], 'usm_estado' => 'S', 'usm_permiso' => $ext[1]));
                            else
                                $this->update(array('usm_estado' => 'S', 'usm_permiso' => $ext[1]), "su_id='{$data['su_id']}' and us_id='{$data['us_id']}' and me_id='{$ext[0]}'");
                        }
                    }
                }
            }
            //var_dump('final'); exit;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //var_dump(array('si_id'=>$ext[1],'us_id'=>$data['us_id'],'me_id'=>$ext[0],'cos_id' =>$login->cosid,'usm_estado'=>'S','usm_permiso'=>$ext[2]));exit;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
