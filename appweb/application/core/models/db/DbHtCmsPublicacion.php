<?php

/**
 * DbHtCmsPublicacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsPublicacion extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_publicacion';
    protected $_primary = array('pub_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    const PATH_EVENTOS = 'eventos';

    public function saveData(array $data = Array()) {
        $publicIdiomObj = new DbHtCmsIdiomaPublic();
        $citObj = new CitDataGral();
        $fecObj = new Cit_Db_CitFechas();
//    	$categoriaarticulo = new DbHtCmsCategoriaArticulo();

        $fecObj->setData($data['pub_fecha']);
        $data['pub_fecha'] = $fecObj->renders('save');

        $this->_db->beginTransaction();

        try {

            switch ($data['action']) {
                case 'add':
                    $dtaPublic['pub_id'] = $data['pub_id'];
                    $dtaPublic['pub_estado'] = $data['pub_estado'];
                    $dtaPublic['pub_tipo'] = $data['pub_tipo'];
                    $dtaPublic['pub_fecha'] = $data['pub_fecha'];
                    $this->createNewRow($dtaPublic);
                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $publicIdiomObj->createNewRow(array('id_id' => $idioma['id_id'], 'pub_id' => $data['pub_id'], 'pub_titulo' => $data['pub_titulo'], 'pub_contenido' => $data['pub_contenido']));
                    }

                    $id = $data['pub_id'];
                    break;
                case 'edit':
                    //var_dump($data);exit; 			
                    $this->updateRows($data, "pub_id='{$data['pub_id']}'");
                    //echo "cat_id='{$data['cat_id']}' and id_id='{$data['id_id']}'";exit;
                    $publicIdiomObj->updateRows($data, "pub_id='{$data['pub_id']}' and id_id='{$data['id_id']}'");
                    $id = $data['pub_id'];
                    break;
            }
            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_cms_promocion');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteData(array $data = array()) {
        $objPublicIdiom = new DbHtCmsIdiomaPublic;
        $objPublicImage = new DbHtCmsPublicImage();
        $objImage = new DbHtCmsImagenes();
        $this->_db->beginTransaction();
        try {
            $dtaEvento = $objPublicImage->fetchAll(array('pub_id = ?' => $data['pub_id']))->toArray();
            $objPublicImage->delete("pub_id = '{$data['pub_id']}'");
            foreach ($dtaEvento as $value):
                $objImage->delete("im_id = '{$value['im_id']}'");
            endforeach;
            $objPublicIdiom->delete("pub_id = '{$data['pub_id']}'");
            $this->delete("pub_id = '{$data['pub_id']}'");

//            $select = $this->select()->setIntegrityCheck(false);
//            $select->from(array('t1' => 'ht_cms_public_imagen'), array('pub_id'))
//                    ->join(array('t2' => 'ht_cms_imagenes'), 't1.im_id = t2.im_id', array());
            foreach ($dtaEvento as $value):
                unlink(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id'] . '/' . $value['im_id'] . '_1.jpg');
                unlink(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id'] . '/' . $value['im_id'] . '_2.jpg');
                unlink(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id'] . '/' . $value['im_id'] . '_3.jpg');
                unlink(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id'] . '/' . $value['im_id'] . '_4.jpg');
                unlink(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id'] . '/' . $value['im_id'] . '_5.jpg');
                rmdir(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id'] . '/' . $value['im_id']); // . $value['im_id']
            endforeach;
            rmdir(dirname(BP). '/html/img/' . self::PATH_EVENTOS . '/01/' .$data['pub_id']); // . $value['im_id']
          
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function saveImagen(array $data = array()) {
        $tipohabimage = new DbHtCmsPublicImage();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'eventos';
        $imagen->_codigo = 'pub_id';

        $this->_db->beginTransaction();

        try {

            $datanew = $imagen->ruteo($data, 'imgevento');
            //var_dump($datanew);exit;
            $imagen->createNewRow($datanew);
            $tipohabimage->createNewRow(array('im_id' => $datanew['im_id'], 'pub_id' => $data['pub_id']));

            $this->_db->commit();
            //return array('action'=>1,'cod'=>$data['fa_id'],'tabla'=>'familia');
        } catch (Zend_Exception $e) {
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function deleteImagePublic(array $data = array()) {
        $tipohabimage = new DbHtCmsPublicImage();
        $imagen = new DbHtCmsImagenes();
        $this->_db->beginTransaction();

        try {
            $tipohabimage->delete("pub_id='{$data['pub_id']}' and im_id in ({$data['imgids']})");
            $imagen->delete("im_id in ({$data['imgids']})");

            $this->_db->commit();
            return 1;
            //return array('action'=>1,'cod'=>$data['fa_id'],'tabla'=>'familia');
        } catch (Zend_Exception $e) {
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
