<?php
/**
 * HtCmsIdioma Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdioma extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma';

    protected $_primary = array('id_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	public function saveTablas($object,array $data = Array(),$anio = ''){
    	$session = new Zend_Session_Namespace('login');
    	$citObj = new CitDataGral();
    	
		$arrIdiomas = $citObj->getDataGral('ht_cms_idioma',array('id_id'),array('where'=>"id_estado like '1'"),'T');
        foreach ($arrIdiomas as $idioma) {
        	//$data['id_id'] = $session->lg; 
        	$data['id_id'] = $idioma['id_id'];
	        if(!empty($anio));
	        	$data['an_id'] = $session->anio; 
	        $object->createNewRowData($data);//$this->createNewRowData()
        	
        	//$articuloidioma->createNewRow(array('id_id'=>$idioma['id_id'],'art_id'=>$data['art_id'],'art_titulo'=>$data['art_titulo']));
        }
    	
        
       
    }
    
	public function updateTablas($object,array $data = Array(),$where,$anio = ''){
    	$session = new Zend_Session_Namespace('login');
    	
    	 if(!empty($anio))
        	$where .= "and an_id = '{$session->anio}'";
    	//var_dump($data,$where." and id_id = '{$session->lg}'"); exit;
        $object->updateRows($data,$where." and id_id = '{$session->lg}'");
    }
    
	public function deleteTablas($object,$delete = ''){
    	$session = new Zend_Session_Namespace('login');
    	$object->delete($delete) ; //."and id_id = '{$session->lg}'");
    }
}
