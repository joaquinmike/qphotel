<?php
/**
 * DbHtCmsPublicImage Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsPublicImage extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_public_imagen';

    protected $_primary = array('pub_id','im_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	
}
