<?php

/**
 * HtHabAccesorios Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbAccesorios extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_accesorios';
    protected $_primary = array('ac_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Completos de una Accesorios
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getDatosAccesorio($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_accesorios'), array('hac_id' => 'ac_id', 'ac_id', 'ac_desc', 'ac_tipo', 'ac_estado', 'ac_precio'));
        $select->where("id_id = '{$sesion->lg}'");
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    /**
     * Guardar un registro
     *
     * @param  $data array [registro que se guardan]
     * @return [0,1,2]
     */
    public function saveData(array $data = Array()) {
        $idiomObj = new DbHtHbIdiomaAccesorios();
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');
            switch ($data['edit']):
                case 'save':
                    unset($data['edit']);
                    if (!empty($data['ac_estado']))
                        $data['ac_estado'] = 1;
                    $this->createNewRowData($data);
                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $idiomObj->createNewRow(array('id_id' => $idioma['id_id'], 'ac_id' => $data['ac_id'], 'ac_desc' => $data['ac_desc']));
                    }
                    break;
                case 'edit':
                    $id = $data['ac_id'];
                    unset($data['edit']);
                    unset($data['ac_id']);
                    $this->updateRows($data, "ac_id = '$id'");
                    $idiomObj->update(array('ac_desc' => $data['ac_desc']), "ac_id = '$id' and id_id = '{$sesion->lg}'");
                    break;
            endswitch;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * Eliminar un registro
     *
     * @param  $data array [id a Eliminar]
     * @return [0,1,2]
     */
    public function deleteData(array $data = Array()) {
        $idiomObj = new DbHtHbIdiomaAccesorios();
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');

            /* $dtaExis = $citObj->getDataGral($this->_name, array('ac_id'),"ac_id = '{$data['id']}'");
              if(empty($dtaExis))
              return 2; */

            $idiomObj->delete("ac_id = '{$data['id']}'");
            $this->delete("ac_id = '{$data['id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            Return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    /**
     * Asignar accesorios a tipo de habitaciones por sucursal
     * sin mostar loas ya asignados (que no estan seleccionados)
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @param $where para la tabla ht_hb_accesorio_sucursal_tiphab
     * @return array de la consulta
     */
    public function selListaAsignarAccesorio($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_accesorios'), array('lleva' => 'ac_id', 'ac_id', 'ac_desc', 'ac_precio'));
        $select->where("id_id = '$sesion->lg'");
        $select->order('ac_id');
        $dataAccesorio = $this->fetchAll($select)->toArray();

        $select2 = $this->select()->setIntegrityCheck(false);
        $select2->from(array('t1' => 'ht_hb_accesorio_sucursal_tiphab'), array('ac_id'));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select2->where($where['where']);
        }else {
            if (!empty($where))
                $select2->where($where);
        }
        $select2->order('ac_id');
        //echo $select2; exit;
        $existe = array();
        $array = array();
        $dataExiste = $this->fetchAll($select2)->toArray();
        foreach ($dataExiste as $result):
            $existe[] = $result['ac_id'];
        endforeach;
        //$i = 0;
        foreach ($dataAccesorio as $value):
            //foreach ($dataExiste as $result):
            if (!in_array($value['ac_id'], $existe)):
                $array[] = $value;
            endif;

        //endforeach;
        endforeach;

        return $array;
    }

}
