<?php

/**
 * DbHtCmsMeses Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsMeses extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_meses';
    protected $_primary = array(
        'mes_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function getMesNombre($id, $idioma) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from('vht_cms_meses', array('mes_desc', 'mes_abr'))
                ->where("mes_id = '{$id}' and id_id = '{$idioma}'");

        $data = $this->fetchRow($select)->toArray();
        return $data;
    }

}