<?php

/**
 * HtCmsArticulo Db_Table_Abstract
 * 
 * @Category 
 * @Author Information Technology Community
 * @Copyright   (c) 
 * @Version V. 1.0
 */
class DbHtCmsArticulo extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_articulo';
    protected $_primary = array('art_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Cms Articulo
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

    public function saveData(array $data = array()) {
        $articuloidioma = new DbHtCmsIdiomaArticulo();
        $citObj = new CitDataGral();
//    	$categoriaarticulo = new DbHtCmsCategoriaArticulo();
        $this->_db->beginTransaction();
        try {
            switch ($data['action']) {
                case 'add':
                    $dataArticulo['art_id'] = $data['art_id'];
                    $dataArticulo['art_est'] = $data['art_est'];
                    $this->createNewRow($data);
                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $articuloidioma->createNewRow(array('id_id' => $idioma['id_id'], 'art_id' => $data['art_id'], 'art_titulo' => $data['art_titulo']));
                    }
//	  				$dataArtIdioma['id_id'] = $data['id_id'];
//	  				$dataArtIdioma['art_id'] = $data['art_id'];
//	  				$dataArtIdioma['art_titulo'] = $data['art_titulo'];	  				
//	        		$articuloidioma->createNewRow($data);
                    $id = $data['art_id'];
                    break;
                case 'edit':
                    //var_dump($data);exit; 			
                    $this->update(array('art_est' => $data['art_est']), "art_id='{$data['art_id']}'");
                    //echo "cat_id='{$data['cat_id']}' and id_id='{$data['id_id']}'";exit;
                    $articuloidioma->updateRows($data, "art_id='{$data['art_id']}' and id_id='{$data['id_id']}'");
                    $id = $data['art_id'];
                    break;
            }
            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_hb_cms_articulo');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteArticulo(array $data = array()) {
        $this->_db->beginTransaction();
        try {
            $this->delete("art_id = '{$data['art_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function getDataArt($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo'), array('art_id', 'art_titulo', 'id_id', 'art_est', 'art_id_id' => 'art_id'))
                ->join(array('t2' => 'cms_categoria_articulo'), "t1.art_id = t2.art_id", array('cat_id'))
                //->leftjoin(array('t3'=>'cms_imagenes_articulo'),'t2.art_id = t3.art_id and t2.su_id = t3.su_id and t2.cat_id = t3.cat_id',array())
                // ->letjoin(array('t4'=>'ht_cms_imagenes'),'t3.im_id = t4.im_id',array('im_id','im_image1','im_image2','im_image3','im_image4','im_image5','im_estado'))
                ->where("t2.cat_id = '{$data['cat_id']}' and t1.id_id = '{$data['id_id']}'");
        //echo $select;exit;	   
        $dataArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dataArticulo);
    }

    public function getDataArtxCatySuc($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'cms_categoria_articulo'), array('cat_id'))
                ->join(array('t2' => 'ht_cms_sucursal_categoria'), 't1.cat_id = t2.cat_id', array('su_id'))
                ->join(array('t4' => 'cms_imagenes_articulo'), 't2.su_id = t4.su_id AND t2.cat_id = t4.cat_id', array())
                ->join(array('t5' => 'ht_cms_imagenes'), 't4.im_id = t5.im_id', array('im_id', 'im_image1'))
                ->join(array('t3' => 'vht_cms_articulo'), 't1.art_id = t3.art_id', array('art_id', 'id_id', 'art_est', 'art_titulo'))
                ->where("t3.id_id = '{$data['id_id']}' and t2.cat_id = '{$data['cat_id']}' and t2.su_id = '{$data['su_id']}'");
        $dtaArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dtaArticulo);
    }

    public function getDataArticulo($data) {    /// para eliminar ...
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo'), array('ho_id', 'su_id', 'cat_id', 'art_id', 'art_titulo', 'id_id', 'art_est'))
                ->join(array('t2' => 'ht_cms_idioma_art_cont'), "t1.su_id = t2.su_id and t1.art_id = t2.art_id and t1.cat_id = t2.cat_id and t2.id_id = '{$data['id_id']}'", array('art_contenido'))
                ->join(array('t3' => 'cms_imagenes_articulo'), 't2.art_id = t3.art_id and t2.su_id = t3.su_id and t2.cat_id = t3.cat_id', array())
                ->join(array('t4' => 'ht_cms_imagenes'), 't3.im_id = t4.im_id', array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5', 'im_estado'))
                ->where("t1.su_id='{$data['su_id']}' and t1.cat_id = '{$data['cat_id']}' and t1.art_id = '{$data['art_id']}' and t1.id_id = '{$data['id_id']}'");
        $dataArticulo = $this->fetchAll($select)->toArray();
        return $this->json($dataArticulo);
    }
    public function getDataArticuloCategoria($idCategoria,$idioma,$idArticulo=null)
    {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1'=>'vht_cms_articulo_categoria_sucursal'),
            array(new Zend_Db_Expr('DISTINCT("t1"."art_id")'),'cat_id','su_id','art_orden','art_link','art_est','art_titulo'
            ,'id_id'))            
            ->where("t1.cat_id = '{$idCategoria}' and t1.id_id = '{$idioma}'")
            ->limit(3)
            ->order('t1.art_orden ASC');   
        if(!empty($idArticulo)){
            $select = $select->join(array('t2'=>'ht_cms_idioma_art_cont'),
                "t2.art_id = t1.art_id",
                array('art_contenido'))
                ->where("t2.art_id = '{$idArticulo}'");
        }
        return $this->fetchAll($select)->toArray();        
    }   
    public function getOneContainer($idCategoria,$idioma,$idArticulo=null)
    {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t2'=>'ht_cms_idioma_art_cont'),                
                array('art_contenido','cat_id','su_id'))            
            ->where("t2.cat_id = '{$idCategoria}' and t2.id_id = '{$idioma}'")
            ->limit(1);            
        if(!empty($idArticulo)){
            $select=$select->where("t2.art_id = '{$idArticulo}'");
        }                      
        return $this->fetchAll($select)->toArray(); 
    }
}

