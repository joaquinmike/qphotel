<?php

/**
 * HtSegPersona Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegPersona extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_seg_persona';
    protected $_primary = array('pe_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
//        $this->_db->beginTransaction();
//        try {
        $usuario = new DbHtSegUsuario();
        $cliente = new DbHtSegCliente();
        $personahotel = new DbHtSegPersonaHotel();
        $pe_id = $this->correlativo(10);
        $us_id = $usuario->correlativo(8);
        $cl_id = $cliente->correlativo(8);
        list($paterno, $materno) = explode(' ', $data['pe_apellidos']);
        $dataPersona = array(
            'pe_id' => $pe_id,
            'pe_nombre' => ucwords(strtolower($data['pe_nombre'])),
            'pe_paterno' => strtoupper($paterno),
            'pe_materno' => strtoupper($materno),
            'pe_nomcomp' => strtoupper($paterno) . ' ' . strtoupper($materno) . ', ' . ucwords(strtolower($data['pe_nombre'])),
            'pe_company' => $data['pe_company'],
            'pe_direccion' => $data['pe_direccion'],
            'pe_pais' => $data['pe_pais'],
            'pe_telefono' => $data['phone_number'],
            'pe_qphciudad' => $data['ub_id']
        );
	$password = $this->RandomString();
        $dataUsuario = array(
            'us_id' => $us_id,
            'pe_id' => $pe_id,
            'per_id' => '0003',
            'ho_id' => htID,
            'us_login' => $data['pe_email'],
            'us_password' => md5($password)
        );
        $this->insert($dataPersona);
        $personahotel->insert(array('pe_id' => $pe_id, 'ho_id' => htID));
        $usuario->insert($dataUsuario);
        $cliente->insert(array(
            'cl_id' => $cl_id, 'pe_id' => $pe_id, 'cl_estado' => 1,
            'cl_puntos' => 0, 'us_id' => $us_id, 'per_id' => '0003')
        );

        //$sesion = new Zend_Session_Namespace('web');
        $send = $this->sendEmail(array('email' => $data['pe_email'],'nombre' => $dataPersona['pe_nomcomp'],'password' => $password));
        $session['pe_nomcomp'] = $dataPersona['pe_nomcomp'];
        $session['us_login'] = ucfirst($dataUsuario['us_login']);
        $session['pe_id'] = $dataPersona['pe_id'];
        $session['us_id'] = $dataUsuario['us_id'];
        //$sesion->clid = $cl_id;
        //var_dump($pe_id);exit;
//            $this->_db->commit();
//        $send = ($send)?$cl_id:$send;
        return array('session' => $session,'cl_id' => ($send)?$cl_id:$send);
//        } catch (Zend_Exception $e) {
//            $this->_db->rollBack();
//            throw new Zend_Db_Statement_Exception($e->getMessage());
//        }
//        return false;
    }
    
     private function RandomString($length = 6, $uc = TRUE, $n = TRUE, $sc = FALSE) {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if ($uc == 1)
            $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if ($n == 1)
            $source .= '1234567890';
        if ($sc == 1)
            $source .= '|@#~$%()=^*+[]{}-_';
        if ($length > 0) {
            $rstr = "";
            $source = str_split($source, 1);
            for ($i = 1; $i <= $length; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($source));
                $rstr .= $source[$num - 1];
            }
        }
        return $rstr;
    }
    
    public function sendEmail($data = Array()){
        
        $nombre = $data['nombre'];
        $asunto = Cit_Init::_('REG_EMAIL_SUBJECT');
        $email = $data['email'];
        
        $smtpHost = 'smtp.gmail.com';
        $smtpConf = array(
            'auth' => 'login',
            'ssl' => 'ssl',
            'port' => '465',
            'username' => 'perucit@gmail.com',
            'password' => 'perucit2012'
        );
        $transport = new Zend_Mail_Transport_Smtp($smtpHost, $smtpConf);
        try {
            //Create email
            $mail = new Zend_Mail();
            $email_to = $email;
            $name_to = "Qp Hotels";
            $content = utf8_decode('
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <p>' . Cit_Init::_('REG_EMAIL_WELCOME') . '<br /><br />

                            qp Hotels® son una colección de hoteles dirigidos tanto a clientes 
                            ejecutivos así como a turistas en selectas ubicaciones a lo largo 
                            del Peru. Nuestra visión es la proporcionar un refugio para los viajeros 
                            mediante la fusión de la comodidad y el lujo moderno de una manera inteligente y eficiente. <br />

                            Ubicados en solicitados lugares de negocios y turismo en áreas 
                            céntricas que combinan tanto la conveniencia de la cercanía a 
                            distritos financieros, gastronómicos y de compras, así como la 
                            comodidad y el relax de las áreas residenciales. 

                            Nuestras habitaciones e instalaciones le ofrecerán el ambiente 
                            ideal para permitirnos hacerlo sentir en casa, si está buscando 
                            tarifas por noche o largas estadías, nuestros hoteles le 
                            ofrecerán lo que estaba buscando.<br />

                            <strong>
                                --------------------------------------<br />
                                ' . Cit_Init::_('REG_EMAIL_USER') .' &nbsp;:&nbsp;'. $email .'<br />
                                ' . Cit_Init::_('REG_EMAIL_PASS') . '&nbsp;:&nbsp;'. $data['password'] .'<br />
                                --------------------------------------<br />
                            </strong>
                            <p>Atte :<br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                ' . Cit_Init::_('REG_EMAIL_TEAM')  . '
                            </p>
                        </td>
                    </tr>
                </table>
            ');
            
            $mail->setReplyTo($email, $nombre);
            $mail->setFrom($email, $nombre);
            $mail->addTo($email_to, $name_to);
            $mail->setSubject($asunto);
            $mail->setBodyHtml($content);

            //Send
            $sent = true;
        
            $mail->send($transport);
        } catch (Exception $e) {
            echo $e->getMessage();
            $sent = false;
        }
        return $sent;
    }
    
}
