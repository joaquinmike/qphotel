<?php

/**
 * HtCmsImagenes Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsImagenes extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_imagenes';
    protected $_primary = array('im_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    public $_carpeta = '';
    public $_codigo = 'tih_id';

    public function ruteo($value, $tipe, $formato = 'default') {
        try {
//            $_FILES[$tipe]['tmp_name'] = 'C:/Users/JoaquinMike/Pictures/Seiya.jpg';
//            $_FILES[$tipe]['name'] = 'Seiya.jpg';
            if (!is_file(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id']))
                mkdir(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id']);
            
            $ext = explode('.', $_FILES[$tipe]['name']);
            if ($tipe == 'articulo') {

                $rutamax = MP . '/img/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id'] . '/' . $value['cat_id'] . '/' . $value[$this->_codigo];

                if (!is_file(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id']))
                    mkdir(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id']);
                if (!is_file(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id'] . '/' . $value['cat_id']))
                    mkdir(MP . '/img/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id'] . '/' . $value['cat_id']);
                //$rutamax = MP.'/img/'.$this->_carpeta.'/'.$value['ho_id'].'/'.$value[$this->_codigo];
            }else {
                if (!is_file(MP . '/img/' . $this->_carpeta . '/' . $value['su_id']))
                    mkdir(MP . '/img/' . $this->_carpeta . '/' . $value['su_id']);
                //$imagepersona = new DbImagenPersona();
                $rutamax = MP . '/img/' . $this->_carpeta . '/' . $value['su_id'] . '/' . $value[$this->_codigo];
            }

//	    if(!is_file(MP.'/img/'.$this->_carpeta.'/'.$value['ho_id']))
//	    	mkdir(MP.'/img/'.$this->_carpeta.'/'.$value['ho_id']);
            if (!is_file($rutamax))
                mkdir($rutamax);

            if (!is_file($rutamax . '/' . $this->correlativo(8)))
                mkdir($rutamax . '/' . $this->correlativo(8));

            $ruta = $rutamax . '/' . $this->correlativo(8) . '/' . $this->correlativo(8) . '_5.' . $ext[count($ext) - 1];

            copy($_FILES[$tipe]['tmp_name'], $ruta);

            if ($tipe == 'articulo') {
                $save = array('im_id' => $this->correlativo(8), 'im_image5' => '/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id'] . '/' . $value['cat_id'] . '/' . $value[$this->_codigo] . '/' . $this->correlativo(8) . '/' . $this->correlativo(8) . '_5.' . $ext[1], 'im_estado' => 1);
            } else {
                $save = array('im_id' => $this->correlativo(8), 'im_image5' => '/' . $this->_carpeta . '/' . $value['su_id'] . '/' . $value[$this->_codigo] . '/' . $this->correlativo(8) . '/' . $this->correlativo(8) . '_5.' . $ext[1], 'im_estado' => 1);
            }

            $resize = new Cit_Image_ImageResize($ruta);

            for ($i = 1; $i <= 4; $i++) {
                if ($tipe == 'articulo') {
                    $save['im_image' . $i] = '/' . $this->_carpeta . '/' . $value['ho_id'] . '/' . $value['su_id'] . '/' . $value['cat_id'] . '/' . $value[$this->_codigo] . '/' . $this->correlativo(8) . '/' . $this->correlativo(8) . '_' . $i . '.' . $ext[1];
                } else {
                    $save['im_image' . $i] = '/' . $this->_carpeta . '/' . $value['su_id'] . '/' . $value[$this->_codigo] . '/' . $this->correlativo(8) . '/' . $this->correlativo(8) . '_' . $i . '.' . $ext[1];
                }

                switch ($i) {
                    case 2:
                        $resize->resize(98, 95);
                        break;
                    case 3:
                        switch ($formato) {
                            case 'promo':
                                $resize->resize(317, 175);
                                break;
                            default:
                                $resize->resize(100, 96);
                                break;
                        }
                        break;
                    case 4:
                        switch ($formato) {
                            case 'banner':
                                $resize->resize(455, 355); //455 x 355 
                                break;
                            case 'room':
                                $resize->resize(660, 300);
                                break;
                            default:
                                $resize->resize(645, 350);
                                break;
                        }
                        break;
                    default:
                        $resize->resize(30 * $i * ($i + 1), 30 * $i * ($i + 1));
                }
                $resize->save($ruta = MP . '/img' . $save['im_image' . $i]);
            }

            return $save;
        } catch (Zend_Exception $e) {
            Zend_Registry::get('Log')->log($e->getMessage(), Zend_Log::CRIT);
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function promocion() {
        
    }

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
