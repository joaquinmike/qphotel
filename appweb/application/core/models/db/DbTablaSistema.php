<?php

/**
 * CmsCategoriaArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbTablaSistema extends Cit_Db_Table_Abstract {

    protected $_name = 'cms_categoria_articulo';
    protected $_primary = array(
        'cat_id',
        'art_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Categoria
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        //echo $select; exit;
        if ($tipo == 'T'):
            $array = $this->fetchAll($select)->toArray();
        else:
            $array = $this->fetchRow($select);
        endif;

        return $array;
    }

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
