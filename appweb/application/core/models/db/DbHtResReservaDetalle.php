<?php

/**
 * HtResReservaDetalle Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtResReservaDetalle extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_reserva_detalle';
    protected $_primary = array('red_item', 'res_id');
    protected $_dependentTables = array('ht_res_reserva');
    protected $_referenceMap = array();

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

}
