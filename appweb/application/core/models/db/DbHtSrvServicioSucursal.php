<?php
/**
 * HtSrvServicioSucursal Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSrvServicioSucursal extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_ser_servicio_sucursal';

    protected $_primary = array(
        'ser_id',
        'su_id'
        );

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function saveData(array $data = ARray())
    {
    	$citObj = new CitDataGral();
        $this->_db->beginTransaction ();		
        try {
        	$sesion = new Zend_Session_Namespace('login');
        	$save['su_id'] = $data['su_id']; 
        	foreach ($data['grid'] as $value):
        		$save['ser_id'] = $value;
        		$this->createNewRow($save);
        	endforeach;
			$this->_db->commit();
			return 1;
		} catch ( Zend_Exception $e ) {
			$this->_db->rollBack ();
			throw new Zend_Db_Statement_Exception ( $e->getMessage () );
		}
    }

	/** Asigancion-accesrio-tipo-hab
	 * Elimina las accesorios por tipo de habitacion
	 *
	 * @param  $data string
	 * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
	 * @return array de la consulta
	*/
    public function deleteData(array $data = Array()){
    	$sesion = new Zend_Session_Namespace('login');
    	$citObj = new CitDataGral();
    	$resObj = new DbHtResReserva();
        $this->_db->beginTransaction ();		
        try {
        	$i = 0; $cont = 0;
        	foreach ($data['grid'] as $value):
        		$where = "t1.su_id = '{$data['su_id']}' and t2.ser_id = '{$value}'";
        		$dataExis = $resObj->returnReservaDetalle($where);
        		if(empty($dataExis)):
        			//var_dump("ser_id = '$value' and su_id = '{$data['su_id']}'");exit;
        			$this->delete("ser_id = '$value' and su_id = '{$data['su_id']}'");
        			$i++;
        		endif;
				$cont++;
        	endforeach;
			
			
			if($i == $cont):
				$array['id'] = 1;
				$array['desc'] = 'Eliminación completa de los servicios seleccionados';
			else:
				if($i == 0){
					$array['id'] = 2;
					$array['desc'] = 'Todos tienes datos relacionados';
				}else{
					$array['id'] = 1;
					$array['desc'] = 'Eliminación parcial, existen datos relacionados';
				}
			endif;
			$this->_db->commit();
			return $array;
			
		} catch ( Zend_Exception $e ) {
			$this->_db->rollBack ();
			throw new Zend_Db_Statement_Exception ( $e->getMessage () );
		}
    }
    
    /** Asigancion-servicio-sucursal
	 * Muestra las accesorios por tipo de habitacion
	 *
	 * @param  $data string
	 * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
	 * @return array de la consulta
	*/
    public function getServicioSuc($where = '',$order  = ''){
    	$sesion = new Zend_Session_Namespace('login');
    	$select = $this->select()->setIntegrityCheck(false);//ht_hb_accesorio_tipo_habitacion
    	$select->from(array('t1'=>'ht_ser_servicio_sucursal'),array('lleva'=>'t1.ser_id','su_id','ser_id'))
               ->join(array('t2'=>'vht_ser_servicio'),'t1.ser_id = t2.ser_id',array('ser_razsocial','ser_titulo'))
               ->join(array('t3'=>'vht_ser_tipo'), 't2.set_id = t3.set_id and t2.id_id = t3.id_id', array('set_titulo'))
    	       ->where("t2.id_id = '{$sesion->lg}'");
    	if(is_array($where)){
			if(!empty($where['where']))
	    		$select->where($where['where']);
	    	if(!empty($where['order']))
	    		$select->order($where['order']);
    	}else{
    		if(!empty($where))
    			$select->where($where);
    	}
    	if(!empty($order))
    		$select->order($order);
    	//echo $select; exit;
    	$dtaServSuc = $this->fetchAll($select)->toArray();
    	return $dtaServSuc;
    	
    }//selTipohabxSucursal

}
