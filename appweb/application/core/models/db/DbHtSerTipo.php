<?php

/**
 * HtSerTipo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSerTipo extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_ser_tipo';
    protected $_primary = array('set_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function deleteData(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $idiomaObj = new DbHtCmsIdioma();
            $idiomaObj->deleteTablas(new DbHtSerIdiomaTipo(), "set_id = '{$data['id']}'");
            $this->delete("set_id = '{$data['id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
