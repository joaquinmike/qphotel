<?php

/**
 * HtHabTarifa Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbTarifa extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_tarifa';
    protected $_primary = array('ta_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData() {
        $this->_db->beginTransaction();
        try {

            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteData(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $idiomaObj = new DbHtCmsIdioma();
            $idiomaObj->deleteTablas(new DbHtHbIdiomaTarifa(), "ta_id = '{$data['id']}'");
            //var_dump("ta_id = '{$data['id']}'"); exit;
            $this->delete("ta_id = '{$data['id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getTarifas($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_habitacion_tarifa'), array('ta_id', 'hb_precio'))
                ->where("t1.su_id = '{$data['su_id']}' and t1.tih_id = '{$data['tih_id']}' and t1.hb_capacidad = '{$data['hb_capacidad']}' and t1.ta_id = '{$data['ta_id']}'");
        //echo $select;exit;
        $dtaArticulo = $this->fetchAll($select)->toArray();
        return $dtaArticulo;
    }

    public function getPrecioHabitacion($post, array $data = Array(),$promo = array()) {
        $objDate = new Cit_Db_CitFechas();
//        $num = $objDate->restaFechas($data['fecha_ini'], $data['fecha_fin']);
//        $num = ($num < 3)?$num-1:$num;
//        $fecfin = $objDate->suma_fechas($data['fecha_ini'], $num);//$num - 1
        $objDate->setData($data['fecha_ini']);
        $fechaini = $objDate->renders('save');
       
        $objDesde = new Zend_Date($data['fecha_ini']);
        $objHasta = new Zend_Date($data['fecha_fin']);
        $dias = $objHasta->getDate()->get(Zend_Date::TIMESTAMP) - $objDesde->getDate()->get(Zend_Date::TIMESTAMP);
        $dias = (int) ($dias / (60 * 60 * 24));
        //$dias = ($dias > 1)?$dias-1:$dias;
        $fecfin = $objDesde->addDay($dias-1);
        
        $objDate->setData($fecfin->get('d/m/Y'));
        $fechafin = $objDate->renders('save');
        
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t10' => 'ht_res_kardex'), array('hb_id' => 'distinct(t10.hb_id)', 'count' => 'count(*)','tih_id'))
                ->join(array('t20' => 'ht_hb_habitacion_tarifa'), 't10.su_id=t20.su_id and t10.tih_id=t20.tih_id and t10.ta_id=t20.ta_id', array(''))
                ->where('t10.tih_id =?', $post['id'])
                ->where('t10.su_id =?', $data['ho_id'])
                ->where("t10.ka_fecha BETWEEN '$fechaini' and '$fechafin'")
                 ->where('t10.ka_situacion =?', '1')
                ->where('t20.hb_capacidad =?', $data['num_adults'])
                ->limit(1)//Solo se esta pidiendo un cuarto por default
                ->group(array('t10.hb_id','t10.tih_id'))->order('2 desc');
        
        $resumen = $select->query()->fetch();
        //$objDate->setData($fecfin);
        //$fec_noches = $objDate->renders('save');
       
        //Sumatoria de todos los días + verifica si hay promo
        $select2 = $this->select()->setIntegrityCheck(false);
        $select2->from(array('t1' => 'ht_res_kardex'), array('hb_precio' => 'sum(hb_precio)'))
                ->join(array('t2' => 'ht_hb_habitacion_tarifa'), 't1.su_id=t2.su_id and t1.tih_id=t2.tih_id and t1.ta_id=t2.ta_id', array(''))
                ->joinLeft(array('t3' => 'ht_cms_promocion_sucursal'), 
                    "t1.su_id = t3.su_id  and t1.tih_id = t3.tih_id and t3.an_id = '{$promo['anio']}' and t3.pro_id = '{$promo['id']}'", 
                    array(''))
                ->joinLeft(array('t4' => 'ht_cms_promocion'), 
                    't3.pro_id = t4.pro_id and t3.an_id = t4.an_id', 
                    array('promo' => 
                        new Zend_Db_Expr('sum(CASE WHEN (ka_fecha BETWEEN pro_fecini and pro_fecfin) THEN hb_precio*pro_dscto/100 ELSE 0 END)')))
                ->where('t1.tih_id =?', $post['id'])
                ->where('t1.su_id =?', $data['ho_id'])
                ->where("t1.ka_fecha BETWEEN '$fechaini' and '$fechafin'")
                ->where('t1.ka_situacion =?', '1')
                ->where('t2.hb_capacidad =?', $data['num_adults'])
                ->where("t1.hb_id in ('{$resumen['hb_id']}')");
//                    ->limit($dat[3]);
                //echo $select2; exit;
        $precio = $select2->query()->fetch();
        //echo $select2; exit;
        $precio['hb_id'] = $resumen['hb_id'];
        $precio['tih_id'] = $resumen['tih_id'];
        $precio['promo'] = round($precio['promo'], 2);
        return $precio;
    }

}
