<?php

/**
 * DbHtHbIdiomaSucTiphab Db_Table_Abstract 
 * @Category Cit
 * @Author Information Technology Community 
 * @Version V. 1.0
 */
class DbHtHbIdiomaSucTiphab extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_idioma_suc_tiphab';
    protected $_primary = array('su_id','tih_id','id_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $dtaSuctipRoom = $this->fetchRow(
                array(
                    'tih_id =?' => $data['tih_id'],'su_id =?' => $data['su_id'],'id_id =?' => $data['id_id']
                )
            );
            if(empty($dtaSuctipRoom['id_id'])){            
                $this->insert(
                        array('tih_id' => $data['tih_id'],'su_id' => $data['su_id'],
                            'id_id' => $data['id_id'],'tih_desc' => $data['tih_desc']
                            )
                        );
            }else{
            $this->update(array('tih_desc' => $data['tih_desc']),
                array('tih_id =?' => $data['tih_id'],'su_id =?' => $data['su_id'],'id_id =?' => $data['id_id'])
                );
            }
            $this->_db->commit();
            return 1;
            //return array('action'=>1,'cod'=>$data['fa_id'],'tabla'=>'familia');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack ();
            throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
        
    }
    
    public function selectOneRow($data)
    {
          $select = $this->select()
                ->from($this->_name,array('tih_id','su_id'))
                ->where('tih_id =?',$data['tih_id'])
                ->where('su_id =?',$data['su_id']);
          return $this->fetchAll($select)->toArray();
    }
    

}
