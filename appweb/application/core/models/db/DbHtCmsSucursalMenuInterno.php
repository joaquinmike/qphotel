<?php

class DbHtCmsSucursalMenuInterno extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_sucursal_menu_interno';
    protected $_primary = array('cat_id',
        'su_id', 'tih_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function getDatosSucurMenuInterno(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false)//ht_hb_accesorio_tipo_habitacion
                ->from(array('t1' => 'ht_cms_sucursal_menu_interno'), array('lleva' => 'tih_id', 'su_id', 'tih_id', 'cat_id', 'cat_orden'))
                ->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id and t1.cat_id = ' .
                        "'{$data['cat_id']}'", array('t2.tih_desc', 't2.tih_abr'))
                ->where("id_id = '{$sesion->lg}' and su_id = '{$data['su_id']}'")
                // ->where("cat_id = '{$data['cat_id']}'")
                ->order('t1.cat_orden');
        // echo $select; exit;
        $dta = $this->fetchAll($select)->toArray();
        return $dta;
    }

    public function selMenuInterno(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('lleva' => 'tih_id', 'tih_id', 'tih_desc'));
        $select->where("id_id = '$sesion->lg' and cat_id = '{$data['cat_id']}'");
        $select->order('tih_id');

        $dataMenu = $this->fetchAll($select)->toArray();

        $select2 = $this->select()->setIntegrityCheck(false);
        $select2->from(array('t1' => 'ht_cms_sucursal_menu_interno'), array('tih_id', 'cat_id', 'su_id'));
        $select2->where("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
        $select2->order('tih_id');
        //echo $select2; exit;
        $existe = array();
        $array = array();
        $dataExiste = $this->fetchAll($select2)->toArray();
        foreach ($dataExiste as $result):
            $existe[] = $result['tih_id'];
        endforeach;
        //$i = 0;
        foreach ($dataMenu as $value):
            //foreach ($dataExiste as $result):
            if (!in_array($value['tih_id'], $existe)):
                $array[] = $value;
            endif;

        //endforeach;
        endforeach;

        return $array;
    }

    public function saveSucursalCategorias(array $data = Array()) {

        $this->_db->beginTransaction();
        try {
            //$this->delete("su_id = '{$data['su_id']}' and cat_id = '{$data['cat_id']}'");
            $save['su_id'] = $data['su_id'];
            $save['cat_id'] = $data['cat_id'];
            foreach ($data['grid'] as $value):
                $save['tih_id'] = $value;
                $this->createNewRow($save);
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteSucursalCategorias(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $save['su_id'] = $data['su_id'];
            $save['cat_id'] = $data['cat_id'];
            foreach ($data['grid'] as $value):
                $this->delete("tih_id = '{$value}' and cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}'");
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}