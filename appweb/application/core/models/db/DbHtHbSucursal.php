<?php

/**
 * HtHbSucursal Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbSucursal extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_sucursal';
    protected $_primary = array('su_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
        $login = new Zend_Session_Namespace('login');
        $sucursal = new DbHtHbSucursal();
        $this->_db->beginTransaction();
        try {
            switch ($data['action']) {
                case 'add':
                    $data['su_id'] = $sucursal->correlativo(8);
                    $this->createNewRow($data);
                    $id = $data['su_id'];
                    break;

                case 'edit':
                    $id = $data['su_id'];
                    //var_dump($data);exit;
                    $su_id = $data['su_id'];
                    unset($data['su_id']);
                    $this->updateRows($data, "su_id='{$su_id}'");
                    break;
            }
            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_hb_sucursal');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }

        return false;
    }

    public function deleteSuc(array $data = array()) {
        $login = new Zend_Session_Namespace('login');
        $sucursal = new DbHtHbSucursal();
        $this->_db->beginTransaction();

        try {
            //$sucursal->delete( "su_id='{$data['su_id']}'");
            $this->delete("su_id='{$data['su_id']}'");

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    /**
     * Devuelve lista de sucursales
     *
     * @param  $ho_id string Código de hotel
     * @return array resultado de la consulta
     */
    public function getListaSucursal($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_sucursal'), array('hsu_id' => 'su_id', 'su_id', 'ho_id', 'su_nombre', 'su_estado', 'su_telefono1', 'su_telefono2', 'su_ruc', 'su_direccion', 'su_desc'))
                ->join(array('t2' => 'ht_seg_ubigeo'), 't1.ub_id = t2.ub_id', array('ub_desc', 'ub_id'));
        $select->where("su_portal = 'N'");
        if ($sesion->tipcod != '0001')
            $select->where("su_id = '{$sesion->suid}'");

        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaSuc = $this->fetchAll($select)->toArray();
        return $dtaSuc;
    }

}
