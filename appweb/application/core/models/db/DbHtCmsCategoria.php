<?php

/**
 * HtCmsCategoria Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsCategoria extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_categoria';
    protected $_primary = array('cat_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();
    const CAT_PROMO = '20110015';
    /**
     * Devolver Datos Cms Categoria
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

    public function saveData(array $data = array()) {
        $categoriaidioma = new DbHtCmsIdiomaCategoria();
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            switch ($data['action']) {
                case 'add':
                    $data['cat_id'] = $this->correlativo(8);
                    //var_dump($data);exit;
                    $this->createNewRow($data);
                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $categoriaidioma->createNewRow(array('id_id' => $idioma['id_id'], 'cat_id' => $data['cat_id'], 'cat_titulo' => $data['cat_titulo']));
                    }
//	        		$categoriaidioma->createNewRow($data);
                    $id = $data['cat_id'];
                    break;
                case 'edit':
                    $this->update(array('cat_estado' => $data['cat_estado'], 'cat_public' => $data['cat_public'], 'cat_div' => $data['cat_div']), "cat_id='{$data['cat_id']}'");
                    //echo "cat_id='{$data['cat_id']}' and id_id='{$data['id_id']}'";exit;
                    $categoriaidioma->updateRows($data, "cat_id='{$data['cat_id']}' and id_id='{$data['id_id']}'");
                    $id = $data['cat_id'];
                    break;
            }
            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_cms_categoria');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteCategoria(array $data = array()) {
        $this->_db->beginTransaction();
        try {
            $this->delete("cat_id = '{$data['cat_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    public function getBannerArticulos($su_id = '00000002', $id_id = 'ES'){
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_cms_articulo'), array('art_id','art_titulo'))
            ->join(array('t2' => 'cms_categoria_articulo'), 't1.art_id = t2.art_id', array())
            ->where('t2.cat_id =?', $cat_id)
            ->where('su_id =?', self::CAT_PROMO)
            ->where('id_id =?', $id_id);
        $data = $this->fetchAll($select)->toArray();
        return $data;
        
    }
    
    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }
    
    

}
