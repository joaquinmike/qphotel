<?php

/**
 * CmsCategoriaArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsMenuInterno extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_cms_menu_interno';
    protected $_primary = array('cat_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Categoria
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
       
        if ($tipo == 'T')
             $array = $this->fetchAll($select)->toArray();
        else
            $array = $this->fetchRow($select)->toArray();
        
        return $array;
    }

}
