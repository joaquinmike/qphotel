<?php

class DbHtResIdiomaAnio extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_res_idioma_anio';
    protected $_primary = array('an_id', 'id_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

}