<?php
/**
 * HtCmsIdiomaCategoria Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaCategoria extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_categoria';

    protected $_primary = array(
        'id_id',
        'cat_id'
        );

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function saveData()
    {
        $this->_db->beginTransaction ();		
        					try {
        						
        					
        						$this->_db->commit();
        						return $id;
        					} catch ( Zend_Exception $e ) {
        						$this->_db->rollBack ();
        						throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        					}
        					return false;
    }

    public function getDataId()
    {
        $select = $this->select();
        					if(!empty($data['where'])){
        						$select->where("");
        					}
        					if(!empty($data['order'])){
        						$select->order($data['order']);
        					}
        					return $this->fetchAll($select)->toArray();
    }


}
