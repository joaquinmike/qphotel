<?php

/**
 * HtSegCliente Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegCliente extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_seg_cliente';
    protected $_primary = array('cl_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
        $login = new Zend_Session_Namespace('login');
        $objPersHotel = new DbHtSegPersonaHotel();
        $objPerson = new DbHtSegPersona();
        $fecObj = new Cit_Db_CitFechas();
        $this->_db->beginTransaction();

        try {
            $fecObj->setFecha($data['pe_fec_nacimiento']);
            $fecObj->setData($data['pe_fec_nacimiento']);
            $data['pe_fec_nacimiento'] = $fecObj->renders('save');

            $data['pe_nomcomp'] = $data['pe_paterno'] . ' ' . $data['pe_materno'] . ' ' . $data['pe_nombre'];

            switch ($data['action']) {
                case 'add':

                    $data['pe_id'] = $objPerson->correlativo(10);
                    $objPerson->createNewRow($data);
                    $data['cl_id'] = $objPerson->correlativo(8);
                    $this->createNewRow(array('cl_id' => $data['cl_id'], 'pe_id' => $data['pe_id'], 'cl_estado' => $data['cl_estado']));
                    $id = $data['cl_id'] . '**' . $data['pe_id'];
                    $objPersHotel->createNewRow(array('pe_id' => $data['pe_id'], 'ho_id' => $login->hoid));
                    break;

                case 'edit':
                    $id = $data['cl_id'] . '**' . $data['pe_id'];
                    $pe_id = $data['pe_id'];
                    unset($data['pe_id']);
                    $cl_id = $data['cl_id'];
                    unset($data['cl_id']);
                    //$objPerson->updateRows($data, "pe_id='{$pe_id}'");
                    //$this->updateRows($data,"cl_id='{$cl_id}'");
                    break;
            }

            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_seg_persona');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }

        return false;
    }

    public function deleteCliente(array $data = array()) {
        $login = new Zend_Session_Namespace('login');
        $objPersHotel = new DbHtSegPersonaHotel();
        $objPerson = new DbHtSegPersona();
        $objUsuario = new DbHtSegUsuario();
        $this->_db->beginTransaction();
        try {
            $this->delete("cl_id='{$data['cl_id']}'");
            $objUsuario->delete("pe_id='{$data['pe_id']}'");
            $objPersHotel->delete("pe_id='{$data['pe_id']}'");
            $objPerson->delete("pe_id='{$data['pe_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
