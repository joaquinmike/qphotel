<?php
/**
 * HtResBonos Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtResBonos extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_res_bonos';

    protected $_primary = array('bo_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function saveData(array $data = array())
    {
    	$citObj = new CitDataGral();
        $this->_db->beginTransaction ();		
        try {
            $this->update($data, "bo_id = 'DO'");
            $this->_db->commit();
            return 1;

        } catch ( Zend_Exception $e ) {
            $this->_db->rollBack ();
            throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

    public function getDataId()
    {
        $select = $this->select();
        if(!empty($data['where'])){
            $select->where("");
        }
        if(!empty($data['order'])){
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }


}
