<?php

/**
 * TablaDetalle Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbTablaDetalle extends Cit_Db_Table_Abstract {

    protected $_name = 'tabla_detalle';
    protected $_primary = array(
        'tad_id',
        'tba_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData() {
        $this->_db->beginTransaction();
        try {


            $this->_db->commit();
            return $id;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function deleteData(array $data = Array()) {
        $fecOjb = new Cit_Db_CitFechas();
        $this->_db->beginTransaction();
        try {
            $idiomaObj = new DbHtCmsIdioma();
            $idiomaObj->deleteTablas(new DbIdiomaTablaDet(), "tad_id = '{$data['tad_id']}' and tba_id = '{$data['tba_id']}'");

            $this->delete("tad_id = '{$data['tad_id']}' and tba_id = '{$data['tba_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
