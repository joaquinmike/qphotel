<?php

/**
 * HtHbHabitacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbHabitacion extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_habitacion';
    protected $_primary = array('hb_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Completos de una habitacion
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function saveImagen(array $data = array(), $formato = 'default') {

        $tipohabimage = new DbHtHbHabitacionImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'habitacion';
        $imagen->_codigo = 'hb_id';

        $this->_db->beginTransaction();
        try {

            $datanew = $imagen->ruteo($data, 'imgtipo', $formato);
            $imagen->createNewRow($datanew);
            $tipohabimage->createNewRow(array('im_id' => $datanew['im_id'], 'hb_id' => $data['hb_id']));
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
            //Zend_Registry::get('Log')->log($e->getMessage(), Zend_Log::CRIT);            
        }
    }

    public function deleteImagen(array $data = array()) {
        $tipohabimage = new DbHtHbHabitacionImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'habitacion';
        $select = $this->select()->setIntegrityCheck(false);

        $this->_db->beginTransaction();
        try {
            $data['imgids'] = str_replace('\\', '', $data['imgids']);
            ////var/www/sistemas/hotels/html/img/habitacion/01/00000062
            $select->from(array('t1' => 'ht_cms_imagenes'), array('im_id', 'im_image1', 'im_image2', 'im_image3', 'im_image4', 'im_image5'))
                    ->where("im_id in ({$data['imgids']})");
            //$select->from(array('t1'=>'ht_hb_habitacion_imagen'), array('hb_id', 'im_id'))
            //       ->where("im_id in ({$data['imgids']})");
            $dataImg = $select->query()->fetchall();
            $tipohabimage->delete("im_id in({$data['imgids']}) and hb_id = '{$data['hb_id']}'");
            $imagen->delete("im_id in ({$data['imgids']})");
            $this->_db->commit();
            foreach ($dataImg as $value) {
                unlink(MP . '/img' . $value['im_image1']);
                unlink(MP . '/img' . $value['im_image2']);
                unlink(MP . '/img' . $value['im_image3']);
                unlink(MP . '/img' . $value['im_image4']);
                unlink(MP . '/img' . $value['im_image5']);
            }
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDatosHabit($where = '', $order = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array(''));
        $select->join(array('t2' => 'vht_hb_habitacion'), 't1.tih_id = t2.tih_id and t1.su_id = t2.su_id and t2.id_id = ' . "'$sesion->lg'", array('hhb_id' => 'hb_id', 'hb_id', 'hb_desc', 'hb_numero', 'hb_estado'));
        $select->join(array('t3' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t3.tih_id and t2.id_id = t3.id_id', array('tih_id', 'tih_desc'));
        $select->join(array('t4' => 'vht_hb_piso'), 't2.pi_id = t4.pi_id and t2.id_id = t4.id_id and t1.su_id = t4.su_id', array('pi_id', 'pi_desc'));
        $select->join(array('t5' => 'ht_hb_sucursal'), 't1.su_id = t5.su_id', array('t1.su_id', 'su_nombre'));
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }

        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

    /**
     * Guardar datos de la habitacion
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function saveData(array $data = Array()) {
        $idiomHabObj = new DbHtHbIdiomaHabitacion();
        $citObj = new CitDataGral();
        $karObj = new DbHtResKardex();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');
            $data['hb_estado'] = 1;
            switch ($data['edit']):
                case 'save':
                    if (empty($data['an_id']))
                        $data['an_id'] = date('Y');

                    unset($data['edit']);
                    if (empty($data['hb_cantidad'])):
                        unset($data['hb_cantidad']);
                        $this->createNewRowData($data);
                        $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                        foreach ($arrIdiomas as $idioma) {
                            $idiomHabObj->createNewRow(array('id_id' => $idioma['id_id'], 'hb_id' => $data['hb_id'], 'hb_desc' => $data['hb_desc']));
                        }
                        //$idiomHabObj->createNewRow(array('id_id'=>$sesion->lg,'hb_id'=>$data['hb_id'],'hb_desc'=>$data['hb_desc']));
                        $karObj->saveKardexHabitacion($data, date('m'));

                    else:
                        $desc = $data['hb_desc'];
                        $cont = $data['hb_cantidad'];
                        unset($data['hb_cantidad']);
                        $num = (int) $data['hb_numero'];
                        for ($i = 0; $i < $cont; $i++):
                            $data['hb_desc'] = $desc . ' ' . $data['hb_numero'];
                            if (0 < $i):
                                $newID = (int) $data['hb_id'];
                                $newID++;
                                $data['hb_id'] = str_pad($newID, 8, '0', STR_PAD_LEFT);
                            endif;
                            //var_dump(array(array('numero'=>$data['hb_numero'],'desc'=>$data['hb_desc'])));
                            $this->createNewRowData($data);
                            $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                            foreach ($arrIdiomas as $idioma) {
                                $idiomHabObj->createNewRow(array('id_id' => $idioma['id_id'], 'hb_id' => $data['hb_id'], 'hb_desc' => $data['hb_desc']));
                            }
//							$idiomHabObj->createNewRow(array('id_id'=>$sesion->lg,'hb_id'=>$data['hb_id'],'hb_desc'=>$data['hb_desc']));

                            $karObj->saveKardexHabitacion($data, date('m'));
                            $num++;
                            $data['hb_numero'] = $num;
                        endfor;
                    endif;
                    //var_dump(array('id_id'=>$sesion->lg,'hb_id'=>$data['hb_id'],'hb_desc'=>$data['hb_desc']));exit;
                    break;

                case 'edit':
                    $hb_id = $data['hb_id'];
                    unset($data['edit']);
                    unset($data['hb_id']);
                    $this->updateRows($data, "hb_id = '$hb_id'");
                    $idiomHabObj->update(array('hb_desc' => $data['hb_desc']), "hb_id = '$hb_id' and id_id = '{$sesion->lg}'");
                    break;
            endswitch;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * Elimindar datos de la habitacion
     *
     * @param  $data string
     * @return array de la consulta
     */
    public function deleteHabitacion(array $data = Array()) {
        $citObj = new CitDataGral();
        $idiomHabObj = new DbHtHbIdiomaHabitacion();
        $resKardexObj = new DbHtResKardex();
        try {
            $this->_db->beginTransaction();
            $sesion = new Zend_Session_Namespace('login');

            //delete from ht_res_kardex where hb_id = '00000061'
            //delete from ht_hb_idioma_habitacion where hb_id = '00000061'
            //delete from ht_hb_habitacion where hb_id = '00000061'
            $resKardexObj->delete("hb_id = '{$data['id']}'");
            $idiomHabObj->delete("hb_id = '{$data['id']}'");
            $this->delete("hb_id = '{$data['id']}'");
            /* $dtaReserva = $citObj->getDataGral('ht_res_reserva_detalle',array('hb_id'),"hb_id = '{$data['id']}'");
              if(empty($dtaReserva)):
              $idiomHabObj->delete("hb_id = '{$data['id']}'");
              $this->delete("hb_id = '{$data['id']}'");
              else:
              return 2;
              endif; */

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /**
     * Mantenimiento-habitacion
     * Filtro de Tipo de Habitaciones por sucursal
     *
     * @param  $data string
     * @return array de la consulta
     */
    public function getSucursalTipohab($where = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false); //
        $select->from(array('t1' => 'ht_hb_sucursal_tipo_habitacion'), array(''));
        $select->join(array('t2' => 'vht_hb_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('t2.tih_id', 'state' => 't2.tih_desc'));
        $select->where("id_id = '{$sesion->lg}'");
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);

        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

}
