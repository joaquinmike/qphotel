<?php
/**
 * VIdiomaSucTipoHabitacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbVHtHbIdiomaSucTipoHabitacion extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_hb_idioma_suc_tiphab';

    protected $_primary = array('su_id','tih_id','id_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    
}
