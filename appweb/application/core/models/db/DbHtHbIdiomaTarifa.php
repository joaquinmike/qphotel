<?php

/**
 * HbIdiomaTarifa Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2011 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbIdiomaTarifa extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_idioma_tarifa';
    protected $_primary = array('ta_id', 'id_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

}
