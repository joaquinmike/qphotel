<?php

/**
 * HtHabTipoHabitacion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbTipoHabitacion extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_tipo_habitacion';
    protected $_primary = array('tih_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Tipo de Habitación
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => $this->_name), $data);
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        
        if ($tipo == 'T'):
            $array = $this->fetchAll($select)->toArray();
        else:
            $array = $this->fetchRow($select);
        endif;

        return $array;
    }

    /**
     * Devolver Datos Tipo de Habitación
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getCombo($where = '', $tipo = 'T') {
        //var_dump($data);
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('tih_id', 'tih_desc'));
        $select->where("id_id = '{$sesion->lg}' and cat_id = 'H'");
        if (!empty($where))
            $select->where($where);

        //echo $select; exit;
        $dtaCbo = $this->fetchAll($select)->toArray();
        return $dtaCbo;
    }

    public function deleteData(array $data = Array()) {
        $this->_db->beginTransaction();
        try {
            $idiomaObj = new DbHtCmsIdioma();
            $idiomaObj->deleteTablas(new DbHtHabIdiomaTipoHabitacion(), "tih_id = '{$data['id']}'");
            $this->delete("tih_id = '{$data['id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteCliente(array $data = array()) {
        $this->_db->beginTransaction();
        try {
            $this->delete("tih_id = '{$data['tih_id']}'");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 0;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function saveImagen(array $data = array(), $format = 'default') {
        $objTipRoom = new DbHtHbTipoHabitacionImagen();
        $objImage = new DbHtCmsImagenes();
        $objImage->_carpeta = 'tipohabitacion';
        $objImage->_codigo = 'tih_id';
        $this->_db->beginTransaction();
        try {
            $dtaImage = $objImage->ruteo($data, 'imgtipo', $format);
            $objImage->createNewRow($dtaImage);
            $objTipRoom->createNewRow(array('im_id' => $dtaImage['im_id'], 'tih_id' => $data['tih_id'],'su_id' => $data['su_id']));
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
        }
    }

    public function saveData(array $data = array()) {
        $idiomtiphabObj = new DbHtHbIdiomaTipoHabitacion();
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            switch ($data['action']):
                case 'add':
                    $data['tih_id'] = $this->correlativo(2);
                    $this->createNewRow($data);

                    $arrIdiomas = $citObj->getDataGral('ht_cms_idioma', array('id_id'), array('where' => "id_estado like '1'"), 'T');
                    foreach ($arrIdiomas as $idioma) {
                        $idiomtiphabObj->createNewRow(array('tih_id' => $data['tih_id'], 'id_id' => $idioma['id_id'], 'tih_desc' => $data['tih_desc'], 'tih_abr' => $data['tih_abr'], 'tih_coment' => $data['tih_coment']));
                        //$categoriaidioma->createNewRow(array('id_id'=>$idioma['id_id'],'cat_id'=>$data['cat_id'],'cat_titulo'=>$data['cat_titulo']));
                    }
                    $id = $data['tih_id'];
                    break;

                case 'edit':
                    $this->updateRows($data, "tih_id='{$data['tih_id']}'");
                    $idiomtiphabObj->update(array('tih_desc' => $data['tih_desc'], 'tih_abr' => $data['tih_abr'], 'tih_coment' => $data['tih_coment']), "tih_id='{$data['tih_id']}' and id_id='{$data['id_id']}'");
                    $id = $data['tih_id'];
                    break;
            endswitch;
            $this->_db->commit();
            return array('action' => 1, 'cod' => $id, 'tabla' => 'ht_hb_tipo_habitacion');
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asigancion-accesrio-tipo-hab
     * Devolver Datos Completos de una Accesorios
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getDatosSucurTipohab($where = '', $order = '') {
        $citObj = new CitDataGral();

        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'vht_hb_tipo_habitacion'), array('lleva' => 't1.tih_id', 't1.tih_id', 'tih_desc', 'tih_abr'));
        $select->join(array('t2' => 'ht_hb_sucursal_tipo_habitacion'), 't1.tih_id = t2.tih_id', array('su_id','tih_state'));
        $select->where("id_id = '{$sesion->lg}' and tih_estado = '1' and cat_id = 'H'");
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        $i = 0;
        foreach ($dtaHabit as $value):
            $save = $citObj->getDataGral('ht_hb_habitacion', array('stock' => new Zend_Db_Expr('count(*)')), "su_id = '{$_POST['su_id']}' and tih_id = '{$value['tih_id']}'", 'U');
            $dtaHabit[$i]['tih_stock'] = $save['stock'];
            $i++;
        endforeach;
        return $dtaHabit;
    }

}
