<?php

/**
 * CmsImagenesArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbCmsImagenesArticulo extends Cit_Db_Table_Abstract {

    protected $_name = 'cms_imagenes_articulo';
    protected $_primary = array(
        'im_id',
        'art_id',
        'su_id',
        'cat_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Imagenes del Articulo
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function saveImagen(array $data = array(), $formato = 'default') {
        //$tipohabimage = new DbHtHbHabitacionImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'articulo';
        $imagen->_codigo = 'art_id';
        $this->_db->beginTransaction();
        try {

            $datanew = $imagen->ruteo($data, 'articulo', $formato);
            $imagen->createNewRow($datanew);
            $this->createNewRow(
                    array(
                        'im_id' => $datanew['im_id'], 
                        'su_id' => $data['su_id'], 
                        'cat_id' => $data['cat_id'], 
                        'art_id' => $data['art_id']
                        )
                    );
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function deleteImagen(array $data = array()) {

        //$tipohabimage = new DbHtHbHabitacionImagen();
        $imagen = new DbHtCmsImagenes();
        $imagen->_carpeta = 'articulo';
        $imagen->_codigo = 'art_id';
        $this->_db->beginTransaction();
        try {
            $data['imgids'] = str_replace("\\", '', $data['imgids']);
            $this->delete("art_id = '{$data['art_id']}' and su_id = '{$data['su_id']}'and cat_id = '{$data['cat_id']}' and im_id in ({$data['imgids']})");
            $imagen->delete("im_id in ({$data['imgids']})");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getListaImagenes($data) {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => 'ht_cms_imagenes'), array('*'))
                ->joinleft(array('t3' => 'cms_imagenes_articulo'), 't1.im_id=t3.im_id', array(''))
                ->where('t3.art_id =?', $data['art_id'])
                ->where('t3.cat_id =?', $data['cat_id'])
                ->where('t3.su_id =?', $data['su_id']);
        $dataimagen = $select->query()->fetchAll();
        return($dataimagen);
    }

}
