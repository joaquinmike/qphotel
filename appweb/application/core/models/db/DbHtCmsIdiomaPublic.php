<?php
/**
 * DbHtCmsIdiomaPublic Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaPublic extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_public';

    protected $_primary = array('pub_id','id_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	
}
