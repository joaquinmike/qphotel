<?php

/**
 * HtSegPerfil Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSegPerfil extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_seg_perfil';
    protected $_primary = array('per_id');
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    public function saveData(array $data = array()) {
        $idiomaperfil = new DbHtSegIdiomaPerfil();
        $this->_db->beginTransaction();
        try {

            $newdat = explode('**', $data['data']);
            foreach ($newdat as $value):
                list($per_id, $per_desc) = explode('::', $value);
                $idiomaperfil->update(array('per_desc' => $per_desc), "per_id='$per_id' and id_id='{$data['id_id']}'");

            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            return 0;
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function saveSegPerfil(array $data = array()) {
        $idiomaObj = new DbHtSegIdiomaPerfil();
        $this->_db->beginTransaction();
        try {
            $this->createNewRow(array('per_id' => $data['tip_id'], 'per_estado' => '1'));
            $idiomaObj->createNewRow(array('id_id' => $data['id_id'], 'per_id' => $data['tip_id'], 'per_desc' => $data['tip_desc']));

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function deleteSegPerfil(array $data = Array()) {
        $idiomaObj = new DbHtSegIdiomaPerfil();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');

            $idiomaObj->delete("per_id = '{$data['id']}'");

            $this->delete("per_id = '{$data['id']}'");

            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 0;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

}
