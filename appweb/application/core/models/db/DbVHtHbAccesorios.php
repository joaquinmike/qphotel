<?php
/**
 * HtHabAccesorios Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbVHtHbAccesorios extends Cit_Db_Table_Abstract
{

    protected $_name = 'vht_hb_accesorios';

    protected $_primary = array('ac_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	
}
