<?php
/**
 * DbHtCmsidiomaDesc Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsIdiomaDesc extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_idioma_desc';

    protected $_primary = array('des_id','id_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	
}
