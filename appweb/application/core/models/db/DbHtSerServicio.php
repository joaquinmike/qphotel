<?php
/**
 * HtSerServicio Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtSerServicio extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_ser_servicio';

    protected $_primary = array('ser_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

    public function saveData(array $data = array())
    {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $servicio = new DbHtSerServicio();
        $seridiomaObj = new DbHtSrvIdiomaServicio();
    	$this->_db->beginTransaction ();	
        	
       try {
       		switch($data['action']){
       			case 'add':
			    	$data['ser_id'] = $servicio->correlativo(8);
			    	$this->createNewRowData($data);
	        		$id = $data['ser_id'];
       				$arrIdiomas = $citObj->getDataGral('ht_cms_idioma',array('id_id'),array('where'=>"id_estado like '1'"),'T');
					foreach ($arrIdiomas as $idioma) {
						$seridiomaObj->createNewRow(array('id_id'=>$idioma['id_id'],'ser_id'=>$data['ser_id'],'ser_titulo'=>$data['ser_titulo'],'ser_desc'=>$data['ser_desc']));
			        }
	        		//$seridiomaObj->createNewRow(array('id_id'=>$sesion->lg,'ser_id'=>$data['ser_id'],'ser_titulo'=>$data['ser_titulo'],'ser_desc'=>$data['ser_desc']));
	        		//var_dump(array('id_id'=>$sesion->lg,'ser_id'=>$data['ser_id'],'ser_titulo'=>$data['ser_titulo'],'ser_desc'=>$data['ser_desc']));exit;
					break;
			    case 'edit':
		       		//unset($data['actio'])
		       		$seridiomaObj->updateRows($data, "ser_id = '{$data['ser_id']}' and id_id = '{$sesion->lg}'");
			    	$this->updateRows($data,"ser_id='{$data['ser_id']}'");
			    	break;
       		}
		    $this->_db->commit();
		    return 1;
		} catch ( Zend_Exception $e ) {
			$this->_db->rollBack ();
			//return 2;
			throw new Zend_Db_Statement_Exception ( $e->getMessage () );
		}
      		 
				
    }
    
	public function deleteSer(array $data = array())
	    {
	    	$sesion = new Zend_Session_Namespace('login');
	        $servicio = new DbHtSerServicio();
	        $servcioIdioma = new DbHtSrvIdiomaServicio();
	        //$citObj = new CitDataGral();
	    	$this->_db->beginTransaction ();
	    	try {
				$servcioIdioma->delete("ser_id='{$data['id']}'");
				$this->delete("ser_id = '{$data['id']}'");
	    		$this->_db->commit();
				return 1;
			} catch ( Zend_Exception $e ) {
				$this->_db->rollBack ();
				return 2;
				//throw new Zend_Db_Statement_Exception ( $e->getMessage () );
			}
	      		 
				return false;		
	    }

    public function getDataId()
    {
        $select = $this->select();
        					if(!empty($data['where'])){
        						$select->where("");
        					}
        					if(!empty($data['order'])){
        						$select->order($data['order']);
        					}
        					return $this->fetchAll($select)->toArray();
    }

    
    public function selListaAsignarServicio($data){
    	$array = array();
    	$sesion = new Zend_Session_Namespace('login');
    	$select = $this->select()->setIntegrityCheck(false);
    	$select->from(array('t1'=>'vht_ser_servicio'),array('lleva'=>'ser_id','ser_id','ser_razsocial','ser_titulo'));
    	
    	$select->where("id_id = '$sesion->lg'");
    	if(!empty($data['su_id']))
	    	$select->where("
	    	 ser_id not in (select ser_id from ht_ser_servicio_sucursal where su_id='{$data['su_id']}')
	    	");
	    if(!empty($data['set_id'])){
	    	$select->where("set_id='{$data['set_id']}'");
	    }
    	$select->order('ser_id');
    	//echo $select;exit;
    	$dataServicio = $this->fetchAll($select)->toArray();
    	//var_dump($dataAccesorio,$existe); exit;
    	return $dataServicio;
    }
    
    public function getServicioSucursal($hoid){
    	$array = array();
    	$sesion = new Zend_Session_Namespace('web');
    	$select = $this->select()->setIntegrityCheck(false);
    	$select->from(array('t1'=>'ht_ser_servicio_sucursal'),array(''));
    	$select->join(array('t2'=>'vht_ser_servicio'),"t1.ser_id = t2.ser_id and id_id = '{$sesion->lg}'",array('lleva'=>'ser_id','ser_id','ser_titulo','ser_web','ser_precio'));
    	$select->where("t1.su_id = '{$hoid}'");
    	$dataServicio = $this->fetchAll($select)->toArray();
    	//var_dump($dataAccesorio,$existe); exit;
    	return $dataServicio;
    }
}
