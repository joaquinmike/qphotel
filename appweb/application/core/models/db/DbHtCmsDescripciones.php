<?php
/**
 * DbHtCmsDescripcion Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtCmsDescripciones extends Cit_Db_Table_Abstract
{

    protected $_name = 'ht_cms_descripciones';

    protected $_primary = array('des_id');

    protected $_dependentTables = array();

    protected $_referenceMap = array();

	public function deleteData(array $data = Array()){
    	$this->_db->beginTransaction ();		
        try {
        	$idiomaObj = new DbHtCmsIdioma();
        	$idiomaObj->deleteTablas(new DbHtCmsIdiomaDesc(),"des_id = '{$data['id']}'");
        	$this->delete("des_id = '{$data['id']}'");
        	$this->_db->commit();
        	return 1;
        } catch ( Zend_Exception $e ) {
        	$this->_db->rollBack ();
        	//return 0;
        	throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }
    
    public function saveData(array $data = Array()){
    	$this->_db->beginTransaction ();
    	$sesion = new Zend_Session_Namespace('login');
    	$idiomDescObj = new DbHtCmsIdiomaDesc();
        try {
            switch ($data['action']):
                case 'add':
                    
                    $this->createNewRow(array('des_id'=>$data['des_id'],'des_estado'=>$data['des_estado'],'des_nombre'=>$data['des_nombre']));
                    //$this->getDefaultAdapter()->fetchRow("SELECT * FROM sp_insert_idiomas_desc('{$data['des_id']}','{$data['des_contenido']}')");
                    $idiomDescObj->createNewRow(array('des_id'=>$data['des_id'],'id_id'=>$sesion->lg,'des_contenido'=>$data['des_contenido']));
                    break;
                case 'edit':
                    $this->update(array('des_estado'=>$data[des_estado],'des_nombre'=>$data['des_nombre']), "des_id = '{$data['des_cod']}'");
                    $idiomDescObj->update(array('des_contenido'=>$data['des_contenido']), "des_id = '{$data['des_cod']}' and id_id = '{$sesion->lg}'");
                    break;
            endswitch;
            $this->_db->commit();
            return 1;
        } catch ( Zend_Exception $e ) {
            $this->_db->rollBack ();
            //return 0;
            throw new Zend_Db_Statement_Exception ( $e->getMessage () );
        }
    }

}