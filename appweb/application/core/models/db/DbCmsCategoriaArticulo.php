<?php

/**
 * CmsCategoriaArticulo Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbCmsCategoriaArticulo extends Cit_Db_Table_Abstract {

    protected $_name = 'cms_categoria_articulo';
    protected $_primary = array(
        'cat_id',
        'art_id',
        'su_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Devolver Datos Categoria
     *
     * @param  $data string
     * @param $where string SQL WHERE clause(s),
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($data, $where = '', $tipo = 'T') {
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from($this->_name, array(new Zend_Db_Expr($data)));
        $select->where($where);
        //echo $select; exit;
        $dtaAnio = $this->fetchAll($select)->toArray();
        if (!empty($dtaAnio[0]))
            $array = $dtaAnio[0];
        else
            $array = array();

        if ($tipo == 'T')
            return $array;
        else
            return $array[0];
    }

   
    public function getDataId() {
        $select = $this->select();
        if (!empty($data['where'])) {
            $select->where("");
        }
        if (!empty($data['order'])) {
            $select->order($data['order']);
        }
        return $this->fetchAll($select)->toArray();
    }

    public function asignArticulo($data) {
        //var_dump($data);exit;
        $dbIdiomaArtCont = new DbHtCmsIdiomaArtCont();
        $select = $this->select()->setIntegrityCheck(false);

        $this->_db->beginTransaction();
        try {
            $art_id = explode('**', $data['artids']);
            $dataCatArt['cat_id'] = $data['cat_id'];
            $dataCatArt['su_id'] = $data['su_id'];
            $select->from(array('t1' => 'ht_cms_idioma'), array('id_id'));
            $dataIdioma = $select->query()->fetchAll();
            foreach ($art_id as $value) {
                $dataCatArt['art_id'] = $value;
                $this->createNewRow($dataCatArt);
                foreach ($dataIdioma as $val) {
                    $dbIdiomaArtCont->createNewRow(array('art_id' => $value, 'id_id' => $val['id_id'],
                        'su_id' => $data['su_id'], 'cat_id' => $data['cat_id']));
                }
            }
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
        return false;
    }

    public function deleteData($data) {
        //var_dump($data);exit;
        $dbIdiomaArtCont = new DbHtCmsIdiomaArtCont();
        $dbImagenesArticulo = new DbCmsImagenesArticulo();
        $dbContenidoArticulo = new DbHtCmsIdiomaArtCont();
        $this->_db->beginTransaction();
        try {
            $this->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}' and art_id in({$data['artids']})");
            $dbIdiomaArtCont->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}' and art_id in({$data['artids']})");
            $dbImagenesArticulo->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}' and art_id in({$data['artids']})");
            $dbContenidoArticulo->delete("cat_id = '{$data['cat_id']}' and su_id = '{$data['su_id']}' and art_id in({$data['artids']})");
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            return 2;
            //throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    public function updateOrden($data) {
        //var_dump($data);exit;
        $this->_db->beginTransaction();
        try {
            $row = explode('**', $data['regmod']);
            foreach ($row as $value) {
                $column = explode(',,', $value);
                foreach ($column as $value1) {
                    $field = explode('::', $value1);
                    switch ($field[0]) {
                        case 'art_id':
                            $art_id = $field[1];
                            break;
                        case 'art_orden':
                            $dataUpdate['art_orden'] = $field[1];
                            break;
                        case 'art_link':
                            $dataUpdate['art_link'] = $field[1];
                            break;
                    }
                }
                $where = "su_id = '{$data['su_id']}' and cat_id = '{$data['cat_id']}' and art_id = '$art_id'";
                $this->update($dataUpdate, $where);
            }
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            //return 2;
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

}
