<?php

/**
 * HbAccesorioTipoHabitacio Db_Table_Abstract
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class DbHtHbAccesorioSucursalTiphab extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_accesorio_sucursal_tiphab';
    protected $_primary = array(
        'tih_id',
        'ac_id', 'su_id'
    );
    protected $_dependentTables = array();
    protected $_referenceMap = array();

    /**
     * Guardar un registro
     *
     * @param  $data array [registro que se guardan]
     * @return [0,1,2]
     */
    public function saveData(array $data = Array()) {
        $citObj = new CitDataGral();
        $this->_db->beginTransaction();
        try {
            $sesion = new Zend_Session_Namespace('login');
            $save['tih_id'] = $data['tih_id'];
            $save['su_id'] = $data['su_id'];
            foreach ($data['grid'] as $value):
                $save['ac_id'] = $value;
                $this->createNewRow($save);
            endforeach;
            $this->_db->commit();
            return 1;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Asigancion-accesrio-tipo-hab
     * Elimina las accesorios por tipo de habitacion
     *
     * @param  $data string
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function deleteData(array $data = Array()) {
        $sesion = new Zend_Session_Namespace('login');
        $citObj = new CitDataGral();
        $resObj = new DbHtResReserva();
        $this->_db->beginTransaction();
        try {
            $i = 0;
            $cont = 0;
            foreach ($data['grid'] as $value):
                //var_dump("ac_id = '$value' and su_id = '{$sesion->suid}' and tih_id = '{$data['tih_id']}'");exit;
                $this->delete(array(
                    'ac_id =? ' => $value,
                    'su_id =? ' => $data['su_id'],
                    'tih_id =?' => $data['tih_id']
                ));
                //$this->delete("ac_id = '$value' and su_id = '{$data['su_id']}' and tih_id = '{$data['tih_id']}'");
                $i++; $cont++;
            endforeach;


            if ($i == $cont):
                $array['id'] = 1;
                $array['desc'] = 'Eliminación completa de los accesorios seleccionados';
            else:
                if ($i == 0) {
                    $array['id'] = 2;
                    $array['desc'] = 'Todos tienes datos relacionados';
                } else {
                    $array['id'] = 1;
                    $array['desc'] = 'Eliminación parcial, existen datos relacionados';
                }
            endif;
            $this->_db->commit();
            return $array;
        } catch (Zend_Exception $e) {
            $this->_db->rollBack();
            throw new Zend_Db_Statement_Exception($e->getMessage());
        }
    }

    /** Mantenimiento Habitación
     * Muestra las accesorios por tipo de habitacion
     *
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @return array de la consulta
     */
    public function getAccesTipohab($where = '') {
        $sesion = new Zend_Session_Namespace('login');
        $select = $this->select()->setIntegrityCheck(false); //ht_hb_accesorio_tipo_habitacion
        $select->from(array('t1' => 'ht_hb_accesorio_sucursal_tiphab'), array('lleva' => 't1.ac_id'));
        $select->join(array('t2' => 'vht_hb_accesorios'), 't1.ac_id = t2.ac_id', array('t2.ac_id', 't2.ac_desc', 't2.ac_precio'));
        $select->where("id_id = '{$sesion->lg}'");
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        if (!empty($order))
            $select->order($order);
        //echo $select; exit;
        $dtaHabit = $this->fetchAll($select)->toArray();
        return $dtaHabit;
    }

//selTipohabxSucursal
}
