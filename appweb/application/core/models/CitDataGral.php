<?php

class CitDataGral extends Cit_Db_Table_Abstract {

    protected $_name = 'ht_hb_habitacion';
    protected $_primary = array('hb_id');
    public $_menuObj;

    /**
     * Devolver Datos
     *
     * @param  $table string nombre de la tabla
     * @param  $data array, campos a mostrar
     * @param $where string|array [SQL WHERE clause(s),order,limit,etc],
     * @param $tipo string se quiere solo un dato (U) o varios (T).
     * @return array de la consulta
     */
    public function getDataGral($table, $data, $where = '', $tipo = 'T') {
        $login = new Zend_Session_Namespace('login');
        //var_dump($data);
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('t1' => $table), $data);
        if (is_array($where)) {
            if (!empty($where['where']))
                $select->where($where['where']);
            if (!empty($where['order']))
                $select->order($where['order']);
        }else {
            if (!empty($where))
                $select->where($where);
        }
        /* if($tipo == 'A'){
          echo 'ok             --'.$select; exit;} */
        //echo $select;exit;
        if ($tipo == 'T'):
            $array = $this->fetchAll($select)->toArray();
        else:
            $array = $this->fetchRow($select);
        endif;

        return $array;
    }

    /**
     * Devolver Numero Correlitavo +1
     *
     * @param $table string nombre de la tabla
     * @param $data array
     * @param $cont contador de la cadena.
     * @param $tipo U=>para devolver solo el dato else array
     * @return array Id nuevo 
     */
    public function returnCorrelativo($table, $data, $cont, $tipo = 'T') {
        $nombre = '';
        foreach ($data as $item => $value):
            $nombre = $value;
        endforeach;

        $dtaTable = $this->getDataGral($table, $data, '', 'U');
        //var_dump($nombre); exit;
        foreach ($dtaTable as $item => $value):

            if (empty($value)) {
                $codigo[$item] = str_pad(0, $cont, '0', STR_PAD_LEFT);
            }else
                $codigo[$item] = $value;
        endforeach;

        $temp = (int) $codigo[$item];
        $temp++;
        $codigo[$item] = str_pad($temp, $cont, '0', STR_PAD_LEFT);
        if ($tipo == 'U'):
            if (!empty($codigo[0])) {
                return array($nombre => $codigo[0]);
            } else {
                return $codigo; //'id'=>num
            }

        else:
            return array($codigo); //pasa los json
        endif;
    }

    /**
     * Devolver los meses activos
     *
     * @param $meses array, los meses completo
     * @return array actual hacia delante
     */
    public function returnMesesActivos(array $meses = Array()) {
        $mes_actual = date('m');
        $fin = (int) $mes_actual;
        for ($i = 1; $i < $fin; $i++):
            $id = str_pad($i, 2, '0', STR_PAD_LEFT);
            unset($meses[$id]);
        endfor;

        return $meses;
    }

    /**
     * Devolver los meses activos
     *
     * @param $meses array, los meses completo
     * @return array actual hacia delante
     */
    public function returnMesesActivosBi(array $meses = Array()) {
        $mes_actual = date('m');
        $fin = (int) $mes_actual;
        for ($i = 1; $i < $fin; $i++):
            $id = str_pad($i, 2, '0', STR_PAD_LEFT);
            unset($meses[$id]);
        endfor;

        return $meses;
    }

    public function returnArrayCombo($data, $campo = array()) {
        if (empty($campo['desc']))
            $campo['desc'] = 'state';
        $array = array();
        foreach ($data as $item => $value):
            $array[] = array($campo['id'] => $item, $campo['desc'] => $value);
        endforeach;

        return $array;
    }

}

