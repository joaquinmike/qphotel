<?php

 /**
 * Form ec_class_product
 * @author: Eliezer Benjamin Gonzales Basilio (egonzales@ispgraf.com)
 * @Copyright (c) 2010 ISPGRAF.SAC - http://www.ispgraf.com
 * @version:
 *  **/

class FormAlumnoTest extends Zend_Dojo_Form
{

/**
	* Inicializa el formulario
	*
*/
	public function init()
{
$this->setLegend('Registro de ec_class_product');
$this->setName('frm_ec_class_product');
$this->setAction('');
$this->setMethod('post');
$this->addSubForm($this->elements(), 'date');


$this->setConfig(Cit_Init::config()->dojoForm);
$this->getView()->formButtons()->setId($this->getName());
$this->getView()->formButtons()->show();
$this->addDecorators(array(array('TabContainer', array(  'style'       => 'width: 500px;
 height: 260px;
 margin-left:auto;
margin-right:auto;
',))));
}
/**
	* Retorna un SubFormulario
	* @return Zend_Dojo_Form_SubForm
	*
	*/
	 private function elements()
{
$subForm = new Zend_Dojo_Form_SubForm();
$subForm->setAttribs(array(
'name'   => 'textboxtab',
'legend' => 'General',
'dijitParams' => array(
'title' => 'General',
)
,));

//clp_id
$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMDES',
array(
	'label' 	 => Cit_Init::_('FAMDES').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMTEL',
array(
	'label' 	 => Cit_Init::_('FAMTEL').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


$subForm->addElement(
'ValidationTextBox',
'FAMCOD',
array(
	'label' 	 => Cit_Init::_('FAMCOD').' :',
'maxlength' => 12,
'ReadOnly'   => 'ReadOnly',
'validators'   => array('Int'),
));


//ec__clp_id
/*$clp=new EcClassProductLang();
$result=$clp->fetchPairs(array('clp_id','clpl_name'),'','clpl_name');
$subForm->addElement(
		'FilteringSelect',
		'ec_clp_id',
		array (
			'required' => true,
			'label' => Cit_Init::_ ( 'Categoria Sup.' ) . ' :',
			'MultiOptions'=>$result
		)
		);*/


//$this->langElements($subForm);
//clp_status
$subForm->addElement(
		'FilteringSelect',
		'clp_status',
		array (
			'required' => true,
			'label' => Cit_Init::_ ( 'Estado' ) . ' :',
			'MultiOptions'=>array('1'=>'Activo','0'=>'Inactivo')
		)
		);

$subForm->setConfig(Cit_Init::config()->dojoSubForm);

 return $subForm;

}
private function langElements($form){
	$ecclp = new EcLang();
	$result=$ecclp->fetchAll()->toArray();
	foreach($result as $value){
		$subForm = new Zend_Dojo_Form_SubForm();
		$subForm->setAttribs(array(
	        	'name' => 'class'.$value['lg_id'],
			));
		//clp_id
		$subForm->addElement(
		'ValidationTextBox',
		'clpl_name',
		array(
			'label' 	 => Cit_Init::_('Nombre-'.$value['lg_id']).' :',
		'maxlength' => 12,
		//'validators'   => array('Int'),
		));
		$subForm->addElement(
	        	'hidden',
	        	'lg_id',
			array(
	                'value'=>$value['lg_id']
			)
			);
			$subForm->addElement(
	        	'hidden',
	        	'clpl_id'
	        	);
	       $subForm->setConfig(Cit_Init::config()->dojoSubForm);
		$form->addSubForm($subForm,'lang'.$value['lg_id']);
	}
}
}
