<?php
/**
 * Alumno model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class FormColegio extends Cit_Forms_Form
{
public function  __construct(){
		$this->setAction('');
		$this->setMethod('post');
		$this->setObject(array('doc'));
		$this->setAtribs(array(
	    'renderTo' 		=> 'script',
	   
	    'autoHeight' 	=> true,
	    'width'   		=> '100%',    
	    'bodyStyle' 	=> 'padding: 5px',
	    'defaults' 		=> array('anchor'=> 0)));
		$sub = new Cit_Forms_SubForm();		
		$sub->addElement(new Cit_Forms_Element_Text('codeis',
						array('xtype'=> 'fieldset',
			            'title'=> 'Informacion Basica',
			            'collapsible'=> true,
			            'items'=>array(
					         array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Abreviado',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_abrev',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'RUC'
			                       ),
			                       array(
			                           'name '=> 'co_ruc',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       ))),
		                        //para apellidos y  nombres 
	                        	array(
	                        	
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Nombre',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_nombre',
			                           'xtype'=> 'textfield',
			                           'width'=> 630,
			                           'allowBlank'=> false
			                       ))),
			                       
			                       
			                     //Departamento
			                     array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Dpto',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
                    'xtype' => 'compositefield',
                    'anchor'=> '-20',
                    'msgTarget'=> 'side',
                    'fieldLabel'=> 'Full Name',
                    'items' => array(array(
							'width'=>130,
                            'xtype'=>          'combo',
                            'mode'=>           'local',
                            
                            'triggerAction'=>  'all',
                            'forceSelection'=> true,
                            'editable'=>       false,
                            'fieldLabel'=>     'Title',
                            'name'=>           'ub_id_dep',
                            'hiddenName'=>     'ub_id_dep',
                            'displayField'=>   'name',
                            'valueField'=>     'value',
			                'emptyText'=>'Departamento',
                            'store'=>        'doc',
			                 'listeners'=>'selectdpto'),
			                       //Combo Provincia
			                       
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Provincia:',
			                       		'width'=> 50
			                       ),
			                       array(
                    'xtype' => 'compositefield',
                    'anchor'=> '-20',
                    'msgTarget'=> 'side',
                    'fieldLabel'=> 'Full Name',
                    'items' => array(array(
							'width'=>130,
                            'xtype'=>          'combo',
                            'mode'=>           'local',
                            
                            'triggerAction'=>  'all',
                            'forceSelection'=> true,
                            'editable'=>       true,
                            'fieldLabel'=>     'Title',
                            'name'=>           'ub_id_prov',
                            'hiddenName'=>     'ub_id_prov',
                            'displayField'=>   'name',
                            'valueField'=>     'value',
			                'emptyText'=>'Provincia',
			                'store'=>'jsonprovincia',
			                'listeners'=>'selectprov'
                            ),
			                       //Combo Distrito
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Distrito',
			                       		'width'=> 50
			                       ),
			                       array(
                    'xtype' => 'compositefield',
                    'anchor'=> '-20',
                    'msgTarget'=> 'side',
                    'fieldLabel'=> 'Full Name',
                    'items' => array(array(
							'width'=>130,
                            'xtype'=>          'combo',
                            'mode'=>           'local',
                            
                            'triggerAction'=>  'all',
                            'forceSelection'=> true,
                            'editable'=>       false,
                            'fieldLabel'=>     'Title',
                            'name'=>           'ub_id',
                            'hiddenName'=>     'ub_id',
                            'displayField'=>   'name',
                            'valueField'=>     'value',
							'emptyText'=>'Distrito',
			                'store'=>'jsondistrito'
			                       )))
			                       ))
			                       ))                       			
			                       )),
			                       //cod educando  
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Direccion',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_direccion',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'C. Poblado'
			                       ),
			                       array(
			                           'name '=> 'col_poblado',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Lugar',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'col_lugar',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Telefono'
			                       ),
			                       array(
			                           'name '=> 'cos_telefono1',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Fax'
			                       ),
			                       
			                       array(
			                           'name '=> 'cos_fax',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Administrad',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_administrador',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Resp.Acta',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_resacta',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Director',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_director',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Sub Director',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_subdirector',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Pagina Web',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_pagweb',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       )),
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Email',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'cos_correo',
			                           'xtype'=> 'textfield',
			                           'width'=> 430			                           
			                       )
			                       ))
			                       //end 	                    	
	                        	))					                    	
		                   ));
						
		$this->addSubform($sub,'Principal');
	}
 	public function __toString()
    {
        return (string)  $this->reder();
    }
}