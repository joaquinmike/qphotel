<?php
/**
 * Alumno model
 * 
 * @Category Cit
 * @Author Information Technology Community
 * @Copyright   (c) 2010 CIT.SAC - http://www.cit.pe
 * @Version V. 1.0
 */
class FormAlumno extends Cit_Forms_Form
{
public function  __construct(){
		$this->setAction('');
		$this->setMethod('post');
		$this->setObject(array('doc','selectdpto','jsonprovincia','selectprov','jsondistrito'));
		$this->setAtribs(array(
	    'renderTo' 		=> 'script',
	   
	    'autoHeight' 	=> true,
	    'width'   		=> '100%',    
	    'bodyStyle' 	=> 'padding: 5px',
	    'defaults' 		=> array('anchor'=> 0)));
		$sub = new Cit_Forms_SubForm();		
		$sub->addElement(new Cit_Forms_Element_Text('codeis',
						array('xtype'=> 'fieldset',
			            'title'=> 'Informacion Basica',
			            'collapsible'=> true,
			            'items'=>array(
					         array(
		                    'xtype' => 'compositefield',
		                    'anchor'=> '-20',
		                    'msgTarget'=> 'side',
		                    'fieldLabel'=> 'Codigo',
		                    'items' => array(
		                        array(
		                            'xtype'=> 'textfield',
		                            'flex' => 1,
		                            'name' => 'al_id',
		                            'fieldLabel'=> 'First',
		                            'allowBlank'=> false,
		                        	'width'=> 130
		                        ),
		                        array('xtype'=> 'displayfield', 'value'=> 'Código Anterior:'),
		                        array(
		                            'xtype'=> 'textfield',
		                            'flex' => 1,
		                            'name' => 'al_id_anterior',
		                            'fieldLabel'=> 'First',
		                            'allowBlank'=> false,
		                        	'width'=> 130
		                        ),
		                        	array('xtype'=> 'displayfield', 'value'=> ' (Colegio Antiguo.) ')
		                        	)),
		                        	//para apellidos y  nombres 
	                        	array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Apellido Paterno',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'pe_apmat',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           'allowBlank'=> false
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Apellido Materno'
			                       ),
			                       array(
			                           'name '=> 'pe_apmat',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           'allowBlank'=> false
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Nombres'
			                       ),
			                        array(
			                           'name '=> 'pe_nombre',
			                           'xtype'=> 'numberfield',
			                           'width'=> 130,
			                           'allowBlank'=> false
			                       )
			                       )),	
			                       //end  apellidos y  nombres 
			                       //cod educando  
			                       array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Cod. Educando',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'al_codeducando',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Sexo:',
			                       		'width'=> 90
			                       ),
			                       array(
			                           'name '=> 'pe_sexo',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Tip Doc.',
			                       		'width'=> 50
			                       ),
			                        array(
			                           'name '=> 'pe_tipdoc',
			                           'xtype'=> 'numberfield',
			                           'width'=> 130
			                          
			                       )
			                       )),
			                       //end 	
								//num doc
								array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Num. Doc',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'pe_numdoc',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Fec. Nac.:',
			                       		'width'=> 90
			                       ),
			                       array(
			                           'name '=> 'pe_fecnac',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Nac. Registra:',
			                       		'width'=> 50
			                       ),
			                        array(
			                           'name '=> 'pe_nacregistra',
			                           'xtype'=> 'numberfield',
			                           'width'=> 130
			                          
			                       )
			                       )),
			                      //Pais
								array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Pais',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(
			                           'name' => 'pe_pais',
			                           'xtype'=> 'textfield',
			                           'width'=> 130			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Nacionalidad:',
			                       		'width'=> 90
			                       ),
			                       array(
			                           'name '=> 'pe_nacionalidad',
			                           'xtype'=> 'textfield',
			                           'width'=> 130,
			                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Dpto. Nac.:',
			                       		'width'=> 50
			                       ),
			        		array(
							'width'=>130,
                            'xtype'=>          'combo',
                            'mode'=>           'local',
			        		
                            'triggerAction'=>  'all',
                            'forceSelection'=> true,
                            'editable'=>       false,
                            'fieldLabel'=>     'Title',
                            'name'=>           'ub_id_dep',
                            'hiddenName'=>     'ub_id_dep',
                            'displayField'=>   'name',
                            'valueField'=>     'value',
			        		'emptyText'=>'Departamento',
                            'store'=>        'doc',
			        		'listeners'=>'selectdpto')                        			
			                       )),

			                       //Provincia
								array(
			                    'xtype'=> 'compositefield',
			                    'fieldLabel'=> 'Prov. Nac',
			                    'combineErrors'=> false,
			                    'items'=> array(
			                       array(		                       
			                       'width'=>130,
		                            'xtype'=>          'combo',
		                            'mode'=>           'local',
					        		                           
		                            'triggerAction'=>  'all',
		                            'forceSelection'=> true,
		                            'editable'=>       false,
		                            'fieldLabel'=>     'Title',
		                            'name'=>           'ub_id_prov',
		                            'hiddenName'=>     'ub_id_prov',
		                            'displayField'=>   'name',
		                            'valueField'=>     'value',
			                       'emptyText'=>'Provincia',
		                            'store'=>        'jsonprovincia',
			                       'listeners'=>'selectprov'		                           
			                       ),
			                       array(
			                           'xtype'=> 'displayfield',
			                           'value'=> 'Dist. Nac:',
			                       		'width'=> 90
			                       ),
			                        array(		                       
			                       'width'=>130,
		                            'xtype'=>          'combo',
		                            'mode'=>           'local',
					        		                            
		                            'triggerAction'=>  'all',
		                            'forceSelection'=> true,
		                            'editable'=>       false,
		                            'fieldLabel'=>     'Title',
		                            'name'=>           'ub_id',
		                            'hiddenName'=>     'ub_id',
		                            'displayField'=>   'name',
		                            'valueField'=>     'value',
			                        'emptyText'=>'Distrito',
		                            'store'=>        'jsondistrito'		                           
			                       ),
			                       
			                       ))
	                        	))					                    	
		                   ));
		
		$sub->addElement(new Cit_Forms_Element_Text('email',
						array('xtype' => 'textfield',				            
				            'fieldLabel'=> 'Email Address',
				            'anchor'    => '-20')));
		
		
						
		$sub->addElement(new Cit_Forms_Element_Text('code',
						array('xtype' => 'textfield',				            
				            'fieldLabel'=> 'Email Address',
				            'anchor'    => '-20')));
						
		$sub->addElement(new Cit_Forms_Element_Text('nombre',
						array(						
						'xtype'=> 'compositefield',
			            'fieldLabel'=> 'Date Range',
			            'msgTarget' => 'side',
			            'anchor' => '-20',
			            'defaults'=>array(
			                'flex'=> 1
			            ),
			            'items'=>array(
			            array(
			                    'xtype'=>'datefield',
			                    'name'=> 'dateone'
			                ),
			            array(
			                    'xtype'=> 'datefield',
			                    'name'=> 'datetwo'
			                )))));	                
	 
						
		$this->addSubform($sub,'Principal');
	}
 	public function __toString()
    {
        return (string)  $this->reder();
    }
}