--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.1
-- Dumped by pg_dump version 9.1.1
-- Started on 2013-03-09 23:27:36

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 260 (class 3079 OID 11638)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 260
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 272 (class 1255 OID 24591)
-- Dependencies: 798 5
-- Name: sp_reserva_inverntario_rooms(character varying, character varying, date, date, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION sp_reserva_inverntario_rooms(canid character varying, csuid character varying, cfecini date, cfecfin date, cidioma character varying) RETURNS SETOF record
    LANGUAGE plpgsql
    AS $$

BEGIN
   
    return query SELECT t1.ka_fecha, t2.hb_precio, t1.ta_id,
    (select hb_precio - pro_dscto*hb_precio/100 from vht_cms_promo_sucursal p1 
        where ka_fecha BETWEEN pro_fecini and pro_fecfin and p1.tih_id = t1.tih_id and 
        t1.su_id = p1.su_id and p1.an_id = Canid and pro_estado = 'S'
    ) as promo, t1.tih_id, tih_abr
    FROM ht_res_kardex AS t1
    INNER JOIN vht_hb_tipo_habitacion AS t6 ON t1.tih_id = t6.tih_id and t6.id_id = Cidioma
    INNER JOIN ht_hb_habitacion_tarifa AS t2 ON t1.su_id = t2.su_id and t1.tih_id = t2.tih_id and t1.ta_id = t2.ta_id
    INNER JOIN vht_hb_tarifa AS t5 ON t1.ta_id = t5.ta_id and t5.id_id = t6.id_id
    WHERE (t1.su_id = Csuid) AND
        (t1.ka_fecha BETWEEN Cfecini and Cfecfin) AND
        (t1.ka_situacion = '1') AND (t2.hb_capacidad = '1')
    GROUP BY ka_fecha,
        hb_precio,
        t1.su_id,
        t1.tih_id,
        tih_abr,
        t1.ta_id
    ORDER BY ka_fecha,t1.tih_id ASC;

END 
$$;


ALTER FUNCTION public.sp_reserva_inverntario_rooms(canid character varying, csuid character varying, cfecini date, cfecfin date, cidioma character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 24592)
-- Dependencies: 5
-- Name: cms_categoria_articulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_categoria_articulo (
    cat_id character(8) NOT NULL,
    art_id character(8) NOT NULL,
    su_id character varying(8) NOT NULL,
    art_orden integer,
    art_link character varying(256)
);


ALTER TABLE public.cms_categoria_articulo OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 24595)
-- Dependencies: 5
-- Name: cms_imagenes_articulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cms_imagenes_articulo (
    im_id character varying(8) NOT NULL,
    art_id character(8) NOT NULL,
    su_id character varying(8) NOT NULL,
    cat_id character(8) NOT NULL,
    im_orden integer,
    im_link character varying(256)
);


ALTER TABLE public.cms_imagenes_articulo OWNER TO postgres;

--
-- TOC entry 163 (class 1259 OID 24598)
-- Dependencies: 5
-- Name: ht_cms_articulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_articulo (
    art_id character(8) NOT NULL,
    art_est integer
);


ALTER TABLE public.ht_cms_articulo OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 24601)
-- Dependencies: 5
-- Name: ht_cms_categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_categoria (
    cat_id character(8) NOT NULL,
    cat_div character varying(100),
    cat_tipo integer,
    cat_estado integer,
    ho_id character varying(2),
    cat_public character(1)
);


ALTER TABLE public.ht_cms_categoria OWNER TO postgres;

--
-- TOC entry 165 (class 1259 OID 24604)
-- Dependencies: 5
-- Name: ht_cms_descripciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_descripciones (
    des_id character varying(6) NOT NULL,
    des_estado character(1),
    des_nombre character varying(64)
);


ALTER TABLE public.ht_cms_descripciones OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 24607)
-- Dependencies: 5
-- Name: ht_cms_idioma; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma (
    id_id character varying(2) NOT NULL,
    id_desc character varying(32),
    id_estado character(1)
);


ALTER TABLE public.ht_cms_idioma OWNER TO postgres;

--
-- TOC entry 167 (class 1259 OID 24610)
-- Dependencies: 5
-- Name: ht_cms_idioma_art_cont; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_art_cont (
    art_id character(8) NOT NULL,
    id_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    cat_id character(8) NOT NULL,
    art_contenido text
);


ALTER TABLE public.ht_cms_idioma_art_cont OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 24616)
-- Dependencies: 5
-- Name: ht_cms_idioma_articulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_articulo (
    id_id character varying(2) NOT NULL,
    art_id character(8) NOT NULL,
    art_titulo character varying(100)
);


ALTER TABLE public.ht_cms_idioma_articulo OWNER TO postgres;

--
-- TOC entry 169 (class 1259 OID 24619)
-- Dependencies: 5
-- Name: ht_cms_idioma_categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_categoria (
    id_id character varying(2) NOT NULL,
    cat_id character(8) NOT NULL,
    cat_titulo character varying(128)
);


ALTER TABLE public.ht_cms_idioma_categoria OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 24622)
-- Dependencies: 5
-- Name: ht_cms_idioma_desc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_desc (
    id_id character varying(2) NOT NULL,
    des_id character varying(6) NOT NULL,
    des_contenido text
);


ALTER TABLE public.ht_cms_idioma_desc OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 24628)
-- Dependencies: 5
-- Name: ht_cms_idioma_menu_interno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_menu_interno (
    cat_id character(1) NOT NULL,
    id_id character varying(2) NOT NULL,
    cat_desc text,
    cat_titulo character varying(64)
);


ALTER TABLE public.ht_cms_idioma_menu_interno OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 24634)
-- Dependencies: 5
-- Name: ht_cms_idioma_mes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_mes (
    id_id character varying(2) NOT NULL,
    mes_id character(2) NOT NULL,
    mes_desc character varying(64),
    mes_abr character varying(16)
);


ALTER TABLE public.ht_cms_idioma_mes OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 24637)
-- Dependencies: 5
-- Name: ht_cms_idioma_promocion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_promocion (
    pro_id character varying(8) NOT NULL,
    an_id character(4) NOT NULL,
    id_id character varying(2) NOT NULL,
    pro_titulo character varying(256),
    pro_desc text
);


ALTER TABLE public.ht_cms_idioma_promocion OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 24643)
-- Dependencies: 5
-- Name: ht_cms_idioma_promsucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_promsucursal (
    id_id character varying(2) NOT NULL,
    pro_id character varying(8) NOT NULL,
    an_id character(4) NOT NULL,
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    pro_desc text
);


ALTER TABLE public.ht_cms_idioma_promsucursal OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 24649)
-- Dependencies: 5
-- Name: ht_cms_idioma_public; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_idioma_public (
    pub_id character(10) NOT NULL,
    id_id character varying(2) NOT NULL,
    pub_titulo character varying(200),
    pub_contenido text
);


ALTER TABLE public.ht_cms_idioma_public OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 24655)
-- Dependencies: 5
-- Name: ht_cms_imagenes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_imagenes (
    im_id character varying(8) NOT NULL,
    im_image1 character varying(128),
    im_image2 character varying(128),
    im_image3 character varying(128),
    im_image4 character varying(128),
    im_image5 character varying(128),
    im_estado integer
);


ALTER TABLE public.ht_cms_imagenes OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 24661)
-- Dependencies: 5
-- Name: ht_cms_menu_interno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_menu_interno (
    cat_id character(1) NOT NULL,
    cat_orden integer
);


ALTER TABLE public.ht_cms_menu_interno OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 24664)
-- Dependencies: 5
-- Name: ht_cms_meses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_meses (
    mes_id character(2) NOT NULL
);


ALTER TABLE public.ht_cms_meses OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 24667)
-- Dependencies: 5
-- Name: ht_cms_promo_imagen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_promo_imagen (
    pro_id character varying(8) NOT NULL,
    an_id character(4) NOT NULL,
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    im_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_cms_promo_imagen OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 24670)
-- Dependencies: 2182 2183 2184 5
-- Name: ht_cms_promocion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_promocion (
    an_id character(4) DEFAULT date_part('year'::text, now()) NOT NULL,
    pro_id character varying(8) NOT NULL,
    ho_id character varying(2) NOT NULL,
    tih_id character varying(2),
    pro_fecalta date,
    pro_fecini date,
    pro_fecfin date,
    pro_dscto numeric,
    pro_estado character(1) DEFAULT 'S'::bpchar,
    CONSTRAINT ckc_pro_estado_ht_cms_p CHECK (((pro_estado IS NULL) OR (pro_estado = ANY (ARRAY['S'::bpchar, 'N'::bpchar]))))
);


ALTER TABLE public.ht_cms_promocion OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 24679)
-- Dependencies: 5
-- Name: ht_cms_promocion_sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_promocion_sucursal (
    pro_id character varying(8) NOT NULL,
    an_id character(4) NOT NULL,
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_cms_promocion_sucursal OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 24682)
-- Dependencies: 5
-- Name: ht_cms_public_imagen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_public_imagen (
    pub_id character(10) NOT NULL,
    im_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_cms_public_imagen OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 24685)
-- Dependencies: 5
-- Name: ht_cms_publicacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_publicacion (
    pub_id character(10) NOT NULL,
    pub_estado character(1),
    pub_tipo character(1),
    pub_fecha date
);


ALTER TABLE public.ht_cms_publicacion OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 24688)
-- Dependencies: 5
-- Name: ht_cms_sucursal_categoria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_sucursal_categoria (
    su_id character varying(8) NOT NULL,
    cat_id character(8) NOT NULL,
    cat_orden integer
);


ALTER TABLE public.ht_cms_sucursal_categoria OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 24691)
-- Dependencies: 5
-- Name: ht_cms_sucursal_menu_interno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_cms_sucursal_menu_interno (
    cat_id character(1) NOT NULL,
    su_id character varying(8) NOT NULL,
    tih_id character varying(2) NOT NULL,
    cat_orden smallint
);


ALTER TABLE public.ht_cms_sucursal_menu_interno OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 24694)
-- Dependencies: 5
-- Name: ht_hb_accesorio_sucursal_tiphab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_accesorio_sucursal_tiphab (
    ac_id character varying(4) NOT NULL,
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_hb_accesorio_sucursal_tiphab OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 24697)
-- Dependencies: 5
-- Name: ht_hb_accesorios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_accesorios (
    ac_id character varying(4) NOT NULL,
    ac_estado integer,
    ac_precio numeric(10,2),
    ac_tipo integer
);


ALTER TABLE public.ht_hb_accesorios OWNER TO postgres;

--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN ht_hb_accesorios.ac_tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ht_hb_accesorios.ac_tipo IS 'el tipo 1 son para el hotel
el tipo2 es para la resrva';


--
-- TOC entry 188 (class 1259 OID 24700)
-- Dependencies: 5
-- Name: ht_hb_habitacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_habitacion (
    hb_id character varying(8) NOT NULL,
    tih_id character varying(2),
    su_id character varying(8),
    pi_id character(4),
    hb_numero integer,
    hb_precio numeric(10,2),
    hb_estado character(1)
);


ALTER TABLE public.ht_hb_habitacion OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 24703)
-- Dependencies: 5
-- Name: ht_hb_habitacion_accesorios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_habitacion_accesorios (
    hb_id character varying(8) NOT NULL,
    ac_id character varying(4) NOT NULL,
    hba_id character(8) NOT NULL
);


ALTER TABLE public.ht_hb_habitacion_accesorios OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 24706)
-- Dependencies: 5
-- Name: ht_hb_habitacion_imagen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_habitacion_imagen (
    hb_id character varying(8) NOT NULL,
    im_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_hb_habitacion_imagen OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 24709)
-- Dependencies: 5
-- Name: ht_hb_habitacion_tarifa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_habitacion_tarifa (
    su_id character varying(8) NOT NULL,
    tih_id character varying(2) NOT NULL,
    hb_capacidad integer NOT NULL,
    ta_id character(1) NOT NULL,
    hb_precio numeric(10,2)
);


ALTER TABLE public.ht_hb_habitacion_tarifa OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 24712)
-- Dependencies: 5
-- Name: ht_hb_hotel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_hotel (
    ho_id character varying(2) NOT NULL,
    ho_ruc character varying(16),
    ho_estado integer,
    ho_nombre character varying(30)
);


ALTER TABLE public.ht_hb_hotel OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 24715)
-- Dependencies: 5
-- Name: ht_hb_idioma_accesorios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_accesorios (
    id_id character varying(2) NOT NULL,
    ac_id character varying(4) NOT NULL,
    ac_desc character varying(128)
);


ALTER TABLE public.ht_hb_idioma_accesorios OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 24718)
-- Dependencies: 5
-- Name: ht_hb_idioma_habitacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_habitacion (
    hb_id character varying(8) NOT NULL,
    id_id character varying(2) NOT NULL,
    hb_desc text
);


ALTER TABLE public.ht_hb_idioma_habitacion OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 24724)
-- Dependencies: 5
-- Name: ht_hb_idioma_piso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_piso (
    pi_id character(4) NOT NULL,
    id_id character varying(2) NOT NULL,
    pi_desc character varying(32)
);


ALTER TABLE public.ht_hb_idioma_piso OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24727)
-- Dependencies: 5
-- Name: ht_hb_idioma_suc_tiphab; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_suc_tiphab (
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    id_id character varying(2) NOT NULL,
    tih_desc text
);


ALTER TABLE public.ht_hb_idioma_suc_tiphab OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 24733)
-- Dependencies: 5
-- Name: ht_hb_idioma_tarifa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_tarifa (
    ta_id character(1) NOT NULL,
    id_id character varying(2) NOT NULL,
    ta_desc character varying(100)
);


ALTER TABLE public.ht_hb_idioma_tarifa OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24736)
-- Dependencies: 5
-- Name: ht_hb_idioma_tipo_habitacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_idioma_tipo_habitacion (
    tih_id character varying(2) NOT NULL,
    id_id character varying(2) NOT NULL,
    tih_desc character varying(64),
    tih_coment text,
    tih_abr character varying(64)
);


ALTER TABLE public.ht_hb_idioma_tipo_habitacion OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 24742)
-- Dependencies: 5
-- Name: ht_hb_piso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_piso (
    pi_id character(4) NOT NULL,
    su_id character varying(8),
    pi_numero numeric
);


ALTER TABLE public.ht_hb_piso OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24748)
-- Dependencies: 2185 5
-- Name: ht_hb_sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_sucursal (
    su_id character varying(8) NOT NULL,
    ho_id character varying(2),
    ub_id character(6),
    su_nombre character varying(128),
    su_estado integer,
    su_telefono1 character varying(25),
    su_telefono2 character varying(25),
    su_ruc character varying(14),
    su_direccion character varying(250),
    su_desc text,
    su_idcpy character varying(8),
    su_portal character(1) DEFAULT 'N'::bpchar,
    su_maestra character varying(1),
    su_reserva character varying(128)
);


ALTER TABLE public.ht_hb_sucursal OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 24755)
-- Dependencies: 2186 5
-- Name: ht_hb_sucursal_tipo_habitacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_sucursal_tipo_habitacion (
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    tih_stock integer,
    tih_state integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.ht_hb_sucursal_tipo_habitacion OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24758)
-- Dependencies: 5
-- Name: ht_hb_tarifa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_tarifa (
    ta_id character(1) NOT NULL
);


ALTER TABLE public.ht_hb_tarifa OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 24761)
-- Dependencies: 5
-- Name: ht_hb_tipo_habitacion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_tipo_habitacion (
    tih_id character varying(2) NOT NULL,
    ht_tih_id character varying(2),
    tih_estado integer,
    cat_id character(1),
    tih_orden integer
);


ALTER TABLE public.ht_hb_tipo_habitacion OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24764)
-- Dependencies: 5
-- Name: ht_hb_tipo_habitacion_imagen; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_hb_tipo_habitacion_imagen (
    tih_id character varying(2) NOT NULL,
    im_id character varying(8) NOT NULL,
    su_id character varying(8)
);


ALTER TABLE public.ht_hb_tipo_habitacion_imagen OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 24767)
-- Dependencies: 5
-- Name: ht_res_anio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_anio (
    an_id character(4) NOT NULL,
    an_abr character(4)
);


ALTER TABLE public.ht_res_anio OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24770)
-- Dependencies: 5
-- Name: ht_res_bonos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_bonos (
    bo_id character(2) NOT NULL,
    bo_dolar integer,
    bo_puntos integer
);


ALTER TABLE public.ht_res_bonos OWNER TO postgres;

--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE ht_res_bonos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE ht_res_bonos IS 'un punto vale un monto, tabla flotante, ';


--
-- TOC entry 207 (class 1259 OID 24773)
-- Dependencies: 5
-- Name: ht_res_cambio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_cambio (
    cam_fecha date NOT NULL,
    cam_sol character varying(5),
    cam_dol character varying(5)
);


ALTER TABLE public.ht_res_cambio OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 24776)
-- Dependencies: 5
-- Name: ht_res_idioma_anio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_idioma_anio (
    id_id character varying(2) NOT NULL,
    an_id character(4) NOT NULL,
    an_desc character varying(512)
);


ALTER TABLE public.ht_res_idioma_anio OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 24782)
-- Dependencies: 5
-- Name: ht_res_kardex; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_kardex (
    hb_id character varying(8) NOT NULL,
    tih_id character varying(2) NOT NULL,
    su_id character varying(8) NOT NULL,
    hb_capacidad integer,
    ta_id character(1),
    res_id character(10),
    ka_fecha date NOT NULL,
    ka_situacion character(1),
    pro_id integer
);


ALTER TABLE public.ht_res_kardex OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24785)
-- Dependencies: 2187 2188 5
-- Name: ht_res_reserva; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_reserva (
    res_id character(10) NOT NULL,
    cl_id character varying(8) NOT NULL,
    res_fecha date,
    su_id character varying(8),
    res_total numeric,
    res_promo character(16) DEFAULT 'N'::bpchar,
    res_tarj_id character varying(4),
    res_nomb_tarj character varying(128),
    res_numero_tarj character varying(16),
    res_tarj_caducidad character varying(16),
    res_cod_tarj character varying(16),
    res_monto_bonos character varying(8),
    res_monto_promo character varying(8),
    res_estado character(1) DEFAULT 1,
    res_persona integer,
    id_id character(2),
    res_codigo character varying(15)
);


ALTER TABLE public.ht_res_reserva OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 24793)
-- Dependencies: 2189 5
-- Name: ht_res_reserva_detalle; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_res_reserva_detalle (
    res_id character(10) NOT NULL,
    red_item smallint NOT NULL,
    hb_id character varying(8),
    pro_id character varying(8),
    an_id character(4),
    ser_id character(8),
    red_fecini date,
    red_fecfin date,
    red_precio numeric(10,2),
    red_tipo character(1) DEFAULT 'R'::bpchar,
    ac_id character varying(4),
    pro_dias integer
);


ALTER TABLE public.ht_res_reserva_detalle OWNER TO postgres;

--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE ht_res_reserva_detalle; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE ht_res_reserva_detalle IS 'Estado: 1 :  Uso promocion 0 ; No uso promocion
Se calcula con la tabla bono';


--
-- TOC entry 212 (class 1259 OID 24797)
-- Dependencies: 5
-- Name: ht_seg_cliente; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_cliente (
    cl_id character varying(8) NOT NULL,
    pe_id character varying(10),
    cl_estado integer,
    cl_puntos numeric,
    us_id character varying(8),
    per_id character varying(4)
);


ALTER TABLE public.ht_seg_cliente OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 24803)
-- Dependencies: 5
-- Name: ht_seg_idioma_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_idioma_menu (
    id_id character varying(2) NOT NULL,
    me_id character varying(8) NOT NULL,
    me_desc character varying(64)
);


ALTER TABLE public.ht_seg_idioma_menu OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 24806)
-- Dependencies: 5
-- Name: ht_seg_idioma_perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_idioma_perfil (
    id_id character varying(2) NOT NULL,
    per_id character varying(4) NOT NULL,
    per_desc character varying(32)
);


ALTER TABLE public.ht_seg_idioma_perfil OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 24809)
-- Dependencies: 5
-- Name: ht_seg_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_menu (
    me_id character varying(8) NOT NULL,
    mo_id character(2),
    ht_me_id character varying(8),
    me_link character varying(250),
    me_estado character(1),
    me_orden integer
);


ALTER TABLE public.ht_seg_menu OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 24812)
-- Dependencies: 5
-- Name: ht_seg_modulo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_modulo (
    mo_id character(2) NOT NULL,
    mo_desc character varying(32),
    mo_estado integer
);


ALTER TABLE public.ht_seg_modulo OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 24815)
-- Dependencies: 5
-- Name: ht_seg_perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_perfil (
    per_id character varying(4) NOT NULL,
    per_desc character varying(32),
    per_estado integer
);


ALTER TABLE public.ht_seg_perfil OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 24818)
-- Dependencies: 5
-- Name: ht_seg_perfil_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_perfil_menu (
    me_id character varying(8) NOT NULL,
    su_id character varying(8),
    per_id character varying(4) NOT NULL,
    pem_permiso character varying(1),
    pem_estado character varying(1)
);


ALTER TABLE public.ht_seg_perfil_menu OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 24821)
-- Dependencies: 5
-- Name: ht_seg_persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_persona (
    pe_id character varying(10) NOT NULL,
    ub_id character(6),
    pe_nombre character varying(64),
    pe_paterno character varying(64),
    pe_materno character varying(64),
    pe_fec_nacimiento date,
    pe_lugar_nacimiento character varying(64),
    pe_estado integer,
    pe_dni character(8),
    pe_pais character(4),
    pe_nacionalidad character(4),
    pe_nomcomp character varying(200),
    pe_sexo character(1),
    pe_tipdoc character varying(8),
    pe_company character varying(32),
    pe_direccion character varying(64),
    pe_telefono character varying(16),
    pe_qphciudad character varying(128)
);


ALTER TABLE public.ht_seg_persona OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 24827)
-- Dependencies: 5
-- Name: ht_seg_persona_hotel; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_persona_hotel (
    pe_id character varying(10) NOT NULL,
    ho_id character varying(2) NOT NULL
);


ALTER TABLE public.ht_seg_persona_hotel OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 24830)
-- Dependencies: 5
-- Name: ht_seg_ubigeo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_ubigeo (
    ub_id character varying(6) NOT NULL,
    ub_desc character varying(64)
);


ALTER TABLE public.ht_seg_ubigeo OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 24833)
-- Dependencies: 5
-- Name: ht_seg_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_usuario (
    us_id character varying(8) NOT NULL,
    per_id character varying(4) NOT NULL,
    pe_id character varying(10) NOT NULL,
    ho_id character varying(2),
    us_login character varying(32),
    us_password character varying(32),
    us_puntos integer,
    su_id character varying(8)
);


ALTER TABLE public.ht_seg_usuario OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 24836)
-- Dependencies: 5
-- Name: ht_seg_usuario_menu; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_seg_usuario_menu (
    us_id character varying(8) NOT NULL,
    per_id character varying(4) NOT NULL,
    pe_id character varying(10) NOT NULL,
    su_id character varying(8) NOT NULL,
    me_id character varying(8) NOT NULL,
    usm_permiso character varying(1),
    usm_estado character varying(1)
);


ALTER TABLE public.ht_seg_usuario_menu OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 24839)
-- Dependencies: 5
-- Name: ht_ser_idioma_servicio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_ser_idioma_servicio (
    ser_id character(8) NOT NULL,
    id_id character varying(2) NOT NULL,
    ser_titulo character varying(60),
    ser_desc character varying(200)
);


ALTER TABLE public.ht_ser_idioma_servicio OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 24842)
-- Dependencies: 5
-- Name: ht_ser_idioma_tipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_ser_idioma_tipo (
    set_id character(2) NOT NULL,
    id_id character varying(2) NOT NULL,
    set_titulo character varying(60),
    set_desc character varying(60)
);


ALTER TABLE public.ht_ser_idioma_tipo OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 24845)
-- Dependencies: 5
-- Name: ht_ser_servicio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_ser_servicio (
    ser_id character(8) NOT NULL,
    set_id character(2),
    ser_razsocial character varying(200),
    ser_ruc character varying(12),
    ser_direccion character varying(200),
    ser_telefono1 character varying(25),
    ser_telefono2 character varying(25),
    ser_web character varying(250),
    ser_correo character varying(60),
    ser_precio numeric,
    ser_estado integer,
    ub_id character(6)
);


ALTER TABLE public.ht_ser_servicio OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 24851)
-- Dependencies: 5
-- Name: ht_ser_servicio_sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_ser_servicio_sucursal (
    ser_id character(8) NOT NULL,
    su_id character varying(8) NOT NULL
);


ALTER TABLE public.ht_ser_servicio_sucursal OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 24854)
-- Dependencies: 5
-- Name: ht_ser_tipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ht_ser_tipo (
    set_id character(2) NOT NULL,
    set_estado character(1)
);


ALTER TABLE public.ht_ser_tipo OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 24857)
-- Dependencies: 5
-- Name: idioma_tabla; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE idioma_tabla (
    id_id character varying(2) NOT NULL,
    tba_id character varying(3) NOT NULL,
    tba_desc character varying(64)
);


ALTER TABLE public.idioma_tabla OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 24860)
-- Dependencies: 5
-- Name: idioma_tabla_det; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE idioma_tabla_det (
    id_id character varying(2) NOT NULL,
    tad_id character varying(4) NOT NULL,
    tba_id character varying(3) NOT NULL,
    tad_desc character varying(64)
);


ALTER TABLE public.idioma_tabla_det OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 24863)
-- Dependencies: 5
-- Name: idioma_tabla_sis; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE idioma_tabla_sis (
    tas_id character(4) NOT NULL,
    id_id character varying(2) NOT NULL,
    tas_desc character varying(64)
);


ALTER TABLE public.idioma_tabla_sis OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 24866)
-- Dependencies: 5
-- Name: menu_sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE menu_sucursal (
    me_id character varying(8) NOT NULL,
    su_id character varying(8) NOT NULL
);


ALTER TABLE public.menu_sucursal OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 24869)
-- Dependencies: 5
-- Name: tabla; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tabla (
    tba_id character varying(3) NOT NULL
);


ALTER TABLE public.tabla OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 24872)
-- Dependencies: 5
-- Name: tabla_detalle; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tabla_detalle (
    tad_id character varying(4) NOT NULL,
    tba_id character varying(3) NOT NULL
);


ALTER TABLE public.tabla_detalle OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 24875)
-- Dependencies: 5
-- Name: tabla_sistema; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tabla_sistema (
    tas_id character(4) NOT NULL,
    tas_orden integer
);


ALTER TABLE public.tabla_sistema OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 24878)
-- Dependencies: 2158 5
-- Name: vht_cms_articulo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_articulo AS
    SELECT ht_cms_articulo.art_id, ht_cms_idioma_articulo.id_id, ht_cms_articulo.art_est, ht_cms_idioma_articulo.art_titulo FROM (ht_cms_articulo LEFT JOIN ht_cms_idioma_articulo ON ((ht_cms_articulo.art_id = ht_cms_idioma_articulo.art_id)));


ALTER TABLE public.vht_cms_articulo OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 24882)
-- Dependencies: 2159 5
-- Name: vht_cms_articulo_categoria_sucursal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_articulo_categoria_sucursal AS
    SELECT cms_categoria_articulo.cat_id, cms_categoria_articulo.art_id, cms_categoria_articulo.su_id, cms_categoria_articulo.art_orden, cms_categoria_articulo.art_link, vht_cms_articulo.id_id, vht_cms_articulo.art_titulo, vht_cms_articulo.art_est FROM (cms_categoria_articulo JOIN vht_cms_articulo ON ((cms_categoria_articulo.art_id = vht_cms_articulo.art_id)));


ALTER TABLE public.vht_cms_articulo_categoria_sucursal OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 24886)
-- Dependencies: 2160 5
-- Name: vht_cms_categoria; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_categoria AS
    SELECT ht_cms_categoria.cat_id, ht_cms_categoria.cat_div, ht_cms_categoria.cat_tipo, ht_cms_categoria.cat_estado, ht_cms_categoria.cat_public, ht_cms_idioma_categoria.cat_titulo, ht_cms_idioma_categoria.id_id FROM ht_cms_categoria, ht_cms_idioma_categoria WHERE (ht_cms_categoria.cat_id = ht_cms_idioma_categoria.cat_id);


ALTER TABLE public.vht_cms_categoria OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 24890)
-- Dependencies: 2161 5
-- Name: vht_cms_categoria_sucursal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_categoria_sucursal AS
    SELECT ht_cms_categoria.cat_id, ht_cms_categoria.cat_div, ht_cms_categoria.cat_tipo, ht_cms_categoria.cat_estado, ht_cms_idioma_categoria.cat_titulo, ht_cms_idioma_categoria.id_id, ht_cms_sucursal_categoria.su_id FROM ht_cms_categoria, ht_cms_idioma_categoria, ht_cms_sucursal_categoria WHERE ((ht_cms_categoria.cat_id = ht_cms_idioma_categoria.cat_id) AND (ht_cms_categoria.cat_id = ht_cms_sucursal_categoria.cat_id));


ALTER TABLE public.vht_cms_categoria_sucursal OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 24894)
-- Dependencies: 2162 5
-- Name: vht_cms_descripciones; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_descripciones AS
    SELECT t1.des_id, t2.des_contenido, t1.des_estado, t2.id_id, t1.des_nombre FROM (ht_cms_descripciones t1 JOIN ht_cms_idioma_desc t2 ON (((t1.des_id)::text = (t2.des_id)::text)));


ALTER TABLE public.vht_cms_descripciones OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 24898)
-- Dependencies: 2163 5
-- Name: vht_cms_menu_interno; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_menu_interno AS
    SELECT t1.cat_id, t2.cat_titulo, t2.cat_desc, t1.cat_orden, t2.id_id FROM (ht_cms_menu_interno t1 JOIN ht_cms_idioma_menu_interno t2 ON ((t1.cat_id = t2.cat_id)));


ALTER TABLE public.vht_cms_menu_interno OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 24902)
-- Dependencies: 2164 5
-- Name: vht_cms_meses; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_meses AS
    SELECT t1.mes_id, t2.mes_desc, t2.mes_abr, t2.id_id FROM (ht_cms_meses t1 JOIN ht_cms_idioma_mes t2 ON ((t1.mes_id = t2.mes_id)));


ALTER TABLE public.vht_cms_meses OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 24906)
-- Dependencies: 2165 5
-- Name: vht_cms_promo_sucursal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_promo_sucursal AS
    SELECT t1.an_id, t1.su_id, t1.tih_id, t1.pro_id, t2.pro_fecalta, t2.pro_fecini, t2.pro_fecfin, t2.pro_dscto, t2.pro_estado FROM (ht_cms_promocion_sucursal t1 JOIN ht_cms_promocion t2 ON ((((t1.pro_id)::text = (t2.pro_id)::text) AND (t1.an_id = t2.an_id)))) ORDER BY t1.an_id, t1.su_id;


ALTER TABLE public.vht_cms_promo_sucursal OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 24910)
-- Dependencies: 2166 5
-- Name: vht_cms_promocion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_promocion AS
    SELECT t1.an_id, t1.pro_id, t2.pro_titulo, t2.pro_desc, t1.pro_fecalta, t1.pro_fecini, t1.pro_fecfin, t1.pro_dscto, t1.pro_estado, t1.tih_id, t1.ho_id, t2.id_id FROM (ht_cms_promocion t1 JOIN ht_cms_idioma_promocion t2 ON (((t1.an_id = t2.an_id) AND ((t1.pro_id)::text = (t2.pro_id)::text))));


ALTER TABLE public.vht_cms_promocion OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 24914)
-- Dependencies: 2167 5
-- Name: vht_cms_publicacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_cms_publicacion AS
    SELECT t1.pub_id, t2.pub_titulo, t2.pub_contenido, t1.pub_estado, t1.pub_tipo, t1.pub_fecha, t2.id_id FROM (ht_cms_publicacion t1 JOIN ht_cms_idioma_public t2 ON ((t1.pub_id = t2.pub_id)));


ALTER TABLE public.vht_cms_publicacion OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 24918)
-- Dependencies: 2168 5
-- Name: vht_hb_accesorios; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_hb_accesorios AS
    SELECT t1.ac_id, t2.ac_desc, t1.ac_estado, t1.ac_precio, t1.ac_tipo, t2.id_id FROM (ht_hb_accesorios t1 JOIN ht_hb_idioma_accesorios t2 ON (((t1.ac_id)::text = (t2.ac_id)::text)));


ALTER TABLE public.vht_hb_accesorios OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 24922)
-- Dependencies: 2169 5
-- Name: vht_hb_habitacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_hb_habitacion AS
    SELECT t1.hb_id, t2.hb_desc, t1.tih_id, t1.su_id, t1.pi_id, t1.hb_numero, t1.hb_estado, t2.id_id FROM (ht_hb_habitacion t1 JOIN ht_hb_idioma_habitacion t2 ON (((t1.hb_id)::text = (t2.hb_id)::text)));


ALTER TABLE public.vht_hb_habitacion OWNER TO postgres;

--
-- TOC entry 248 (class 1259 OID 24926)
-- Dependencies: 2170 5
-- Name: vht_hb_piso; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_hb_piso AS
    SELECT t1.pi_id, t1.su_id, t2.pi_desc, t1.pi_numero, t2.id_id FROM (ht_hb_piso t1 JOIN ht_hb_idioma_piso t2 ON ((t1.pi_id = t2.pi_id)));


ALTER TABLE public.vht_hb_piso OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 24930)
-- Dependencies: 2171 5
-- Name: vht_hb_tarifa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_hb_tarifa AS
    SELECT t1.ta_id, t2.ta_desc, t2.id_id FROM (ht_hb_tarifa t1 JOIN ht_hb_idioma_tarifa t2 ON ((t1.ta_id = t2.ta_id)));


ALTER TABLE public.vht_hb_tarifa OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 24934)
-- Dependencies: 2172 5
-- Name: vht_hb_tipo_habitacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_hb_tipo_habitacion AS
    SELECT t1.tih_id, t2.tih_desc, t2.tih_abr, t2.tih_coment, t1.ht_tih_id, t1.tih_estado, t1.tih_orden, t1.cat_id, t2.id_id FROM (ht_hb_tipo_habitacion t1 JOIN ht_hb_idioma_tipo_habitacion t2 ON (((t1.tih_id)::text = (t2.tih_id)::text)));


ALTER TABLE public.vht_hb_tipo_habitacion OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 24938)
-- Dependencies: 2173 5
-- Name: vht_res_anio; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_res_anio AS
    SELECT t1.an_id, t1.an_abr, t2.an_desc, t2.id_id FROM (ht_res_anio t1 JOIN ht_res_idioma_anio t2 ON ((t1.an_id = t2.an_id)));


ALTER TABLE public.vht_res_anio OWNER TO postgres;

--
-- TOC entry 252 (class 1259 OID 24942)
-- Dependencies: 2174 5
-- Name: vht_seg_cliente; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_seg_cliente AS
    SELECT ht_seg_persona.pe_id, ht_seg_persona.ub_id, ht_seg_persona.pe_nombre, ht_seg_persona.pe_paterno, ht_seg_persona.pe_materno, ht_seg_persona.pe_fec_nacimiento, ht_seg_persona.pe_lugar_nacimiento, ht_seg_persona.pe_estado, ht_seg_persona.pe_dni, ht_seg_persona.pe_pais, ht_seg_persona.pe_nacionalidad, ht_seg_persona.pe_nomcomp, ht_seg_cliente.cl_estado, ht_seg_cliente.cl_puntos, ht_seg_cliente.cl_id, ht_seg_persona.pe_sexo, ht_seg_persona.pe_tipdoc FROM (ht_seg_persona JOIN ht_seg_cliente ON (((ht_seg_persona.pe_id)::text = (ht_seg_cliente.pe_id)::text)));


ALTER TABLE public.vht_seg_cliente OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 24947)
-- Dependencies: 2175 5
-- Name: vht_seg_menu; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_seg_menu AS
    SELECT t1.me_id, t2.me_desc, t1.mo_id, t1.ht_me_id, t1.me_link, t1.me_estado, t1.me_orden, t2.id_id FROM (ht_seg_menu t1 JOIN ht_seg_idioma_menu t2 ON (((t1.me_id)::text = (t2.me_id)::text)));


ALTER TABLE public.vht_seg_menu OWNER TO postgres;

--
-- TOC entry 254 (class 1259 OID 24951)
-- Dependencies: 2176 5
-- Name: vht_seg_perfil; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_seg_perfil AS
    SELECT t1.per_id, t2.per_desc, t1.per_estado, t2.id_id FROM (ht_seg_perfil t1 JOIN ht_seg_idioma_perfil t2 ON (((t1.per_id)::text = (t2.per_id)::text)));


ALTER TABLE public.vht_seg_perfil OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 24955)
-- Dependencies: 2177 5
-- Name: vht_ser_servicio; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_ser_servicio AS
    SELECT t1.ser_id, t1.set_id, t1.ser_estado, t1.ser_razsocial, t1.ser_ruc, t1.ser_direccion, t1.ser_telefono1, t1.ser_telefono2, t1.ser_web, t1.ser_correo, t1.ser_precio, t2.ser_titulo, t2.ser_desc, t2.id_id, t1.ub_id FROM (ht_ser_servicio t1 JOIN ht_ser_idioma_servicio t2 ON ((t1.ser_id = t2.ser_id)));


ALTER TABLE public.vht_ser_servicio OWNER TO postgres;

--
-- TOC entry 256 (class 1259 OID 24959)
-- Dependencies: 2178 5
-- Name: vht_ser_tipo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vht_ser_tipo AS
    SELECT t1.set_id, t2.set_titulo, t2.set_desc, t1.set_estado, t2.id_id FROM (ht_ser_tipo t1 JOIN ht_ser_idioma_tipo t2 ON ((t1.set_id = t2.set_id)));


ALTER TABLE public.vht_ser_tipo OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 24963)
-- Dependencies: 2179 5
-- Name: vtabla; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vtabla AS
    SELECT t1.tba_id, t2.tba_desc, t2.id_id FROM (tabla t1 JOIN idioma_tabla t2 ON (((t1.tba_id)::text = (t2.tba_id)::text)));


ALTER TABLE public.vtabla OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 24967)
-- Dependencies: 2180 5
-- Name: vtabla_detalle; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vtabla_detalle AS
    SELECT t1.tad_id, t1.tba_id, t2.tad_desc, t2.id_id FROM (tabla_detalle t1 JOIN idioma_tabla_det t2 ON ((((t1.tad_id)::text = (t2.tad_id)::text) AND ((t1.tba_id)::text = (t2.tba_id)::text))));


ALTER TABLE public.vtabla_detalle OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 24971)
-- Dependencies: 2181 5
-- Name: vtabla_sistema; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW vtabla_sistema AS
    SELECT t1.tas_id, t2.tas_desc, t1.tas_orden, t2.id_id FROM (tabla_sistema t1 JOIN idioma_tabla_sis t2 ON ((t1.tas_id = t2.tas_id)));


ALTER TABLE public.vtabla_sistema OWNER TO postgres;

--
-- TOC entry 2545 (class 0 OID 24592)
-- Dependencies: 161
-- Data for Name: cms_categoria_articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_categoria_articulo (cat_id, art_id, su_id, art_orden, art_link) FROM stdin;
20110014	00000004	00000002	\N	\N
20110003	00000015	00000001	\N	\N
20110014	00000012	00000001	\N	\N
20110014	00000012	00000003	\N	\N
20110014	00000012	00000002	\N	\N
20110013	00000004	00000002	\N	\N
20110009	00000005	00000001	3	/index
20110009	00000002	00000001	1	/index
20110009	00000002	00000002	1	/
20110009	00000001	00000002	2	/promociones/index
20110009	00000016	00000002	3	/mapas
20110009	00000002	00000003	1	/
20110009	00000016	00000003	3	/mapas
20110009	00000001	00000003	2	/promociones
20110009	00000001	00000001	2	/promociones
\.


--
-- TOC entry 2546 (class 0 OID 24595)
-- Dependencies: 162
-- Data for Name: cms_imagenes_articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cms_imagenes_articulo (im_id, art_id, su_id, cat_id, im_orden, im_link) FROM stdin;
00000034	00000004	00000002	20110014	\N	\N
00000036	00000015	00000001	20110003	\N	\N
00000037	00000015	00000001	20110003	\N	\N
00000038	00000015	00000001	20110003	\N	\N
00000039	00000015	00000001	20110003	\N	\N
00000040	00000015	00000001	20110003	\N	\N
00000041	00000015	00000001	20110003	\N	\N
00000042	00000015	00000001	20110003	\N	\N
00000058	00000012	00000002	20110014	\N	\N
00000059	00000012	00000003	20110014	\N	\N
\.


--
-- TOC entry 2547 (class 0 OID 24598)
-- Dependencies: 163
-- Data for Name: ht_cms_articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_articulo (art_id, art_est) FROM stdin;
00000007	1
00000008	1
00000005	1
00000009	1
00000001	1
00000002	1
00000012	1
00000013	1
00000014	1
00000003	1
00000011	1
00000010	1
00000006	1
00000004	1
00000015	1
00000016	1
\.


--
-- TOC entry 2548 (class 0 OID 24601)
-- Dependencies: 164
-- Data for Name: ht_cms_categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_categoria (cat_id, cat_div, cat_tipo, cat_estado, ho_id, cat_public) FROM stdin;
20110008	footer	\N	1	01	C
20110003	otros	1	1	01	C
20110001	banner	1	1	01	C
20110009	Portal_menu	\N	1	01	C
20110014		\N	1	\N	C
20110010	/comentarios	\N	1	\N	P
20110012	/publicacion/index/tipo/N	\N	1	\N	P
20110011	/promociones/index	\N	1	\N	P
20110013	/publicacion/index/tipo/E	\N	1	\N	P
\.


--
-- TOC entry 2549 (class 0 OID 24604)
-- Dependencies: 165
-- Data for Name: ht_cms_descripciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_descripciones (des_id, des_estado, des_nombre) FROM stdin;
FEES	1	Aditional Fees
COST	1	Estimated Cost
NOS	1	Acerca de nosotros
IGV	1	Impuestos
TAXI	1	Taxi Price
989898	1	Content Guaranteed
ADD	1	Additional
ADD2	1	Additional_2
VER	1	Guaranteed
\.


--
-- TOC entry 2550 (class 0 OID 24607)
-- Dependencies: 166
-- Data for Name: ht_cms_idioma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma (id_id, id_desc, id_estado) FROM stdin;
ES	Español	1
EN	Ingles	1
\.


--
-- TOC entry 2551 (class 0 OID 24610)
-- Dependencies: 167
-- Data for Name: ht_cms_idioma_art_cont; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_art_cont (art_id, id_id, su_id, cat_id, art_contenido) FROM stdin;
00000004	ES	00000002	20110014	qp hotels prueba de texto<br>
00000004	EN	00000002	20110014	qp hotels prueba de texto<br>
00000015	ES	00000001	20110003	qp hotels prueba de texto<br>
00000015	EN	00000001	20110003	qp hotels prueba de texto<br>
00000012	ES	00000001	20110014	qp hotels prueba de texto<br>
00000012	EN	00000001	20110014	qp hotels prueba de texto<br>
00000012	ES	00000003	20110014	qp hotels prueba de texto<br>
00000012	EN	00000003	20110014	qp hotels prueba de texto<br>
00000012	ES	00000002	20110014	qp hotels prueba de texto<br>
00000012	EN	00000002	20110014	qp hotels prueba de texto<br>
00000004	ES	00000002	20110013	qp hotels prueba de texto<br>
00000004	EN	00000002	20110013	qp hotels prueba de texto<br>
00000002	ES	00000001	20110009	\N
00000002	EN	00000001	20110009	\N
00000001	ES	00000001	20110009	\N
00000001	EN	00000001	20110009	\N
00000005	ES	00000001	20110009	\N
00000005	EN	00000001	20110009	\N
00000001	ES	00000002	20110009	\N
00000001	EN	00000002	20110009	\N
00000002	ES	00000002	20110009	\N
00000002	EN	00000002	20110009	\N
00000001	ES	00000003	20110009	\N
00000001	EN	00000003	20110009	\N
00000002	ES	00000003	20110009	\N
00000002	EN	00000003	20110009	\N
00000016	ES	00000002	20110009	\N
00000016	EN	00000002	20110009	\N
00000016	ES	00000003	20110009	\N
00000016	EN	00000003	20110009	\N
\.


--
-- TOC entry 2552 (class 0 OID 24616)
-- Dependencies: 168
-- Data for Name: ht_cms_idioma_articulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_articulo (id_id, art_id, art_titulo) FROM stdin;
EN	00000004	Home
EN	00000001	Promotions
EN	00000005	About QP
ES	00000007	News
EN	00000007	News
ES	00000008	noticias news 2
EN	00000008	noticias news 2
ES	00000002	Inicio
ES	00000005	Eventos
ES	00000009	Destinos
EN	00000009	Destinos
ES	00000001	Promociones
EN	00000002	Home
ES	00000012	Imagenes Sucursal
EN	00000012	Imagenes Sucursal
EN	00000013	Porque Nosotros
ES	00000013	Porque Nosotros
ES	00000014	Porque nosotros 2
EN	00000014	Porque nosotros 2
ES	00000011	Destinos
ES	00000010	rewards
ES	00000003	Direccion
EN	00000003	direction
EN	00000011	destinos
EN	00000010	rewards
EN	00000006	landing page photo
ES	00000006	landing page foto
ES	00000004	Acerca de qp Hotels
ES	00000015	Imagenes Home
EN	00000015	Imagenes Home
ES	00000016	Mapas
EN	00000016	Mapas
\.


--
-- TOC entry 2553 (class 0 OID 24619)
-- Dependencies: 169
-- Data for Name: ht_cms_idioma_categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_categoria (id_id, cat_id, cat_titulo) FROM stdin;
ES	20110003	Banner Portal
ES	20110008	Footer
EN	20110001	Menu Top
EN	20110003	Banner Portal
EN	20110008	Footer
EN	20110009	Promociones
ES	20110001	Menu Horizontal
ES	20110009	Menu Top Portal
ES	20110011	Promociones
ES	20110012	Noticias
ES	20110013	Eventos
ES	20110014	Imagenes del Hotel
EN	20110014	Imagenes del Hotel
ES	20110010	beneficios
EN	20110010	benifets
EN	20110012	news
EN	20110011	promotions
EN	20110013	events
\.


--
-- TOC entry 2554 (class 0 OID 24622)
-- Dependencies: 170
-- Data for Name: ht_cms_idioma_desc; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_desc (id_id, des_id, des_contenido) FROM stdin;
EN	VER	<p>best online rates <a href=\\\\"#\\\\"><span class=\\\\"mayu-rate\\\\">GUARANTEED</span></a></p><br><br>
EN	IGV	<font size="1">Los impuestos son estimados basado en 18.00% por habitación y estancia de impuestos. Cambios en los impuestos o tasas aplicadas después de la reserva puede afectar la tasa total de su estancia....</font>
EN	TAXI	AIRPORT PICKUP IS AVALIABLE. THE COST OF THIS SERVICE IS US$35.00(ONE WAY). RESERVATION IS REQUIRED AT LEAST 24 HOURS IN ADVANCE AT THE MAIL RESERVAS@QPHOTELS.COM FROM MONDAT TO SATURDAY.
EN	COST	** (FOREIGN GUESTS (NON-RESIDENT) WILL BE EXEMPT FROM TAKES (18 PERCENT SALES\n                                            TAX) GUESTS MUST SHOW PASSPORT AND INMIGRATION CARD WITH STAMP)
EN	ADD	'<p>Total estimated cost for stay includes room rate, estimated taxes, and estimated fees.  Total estimated cost for stay does not include any additional applicable service charges or fees that may be charged by the hotel.  Estimated taxes and fees includes applicable local taxes, governmental fees, and resort fees as estimated by the hotel.  Carlson Hotels does not and can not verify the accuracy of the estimated taxes and estimated fees, and actual rates may vary at any time and from time to time.</p>
EN	ADD2	<p>Additional taxes and surcharges may apply.<br>\nTotal estimated cost is only available in the currency applicable to the hotel.</p>
EN	989898	<div style=\\\\"font-family: tahoma, arial, helvetica, sans-serif; text-align: center; font-weight: bold; \\\\"><font size=\\\\"2\\\\"><u>BEST RATE GUARANTEE</u><br></font></div><font><div style=\\\\"font-family: tahoma, arial, helvetica, sans-serif; font-size: 13px; text-align: center; \\\\"><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\"><br></font></div><div style=\\\\"text-align: left; \\\\"><font size=\\\\"1\\\\" face=\\\\"tahoma\\\\"><br></font></div><font><div style=\\\\"text-align: left; \\\\"><div><div>On www.qphotels.com we guarantee that you will get the best rate online. If you book your reservation at our website, &nbsp;you will always get the best available rate on internet.</div><br><div><b>TERMS AND CONDITIONS&nbsp;</b></div>&nbsp; &nbsp;<br><div>This guarantee applies if within 24 hours after your online reservation is confirmed you find an online rate that is lower than the best rate on www.qphotels.com. The lower online rate must be for the same room type, number of guests and arrival and departure dates.You must already have a confirmed online reservation on www.qphotels.com and a confirmation number so as to submit your online claim. Claims must be made within 24 hours after your online reservation is confirmed and 48 hours prior to your arrival date.&nbsp;After your claim is made, our Reservations representative will process your claim to verify its availability and validity. If your claim meets all the terms and conditions, will contact you by email to honor the lower online rate and discount that rate by 15%.&nbsp;This guarantee only applies for the room rate and does not apply for taxes, fees or any other charges. This guarantee only applies for general public, in that way, promotional rates, group rates, corporate rates or any other rate does not qualify. When a claim is for a reservation with multiple nights, the rates will be compared for each night individually. This guarantee cannot be combined with other offers or promotions.</div></div><div><br></div></div></font></font>
ES	IGV	<font size="1" face="arial">Las tarifas mencionadas están sujetas al 19% I.G.V válidas para una o dos personas y están expresadas en dólares americanos. De acuerdo a D.L personas naturales no residentes están exoneradas de pagar el I.G.V presentando su pasaporte vigente y/o carné de extranjería con su tarjeta de migración debidamente sellada.</font>
ES	ADD	<p><p><font face="arial">El costo total estimado de la estancia incluye la tarifa de la habitación, los impuestos y cargos estimados. El costo total estimado de la estancia no incluye ningún servicio adicional aplicable que pudiera ser cobrado por el hotel.&nbsp;</font><span style="font-family: arial;">Los impuestos y cargos estimados incluyen los impuestos locales aplicables, cargos gubernamentales y las tasas del resort estimadas por el hotel.&nbsp;</span></p><span style="font-family: arial;"><b><font color="#ff6600">qp</font> Hotels©</b> no verifica ni puede verificar la precisión de los impuestos y cargos estimados, y las tarifas reales pueden variar de manera periódica y en cualquier momento.</span></p>
ES	TAXI	Contamos con servicio de traslado. El costo de este servicio es de USD $36.00 (one way). La solicitud debe ser proporcionada con 24 horas de anticipación al siguiente correo electrónico: info@qphotels.com (de Lunes a Sábado)<br><br>
ES	ADD2	<p><font face="arial">Se pueden aplicar otras tasas y recargos adicionales.<br>\nEl precio total estimado sólo está disponible en la moneda aceptada por el hotel.</font></p>
ES	VER	<p><font face="arial">Mejor tarifa en línea <a href="http://qphotel.com/guaranteed"><span class="mayu-rate">GARANTIZADA</span></a></font></p>
ES	NOS	<b><font color="#ff6600">qp</font> Hotels®</b> son una colección de hoteles dirigidos tanto a clientes ejecutivos así como a turistas en selectas ubicaciones a lo largo del Perú.&nbsp;<br><br>Nuestra misión es la de proporcionar un refugio para los viajeros mediante la fusión de la comodidad y el lujo moderno de una manera inteligente y eficiente.&nbsp;<br><br>Ubicados estratégicamente en lugares de negocios y turismo, que combinan tanto la conveniencia de la cercanía a distritos financieros, gastronómicos y de compras, además de la comodidad y el relax de las áreas residenciales.&nbsp;<br><br><b><font color="#ff6600">qp</font> Hotels®</b> le ofrecerá el ambiente ideal para hacerlo sentir en casa, brindándole el servicio personalizado que usted merece.
EN	NOS	<div><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\" size=\\\\"2\\\\">Acerca de qp Hotels Â®</font></div><div><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\" size=\\\\"2\\\\"><br></font></div><div><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\" size=\\\\"2\\\\" color=\\\\"#808080\\\\">qp HotelsÂ® son una colecciÃ³n de â€œAn all suites boutique hotelâ€ dirigidos a clientes ejecutivos asÃ­ como a turistas en selectas ubicaciones a lo largo de toda AmÃ©rica. Nuestra visiÃ³n en los hoteles qp es de proporcionar un refugio para los viajeros mediante la fusiÃ³n de la comodidad y el lujo moderno de una manera inteligente y eficiente. Nuestros hoteles se ubican en solicitados lugares de negocios y turismo en LatinoamÃ©rica en Ã¡reas cÃ©ntricas que combinan tanto la conveniencia de la cercanÃ­a a distritos financieros, gastronÃ³micos y de compras, asÃ­ como la comodidad y el relax de las Ã¡reas residenciales. Nuestro hotel bandera en Miraflores, Lima, PerÃº estÃ¡ convenientemente ubicado en una Ã¡rea residencial tranquila en el corazón del hermoso distrito de Miraflores, encontrÃ¡ndose cerca a las playas, el centro de Lima, el distrito financiero de San Isidro y las tiendas y restaurantes de moda.</font></div><div><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\" size=\\\\"2\\\\" color=\\\\"#808080\\\\"><br></font></div><div><font face=\\\\"tahoma, arial, helvetica, sans-serif\\\\" size=\\\\"2\\\\" color=\\\\"#808080\\\\">&nbsp;</font></div>
ES	COST	<font face="arial">** (FOREIGN GUESTS (NON-RESIDENT) WILL BE EXEMPT FROM TAXES (18 PERCENT SALES TAX) GUESTS MUST SHOW PASSPORT AND IMMIGRATION CARD WITH STAMP)</font>
ES	989898	<div style="font-family: tahoma, arial, helvetica, sans-serif; text-align: center; font-weight: bold; "><font size="2"><u>BEST RATE GUARANTEE</u><br></font></div><font><div style="font-family: tahoma, arial, helvetica, sans-serif; font-size: 13px; text-align: center; "><br></div><font><div style="text-align: left; "><div><div>On www.qphotels.com we guarantee that you will get the best rate online. If you book your reservation at our website, &nbsp;you will always get the best available rate on internet.</div><br><div><b>TERMS AND CONDITIONS&nbsp;</b></div>&nbsp; &nbsp;<br><div>This guarantee applies if within 24 hours after your online reservation is confirmed you find an online rate that is lower than the best rate on www.qphotels.com. The lower online rate must be for the same room type, number of guests and arrival and departure dates.You must already have a confirmed online reservation on www.qphotels.com and a confirmation number so as to submit your online claim. Claims must be made within 24 hours after your online reservation is confirmed and 48 hours prior to your arrival date.&nbsp;After your claim is made, our Reservations representative will process your claim to verify its availability and validity. If your claim meets all the terms and conditions, will contact you by email to honor the lower online rate and discount that rate by 15%.&nbsp;This guarantee only applies for the room rate and does not apply for taxes, fees or any other charges. This guarantee only applies for general public, in that way, promotional rates, group rates, corporate rates or any other rate does not qualify. When a claim is for a reservation with multiple nights, the rates will be compared for each night individually. This guarantee cannot be combined with other offers or promotions.</div></div><div><br></div></div></font></font>
\.


--
-- TOC entry 2555 (class 0 OID 24628)
-- Dependencies: 171
-- Data for Name: ht_cms_idioma_menu_interno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_menu_interno (cat_id, id_id, cat_desc, cat_titulo) FROM stdin;
D	EN	EN-Enjoy Okey	Enjoy
W	EN	EN-Working	Work
E	EN	EN-Explorar from Peru	Explorer
H	EN	Choose the best Room that suit your needs and taste. Take a look at each room and see all the facilities they offer,	Stay
H	ES	Descanse en nuestras habitaciones diseñadas para ajustarse a cualquier necesidad de estadía. Usted encontrará una perfecta armonía entre elegancia y modernismo.	Descanse
E	ES	Compruebe la perfecta ubicación de nuestro establecimiento, con una interesante propuesta de destinos cercanos para visitar.	Explore
D	ES	Tomarse un tiempo para poder disfrutar de todos los ambientes de nuestro hotel, es definitivamente una gran decisión.	Disfrute
W	ES	Negocie en nuestros modernos ambientes de trabajo.  	Negocie
\.


--
-- TOC entry 2556 (class 0 OID 24634)
-- Dependencies: 172
-- Data for Name: ht_cms_idioma_mes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_mes (id_id, mes_id, mes_desc, mes_abr) FROM stdin;
ES	01	Enero	Ene
ES	02	Febrero	Feb
ES	03	Marzo	Mar
ES	04	Abril	Abr
ES	05	Mayo	May
ES	06	Junio	Jun
ES	07	Julio	Jul
ES	08	Agosto	Ago
ES	09	Septiembre	Sep
ES	10	Octubre	Oct
ES	11	Noviembre	Nov
ES	12	Diciembre	Dic
EN	01	January	Jan
EN	03	March	Mar
EN	04	April	Apr
EN	07	July	Jul
EN	09	September	Sep
EN	11	November	Nov
EN	05	May	May
EN	02	February	Feb
EN	10	October	Oct
EN	06	June	Jun
EN	08	August	Aug
EN	12	December	Dic
\.


--
-- TOC entry 2557 (class 0 OID 24637)
-- Dependencies: 173
-- Data for Name: ht_cms_idioma_promocion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_promocion (pro_id, an_id, id_id, pro_titulo, pro_desc) FROM stdin;
VISA2	2012	EN	Visa Lima	Reciba un 10% de descuento en su alojamiento y un 20% de descuento en comidas en los hoteles de la cadena qp Hotels (Lima, Arequipa y Trujillo) al pagar con su tarjeta Visa.\n\nPara redimir esta oferta reserve en línea con toda seguridad en www.qphotels.com/visanetperu , o llame a nuestro centro de reservas al +51 (1) 319 2929  y mencione “PROMOCION VISAPERU".\n\nTérminos Legales\nLa oferta es válida sola para peruanos o residentes para estadías hasta el 31 de Octubre del 2013 y sujeta a disponibilidad. No válida para días feriados ni festivos, válida para cualquier número de noches, los 7 días de la semana, en qp Hotels Lima. Las tarifas son por habitación, por noche, basadas en dos personas en una habitación Deluxe Suite, incluye el IGV. Se permiten cancelaciones sin cargo 48 horas antes del día de llegada. Se hará un cargo por una noche de alojamiento cuando las cancelaciones se reciban después de esta fecha y en caso de no presentarse el huésped.\nEl 20% de descuento en las comidas es válido solamente en los restaurantes de los hoteles, se aplica al almuerzo y/o cena, excluye bebidas alcohólicas. \nLa tarjeta de crédito utilizada para pagar se deberá presentar al llegar al hotel. Si el huésped paga por otra persona, debe comunicarse con el Centro de Reservas de qp Hotels para recibir asistencia adicional. Oferta válida solamente cuando usted paga con su tarjeta Visa. No es válida en conjunción con ninguna otra oferta\n
NBO	2012	ES	Noche de bodas	15% de Descuento en la tarifa de SUPERIOR SUITE QUEEN
16	2012	ES	Visa Arequipa	Use su tarjeta Visa al reservar su estadía de 3 noches o más y reciba un 20% de descuento en la Mejor tarifa disponible en superior suite.\n\nTérminos Legales\n La oferta es válida para estadías hasta el 31 de Diciembre del 2013 y sujeta a disponibilidad. No válida para días feriados ni festivos, válida para cualquier número de noches, los 7 días de la semana, en qp Hotels Arequipa. Las tarifas son por habitación, por noche, basadas en dos personas en una habitación Superior Suite, incluye el IGV. Se permiten cancelaciones sin cargo 48 horas antes del día de llegada. Se hará un cargo por una noche de alojamiento cuando las cancelaciones se reciban después de esta fecha y en caso de no presentarse el huésped.\nLa tarjeta de crédito utilizada para pagar se deberá presentar al llegar al hotel. Si el huésped paga por otra persona, debe comunicarse con el Centro de Reservas de qp Hotels para recibir asistencia adicional. Oferta válida solamente cuando usted paga con su tarjeta Visa. \n
VISALIM	2012	EN	Visa Lima	Reciba un 10% de descuento en su alojamiento y un 20% de descuento en comidas en los hoteles de la cadena qp Hotels (Lima, Arequipa y Trujillo) al pagar con su tarjeta Visa.
24	2012	ES	Visa Lima	Reciba un 10% de descuento en su alojamiento y un 20% de descuento en comidas en los hoteles de la cadena qp Hotels (Lima, Arequipa y Trujillo) al pagar con su tarjeta Visa.
NBO	2012	EN	Noche de bonas	Noche de bonas
24	2012	EN	Nightwish Promo	Descuento para fans de la banda de Nightwish
16	2012	EN	TEST 2 Promo	Prueba numero 2
2013	2013	EN	Año 2013 xD!	por este año 30% de descuento
VISA2	2012	ES	Visa Lima	Reciba un 10% de descuento en su alojamiento y un 20% de descuento en comidas en los hoteles de la cadena qp Hotels (Lima, Arequipa y Trujillo) al pagar con su tarjeta Visa.\n\nPara redimir esta oferta reserve en línea con toda seguridad en www.qphotels.com/visanetperu , o llame a nuestro centro de reservas al +51 (1) 319 2929  y mencione “PROMOCION VISAPERU".\n\nTérminos Legales\nLa oferta es válida sola para peruanos o residentes para estadías hasta el 31 de Octubre del 2013 y sujeta a disponibilidad. No válida para días feriados ni festivos, válida para cualquier número de noches, los 7 días de la semana, en qp Hotels Lima. Las tarifas son por habitación, por noche, basadas en dos personas en una habitación Deluxe Suite, incluye el IGV. Se permiten cancelaciones sin cargo 48 horas antes del día de llegada. Se hará un cargo por una noche de alojamiento cuando las cancelaciones se reciban después de esta fecha y en caso de no presentarse el huésped.\nEl 20% de descuento en las comidas es válido solamente en los restaurantes de los hoteles, se aplica al almuerzo y/o cena, excluye bebidas alcohólicas. \nLa tarjeta de crédito utilizada para pagar se deberá presentar al llegar al hotel. Si el huésped paga por otra persona, debe comunicarse con el Centro de Reservas de qp Hotels para recibir asistencia adicional. Oferta válida solamente cuando usted paga con su tarjeta Visa. No es válida en conjunción con ninguna otra oferta\n
2013	2013	ES	Año 2013 xD!	por este año 30% de descuento
VISALIM	2012	ES	Visa Lima	Reciba un 10% de descuento en su alojamiento y un 20% de descuento en comidas en los hoteles de la cadena qp Hotels (Lima, Arequipa y Trujillo) al pagar con su tarjeta Visa.
\.


--
-- TOC entry 2558 (class 0 OID 24643)
-- Dependencies: 174
-- Data for Name: ht_cms_idioma_promsucursal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_promsucursal (id_id, pro_id, an_id, tih_id, su_id, pro_desc) FROM stdin;
ES	2013	2013	01	00000002	okas okas okas<br>
ES	VISA2	2012	02	00000002	Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan \neuripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, \nquo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, \nlegimus senserit definiebas an eos.
\.


--
-- TOC entry 2559 (class 0 OID 24649)
-- Dependencies: 175
-- Data for Name: ht_cms_idioma_public; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_idioma_public (pub_id, id_id, pub_titulo, pub_contenido) FROM stdin;
0000000002	EN	qp Hotels próximo a estrenar local en Trujillo y mira Cusco, Piura e Ica	<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Su nuevo proyecto se levantará en Trujillo, dijo su gerente ejecutivo, Diego Castro.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Para ello tienen estimado invertir alrededor de US$1.9 millones en casi 600 metros cuadrados. Este se instalará en los alrededores de la urbanización El Golf, que es una zona residencial.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;Tendrá una extensión de cuatro pisos y a diferencia del concepto boutique que</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">han venido ofertando a la fecha, evalúan orientarlo hacia un público más corporativo.</span><p style="padding-left: 0px; margin-left: 0px; font-family: Arial, Helvetica, Arial, sans-serif; "><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Por lo pronto, prevén que lo estrenarán en el primer semestre del próximo año.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;De igual manera, a la fecha analizan a las ciudades de Cusco, Piura e Ica para seguir expandiéndose en el interior del país.</span><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Ya han identificado terrenos en estas tres zonas y las conversaciones</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;se encuentran avanzadas. En la primera de ellas evalúan adquirir un hotel y</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;comprarían los terrenos en las demás ciudades en cuestión.</span><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Se espera que en el año 2013 se avance con el proyecto, en cualquiera de estas alternativas, para poder inaugurarlo en el primer semestre del 2014.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;“De concretarse en Cusco no necesariamente tendría un perfil tan corporativo como se puede obtener en Piura o en Ica”, dijo Diego Castro. A largo plazo les gustaría incursionar en Iquitos y Tacna, ya que consideran que son mercados&nbsp;</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">que están en crecimiento.</span><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><span style="text-decoration: underline; "><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">Estreno</span></span>&nbsp;<br style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; "><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">A principios de junio, QP Hotels abrió las puertas de su segundo establecimiento, ubicándose este en el departamento de Arequipa.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;Para ello invirtieron entre US$1.4 millones y US$1.6 millones en la puesta en valor de una casona y en su implementación.</span><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; text-align: justify; ">&nbsp;Este hotel cuenta con cuatro pisos, dos sótanos y 19 habitaciones ya que proponen una propuesta tipo boutique. Todo ello en un espacio que supera los 400 m2.</span></p><p style="text-align: right;padding-left: 0px; margin-left: 0px; font-family: Arial, Helvetica, Arial, sans-serif; "><span style="color: rgb(51, 51, 51); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; text-align: justify; ">Fuente: Gestión</span></p>
0000000001	EN	Evento 1	Evento 1
0000000002	ES	qp Hotels próximo a estrenar local en Trujillo y mira Cusco, Piura e Ica	<font face="arial" size="2"><span style="text-align: justify; ">Su nuevo proyecto se levantará en Trujillo, dijo su gerente ejecutivo, Diego Castro.</span><span style="text-align: justify; ">Para ello tienen estimado invertir alrededor de US$1.9 millones en casi 600 metros cuadrados. Este se instalará en los alrededores de la urbanización El Golf, que es una zona residencial.</span><span style="text-align: justify; ">&nbsp;Tendrá una extensión de cuatro pisos y a diferencia del concepto boutique que</span><span style="text-align: justify; ">han venido ofertando a la fecha, evalúan orientarlo hacia un público más corporativo.</span></font><p style="padding-left: 0px; margin-left: 0px; "><font face="arial" size="2"><span style="text-align: justify; ">Por lo pronto, prevén que lo estrenarán en el primer semestre del próximo año.</span><span style="text-align: justify; ">&nbsp;De igual manera, a la fecha analizan a las ciudades de Cusco, Piura e Ica para seguir expandiéndose en el interior del país.</span><br style="text-align: justify; "><br style="text-align: justify; "><span style="text-align: justify; ">Ya han identificado terrenos en estas tres zonas y las conversaciones</span><span style="text-align: justify; ">&nbsp;se encuentran avanzadas. En la primera de ellas evalúan adquirir un hotel y</span><span style="text-align: justify; ">&nbsp;comprarían los terrenos en las demás ciudades en cuestión.</span><br style="text-align: justify; "><br style="text-align: justify; "><span style="text-align: justify; ">Se espera que en el año 2013 se avance con el proyecto, en cualquiera de estas alternativas, para poder inaugurarlo en el primer semestre del 2014.</span><span style="text-align: justify; ">&nbsp;“De concretarse en Cusco no necesariamente tendría un perfil tan corporativo como se puede obtener en Piura o en Ica”, dijo Diego Castro. A largo plazo les gustaría incursionar en Iquitos y Tacna, ya que consideran que son mercados&nbsp;</span><span style="text-align: justify; ">que están en crecimiento.</span><br style="text-align: justify; "><br style="text-align: justify; "><span style="text-decoration: underline; "><span style="text-align: justify; ">Estreno</span></span>&nbsp;<br style="text-align: justify; "><span style="text-align: justify; ">A principios de junio, QP Hotels abrió las puertas de su segundo establecimiento, ubicándose este en el departamento de Arequipa.</span><span style="text-align: justify; ">&nbsp;Para ello invirtieron entre US$1.4 millones y US$1.6 millones en la puesta en valor de una casona y en su implementación.</span><span style="text-align: justify; ">&nbsp;Este hotel cuenta con cuatro pisos, dos sótanos y 19 habitaciones ya que proponen una propuesta tipo boutique. Todo ello en un espacio que supera los 400 m2.</span></font></p><font face="arial" size="2"><br><br></font><p style="text-align: right; padding-left: 0px; margin-left: 0px; "><span style="color: rgb(51, 51, 51); font-weight: bold; text-align: justify; "><font face="arial" size="2">Fuente: Gestión</font></span></p>
0000000001	ES	qp Hotels expande su presencia a Arequipa y apunta a Trujillo	La cadena QP Hotels inauguró el sábado 9 de\njunio su segundo hotel boutique bajo el concepto de “All suites boutique” en\nArequipa, en el marco de su plan de expansión a nivel nacional que lo llevará a\naterrizar también a la ciudad de Trujillo. &nbsp;<br><br>Diego Castro, gerente ejecutivo de qp Hotels, afirmó que el nuevo hotel está ubicado\nen el Centro Histórico de Arequipa y rescata una arquitectura original de la\népoca Republicana, que se entremezcla con la modernidad y tecnología.\n“Proporciona un refugio para los viajeros mediante la fusión de la comodidad y\nel lujo moderno de una manera inteligente y eficiente que harán de la estadía\ndel huésped sea una experiencia inolvidable”, refirió. &nbsp;<br><br>Como se sabe, el primer qp Hotels nace en Miraflores bajo un concepto de “All suites\nboutique” dirigido a clientes ejecutivos así como a turistas de toda América.\n\n“Este tipo de hoteles se ubican en zonas céntricas que combinan la cercanía a\ndistritos financieros, gastronómicos y turísticos con la conservación del\npatrimonio, tecnología y modernidad. Además, se basan en la atención\npersonalizada con dirección a los detalles, experiencias y características que\nhoy busca un huésped de lujo”, expresó Castro.&nbsp;<br><br>El ministerio de Comercio Exterior y Turismo\n(Mincetur) otorgó a qp Hotels el certificado que lo acredita como un Apart\nHotel de 4 Estrellas. "Siempre ha sido nuestro objetivo crear una cadena\nhotelera que responda a la exigente demanda del segmento corporativo, primero\nha sido en Lima, ahora en Arequipa y próximamente en Trujillo”, puntualizó el\nejecutivo&nbsp;<br><br><p style="text-align: right;"><b>Fuente: infoturperu</b></p><p></p>
0000000003	ES	Feria AVIT 2012 Arequipa	<font color="#333333" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center;">Imágenes de lo que fue el Desarrollo de los 4 días de la Feria Turística, un evento el cual congregó a grandes cantidades de empresas enfocadas al sector turístico de la región de Arequipa. La cadena&nbsp;</font><font color="#ff6600" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center; font-weight: bold;">qp</font><font color="#333333" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center;"><b>&nbsp;Hotels©&nbsp;</b>estuvo presente realizando una exposición de todas sus filiales.&nbsp;</font>
0000000004	EN	Banda Nightwish en qp Hotels	<b><font color="#ff6600">qp</font>&nbsp;Hotels©</b>&nbsp;tuvo el agrado de tener como huéspedes a la reconocida banda finlandesa de metal sinfónico Nightwish, la cual dió una presentación en vivo el 7 de Diciembre @El Centro de convenciones Claro de Plaza San Miguel.
0000000004	ES	Banda Nightwish en qp Hotels	<b><font color="#ff6600">qp</font>&nbsp;Hotels©</b>&nbsp;tuvo el agrado de tener como huéspedes a la reconocida banda finlandesa de metal sinfónico Nightwish, la cual dió una presentación en vivo el 7 de Diciembre @El Centro de convenciones Claro de Plaza San Miguel.
0000000005	EN	Certificación  CALTUR 2012	<b><font color="#ff6600">​qp</font> Hotels©</b> tuvo el logro de obtener la&nbsp;<span style="color: rgb(51, 51, 51); font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px;">certificación CALTUR 2012 "Reconocimiento de buenas prácticas" en el marco del Plan Nacional de Calidad Turística. Comprobando una vez más los altos estandares de calidad que el hotel cumple para con sus huéspedes.</span><br><br>
0000000005	ES	Certificación  CALTUR 2012	<b><font color="#ff6600">​qp</font> Hotels©</b> tuvo el logro de obtener la&nbsp;<span style="color: rgb(51, 51, 51); font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px;">certificación CALTUR 2012 "Reconocimiento de buenas prácticas" en el marco del Plan Nacional de Calidad Turística. Comprobando una vez más los altos estandares de calidad que el hotel cumple para con sus huéspedes.</span><br><br>
0000000003	EN	Feria AVIT 2012 Arequipa	<font color="#333333" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center;">Imágenes de lo que fue el Desarrollo de los 4 días de la Feria Turística, un evento el cual congregó a grandes cantidades de empresas enfocadas al sector turístico de la región de Arequipa. La cadena&nbsp;</font><font color="#ff6600" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center; font-weight: bold;">qp</font><font color="#333333" style="font-family: 'lucida grande', tahoma, verdana, arial, sans-serif; line-height: 18px; text-align: center;"><b>&nbsp;Hotels©&nbsp;</b>estuvo presente realizando una exposición de todas sus filiales.&nbsp;</font>
0000000006	EN	asdf	asdf
0000000006	ES	asdf	asdf
\.


--
-- TOC entry 2560 (class 0 OID 24655)
-- Dependencies: 176
-- Data for Name: ht_cms_imagenes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_imagenes (im_id, im_image1, im_image2, im_image3, im_image4, im_image5, im_estado) FROM stdin;
00000001	/eventos/01/0000000001/00000001/00000001_1.jpg	/eventos/01/0000000001/00000001/00000001_2.jpg	/eventos/01/0000000001/00000001/00000001_3.jpg	/eventos/01/0000000001/00000001/00000001_4.jpg	/eventos/01/0000000001/00000001/00000001_5.jpg	1
00000002	/eventos/01/0000000002/00000002/00000002_1.JPG	/eventos/01/0000000002/00000002/00000002_2.JPG	/eventos/01/0000000002/00000002/00000002_3.JPG	/eventos/01/0000000002/00000002/00000002_4.JPG	/eventos/01/0000000002/00000002/00000002_5.JPG	1
00000082	/habitacion//00000010/00000082/00000082_1.JPG	/habitacion//00000010/00000082/00000082_2.JPG	/habitacion//00000010/00000082/00000082_3.JPG	/habitacion//00000010/00000082/00000082_4.JPG	/habitacion//00000010/00000082/00000082_5.JPG	1
00000085	/habitacion//00000017/00000085/00000085_1.jpg	/habitacion//00000017/00000085/00000085_2.jpg	/habitacion//00000017/00000085/00000085_3.jpg	/habitacion//00000017/00000085/00000085_4.jpg	/habitacion//00000017/00000085/00000085_5.jpg	1
00000087	/tipohabitacion//06/00000087/00000087_1.jpg	/tipohabitacion//06/00000087/00000087_2.jpg	/tipohabitacion//06/00000087/00000087_3.jpg	/tipohabitacion//06/00000087/00000087_4.jpg	/tipohabitacion//06/00000087/00000087_5.jpg	1
00000010	/tipohabitacion/01/07/00000010/00000010_1.JPG	/tipohabitacion/01/07/00000010/00000010_2.JPG	/tipohabitacion/01/07/00000010/00000010_3.JPG	/tipohabitacion/01/07/00000010/00000010_4.JPG	/tipohabitacion/01/07/00000010/00000010_5.JPG	1
00000011	/tipohabitacion/01/07/00000011/00000011_1.JPG	/tipohabitacion/01/07/00000011/00000011_2.JPG	/tipohabitacion/01/07/00000011/00000011_3.JPG	/tipohabitacion/01/07/00000011/00000011_4.JPG	/tipohabitacion/01/07/00000011/00000011_5.JPG	1
00000090	/habitacion//00000053/00000090/00000090_1.jpg	/habitacion//00000053/00000090/00000090_2.jpg	/habitacion//00000053/00000090/00000090_3.jpg	/habitacion//00000053/00000090/00000090_4.jpg	/habitacion//00000053/00000090/00000090_5.jpg	1
00000092	/habitacion//00000042/00000092/00000092_1.jpg	/habitacion//00000042/00000092/00000092_2.jpg	/habitacion//00000042/00000092/00000092_3.jpg	/habitacion//00000042/00000092/00000092_4.jpg	/habitacion//00000042/00000092/00000092_5.jpg	1
00000014	/tipohabitacion/01/08/00000014/00000014_1.JPG	/tipohabitacion/01/08/00000014/00000014_2.JPG	/tipohabitacion/01/08/00000014/00000014_3.JPG	/tipohabitacion/01/08/00000014/00000014_4.JPG	/tipohabitacion/01/08/00000014/00000014_5.JPG	1
00000015	/tipohabitacion/01/08/00000015/00000015_1.JPG	/tipohabitacion/01/08/00000015/00000015_2.JPG	/tipohabitacion/01/08/00000015/00000015_3.JPG	/tipohabitacion/01/08/00000015/00000015_4.JPG	/tipohabitacion/01/08/00000015/00000015_5.JPG	1
00000016	/tipohabitacion/01/08/00000016/00000016_1.JPG	/tipohabitacion/01/08/00000016/00000016_2.JPG	/tipohabitacion/01/08/00000016/00000016_3.JPG	/tipohabitacion/01/08/00000016/00000016_4.JPG	/tipohabitacion/01/08/00000016/00000016_5.JPG	1
00000017	/tipohabitacion/01/08/00000017/00000017_1.JPG	/tipohabitacion/01/08/00000017/00000017_2.JPG	/tipohabitacion/01/08/00000017/00000017_3.JPG	/tipohabitacion/01/08/00000017/00000017_4.JPG	/tipohabitacion/01/08/00000017/00000017_5.JPG	1
00000018	/tipohabitacion/01/07/00000018/00000018_1.JPG	/tipohabitacion/01/07/00000018/00000018_2.JPG	/tipohabitacion/01/07/00000018/00000018_3.JPG	/tipohabitacion/01/07/00000018/00000018_4.JPG	/tipohabitacion/01/07/00000018/00000018_5.JPG	1
00000019	/tipohabitacion/01/07/00000019/00000019_1.JPG	/tipohabitacion/01/07/00000019/00000019_2.JPG	/tipohabitacion/01/07/00000019/00000019_3.JPG	/tipohabitacion/01/07/00000019/00000019_4.JPG	/tipohabitacion/01/07/00000019/00000019_5.JPG	1
00000020	/tipohabitacion/01/07/00000020/00000020_1.JPG	/tipohabitacion/01/07/00000020/00000020_2.JPG	/tipohabitacion/01/07/00000020/00000020_3.JPG	/tipohabitacion/01/07/00000020/00000020_4.JPG	/tipohabitacion/01/07/00000020/00000020_5.JPG	1
00000093	/eventos//0000000003/00000093/00000093_1.jpg	/eventos//0000000003/00000093/00000093_2.jpg	/eventos//0000000003/00000093/00000093_3.jpg	/eventos//0000000003/00000093/00000093_4.jpg	/eventos//0000000003/00000093/00000093_5.jpg	1
00000022	/tipohabitacion/01/10/00000022/00000022_1.jpg	/tipohabitacion/01/10/00000022/00000022_2.jpg	/tipohabitacion/01/10/00000022/00000022_3.jpg	/tipohabitacion/01/10/00000022/00000022_4.jpg	/tipohabitacion/01/10/00000022/00000022_5.jpg	1
00000023	/tipohabitacion/01/10/00000023/00000023_1.jpg	/tipohabitacion/01/10/00000023/00000023_2.jpg	/tipohabitacion/01/10/00000023/00000023_3.jpg	/tipohabitacion/01/10/00000023/00000023_4.jpg	/tipohabitacion/01/10/00000023/00000023_5.jpg	1
00000024	/tipohabitacion/01/09/00000024/00000024_1.jpg	/tipohabitacion/01/09/00000024/00000024_2.jpg	/tipohabitacion/01/09/00000024/00000024_3.jpg	/tipohabitacion/01/09/00000024/00000024_4.jpg	/tipohabitacion/01/09/00000024/00000024_5.jpg	1
00000025	/tipohabitacion/01/09/00000025/00000025_1.jpg	/tipohabitacion/01/09/00000025/00000025_2.jpg	/tipohabitacion/01/09/00000025/00000025_3.jpg	/tipohabitacion/01/09/00000025/00000025_4.jpg	/tipohabitacion/01/09/00000025/00000025_5.jpg	1
00000026	/tipohabitacion/01/09/00000026/00000026_1.jpg	/tipohabitacion/01/09/00000026/00000026_2.jpg	/tipohabitacion/01/09/00000026/00000026_3.jpg	/tipohabitacion/01/09/00000026/00000026_4.jpg	/tipohabitacion/01/09/00000026/00000026_5.jpg	1
00000027	/tipohabitacion/01/08/00000027/00000027_1.JPG	/tipohabitacion/01/08/00000027/00000027_2.JPG	/tipohabitacion/01/08/00000027/00000027_3.JPG	/tipohabitacion/01/08/00000027/00000027_4.JPG	/tipohabitacion/01/08/00000027/00000027_5.JPG	1
00000097	/promocion/00000003/16-2012/00000097/00000097_1.jpg	/promocion/00000003/16-2012/00000097/00000097_2.jpg	/promocion/00000003/16-2012/00000097/00000097_3.jpg	/promocion/00000003/16-2012/00000097/00000097_4.jpg	/promocion/00000003/16-2012/00000097/00000097_5.jpg	1
00000029	/tipohabitacion/01/11/00000029/00000029_1.JPG	/tipohabitacion/01/11/00000029/00000029_2.JPG	/tipohabitacion/01/11/00000029/00000029_3.JPG	/tipohabitacion/01/11/00000029/00000029_4.JPG	/tipohabitacion/01/11/00000029/00000029_5.JPG	1
00000030	/tipohabitacion/01/14/00000030/00000030_1.JPG	/tipohabitacion/01/14/00000030/00000030_2.JPG	/tipohabitacion/01/14/00000030/00000030_3.JPG	/tipohabitacion/01/14/00000030/00000030_4.JPG	/tipohabitacion/01/14/00000030/00000030_5.JPG	1
00000031	/tipohabitacion/01/13/00000031/00000031_1.JPG	/tipohabitacion/01/13/00000031/00000031_2.JPG	/tipohabitacion/01/13/00000031/00000031_3.JPG	/tipohabitacion/01/13/00000031/00000031_4.JPG	/tipohabitacion/01/13/00000031/00000031_5.JPG	1
00000032	/tipohabitacion/01/12/00000032/00000032_1.JPG	/tipohabitacion/01/12/00000032/00000032_2.JPG	/tipohabitacion/01/12/00000032/00000032_3.JPG	/tipohabitacion/01/12/00000032/00000032_4.JPG	/tipohabitacion/01/12/00000032/00000032_5.JPG	1
00000033	/tipohabitacion/01/15/00000033/00000033_1.JPG	/tipohabitacion/01/15/00000033/00000033_2.JPG	/tipohabitacion/01/15/00000033/00000033_3.JPG	/tipohabitacion/01/15/00000033/00000033_4.JPG	/tipohabitacion/01/15/00000033/00000033_5.JPG	1
00000034	/articulo/01/00000002/20110014/00000004/00000034/00000034_1.jpg	/articulo/01/00000002/20110014/00000004/00000034/00000034_2.jpg	/articulo/01/00000002/20110014/00000004/00000034/00000034_3.jpg	/articulo/01/00000002/20110014/00000004/00000034/00000034_4.jpg	/articulo/01/00000002/20110014/00000004/00000034/00000034_5.jpg	1
00000036	/articulo/01/00000001/20110003/00000015/00000036/00000036_1.jpg	/articulo/01/00000001/20110003/00000015/00000036/00000036_2.jpg	/articulo/01/00000001/20110003/00000015/00000036/00000036_3.jpg	/articulo/01/00000001/20110003/00000015/00000036/00000036_4.jpg	/articulo/01/00000001/20110003/00000015/00000036/00000036_5.jpg	1
00000037	/articulo/01/00000001/20110003/00000015/00000037/00000037_1.jpg	/articulo/01/00000001/20110003/00000015/00000037/00000037_2.jpg	/articulo/01/00000001/20110003/00000015/00000037/00000037_3.jpg	/articulo/01/00000001/20110003/00000015/00000037/00000037_4.jpg	/articulo/01/00000001/20110003/00000015/00000037/00000037_5.jpg	1
00000038	/articulo/01/00000001/20110003/00000015/00000038/00000038_1.JPG	/articulo/01/00000001/20110003/00000015/00000038/00000038_2.JPG	/articulo/01/00000001/20110003/00000015/00000038/00000038_3.JPG	/articulo/01/00000001/20110003/00000015/00000038/00000038_4.JPG	/articulo/01/00000001/20110003/00000015/00000038/00000038_5.JPG	1
00000039	/articulo/01/00000001/20110003/00000015/00000039/00000039_1.JPG	/articulo/01/00000001/20110003/00000015/00000039/00000039_2.JPG	/articulo/01/00000001/20110003/00000015/00000039/00000039_3.JPG	/articulo/01/00000001/20110003/00000015/00000039/00000039_4.JPG	/articulo/01/00000001/20110003/00000015/00000039/00000039_5.JPG	1
00000040	/articulo/01/00000001/20110003/00000015/00000040/00000040_1.JPG	/articulo/01/00000001/20110003/00000015/00000040/00000040_2.JPG	/articulo/01/00000001/20110003/00000015/00000040/00000040_3.JPG	/articulo/01/00000001/20110003/00000015/00000040/00000040_4.JPG	/articulo/01/00000001/20110003/00000015/00000040/00000040_5.JPG	1
00000041	/articulo/01/00000001/20110003/00000015/00000041/00000041_1.JPG	/articulo/01/00000001/20110003/00000015/00000041/00000041_2.JPG	/articulo/01/00000001/20110003/00000015/00000041/00000041_3.JPG	/articulo/01/00000001/20110003/00000015/00000041/00000041_4.JPG	/articulo/01/00000001/20110003/00000015/00000041/00000041_5.JPG	1
00000083	/habitacion//00000013/00000083/00000083_1.JPG	/habitacion//00000013/00000083/00000083_2.JPG	/habitacion//00000013/00000083/00000083_3.JPG	/habitacion//00000013/00000083/00000083_4.JPG	/habitacion//00000013/00000083/00000083_5.JPG	1
00000042	/articulo/01/00000001/20110003/00000015/00000042/00000042_1.jpg	/articulo/01/00000001/20110003/00000015/00000042/00000042_2.jpg	/articulo/01/00000001/20110003/00000015/00000042/00000042_3.jpg	/articulo/01/00000001/20110003/00000015/00000042/00000042_4.jpg	/articulo/01/00000001/20110003/00000015/00000042/00000042_5.jpg	1
00000043	/promocion/00000002/AQP012012-630231/00000043/00000043_1.jpg	/promocion/00000002/AQP012012-630231/00000043/00000043_2.jpg	/promocion/00000002/AQP012012-630231/00000043/00000043_3.jpg	/promocion/00000002/AQP012012-630231/00000043/00000043_4.jpg	/promocion/00000002/AQP012012-630231/00000043/00000043_5.jpg	1
00000044	/tipohabitacion/01/02/00000044/00000044_1.JPG	/tipohabitacion/01/02/00000044/00000044_2.JPG	/tipohabitacion/01/02/00000044/00000044_3.JPG	/tipohabitacion/01/02/00000044/00000044_4.JPG	/tipohabitacion/01/02/00000044/00000044_5.JPG	1
00000086	/habitacion//00000001/00000086/00000086_1.jpg	/habitacion//00000001/00000086/00000086_2.jpg	/habitacion//00000001/00000086/00000086_3.jpg	/habitacion//00000001/00000086/00000086_4.jpg	/habitacion//00000001/00000086/00000086_5.jpg	1
00000049	/habitacion/01/00000009/00000049/00000049_1.JPG	/habitacion/01/00000009/00000049/00000049_2.JPG	/habitacion/01/00000009/00000049/00000049_3.JPG	/habitacion/01/00000009/00000049/00000049_4.JPG	/habitacion/01/00000009/00000049/00000049_5.JPG	1
00000095	/eventos//0000000005/00000095/00000095_1.jpg	/eventos//0000000005/00000095/00000095_2.jpg	/eventos//0000000005/00000095/00000095_3.jpg	/eventos//0000000005/00000095/00000095_4.jpg	/eventos//0000000005/00000095/00000095_5.jpg	1
00000050	/promocion/00000002/AQP22012-644039/00000050/00000050_1.jpg	/promocion/00000002/AQP22012-644039/00000050/00000050_2.jpg	/promocion/00000002/AQP22012-644039/00000050/00000050_3.jpg	/promocion/00000002/AQP22012-644039/00000050/00000050_4.jpg	/promocion/00000002/AQP22012-644039/00000050/00000050_5.jpg	1
00000098	/promocion/00000002/24-2012/00000098/00000098_1.	/promocion/00000002/24-2012/00000098/00000098_2.	/promocion/00000002/24-2012/00000098/00000098_3.	/promocion/00000002/24-2012/00000098/00000098_4.	/promocion/00000002/24-2012/00000098/00000098_5.	1
00000053	/promocion/00000002/TRUJ2012-947586/00000053/00000053_1.jpg	/promocion/00000002/TRUJ2012-947586/00000053/00000053_2.jpg	/promocion/00000002/TRUJ2012-947586/00000053/00000053_3.jpg	/promocion/00000002/TRUJ2012-947586/00000053/00000053_4.jpg	/promocion/00000002/TRUJ2012-947586/00000053/00000053_5.jpg	1
00000054	/promocion/00000002/AQP2012-291718/00000054/00000054_1.jpg	/promocion/00000002/AQP2012-291718/00000054/00000054_2.jpg	/promocion/00000002/AQP2012-291718/00000054/00000054_3.jpg	/promocion/00000002/AQP2012-291718/00000054/00000054_4.jpg	/promocion/00000002/AQP2012-291718/00000054/00000054_5.jpg	1
00000055	/promocion/00000002/AQP 22012-240275/00000055/00000055_1.jpg	/promocion/00000002/AQP 22012-240275/00000055/00000055_2.jpg	/promocion/00000002/AQP 22012-240275/00000055/00000055_3.jpg	/promocion/00000002/AQP 22012-240275/00000055/00000055_4.jpg	/promocion/00000002/AQP 22012-240275/00000055/00000055_5.jpg	1
00000056	/promocion/00000002/ADV2012-696091/00000056/00000056_1.jpg	/promocion/00000002/ADV2012-696091/00000056/00000056_2.jpg	/promocion/00000002/ADV2012-696091/00000056/00000056_3.jpg	/promocion/00000002/ADV2012-696091/00000056/00000056_4.jpg	/promocion/00000002/ADV2012-696091/00000056/00000056_5.jpg	1
00000057	/promocion/00000002/NBO2012-729398/00000057/00000057_1.jpg	/promocion/00000002/NBO2012-729398/00000057/00000057_2.jpg	/promocion/00000002/NBO2012-729398/00000057/00000057_3.jpg	/promocion/00000002/NBO2012-729398/00000057/00000057_4.jpg	/promocion/00000002/NBO2012-729398/00000057/00000057_5.jpg	1
00000058	/articulo/01/00000002/20110014/00000012/00000058/00000058_1.png	/articulo/01/00000002/20110014/00000012/00000058/00000058_2.png	/articulo/01/00000002/20110014/00000012/00000058/00000058_3.png	/articulo/01/00000002/20110014/00000012/00000058/00000058_4.png	/articulo/01/00000002/20110014/00000012/00000058/00000058_5.png	1
00000059	/articulo/01/00000003/20110014/00000012/00000059/00000059_1.png	/articulo/01/00000003/20110014/00000012/00000059/00000059_2.png	/articulo/01/00000003/20110014/00000012/00000059/00000059_3.png	/articulo/01/00000003/20110014/00000012/00000059/00000059_4.png	/articulo/01/00000003/20110014/00000012/00000059/00000059_5.png	1
00000062	/promocion/00000002/242012-924001/00000062/00000062_1.jpg	/promocion/00000002/242012-924001/00000062/00000062_2.jpg	/promocion/00000002/242012-924001/00000062/00000062_3.jpg	/promocion/00000002/242012-924001/00000062/00000062_4.jpg	/promocion/00000002/242012-924001/00000062/00000062_5.jpg	1
00000063	/promocion/00000002/242012-50268/00000063/00000063_1.jpg	/promocion/00000002/242012-50268/00000063/00000063_2.jpg	/promocion/00000002/242012-50268/00000063/00000063_3.jpg	/promocion/00000002/242012-50268/00000063/00000063_4.jpg	/promocion/00000002/242012-50268/00000063/00000063_5.jpg	1
00000089	/habitacion//00000052/00000089/00000089_1.jpg	/habitacion//00000052/00000089/00000089_2.jpg	/habitacion//00000052/00000089/00000089_3.jpg	/habitacion//00000052/00000089/00000089_4.jpg	/habitacion//00000052/00000089/00000089_5.jpg	1
00000091	/habitacion//00000081/00000091/00000091_1.jpg	/habitacion//00000081/00000091/00000091_2.jpg	/habitacion//00000081/00000091/00000091_3.jpg	/habitacion//00000081/00000091/00000091_4.jpg	/habitacion//00000081/00000091/00000091_5.jpg	1
00000094	/eventos//0000000004/00000094/00000094_1.jpg	/eventos//0000000004/00000094/00000094_2.jpg	/eventos//0000000004/00000094/00000094_3.jpg	/eventos//0000000004/00000094/00000094_4.jpg	/eventos//0000000004/00000094/00000094_5.jpg	1
00000096	/promocion/00000002/NBO-2012/00000096/00000096_1.jpg	/promocion/00000002/NBO-2012/00000096/00000096_2.jpg	/promocion/00000002/NBO-2012/00000096/00000096_3.jpg	/promocion/00000002/NBO-2012/00000096/00000096_4.jpg	/promocion/00000002/NBO-2012/00000096/00000096_5.jpg	1
00000099	/promocion/00000002/VISALIM-2012/00000099/00000099_1.jpg	/promocion/00000002/VISALIM-2012/00000099/00000099_2.jpg	/promocion/00000002/VISALIM-2012/00000099/00000099_3.jpg	/promocion/00000002/VISALIM-2012/00000099/00000099_4.jpg	/promocion/00000002/VISALIM-2012/00000099/00000099_5.jpg	1
00000100	/promocion/00000002/VISA2-2012/00000100/00000100_1.	/promocion/00000002/VISA2-2012/00000100/00000100_2.	/promocion/00000002/VISA2-2012/00000100/00000100_3.	/promocion/00000002/VISA2-2012/00000100/00000100_4.	/promocion/00000002/VISA2-2012/00000100/00000100_5.	1
00000069	/promocion/00000002/162012-592943/00000069/00000069_1.jpg	/promocion/00000002/162012-592943/00000069/00000069_2.jpg	/promocion/00000002/162012-592943/00000069/00000069_3.jpg	/promocion/00000002/162012-592943/00000069/00000069_4.jpg	/promocion/00000002/162012-592943/00000069/00000069_5.jpg	1
00000101	/tipohabitacion//01/00000101/00000101_1.jpg	/tipohabitacion//01/00000101/00000101_2.jpg	/tipohabitacion//01/00000101/00000101_3.jpg	/tipohabitacion//01/00000101/00000101_4.jpg	/tipohabitacion//01/00000101/00000101_5.jpg	1
00000072	/tipohabitacion//05/00000072/00000072_1.jpg	/tipohabitacion//05/00000072/00000072_2.jpg	/tipohabitacion//05/00000072/00000072_3.jpg	/tipohabitacion//05/00000072/00000072_4.jpg	/tipohabitacion//05/00000072/00000072_5.jpg	1
00000073	/tipohabitacion//04/00000073/00000073_1.JPG	/tipohabitacion//04/00000073/00000073_2.JPG	/tipohabitacion//04/00000073/00000073_3.JPG	/tipohabitacion//04/00000073/00000073_4.JPG	/tipohabitacion//04/00000073/00000073_5.JPG	1
00000106	/tipohabitacion/00000003/04/00000106/00000106_1.jpg	/tipohabitacion/00000003/04/00000106/00000106_2.jpg	/tipohabitacion/00000003/04/00000106/00000106_3.jpg	/tipohabitacion/00000003/04/00000106/00000106_4.jpg	/tipohabitacion/00000003/04/00000106/00000106_5.jpg	1
00000078	/tipohabitacion//01/00000078/00000078_1.jpg	/tipohabitacion//01/00000078/00000078_2.jpg	/tipohabitacion//01/00000078/00000078_3.jpg	/tipohabitacion//01/00000078/00000078_4.jpg	/tipohabitacion//01/00000078/00000078_5.jpg	1
00000079	/tipohabitacion//03/00000079/00000079_1.JPG	/tipohabitacion//03/00000079/00000079_2.JPG	/tipohabitacion//03/00000079/00000079_3.JPG	/tipohabitacion//03/00000079/00000079_4.JPG	/tipohabitacion//03/00000079/00000079_5.JPG	1
00000080	/tipohabitacion//02/00000080/00000080_1.jpg	/tipohabitacion//02/00000080/00000080_2.jpg	/tipohabitacion//02/00000080/00000080_3.jpg	/tipohabitacion//02/00000080/00000080_4.jpg	/tipohabitacion//02/00000080/00000080_5.jpg	1
00000111	/tipohabitacion/00000003/06/00000111/00000111_1.jpg	/tipohabitacion/00000003/06/00000111/00000111_2.jpg	/tipohabitacion/00000003/06/00000111/00000111_3.jpg	/tipohabitacion/00000003/06/00000111/00000111_4.jpg	/tipohabitacion/00000003/06/00000111/00000111_5.jpg	1
00000112	/tipohabitacion/00000003/05/00000112/00000112_1.jpg	/tipohabitacion/00000003/05/00000112/00000112_2.jpg	/tipohabitacion/00000003/05/00000112/00000112_3.jpg	/tipohabitacion/00000003/05/00000112/00000112_4.jpg	/tipohabitacion/00000003/05/00000112/00000112_5.jpg	1
00000113	/tipohabitacion/00000002/01/00000113/00000113_1.jpg	/tipohabitacion/00000002/01/00000113/00000113_2.jpg	/tipohabitacion/00000002/01/00000113/00000113_3.jpg	/tipohabitacion/00000002/01/00000113/00000113_4.jpg	/tipohabitacion/00000002/01/00000113/00000113_5.jpg	1
00000114	/tipohabitacion/00000002/02/00000114/00000114_1.jpg	/tipohabitacion/00000002/02/00000114/00000114_2.jpg	/tipohabitacion/00000002/02/00000114/00000114_3.jpg	/tipohabitacion/00000002/02/00000114/00000114_4.jpg	/tipohabitacion/00000002/02/00000114/00000114_5.jpg	1
00000116	/tipohabitacion/00000002/04/00000116/00000116_1.JPG	/tipohabitacion/00000002/04/00000116/00000116_2.JPG	/tipohabitacion/00000002/04/00000116/00000116_3.JPG	/tipohabitacion/00000002/04/00000116/00000116_4.JPG	/tipohabitacion/00000002/04/00000116/00000116_5.JPG	1
00000117	/tipohabitacion/00000003/01/00000117/00000117_1.jpg	/tipohabitacion/00000003/01/00000117/00000117_2.jpg	/tipohabitacion/00000003/01/00000117/00000117_3.jpg	/tipohabitacion/00000003/01/00000117/00000117_4.jpg	/tipohabitacion/00000003/01/00000117/00000117_5.jpg	1
00000118	/tipohabitacion/00000003/02/00000118/00000118_1.jpg	/tipohabitacion/00000003/02/00000118/00000118_2.jpg	/tipohabitacion/00000003/02/00000118/00000118_3.jpg	/tipohabitacion/00000003/02/00000118/00000118_4.jpg	/tipohabitacion/00000003/02/00000118/00000118_5.jpg	1
00000119	/tipohabitacion/00000003/03/00000119/00000119_1.jpg	/tipohabitacion/00000003/03/00000119/00000119_2.jpg	/tipohabitacion/00000003/03/00000119/00000119_3.jpg	/tipohabitacion/00000003/03/00000119/00000119_4.jpg	/tipohabitacion/00000003/03/00000119/00000119_5.jpg	1
00000120	/tipohabitacion/00000002/03/00000120/00000120_1.JPG	/tipohabitacion/00000002/03/00000120/00000120_2.JPG	/tipohabitacion/00000002/03/00000120/00000120_3.JPG	/tipohabitacion/00000002/03/00000120/00000120_4.JPG	/tipohabitacion/00000002/03/00000120/00000120_5.JPG	1
00000121	/tipohabitacion/00000002/15/00000121/00000121_1.jpg	/tipohabitacion/00000002/15/00000121/00000121_2.jpg	/tipohabitacion/00000002/15/00000121/00000121_3.jpg	/tipohabitacion/00000002/15/00000121/00000121_4.jpg	/tipohabitacion/00000002/15/00000121/00000121_5.jpg	1
00000122	/tipohabitacion/00000002/15/00000122/00000122_1.jpg	/tipohabitacion/00000002/15/00000122/00000122_2.jpg	/tipohabitacion/00000002/15/00000122/00000122_3.jpg	/tipohabitacion/00000002/15/00000122/00000122_4.jpg	/tipohabitacion/00000002/15/00000122/00000122_5.jpg	1
00000123	/tipohabitacion/00000002/01/00000123/00000123_1.jpg	/tipohabitacion/00000002/01/00000123/00000123_2.jpg	/tipohabitacion/00000002/01/00000123/00000123_3.jpg	/tipohabitacion/00000002/01/00000123/00000123_4.jpg	/tipohabitacion/00000002/01/00000123/00000123_5.jpg	1
\.


--
-- TOC entry 2561 (class 0 OID 24661)
-- Dependencies: 177
-- Data for Name: ht_cms_menu_interno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_menu_interno (cat_id, cat_orden) FROM stdin;
H	1
D	2
W	3
E	4
\.


--
-- TOC entry 2562 (class 0 OID 24664)
-- Dependencies: 178
-- Data for Name: ht_cms_meses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_meses (mes_id) FROM stdin;
01
02
03
04
05
06
07
08
09
10
11
12
\.


--
-- TOC entry 2563 (class 0 OID 24667)
-- Dependencies: 179
-- Data for Name: ht_cms_promo_imagen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_promo_imagen (pro_id, an_id, tih_id, su_id, im_id) FROM stdin;
24	2012	01	00000002	00000063
16	2012	03	00000002	00000069
NBO	2012	04	00000002	00000096
16	2012	02	00000003	00000097
24	2012	02	00000002	00000098
VISALIM	2012	02	00000002	00000099
VISA2	2012	02	00000002	00000100
\.


--
-- TOC entry 2564 (class 0 OID 24670)
-- Dependencies: 180
-- Data for Name: ht_cms_promocion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_promocion (an_id, pro_id, ho_id, tih_id, pro_fecalta, pro_fecini, pro_fecfin, pro_dscto, pro_estado) FROM stdin;
2012	16	01	02	2012-11-05	2013-01-01	2013-01-31	20	S
2012	24	01	02	2012-11-05	2013-01-01	2013-01-31	10	N
2012	NBO	01	04	2012-10-02	2013-01-01	2013-01-31	15	S
2013	2013	01	01	2013-01-08	2013-01-01	2013-02-18	30	S
2012	VISALIM	01	02	2012-12-20	2013-01-01	2013-03-07	10	S
2012	VISA2	01	02	2012-12-20	2013-01-01	2013-02-27	10	S
\.


--
-- TOC entry 2565 (class 0 OID 24679)
-- Dependencies: 181
-- Data for Name: ht_cms_promocion_sucursal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_promocion_sucursal (pro_id, an_id, tih_id, su_id) FROM stdin;
NBO	2012	04	00000002
24	2012	01	00000002
16	2012	03	00000002
16	2012	02	00000003
24	2012	02	00000002
VISALIM	2012	02	00000002
VISA2	2012	02	00000002
2013	2013	01	00000002
\.


--
-- TOC entry 2566 (class 0 OID 24682)
-- Dependencies: 182
-- Data for Name: ht_cms_public_imagen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_public_imagen (pub_id, im_id) FROM stdin;
0000000001	00000001
0000000002	00000002
0000000003	00000093
0000000004	00000094
0000000005	00000095
\.


--
-- TOC entry 2567 (class 0 OID 24685)
-- Dependencies: 183
-- Data for Name: ht_cms_publicacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_publicacion (pub_id, pub_estado, pub_tipo, pub_fecha) FROM stdin;
0000000006	1	A	2012-12-22
0000000002	1	N	2012-09-12
0000000001	1	N	2012-09-12
0000000003	1	N	2012-12-20
0000000004	1	N	2012-12-20
0000000005	1	E	2012-12-20
\.


--
-- TOC entry 2568 (class 0 OID 24688)
-- Dependencies: 184
-- Data for Name: ht_cms_sucursal_categoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_sucursal_categoria (su_id, cat_id, cat_orden) FROM stdin;
00000002	20110003	\N
00000002	20110008	\N
00000002	20110009	\N
00000002	20110001	\N
00000003	20110003	\N
00000003	20110009	\N
00000001	20110014	\N
00000002	20110014	\N
00000003	20110014	\N
00000001	20110011	1
00000001	20110012	2
00000001	20110013	3
00000001	20110010	4
00000001	20110009	\N
00000001	20110003	\N
00000001	20110001	\N
00000003	20110011	1
00000002	20110010	4
00000003	20110012	2
00000002	20110012	2
00000002	20110011	1
00000002	20110013	3
00000003	20110013	3
00000003	20110010	4
\.


--
-- TOC entry 2569 (class 0 OID 24691)
-- Dependencies: 185
-- Data for Name: ht_cms_sucursal_menu_interno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_cms_sucursal_menu_interno (cat_id, su_id, tih_id, cat_orden) FROM stdin;
D	00000002	07	\N
D	00000002	08	\N
D	00000002	09	\N
D	00000002	10	\N
W	00000002	11	\N
W	00000002	12	\N
W	00000002	13	\N
W	00000002	14	\N
E	00000002	15	\N
D	00000003	07	\N
\.


--
-- TOC entry 2570 (class 0 OID 24694)
-- Dependencies: 186
-- Data for Name: ht_hb_accesorio_sucursal_tiphab; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_accesorio_sucursal_tiphab (ac_id, tih_id, su_id) FROM stdin;
0001	02	00000002
0001	03	00000002
0002	02	00000002
0002	03	00000002
0002	04	00000002
0002	01	00000002
0003	01	00000002
0004	01	00000002
0005	01	00000002
0006	01	00000002
0007	01	00000002
0008	01	00000002
0009	01	00000002
0012	01	00000002
0003	02	00000002
0004	02	00000002
0005	02	00000002
0006	02	00000002
0007	02	00000002
0008	02	00000002
0009	02	00000002
0010	02	00000002
0011	02	00000002
0012	02	00000002
0014	02	00000002
0016	02	00000002
0017	03	00000002
0017	03	00000003
0018	03	00000003
0010	01	00000002
0010	06	00000002
0010	03	00000002
0010	04	00000002
0010	05	00000003
0010	06	00000003
0010	03	00000003
0010	04	00000003
0010	02	00000003
0011	01	00000003
0004	01	00000003
0005	01	00000003
\.


--
-- TOC entry 2571 (class 0 OID 24697)
-- Dependencies: 187
-- Data for Name: ht_hb_accesorios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_accesorios (ac_id, ac_estado, ac_precio, ac_tipo) FROM stdin;
0001	1	\N	\N
0003	1	\N	\N
0004	1	\N	\N
0005	1	\N	\N
0006	1	\N	\N
0007	1	\N	\N
0008	1	\N	\N
0009	1	\N	\N
0011	1	\N	\N
0013	1	\N	\N
0014	1	\N	\N
0015	1	\N	\N
0016	1	\N	\N
0010	1	20.00	\N
0018	1	5.00	\N
0002	1	\N	\N
0017	\N	15.00	\N
0012	\N	\N	\N
\.


--
-- TOC entry 2572 (class 0 OID 24700)
-- Dependencies: 188
-- Data for Name: ht_hb_habitacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_habitacion (hb_id, tih_id, su_id, pi_id, hb_numero, hb_precio, hb_estado) FROM stdin;
00000001	01	00000002	0001	1	\N	1
00000002	01	00000002	0001	2	\N	1
00000003	01	00000002	0001	3	\N	1
00000004	01	00000002	0001	4	\N	1
00000005	01	00000002	0001	5	\N	1
00000006	01	00000002	0001	6	\N	1
00000007	01	00000002	0001	7	\N	1
00000009	04	00000002	0001	1	\N	1
00000010	03	00000002	0001	1	\N	1
00000011	03	00000002	0001	2	\N	1
00000012	03	00000002	0001	3	\N	1
00000013	05	00000002	0001	1	\N	1
00000014	05	00000002	0001	2	\N	1
00000015	05	00000002	0001	3	\N	1
00000016	05	00000002	0001	4	\N	1
00000017	02	00000002	0001	1	\N	1
00000018	02	00000002	0001	2	\N	1
00000019	02	00000002	0001	3	\N	1
00000020	02	00000002	0001	4	\N	1
00000021	02	00000002	0001	5	\N	1
00000022	02	00000002	0001	6	\N	1
00000023	02	00000002	0001	7	\N	1
00000024	02	00000002	0001	8	\N	1
00000025	02	00000002	0001	9	\N	1
00000026	02	00000002	0001	10	\N	1
00000027	02	00000002	0001	11	\N	1
00000028	02	00000002	0001	12	\N	1
00000029	02	00000002	0001	13	\N	1
00000030	02	00000002	0001	14	\N	1
00000031	02	00000002	0001	15	\N	1
00000032	02	00000002	0001	16	\N	1
00000033	02	00000002	0001	17	\N	1
00000034	02	00000002	0001	18	\N	1
00000035	02	00000002	0001	19	\N	1
00000036	02	00000002	0001	20	\N	1
00000037	02	00000002	0001	21	\N	1
00000038	02	00000002	0001	22	\N	1
00000039	02	00000002	0001	23	\N	1
00000040	02	00000002	0001	24	\N	1
00000041	02	00000002	0001	25	\N	1
00000042	01	00000003	0002	1	\N	1
00000043	01	00000003	0002	2	\N	1
00000044	01	00000003	0002	3	\N	1
00000045	01	00000003	0002	4	\N	1
00000046	01	00000003	0002	5	\N	1
00000047	01	00000003	0002	6	\N	1
00000048	01	00000003	0002	7	\N	1
00000049	02	00000003	0002	1	\N	1
00000050	02	00000003	0002	2	\N	1
00000051	02	00000003	0002	3	\N	1
00000052	03	00000003	0002	1	\N	1
00000053	04	00000003	0002	1	\N	1
00000054	04	00000003	0002	2	\N	1
00000055	04	00000003	0002	3	\N	1
00000056	04	00000003	0002	4	\N	1
00000057	04	00000003	0002	5	\N	1
00000058	05	00000003	0002	1	\N	1
00000059	05	00000003	0002	2	\N	1
00000060	06	00000003	0002	1	\N	1
00000076	01	00000002	0001	9	\N	1
00000077	06	00000002	0001	1	\N	1
00000078	06	00000002	0001	2	\N	1
00000079	06	00000002	0001	3	\N	1
00000080	06	00000002	0001	4	\N	1
00000081	06	00000003	0002	2	\N	1
00000082	04	00000003	0002	2	\N	1
00000083	04	00000003	0002	3	\N	1
\.


--
-- TOC entry 2573 (class 0 OID 24703)
-- Dependencies: 189
-- Data for Name: ht_hb_habitacion_accesorios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_habitacion_accesorios (hb_id, ac_id, hba_id) FROM stdin;
\.


--
-- TOC entry 2574 (class 0 OID 24706)
-- Dependencies: 190
-- Data for Name: ht_hb_habitacion_imagen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_habitacion_imagen (hb_id, im_id) FROM stdin;
00000017	00000085
00000001	00000086
00000052	00000089
00000053	00000090
00000081	00000091
00000042	00000092
00000009	00000049
00000010	00000082
00000013	00000083
\.


--
-- TOC entry 2575 (class 0 OID 24709)
-- Dependencies: 191
-- Data for Name: ht_hb_habitacion_tarifa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_habitacion_tarifa (su_id, tih_id, hb_capacidad, ta_id, hb_precio) FROM stdin;
00000002	01	1	P	76.00
00000003	04	2	D	124.00
00000003	04	2	F	115.00
00000003	03	2	D	107.00
00000003	03	2	F	99.00
00000003	06	2	D	124.00
00000003	06	2	F	115.00
00000003	01	2	D	75.00
00000003	01	2	F	68.00
00000003	02	2	D	90.00
00000003	02	2	F	84.00
00000002	01	1	D	106.00
00000002	01	1	F	99.00
00000003	03	1	D	107.00
00000003	03	1	F	99.00
00000003	06	1	D	124.00
00000003	06	1	F	115.00
00000003	01	1	D	75.00
00000002	01	2	D	106.00
00000002	01	2	F	99.00
00000002	02	3	D	149.00
00000002	02	3	F	140.00
00000002	03	2	D	153.00
00000002	03	2	F	141.00
00000002	03	3	D	173.00
00000002	03	3	F	161.00
00000002	04	2	D	177.00
00000002	04	2	F	164.00
00000003	02	3	D	110.00
00000003	02	3	F	104.00
00000003	03	3	D	127.00
00000003	04	3	D	144.00
00000003	04	3	F	135.00
00000003	06	3	D	144.00
00000003	06	3	F	135.00
00000002	04	1	D	177.00
00000002	04	1	F	164.00
00000002	03	1	D	153.00
00000002	03	1	F	141.00
00000003	01	1	F	68.00
00000003	02	1	D	90.00
00000003	02	1	F	84.00
00000003	04	1	D	124.00
00000003	04	1	F	115.00
00000002	02	1	D	135.00
00000002	02	1	F	110.00
00000002	02	2	D	129.00
00000002	02	2	F	120.00
00000002	04	3	D	197.00
00000002	04	3	F	184.00
00000002	06	3	D	149.00
00000002	06	3	F	140.00
00000002	06	2	D	129.00
00000002	06	2	F	120.00
00000002	06	1	D	129.00
00000002	06	1	F	120.00
00000003	05	2	D	124.00
00000003	05	2	F	115.00
00000003	05	3	D	144.00
00000003	05	3	F	135.00
00000003	05	1	D	129.00
00000003	05	1	F	120.00
\.


--
-- TOC entry 2576 (class 0 OID 24712)
-- Dependencies: 192
-- Data for Name: ht_hb_hotel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_hotel (ho_id, ho_ruc, ho_estado, ho_nombre) FROM stdin;
01	88457486274	1	PQ Hotels
\.


--
-- TOC entry 2577 (class 0 OID 24715)
-- Dependencies: 193
-- Data for Name: ht_hb_idioma_accesorios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_accesorios (id_id, ac_id, ac_desc) FROM stdin;
EN	0001	SOFA CAMA
EN	0002	cuna
ES	0001	Modernos Kitchenette, equipados con menaje y horno microondas digital
ES	0003	Escritorio y silla 
EN	0003	Escritorio y silla 
ES	0004	Teléfono DDI, DDN
EN	0004	Teléfono DDI, DDN
ES	0005	Caja de seguridad digital laptop size
EN	0005	Caja de seguridad digital laptop size
ES	0006	Secadora de cabello
EN	0006	Secadora de cabello
ES	0007	110 / 220 voltios       
EN	0007	110 / 220 voltios       
ES	0008	Plancha y planchador   
EN	0008	Plancha y planchador   
ES	0009	Control de temperatura individual para aire acondicionado y calefacción      
EN	0009	Control de temperatura individual para aire acondicionado y calefacción      
EN	0010	Sofá cama desplegable
ES	0011	Sala de estar
EN	0011	Sala de estar
EN	0012	Plancha y planchador        
ES	0013	Tina de hidromasaje 
EN	0013	Tina de hidromasaje 
ES	0014	Servicio de canapes
EN	0014	Servicio de canapes
ES	0015	Tina de hidromasaje Jacuzzi
EN	0015	Tina de hidromasaje Jacuzzi
ES	0016	Degustación de vino y pisco de cortesía lunes a viernes de 4 a 6pm. 
EN	0016	Degustación de vino y pisco de cortesía lunes a viernes de 4 a 6pm. 
ES	0010	Sofá cama desplegable
EN	0017	laptop 
ES	0018	Termo electrico
EN	0018	Termo electrico
ES	0002	Internet Wi-Fi complementario
ES	0017	laptop 
ES	0012	Plancha y planchador        
\.


--
-- TOC entry 2578 (class 0 OID 24718)
-- Dependencies: 194
-- Data for Name: ht_hb_idioma_habitacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_habitacion (hb_id, id_id, hb_desc) FROM stdin;
00000001	ES	DRQ 1
00000001	EN	DRQ 1
00000002	ES	DRQ 2
00000002	EN	DRQ 2
00000003	ES	DRQ 3
00000003	EN	DRQ 3
00000004	ES	DRQ 4
00000004	EN	DRQ 4
00000005	ES	DRQ 5
00000005	EN	DRQ 5
00000006	ES	DRQ 6
00000006	EN	DRQ 6
00000007	ES	DRQ 7
00000007	EN	DRQ 7
00000009	ES	SSQ 1
00000009	EN	SSQ 1
00000010	ES	ESQ 1
00000010	EN	ESQ 1
00000011	ES	ESQ 2
00000011	EN	ESQ 2
00000012	ES	ESQ 3
00000012	EN	ESQ 3
00000013	ES	DST 1
00000013	EN	DST 1
00000014	ES	DST 2
00000014	EN	DST 2
00000015	ES	DST 3
00000015	EN	DST 3
00000016	ES	DST 4
00000016	EN	DST 4
00000017	ES	DSQ 1
00000017	EN	DSQ 1
00000018	ES	DSQ 2
00000018	EN	DSQ 2
00000019	ES	DSQ 3
00000019	EN	DSQ 3
00000020	ES	DSQ 4
00000020	EN	DSQ 4
00000021	ES	DSQ 5
00000021	EN	DSQ 5
00000022	ES	DSQ 6
00000022	EN	DSQ 6
00000023	ES	DSQ 7
00000023	EN	DSQ 7
00000024	ES	DSQ 8
00000024	EN	DSQ 8
00000025	ES	DSQ 9
00000025	EN	DSQ 9
00000026	ES	DSQ 10
00000026	EN	DSQ 10
00000027	ES	DSQ 11
00000027	EN	DSQ 11
00000028	ES	DSQ 12
00000028	EN	DSQ 12
00000029	ES	DSQ 13
00000029	EN	DSQ 13
00000030	ES	DSQ 14
00000030	EN	DSQ 14
00000031	ES	DSQ 15
00000031	EN	DSQ 15
00000032	ES	DSQ 16
00000032	EN	DSQ 16
00000033	ES	DSQ 17
00000033	EN	DSQ 17
00000034	ES	DSQ 18
00000034	EN	DSQ 18
00000035	ES	DSQ 19
00000035	EN	DSQ 19
00000036	ES	DSQ 20
00000036	EN	DSQ 20
00000037	ES	DSQ 21
00000037	EN	DSQ 21
00000038	ES	DSQ 22
00000038	EN	DSQ 22
00000039	ES	DSQ 23
00000039	EN	DSQ 23
00000040	ES	DSQ 24
00000040	EN	DSQ 24
00000041	ES	DSQ 25
00000041	EN	DSQ 25
00000042	ES	O 1
00000042	EN	O 1
00000043	ES	O 2
00000043	EN	O 2
00000044	ES	O 3
00000044	EN	O 3
00000045	ES	O 4
00000045	EN	O 4
00000046	ES	O 5
00000046	EN	O 5
00000047	ES	O 6
00000047	EN	O 6
00000048	ES	O 7
00000048	EN	O 7
00000049	ES	. 1
00000049	EN	. 1
00000050	ES	. 2
00000050	EN	. 2
00000051	ES	. 3
00000051	EN	. 3
00000052	ES	. 1
00000052	EN	. 1
00000053	ES	. 1
00000053	EN	. 1
00000054	ES	. 2
00000054	EN	. 2
00000055	ES	. 3
00000055	EN	. 3
00000056	ES	. 4
00000056	EN	. 4
00000057	ES	. 5
00000057	EN	. 5
00000058	ES	. 1
00000058	EN	. 1
00000059	ES	. 2
00000059	EN	. 2
00000060	ES	. 1
00000060	EN	. 1
00000076	ES	1 9
00000076	EN	1 9
00000077	ES	DST
00000077	EN	DST
00000078	ES	DST 2
00000078	EN	DST 2
00000079	ES	DST 3
00000079	EN	DST 3
00000080	ES	DST 4
00000080	EN	DST 4
00000081	ES	DST 2
00000081	EN	DST 2
00000082	ES	Habitaciòn Nº 2 2
00000082	EN	Habitaciòn Nº 2 2
00000083	ES	Habitaciòn Nº 2 3
00000083	EN	Habitaciòn Nº 2 3
\.


--
-- TOC entry 2579 (class 0 OID 24724)
-- Dependencies: 195
-- Data for Name: ht_hb_idioma_piso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_piso (pi_id, id_id, pi_desc) FROM stdin;
0001	ES	1
0001	EN	1
0002	ES	1
0002	EN	1
\.


--
-- TOC entry 2580 (class 0 OID 24727)
-- Dependencies: 196
-- Data for Name: ht_hb_idioma_suc_tiphab; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_suc_tiphab (tih_id, su_id, id_id, tih_desc) FROM stdin;
06	00000002	ES	Habitación independiente con aire acondicionado, calefacción, TV de plasma y baño privado.\n<p><strong><strong></strong></strong>Cuenta con:Caja fuerte, \nAire acondicionado, \nEscritorio, \nZona de estar, \nCalefacción, \nDucha, \nBañera, \nSecador de pelo, \nArtículos de aseo gratuitos, \nAseo, \nBaño, \nTeléfono, \nRadio , \nReproductor de DVD, \nReproductor de CD, \nCanales por cable, \nTV de pantalla plana, \nMinibar, \n<span class="jq_tooltip" style="cursor:help;border-bottom:1px dotted #003580;" rel="180">Zona de cocina</span>, \nMicroondas, \nCafetera, \nServicio de despertador, Reloj despertador<strong>.&nbsp; <br></strong></p><p><strong></strong>Area:30 m² / Tipo de cama: 2&nbsp;<span class="jq_tooltip rt_detail_bed_info" rel="180">Individual(es)</span></p><div class="info"> \n</div>
02	00000002	EN	comment<br>
01	00000002	EN	okas okas okas jajaja...<br>
01	00000002	ES	okas okas<br>
\.


--
-- TOC entry 2581 (class 0 OID 24733)
-- Dependencies: 197
-- Data for Name: ht_hb_idioma_tarifa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_tarifa (ta_id, id_id, ta_desc) FROM stdin;
F	ES	Sab-Dom
D	ES	Lun-Vie
D	EN	Mon-Fri
F	EN	Sat-Sun
P	ES	Promo
P	EN	Promo
Y	ES	FIN DE AÑO
Y	EN	FIN DE AÑO
\.


--
-- TOC entry 2582 (class 0 OID 24736)
-- Dependencies: 198
-- Data for Name: ht_hb_idioma_tipo_habitacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_idioma_tipo_habitacion (tih_id, id_id, tih_desc, tih_coment, tih_abr) FROM stdin;
07	EN	qp Cafe Lounge	<div>Inicie su día disfrutando de nuestro variado y personalizado desayuno buffet y durante su almuerzo y/o cena podrá descubrir sabores con la personalidad propia reflejada en cada uno de nuestros destinos.</div><div>El elegante y moderno ambiente lounge le permitirá disfrutar de un día ideal, en nuestra cocina encontrara la armonía entre lo clásico y moderno buscando satisfacer los más exquisitos paladares</div>	
07	ES	qp Cafe Lounge	Inicie su día disfrutando de nuestro variado y personalizado desayuno buffet y durante su almuerzo y/o cena podrá descubrir sabores con la personalidad propia reflejada en cada uno de nuestros destinos.<br><br>El elegante y moderno ambiente lounge le permitirá disfrutar de un día ideal, en nuestra cocina encontrara la armonía entre lo clásico y moderno buscando satisfacer los más exquisitos paladares.<br><br><p style="text-align: right;">Horario todos los días de 07:00 a 23:30</p>	
08	EN	360° Lounge	Visita\n360°Lounge, el espacio ideal en el cual podrás abrir tu mente y sentirte\nvivo mientras disfrutas de nuestros cócteles o simplemente contemplas la\nespectacular vista de la ciudad desde la terraza. Ideal para trabajar y\ndisfrutar, 360°Lounge le ofrece todo lo que necesita para hacer que su\nnegociación sea un éxito, su reunión memorable y su evento especial simplemente\nespectacular. Con una actitud creativa y un diseño innovador, el\n360°Lounge será una bocanada de aire fresco para sus invitados. \nHorario Lunes a Jueves de 17:00 a 00:00; Viernes y Sábado 17:00 a 02:30	
08	ES	360° Lounge	El espacio ideal en el cual podrás abrir tu mente y sentirte\nvivo mientras disfrutas de nuestros cócteles o simplemente contemplas la\nespectacular vista de la ciudad desde la terraza. Ideal para trabajar y\ndisfrutar.<br><br>360°Lounge le ofrece todo lo que necesita para hacer que su\nnegociación sea un éxito, su reunión memorable y su evento especial simplemente\nespectacular. Con una actitud creativa y un diseño innovador, el\n360°Lounge será una bocanada de aire fresco para sus invitados.<br><br><p style="text-align: right;">Horario Lunes a Jueves de 17:00 a 00:00; Viernes y Sábado 17:00 a 02:30</p>	
09	EN	Gimnasio	Relájese y ejercítese en nuestro gimnasio\nmientras disfruta de su programa favorito, qp gym ubicado en el segundo piso le\nproporcionara un ambiente privado en el espacio y con la tranquilidad que\nestaba buscando.\nHorario: Todos los días 06:00 a 22:00	
12	EN	Centro de Negocios	Nuestro completo centro de negocios le ofrecerá una gran variedad de servicios para así satisfacer todas las necesidades que requiere el ejecutivo de hoy, tenga la seguridad que estará conectado con su oficina mientras se encuentre lejos. Contamos con un servicio completo de impresiones, fotocopias y fax, además de autoservicio de internet. Cualquiera sea su evento, la atención al detalle y profesionalismo harán de este todo un éxito. Eche una mirada a los servicios que ofrecemos en nuestro centro de negocios.	
12	ES	Centro de Negocios	Nuestro completo centro de negocios le ofrecerá una gran variedad de servicios para así satisfacer todas las necesidades que requiere el ejecutivo de hoy, tenga la seguridad que estará conectado con su oficina mientras se encuentre lejos. Contamos con un servicio completo de impresiones, fotocopias y fax, además de autoservicio de internet. Cualquiera sea su evento, la atención al detalle y profesionalismo harán de este todo un éxito. Eche una mirada a los servicios que ofrecemos en nuestro centro de negocios.	
09	ES	Gimnasio	Relájese y ejercítese en nuestro gimnasio\nmientras disfruta de su programa favorito, qp gym ubicado en el segundo piso le\nproporcionara un ambiente privado en el espacio y con la tranquilidad que\nestaba buscando.<br><br><br><p style="text-align: right;">Horario: Todos los días 06:00 a 22:00</p>	
10	ES	Piscina	Nuestra piscina con hidromasajes y caída de\nagua ubicada en el primer piso le ofrecerá el espacio perfecto para\ndespreocuparse, o simplemente descansar en la terraza al aire libre después de\nhaber disfrutado de un momento de relax en el agua o visitado nuestro gimnasio.&nbsp;<br><br><p style="text-align: right;">Horario: Todos los días 06:00 a 22:00</p>	
10	EN	Piscina	Nuestra piscina con hidromasajes y caída de\nagua ubicada en el primer piso le ofrecerá el espacio perfecto para\ndespreocuparse, o simplemente descansar en la terraza al aire libre después de\nhaber disfrutado de un momento de relax en el agua o visitado nuestro gimnasio.&nbsp;<br><br><p style="text-align: right;">Horario: Todos los días 06:00 a 22:00</p>	
11	EN	Oficinas temporales	Una oficina moderna y flexible que se adaptara\na su forma de trabajar, a sus objetivos de negocios y a su presupuesto. \nNegocie con acceso gratuito a Internet y disfrute de café, té y agua de cortesía.	
11	ES	Oficinas temporales	Una oficina moderna y flexible que se adaptara\na su forma de trabajar, a sus objetivos de negocios y a su presupuesto. \nNegocie con acceso gratuito a Internet y disfrute de café, té y agua de cortesía.	
13	EN	Directorio		
13	ES	Sala Caral		
14	EN	Directorio		
14	ES	Sala de Directorio		
15	ES	qp Store		
15	EN	qp Store		
01	ES	Deluxe Room	Nuestras 8 habitaciones\nestándar de 19 m2 aprox. están elegantemente diseñadas y equipadas con armarios\na medida, su mobiliario contemporáneo acentúa un equilibrio armonioso entre un\ndiseño moderno y detalles tradicionales. Esta habitación ofrece grandes ventanas\ncon vista urbana, área de trabajo, moderno baño&nbsp; y mucho más para hacer de\néste, un lugar de calidad donde descansar. <br>	DRQ
06	ES	Deluxe Suite Twin		SST
05	ES	Superior Suite Twin		SST
02	EN	Deluxe Suite		DSQ
03	EN	Superior Suite		SSQ
04	EN	Superior Suite		SSQ
05	EN	Deluxe Suite Twin		DST
06	EN	Superior Suite Twin		SST
02	ES	Deluxe Suite	Contamos con 25 habitaciones de 30m2. Le ofrecerán un ambiente de descanso diseñado especialmente para usted, junto a un amplio espacio lleno de comodidad para su estancia donde encontrará la practicidad de un sofá cama, área de trabajo, un moderno y atractivo kitchenette. Esta Suite cuenta con una &nbsp;cama Queen cubierta con lujosos duvets de plumas de ganso; sin olvidarnos del baño que está completamente equipado para pasar sus largas estadías. Además, se podrá apreciar toda la vista urbana de Miraflores ya que cuenta con grandes ventanas. Esta habitación está diseñada especialmente para el descanso y el comfort del huésped.	DSQ
01	EN	Deluxe room	\N	DRQ
03	ES	Executive Suite	Nuestras 3 habitaciones Ejecutivas de 35 m2 aprox. Cuentan con una infraestructura minimalista y colores cálidos donde se combinan estos elegantes diseños haciendo de este un ambiente acogedor, donde podrá encontrar una cama Queen Size así como también un armario amplio con caja fuerte en su interior. Con un diseño y mobiliario moderno no abandona el lado tradicional, donde encontrará una amplia sala de estar con la facilidad de un sofá cama, un mini desk, tv en la sala y dormitorio; sumándole a todo esto un kitchenette completamente equipado como si usted estuviera en casa. El baño está provisto de una elegante tina de hidromasajes y amenities haciendo de su estadía más placentera.	ESQ
04	ES	Superior Suite	Nuestra habitación de 38 m2 aprox cuenta con una elegante decoración y mobiliario que están provistos de detalles armoniosos exclusivos para noches de bodas y momentos especiales debido al amplio jacuzzi con hidromasajes que se encuentra alojado en una cúpula muy aparte del lujoso baño, el cual está equipado con amenities. La sala cuenta con un sofá cama, área de trabajo y un moderno kitchenette. La cama Queen Size le ofrecerá una estadía de disfrute y comfort.	SSQ
\.


--
-- TOC entry 2583 (class 0 OID 24742)
-- Dependencies: 199
-- Data for Name: ht_hb_piso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_piso (pi_id, su_id, pi_numero) FROM stdin;
0001	00000002	1
0002	00000003	1
\.


--
-- TOC entry 2584 (class 0 OID 24748)
-- Dependencies: 200
-- Data for Name: ht_hb_sucursal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_sucursal (su_id, ho_id, ub_id, su_nombre, su_estado, su_telefono1, su_telefono2, su_ruc, su_direccion, su_desc, su_idcpy, su_portal, su_maestra, su_reserva) FROM stdin;
00000001	01	150101	select destination	1	962728856	\N	88456231457	888	<span class=\\"welcome-letra\\" style=\\"font-size: 13px; \\">welcome to!</span><br>\n<span  class=\\"welcome-letra2\\" >\nqp Hotels\n</span><br>\n<font size=\\"2\\">\nA&nbsp;</font><span class=\\"color-p\\" style=\\"font-size: 13px; \\"><font color=\\"#ff6600\\">q</font></span><font size=\\"2\\">uality&nbsp;</font><font style=\\"font-size: 13px; \\" color=\\"#ff6600\\">p</font><font size=\\"2\\">lace to stay.</font><br><font color=\\"#3366ff\\" size=\\"2\\"></font>\n	\N	S	\N	\N
00000003	01	040101	Arequipa 	1	54-612-000	\N	20515298127	Calle Villalba 305, Arequipa, Peru	<p class="welcome-letra" >\nwelcome to!</p>\n<p class="welcome-letra2" >\nqp Hotels\n</p>\n<p class="welcome-letra3" >\nArequipa\n</p>\n<p class="welcome-letra2" >\n****\n</p>	00000002	N	\N	qp Hotels AREQUIPA
00000002	01	150101	Lima 	1	511-319 2929	511-719-3888	23945822	Av. Jorge Chavez 206, Miraflores, Lima	<p class="welcome-letra" >\nwelcome to!</p>\n<p class="welcome-letra2" >\nqp Hotels\n</p>\n<p class="welcome-letra3" >\nLima\n</p>\n<p class="welcome-letra2" >\n****\n</p>	\N	N	S	qp Hotels LIMA
\.


--
-- TOC entry 2585 (class 0 OID 24755)
-- Dependencies: 201
-- Data for Name: ht_hb_sucursal_tipo_habitacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_sucursal_tipo_habitacion (tih_id, su_id, tih_stock, tih_state) FROM stdin;
02	00000002	\N	1
03	00000002	\N	1
04	00000002	\N	1
01	00000003	\N	1
02	00000003	\N	1
03	00000003	\N	1
04	00000003	\N	1
05	00000003	\N	1
06	00000003	\N	1
06	00000002	\N	0
01	00000002	\N	0
\.


--
-- TOC entry 2586 (class 0 OID 24758)
-- Dependencies: 202
-- Data for Name: ht_hb_tarifa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_tarifa (ta_id) FROM stdin;
F
D
P
Y
\.


--
-- TOC entry 2587 (class 0 OID 24761)
-- Dependencies: 203
-- Data for Name: ht_hb_tipo_habitacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_tipo_habitacion (tih_id, ht_tih_id, tih_estado, cat_id, tih_orden) FROM stdin;
14	\N	1	W	3
15	\N	1	E	1
01	\N	1	H	1
07	\N	1	D	1
08	\N	1	D	2
02	\N	1	H	2
09	\N	1	D	3
10	\N	1	D	4
12	\N	1	W	1
11	\N	1	W	2
13	\N	1	W	4
03	\N	1	H	4
06	\N	1	H	3
05	\N	1	H	6
04	\N	1	H	5
\.


--
-- TOC entry 2588 (class 0 OID 24764)
-- Dependencies: 204
-- Data for Name: ht_hb_tipo_habitacion_imagen; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_hb_tipo_habitacion_imagen (tih_id, im_id, su_id) FROM stdin;
07	00000010	\N
07	00000011	\N
08	00000014	\N
08	00000015	\N
08	00000016	\N
08	00000017	\N
07	00000018	\N
07	00000019	\N
07	00000020	\N
10	00000022	\N
10	00000023	\N
09	00000024	\N
09	00000025	\N
09	00000026	\N
08	00000027	\N
11	00000029	\N
14	00000030	\N
13	00000031	\N
12	00000032	\N
15	00000033	\N
02	00000044	\N
05	00000072	\N
04	00000073	\N
01	00000078	\N
03	00000079	\N
02	00000080	\N
06	00000087	\N
01	00000101	\N
06	00000111	00000003
05	00000112	00000003
01	00000113	00000002
04	00000106	00000003
02	00000114	00000002
04	00000116	00000002
01	00000117	00000003
02	00000118	00000003
03	00000119	00000003
03	00000120	00000002
15	00000121	00000002
15	00000122	00000002
01	00000123	00000002
\.


--
-- TOC entry 2589 (class 0 OID 24767)
-- Dependencies: 205
-- Data for Name: ht_res_anio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_anio (an_id, an_abr) FROM stdin;
2011	2011
2012	2012
2013	2013
2014	2014
\.


--
-- TOC entry 2590 (class 0 OID 24770)
-- Dependencies: 206
-- Data for Name: ht_res_bonos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_bonos (bo_id, bo_dolar, bo_puntos) FROM stdin;
\.


--
-- TOC entry 2591 (class 0 OID 24773)
-- Dependencies: 207
-- Data for Name: ht_res_cambio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_cambio (cam_fecha, cam_sol, cam_dol) FROM stdin;
\.


--
-- TOC entry 2592 (class 0 OID 24776)
-- Dependencies: 208
-- Data for Name: ht_res_idioma_anio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_idioma_anio (id_id, an_id, an_desc) FROM stdin;
ES	2011	Año 2011
ES	2012	Año 2012
ES	2013	Año 2013
ES	2014	Año 2014
\.


--
-- TOC entry 2593 (class 0 OID 24782)
-- Dependencies: 209
-- Data for Name: ht_res_kardex; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_kardex (hb_id, tih_id, su_id, hb_capacidad, ta_id, res_id, ka_fecha, ka_situacion, pro_id) FROM stdin;
00000011	03	00000002	\N	D	\N	2012-12-10	1	\N
00000011	03	00000002	\N	D	\N	2012-12-11	1	\N
00000011	03	00000002	\N	D	\N	2012-12-12	1	\N
00000011	03	00000002	\N	D	\N	2012-12-13	1	\N
00000011	03	00000002	\N	F	\N	2012-12-14	1	\N
00000011	03	00000002	\N	F	\N	2012-12-15	1	\N
00000011	03	00000002	\N	D	\N	2012-12-16	1	\N
00000011	03	00000002	\N	D	\N	2012-12-17	1	\N
00000011	03	00000002	\N	D	\N	2012-12-18	1	\N
00000011	03	00000002	\N	D	\N	2012-12-19	1	\N
00000011	03	00000002	\N	D	\N	2012-12-20	1	\N
00000011	03	00000002	\N	F	\N	2012-12-21	1	\N
00000011	03	00000002	\N	F	\N	2012-12-22	1	\N
00000011	03	00000002	\N	D	\N	2012-12-23	1	\N
00000011	03	00000002	\N	D	\N	2012-12-24	1	\N
00000011	03	00000002	\N	D	\N	2012-12-25	1	\N
00000011	03	00000002	\N	D	\N	2012-12-26	1	\N
00000011	03	00000002	\N	D	\N	2012-12-27	1	\N
00000011	03	00000002	\N	F	\N	2012-12-28	1	\N
00000011	03	00000002	\N	F	\N	2012-12-29	1	\N
00000011	03	00000002	\N	D	\N	2012-12-30	1	\N
00000011	03	00000002	\N	D	\N	2012-12-31	1	\N
00000012	03	00000002	\N	F	\N	2012-12-01	1	\N
00000012	03	00000002	\N	D	\N	2012-12-02	1	\N
00000012	03	00000002	\N	D	\N	2012-12-03	1	\N
00000012	03	00000002	\N	D	\N	2012-12-04	1	\N
00000012	03	00000002	\N	D	\N	2012-12-05	1	\N
00000012	03	00000002	\N	D	\N	2012-12-06	1	\N
00000012	03	00000002	\N	F	\N	2012-12-07	1	\N
00000012	03	00000002	\N	F	\N	2012-12-08	1	\N
00000012	03	00000002	\N	D	\N	2012-12-09	1	\N
00000012	03	00000002	\N	D	\N	2012-12-10	1	\N
00000012	03	00000002	\N	D	\N	2012-12-11	1	\N
00000012	03	00000002	\N	D	\N	2012-12-12	1	\N
00000012	03	00000002	\N	D	\N	2012-12-13	1	\N
00000012	03	00000002	\N	F	\N	2012-12-14	1	\N
00000012	03	00000002	\N	F	\N	2012-12-15	1	\N
00000012	03	00000002	\N	D	\N	2012-12-16	1	\N
00000012	03	00000002	\N	D	\N	2012-12-17	1	\N
00000012	03	00000002	\N	D	\N	2012-12-18	1	\N
00000012	03	00000002	\N	D	\N	2012-12-19	1	\N
00000012	03	00000002	\N	D	\N	2012-12-20	1	\N
00000012	03	00000002	\N	F	\N	2012-12-21	1	\N
00000012	03	00000002	\N	F	\N	2012-12-22	1	\N
00000012	03	00000002	\N	D	\N	2012-12-23	1	\N
00000012	03	00000002	\N	D	\N	2012-12-24	1	\N
00000012	03	00000002	\N	D	\N	2012-12-25	1	\N
00000012	03	00000002	\N	D	\N	2012-12-26	1	\N
00000012	03	00000002	\N	D	\N	2012-12-27	1	\N
00000012	03	00000002	\N	F	\N	2012-12-28	1	\N
00000012	03	00000002	\N	F	\N	2012-12-29	1	\N
00000012	03	00000002	\N	D	\N	2012-12-30	1	\N
00000012	03	00000002	\N	D	\N	2012-12-31	1	\N
00000005	01	00000002	\N	D	\N	2012-10-02	1	\N
00000006	01	00000002	\N	D	\N	2012-10-02	1	\N
00000007	01	00000002	\N	D	\N	2012-10-02	1	\N
00000017	02	00000002	\N	F	\N	2012-09-01	0	\N
00000017	02	00000002	\N	D	\N	2012-09-02	0	\N
00000017	02	00000002	\N	D	\N	2012-09-03	0	\N
00000017	02	00000002	\N	D	\N	2012-09-04	0	\N
00000017	02	00000002	\N	D	\N	2012-09-05	0	\N
00000017	02	00000002	\N	D	\N	2012-09-06	0	\N
00000017	02	00000002	\N	F	\N	2012-09-07	0	\N
00000017	02	00000002	\N	F	\N	2012-09-08	0	\N
00000017	02	00000002	\N	D	\N	2012-09-09	0	\N
00000017	02	00000002	\N	D	\N	2012-09-10	0	\N
00000017	02	00000002	\N	D	\N	2012-09-11	0	\N
00000017	02	00000002	\N	D	\N	2012-09-12	0	\N
00000017	02	00000002	\N	D	\N	2012-09-13	0	\N
00000017	02	00000002	\N	F	\N	2012-09-14	0	\N
00000017	02	00000002	\N	F	\N	2012-09-15	0	\N
00000017	02	00000002	\N	D	\N	2012-09-16	0	\N
00000017	02	00000002	\N	D	0000000002	2012-11-04	R	\N
00000017	02	00000002	\N	D	0000000002	2012-11-05	R	\N
00000017	02	00000002	\N	D	0000000002	2012-11-06	R	\N
00000017	02	00000002	\N	D	0000000002	2012-11-07	R	\N
00000017	02	00000002	\N	F	0000000002	2012-11-02	R	\N
00000017	02	00000002	\N	F	0000000002	2012-11-03	R	\N
00000017	02	00000002	\N	D	\N	2012-09-17	0	\N
00000017	02	00000002	\N	D	\N	2012-09-18	0	\N
00000017	02	00000002	\N	D	\N	2012-09-19	0	\N
00000017	02	00000002	\N	D	\N	2012-09-20	0	\N
00000017	02	00000002	\N	F	\N	2012-09-21	0	\N
00000001	01	00000002	\N	F	\N	2012-09-01	0	\N
00000001	01	00000002	\N	D	\N	2012-09-02	0	\N
00000001	01	00000002	\N	D	\N	2012-09-03	0	\N
00000001	01	00000002	\N	D	\N	2012-09-04	0	\N
00000001	01	00000002	\N	D	\N	2012-09-05	0	\N
00000001	01	00000002	\N	D	\N	2012-09-06	0	\N
00000001	01	00000002	\N	F	\N	2012-09-07	0	\N
00000001	01	00000002	\N	F	\N	2012-09-08	0	\N
00000001	01	00000002	\N	D	\N	2012-09-09	0	\N
00000001	01	00000002	\N	D	\N	2012-09-10	0	\N
00000001	01	00000002	\N	D	\N	2012-09-11	0	\N
00000017	02	00000002	\N	F	\N	2012-09-22	0	\N
00000017	02	00000002	\N	D	\N	2012-09-23	0	\N
00000017	02	00000002	\N	D	\N	2012-09-24	0	\N
00000017	02	00000002	\N	D	\N	2012-09-25	0	\N
00000017	02	00000002	\N	D	\N	2012-09-26	0	\N
00000017	02	00000002	\N	D	\N	2012-09-27	0	\N
00000017	02	00000002	\N	F	\N	2012-09-28	0	\N
00000017	02	00000002	\N	F	\N	2012-09-29	0	\N
00000017	02	00000002	\N	D	\N	2012-09-30	0	\N
00000018	02	00000002	\N	F	\N	2012-09-01	0	\N
00000018	02	00000002	\N	D	\N	2012-09-02	0	\N
00000018	02	00000002	\N	D	\N	2012-09-03	0	\N
00000018	02	00000002	\N	D	\N	2012-09-04	0	\N
00000018	02	00000002	\N	D	\N	2012-09-05	0	\N
00000018	02	00000002	\N	D	\N	2012-09-06	0	\N
00000018	02	00000002	\N	F	\N	2012-09-07	0	\N
00000018	02	00000002	\N	F	\N	2012-09-08	0	\N
00000018	02	00000002	\N	D	\N	2012-09-09	0	\N
00000018	02	00000002	\N	D	\N	2012-09-10	0	\N
00000018	02	00000002	\N	D	\N	2012-09-11	0	\N
00000018	02	00000002	\N	D	\N	2012-09-12	0	\N
00000018	02	00000002	\N	D	\N	2012-09-13	0	\N
00000018	02	00000002	\N	F	\N	2012-09-14	0	\N
00000018	02	00000002	\N	F	\N	2012-09-15	0	\N
00000018	02	00000002	\N	D	\N	2012-09-16	0	\N
00000018	02	00000002	\N	D	\N	2012-09-17	0	\N
00000018	02	00000002	\N	D	\N	2012-09-18	0	\N
00000018	02	00000002	\N	D	\N	2012-09-19	0	\N
00000018	02	00000002	\N	D	\N	2012-09-20	0	\N
00000018	02	00000002	\N	F	\N	2012-09-21	0	\N
00000018	02	00000002	\N	F	\N	2012-09-22	0	\N
00000018	02	00000002	\N	D	\N	2012-09-23	0	\N
00000018	02	00000002	\N	D	\N	2012-09-24	0	\N
00000018	02	00000002	\N	D	\N	2012-09-25	0	\N
00000018	02	00000002	\N	D	\N	2012-09-26	0	\N
00000018	02	00000002	\N	D	\N	2012-09-27	0	\N
00000018	02	00000002	\N	F	\N	2012-09-28	0	\N
00000018	02	00000002	\N	F	\N	2012-09-29	0	\N
00000018	02	00000002	\N	D	\N	2012-09-30	0	\N
00000019	02	00000002	\N	F	\N	2012-09-01	0	\N
00000019	02	00000002	\N	D	\N	2012-09-02	0	\N
00000019	02	00000002	\N	D	\N	2012-09-03	0	\N
00000019	02	00000002	\N	D	\N	2012-09-04	0	\N
00000019	02	00000002	\N	D	\N	2012-09-05	0	\N
00000019	02	00000002	\N	D	\N	2012-09-06	0	\N
00000019	02	00000002	\N	F	\N	2012-09-07	0	\N
00000019	02	00000002	\N	F	\N	2012-09-08	0	\N
00000019	02	00000002	\N	D	\N	2012-09-09	0	\N
00000019	02	00000002	\N	D	\N	2012-09-10	0	\N
00000019	02	00000002	\N	D	\N	2012-09-11	0	\N
00000019	02	00000002	\N	D	\N	2012-09-12	0	\N
00000019	02	00000002	\N	D	\N	2012-09-13	0	\N
00000019	02	00000002	\N	F	\N	2012-09-14	0	\N
00000019	02	00000002	\N	F	\N	2012-09-15	0	\N
00000019	02	00000002	\N	D	\N	2012-09-16	0	\N
00000019	02	00000002	\N	D	\N	2012-09-17	0	\N
00000019	02	00000002	\N	D	\N	2012-09-18	0	\N
00000019	02	00000002	\N	D	\N	2012-09-19	0	\N
00000019	02	00000002	\N	D	\N	2012-09-20	0	\N
00000019	02	00000002	\N	F	\N	2012-09-21	0	\N
00000019	02	00000002	\N	F	\N	2012-09-22	0	\N
00000019	02	00000002	\N	D	\N	2012-09-23	0	\N
00000019	02	00000002	\N	D	\N	2012-09-24	0	\N
00000019	02	00000002	\N	D	\N	2012-09-25	0	\N
00000019	02	00000002	\N	D	\N	2012-09-26	0	\N
00000019	02	00000002	\N	D	\N	2012-09-27	0	\N
00000019	02	00000002	\N	F	\N	2012-09-28	0	\N
00000019	02	00000002	\N	F	\N	2012-09-29	0	\N
00000019	02	00000002	\N	D	\N	2012-09-30	0	\N
00000020	02	00000002	\N	F	\N	2012-09-01	0	\N
00000020	02	00000002	\N	D	\N	2012-09-02	0	\N
00000020	02	00000002	\N	D	\N	2012-09-03	0	\N
00000020	02	00000002	\N	D	\N	2012-09-04	0	\N
00000020	02	00000002	\N	D	\N	2012-09-05	0	\N
00000020	02	00000002	\N	D	\N	2012-09-06	0	\N
00000020	02	00000002	\N	F	\N	2012-09-07	0	\N
00000020	02	00000002	\N	F	\N	2012-09-08	0	\N
00000020	02	00000002	\N	D	\N	2012-09-09	0	\N
00000020	02	00000002	\N	D	\N	2012-09-10	0	\N
00000020	02	00000002	\N	D	\N	2012-09-11	0	\N
00000020	02	00000002	\N	D	\N	2012-09-12	0	\N
00000020	02	00000002	\N	D	\N	2012-09-13	0	\N
00000020	02	00000002	\N	F	\N	2012-09-14	0	\N
00000020	02	00000002	\N	F	\N	2012-09-15	0	\N
00000020	02	00000002	\N	D	\N	2012-09-16	0	\N
00000020	02	00000002	\N	D	\N	2012-09-17	0	\N
00000020	02	00000002	\N	D	\N	2012-09-18	0	\N
00000020	02	00000002	\N	D	\N	2012-09-19	0	\N
00000020	02	00000002	\N	D	\N	2012-09-20	0	\N
00000020	02	00000002	\N	F	\N	2012-09-21	0	\N
00000020	02	00000002	\N	F	\N	2012-09-22	0	\N
00000020	02	00000002	\N	D	\N	2012-09-23	0	\N
00000020	02	00000002	\N	D	\N	2012-09-24	0	\N
00000020	02	00000002	\N	D	\N	2012-09-25	0	\N
00000020	02	00000002	\N	D	\N	2012-09-26	0	\N
00000020	02	00000002	\N	D	\N	2012-09-27	0	\N
00000020	02	00000002	\N	F	\N	2012-09-28	0	\N
00000020	02	00000002	\N	F	\N	2012-09-29	0	\N
00000020	02	00000002	\N	D	\N	2012-09-30	0	\N
00000021	02	00000002	\N	F	\N	2012-09-01	0	\N
00000021	02	00000002	\N	D	\N	2012-09-02	0	\N
00000005	01	00000002	\N	F	\N	2012-09-08	0	\N
00000005	01	00000002	\N	D	\N	2012-09-09	0	\N
00000005	01	00000002	\N	D	\N	2012-09-10	0	\N
00000005	01	00000002	\N	D	\N	2012-09-11	0	\N
00000005	01	00000002	\N	D	\N	2012-09-12	0	\N
00000005	01	00000002	\N	D	\N	2012-09-13	0	\N
00000005	01	00000002	\N	F	\N	2012-09-14	0	\N
00000005	01	00000002	\N	F	\N	2012-09-15	0	\N
00000005	01	00000002	\N	D	\N	2012-09-16	0	\N
00000005	01	00000002	\N	D	\N	2012-09-17	0	\N
00000005	01	00000002	\N	D	\N	2012-09-18	0	\N
00000010	03	00000002	\N	F	\N	2012-09-01	1	\N
00000010	03	00000002	\N	D	\N	2012-09-02	1	\N
00000010	03	00000002	\N	D	\N	2012-09-03	1	\N
00000010	03	00000002	\N	D	\N	2012-09-04	1	\N
00000010	03	00000002	\N	D	\N	2012-09-05	1	\N
00000010	03	00000002	\N	D	\N	2012-09-06	1	\N
00000010	03	00000002	\N	F	\N	2012-09-07	1	\N
00000010	03	00000002	\N	F	\N	2012-09-08	1	\N
00000010	03	00000002	\N	D	\N	2012-09-09	1	\N
00000010	03	00000002	\N	D	\N	2012-09-10	1	\N
00000010	03	00000002	\N	D	\N	2012-09-11	1	\N
00000010	03	00000002	\N	D	\N	2012-09-12	1	\N
00000010	03	00000002	\N	D	\N	2012-09-13	1	\N
00000010	03	00000002	\N	F	\N	2012-09-14	1	\N
00000010	03	00000002	\N	F	\N	2012-09-15	1	\N
00000010	03	00000002	\N	D	\N	2012-09-16	1	\N
00000010	03	00000002	\N	D	\N	2012-09-17	1	\N
00000010	03	00000002	\N	D	\N	2012-09-18	1	\N
00000010	03	00000002	\N	D	\N	2012-09-19	1	\N
00000010	03	00000002	\N	D	\N	2012-09-20	1	\N
00000010	03	00000002	\N	F	\N	2012-09-21	1	\N
00000010	03	00000002	\N	F	\N	2012-09-22	1	\N
00000010	03	00000002	\N	D	\N	2012-09-23	1	\N
00000010	03	00000002	\N	D	\N	2012-09-24	1	\N
00000010	03	00000002	\N	D	\N	2012-09-25	1	\N
00000010	03	00000002	\N	D	\N	2012-09-26	1	\N
00000010	03	00000002	\N	D	\N	2012-09-27	1	\N
00000010	03	00000002	\N	F	\N	2012-09-28	1	\N
00000010	03	00000002	\N	F	\N	2012-09-29	1	\N
00000010	03	00000002	\N	D	\N	2012-09-30	1	\N
00000011	03	00000002	\N	F	\N	2012-09-01	1	\N
00000011	03	00000002	\N	D	\N	2012-09-02	1	\N
00000011	03	00000002	\N	D	\N	2012-09-03	1	\N
00000011	03	00000002	\N	D	\N	2012-09-04	1	\N
00000011	03	00000002	\N	D	\N	2012-09-05	1	\N
00000011	03	00000002	\N	D	\N	2012-09-06	1	\N
00000011	03	00000002	\N	F	\N	2012-09-07	1	\N
00000011	03	00000002	\N	F	\N	2012-09-08	1	\N
00000011	03	00000002	\N	D	\N	2012-09-09	1	\N
00000011	03	00000002	\N	D	\N	2012-09-10	1	\N
00000011	03	00000002	\N	D	\N	2012-09-11	1	\N
00000011	03	00000002	\N	D	\N	2012-09-12	1	\N
00000011	03	00000002	\N	D	\N	2012-09-13	1	\N
00000011	03	00000002	\N	F	\N	2012-09-14	1	\N
00000011	03	00000002	\N	F	\N	2012-09-15	1	\N
00000011	03	00000002	\N	D	\N	2012-09-16	1	\N
00000011	03	00000002	\N	D	\N	2012-09-17	1	\N
00000011	03	00000002	\N	D	\N	2012-09-18	1	\N
00000011	03	00000002	\N	D	\N	2012-09-19	1	\N
00000011	03	00000002	\N	D	\N	2012-09-20	1	\N
00000011	03	00000002	\N	F	\N	2012-09-21	1	\N
00000011	03	00000002	\N	F	\N	2012-09-22	1	\N
00000011	03	00000002	\N	D	\N	2012-09-23	1	\N
00000011	03	00000002	\N	D	\N	2012-09-24	1	\N
00000011	03	00000002	\N	D	\N	2012-09-25	1	\N
00000011	03	00000002	\N	D	\N	2012-09-26	1	\N
00000011	03	00000002	\N	D	\N	2012-09-27	1	\N
00000011	03	00000002	\N	F	\N	2012-09-28	1	\N
00000011	03	00000002	\N	F	\N	2012-09-29	1	\N
00000011	03	00000002	\N	D	\N	2012-09-30	1	\N
00000012	03	00000002	\N	F	\N	2012-09-01	1	\N
00000012	03	00000002	\N	D	\N	2012-09-02	1	\N
00000012	03	00000002	\N	D	\N	2012-09-03	1	\N
00000012	03	00000002	\N	D	\N	2012-09-04	1	\N
00000012	03	00000002	\N	D	\N	2012-09-05	1	\N
00000012	03	00000002	\N	D	\N	2012-09-06	1	\N
00000012	03	00000002	\N	F	\N	2012-09-07	1	\N
00000012	03	00000002	\N	F	\N	2012-09-08	1	\N
00000012	03	00000002	\N	D	\N	2012-09-09	1	\N
00000012	03	00000002	\N	D	\N	2012-09-10	1	\N
00000012	03	00000002	\N	D	\N	2012-09-11	1	\N
00000012	03	00000002	\N	D	\N	2012-09-12	1	\N
00000012	03	00000002	\N	D	\N	2012-09-13	1	\N
00000012	03	00000002	\N	F	\N	2012-09-14	1	\N
00000012	03	00000002	\N	F	\N	2012-09-15	1	\N
00000012	03	00000002	\N	D	\N	2012-09-16	1	\N
00000012	03	00000002	\N	D	\N	2012-09-17	1	\N
00000012	03	00000002	\N	D	\N	2012-09-18	1	\N
00000012	03	00000002	\N	D	\N	2012-09-19	1	\N
00000012	03	00000002	\N	D	\N	2012-09-20	1	\N
00000012	03	00000002	\N	F	\N	2012-09-21	1	\N
00000012	03	00000002	\N	F	\N	2012-09-22	1	\N
00000012	03	00000002	\N	D	\N	2012-09-23	1	\N
00000012	03	00000002	\N	D	\N	2012-09-24	1	\N
00000012	03	00000002	\N	D	\N	2012-09-25	1	\N
00000012	03	00000002	\N	D	\N	2012-09-26	1	\N
00000012	03	00000002	\N	D	\N	2012-09-27	1	\N
00000012	03	00000002	\N	F	\N	2012-09-28	1	\N
00000012	03	00000002	\N	F	\N	2012-09-29	1	\N
00000012	03	00000002	\N	D	\N	2012-09-30	1	\N
00000013	05	00000002	\N	F	\N	2012-09-01	1	\N
00000013	05	00000002	\N	D	\N	2012-09-02	1	\N
00000013	05	00000002	\N	D	\N	2012-09-03	1	\N
00000013	05	00000002	\N	D	\N	2012-09-04	1	\N
00000013	05	00000002	\N	D	\N	2012-09-05	1	\N
00000013	05	00000002	\N	D	\N	2012-09-06	1	\N
00000013	05	00000002	\N	F	\N	2012-09-07	1	\N
00000013	05	00000002	\N	F	\N	2012-09-08	1	\N
00000013	05	00000002	\N	D	\N	2012-09-09	1	\N
00000013	05	00000002	\N	D	\N	2012-09-10	1	\N
00000013	05	00000002	\N	D	\N	2012-09-11	1	\N
00000013	05	00000002	\N	D	\N	2012-09-12	1	\N
00000013	05	00000002	\N	D	\N	2012-09-13	1	\N
00000013	05	00000002	\N	F	\N	2012-09-14	1	\N
00000013	05	00000002	\N	F	\N	2012-09-15	1	\N
00000013	05	00000002	\N	D	\N	2012-09-16	1	\N
00000013	05	00000002	\N	D	\N	2012-09-17	1	\N
00000013	05	00000002	\N	D	\N	2012-09-18	1	\N
00000013	05	00000002	\N	D	\N	2012-09-19	1	\N
00000013	05	00000002	\N	D	\N	2012-09-20	1	\N
00000013	05	00000002	\N	F	\N	2012-09-21	1	\N
00000013	05	00000002	\N	F	\N	2012-09-22	1	\N
00000013	05	00000002	\N	D	\N	2012-09-23	1	\N
00000013	05	00000002	\N	D	\N	2012-09-24	1	\N
00000013	05	00000002	\N	D	\N	2012-09-25	1	\N
00000013	05	00000002	\N	D	\N	2012-09-26	1	\N
00000013	05	00000002	\N	D	\N	2012-09-27	1	\N
00000013	05	00000002	\N	F	\N	2012-09-28	1	\N
00000013	05	00000002	\N	F	\N	2012-09-29	1	\N
00000013	05	00000002	\N	D	\N	2012-09-30	1	\N
00000014	05	00000002	\N	F	\N	2012-09-01	1	\N
00000014	05	00000002	\N	D	\N	2012-09-02	1	\N
00000014	05	00000002	\N	D	\N	2012-09-03	1	\N
00000014	05	00000002	\N	D	\N	2012-09-04	1	\N
00000014	05	00000002	\N	D	\N	2012-09-05	1	\N
00000014	05	00000002	\N	D	\N	2012-09-06	1	\N
00000014	05	00000002	\N	F	\N	2012-09-07	1	\N
00000014	05	00000002	\N	F	\N	2012-09-08	1	\N
00000014	05	00000002	\N	D	\N	2012-09-09	1	\N
00000014	05	00000002	\N	D	\N	2012-09-10	1	\N
00000014	05	00000002	\N	D	\N	2012-09-11	1	\N
00000014	05	00000002	\N	D	\N	2012-09-12	1	\N
00000014	05	00000002	\N	D	\N	2012-09-13	1	\N
00000014	05	00000002	\N	F	\N	2012-09-14	1	\N
00000014	05	00000002	\N	F	\N	2012-09-15	1	\N
00000014	05	00000002	\N	D	\N	2012-09-16	1	\N
00000014	05	00000002	\N	D	\N	2012-09-17	1	\N
00000014	05	00000002	\N	D	\N	2012-09-18	1	\N
00000014	05	00000002	\N	D	\N	2012-09-19	1	\N
00000014	05	00000002	\N	D	\N	2012-09-20	1	\N
00000014	05	00000002	\N	F	\N	2012-09-21	1	\N
00000014	05	00000002	\N	F	\N	2012-09-22	1	\N
00000014	05	00000002	\N	D	\N	2012-09-23	1	\N
00000014	05	00000002	\N	D	\N	2012-09-24	1	\N
00000014	05	00000002	\N	D	\N	2012-09-25	1	\N
00000014	05	00000002	\N	D	\N	2012-09-26	1	\N
00000014	05	00000002	\N	D	\N	2012-09-27	1	\N
00000014	05	00000002	\N	F	\N	2012-09-28	1	\N
00000014	05	00000002	\N	F	\N	2012-09-29	1	\N
00000014	05	00000002	\N	D	\N	2012-09-30	1	\N
00000015	05	00000002	\N	F	\N	2012-09-01	1	\N
00000015	05	00000002	\N	D	\N	2012-09-02	1	\N
00000015	05	00000002	\N	D	\N	2012-09-03	1	\N
00000015	05	00000002	\N	D	\N	2012-09-04	1	\N
00000015	05	00000002	\N	D	\N	2012-09-05	1	\N
00000015	05	00000002	\N	D	\N	2012-09-06	1	\N
00000015	05	00000002	\N	F	\N	2012-09-07	1	\N
00000015	05	00000002	\N	F	\N	2012-09-08	1	\N
00000015	05	00000002	\N	D	\N	2012-09-09	1	\N
00000015	05	00000002	\N	D	\N	2012-09-10	1	\N
00000015	05	00000002	\N	D	\N	2012-09-11	1	\N
00000015	05	00000002	\N	D	\N	2012-09-12	1	\N
00000015	05	00000002	\N	D	\N	2012-09-13	1	\N
00000015	05	00000002	\N	F	\N	2012-09-14	1	\N
00000015	05	00000002	\N	F	\N	2012-09-15	1	\N
00000015	05	00000002	\N	D	\N	2012-09-16	1	\N
00000015	05	00000002	\N	D	\N	2012-09-17	1	\N
00000015	05	00000002	\N	D	\N	2012-09-18	1	\N
00000015	05	00000002	\N	D	\N	2012-09-19	1	\N
00000015	05	00000002	\N	D	\N	2012-09-20	1	\N
00000015	05	00000002	\N	F	\N	2012-09-21	1	\N
00000015	05	00000002	\N	F	\N	2012-09-22	1	\N
00000015	05	00000002	\N	D	\N	2012-09-23	1	\N
00000015	05	00000002	\N	D	\N	2012-09-24	1	\N
00000015	05	00000002	\N	D	\N	2012-09-25	1	\N
00000015	05	00000002	\N	D	\N	2012-09-26	1	\N
00000015	05	00000002	\N	D	\N	2012-09-27	1	\N
00000015	05	00000002	\N	F	\N	2012-09-28	1	\N
00000015	05	00000002	\N	F	\N	2012-09-29	1	\N
00000015	05	00000002	\N	D	\N	2012-09-30	1	\N
00000016	05	00000002	\N	F	\N	2012-09-01	1	\N
00000016	05	00000002	\N	D	\N	2012-09-02	1	\N
00000016	05	00000002	\N	D	\N	2012-09-03	1	\N
00000016	05	00000002	\N	D	\N	2012-09-04	1	\N
00000016	05	00000002	\N	D	\N	2012-09-05	1	\N
00000016	05	00000002	\N	D	\N	2012-09-06	1	\N
00000016	05	00000002	\N	F	\N	2012-09-07	1	\N
00000016	05	00000002	\N	F	\N	2012-09-08	1	\N
00000016	05	00000002	\N	D	\N	2012-09-09	1	\N
00000016	05	00000002	\N	D	\N	2012-09-10	1	\N
00000016	05	00000002	\N	D	\N	2012-09-11	1	\N
00000016	05	00000002	\N	D	\N	2012-09-12	1	\N
00000016	05	00000002	\N	D	\N	2012-09-13	1	\N
00000016	05	00000002	\N	F	\N	2012-09-14	1	\N
00000016	05	00000002	\N	F	\N	2012-09-15	1	\N
00000016	05	00000002	\N	D	\N	2012-09-16	1	\N
00000016	05	00000002	\N	D	\N	2012-09-17	1	\N
00000016	05	00000002	\N	D	\N	2012-09-18	1	\N
00000016	05	00000002	\N	D	\N	2012-09-19	1	\N
00000016	05	00000002	\N	D	\N	2012-09-20	1	\N
00000016	05	00000002	\N	F	\N	2012-09-21	1	\N
00000016	05	00000002	\N	F	\N	2012-09-22	1	\N
00000016	05	00000002	\N	D	\N	2012-09-23	1	\N
00000016	05	00000002	\N	D	\N	2012-09-24	1	\N
00000016	05	00000002	\N	D	\N	2012-09-25	1	\N
00000016	05	00000002	\N	D	\N	2012-09-26	1	\N
00000016	05	00000002	\N	D	\N	2012-09-27	1	\N
00000016	05	00000002	\N	F	\N	2012-09-28	1	\N
00000016	05	00000002	\N	F	\N	2012-09-29	1	\N
00000016	05	00000002	\N	D	\N	2012-09-30	1	\N
00000076	01	00000002	\N	D	0000000010	2012-11-15	R	\N
00000002	01	00000002	\N	F	\N	2013-02-02	1	\N
00000002	01	00000002	\N	D	\N	2013-02-03	1	\N
00000002	01	00000002	\N	D	\N	2013-02-04	1	\N
00000002	01	00000002	\N	D	\N	2013-02-05	1	\N
00000002	01	00000002	\N	D	\N	2013-02-06	1	\N
00000002	01	00000002	\N	D	\N	2013-02-07	1	\N
00000002	01	00000002	\N	F	\N	2013-02-08	1	\N
00000002	01	00000002	\N	F	\N	2013-02-09	1	\N
00000002	01	00000002	\N	D	\N	2013-02-10	1	\N
00000002	01	00000002	\N	D	\N	2013-02-11	1	\N
00000002	01	00000002	\N	D	\N	2013-02-12	1	\N
00000002	01	00000002	\N	D	\N	2013-02-13	1	\N
00000002	01	00000002	\N	D	\N	2013-02-14	1	\N
00000002	01	00000002	\N	F	\N	2013-02-15	1	\N
00000002	01	00000002	\N	F	\N	2013-02-16	1	\N
00000002	01	00000002	\N	D	\N	2013-02-17	1	\N
00000002	01	00000002	\N	D	\N	2013-02-18	1	\N
00000002	01	00000002	\N	D	\N	2013-02-19	1	\N
00000002	01	00000002	\N	D	\N	2013-02-20	1	\N
00000002	01	00000002	\N	D	\N	2013-02-21	1	\N
00000002	01	00000002	\N	F	\N	2013-02-22	1	\N
00000002	01	00000002	\N	F	\N	2013-02-23	1	\N
00000002	01	00000002	\N	D	\N	2013-02-24	1	\N
00000002	01	00000002	\N	D	\N	2013-02-25	1	\N
00000002	01	00000002	\N	D	\N	2013-02-26	1	\N
00000002	01	00000002	\N	D	\N	2013-02-27	1	\N
00000002	01	00000002	\N	D	\N	2013-02-28	1	\N
00000013	05	00000002	\N	D	0000000008	2012-11-15	R	\N
00000013	05	00000002	\N	D	0000000008	2012-11-16	R	\N
00000014	05	00000002	\N	D	\N	2012-11-15	1	\N
00000014	05	00000002	\N	D	\N	2012-11-16	1	\N
00000015	05	00000002	\N	D	\N	2012-11-15	1	\N
00000015	05	00000002	\N	D	\N	2012-11-16	1	\N
00000016	05	00000002	\N	D	\N	2012-11-15	1	\N
00000016	05	00000002	\N	D	\N	2012-11-16	1	\N
00000003	01	00000002	\N	F	\N	2013-02-01	1	\N
00000003	01	00000002	\N	F	\N	2013-02-02	1	\N
00000003	01	00000002	\N	D	\N	2013-02-03	1	\N
00000003	01	00000002	\N	D	\N	2013-02-04	1	\N
00000003	01	00000002	\N	D	\N	2013-02-05	1	\N
00000003	01	00000002	\N	D	\N	2013-02-06	1	\N
00000003	01	00000002	\N	D	\N	2013-02-07	1	\N
00000003	01	00000002	\N	F	\N	2013-02-08	1	\N
00000003	01	00000002	\N	F	\N	2013-02-09	1	\N
00000003	01	00000002	\N	D	\N	2013-02-10	1	\N
00000003	01	00000002	\N	D	\N	2013-02-11	1	\N
00000003	01	00000002	\N	D	\N	2013-02-12	1	\N
00000003	01	00000002	\N	D	\N	2013-02-13	1	\N
00000003	01	00000002	\N	D	\N	2013-02-14	1	\N
00000003	01	00000002	\N	F	\N	2013-02-15	1	\N
00000003	01	00000002	\N	F	\N	2013-02-16	1	\N
00000003	01	00000002	\N	D	\N	2013-02-17	1	\N
00000003	01	00000002	\N	D	\N	2013-02-18	1	\N
00000003	01	00000002	\N	D	\N	2013-02-19	1	\N
00000003	01	00000002	\N	D	\N	2013-02-20	1	\N
00000003	01	00000002	\N	D	\N	2013-02-21	1	\N
00000003	01	00000002	\N	F	\N	2013-02-22	1	\N
00000003	01	00000002	\N	F	\N	2013-02-23	1	\N
00000003	01	00000002	\N	D	\N	2013-02-24	1	\N
00000003	01	00000002	\N	D	\N	2013-02-25	1	\N
00000003	01	00000002	\N	D	\N	2013-02-26	1	\N
00000003	01	00000002	\N	D	\N	2013-02-27	1	\N
00000003	01	00000002	\N	D	\N	2013-02-28	1	\N
00000004	01	00000002	\N	F	\N	2013-02-01	1	\N
00000004	01	00000002	\N	F	\N	2013-02-02	1	\N
00000004	01	00000002	\N	D	\N	2013-02-03	1	\N
00000004	01	00000002	\N	D	\N	2013-02-04	1	\N
00000004	01	00000002	\N	D	\N	2013-02-05	1	\N
00000004	01	00000002	\N	D	\N	2013-02-06	1	\N
00000004	01	00000002	\N	D	\N	2013-02-07	1	\N
00000004	01	00000002	\N	F	\N	2013-02-08	1	\N
00000004	01	00000002	\N	F	\N	2013-02-09	1	\N
00000004	01	00000002	\N	D	\N	2013-02-10	1	\N
00000004	01	00000002	\N	D	\N	2013-02-11	1	\N
00000004	01	00000002	\N	D	\N	2013-02-12	1	\N
00000004	01	00000002	\N	D	\N	2013-02-13	1	\N
00000004	01	00000002	\N	D	\N	2013-02-14	1	\N
00000004	01	00000002	\N	F	\N	2013-02-15	1	\N
00000004	01	00000002	\N	F	\N	2013-02-16	1	\N
00000004	01	00000002	\N	D	\N	2013-02-17	1	\N
00000004	01	00000002	\N	D	\N	2013-02-18	1	\N
00000004	01	00000002	\N	D	\N	2013-02-19	1	\N
00000004	01	00000002	\N	D	\N	2013-02-20	1	\N
00000004	01	00000002	\N	D	\N	2013-02-21	1	\N
00000004	01	00000002	\N	F	\N	2013-02-22	1	\N
00000004	01	00000002	\N	F	\N	2013-02-23	1	\N
00000004	01	00000002	\N	D	\N	2013-02-24	1	\N
00000004	01	00000002	\N	D	\N	2013-02-25	1	\N
00000004	01	00000002	\N	D	\N	2013-02-26	1	\N
00000004	01	00000002	\N	D	\N	2013-02-27	1	\N
00000004	01	00000002	\N	D	\N	2013-02-28	1	\N
00000005	01	00000002	\N	F	\N	2013-02-01	1	\N
00000005	01	00000002	\N	F	\N	2013-02-02	1	\N
00000005	01	00000002	\N	D	\N	2013-02-03	1	\N
00000005	01	00000002	\N	D	\N	2013-02-04	1	\N
00000005	01	00000002	\N	D	\N	2013-02-05	1	\N
00000005	01	00000002	\N	D	\N	2013-02-06	1	\N
00000005	01	00000002	\N	D	\N	2013-02-07	1	\N
00000005	01	00000002	\N	F	\N	2013-02-08	1	\N
00000005	01	00000002	\N	F	\N	2013-02-09	1	\N
00000005	01	00000002	\N	D	\N	2013-02-10	1	\N
00000005	01	00000002	\N	D	\N	2013-02-11	1	\N
00000005	01	00000002	\N	D	\N	2013-02-12	1	\N
00000005	01	00000002	\N	D	\N	2013-02-13	1	\N
00000005	01	00000002	\N	D	\N	2013-02-14	1	\N
00000005	01	00000002	\N	F	\N	2013-02-15	1	\N
00000005	01	00000002	\N	F	\N	2013-02-16	1	\N
00000005	01	00000002	\N	D	\N	2013-02-17	1	\N
00000005	01	00000002	\N	D	\N	2013-02-18	1	\N
00000005	01	00000002	\N	D	\N	2013-02-19	1	\N
00000005	01	00000002	\N	D	\N	2013-02-20	1	\N
00000005	01	00000002	\N	D	\N	2013-02-21	1	\N
00000005	01	00000002	\N	F	\N	2013-02-22	1	\N
00000005	01	00000002	\N	F	\N	2013-02-23	1	\N
00000005	01	00000002	\N	D	\N	2013-02-24	1	\N
00000005	01	00000002	\N	D	\N	2013-02-25	1	\N
00000005	01	00000002	\N	D	\N	2013-02-26	1	\N
00000005	01	00000002	\N	D	\N	2013-02-27	1	\N
00000005	01	00000002	\N	D	\N	2013-02-28	1	\N
00000006	01	00000002	\N	F	\N	2013-02-01	1	\N
00000006	01	00000002	\N	F	\N	2013-02-02	1	\N
00000006	01	00000002	\N	D	\N	2013-02-03	1	\N
00000006	01	00000002	\N	D	\N	2013-02-04	1	\N
00000006	01	00000002	\N	D	\N	2013-02-05	1	\N
00000006	01	00000002	\N	D	\N	2013-02-06	1	\N
00000006	01	00000002	\N	D	\N	2013-02-07	1	\N
00000006	01	00000002	\N	F	\N	2013-02-08	1	\N
00000006	01	00000002	\N	F	\N	2013-02-09	1	\N
00000006	01	00000002	\N	D	\N	2013-02-10	1	\N
00000006	01	00000002	\N	D	\N	2013-02-11	1	\N
00000006	01	00000002	\N	D	\N	2013-02-12	1	\N
00000006	01	00000002	\N	D	\N	2013-02-13	1	\N
00000006	01	00000002	\N	D	\N	2013-02-14	1	\N
00000006	01	00000002	\N	F	\N	2013-02-15	1	\N
00000006	01	00000002	\N	F	\N	2013-02-16	1	\N
00000006	01	00000002	\N	D	\N	2013-02-17	1	\N
00000006	01	00000002	\N	D	\N	2013-02-18	1	\N
00000006	01	00000002	\N	D	\N	2013-02-19	1	\N
00000006	01	00000002	\N	D	\N	2013-02-20	1	\N
00000006	01	00000002	\N	D	\N	2013-02-21	1	\N
00000006	01	00000002	\N	F	\N	2013-02-22	1	\N
00000006	01	00000002	\N	F	\N	2013-02-23	1	\N
00000006	01	00000002	\N	D	\N	2013-02-24	1	\N
00000006	01	00000002	\N	D	\N	2013-02-25	1	\N
00000006	01	00000002	\N	D	\N	2013-02-26	1	\N
00000006	01	00000002	\N	D	\N	2013-02-27	1	\N
00000006	01	00000002	\N	D	\N	2013-02-28	1	\N
00000007	01	00000002	\N	F	\N	2013-02-01	1	\N
00000007	01	00000002	\N	F	\N	2013-02-02	1	\N
00000007	01	00000002	\N	D	\N	2013-02-03	1	\N
00000007	01	00000002	\N	D	\N	2013-02-04	1	\N
00000007	01	00000002	\N	D	\N	2013-02-05	1	\N
00000007	01	00000002	\N	D	\N	2013-02-06	1	\N
00000007	01	00000002	\N	D	\N	2013-02-07	1	\N
00000007	01	00000002	\N	F	\N	2013-02-08	1	\N
00000007	01	00000002	\N	F	\N	2013-02-09	1	\N
00000007	01	00000002	\N	D	\N	2013-02-10	1	\N
00000007	01	00000002	\N	D	\N	2013-02-11	1	\N
00000007	01	00000002	\N	D	\N	2013-02-12	1	\N
00000007	01	00000002	\N	D	\N	2013-02-13	1	\N
00000007	01	00000002	\N	D	\N	2013-02-14	1	\N
00000007	01	00000002	\N	F	\N	2013-02-15	1	\N
00000007	01	00000002	\N	F	\N	2013-02-16	1	\N
00000007	01	00000002	\N	D	\N	2013-02-17	1	\N
00000007	01	00000002	\N	D	\N	2013-02-18	1	\N
00000007	01	00000002	\N	D	\N	2013-02-19	1	\N
00000007	01	00000002	\N	D	\N	2013-02-20	1	\N
00000007	01	00000002	\N	D	\N	2013-02-21	1	\N
00000007	01	00000002	\N	F	\N	2013-02-22	1	\N
00000007	01	00000002	\N	F	\N	2013-02-23	1	\N
00000007	01	00000002	\N	D	\N	2013-02-24	1	\N
00000007	01	00000002	\N	D	\N	2013-02-25	1	\N
00000007	01	00000002	\N	D	\N	2013-02-26	1	\N
00000007	01	00000002	\N	D	\N	2013-02-27	1	\N
00000007	01	00000002	\N	D	\N	2013-02-28	1	\N
00000076	01	00000002	\N	F	\N	2013-02-01	1	\N
00000076	01	00000002	\N	F	\N	2013-02-02	1	\N
00000076	01	00000002	\N	D	\N	2013-02-03	1	\N
00000076	01	00000002	\N	D	\N	2013-02-04	1	\N
00000076	01	00000002	\N	D	\N	2013-02-05	1	\N
00000076	01	00000002	\N	D	\N	2013-02-06	1	\N
00000076	01	00000002	\N	D	\N	2013-02-07	1	\N
00000076	01	00000002	\N	F	\N	2013-02-08	1	\N
00000076	01	00000002	\N	F	\N	2013-02-09	1	\N
00000076	01	00000002	\N	D	\N	2013-02-10	1	\N
00000076	01	00000002	\N	D	\N	2013-02-11	1	\N
00000076	01	00000002	\N	D	\N	2013-02-12	1	\N
00000076	01	00000002	\N	D	\N	2013-02-13	1	\N
00000076	01	00000002	\N	D	\N	2013-02-14	1	\N
00000076	01	00000002	\N	F	\N	2013-02-15	1	\N
00000076	01	00000002	\N	F	\N	2013-02-16	1	\N
00000076	01	00000002	\N	D	\N	2013-02-17	1	\N
00000076	01	00000002	\N	D	\N	2013-02-18	1	\N
00000076	01	00000002	\N	D	\N	2013-02-19	1	\N
00000076	01	00000002	\N	D	\N	2013-02-20	1	\N
00000076	01	00000002	\N	D	\N	2013-02-21	1	\N
00000076	01	00000002	\N	F	\N	2013-02-22	1	\N
00000076	01	00000002	\N	F	\N	2013-02-23	1	\N
00000076	01	00000002	\N	D	\N	2013-02-24	1	\N
00000076	01	00000002	\N	D	\N	2013-02-25	1	\N
00000076	01	00000002	\N	D	\N	2013-02-26	1	\N
00000076	01	00000002	\N	D	\N	2013-02-27	1	\N
00000076	01	00000002	\N	D	\N	2013-02-28	1	\N
00000001	01	00000002	\N	F	\N	2013-03-01	1	\N
00000001	01	00000002	\N	F	\N	2013-03-02	1	\N
00000001	01	00000002	\N	D	\N	2013-03-03	1	\N
00000001	01	00000002	\N	D	\N	2013-03-04	1	\N
00000001	01	00000002	\N	D	\N	2013-03-05	1	\N
00000001	01	00000002	\N	D	\N	2013-03-06	1	\N
00000001	01	00000002	\N	D	\N	2013-03-07	1	\N
00000001	01	00000002	\N	F	\N	2013-03-08	1	\N
00000001	01	00000002	\N	F	\N	2013-03-09	1	\N
00000001	01	00000002	\N	D	\N	2013-03-10	1	\N
00000001	01	00000002	\N	D	\N	2013-03-11	1	\N
00000001	01	00000002	\N	D	\N	2013-03-12	1	\N
00000001	01	00000002	\N	D	\N	2013-03-13	1	\N
00000001	01	00000002	\N	D	\N	2013-03-14	1	\N
00000001	01	00000002	\N	F	\N	2013-03-15	1	\N
00000001	01	00000002	\N	F	\N	2013-03-16	1	\N
00000001	01	00000002	\N	D	\N	2013-03-17	1	\N
00000001	01	00000002	\N	D	\N	2013-03-18	1	\N
00000001	01	00000002	\N	D	\N	2013-03-19	1	\N
00000001	01	00000002	\N	D	\N	2013-03-20	1	\N
00000001	01	00000002	\N	D	\N	2013-03-21	1	\N
00000001	01	00000002	\N	F	\N	2013-03-22	1	\N
00000001	01	00000002	\N	F	\N	2013-03-23	1	\N
00000001	01	00000002	\N	D	\N	2013-03-24	1	\N
00000001	01	00000002	\N	D	\N	2013-03-25	1	\N
00000001	01	00000002	\N	D	\N	2013-03-26	1	\N
00000001	01	00000002	\N	D	\N	2013-03-27	1	\N
00000001	01	00000002	\N	D	\N	2013-03-28	1	\N
00000001	01	00000002	\N	F	\N	2013-03-29	1	\N
00000001	01	00000002	\N	F	\N	2013-03-30	1	\N
00000001	01	00000002	\N	D	\N	2013-03-31	1	\N
00000002	01	00000002	\N	F	\N	2013-03-01	1	\N
00000010	03	00000002	\N	F	0000000013	2012-11-30	R	\N
00000002	01	00000002	\N	F	\N	2013-03-02	1	\N
00000002	01	00000002	\N	D	\N	2013-03-03	1	\N
00000002	01	00000002	\N	D	\N	2013-03-04	1	\N
00000002	01	00000002	\N	D	\N	2013-03-05	1	\N
00000002	01	00000002	\N	D	\N	2013-03-06	1	\N
00000002	01	00000002	\N	D	\N	2013-03-07	1	\N
00000002	01	00000002	\N	F	\N	2013-03-08	1	\N
00000002	01	00000002	\N	F	\N	2013-03-09	1	\N
00000002	01	00000002	\N	D	\N	2013-03-10	1	\N
00000002	01	00000002	\N	D	\N	2013-03-11	1	\N
00000002	01	00000002	\N	D	\N	2013-03-12	1	\N
00000002	01	00000002	\N	D	\N	2013-03-13	1	\N
00000002	01	00000002	\N	D	\N	2013-03-14	1	\N
00000002	01	00000002	\N	F	\N	2013-03-15	1	\N
00000002	01	00000002	\N	F	\N	2013-03-16	1	\N
00000002	01	00000002	\N	D	\N	2013-03-17	1	\N
00000002	01	00000002	\N	D	\N	2013-03-18	1	\N
00000002	01	00000002	\N	D	\N	2013-03-19	1	\N
00000002	01	00000002	\N	D	\N	2013-03-20	1	\N
00000002	01	00000002	\N	D	\N	2013-03-21	1	\N
00000002	01	00000002	\N	F	\N	2013-03-22	1	\N
00000002	01	00000002	\N	F	\N	2013-03-23	1	\N
00000002	01	00000002	\N	D	\N	2013-03-24	1	\N
00000002	01	00000002	\N	D	\N	2013-03-25	1	\N
00000002	01	00000002	\N	D	\N	2013-03-26	1	\N
00000002	01	00000002	\N	D	\N	2013-03-27	1	\N
00000002	01	00000002	\N	D	\N	2013-03-28	1	\N
00000002	01	00000002	\N	F	\N	2013-03-29	1	\N
00000002	01	00000002	\N	F	\N	2013-03-30	1	\N
00000002	01	00000002	\N	D	\N	2013-03-31	1	\N
00000003	01	00000002	\N	F	\N	2013-03-01	1	\N
00000003	01	00000002	\N	F	\N	2013-03-02	1	\N
00000003	01	00000002	\N	D	\N	2013-03-03	1	\N
00000003	01	00000002	\N	D	\N	2013-03-04	1	\N
00000003	01	00000002	\N	D	\N	2013-03-05	1	\N
00000003	01	00000002	\N	D	\N	2013-03-06	1	\N
00000003	01	00000002	\N	D	\N	2013-03-07	1	\N
00000003	01	00000002	\N	F	\N	2013-03-08	1	\N
00000003	01	00000002	\N	F	\N	2013-03-09	1	\N
00000003	01	00000002	\N	D	\N	2013-03-10	1	\N
00000003	01	00000002	\N	D	\N	2013-03-11	1	\N
00000003	01	00000002	\N	D	\N	2013-03-12	1	\N
00000003	01	00000002	\N	D	\N	2013-03-13	1	\N
00000003	01	00000002	\N	D	\N	2013-03-14	1	\N
00000003	01	00000002	\N	F	\N	2013-03-15	1	\N
00000003	01	00000002	\N	F	\N	2013-03-16	1	\N
00000003	01	00000002	\N	D	\N	2013-03-17	1	\N
00000003	01	00000002	\N	D	\N	2013-03-18	1	\N
00000003	01	00000002	\N	D	\N	2013-03-19	1	\N
00000003	01	00000002	\N	D	\N	2013-03-20	1	\N
00000003	01	00000002	\N	D	\N	2013-03-21	1	\N
00000003	01	00000002	\N	F	\N	2013-03-22	1	\N
00000003	01	00000002	\N	F	\N	2013-03-23	1	\N
00000003	01	00000002	\N	D	\N	2013-03-24	1	\N
00000003	01	00000002	\N	D	\N	2013-03-25	1	\N
00000003	01	00000002	\N	D	\N	2013-03-26	1	\N
00000003	01	00000002	\N	D	\N	2013-03-27	1	\N
00000003	01	00000002	\N	D	\N	2013-03-28	1	\N
00000003	01	00000002	\N	F	\N	2013-03-29	1	\N
00000003	01	00000002	\N	F	\N	2013-03-30	1	\N
00000003	01	00000002	\N	D	\N	2013-03-31	1	\N
00000004	01	00000002	\N	F	\N	2013-03-01	1	\N
00000004	01	00000002	\N	F	\N	2013-03-02	1	\N
00000004	01	00000002	\N	D	\N	2013-03-03	1	\N
00000004	01	00000002	\N	D	\N	2013-03-04	1	\N
00000004	01	00000002	\N	D	\N	2013-03-05	1	\N
00000004	01	00000002	\N	D	\N	2013-03-06	1	\N
00000004	01	00000002	\N	D	\N	2013-03-07	1	\N
00000004	01	00000002	\N	F	\N	2013-03-08	1	\N
00000004	01	00000002	\N	F	\N	2013-03-09	1	\N
00000004	01	00000002	\N	D	\N	2013-03-10	1	\N
00000004	01	00000002	\N	D	\N	2013-03-11	1	\N
00000004	01	00000002	\N	D	\N	2013-03-12	1	\N
00000004	01	00000002	\N	D	\N	2013-03-13	1	\N
00000004	01	00000002	\N	D	\N	2013-03-14	1	\N
00000004	01	00000002	\N	F	\N	2013-03-15	1	\N
00000004	01	00000002	\N	F	\N	2013-03-16	1	\N
00000004	01	00000002	\N	D	\N	2013-03-17	1	\N
00000004	01	00000002	\N	D	\N	2013-03-18	1	\N
00000004	01	00000002	\N	D	\N	2013-03-19	1	\N
00000004	01	00000002	\N	D	\N	2013-03-20	1	\N
00000004	01	00000002	\N	D	\N	2013-03-21	1	\N
00000004	01	00000002	\N	F	\N	2013-03-22	1	\N
00000004	01	00000002	\N	F	\N	2013-03-23	1	\N
00000004	01	00000002	\N	D	\N	2013-03-24	1	\N
00000004	01	00000002	\N	D	\N	2013-03-25	1	\N
00000004	01	00000002	\N	D	\N	2013-03-26	1	\N
00000004	01	00000002	\N	D	\N	2013-03-27	1	\N
00000004	01	00000002	\N	D	\N	2013-03-28	1	\N
00000004	01	00000002	\N	F	\N	2013-03-29	1	\N
00000004	01	00000002	\N	F	\N	2013-03-30	1	\N
00000004	01	00000002	\N	D	\N	2013-03-31	1	\N
00000005	01	00000002	\N	F	\N	2013-03-01	1	\N
00000005	01	00000002	\N	F	\N	2013-03-02	1	\N
00000005	01	00000002	\N	D	\N	2013-03-03	1	\N
00000005	01	00000002	\N	D	\N	2013-03-04	1	\N
00000005	01	00000002	\N	D	\N	2013-03-05	1	\N
00000005	01	00000002	\N	D	\N	2013-03-06	1	\N
00000005	01	00000002	\N	D	\N	2013-03-07	1	\N
00000005	01	00000002	\N	F	\N	2013-03-08	1	\N
00000005	01	00000002	\N	F	\N	2013-03-09	1	\N
00000005	01	00000002	\N	D	\N	2013-03-10	1	\N
00000005	01	00000002	\N	D	\N	2013-03-11	1	\N
00000005	01	00000002	\N	D	\N	2013-03-12	1	\N
00000005	01	00000002	\N	D	\N	2013-03-13	1	\N
00000005	01	00000002	\N	D	\N	2013-03-14	1	\N
00000005	01	00000002	\N	F	\N	2013-03-15	1	\N
00000005	01	00000002	\N	F	\N	2013-03-16	1	\N
00000005	01	00000002	\N	D	\N	2013-03-17	1	\N
00000005	01	00000002	\N	D	\N	2013-03-18	1	\N
00000005	01	00000002	\N	D	\N	2013-03-19	1	\N
00000005	01	00000002	\N	D	\N	2013-03-20	1	\N
00000005	01	00000002	\N	D	\N	2013-03-21	1	\N
00000005	01	00000002	\N	F	\N	2013-03-22	1	\N
00000005	01	00000002	\N	F	\N	2013-03-23	1	\N
00000005	01	00000002	\N	D	\N	2013-03-24	1	\N
00000005	01	00000002	\N	D	\N	2013-03-25	1	\N
00000005	01	00000002	\N	D	\N	2013-03-26	1	\N
00000005	01	00000002	\N	D	\N	2013-03-27	1	\N
00000005	01	00000002	\N	D	\N	2013-03-28	1	\N
00000005	01	00000002	\N	F	\N	2013-03-29	1	\N
00000005	01	00000002	\N	F	\N	2013-03-30	1	\N
00000005	01	00000002	\N	D	\N	2013-03-31	1	\N
00000006	01	00000002	\N	F	\N	2013-03-01	1	\N
00000006	01	00000002	\N	F	\N	2013-03-02	1	\N
00000006	01	00000002	\N	D	\N	2013-03-03	1	\N
00000001	01	00000002	\N	D	\N	2013-01-02	1	\N
00000001	01	00000002	\N	F	\N	2013-01-04	1	\N
00000001	01	00000002	\N	F	\N	2013-01-05	1	\N
00000001	01	00000002	\N	D	\N	2013-01-06	1	\N
00000001	01	00000002	\N	D	\N	2013-01-07	1	\N
00000001	01	00000002	\N	D	\N	2013-01-10	1	\N
00000001	01	00000002	\N	D	\N	2013-01-15	1	\N
00000001	01	00000002	\N	D	\N	2013-01-16	1	\N
00000001	01	00000002	\N	D	\N	2013-01-17	1	\N
00000001	01	00000002	\N	F	\N	2013-01-18	1	\N
00000001	01	00000002	\N	F	\N	2013-01-19	1	\N
00000001	01	00000002	\N	D	\N	2013-01-20	1	\N
00000001	01	00000002	\N	D	\N	2013-01-21	1	\N
00000001	01	00000002	\N	D	\N	2013-01-22	1	\N
00000001	01	00000002	\N	D	\N	2013-01-23	1	\N
00000001	01	00000002	\N	D	\N	2013-01-24	1	\N
00000001	01	00000002	\N	F	\N	2013-01-25	1	\N
00000001	01	00000002	\N	F	\N	2013-01-26	1	\N
00000001	01	00000002	\N	D	\N	2013-01-27	1	\N
00000001	01	00000002	\N	D	\N	2013-01-28	1	\N
00000001	01	00000002	\N	D	\N	2013-01-29	1	\N
00000001	01	00000002	\N	D	\N	2013-01-30	1	\N
00000001	01	00000002	\N	D	\N	2013-01-31	1	\N
00000002	01	00000002	\N	D	\N	2013-01-02	1	\N
00000002	01	00000002	\N	D	\N	2013-01-03	1	\N
00000002	01	00000002	\N	F	\N	2013-01-04	1	\N
00000002	01	00000002	\N	F	\N	2013-01-05	1	\N
00000002	01	00000002	\N	D	\N	2013-01-06	1	\N
00000002	01	00000002	\N	D	\N	2013-01-07	1	\N
00000002	01	00000002	\N	D	\N	2013-01-16	1	\N
00000002	01	00000002	\N	D	\N	2013-01-17	1	\N
00000002	01	00000002	\N	F	\N	2013-01-18	1	\N
00000002	01	00000002	\N	F	\N	2013-01-19	1	\N
00000002	01	00000002	\N	D	\N	2013-01-20	1	\N
00000002	01	00000002	\N	D	\N	2013-01-21	1	\N
00000002	01	00000002	\N	D	\N	2013-01-22	1	\N
00000002	01	00000002	\N	D	\N	2013-01-23	1	\N
00000002	01	00000002	\N	D	\N	2013-01-24	1	\N
00000002	01	00000002	\N	F	\N	2013-01-25	1	\N
00000002	01	00000002	\N	F	\N	2013-01-26	1	\N
00000002	01	00000002	\N	D	\N	2013-01-27	1	\N
00000002	01	00000002	\N	D	\N	2013-01-28	1	\N
00000002	01	00000002	\N	D	\N	2013-01-29	1	\N
00000002	01	00000002	\N	D	\N	2013-01-30	1	\N
00000002	01	00000002	\N	D	\N	2013-01-31	1	\N
00000003	01	00000002	\N	D	\N	2013-01-02	1	\N
00000003	01	00000002	\N	D	\N	2013-01-03	1	\N
00000003	01	00000002	\N	F	\N	2013-01-04	1	\N
00000003	01	00000002	\N	F	\N	2013-01-05	1	\N
00000003	01	00000002	\N	D	\N	2013-01-06	1	\N
00000003	01	00000002	\N	D	\N	2013-01-07	1	\N
00000003	01	00000002	\N	D	\N	2013-01-08	1	\N
00000003	01	00000002	\N	D	\N	2013-01-09	1	\N
00000003	01	00000002	\N	D	\N	2013-01-10	1	\N
00000003	01	00000002	\N	F	\N	2013-01-11	1	\N
00000003	01	00000002	\N	D	\N	2013-01-14	1	\N
00000003	01	00000002	\N	D	\N	2013-01-15	1	\N
00000003	01	00000002	\N	D	\N	2013-01-16	1	\N
00000003	01	00000002	\N	D	\N	2013-01-17	1	\N
00000003	01	00000002	\N	F	\N	2013-01-18	1	\N
00000003	01	00000002	\N	F	\N	2013-01-19	1	\N
00000003	01	00000002	\N	D	\N	2013-01-20	1	\N
00000003	01	00000002	\N	D	\N	2013-01-21	1	\N
00000003	01	00000002	\N	D	\N	2013-01-22	1	\N
00000003	01	00000002	\N	D	\N	2013-01-23	1	\N
00000003	01	00000002	\N	D	\N	2013-01-24	1	\N
00000003	01	00000002	\N	F	\N	2013-01-25	1	\N
00000003	01	00000002	\N	F	\N	2013-01-26	1	\N
00000003	01	00000002	\N	D	\N	2013-01-27	1	\N
00000003	01	00000002	\N	D	\N	2013-01-28	1	\N
00000003	01	00000002	\N	D	\N	2013-01-29	1	\N
00000003	01	00000002	\N	D	\N	2013-01-30	1	\N
00000003	01	00000002	\N	D	\N	2013-01-31	1	\N
00000004	01	00000002	\N	D	\N	2013-01-02	1	\N
00000004	01	00000002	\N	D	\N	2013-01-03	1	\N
00000004	01	00000002	\N	F	\N	2013-01-04	1	\N
00000004	01	00000002	\N	F	\N	2013-01-05	1	\N
00000004	01	00000002	\N	D	\N	2013-01-06	1	\N
00000004	01	00000002	\N	D	\N	2013-01-07	1	\N
00000004	01	00000002	\N	D	\N	2013-01-08	1	\N
00000004	01	00000002	\N	D	\N	2013-01-09	1	\N
00000004	01	00000002	\N	D	\N	2013-01-10	1	\N
00000004	01	00000002	\N	F	\N	2013-01-11	1	\N
00000004	01	00000002	\N	F	\N	2013-01-12	1	\N
00000004	01	00000002	\N	D	\N	2013-01-13	1	\N
00000004	01	00000002	\N	D	\N	2013-01-14	1	\N
00000004	01	00000002	\N	D	\N	2013-01-15	1	\N
00000004	01	00000002	\N	D	\N	2013-01-16	1	\N
00000004	01	00000002	\N	D	\N	2013-01-17	1	\N
00000004	01	00000002	\N	F	\N	2013-01-18	1	\N
00000004	01	00000002	\N	F	\N	2013-01-19	1	\N
00000004	01	00000002	\N	D	\N	2013-01-20	1	\N
00000004	01	00000002	\N	D	\N	2013-01-21	1	\N
00000004	01	00000002	\N	D	\N	2013-01-22	1	\N
00000004	01	00000002	\N	D	\N	2013-01-23	1	\N
00000004	01	00000002	\N	D	\N	2013-01-24	1	\N
00000004	01	00000002	\N	F	\N	2013-01-25	1	\N
00000004	01	00000002	\N	F	\N	2013-01-26	1	\N
00000004	01	00000002	\N	D	\N	2013-01-27	1	\N
00000004	01	00000002	\N	D	\N	2013-01-28	1	\N
00000004	01	00000002	\N	D	\N	2013-01-29	1	\N
00000004	01	00000002	\N	D	\N	2013-01-30	1	\N
00000004	01	00000002	\N	D	\N	2013-01-31	1	\N
00000005	01	00000002	\N	D	\N	2013-01-02	1	\N
00000005	01	00000002	\N	D	\N	2013-01-03	1	\N
00000001	01	00000002	\N	D	\N	2013-01-01	1	\N
00000002	01	00000002	\N	D	\N	2013-01-01	1	\N
00000002	01	00000002	\N	D	0000000042	2013-01-08	R	\N
00000002	01	00000002	\N	D	0000000042	2013-01-09	R	\N
00000002	01	00000002	\N	D	0000000042	2013-01-10	R	\N
00000002	01	00000002	\N	F	0000000042	2013-01-11	R	\N
00000001	01	00000002	\N	D	0000000045	2013-01-03	1	\N
00000005	01	00000002	\N	F	\N	2013-01-04	1	\N
00000005	01	00000002	\N	F	\N	2013-01-05	1	\N
00000005	01	00000002	\N	D	\N	2013-01-06	1	\N
00000005	01	00000002	\N	D	\N	2013-01-07	1	\N
00000005	01	00000002	\N	D	\N	2013-01-08	1	\N
00000005	01	00000002	\N	D	\N	2013-01-09	1	\N
00000005	01	00000002	\N	D	\N	2013-01-10	1	\N
00000005	01	00000002	\N	F	\N	2013-01-11	1	\N
00000005	01	00000002	\N	F	\N	2013-01-12	1	\N
00000005	01	00000002	\N	D	\N	2013-01-13	1	\N
00000005	01	00000002	\N	D	\N	2013-01-14	1	\N
00000005	01	00000002	\N	D	\N	2013-01-15	1	\N
00000005	01	00000002	\N	D	\N	2013-01-16	1	\N
00000005	01	00000002	\N	D	\N	2013-01-17	1	\N
00000005	01	00000002	\N	F	\N	2013-01-18	1	\N
00000005	01	00000002	\N	F	\N	2013-01-19	1	\N
00000005	01	00000002	\N	D	\N	2013-01-20	1	\N
00000005	01	00000002	\N	D	\N	2013-01-21	1	\N
00000005	01	00000002	\N	D	\N	2013-01-22	1	\N
00000005	01	00000002	\N	D	\N	2013-01-23	1	\N
00000005	01	00000002	\N	D	\N	2013-01-24	1	\N
00000005	01	00000002	\N	F	\N	2013-01-25	1	\N
00000005	01	00000002	\N	F	\N	2013-01-26	1	\N
00000003	01	00000002	\N	F	0000000053	2013-01-12	R	\N
00000003	01	00000002	\N	D	0000000053	2013-01-13	R	\N
00000005	01	00000002	\N	D	\N	2013-01-27	1	\N
00000005	01	00000002	\N	D	\N	2013-01-28	1	\N
00000005	01	00000002	\N	D	\N	2013-01-29	1	\N
00000005	01	00000002	\N	D	\N	2013-01-30	1	\N
00000005	01	00000002	\N	D	\N	2013-01-31	1	\N
00000006	01	00000002	\N	D	\N	2013-01-02	1	\N
00000006	01	00000002	\N	D	\N	2013-01-03	1	\N
00000006	01	00000002	\N	F	\N	2013-01-04	1	\N
00000006	01	00000002	\N	F	\N	2013-01-05	1	\N
00000006	01	00000002	\N	D	\N	2013-01-06	1	\N
00000006	01	00000002	\N	D	\N	2013-01-07	1	\N
00000006	01	00000002	\N	D	\N	2013-01-08	1	\N
00000006	01	00000002	\N	D	\N	2013-01-09	1	\N
00000006	01	00000002	\N	D	\N	2013-01-10	1	\N
00000006	01	00000002	\N	F	\N	2013-01-11	1	\N
00000006	01	00000002	\N	F	\N	2013-01-12	1	\N
00000006	01	00000002	\N	D	\N	2013-01-13	1	\N
00000006	01	00000002	\N	D	\N	2013-01-14	1	\N
00000006	01	00000002	\N	D	\N	2013-01-15	1	\N
00000006	01	00000002	\N	D	\N	2013-01-16	1	\N
00000006	01	00000002	\N	D	\N	2013-01-17	1	\N
00000006	01	00000002	\N	F	\N	2013-01-18	1	\N
00000006	01	00000002	\N	F	\N	2013-01-19	1	\N
00000006	01	00000002	\N	D	\N	2013-01-20	1	\N
00000006	01	00000002	\N	D	\N	2013-01-21	1	\N
00000006	01	00000002	\N	D	\N	2013-01-22	1	\N
00000006	01	00000002	\N	D	\N	2013-01-23	1	\N
00000006	01	00000002	\N	D	\N	2013-01-24	1	\N
00000006	01	00000002	\N	F	\N	2013-01-25	1	\N
00000006	01	00000002	\N	F	\N	2013-01-26	1	\N
00000006	01	00000002	\N	D	\N	2013-01-27	1	\N
00000006	01	00000002	\N	D	\N	2013-01-28	1	\N
00000006	01	00000002	\N	D	\N	2013-01-29	1	\N
00000006	01	00000002	\N	D	\N	2013-01-30	1	\N
00000006	01	00000002	\N	D	\N	2013-01-31	1	\N
00000007	01	00000002	\N	D	\N	2013-01-02	1	\N
00000007	01	00000002	\N	D	\N	2013-01-03	1	\N
00000007	01	00000002	\N	F	\N	2013-01-04	1	\N
00000007	01	00000002	\N	F	\N	2013-01-05	1	\N
00000007	01	00000002	\N	D	\N	2013-01-06	1	\N
00000007	01	00000002	\N	D	\N	2013-01-07	1	\N
00000007	01	00000002	\N	D	\N	2013-01-08	1	\N
00000007	01	00000002	\N	D	\N	2013-01-09	1	\N
00000007	01	00000002	\N	D	\N	2013-01-10	1	\N
00000007	01	00000002	\N	F	\N	2013-01-11	1	\N
00000007	01	00000002	\N	F	\N	2013-01-12	1	\N
00000007	01	00000002	\N	D	\N	2013-01-13	1	\N
00000007	01	00000002	\N	D	\N	2013-01-14	1	\N
00000007	01	00000002	\N	D	\N	2013-01-15	1	\N
00000007	01	00000002	\N	D	\N	2013-01-16	1	\N
00000007	01	00000002	\N	D	\N	2013-01-17	1	\N
00000007	01	00000002	\N	F	\N	2013-01-18	1	\N
00000007	01	00000002	\N	F	\N	2013-01-19	1	\N
00000007	01	00000002	\N	D	\N	2013-01-20	1	\N
00000007	01	00000002	\N	D	\N	2013-01-21	1	\N
00000007	01	00000002	\N	D	\N	2013-01-22	1	\N
00000007	01	00000002	\N	D	\N	2013-01-23	1	\N
00000007	01	00000002	\N	D	\N	2013-01-24	1	\N
00000007	01	00000002	\N	F	\N	2013-01-25	1	\N
00000007	01	00000002	\N	F	\N	2013-01-26	1	\N
00000007	01	00000002	\N	D	\N	2013-01-27	1	\N
00000007	01	00000002	\N	D	\N	2013-01-28	1	\N
00000007	01	00000002	\N	D	\N	2013-01-29	1	\N
00000007	01	00000002	\N	D	\N	2013-01-30	1	\N
00000007	01	00000002	\N	D	\N	2013-01-31	1	\N
00000076	01	00000002	\N	D	\N	2013-01-02	1	\N
00000076	01	00000002	\N	D	\N	2013-01-03	1	\N
00000076	01	00000002	\N	F	\N	2013-01-04	1	\N
00000076	01	00000002	\N	F	\N	2013-01-05	1	\N
00000076	01	00000002	\N	D	\N	2013-01-06	1	\N
00000076	01	00000002	\N	D	\N	2013-01-07	1	\N
00000076	01	00000002	\N	D	\N	2013-01-08	1	\N
00000076	01	00000002	\N	D	\N	2013-01-09	1	\N
00000076	01	00000002	\N	D	\N	2013-01-10	1	\N
00000076	01	00000002	\N	F	\N	2013-01-11	1	\N
00000076	01	00000002	\N	F	\N	2013-01-12	1	\N
00000076	01	00000002	\N	D	\N	2013-01-13	1	\N
00000076	01	00000002	\N	D	\N	2013-01-14	1	\N
00000076	01	00000002	\N	D	\N	2013-01-15	1	\N
00000076	01	00000002	\N	D	\N	2013-01-16	1	\N
00000076	01	00000002	\N	D	\N	2013-01-17	1	\N
00000076	01	00000002	\N	F	\N	2013-01-18	1	\N
00000076	01	00000002	\N	F	\N	2013-01-19	1	\N
00000076	01	00000002	\N	D	\N	2013-01-20	1	\N
00000076	01	00000002	\N	D	\N	2013-01-21	1	\N
00000076	01	00000002	\N	D	\N	2013-01-22	1	\N
00000076	01	00000002	\N	D	\N	2013-01-23	1	\N
00000076	01	00000002	\N	D	\N	2013-01-24	1	\N
00000076	01	00000002	\N	F	\N	2013-01-25	1	\N
00000076	01	00000002	\N	F	\N	2013-01-26	1	\N
00000076	01	00000002	\N	D	\N	2013-01-27	1	\N
00000076	01	00000002	\N	D	\N	2013-01-28	1	\N
00000076	01	00000002	\N	D	\N	2013-01-29	1	\N
00000076	01	00000002	\N	D	\N	2013-01-30	1	\N
00000076	01	00000002	\N	D	\N	2013-01-31	1	\N
00000006	01	00000002	\N	D	\N	2013-01-01	1	\N
00000007	01	00000002	\N	D	\N	2013-01-01	1	\N
00000076	01	00000002	\N	D	\N	2013-01-01	1	\N
00000006	01	00000002	\N	D	\N	2013-03-04	1	\N
00000006	01	00000002	\N	D	\N	2013-03-05	1	\N
00000006	01	00000002	\N	D	\N	2013-03-06	1	\N
00000006	01	00000002	\N	D	\N	2013-03-07	1	\N
00000006	01	00000002	\N	F	\N	2013-03-08	1	\N
00000006	01	00000002	\N	F	\N	2013-03-09	1	\N
00000006	01	00000002	\N	D	\N	2013-03-10	1	\N
00000006	01	00000002	\N	D	\N	2013-03-11	1	\N
00000006	01	00000002	\N	D	\N	2013-03-12	1	\N
00000006	01	00000002	\N	D	\N	2013-03-13	1	\N
00000006	01	00000002	\N	D	\N	2013-03-14	1	\N
00000006	01	00000002	\N	F	\N	2013-03-15	1	\N
00000006	01	00000002	\N	F	\N	2013-03-16	1	\N
00000006	01	00000002	\N	D	\N	2013-03-17	1	\N
00000006	01	00000002	\N	D	\N	2013-03-18	1	\N
00000006	01	00000002	\N	D	\N	2013-03-19	1	\N
00000006	01	00000002	\N	D	\N	2013-03-20	1	\N
00000006	01	00000002	\N	D	\N	2013-03-21	1	\N
00000006	01	00000002	\N	F	\N	2013-03-22	1	\N
00000006	01	00000002	\N	F	\N	2013-03-23	1	\N
00000006	01	00000002	\N	D	\N	2013-03-24	1	\N
00000006	01	00000002	\N	D	\N	2013-03-25	1	\N
00000006	01	00000002	\N	D	\N	2013-03-26	1	\N
00000006	01	00000002	\N	D	\N	2013-03-27	1	\N
00000006	01	00000002	\N	D	\N	2013-03-28	1	\N
00000006	01	00000002	\N	F	\N	2013-03-29	1	\N
00000006	01	00000002	\N	F	\N	2013-03-30	1	\N
00000006	01	00000002	\N	D	\N	2013-03-31	1	\N
00000007	01	00000002	\N	F	\N	2013-03-01	1	\N
00000007	01	00000002	\N	F	\N	2013-03-02	1	\N
00000007	01	00000002	\N	D	\N	2013-03-03	1	\N
00000007	01	00000002	\N	D	\N	2013-03-04	1	\N
00000007	01	00000002	\N	D	\N	2013-03-05	1	\N
00000007	01	00000002	\N	D	\N	2013-03-06	1	\N
00000007	01	00000002	\N	D	\N	2013-03-07	1	\N
00000007	01	00000002	\N	F	\N	2013-03-08	1	\N
00000007	01	00000002	\N	F	\N	2013-03-09	1	\N
00000007	01	00000002	\N	D	\N	2013-03-10	1	\N
00000007	01	00000002	\N	D	\N	2013-03-11	1	\N
00000007	01	00000002	\N	D	\N	2013-03-12	1	\N
00000007	01	00000002	\N	D	\N	2013-03-13	1	\N
00000007	01	00000002	\N	D	\N	2013-03-14	1	\N
00000007	01	00000002	\N	F	\N	2013-03-15	1	\N
00000007	01	00000002	\N	F	\N	2013-03-16	1	\N
00000007	01	00000002	\N	D	\N	2013-03-17	1	\N
00000007	01	00000002	\N	D	\N	2013-03-18	1	\N
00000007	01	00000002	\N	D	\N	2013-03-19	1	\N
00000007	01	00000002	\N	D	\N	2013-03-20	1	\N
00000007	01	00000002	\N	D	\N	2013-03-21	1	\N
00000007	01	00000002	\N	F	\N	2013-03-22	1	\N
00000007	01	00000002	\N	F	\N	2013-03-23	1	\N
00000007	01	00000002	\N	D	\N	2013-03-24	1	\N
00000007	01	00000002	\N	D	\N	2013-03-25	1	\N
00000007	01	00000002	\N	D	\N	2013-03-26	1	\N
00000007	01	00000002	\N	D	\N	2013-03-27	1	\N
00000007	01	00000002	\N	D	\N	2013-03-28	1	\N
00000007	01	00000002	\N	F	\N	2013-03-29	1	\N
00000007	01	00000002	\N	F	\N	2013-03-30	1	\N
00000007	01	00000002	\N	D	\N	2013-03-31	1	\N
00000076	01	00000002	\N	F	\N	2013-03-01	1	\N
00000076	01	00000002	\N	F	\N	2013-03-02	1	\N
00000076	01	00000002	\N	D	\N	2013-03-03	1	\N
00000021	02	00000002	\N	D	\N	2012-09-03	0	\N
00000021	02	00000002	\N	D	\N	2012-09-04	0	\N
00000021	02	00000002	\N	D	\N	2012-09-05	0	\N
00000021	02	00000002	\N	D	\N	2012-09-06	0	\N
00000021	02	00000002	\N	F	\N	2012-09-07	0	\N
00000021	02	00000002	\N	F	\N	2012-09-08	0	\N
00000021	02	00000002	\N	D	\N	2012-09-09	0	\N
00000021	02	00000002	\N	D	\N	2012-09-10	0	\N
00000021	02	00000002	\N	D	\N	2012-09-11	0	\N
00000021	02	00000002	\N	D	\N	2012-09-12	0	\N
00000021	02	00000002	\N	D	\N	2012-09-13	0	\N
00000021	02	00000002	\N	F	\N	2012-09-14	0	\N
00000021	02	00000002	\N	F	\N	2012-09-15	0	\N
00000021	02	00000002	\N	D	\N	2012-09-16	0	\N
00000021	02	00000002	\N	D	\N	2012-09-17	0	\N
00000007	01	00000002	\N	D	\N	2012-10-29	1	\N
00000007	01	00000002	\N	D	\N	2012-10-30	1	\N
00000007	01	00000002	\N	D	\N	2012-10-31	1	\N
00000021	02	00000002	\N	D	\N	2012-09-18	0	\N
00000021	02	00000002	\N	D	\N	2012-09-19	0	\N
00000021	02	00000002	\N	D	\N	2012-09-20	0	\N
00000021	02	00000002	\N	F	\N	2012-09-21	0	\N
00000021	02	00000002	\N	F	\N	2012-09-22	0	\N
00000021	02	00000002	\N	D	\N	2012-09-23	0	\N
00000021	02	00000002	\N	D	\N	2012-09-24	0	\N
00000021	02	00000002	\N	D	\N	2012-09-25	0	\N
00000021	02	00000002	\N	D	\N	2012-09-26	0	\N
00000021	02	00000002	\N	D	\N	2012-09-27	0	\N
00000021	02	00000002	\N	F	\N	2012-09-28	0	\N
00000021	02	00000002	\N	F	\N	2012-09-29	0	\N
00000021	02	00000002	\N	D	\N	2012-09-30	0	\N
00000007	01	00000002	\N	D	\N	2012-10-27	1	\N
00000007	01	00000002	\N	D	\N	2012-10-28	1	\N
00000022	02	00000002	\N	F	\N	2012-09-01	0	\N
00000022	02	00000002	\N	D	\N	2012-09-02	0	\N
00000022	02	00000002	\N	D	\N	2012-09-03	0	\N
00000076	01	00000002	\N	D	\N	2013-03-04	1	\N
00000001	01	00000002	\N	D	\N	2012-10-20	1	\N
00000076	01	00000002	\N	D	\N	2013-03-05	1	\N
00000076	01	00000002	\N	D	\N	2013-03-06	1	\N
00000076	01	00000002	\N	D	\N	2013-03-07	1	\N
00000076	01	00000002	\N	F	\N	2013-03-08	1	\N
00000076	01	00000002	\N	F	\N	2013-03-09	1	\N
00000076	01	00000002	\N	D	\N	2013-03-10	1	\N
00000076	01	00000002	\N	D	\N	2013-03-11	1	\N
00000076	01	00000002	\N	D	\N	2013-03-12	1	\N
00000076	01	00000002	\N	D	\N	2013-03-13	1	\N
00000076	01	00000002	\N	D	\N	2013-03-14	1	\N
00000076	01	00000002	\N	F	\N	2013-03-15	1	\N
00000001	01	00000002	\N	D	\N	2012-10-18	1	\N
00000001	01	00000002	\N	D	\N	2012-10-19	1	\N
00000076	01	00000002	\N	F	\N	2013-03-16	1	\N
00000076	01	00000002	\N	D	\N	2013-03-17	1	\N
00000076	01	00000002	\N	D	\N	2013-03-18	1	\N
00000076	01	00000002	\N	D	\N	2013-03-19	1	\N
00000076	01	00000002	\N	D	\N	2013-03-20	1	\N
00000076	01	00000002	\N	D	\N	2013-03-21	1	\N
00000076	01	00000002	\N	F	\N	2013-03-22	1	\N
00000076	01	00000002	\N	F	\N	2013-03-23	1	\N
00000076	01	00000002	\N	D	\N	2013-03-24	1	\N
00000076	01	00000002	\N	D	\N	2013-03-25	1	\N
00000076	01	00000002	\N	D	\N	2013-03-26	1	\N
00000076	01	00000002	\N	D	\N	2013-03-27	1	\N
00000022	02	00000002	\N	D	\N	2012-09-04	0	\N
00000022	02	00000002	\N	D	\N	2012-09-05	0	\N
00000022	02	00000002	\N	D	\N	2012-09-06	0	\N
00000022	02	00000002	\N	F	\N	2012-09-07	0	\N
00000022	02	00000002	\N	F	\N	2012-09-08	0	\N
00000022	02	00000002	\N	D	\N	2012-09-09	0	\N
00000022	02	00000002	\N	D	\N	2012-09-10	0	\N
00000022	02	00000002	\N	D	\N	2012-09-11	0	\N
00000022	02	00000002	\N	D	\N	2012-09-12	0	\N
00000022	02	00000002	\N	D	\N	2012-09-13	0	\N
00000022	02	00000002	\N	F	\N	2012-09-14	0	\N
00000022	02	00000002	\N	F	\N	2012-09-15	0	\N
00000022	02	00000002	\N	D	\N	2012-09-16	0	\N
00000022	02	00000002	\N	D	\N	2012-09-17	0	\N
00000022	02	00000002	\N	D	\N	2012-09-18	0	\N
00000022	02	00000002	\N	D	\N	2012-09-19	0	\N
00000022	02	00000002	\N	D	\N	2012-09-20	0	\N
00000022	02	00000002	\N	F	\N	2012-09-21	0	\N
00000022	02	00000002	\N	F	\N	2012-09-22	0	\N
00000022	02	00000002	\N	D	\N	2012-09-23	0	\N
00000022	02	00000002	\N	D	\N	2012-09-24	0	\N
00000022	02	00000002	\N	D	\N	2012-09-25	0	\N
00000022	02	00000002	\N	D	\N	2012-09-26	0	\N
00000022	02	00000002	\N	D	\N	2012-09-27	0	\N
00000022	02	00000002	\N	F	\N	2012-09-28	0	\N
00000022	02	00000002	\N	F	\N	2012-09-29	0	\N
00000022	02	00000002	\N	D	\N	2012-09-30	0	\N
00000023	02	00000002	\N	F	\N	2012-09-01	0	\N
00000023	02	00000002	\N	D	\N	2012-09-02	0	\N
00000023	02	00000002	\N	D	\N	2012-09-03	0	\N
00000023	02	00000002	\N	D	\N	2012-09-04	0	\N
00000023	02	00000002	\N	D	\N	2012-09-05	0	\N
00000023	02	00000002	\N	D	\N	2012-09-06	0	\N
00000023	02	00000002	\N	F	\N	2012-09-07	0	\N
00000023	02	00000002	\N	F	\N	2012-09-08	0	\N
00000023	02	00000002	\N	D	\N	2012-09-09	0	\N
00000023	02	00000002	\N	D	\N	2012-09-10	0	\N
00000023	02	00000002	\N	D	\N	2012-09-11	0	\N
00000023	02	00000002	\N	D	\N	2012-09-12	0	\N
00000023	02	00000002	\N	D	\N	2012-09-13	0	\N
00000023	02	00000002	\N	F	\N	2012-09-14	0	\N
00000023	02	00000002	\N	F	\N	2012-09-15	0	\N
00000023	02	00000002	\N	D	\N	2012-09-16	0	\N
00000023	02	00000002	\N	D	\N	2012-09-17	0	\N
00000023	02	00000002	\N	D	\N	2012-09-18	0	\N
00000023	02	00000002	\N	D	\N	2012-09-19	0	\N
00000023	02	00000002	\N	D	\N	2012-09-20	0	\N
00000023	02	00000002	\N	F	\N	2012-09-21	0	\N
00000023	02	00000002	\N	F	\N	2012-09-22	0	\N
00000023	02	00000002	\N	D	\N	2012-09-23	0	\N
00000023	02	00000002	\N	D	\N	2012-09-24	0	\N
00000023	02	00000002	\N	D	\N	2012-09-25	0	\N
00000023	02	00000002	\N	D	\N	2012-09-26	0	\N
00000023	02	00000002	\N	D	\N	2012-09-27	0	\N
00000023	02	00000002	\N	F	\N	2012-09-28	0	\N
00000023	02	00000002	\N	F	\N	2012-09-29	0	\N
00000023	02	00000002	\N	D	\N	2012-09-30	0	\N
00000024	02	00000002	\N	F	\N	2012-09-01	0	\N
00000024	02	00000002	\N	D	\N	2012-09-02	0	\N
00000024	02	00000002	\N	D	\N	2012-09-03	0	\N
00000024	02	00000002	\N	D	\N	2012-09-04	0	\N
00000024	02	00000002	\N	D	\N	2012-09-05	0	\N
00000024	02	00000002	\N	D	\N	2012-09-06	0	\N
00000024	02	00000002	\N	F	\N	2012-09-07	0	\N
00000024	02	00000002	\N	F	\N	2012-09-08	0	\N
00000024	02	00000002	\N	D	\N	2012-09-09	0	\N
00000024	02	00000002	\N	D	\N	2012-09-10	0	\N
00000024	02	00000002	\N	D	\N	2012-09-11	0	\N
00000024	02	00000002	\N	D	\N	2012-09-12	0	\N
00000024	02	00000002	\N	D	\N	2012-09-13	0	\N
00000024	02	00000002	\N	F	\N	2012-09-14	0	\N
00000024	02	00000002	\N	F	\N	2012-09-15	0	\N
00000024	02	00000002	\N	D	\N	2012-09-16	0	\N
00000024	02	00000002	\N	D	\N	2012-09-17	0	\N
00000024	02	00000002	\N	D	\N	2012-09-18	0	\N
00000024	02	00000002	\N	D	\N	2012-09-19	0	\N
00000024	02	00000002	\N	D	\N	2012-09-20	0	\N
00000024	02	00000002	\N	F	\N	2012-09-21	0	\N
00000024	02	00000002	\N	F	\N	2012-09-22	0	\N
00000024	02	00000002	\N	D	\N	2012-09-23	0	\N
00000024	02	00000002	\N	D	\N	2012-09-24	0	\N
00000024	02	00000002	\N	D	\N	2012-09-25	0	\N
00000024	02	00000002	\N	D	\N	2012-09-26	0	\N
00000024	02	00000002	\N	D	\N	2012-09-27	0	\N
00000024	02	00000002	\N	F	\N	2012-09-28	0	\N
00000024	02	00000002	\N	F	\N	2012-09-29	0	\N
00000024	02	00000002	\N	D	\N	2012-09-30	0	\N
00000025	02	00000002	\N	F	\N	2012-09-01	0	\N
00000025	02	00000002	\N	D	\N	2012-09-02	0	\N
00000025	02	00000002	\N	D	\N	2012-09-03	0	\N
00000025	02	00000002	\N	D	\N	2012-09-04	0	\N
00000025	02	00000002	\N	D	\N	2012-09-05	0	\N
00000025	02	00000002	\N	D	\N	2012-09-06	0	\N
00000025	02	00000002	\N	F	\N	2012-09-07	0	\N
00000025	02	00000002	\N	F	\N	2012-09-08	0	\N
00000025	02	00000002	\N	D	\N	2012-09-09	0	\N
00000025	02	00000002	\N	D	\N	2012-09-10	0	\N
00000025	02	00000002	\N	D	\N	2012-09-11	0	\N
00000025	02	00000002	\N	D	\N	2012-09-12	0	\N
00000025	02	00000002	\N	D	\N	2012-09-13	0	\N
00000025	02	00000002	\N	F	\N	2012-09-14	0	\N
00000025	02	00000002	\N	F	\N	2012-09-15	0	\N
00000025	02	00000002	\N	D	\N	2012-09-16	0	\N
00000025	02	00000002	\N	D	\N	2012-09-17	0	\N
00000025	02	00000002	\N	D	\N	2012-09-18	0	\N
00000025	02	00000002	\N	D	\N	2012-09-19	0	\N
00000025	02	00000002	\N	D	\N	2012-09-20	0	\N
00000025	02	00000002	\N	F	\N	2012-09-21	0	\N
00000025	02	00000002	\N	F	\N	2012-09-22	0	\N
00000025	02	00000002	\N	D	\N	2012-09-23	0	\N
00000025	02	00000002	\N	D	\N	2012-09-24	0	\N
00000025	02	00000002	\N	D	\N	2012-09-25	0	\N
00000025	02	00000002	\N	D	\N	2012-09-26	0	\N
00000025	02	00000002	\N	D	\N	2012-09-27	0	\N
00000025	02	00000002	\N	F	\N	2012-09-28	0	\N
00000025	02	00000002	\N	F	\N	2012-09-29	0	\N
00000025	02	00000002	\N	D	\N	2012-09-30	0	\N
00000026	02	00000002	\N	F	\N	2012-09-01	0	\N
00000026	02	00000002	\N	D	\N	2012-09-02	0	\N
00000026	02	00000002	\N	D	\N	2012-09-03	0	\N
00000026	02	00000002	\N	D	\N	2012-09-04	0	\N
00000026	02	00000002	\N	D	\N	2012-09-05	0	\N
00000026	02	00000002	\N	D	\N	2012-09-06	0	\N
00000026	02	00000002	\N	F	\N	2012-09-07	0	\N
00000026	02	00000002	\N	F	\N	2012-09-08	0	\N
00000026	02	00000002	\N	D	\N	2012-09-09	0	\N
00000026	02	00000002	\N	D	\N	2012-09-10	0	\N
00000003	01	00000002	\N	D	\N	2012-10-27	1	\N
00000003	01	00000002	\N	D	\N	2012-10-28	1	\N
00000004	01	00000002	\N	D	\N	2012-10-06	1	\N
00000004	01	00000002	\N	D	\N	2012-10-07	1	\N
00000002	01	00000002	\N	D	\N	2012-10-14	1	\N
00000003	01	00000002	\N	D	\N	2012-10-06	1	\N
00000003	01	00000002	\N	D	\N	2012-10-07	1	\N
00000003	01	00000002	\N	D	\N	2012-10-13	1	\N
00000003	01	00000002	\N	D	\N	2012-10-14	1	\N
00000003	01	00000002	\N	D	\N	2012-10-20	1	\N
00000003	01	00000002	\N	D	\N	2012-10-21	1	\N
00000001	01	00000002	\N	D	\N	2012-10-21	1	\N
00000001	01	00000002	\N	D	\N	2012-10-27	1	\N
00000001	01	00000002	\N	D	\N	2012-10-28	1	\N
00000002	01	00000002	\N	D	\N	2012-10-06	1	\N
00000002	01	00000002	\N	D	\N	2012-10-07	1	\N
00000026	02	00000002	\N	D	\N	2012-09-11	0	\N
00000026	02	00000002	\N	D	\N	2012-09-12	0	\N
00000003	01	00000002	\N	D	\N	2012-10-29	1	\N
00000003	01	00000002	\N	D	\N	2012-10-30	1	\N
00000003	01	00000002	\N	D	\N	2012-10-31	1	\N
00000004	01	00000002	\N	D	\N	2012-10-01	1	\N
00000004	01	00000002	\N	D	\N	2012-10-02	1	\N
00000004	01	00000002	\N	D	\N	2012-10-03	1	\N
00000004	01	00000002	\N	D	\N	2012-10-04	1	\N
00000004	01	00000002	\N	D	\N	2012-10-05	1	\N
00000026	02	00000002	\N	D	\N	2012-09-13	0	\N
00000026	02	00000002	\N	F	\N	2012-09-14	0	\N
00000004	01	00000002	\N	D	\N	2012-10-08	1	\N
00000004	01	00000002	\N	D	\N	2012-10-09	1	\N
00000004	01	00000002	\N	D	\N	2012-10-10	1	\N
00000004	01	00000002	\N	D	\N	2012-10-11	1	\N
00000026	02	00000002	\N	F	\N	2012-09-15	0	\N
00000002	01	00000002	\N	D	\N	2012-10-15	1	\N
00000002	01	00000002	\N	D	\N	2012-10-16	1	\N
00000002	01	00000002	\N	D	\N	2012-10-17	1	\N
00000002	01	00000002	\N	D	\N	2012-10-18	1	\N
00000003	01	00000002	\N	D	\N	2012-10-05	1	\N
00000026	02	00000002	\N	D	\N	2012-09-16	0	\N
00000026	02	00000002	\N	D	\N	2012-09-17	0	\N
00000003	01	00000002	\N	D	\N	2012-10-08	1	\N
00000003	01	00000002	\N	D	\N	2012-10-09	1	\N
00000003	01	00000002	\N	D	\N	2012-10-10	1	\N
00000003	01	00000002	\N	D	\N	2012-10-11	1	\N
00000003	01	00000002	\N	D	\N	2012-10-12	1	\N
00000026	02	00000002	\N	D	\N	2012-09-18	0	\N
00000026	02	00000002	\N	D	\N	2012-09-19	0	\N
00000003	01	00000002	\N	D	\N	2012-10-15	1	\N
00000003	01	00000002	\N	D	\N	2012-10-16	1	\N
00000003	01	00000002	\N	D	\N	2012-10-17	1	\N
00000003	01	00000002	\N	D	\N	2012-10-18	1	\N
00000003	01	00000002	\N	D	\N	2012-10-19	1	\N
00000026	02	00000002	\N	D	\N	2012-09-20	0	\N
00000026	02	00000002	\N	F	\N	2012-09-21	0	\N
00000003	01	00000002	\N	D	\N	2012-10-22	1	\N
00000003	01	00000002	\N	D	\N	2012-10-23	1	\N
00000003	01	00000002	\N	D	\N	2012-10-24	1	\N
00000003	01	00000002	\N	D	\N	2012-10-25	1	\N
00000003	01	00000002	\N	D	\N	2012-10-26	1	\N
00000001	01	00000002	\N	D	\N	2012-10-22	1	\N
00000001	01	00000002	\N	D	\N	2012-10-23	1	\N
00000001	01	00000002	\N	D	\N	2012-10-24	1	\N
00000001	01	00000002	\N	D	\N	2012-10-25	1	\N
00000001	01	00000002	\N	D	\N	2012-10-26	1	\N
00000001	01	00000002	\N	D	\N	2012-10-29	1	\N
00000001	01	00000002	\N	D	\N	2012-10-30	1	\N
00000001	01	00000002	\N	D	\N	2012-10-31	1	\N
00000002	01	00000002	\N	D	\N	2012-10-02	1	\N
00000002	01	00000002	\N	D	\N	2012-10-03	1	\N
00000002	01	00000002	\N	D	\N	2012-10-04	1	\N
00000002	01	00000002	\N	D	\N	2012-10-05	1	\N
00000002	01	00000002	\N	D	\N	2012-10-08	1	\N
00000002	01	00000002	\N	D	\N	2012-10-09	1	\N
00000002	01	00000002	\N	D	\N	2012-10-10	1	\N
00000002	01	00000002	\N	D	\N	2012-10-11	1	\N
00000002	01	00000002	\N	D	\N	2012-10-12	1	\N
00000004	01	00000002	\N	D	\N	2012-10-16	1	\N
00000004	01	00000002	\N	D	\N	2012-10-17	1	\N
00000004	01	00000002	\N	D	\N	2012-10-18	1	\N
00000004	01	00000002	\N	D	\N	2012-10-19	1	\N
00000002	01	00000002	\N	D	\N	2012-10-13	1	\N
00000004	01	00000002	\N	D	\N	2012-10-20	1	\N
00000004	01	00000002	\N	D	\N	2012-10-21	1	\N
00000004	01	00000002	\N	D	\N	2012-10-27	1	\N
00000004	01	00000002	\N	D	\N	2012-10-28	1	\N
00000026	02	00000002	\N	F	\N	2012-09-22	0	\N
00000026	02	00000002	\N	D	\N	2012-09-23	0	\N
00000026	02	00000002	\N	D	\N	2012-09-24	0	\N
00000026	02	00000002	\N	D	\N	2012-09-25	0	\N
00000026	02	00000002	\N	D	\N	2012-09-26	0	\N
00000026	02	00000002	\N	D	\N	2012-09-27	0	\N
00000026	02	00000002	\N	F	\N	2012-09-28	0	\N
00000026	02	00000002	\N	F	\N	2012-09-29	0	\N
00000026	02	00000002	\N	D	\N	2012-09-30	0	\N
00000027	02	00000002	\N	F	\N	2012-09-01	0	\N
00000027	02	00000002	\N	D	\N	2012-09-02	0	\N
00000027	02	00000002	\N	D	\N	2012-09-03	0	\N
00000027	02	00000002	\N	D	\N	2012-09-04	0	\N
00000027	02	00000002	\N	D	\N	2012-09-05	0	\N
00000027	02	00000002	\N	D	\N	2012-09-06	0	\N
00000027	02	00000002	\N	F	\N	2012-09-07	0	\N
00000027	02	00000002	\N	F	\N	2012-09-08	0	\N
00000027	02	00000002	\N	D	\N	2012-09-09	0	\N
00000027	02	00000002	\N	D	\N	2012-09-10	0	\N
00000027	02	00000002	\N	D	\N	2012-09-11	0	\N
00000027	02	00000002	\N	D	\N	2012-09-12	0	\N
00000027	02	00000002	\N	D	\N	2012-09-13	0	\N
00000004	01	00000002	\N	D	\N	2012-10-22	1	\N
00000004	01	00000002	\N	D	\N	2012-10-23	1	\N
00000004	01	00000002	\N	D	\N	2012-10-24	1	\N
00000004	01	00000002	\N	D	\N	2012-10-25	1	\N
00000004	01	00000002	\N	D	\N	2012-10-26	1	\N
00000004	01	00000002	\N	D	\N	2012-10-29	1	\N
00000004	01	00000002	\N	D	\N	2012-10-30	1	\N
00000027	02	00000002	\N	F	\N	2012-09-14	0	\N
00000027	02	00000002	\N	F	\N	2012-09-15	0	\N
00000027	02	00000002	\N	D	\N	2012-09-16	0	\N
00000027	02	00000002	\N	D	\N	2012-09-17	0	\N
00000027	02	00000002	\N	D	\N	2012-09-18	0	\N
00000027	02	00000002	\N	D	\N	2012-09-19	0	\N
00000027	02	00000002	\N	D	\N	2012-09-20	0	\N
00000027	02	00000002	\N	F	\N	2012-09-21	0	\N
00000027	02	00000002	\N	F	\N	2012-09-22	0	\N
00000027	02	00000002	\N	D	\N	2012-09-23	0	\N
00000027	02	00000002	\N	D	\N	2012-09-24	0	\N
00000027	02	00000002	\N	D	\N	2012-09-25	0	\N
00000027	02	00000002	\N	D	\N	2012-09-26	0	\N
00000027	02	00000002	\N	D	\N	2012-09-27	0	\N
00000027	02	00000002	\N	F	\N	2012-09-28	0	\N
00000027	02	00000002	\N	F	\N	2012-09-29	0	\N
00000027	02	00000002	\N	D	\N	2012-09-30	0	\N
00000028	02	00000002	\N	F	\N	2012-09-01	0	\N
00000028	02	00000002	\N	D	\N	2012-09-02	0	\N
00000028	02	00000002	\N	D	\N	2012-09-03	0	\N
00000028	02	00000002	\N	D	\N	2012-09-04	0	\N
00000028	02	00000002	\N	D	\N	2012-09-05	0	\N
00000028	02	00000002	\N	D	\N	2012-09-06	0	\N
00000028	02	00000002	\N	F	\N	2012-09-07	0	\N
00000028	02	00000002	\N	F	\N	2012-09-08	0	\N
00000028	02	00000002	\N	D	\N	2012-09-09	0	\N
00000028	02	00000002	\N	D	\N	2012-09-10	0	\N
00000028	02	00000002	\N	D	\N	2012-09-11	0	\N
00000028	02	00000002	\N	D	\N	2012-09-12	0	\N
00000028	02	00000002	\N	D	\N	2012-09-13	0	\N
00000028	02	00000002	\N	F	\N	2012-09-14	0	\N
00000028	02	00000002	\N	F	\N	2012-09-15	0	\N
00000028	02	00000002	\N	D	\N	2012-09-16	0	\N
00000028	02	00000002	\N	D	\N	2012-09-17	0	\N
00000028	02	00000002	\N	D	\N	2012-09-18	0	\N
00000028	02	00000002	\N	D	\N	2012-09-19	0	\N
00000028	02	00000002	\N	D	\N	2012-09-20	0	\N
00000028	02	00000002	\N	F	\N	2012-09-21	0	\N
00000028	02	00000002	\N	F	\N	2012-09-22	0	\N
00000028	02	00000002	\N	D	\N	2012-09-23	0	\N
00000004	01	00000002	\N	D	\N	2012-10-31	1	\N
00000005	01	00000002	\N	D	\N	2012-10-01	1	\N
00000005	01	00000002	\N	D	\N	2012-10-03	1	\N
00000005	01	00000002	\N	D	\N	2012-10-04	1	\N
00000005	01	00000002	\N	D	\N	2012-10-05	1	\N
00000005	01	00000002	\N	D	\N	2012-10-08	1	\N
00000005	01	00000002	\N	D	\N	2012-10-09	1	\N
00000005	01	00000002	\N	D	\N	2012-10-10	1	\N
00000005	01	00000002	\N	D	\N	2012-10-11	1	\N
00000005	01	00000002	\N	D	\N	2012-10-12	1	\N
00000005	01	00000002	\N	D	\N	2012-10-15	1	\N
00000005	01	00000002	\N	D	\N	2012-10-16	1	\N
00000005	01	00000002	\N	D	\N	2012-10-17	1	\N
00000005	01	00000002	\N	D	\N	2012-10-18	1	\N
00000005	01	00000002	\N	D	\N	2012-10-19	1	\N
00000005	01	00000002	\N	D	\N	2012-10-22	1	\N
00000005	01	00000002	\N	D	\N	2012-10-23	1	\N
00000005	01	00000002	\N	D	\N	2012-10-24	1	\N
00000005	01	00000002	\N	D	\N	2012-10-25	1	\N
00000005	01	00000002	\N	D	\N	2012-10-26	1	\N
00000005	01	00000002	\N	D	\N	2012-10-29	1	\N
00000005	01	00000002	\N	D	\N	2012-10-30	1	\N
00000005	01	00000002	\N	D	\N	2012-10-31	1	\N
00000006	01	00000002	\N	D	\N	2012-10-01	1	\N
00000006	01	00000002	\N	D	\N	2012-10-03	1	\N
00000006	01	00000002	\N	D	\N	2012-10-04	1	\N
00000006	01	00000002	\N	D	\N	2012-10-05	1	\N
00000006	01	00000002	\N	D	\N	2012-10-08	1	\N
00000006	01	00000002	\N	D	\N	2012-10-09	1	\N
00000006	01	00000002	\N	D	\N	2012-10-10	1	\N
00000006	01	00000002	\N	D	\N	2012-10-11	1	\N
00000006	01	00000002	\N	D	\N	2012-10-12	1	\N
00000006	01	00000002	\N	D	\N	2012-10-15	1	\N
00000006	01	00000002	\N	D	\N	2012-10-16	1	\N
00000006	01	00000002	\N	D	\N	2012-10-17	1	\N
00000006	01	00000002	\N	D	\N	2012-10-18	1	\N
00000006	01	00000002	\N	D	\N	2012-10-19	1	\N
00000006	01	00000002	\N	D	\N	2012-10-22	1	\N
00000006	01	00000002	\N	D	\N	2012-10-23	1	\N
00000006	01	00000002	\N	D	\N	2012-10-24	1	\N
00000006	01	00000002	\N	D	\N	2012-10-25	1	\N
00000006	01	00000002	\N	D	\N	2012-10-26	1	\N
00000006	01	00000002	\N	D	\N	2012-10-29	1	\N
00000006	01	00000002	\N	D	\N	2012-10-30	1	\N
00000006	01	00000002	\N	D	\N	2012-10-31	1	\N
00000007	01	00000002	\N	D	\N	2012-10-01	1	\N
00000007	01	00000002	\N	D	\N	2012-10-03	1	\N
00000007	01	00000002	\N	D	\N	2012-10-04	1	\N
00000007	01	00000002	\N	D	\N	2012-10-05	1	\N
00000007	01	00000002	\N	D	\N	2012-10-08	1	\N
00000007	01	00000002	\N	D	\N	2012-10-09	1	\N
00000007	01	00000002	\N	D	\N	2012-10-10	1	\N
00000007	01	00000002	\N	D	\N	2012-10-11	1	\N
00000007	01	00000002	\N	D	\N	2012-10-12	1	\N
00000007	01	00000002	\N	D	\N	2012-10-15	1	\N
00000007	01	00000002	\N	D	\N	2012-10-16	1	\N
00000007	01	00000002	\N	D	\N	2012-10-17	1	\N
00000007	01	00000002	\N	D	\N	2012-10-18	1	\N
00000007	01	00000002	\N	D	\N	2012-10-19	1	\N
00000007	01	00000002	\N	D	\N	2012-10-22	1	\N
00000007	01	00000002	\N	D	\N	2012-10-23	1	\N
00000007	01	00000002	\N	D	\N	2012-10-24	1	\N
00000007	01	00000002	\N	D	\N	2012-10-25	1	\N
00000007	01	00000002	\N	D	\N	2012-10-26	1	\N
00000001	01	00000002	\N	D	\N	2012-10-01	0	\N
00000001	01	00000002	\N	D	\N	2012-10-02	0	\N
00000001	01	00000002	\N	D	\N	2012-10-03	0	\N
00000001	01	00000002	\N	D	\N	2012-10-04	0	\N
00000001	01	00000002	\N	D	\N	2012-10-05	0	\N
00000001	01	00000002	\N	D	\N	2012-10-08	0	\N
00000001	01	00000002	\N	D	\N	2012-10-09	0	\N
00000001	01	00000002	\N	D	\N	2012-10-10	1	\N
00000001	01	00000002	\N	D	\N	2012-10-11	1	\N
00000001	01	00000002	\N	D	\N	2012-10-12	1	\N
00000001	01	00000002	\N	D	\N	2012-10-15	1	\N
00000001	01	00000002	\N	D	\N	2012-10-16	1	\N
00000001	01	00000002	\N	D	\N	2012-10-17	1	\N
00000002	01	00000002	\N	D	\N	2012-10-01	1	\N
00000002	01	00000002	\N	D	\N	2012-10-19	1	\N
00000002	01	00000002	\N	D	\N	2012-10-22	1	\N
00000002	01	00000002	\N	D	\N	2012-10-23	1	\N
00000002	01	00000002	\N	D	\N	2012-10-24	1	\N
00000002	01	00000002	\N	D	\N	2012-10-25	1	\N
00000002	01	00000002	\N	D	\N	2012-10-26	1	\N
00000002	01	00000002	\N	D	\N	2012-10-29	1	\N
00000002	01	00000002	\N	D	\N	2012-10-30	1	\N
00000002	01	00000002	\N	D	\N	2012-10-31	1	\N
00000003	01	00000002	\N	D	\N	2012-10-01	1	\N
00000003	01	00000002	\N	D	\N	2012-10-02	1	\N
00000028	02	00000002	\N	D	\N	2012-09-24	0	\N
00000028	02	00000002	\N	D	\N	2012-09-25	0	\N
00000028	02	00000002	\N	D	\N	2012-09-26	0	\N
00000028	02	00000002	\N	D	\N	2012-09-27	0	\N
00000028	02	00000002	\N	F	\N	2012-09-28	0	\N
00000028	02	00000002	\N	F	\N	2012-09-29	0	\N
00000028	02	00000002	\N	D	\N	2012-09-30	0	\N
00000029	02	00000002	\N	F	\N	2012-09-01	0	\N
00000029	02	00000002	\N	D	\N	2012-09-02	0	\N
00000029	02	00000002	\N	D	\N	2012-09-03	0	\N
00000029	02	00000002	\N	D	\N	2012-09-04	0	\N
00000029	02	00000002	\N	D	\N	2012-09-05	0	\N
00000029	02	00000002	\N	D	\N	2012-09-06	0	\N
00000029	02	00000002	\N	F	\N	2012-09-07	0	\N
00000029	02	00000002	\N	F	\N	2012-09-08	0	\N
00000029	02	00000002	\N	D	\N	2012-09-09	0	\N
00000029	02	00000002	\N	D	\N	2012-09-10	0	\N
00000029	02	00000002	\N	D	\N	2012-09-11	0	\N
00000029	02	00000002	\N	D	\N	2012-09-12	0	\N
00000029	02	00000002	\N	D	\N	2012-09-13	0	\N
00000029	02	00000002	\N	F	\N	2012-09-14	0	\N
00000029	02	00000002	\N	F	\N	2012-09-15	0	\N
00000029	02	00000002	\N	D	\N	2012-09-16	0	\N
00000029	02	00000002	\N	D	\N	2012-09-17	0	\N
00000029	02	00000002	\N	D	\N	2012-09-18	0	\N
00000029	02	00000002	\N	D	\N	2012-09-19	0	\N
00000029	02	00000002	\N	D	\N	2012-09-20	0	\N
00000029	02	00000002	\N	F	\N	2012-09-21	0	\N
00000029	02	00000002	\N	F	\N	2012-09-22	0	\N
00000029	02	00000002	\N	D	\N	2012-09-23	0	\N
00000029	02	00000002	\N	D	\N	2012-09-24	0	\N
00000029	02	00000002	\N	D	\N	2012-09-25	0	\N
00000029	02	00000002	\N	D	\N	2012-09-26	0	\N
00000029	02	00000002	\N	D	\N	2012-09-27	0	\N
00000029	02	00000002	\N	F	\N	2012-09-28	0	\N
00000029	02	00000002	\N	F	\N	2012-09-29	0	\N
00000029	02	00000002	\N	D	\N	2012-09-30	0	\N
00000030	02	00000002	\N	F	\N	2012-09-01	0	\N
00000030	02	00000002	\N	D	\N	2012-09-02	0	\N
00000030	02	00000002	\N	D	\N	2012-09-03	0	\N
00000030	02	00000002	\N	D	\N	2012-09-04	0	\N
00000030	02	00000002	\N	D	\N	2012-09-05	0	\N
00000030	02	00000002	\N	D	\N	2012-09-06	0	\N
00000030	02	00000002	\N	F	\N	2012-09-07	0	\N
00000030	02	00000002	\N	F	\N	2012-09-08	0	\N
00000030	02	00000002	\N	D	\N	2012-09-09	0	\N
00000030	02	00000002	\N	D	\N	2012-09-10	0	\N
00000030	02	00000002	\N	D	\N	2012-09-11	0	\N
00000030	02	00000002	\N	D	\N	2012-09-12	0	\N
00000030	02	00000002	\N	D	\N	2012-09-13	0	\N
00000030	02	00000002	\N	F	\N	2012-09-14	0	\N
00000030	02	00000002	\N	F	\N	2012-09-15	0	\N
00000030	02	00000002	\N	D	\N	2012-09-16	0	\N
00000030	02	00000002	\N	D	\N	2012-09-17	0	\N
00000030	02	00000002	\N	D	\N	2012-09-18	0	\N
00000030	02	00000002	\N	D	\N	2012-09-19	0	\N
00000030	02	00000002	\N	D	\N	2012-09-20	0	\N
00000030	02	00000002	\N	F	\N	2012-09-21	0	\N
00000030	02	00000002	\N	F	\N	2012-09-22	0	\N
00000030	02	00000002	\N	D	\N	2012-09-23	0	\N
00000030	02	00000002	\N	D	\N	2012-09-24	0	\N
00000030	02	00000002	\N	D	\N	2012-09-25	0	\N
00000030	02	00000002	\N	D	\N	2012-09-26	0	\N
00000030	02	00000002	\N	D	\N	2012-09-27	0	\N
00000030	02	00000002	\N	F	\N	2012-09-28	0	\N
00000030	02	00000002	\N	F	\N	2012-09-29	0	\N
00000030	02	00000002	\N	D	\N	2012-09-30	0	\N
00000031	02	00000002	\N	F	\N	2012-09-01	0	\N
00000031	02	00000002	\N	D	\N	2012-09-02	0	\N
00000031	02	00000002	\N	D	\N	2012-09-03	0	\N
00000031	02	00000002	\N	D	\N	2012-09-04	0	\N
00000031	02	00000002	\N	D	\N	2012-09-05	0	\N
00000031	02	00000002	\N	D	\N	2012-09-06	0	\N
00000031	02	00000002	\N	F	\N	2012-09-07	0	\N
00000031	02	00000002	\N	F	\N	2012-09-08	0	\N
00000031	02	00000002	\N	D	\N	2012-09-09	0	\N
00000031	02	00000002	\N	D	\N	2012-09-10	0	\N
00000031	02	00000002	\N	D	\N	2012-09-11	0	\N
00000031	02	00000002	\N	D	\N	2012-09-12	0	\N
00000031	02	00000002	\N	D	\N	2012-09-13	0	\N
00000031	02	00000002	\N	F	\N	2012-09-14	0	\N
00000031	02	00000002	\N	F	\N	2012-09-15	0	\N
00000031	02	00000002	\N	D	\N	2012-09-16	0	\N
00000031	02	00000002	\N	D	\N	2012-09-17	0	\N
00000031	02	00000002	\N	D	\N	2012-09-18	0	\N
00000031	02	00000002	\N	D	\N	2012-09-19	0	\N
00000031	02	00000002	\N	D	\N	2012-09-20	0	\N
00000031	02	00000002	\N	F	\N	2012-09-21	0	\N
00000031	02	00000002	\N	F	\N	2012-09-22	0	\N
00000031	02	00000002	\N	D	\N	2012-09-23	0	\N
00000031	02	00000002	\N	D	\N	2012-09-24	0	\N
00000031	02	00000002	\N	D	\N	2012-09-25	0	\N
00000031	02	00000002	\N	D	\N	2012-09-26	0	\N
00000031	02	00000002	\N	D	\N	2012-09-27	0	\N
00000031	02	00000002	\N	F	\N	2012-09-28	0	\N
00000031	02	00000002	\N	F	\N	2012-09-29	0	\N
00000031	02	00000002	\N	D	\N	2012-09-30	0	\N
00000032	02	00000002	\N	F	\N	2012-09-01	0	\N
00000032	02	00000002	\N	D	\N	2012-09-02	0	\N
00000032	02	00000002	\N	D	\N	2012-09-03	0	\N
00000032	02	00000002	\N	D	\N	2012-09-04	0	\N
00000032	02	00000002	\N	D	\N	2012-09-05	0	\N
00000032	02	00000002	\N	D	\N	2012-09-06	0	\N
00000032	02	00000002	\N	F	\N	2012-09-07	0	\N
00000032	02	00000002	\N	F	\N	2012-09-08	0	\N
00000032	02	00000002	\N	D	\N	2012-09-09	0	\N
00000032	02	00000002	\N	D	\N	2012-09-10	0	\N
00000032	02	00000002	\N	D	\N	2012-09-11	0	\N
00000032	02	00000002	\N	D	\N	2012-09-12	0	\N
00000032	02	00000002	\N	D	\N	2012-09-13	0	\N
00000032	02	00000002	\N	F	\N	2012-09-14	0	\N
00000032	02	00000002	\N	F	\N	2012-09-15	0	\N
00000032	02	00000002	\N	D	\N	2012-09-16	0	\N
00000032	02	00000002	\N	D	\N	2012-09-17	0	\N
00000003	01	00000002	\N	D	\N	2012-10-03	1	\N
00000003	01	00000002	\N	D	\N	2012-10-04	1	\N
00000032	02	00000002	\N	D	\N	2012-09-18	0	\N
00000032	02	00000002	\N	D	\N	2012-09-19	0	\N
00000032	02	00000002	\N	D	\N	2012-09-20	0	\N
00000032	02	00000002	\N	F	\N	2012-09-21	0	\N
00000032	02	00000002	\N	F	\N	2012-09-22	0	\N
00000032	02	00000002	\N	D	\N	2012-09-23	0	\N
00000032	02	00000002	\N	D	\N	2012-09-24	0	\N
00000032	02	00000002	\N	D	\N	2012-09-25	0	\N
00000032	02	00000002	\N	D	\N	2012-09-26	0	\N
00000032	02	00000002	\N	D	\N	2012-09-27	0	\N
00000032	02	00000002	\N	F	\N	2012-09-28	0	\N
00000032	02	00000002	\N	F	\N	2012-09-29	0	\N
00000032	02	00000002	\N	D	\N	2012-09-30	0	\N
00000033	02	00000002	\N	F	\N	2012-09-01	0	\N
00000033	02	00000002	\N	D	\N	2012-09-02	0	\N
00000033	02	00000002	\N	D	\N	2012-09-03	0	\N
00000033	02	00000002	\N	D	\N	2012-09-04	0	\N
00000033	02	00000002	\N	D	\N	2012-09-05	0	\N
00000033	02	00000002	\N	D	\N	2012-09-06	0	\N
00000033	02	00000002	\N	F	\N	2012-09-07	0	\N
00000033	02	00000002	\N	F	\N	2012-09-08	0	\N
00000033	02	00000002	\N	D	\N	2012-09-09	0	\N
00000033	02	00000002	\N	D	\N	2012-09-10	0	\N
00000033	02	00000002	\N	D	\N	2012-09-11	0	\N
00000033	02	00000002	\N	D	\N	2012-09-12	0	\N
00000033	02	00000002	\N	D	\N	2012-09-13	0	\N
00000033	02	00000002	\N	F	\N	2012-09-14	0	\N
00000033	02	00000002	\N	F	\N	2012-09-15	0	\N
00000033	02	00000002	\N	D	\N	2012-09-16	0	\N
00000033	02	00000002	\N	D	\N	2012-09-17	0	\N
00000033	02	00000002	\N	D	\N	2012-09-18	0	\N
00000033	02	00000002	\N	D	\N	2012-09-19	0	\N
00000033	02	00000002	\N	D	\N	2012-09-20	0	\N
00000033	02	00000002	\N	F	\N	2012-09-21	0	\N
00000033	02	00000002	\N	F	\N	2012-09-22	0	\N
00000033	02	00000002	\N	D	\N	2012-09-23	0	\N
00000033	02	00000002	\N	D	\N	2012-09-24	0	\N
00000033	02	00000002	\N	D	\N	2012-09-25	0	\N
00000033	02	00000002	\N	D	\N	2012-09-26	0	\N
00000033	02	00000002	\N	D	\N	2012-09-27	0	\N
00000033	02	00000002	\N	F	\N	2012-09-28	0	\N
00000033	02	00000002	\N	F	\N	2012-09-29	0	\N
00000033	02	00000002	\N	D	\N	2012-09-30	0	\N
00000034	02	00000002	\N	F	\N	2012-09-01	0	\N
00000034	02	00000002	\N	D	\N	2012-09-02	0	\N
00000034	02	00000002	\N	D	\N	2012-09-03	0	\N
00000034	02	00000002	\N	D	\N	2012-09-04	0	\N
00000034	02	00000002	\N	D	\N	2012-09-05	0	\N
00000034	02	00000002	\N	D	\N	2012-09-06	0	\N
00000034	02	00000002	\N	F	\N	2012-09-07	0	\N
00000034	02	00000002	\N	F	\N	2012-09-08	0	\N
00000034	02	00000002	\N	D	\N	2012-09-09	0	\N
00000034	02	00000002	\N	D	\N	2012-09-10	0	\N
00000034	02	00000002	\N	D	\N	2012-09-11	0	\N
00000034	02	00000002	\N	D	\N	2012-09-12	0	\N
00000034	02	00000002	\N	D	\N	2012-09-13	0	\N
00000034	02	00000002	\N	F	\N	2012-09-14	0	\N
00000034	02	00000002	\N	F	\N	2012-09-15	0	\N
00000034	02	00000002	\N	D	\N	2012-09-16	0	\N
00000034	02	00000002	\N	D	\N	2012-09-17	0	\N
00000034	02	00000002	\N	D	\N	2012-09-18	0	\N
00000034	02	00000002	\N	D	\N	2012-09-19	0	\N
00000034	02	00000002	\N	D	\N	2012-09-20	0	\N
00000034	02	00000002	\N	F	\N	2012-09-21	0	\N
00000034	02	00000002	\N	F	\N	2012-09-22	0	\N
00000034	02	00000002	\N	D	\N	2012-09-23	0	\N
00000034	02	00000002	\N	D	\N	2012-09-24	0	\N
00000034	02	00000002	\N	D	\N	2012-09-25	0	\N
00000034	02	00000002	\N	D	\N	2012-09-26	0	\N
00000034	02	00000002	\N	D	\N	2012-09-27	0	\N
00000034	02	00000002	\N	F	\N	2012-09-28	0	\N
00000034	02	00000002	\N	F	\N	2012-09-29	0	\N
00000034	02	00000002	\N	D	\N	2012-09-30	0	\N
00000035	02	00000002	\N	F	\N	2012-09-01	0	\N
00000035	02	00000002	\N	D	\N	2012-09-02	0	\N
00000035	02	00000002	\N	D	\N	2012-09-03	0	\N
00000035	02	00000002	\N	D	\N	2012-09-04	0	\N
00000035	02	00000002	\N	D	\N	2012-09-05	0	\N
00000035	02	00000002	\N	D	\N	2012-09-06	0	\N
00000035	02	00000002	\N	F	\N	2012-09-07	0	\N
00000035	02	00000002	\N	F	\N	2012-09-08	0	\N
00000035	02	00000002	\N	D	\N	2012-09-09	0	\N
00000035	02	00000002	\N	D	\N	2012-09-10	0	\N
00000035	02	00000002	\N	D	\N	2012-09-11	0	\N
00000035	02	00000002	\N	D	\N	2012-09-12	0	\N
00000035	02	00000002	\N	D	\N	2012-09-13	0	\N
00000035	02	00000002	\N	F	\N	2012-09-14	0	\N
00000035	02	00000002	\N	F	\N	2012-09-15	0	\N
00000035	02	00000002	\N	D	\N	2012-09-16	0	\N
00000035	02	00000002	\N	D	\N	2012-09-17	0	\N
00000005	01	00000002	\N	D	\N	2012-10-06	1	\N
00000005	01	00000002	\N	D	\N	2012-10-07	1	\N
00000005	01	00000002	\N	D	\N	2012-10-13	1	\N
00000005	01	00000002	\N	D	\N	2012-10-14	1	\N
00000005	01	00000002	\N	D	\N	2012-10-20	1	\N
00000005	01	00000002	\N	D	\N	2012-10-21	1	\N
00000005	01	00000002	\N	D	\N	2012-10-27	1	\N
00000005	01	00000002	\N	D	\N	2012-10-28	1	\N
00000006	01	00000002	\N	D	\N	2012-10-06	1	\N
00000006	01	00000002	\N	D	\N	2012-10-07	1	\N
00000006	01	00000002	\N	D	\N	2012-10-13	1	\N
00000006	01	00000002	\N	D	\N	2012-10-14	1	\N
00000006	01	00000002	\N	D	\N	2012-10-20	1	\N
00000006	01	00000002	\N	D	\N	2012-10-21	1	\N
00000006	01	00000002	\N	D	\N	2012-10-27	1	\N
00000006	01	00000002	\N	D	\N	2012-10-28	1	\N
00000007	01	00000002	\N	D	\N	2012-10-06	1	\N
00000007	01	00000002	\N	D	\N	2012-10-07	1	\N
00000007	01	00000002	\N	D	\N	2012-10-13	1	\N
00000007	01	00000002	\N	D	\N	2012-10-14	1	\N
00000007	01	00000002	\N	D	\N	2012-10-20	1	\N
00000007	01	00000002	\N	D	\N	2012-10-21	1	\N
00000001	01	00000002	\N	D	\N	2012-10-06	0	\N
00000001	01	00000002	\N	D	\N	2012-10-07	0	\N
00000001	01	00000002	\N	D	\N	2012-10-13	1	\N
00000001	01	00000002	\N	D	\N	2012-10-14	1	\N
00000002	01	00000002	\N	D	\N	2012-10-20	1	\N
00000002	01	00000002	\N	D	\N	2012-10-21	1	\N
00000002	01	00000002	\N	D	\N	2012-10-27	1	\N
00000002	01	00000002	\N	D	\N	2012-10-28	1	\N
00000013	05	00000002	\N	D	\N	2012-10-01	1	\N
00000013	05	00000002	\N	D	\N	2012-10-02	1	\N
00000013	05	00000002	\N	D	\N	2012-10-03	1	\N
00000013	05	00000002	\N	D	\N	2012-10-04	1	\N
00000013	05	00000002	\N	F	\N	2012-10-05	1	\N
00000013	05	00000002	\N	F	\N	2012-10-06	1	\N
00000013	05	00000002	\N	D	\N	2012-10-07	1	\N
00000013	05	00000002	\N	D	\N	2012-10-08	1	\N
00000013	05	00000002	\N	D	\N	2012-10-09	1	\N
00000013	05	00000002	\N	D	\N	2012-10-10	1	\N
00000013	05	00000002	\N	D	\N	2012-10-11	1	\N
00000013	05	00000002	\N	F	\N	2012-10-12	1	\N
00000013	05	00000002	\N	F	\N	2012-10-13	1	\N
00000013	05	00000002	\N	D	\N	2012-10-14	1	\N
00000013	05	00000002	\N	D	\N	2012-10-15	1	\N
00000013	05	00000002	\N	D	\N	2012-10-16	1	\N
00000013	05	00000002	\N	D	\N	2012-10-17	1	\N
00000013	05	00000002	\N	D	\N	2012-10-18	1	\N
00000013	05	00000002	\N	F	\N	2012-10-19	1	\N
00000013	05	00000002	\N	F	\N	2012-10-20	1	\N
00000013	05	00000002	\N	D	\N	2012-10-21	1	\N
00000013	05	00000002	\N	D	\N	2012-10-22	1	\N
00000013	05	00000002	\N	D	\N	2012-10-23	1	\N
00000013	05	00000002	\N	D	\N	2012-10-24	1	\N
00000013	05	00000002	\N	D	\N	2012-10-25	1	\N
00000013	05	00000002	\N	F	\N	2012-10-26	1	\N
00000013	05	00000002	\N	F	\N	2012-10-27	1	\N
00000013	05	00000002	\N	D	\N	2012-10-28	1	\N
00000013	05	00000002	\N	D	\N	2012-10-29	1	\N
00000013	05	00000002	\N	D	\N	2012-10-30	1	\N
00000013	05	00000002	\N	D	\N	2012-10-31	1	\N
00000014	05	00000002	\N	D	\N	2012-10-01	1	\N
00000014	05	00000002	\N	D	\N	2012-10-02	1	\N
00000014	05	00000002	\N	D	\N	2012-10-03	1	\N
00000014	05	00000002	\N	D	\N	2012-10-04	1	\N
00000014	05	00000002	\N	F	\N	2012-10-05	1	\N
00000014	05	00000002	\N	F	\N	2012-10-06	1	\N
00000014	05	00000002	\N	D	\N	2012-10-07	1	\N
00000014	05	00000002	\N	D	\N	2012-10-08	1	\N
00000014	05	00000002	\N	D	\N	2012-10-09	1	\N
00000014	05	00000002	\N	D	\N	2012-10-10	1	\N
00000014	05	00000002	\N	D	\N	2012-10-11	1	\N
00000014	05	00000002	\N	F	\N	2012-10-12	1	\N
00000014	05	00000002	\N	F	\N	2012-10-13	1	\N
00000014	05	00000002	\N	D	\N	2012-10-14	1	\N
00000014	05	00000002	\N	D	\N	2012-10-15	1	\N
00000014	05	00000002	\N	D	\N	2012-10-16	1	\N
00000014	05	00000002	\N	D	\N	2012-10-17	1	\N
00000014	05	00000002	\N	D	\N	2012-10-18	1	\N
00000014	05	00000002	\N	F	\N	2012-10-19	1	\N
00000014	05	00000002	\N	F	\N	2012-10-20	1	\N
00000014	05	00000002	\N	D	\N	2012-10-21	1	\N
00000014	05	00000002	\N	D	\N	2012-10-22	1	\N
00000014	05	00000002	\N	D	\N	2012-10-23	1	\N
00000014	05	00000002	\N	D	\N	2012-10-24	1	\N
00000014	05	00000002	\N	D	\N	2012-10-25	1	\N
00000014	05	00000002	\N	F	\N	2012-10-26	1	\N
00000014	05	00000002	\N	F	\N	2012-10-27	1	\N
00000014	05	00000002	\N	D	\N	2012-10-28	1	\N
00000014	05	00000002	\N	D	\N	2012-10-29	1	\N
00000014	05	00000002	\N	D	\N	2012-10-30	1	\N
00000017	02	00000002	\N	D	\N	2013-01-01	1	\N
00000017	02	00000002	\N	D	\N	2013-01-08	1	\N
00000017	02	00000002	\N	D	\N	2013-01-09	1	\N
00000017	02	00000002	\N	D	\N	2013-01-15	1	\N
00000017	02	00000002	\N	D	\N	2013-01-16	1	\N
00000017	02	00000002	\N	D	\N	2013-01-17	1	\N
00000017	02	00000002	\N	F	\N	2013-01-18	1	\N
00000017	02	00000002	\N	F	\N	2013-01-19	1	\N
00000017	02	00000002	\N	D	\N	2013-01-20	1	\N
00000017	02	00000002	\N	D	\N	2013-01-21	1	\N
00000017	02	00000002	\N	D	\N	2013-01-22	1	\N
00000017	02	00000002	\N	D	\N	2013-01-24	1	\N
00000017	02	00000002	\N	F	\N	2013-01-25	1	\N
00000017	02	00000002	\N	F	\N	2013-01-26	1	\N
00000017	02	00000002	\N	D	\N	2013-01-27	1	\N
00000017	02	00000002	\N	D	\N	2013-01-28	1	\N
00000017	02	00000002	\N	D	\N	2013-01-29	1	\N
00000017	02	00000002	\N	D	\N	2013-01-30	1	\N
00000017	02	00000002	\N	D	\N	2013-01-31	1	\N
00000018	02	00000002	\N	D	\N	2013-01-01	1	\N
00000018	02	00000002	\N	D	\N	2013-01-02	1	\N
00000018	02	00000002	\N	D	\N	2013-01-03	1	\N
00000018	02	00000002	\N	F	\N	2013-01-04	1	\N
00000018	02	00000002	\N	F	\N	2013-01-05	1	\N
00000018	02	00000002	\N	D	\N	2013-01-06	1	\N
00000018	02	00000002	\N	D	\N	2013-01-07	1	\N
00000018	02	00000002	\N	D	\N	2013-01-08	1	\N
00000018	02	00000002	\N	D	\N	2013-01-09	1	\N
00000018	02	00000002	\N	D	\N	2013-01-15	1	\N
00000018	02	00000002	\N	D	\N	2013-01-16	1	\N
00000018	02	00000002	\N	D	\N	2013-01-17	1	\N
00000018	02	00000002	\N	F	\N	2013-01-18	1	\N
00000018	02	00000002	\N	F	\N	2013-01-19	1	\N
00000018	02	00000002	\N	D	\N	2013-01-20	1	\N
00000018	02	00000002	\N	D	\N	2013-01-21	1	\N
00000018	02	00000002	\N	D	\N	2013-01-22	1	\N
00000018	02	00000002	\N	D	\N	2013-01-23	1	\N
00000018	02	00000002	\N	D	\N	2013-01-24	1	\N
00000018	02	00000002	\N	F	\N	2013-01-25	1	\N
00000018	02	00000002	\N	F	\N	2013-01-26	1	\N
00000018	02	00000002	\N	D	\N	2013-01-27	1	\N
00000018	02	00000002	\N	D	\N	2013-01-28	1	\N
00000018	02	00000002	\N	D	\N	2013-01-29	1	\N
00000018	02	00000002	\N	D	\N	2013-01-30	1	\N
00000018	02	00000002	\N	D	\N	2013-01-31	1	\N
00000019	02	00000002	\N	D	\N	2013-01-01	1	\N
00000019	02	00000002	\N	D	\N	2013-01-02	1	\N
00000019	02	00000002	\N	D	\N	2013-01-03	1	\N
00000019	02	00000002	\N	F	\N	2013-01-04	1	\N
00000017	02	00000002	\N	D	0000000043	2013-01-10	R	\N
00000017	02	00000002	\N	D	0000000044	2013-01-02	R	\N
00000017	02	00000002	\N	D	0000000044	2013-01-03	R	\N
00000017	02	00000002	\N	F	0000000044	2013-01-04	R	\N
00000017	02	00000002	\N	F	0000000044	2013-01-05	R	\N
00000018	02	00000002	\N	D	0000000047	2013-01-10	R	\N
00000018	02	00000002	\N	F	0000000047	2013-01-11	R	\N
00000018	02	00000002	\N	F	0000000047	2013-01-12	R	\N
00000018	02	00000002	\N	D	0000000047	2013-01-13	R	\N
00000018	02	00000002	\N	D	0000000047	2013-01-14	R	\N
00000019	02	00000002	\N	F	\N	2013-01-05	1	\N
00000019	02	00000002	\N	D	\N	2013-01-06	1	\N
00000019	02	00000002	\N	D	\N	2013-01-07	1	\N
00000019	02	00000002	\N	D	\N	2013-01-08	1	\N
00000019	02	00000002	\N	D	\N	2013-01-09	1	\N
00000019	02	00000002	\N	D	\N	2013-01-10	1	\N
00000019	02	00000002	\N	D	\N	2013-01-15	1	\N
00000019	02	00000002	\N	D	\N	2013-01-16	1	\N
00000019	02	00000002	\N	D	\N	2013-01-17	1	\N
00000019	02	00000002	\N	F	\N	2013-01-18	1	\N
00000019	02	00000002	\N	F	\N	2013-01-19	1	\N
00000019	02	00000002	\N	D	\N	2013-01-20	1	\N
00000019	02	00000002	\N	D	\N	2013-01-21	1	\N
00000019	02	00000002	\N	D	\N	2013-01-22	1	\N
00000019	02	00000002	\N	D	\N	2013-01-23	1	\N
00000019	02	00000002	\N	D	\N	2013-01-24	1	\N
00000019	02	00000002	\N	F	\N	2013-01-25	1	\N
00000019	02	00000002	\N	F	\N	2013-01-26	1	\N
00000019	02	00000002	\N	D	\N	2013-01-27	1	\N
00000019	02	00000002	\N	D	\N	2013-01-28	1	\N
00000019	02	00000002	\N	D	\N	2013-01-29	1	\N
00000019	02	00000002	\N	D	\N	2013-01-30	1	\N
00000019	02	00000002	\N	D	\N	2013-01-31	1	\N
00000020	02	00000002	\N	D	\N	2013-01-01	1	\N
00000020	02	00000002	\N	D	\N	2013-01-02	1	\N
00000020	02	00000002	\N	D	\N	2013-01-03	1	\N
00000020	02	00000002	\N	F	\N	2013-01-04	1	\N
00000020	02	00000002	\N	F	\N	2013-01-05	1	\N
00000020	02	00000002	\N	D	\N	2013-01-06	1	\N
00000020	02	00000002	\N	D	\N	2013-01-07	1	\N
00000020	02	00000002	\N	D	\N	2013-01-08	1	\N
00000020	02	00000002	\N	D	\N	2013-01-09	1	\N
00000020	02	00000002	\N	D	\N	2013-01-10	1	\N
00000020	02	00000002	\N	F	\N	2013-01-11	1	\N
00000020	02	00000002	\N	F	\N	2013-01-12	1	\N
00000020	02	00000002	\N	D	\N	2013-01-13	1	\N
00000020	02	00000002	\N	D	\N	2013-01-14	1	\N
00000020	02	00000002	\N	D	\N	2013-01-15	1	\N
00000020	02	00000002	\N	D	\N	2013-01-16	1	\N
00000020	02	00000002	\N	D	\N	2013-01-17	1	\N
00000020	02	00000002	\N	F	\N	2013-01-18	1	\N
00000020	02	00000002	\N	F	\N	2013-01-19	1	\N
00000020	02	00000002	\N	D	\N	2013-01-20	1	\N
00000020	02	00000002	\N	D	\N	2013-01-21	1	\N
00000020	02	00000002	\N	D	\N	2013-01-22	1	\N
00000020	02	00000002	\N	D	\N	2013-01-23	1	\N
00000020	02	00000002	\N	D	\N	2013-01-24	1	\N
00000020	02	00000002	\N	F	\N	2013-01-25	1	\N
00000020	02	00000002	\N	F	\N	2013-01-26	1	\N
00000020	02	00000002	\N	D	\N	2013-01-27	1	\N
00000020	02	00000002	\N	D	\N	2013-01-28	1	\N
00000020	02	00000002	\N	D	\N	2013-01-29	1	\N
00000020	02	00000002	\N	D	\N	2013-01-30	1	\N
00000020	02	00000002	\N	D	\N	2013-01-31	1	\N
00000021	02	00000002	\N	D	\N	2013-01-01	1	\N
00000021	02	00000002	\N	D	\N	2013-01-02	1	\N
00000021	02	00000002	\N	D	\N	2013-01-03	1	\N
00000021	02	00000002	\N	F	\N	2013-01-04	1	\N
00000021	02	00000002	\N	F	\N	2013-01-05	1	\N
00000021	02	00000002	\N	D	\N	2013-01-06	1	\N
00000021	02	00000002	\N	D	\N	2013-01-07	1	\N
00000021	02	00000002	\N	D	\N	2013-01-08	1	\N
00000021	02	00000002	\N	D	\N	2013-01-09	1	\N
00000021	02	00000002	\N	D	\N	2013-01-10	1	\N
00000021	02	00000002	\N	F	\N	2013-01-11	1	\N
00000021	02	00000002	\N	F	\N	2013-01-12	1	\N
00000021	02	00000002	\N	D	\N	2013-01-13	1	\N
00000021	02	00000002	\N	D	\N	2013-01-14	1	\N
00000021	02	00000002	\N	D	\N	2013-01-15	1	\N
00000021	02	00000002	\N	D	\N	2013-01-16	1	\N
00000021	02	00000002	\N	D	\N	2013-01-17	1	\N
00000021	02	00000002	\N	F	\N	2013-01-18	1	\N
00000021	02	00000002	\N	F	\N	2013-01-19	1	\N
00000021	02	00000002	\N	D	\N	2013-01-20	1	\N
00000021	02	00000002	\N	D	\N	2013-01-21	1	\N
00000021	02	00000002	\N	D	\N	2013-01-22	1	\N
00000021	02	00000002	\N	D	\N	2013-01-23	1	\N
00000021	02	00000002	\N	D	\N	2013-01-24	1	\N
00000021	02	00000002	\N	F	\N	2013-01-25	1	\N
00000021	02	00000002	\N	F	\N	2013-01-26	1	\N
00000021	02	00000002	\N	D	\N	2013-01-27	1	\N
00000021	02	00000002	\N	D	\N	2013-01-28	1	\N
00000021	02	00000002	\N	D	\N	2013-01-29	1	\N
00000021	02	00000002	\N	D	\N	2013-01-30	1	\N
00000021	02	00000002	\N	D	\N	2013-01-31	1	\N
00000022	02	00000002	\N	D	\N	2013-01-01	1	\N
00000022	02	00000002	\N	D	\N	2013-01-02	1	\N
00000022	02	00000002	\N	D	\N	2013-01-03	1	\N
00000022	02	00000002	\N	F	\N	2013-01-04	1	\N
00000022	02	00000002	\N	F	\N	2013-01-05	1	\N
00000022	02	00000002	\N	D	\N	2013-01-06	1	\N
00000022	02	00000002	\N	D	\N	2013-01-07	1	\N
00000022	02	00000002	\N	D	\N	2013-01-08	1	\N
00000022	02	00000002	\N	D	\N	2013-01-09	1	\N
00000022	02	00000002	\N	D	\N	2013-01-10	1	\N
00000022	02	00000002	\N	F	\N	2013-01-11	1	\N
00000022	02	00000002	\N	F	\N	2013-01-12	1	\N
00000022	02	00000002	\N	D	\N	2013-01-13	1	\N
00000022	02	00000002	\N	D	\N	2013-01-14	1	\N
00000022	02	00000002	\N	D	\N	2013-01-15	1	\N
00000022	02	00000002	\N	D	\N	2013-01-16	1	\N
00000022	02	00000002	\N	D	\N	2013-01-17	1	\N
00000022	02	00000002	\N	F	\N	2013-01-18	1	\N
00000022	02	00000002	\N	F	\N	2013-01-19	1	\N
00000022	02	00000002	\N	D	\N	2013-01-20	1	\N
00000022	02	00000002	\N	D	\N	2013-01-21	1	\N
00000022	02	00000002	\N	D	\N	2013-01-22	1	\N
00000022	02	00000002	\N	D	\N	2013-01-23	1	\N
00000022	02	00000002	\N	D	\N	2013-01-24	1	\N
00000022	02	00000002	\N	F	\N	2013-01-25	1	\N
00000022	02	00000002	\N	F	\N	2013-01-26	1	\N
00000022	02	00000002	\N	D	\N	2013-01-27	1	\N
00000022	02	00000002	\N	D	\N	2013-01-28	1	\N
00000022	02	00000002	\N	D	\N	2013-01-29	1	\N
00000022	02	00000002	\N	D	\N	2013-01-30	1	\N
00000022	02	00000002	\N	D	\N	2013-01-31	1	\N
00000023	02	00000002	\N	D	\N	2013-01-01	1	\N
00000023	02	00000002	\N	D	\N	2013-01-02	1	\N
00000023	02	00000002	\N	D	\N	2013-01-03	1	\N
00000023	02	00000002	\N	F	\N	2013-01-04	1	\N
00000023	02	00000002	\N	F	\N	2013-01-05	1	\N
00000023	02	00000002	\N	D	\N	2013-01-06	1	\N
00000023	02	00000002	\N	D	\N	2013-01-07	1	\N
00000023	02	00000002	\N	D	\N	2013-01-08	1	\N
00000023	02	00000002	\N	D	\N	2013-01-09	1	\N
00000023	02	00000002	\N	D	\N	2013-01-10	1	\N
00000023	02	00000002	\N	F	\N	2013-01-11	1	\N
00000023	02	00000002	\N	F	\N	2013-01-12	1	\N
00000023	02	00000002	\N	D	\N	2013-01-13	1	\N
00000023	02	00000002	\N	D	\N	2013-01-14	1	\N
00000023	02	00000002	\N	D	\N	2013-01-15	1	\N
00000023	02	00000002	\N	D	\N	2013-01-16	1	\N
00000023	02	00000002	\N	D	\N	2013-01-17	1	\N
00000023	02	00000002	\N	F	\N	2013-01-18	1	\N
00000023	02	00000002	\N	F	\N	2013-01-19	1	\N
00000023	02	00000002	\N	D	\N	2013-01-20	1	\N
00000023	02	00000002	\N	D	\N	2013-01-21	1	\N
00000023	02	00000002	\N	D	\N	2013-01-22	1	\N
00000023	02	00000002	\N	D	\N	2013-01-23	1	\N
00000023	02	00000002	\N	D	\N	2013-01-24	1	\N
00000023	02	00000002	\N	F	\N	2013-01-25	1	\N
00000023	02	00000002	\N	F	\N	2013-01-26	1	\N
00000023	02	00000002	\N	D	\N	2013-01-27	1	\N
00000023	02	00000002	\N	D	\N	2013-01-28	1	\N
00000023	02	00000002	\N	D	\N	2013-01-29	1	\N
00000023	02	00000002	\N	D	\N	2013-01-30	1	\N
00000023	02	00000002	\N	D	\N	2013-01-31	1	\N
00000024	02	00000002	\N	D	\N	2013-01-01	1	\N
00000024	02	00000002	\N	D	\N	2013-01-02	1	\N
00000024	02	00000002	\N	D	\N	2013-01-03	1	\N
00000024	02	00000002	\N	F	\N	2013-01-04	1	\N
00000024	02	00000002	\N	F	\N	2013-01-05	1	\N
00000024	02	00000002	\N	D	\N	2013-01-06	1	\N
00000024	02	00000002	\N	D	\N	2013-01-07	1	\N
00000024	02	00000002	\N	D	\N	2013-01-08	1	\N
00000024	02	00000002	\N	D	\N	2013-01-09	1	\N
00000024	02	00000002	\N	D	\N	2013-01-10	1	\N
00000024	02	00000002	\N	F	\N	2013-01-11	1	\N
00000024	02	00000002	\N	F	\N	2013-01-12	1	\N
00000024	02	00000002	\N	D	\N	2013-01-13	1	\N
00000024	02	00000002	\N	D	\N	2013-01-14	1	\N
00000024	02	00000002	\N	D	\N	2013-01-15	1	\N
00000024	02	00000002	\N	D	\N	2013-01-16	1	\N
00000024	02	00000002	\N	D	\N	2013-01-17	1	\N
00000024	02	00000002	\N	F	\N	2013-01-18	1	\N
00000024	02	00000002	\N	F	\N	2013-01-19	1	\N
00000024	02	00000002	\N	D	\N	2013-01-20	1	\N
00000024	02	00000002	\N	D	\N	2013-01-21	1	\N
00000024	02	00000002	\N	D	\N	2013-01-22	1	\N
00000024	02	00000002	\N	D	\N	2013-01-23	1	\N
00000024	02	00000002	\N	D	\N	2013-01-24	1	\N
00000024	02	00000002	\N	F	\N	2013-01-25	1	\N
00000024	02	00000002	\N	F	\N	2013-01-26	1	\N
00000024	02	00000002	\N	D	\N	2013-01-27	1	\N
00000024	02	00000002	\N	D	\N	2013-01-28	1	\N
00000024	02	00000002	\N	D	\N	2013-01-29	1	\N
00000024	02	00000002	\N	D	\N	2013-01-30	1	\N
00000024	02	00000002	\N	D	\N	2013-01-31	1	\N
00000025	02	00000002	\N	D	\N	2013-01-01	1	\N
00000025	02	00000002	\N	D	\N	2013-01-02	1	\N
00000025	02	00000002	\N	D	\N	2013-01-03	1	\N
00000025	02	00000002	\N	F	\N	2013-01-04	1	\N
00000025	02	00000002	\N	F	\N	2013-01-05	1	\N
00000025	02	00000002	\N	D	\N	2013-01-06	1	\N
00000025	02	00000002	\N	D	\N	2013-01-07	1	\N
00000025	02	00000002	\N	D	\N	2013-01-08	1	\N
00000025	02	00000002	\N	D	\N	2013-01-09	1	\N
00000025	02	00000002	\N	D	\N	2013-01-10	1	\N
00000025	02	00000002	\N	F	\N	2013-01-11	1	\N
00000025	02	00000002	\N	F	\N	2013-01-12	1	\N
00000025	02	00000002	\N	D	\N	2013-01-13	1	\N
00000025	02	00000002	\N	D	\N	2013-01-14	1	\N
00000025	02	00000002	\N	D	\N	2013-01-15	1	\N
00000025	02	00000002	\N	D	\N	2013-01-16	1	\N
00000025	02	00000002	\N	D	\N	2013-01-17	1	\N
00000025	02	00000002	\N	F	\N	2013-01-18	1	\N
00000025	02	00000002	\N	F	\N	2013-01-19	1	\N
00000025	02	00000002	\N	D	\N	2013-01-20	1	\N
00000025	02	00000002	\N	D	\N	2013-01-21	1	\N
00000025	02	00000002	\N	D	\N	2013-01-22	1	\N
00000025	02	00000002	\N	D	\N	2013-01-23	1	\N
00000025	02	00000002	\N	D	\N	2013-01-24	1	\N
00000025	02	00000002	\N	F	\N	2013-01-25	1	\N
00000025	02	00000002	\N	F	\N	2013-01-26	1	\N
00000025	02	00000002	\N	D	\N	2013-01-27	1	\N
00000025	02	00000002	\N	D	\N	2013-01-28	1	\N
00000025	02	00000002	\N	D	\N	2013-01-29	1	\N
00000025	02	00000002	\N	D	\N	2013-01-30	1	\N
00000025	02	00000002	\N	D	\N	2013-01-31	1	\N
00000026	02	00000002	\N	D	\N	2013-01-01	1	\N
00000026	02	00000002	\N	D	\N	2013-01-02	1	\N
00000026	02	00000002	\N	D	\N	2013-01-03	1	\N
00000026	02	00000002	\N	F	\N	2013-01-04	1	\N
00000026	02	00000002	\N	F	\N	2013-01-05	1	\N
00000026	02	00000002	\N	D	\N	2013-01-06	1	\N
00000026	02	00000002	\N	D	\N	2013-01-07	1	\N
00000026	02	00000002	\N	D	\N	2013-01-08	1	\N
00000026	02	00000002	\N	D	\N	2013-01-09	1	\N
00000026	02	00000002	\N	D	\N	2013-01-10	1	\N
00000026	02	00000002	\N	F	\N	2013-01-11	1	\N
00000026	02	00000002	\N	F	\N	2013-01-12	1	\N
00000026	02	00000002	\N	D	\N	2013-01-13	1	\N
00000026	02	00000002	\N	D	\N	2013-01-14	1	\N
00000026	02	00000002	\N	D	\N	2013-01-15	1	\N
00000026	02	00000002	\N	D	\N	2013-01-16	1	\N
00000026	02	00000002	\N	D	\N	2013-01-17	1	\N
00000026	02	00000002	\N	F	\N	2013-01-18	1	\N
00000026	02	00000002	\N	F	\N	2013-01-19	1	\N
00000026	02	00000002	\N	D	\N	2013-01-20	1	\N
00000026	02	00000002	\N	D	\N	2013-01-21	1	\N
00000026	02	00000002	\N	D	\N	2013-01-22	1	\N
00000026	02	00000002	\N	D	\N	2013-01-23	1	\N
00000026	02	00000002	\N	D	\N	2013-01-24	1	\N
00000026	02	00000002	\N	F	\N	2013-01-25	1	\N
00000026	02	00000002	\N	F	\N	2013-01-26	1	\N
00000026	02	00000002	\N	D	\N	2013-01-27	1	\N
00000026	02	00000002	\N	D	\N	2013-01-28	1	\N
00000026	02	00000002	\N	D	\N	2013-01-29	1	\N
00000026	02	00000002	\N	D	\N	2013-01-30	1	\N
00000026	02	00000002	\N	D	\N	2013-01-31	1	\N
00000027	02	00000002	\N	D	\N	2013-01-01	1	\N
00000027	02	00000002	\N	D	\N	2013-01-02	1	\N
00000027	02	00000002	\N	D	\N	2013-01-03	1	\N
00000027	02	00000002	\N	F	\N	2013-01-04	1	\N
00000027	02	00000002	\N	F	\N	2013-01-05	1	\N
00000027	02	00000002	\N	D	\N	2013-01-06	1	\N
00000027	02	00000002	\N	D	\N	2013-01-07	1	\N
00000027	02	00000002	\N	D	\N	2013-01-08	1	\N
00000027	02	00000002	\N	D	\N	2013-01-09	1	\N
00000027	02	00000002	\N	D	\N	2013-01-10	1	\N
00000027	02	00000002	\N	F	\N	2013-01-11	1	\N
00000027	02	00000002	\N	F	\N	2013-01-12	1	\N
00000027	02	00000002	\N	D	\N	2013-01-13	1	\N
00000027	02	00000002	\N	D	\N	2013-01-14	1	\N
00000027	02	00000002	\N	D	\N	2013-01-15	1	\N
00000027	02	00000002	\N	D	\N	2013-01-16	1	\N
00000027	02	00000002	\N	D	\N	2013-01-17	1	\N
00000027	02	00000002	\N	F	\N	2013-01-18	1	\N
00000027	02	00000002	\N	F	\N	2013-01-19	1	\N
00000027	02	00000002	\N	D	\N	2013-01-20	1	\N
00000027	02	00000002	\N	D	\N	2013-01-21	1	\N
00000027	02	00000002	\N	D	\N	2013-01-22	1	\N
00000027	02	00000002	\N	D	\N	2013-01-23	1	\N
00000027	02	00000002	\N	D	\N	2013-01-24	1	\N
00000027	02	00000002	\N	F	\N	2013-01-25	1	\N
00000027	02	00000002	\N	F	\N	2013-01-26	1	\N
00000027	02	00000002	\N	D	\N	2013-01-27	1	\N
00000027	02	00000002	\N	D	\N	2013-01-28	1	\N
00000027	02	00000002	\N	D	\N	2013-01-29	1	\N
00000027	02	00000002	\N	D	\N	2013-01-30	1	\N
00000027	02	00000002	\N	D	\N	2013-01-31	1	\N
00000028	02	00000002	\N	D	\N	2013-01-01	1	\N
00000028	02	00000002	\N	D	\N	2013-01-02	1	\N
00000028	02	00000002	\N	D	\N	2013-01-03	1	\N
00000028	02	00000002	\N	F	\N	2013-01-04	1	\N
00000028	02	00000002	\N	F	\N	2013-01-05	1	\N
00000028	02	00000002	\N	D	\N	2013-01-06	1	\N
00000028	02	00000002	\N	D	\N	2013-01-07	1	\N
00000028	02	00000002	\N	D	\N	2013-01-08	1	\N
00000028	02	00000002	\N	D	\N	2013-01-09	1	\N
00000028	02	00000002	\N	D	\N	2013-01-10	1	\N
00000028	02	00000002	\N	F	\N	2013-01-11	1	\N
00000028	02	00000002	\N	F	\N	2013-01-12	1	\N
00000028	02	00000002	\N	D	\N	2013-01-13	1	\N
00000028	02	00000002	\N	D	\N	2013-01-14	1	\N
00000028	02	00000002	\N	D	\N	2013-01-15	1	\N
00000028	02	00000002	\N	D	\N	2013-01-16	1	\N
00000028	02	00000002	\N	D	\N	2013-01-17	1	\N
00000028	02	00000002	\N	F	\N	2013-01-18	1	\N
00000028	02	00000002	\N	F	\N	2013-01-19	1	\N
00000028	02	00000002	\N	D	\N	2013-01-20	1	\N
00000028	02	00000002	\N	D	\N	2013-01-21	1	\N
00000028	02	00000002	\N	D	\N	2013-01-22	1	\N
00000028	02	00000002	\N	D	\N	2013-01-23	1	\N
00000028	02	00000002	\N	D	\N	2013-01-24	1	\N
00000028	02	00000002	\N	F	\N	2013-01-25	1	\N
00000028	02	00000002	\N	F	\N	2013-01-26	1	\N
00000028	02	00000002	\N	D	\N	2013-01-27	1	\N
00000028	02	00000002	\N	D	\N	2013-01-28	1	\N
00000028	02	00000002	\N	D	\N	2013-01-29	1	\N
00000028	02	00000002	\N	D	\N	2013-01-30	1	\N
00000028	02	00000002	\N	D	\N	2013-01-31	1	\N
00000029	02	00000002	\N	D	\N	2013-01-01	1	\N
00000029	02	00000002	\N	D	\N	2013-01-02	1	\N
00000029	02	00000002	\N	D	\N	2013-01-03	1	\N
00000029	02	00000002	\N	F	\N	2013-01-04	1	\N
00000029	02	00000002	\N	F	\N	2013-01-05	1	\N
00000029	02	00000002	\N	D	\N	2013-01-06	1	\N
00000029	02	00000002	\N	D	\N	2013-01-07	1	\N
00000029	02	00000002	\N	D	\N	2013-01-08	1	\N
00000029	02	00000002	\N	D	\N	2013-01-09	1	\N
00000029	02	00000002	\N	D	\N	2013-01-10	1	\N
00000029	02	00000002	\N	F	\N	2013-01-11	1	\N
00000029	02	00000002	\N	F	\N	2013-01-12	1	\N
00000029	02	00000002	\N	D	\N	2013-01-13	1	\N
00000029	02	00000002	\N	D	\N	2013-01-14	1	\N
00000029	02	00000002	\N	D	\N	2013-01-15	1	\N
00000029	02	00000002	\N	D	\N	2013-01-16	1	\N
00000029	02	00000002	\N	D	\N	2013-01-17	1	\N
00000029	02	00000002	\N	F	\N	2013-01-18	1	\N
00000029	02	00000002	\N	F	\N	2013-01-19	1	\N
00000029	02	00000002	\N	D	\N	2013-01-20	1	\N
00000029	02	00000002	\N	D	\N	2013-01-21	1	\N
00000029	02	00000002	\N	D	\N	2013-01-22	1	\N
00000029	02	00000002	\N	D	\N	2013-01-23	1	\N
00000029	02	00000002	\N	D	\N	2013-01-24	1	\N
00000029	02	00000002	\N	F	\N	2013-01-25	1	\N
00000029	02	00000002	\N	F	\N	2013-01-26	1	\N
00000029	02	00000002	\N	D	\N	2013-01-27	1	\N
00000029	02	00000002	\N	D	\N	2013-01-28	1	\N
00000029	02	00000002	\N	D	\N	2013-01-29	1	\N
00000029	02	00000002	\N	D	\N	2013-01-30	1	\N
00000029	02	00000002	\N	D	\N	2013-01-31	1	\N
00000030	02	00000002	\N	D	\N	2013-01-01	1	\N
00000030	02	00000002	\N	D	\N	2013-01-02	1	\N
00000030	02	00000002	\N	D	\N	2013-01-03	1	\N
00000030	02	00000002	\N	F	\N	2013-01-04	1	\N
00000030	02	00000002	\N	F	\N	2013-01-05	1	\N
00000030	02	00000002	\N	D	\N	2013-01-06	1	\N
00000030	02	00000002	\N	D	\N	2013-01-07	1	\N
00000030	02	00000002	\N	D	\N	2013-01-08	1	\N
00000030	02	00000002	\N	D	\N	2013-01-09	1	\N
00000030	02	00000002	\N	D	\N	2013-01-10	1	\N
00000030	02	00000002	\N	F	\N	2013-01-11	1	\N
00000030	02	00000002	\N	F	\N	2013-01-12	1	\N
00000030	02	00000002	\N	D	\N	2013-01-13	1	\N
00000030	02	00000002	\N	D	\N	2013-01-14	1	\N
00000030	02	00000002	\N	D	\N	2013-01-15	1	\N
00000030	02	00000002	\N	D	\N	2013-01-16	1	\N
00000030	02	00000002	\N	D	\N	2013-01-17	1	\N
00000030	02	00000002	\N	F	\N	2013-01-18	1	\N
00000030	02	00000002	\N	F	\N	2013-01-19	1	\N
00000030	02	00000002	\N	D	\N	2013-01-20	1	\N
00000030	02	00000002	\N	D	\N	2013-01-21	1	\N
00000030	02	00000002	\N	D	\N	2013-01-22	1	\N
00000030	02	00000002	\N	D	\N	2013-01-23	1	\N
00000030	02	00000002	\N	D	\N	2013-01-24	1	\N
00000030	02	00000002	\N	F	\N	2013-01-25	1	\N
00000030	02	00000002	\N	F	\N	2013-01-26	1	\N
00000030	02	00000002	\N	D	\N	2013-01-27	1	\N
00000030	02	00000002	\N	D	\N	2013-01-28	1	\N
00000030	02	00000002	\N	D	\N	2013-01-29	1	\N
00000030	02	00000002	\N	D	\N	2013-01-30	1	\N
00000030	02	00000002	\N	D	\N	2013-01-31	1	\N
00000031	02	00000002	\N	D	\N	2013-01-01	1	\N
00000031	02	00000002	\N	D	\N	2013-01-02	1	\N
00000031	02	00000002	\N	D	\N	2013-01-03	1	\N
00000031	02	00000002	\N	F	\N	2013-01-04	1	\N
00000031	02	00000002	\N	F	\N	2013-01-05	1	\N
00000031	02	00000002	\N	D	\N	2013-01-06	1	\N
00000031	02	00000002	\N	D	\N	2013-01-07	1	\N
00000031	02	00000002	\N	D	\N	2013-01-08	1	\N
00000031	02	00000002	\N	D	\N	2013-01-09	1	\N
00000031	02	00000002	\N	D	\N	2013-01-10	1	\N
00000031	02	00000002	\N	F	\N	2013-01-11	1	\N
00000031	02	00000002	\N	F	\N	2013-01-12	1	\N
00000031	02	00000002	\N	D	\N	2013-01-13	1	\N
00000031	02	00000002	\N	D	\N	2013-01-14	1	\N
00000031	02	00000002	\N	D	\N	2013-01-15	1	\N
00000031	02	00000002	\N	D	\N	2013-01-16	1	\N
00000031	02	00000002	\N	D	\N	2013-01-17	1	\N
00000031	02	00000002	\N	F	\N	2013-01-18	1	\N
00000031	02	00000002	\N	F	\N	2013-01-19	1	\N
00000031	02	00000002	\N	D	\N	2013-01-20	1	\N
00000031	02	00000002	\N	D	\N	2013-01-21	1	\N
00000031	02	00000002	\N	D	\N	2013-01-22	1	\N
00000031	02	00000002	\N	D	\N	2013-01-23	1	\N
00000031	02	00000002	\N	D	\N	2013-01-24	1	\N
00000031	02	00000002	\N	F	\N	2013-01-25	1	\N
00000031	02	00000002	\N	F	\N	2013-01-26	1	\N
00000031	02	00000002	\N	D	\N	2013-01-27	1	\N
00000031	02	00000002	\N	D	\N	2013-01-28	1	\N
00000031	02	00000002	\N	D	\N	2013-01-29	1	\N
00000031	02	00000002	\N	D	\N	2013-01-30	1	\N
00000031	02	00000002	\N	D	\N	2013-01-31	1	\N
00000032	02	00000002	\N	D	\N	2013-01-01	1	\N
00000032	02	00000002	\N	D	\N	2013-01-02	1	\N
00000032	02	00000002	\N	D	\N	2013-01-03	1	\N
00000032	02	00000002	\N	F	\N	2013-01-04	1	\N
00000032	02	00000002	\N	F	\N	2013-01-05	1	\N
00000032	02	00000002	\N	D	\N	2013-01-06	1	\N
00000032	02	00000002	\N	D	\N	2013-01-07	1	\N
00000032	02	00000002	\N	D	\N	2013-01-08	1	\N
00000032	02	00000002	\N	D	\N	2013-01-09	1	\N
00000032	02	00000002	\N	D	\N	2013-01-10	1	\N
00000032	02	00000002	\N	F	\N	2013-01-11	1	\N
00000032	02	00000002	\N	F	\N	2013-01-12	1	\N
00000032	02	00000002	\N	D	\N	2013-01-13	1	\N
00000032	02	00000002	\N	D	\N	2013-01-14	1	\N
00000032	02	00000002	\N	D	\N	2013-01-15	1	\N
00000032	02	00000002	\N	D	\N	2013-01-16	1	\N
00000032	02	00000002	\N	D	\N	2013-01-17	1	\N
00000032	02	00000002	\N	F	\N	2013-01-18	1	\N
00000032	02	00000002	\N	F	\N	2013-01-19	1	\N
00000032	02	00000002	\N	D	\N	2013-01-20	1	\N
00000032	02	00000002	\N	D	\N	2013-01-21	1	\N
00000032	02	00000002	\N	D	\N	2013-01-22	1	\N
00000032	02	00000002	\N	D	\N	2013-01-23	1	\N
00000032	02	00000002	\N	D	\N	2013-01-24	1	\N
00000032	02	00000002	\N	F	\N	2013-01-25	1	\N
00000032	02	00000002	\N	F	\N	2013-01-26	1	\N
00000032	02	00000002	\N	D	\N	2013-01-27	1	\N
00000032	02	00000002	\N	D	\N	2013-01-28	1	\N
00000032	02	00000002	\N	D	\N	2013-01-29	1	\N
00000032	02	00000002	\N	D	\N	2013-01-30	1	\N
00000032	02	00000002	\N	D	\N	2013-01-31	1	\N
00000033	02	00000002	\N	D	\N	2013-01-01	1	\N
00000033	02	00000002	\N	D	\N	2013-01-02	1	\N
00000033	02	00000002	\N	D	\N	2013-01-03	1	\N
00000033	02	00000002	\N	F	\N	2013-01-04	1	\N
00000033	02	00000002	\N	F	\N	2013-01-05	1	\N
00000033	02	00000002	\N	D	\N	2013-01-06	1	\N
00000033	02	00000002	\N	D	\N	2013-01-07	1	\N
00000033	02	00000002	\N	D	\N	2013-01-08	1	\N
00000033	02	00000002	\N	D	\N	2013-01-09	1	\N
00000033	02	00000002	\N	D	\N	2013-01-10	1	\N
00000033	02	00000002	\N	F	\N	2013-01-11	1	\N
00000033	02	00000002	\N	F	\N	2013-01-12	1	\N
00000033	02	00000002	\N	D	\N	2013-01-13	1	\N
00000033	02	00000002	\N	D	\N	2013-01-14	1	\N
00000033	02	00000002	\N	D	\N	2013-01-15	1	\N
00000033	02	00000002	\N	D	\N	2013-01-16	1	\N
00000033	02	00000002	\N	D	\N	2013-01-17	1	\N
00000033	02	00000002	\N	F	\N	2013-01-18	1	\N
00000033	02	00000002	\N	F	\N	2013-01-19	1	\N
00000033	02	00000002	\N	D	\N	2013-01-20	1	\N
00000033	02	00000002	\N	D	\N	2013-01-21	1	\N
00000033	02	00000002	\N	D	\N	2013-01-22	1	\N
00000033	02	00000002	\N	D	\N	2013-01-23	1	\N
00000033	02	00000002	\N	D	\N	2013-01-24	1	\N
00000033	02	00000002	\N	F	\N	2013-01-25	1	\N
00000033	02	00000002	\N	F	\N	2013-01-26	1	\N
00000033	02	00000002	\N	D	\N	2013-01-27	1	\N
00000033	02	00000002	\N	D	\N	2013-01-28	1	\N
00000033	02	00000002	\N	D	\N	2013-01-29	1	\N
00000033	02	00000002	\N	D	\N	2013-01-30	1	\N
00000033	02	00000002	\N	D	\N	2013-01-31	1	\N
00000034	02	00000002	\N	D	\N	2013-01-01	1	\N
00000034	02	00000002	\N	D	\N	2013-01-02	1	\N
00000034	02	00000002	\N	D	\N	2013-01-03	1	\N
00000034	02	00000002	\N	F	\N	2013-01-04	1	\N
00000034	02	00000002	\N	F	\N	2013-01-05	1	\N
00000034	02	00000002	\N	D	\N	2013-01-06	1	\N
00000034	02	00000002	\N	D	\N	2013-01-07	1	\N
00000034	02	00000002	\N	D	\N	2013-01-08	1	\N
00000034	02	00000002	\N	D	\N	2013-01-09	1	\N
00000034	02	00000002	\N	D	\N	2013-01-10	1	\N
00000034	02	00000002	\N	F	\N	2013-01-11	1	\N
00000034	02	00000002	\N	F	\N	2013-01-12	1	\N
00000034	02	00000002	\N	D	\N	2013-01-13	1	\N
00000034	02	00000002	\N	D	\N	2013-01-14	1	\N
00000034	02	00000002	\N	D	\N	2013-01-15	1	\N
00000034	02	00000002	\N	D	\N	2013-01-16	1	\N
00000034	02	00000002	\N	D	\N	2013-01-17	1	\N
00000034	02	00000002	\N	F	\N	2013-01-18	1	\N
00000034	02	00000002	\N	F	\N	2013-01-19	1	\N
00000034	02	00000002	\N	D	\N	2013-01-20	1	\N
00000034	02	00000002	\N	D	\N	2013-01-21	1	\N
00000034	02	00000002	\N	D	\N	2013-01-22	1	\N
00000034	02	00000002	\N	D	\N	2013-01-23	1	\N
00000034	02	00000002	\N	D	\N	2013-01-24	1	\N
00000034	02	00000002	\N	F	\N	2013-01-25	1	\N
00000034	02	00000002	\N	F	\N	2013-01-26	1	\N
00000034	02	00000002	\N	D	\N	2013-01-27	1	\N
00000034	02	00000002	\N	D	\N	2013-01-28	1	\N
00000034	02	00000002	\N	D	\N	2013-01-29	1	\N
00000034	02	00000002	\N	D	\N	2013-01-30	1	\N
00000034	02	00000002	\N	D	\N	2013-01-31	1	\N
00000035	02	00000002	\N	D	\N	2013-01-01	1	\N
00000035	02	00000002	\N	D	\N	2013-01-02	1	\N
00000035	02	00000002	\N	D	\N	2013-01-03	1	\N
00000035	02	00000002	\N	F	\N	2013-01-04	1	\N
00000035	02	00000002	\N	F	\N	2013-01-05	1	\N
00000035	02	00000002	\N	D	\N	2013-01-06	1	\N
00000035	02	00000002	\N	D	\N	2013-01-07	1	\N
00000035	02	00000002	\N	D	\N	2013-01-08	1	\N
00000035	02	00000002	\N	D	\N	2013-01-09	1	\N
00000035	02	00000002	\N	D	\N	2013-01-10	1	\N
00000035	02	00000002	\N	F	\N	2013-01-11	1	\N
00000035	02	00000002	\N	F	\N	2013-01-12	1	\N
00000035	02	00000002	\N	D	\N	2013-01-13	1	\N
00000035	02	00000002	\N	D	\N	2013-01-14	1	\N
00000035	02	00000002	\N	D	\N	2013-01-15	1	\N
00000035	02	00000002	\N	D	\N	2013-01-16	1	\N
00000035	02	00000002	\N	D	\N	2013-01-17	1	\N
00000035	02	00000002	\N	F	\N	2013-01-18	1	\N
00000035	02	00000002	\N	F	\N	2013-01-19	1	\N
00000035	02	00000002	\N	D	\N	2013-01-20	1	\N
00000035	02	00000002	\N	D	\N	2013-01-21	1	\N
00000035	02	00000002	\N	D	\N	2013-01-22	1	\N
00000035	02	00000002	\N	D	\N	2013-01-23	1	\N
00000035	02	00000002	\N	D	\N	2013-01-24	1	\N
00000035	02	00000002	\N	F	\N	2013-01-25	1	\N
00000035	02	00000002	\N	F	\N	2013-01-26	1	\N
00000035	02	00000002	\N	D	\N	2013-01-27	1	\N
00000035	02	00000002	\N	D	\N	2013-01-28	1	\N
00000035	02	00000002	\N	D	\N	2013-01-29	1	\N
00000035	02	00000002	\N	D	\N	2013-01-30	1	\N
00000035	02	00000002	\N	D	\N	2013-01-31	1	\N
00000036	02	00000002	\N	D	\N	2013-01-01	1	\N
00000036	02	00000002	\N	D	\N	2013-01-02	1	\N
00000036	02	00000002	\N	D	\N	2013-01-03	1	\N
00000036	02	00000002	\N	F	\N	2013-01-04	1	\N
00000036	02	00000002	\N	F	\N	2013-01-05	1	\N
00000036	02	00000002	\N	D	\N	2013-01-06	1	\N
00000036	02	00000002	\N	D	\N	2013-01-07	1	\N
00000036	02	00000002	\N	D	\N	2013-01-08	1	\N
00000036	02	00000002	\N	D	\N	2013-01-09	1	\N
00000036	02	00000002	\N	D	\N	2013-01-10	1	\N
00000036	02	00000002	\N	F	\N	2013-01-11	1	\N
00000036	02	00000002	\N	F	\N	2013-01-12	1	\N
00000036	02	00000002	\N	D	\N	2013-01-13	1	\N
00000036	02	00000002	\N	D	\N	2013-01-14	1	\N
00000036	02	00000002	\N	D	\N	2013-01-15	1	\N
00000036	02	00000002	\N	D	\N	2013-01-16	1	\N
00000036	02	00000002	\N	D	\N	2013-01-17	1	\N
00000036	02	00000002	\N	F	\N	2013-01-18	1	\N
00000036	02	00000002	\N	F	\N	2013-01-19	1	\N
00000036	02	00000002	\N	D	\N	2013-01-20	1	\N
00000036	02	00000002	\N	D	\N	2013-01-21	1	\N
00000036	02	00000002	\N	D	\N	2013-01-22	1	\N
00000036	02	00000002	\N	D	\N	2013-01-23	1	\N
00000036	02	00000002	\N	D	\N	2013-01-24	1	\N
00000036	02	00000002	\N	F	\N	2013-01-25	1	\N
00000036	02	00000002	\N	F	\N	2013-01-26	1	\N
00000036	02	00000002	\N	D	\N	2013-01-27	1	\N
00000036	02	00000002	\N	D	\N	2013-01-28	1	\N
00000036	02	00000002	\N	D	\N	2013-01-29	1	\N
00000036	02	00000002	\N	D	\N	2013-01-30	1	\N
00000036	02	00000002	\N	D	\N	2013-01-31	1	\N
00000037	02	00000002	\N	D	\N	2013-01-01	1	\N
00000037	02	00000002	\N	D	\N	2013-01-02	1	\N
00000037	02	00000002	\N	D	\N	2013-01-03	1	\N
00000037	02	00000002	\N	F	\N	2013-01-04	1	\N
00000037	02	00000002	\N	F	\N	2013-01-05	1	\N
00000037	02	00000002	\N	D	\N	2013-01-06	1	\N
00000037	02	00000002	\N	D	\N	2013-01-07	1	\N
00000037	02	00000002	\N	D	\N	2013-01-08	1	\N
00000037	02	00000002	\N	D	\N	2013-01-09	1	\N
00000037	02	00000002	\N	D	\N	2013-01-10	1	\N
00000037	02	00000002	\N	F	\N	2013-01-11	1	\N
00000037	02	00000002	\N	F	\N	2013-01-12	1	\N
00000037	02	00000002	\N	D	\N	2013-01-13	1	\N
00000037	02	00000002	\N	D	\N	2013-01-14	1	\N
00000037	02	00000002	\N	D	\N	2013-01-15	1	\N
00000037	02	00000002	\N	D	\N	2013-01-16	1	\N
00000037	02	00000002	\N	D	\N	2013-01-17	1	\N
00000037	02	00000002	\N	F	\N	2013-01-18	1	\N
00000037	02	00000002	\N	F	\N	2013-01-19	1	\N
00000037	02	00000002	\N	D	\N	2013-01-20	1	\N
00000037	02	00000002	\N	D	\N	2013-01-21	1	\N
00000037	02	00000002	\N	D	\N	2013-01-22	1	\N
00000037	02	00000002	\N	D	\N	2013-01-23	1	\N
00000037	02	00000002	\N	D	\N	2013-01-24	1	\N
00000037	02	00000002	\N	F	\N	2013-01-25	1	\N
00000037	02	00000002	\N	F	\N	2013-01-26	1	\N
00000037	02	00000002	\N	D	\N	2013-01-27	1	\N
00000037	02	00000002	\N	D	\N	2013-01-28	1	\N
00000037	02	00000002	\N	D	\N	2013-01-29	1	\N
00000037	02	00000002	\N	D	\N	2013-01-30	1	\N
00000037	02	00000002	\N	D	\N	2013-01-31	1	\N
00000038	02	00000002	\N	D	\N	2013-01-01	1	\N
00000038	02	00000002	\N	D	\N	2013-01-02	1	\N
00000038	02	00000002	\N	D	\N	2013-01-03	1	\N
00000038	02	00000002	\N	F	\N	2013-01-04	1	\N
00000038	02	00000002	\N	F	\N	2013-01-05	1	\N
00000038	02	00000002	\N	D	\N	2013-01-06	1	\N
00000038	02	00000002	\N	D	\N	2013-01-07	1	\N
00000038	02	00000002	\N	D	\N	2013-01-08	1	\N
00000038	02	00000002	\N	D	\N	2013-01-09	1	\N
00000038	02	00000002	\N	D	\N	2013-01-10	1	\N
00000038	02	00000002	\N	F	\N	2013-01-11	1	\N
00000038	02	00000002	\N	F	\N	2013-01-12	1	\N
00000038	02	00000002	\N	D	\N	2013-01-13	1	\N
00000038	02	00000002	\N	D	\N	2013-01-14	1	\N
00000038	02	00000002	\N	D	\N	2013-01-15	1	\N
00000038	02	00000002	\N	D	\N	2013-01-16	1	\N
00000038	02	00000002	\N	D	\N	2013-01-17	1	\N
00000038	02	00000002	\N	F	\N	2013-01-18	1	\N
00000038	02	00000002	\N	F	\N	2013-01-19	1	\N
00000038	02	00000002	\N	D	\N	2013-01-20	1	\N
00000038	02	00000002	\N	D	\N	2013-01-21	1	\N
00000038	02	00000002	\N	D	\N	2013-01-22	1	\N
00000038	02	00000002	\N	D	\N	2013-01-23	1	\N
00000038	02	00000002	\N	D	\N	2013-01-24	1	\N
00000038	02	00000002	\N	F	\N	2013-01-25	1	\N
00000038	02	00000002	\N	F	\N	2013-01-26	1	\N
00000038	02	00000002	\N	D	\N	2013-01-27	1	\N
00000038	02	00000002	\N	D	\N	2013-01-28	1	\N
00000038	02	00000002	\N	D	\N	2013-01-29	1	\N
00000038	02	00000002	\N	D	\N	2013-01-30	1	\N
00000038	02	00000002	\N	D	\N	2013-01-31	1	\N
00000039	02	00000002	\N	D	\N	2013-01-01	1	\N
00000039	02	00000002	\N	D	\N	2013-01-02	1	\N
00000039	02	00000002	\N	D	\N	2013-01-03	1	\N
00000039	02	00000002	\N	F	\N	2013-01-04	1	\N
00000039	02	00000002	\N	F	\N	2013-01-05	1	\N
00000039	02	00000002	\N	D	\N	2013-01-06	1	\N
00000039	02	00000002	\N	D	\N	2013-01-07	1	\N
00000039	02	00000002	\N	D	\N	2013-01-08	1	\N
00000039	02	00000002	\N	D	\N	2013-01-09	1	\N
00000039	02	00000002	\N	D	\N	2013-01-10	1	\N
00000039	02	00000002	\N	F	\N	2013-01-11	1	\N
00000039	02	00000002	\N	F	\N	2013-01-12	1	\N
00000039	02	00000002	\N	D	\N	2013-01-13	1	\N
00000039	02	00000002	\N	D	\N	2013-01-14	1	\N
00000039	02	00000002	\N	D	\N	2013-01-15	1	\N
00000039	02	00000002	\N	D	\N	2013-01-16	1	\N
00000039	02	00000002	\N	D	\N	2013-01-17	1	\N
00000039	02	00000002	\N	F	\N	2013-01-18	1	\N
00000039	02	00000002	\N	F	\N	2013-01-19	1	\N
00000039	02	00000002	\N	D	\N	2013-01-20	1	\N
00000039	02	00000002	\N	D	\N	2013-01-21	1	\N
00000039	02	00000002	\N	D	\N	2013-01-22	1	\N
00000039	02	00000002	\N	D	\N	2013-01-23	1	\N
00000039	02	00000002	\N	D	\N	2013-01-24	1	\N
00000039	02	00000002	\N	F	\N	2013-01-25	1	\N
00000039	02	00000002	\N	F	\N	2013-01-26	1	\N
00000039	02	00000002	\N	D	\N	2013-01-27	1	\N
00000039	02	00000002	\N	D	\N	2013-01-28	1	\N
00000039	02	00000002	\N	D	\N	2013-01-29	1	\N
00000039	02	00000002	\N	D	\N	2013-01-30	1	\N
00000039	02	00000002	\N	D	\N	2013-01-31	1	\N
00000040	02	00000002	\N	D	\N	2013-01-01	1	\N
00000040	02	00000002	\N	D	\N	2013-01-02	1	\N
00000040	02	00000002	\N	D	\N	2013-01-03	1	\N
00000040	02	00000002	\N	F	\N	2013-01-04	1	\N
00000040	02	00000002	\N	F	\N	2013-01-05	1	\N
00000040	02	00000002	\N	D	\N	2013-01-06	1	\N
00000040	02	00000002	\N	D	\N	2013-01-07	1	\N
00000040	02	00000002	\N	D	\N	2013-01-08	1	\N
00000040	02	00000002	\N	D	\N	2013-01-09	1	\N
00000040	02	00000002	\N	D	\N	2013-01-10	1	\N
00000040	02	00000002	\N	F	\N	2013-01-11	1	\N
00000040	02	00000002	\N	F	\N	2013-01-12	1	\N
00000040	02	00000002	\N	D	\N	2013-01-13	1	\N
00000040	02	00000002	\N	D	\N	2013-01-14	1	\N
00000040	02	00000002	\N	D	\N	2013-01-15	1	\N
00000040	02	00000002	\N	D	\N	2013-01-16	1	\N
00000040	02	00000002	\N	D	\N	2013-01-17	1	\N
00000040	02	00000002	\N	F	\N	2013-01-18	1	\N
00000040	02	00000002	\N	F	\N	2013-01-19	1	\N
00000040	02	00000002	\N	D	\N	2013-01-20	1	\N
00000040	02	00000002	\N	D	\N	2013-01-21	1	\N
00000040	02	00000002	\N	D	\N	2013-01-22	1	\N
00000040	02	00000002	\N	D	\N	2013-01-23	1	\N
00000040	02	00000002	\N	D	\N	2013-01-24	1	\N
00000040	02	00000002	\N	F	\N	2013-01-25	1	\N
00000040	02	00000002	\N	F	\N	2013-01-26	1	\N
00000040	02	00000002	\N	D	\N	2013-01-27	1	\N
00000040	02	00000002	\N	D	\N	2013-01-28	1	\N
00000040	02	00000002	\N	D	\N	2013-01-29	1	\N
00000040	02	00000002	\N	D	\N	2013-01-30	1	\N
00000040	02	00000002	\N	D	\N	2013-01-31	1	\N
00000041	02	00000002	\N	D	\N	2013-01-01	1	\N
00000041	02	00000002	\N	D	\N	2013-01-02	1	\N
00000041	02	00000002	\N	D	\N	2013-01-03	1	\N
00000041	02	00000002	\N	F	\N	2013-01-04	1	\N
00000041	02	00000002	\N	F	\N	2013-01-05	1	\N
00000041	02	00000002	\N	D	\N	2013-01-06	1	\N
00000041	02	00000002	\N	D	\N	2013-01-07	1	\N
00000041	02	00000002	\N	D	\N	2013-01-08	1	\N
00000041	02	00000002	\N	D	\N	2013-01-09	1	\N
00000041	02	00000002	\N	D	\N	2013-01-10	1	\N
00000041	02	00000002	\N	F	\N	2013-01-11	1	\N
00000041	02	00000002	\N	F	\N	2013-01-12	1	\N
00000041	02	00000002	\N	D	\N	2013-01-13	1	\N
00000041	02	00000002	\N	D	\N	2013-01-14	1	\N
00000041	02	00000002	\N	D	\N	2013-01-15	1	\N
00000041	02	00000002	\N	D	\N	2013-01-16	1	\N
00000041	02	00000002	\N	D	\N	2013-01-17	1	\N
00000041	02	00000002	\N	F	\N	2013-01-18	1	\N
00000041	02	00000002	\N	F	\N	2013-01-19	1	\N
00000041	02	00000002	\N	D	\N	2013-01-20	1	\N
00000041	02	00000002	\N	D	\N	2013-01-21	1	\N
00000041	02	00000002	\N	D	\N	2013-01-22	1	\N
00000041	02	00000002	\N	D	\N	2013-01-23	1	\N
00000041	02	00000002	\N	D	\N	2013-01-24	1	\N
00000041	02	00000002	\N	F	\N	2013-01-25	1	\N
00000041	02	00000002	\N	F	\N	2013-01-26	1	\N
00000041	02	00000002	\N	D	\N	2013-01-27	1	\N
00000041	02	00000002	\N	D	\N	2013-01-28	1	\N
00000041	02	00000002	\N	D	\N	2013-01-29	1	\N
00000041	02	00000002	\N	D	\N	2013-01-30	1	\N
00000041	02	00000002	\N	D	\N	2013-01-31	1	\N
00000076	01	00000002	\N	D	\N	2013-03-28	1	\N
00000035	02	00000002	\N	D	\N	2012-09-18	0	\N
00000035	02	00000002	\N	D	\N	2012-09-19	0	\N
00000035	02	00000002	\N	D	\N	2012-09-20	0	\N
00000035	02	00000002	\N	F	\N	2012-09-21	0	\N
00000035	02	00000002	\N	F	\N	2012-09-22	0	\N
00000035	02	00000002	\N	D	\N	2012-09-23	0	\N
00000076	01	00000002	\N	F	\N	2013-03-29	1	\N
00000076	01	00000002	\N	F	\N	2013-03-30	1	\N
00000076	01	00000002	\N	D	\N	2013-03-31	1	\N
00000001	01	00000002	\N	D	\N	2013-04-01	1	\N
00000001	01	00000002	\N	D	\N	2013-04-02	1	\N
00000001	01	00000002	\N	D	\N	2013-04-03	1	\N
00000001	01	00000002	\N	D	\N	2013-04-04	1	\N
00000001	01	00000002	\N	F	\N	2013-04-05	1	\N
00000001	01	00000002	\N	F	\N	2013-04-06	1	\N
00000001	01	00000002	\N	D	\N	2013-04-07	1	\N
00000001	01	00000002	\N	D	\N	2013-04-08	1	\N
00000001	01	00000002	\N	D	\N	2013-04-09	1	\N
00000001	01	00000002	\N	D	\N	2013-04-10	1	\N
00000001	01	00000002	\N	D	\N	2013-04-11	1	\N
00000001	01	00000002	\N	F	\N	2013-04-12	1	\N
00000001	01	00000002	\N	F	\N	2013-04-13	1	\N
00000001	01	00000002	\N	D	\N	2013-04-14	1	\N
00000001	01	00000002	\N	D	\N	2013-04-15	1	\N
00000001	01	00000002	\N	D	\N	2013-04-16	1	\N
00000001	01	00000002	\N	D	\N	2013-04-17	1	\N
00000001	01	00000002	\N	D	\N	2013-04-18	1	\N
00000001	01	00000002	\N	F	\N	2013-04-19	1	\N
00000001	01	00000002	\N	F	\N	2013-04-20	1	\N
00000001	01	00000002	\N	D	\N	2013-04-21	1	\N
00000001	01	00000002	\N	D	\N	2013-04-22	1	\N
00000001	01	00000002	\N	D	\N	2013-04-23	1	\N
00000001	01	00000002	\N	D	\N	2013-04-24	1	\N
00000001	01	00000002	\N	D	\N	2013-04-25	1	\N
00000001	01	00000002	\N	F	\N	2013-04-26	1	\N
00000001	01	00000002	\N	F	\N	2013-04-27	1	\N
00000001	01	00000002	\N	D	\N	2013-04-28	1	\N
00000001	01	00000002	\N	D	\N	2013-04-29	1	\N
00000001	01	00000002	\N	D	\N	2013-04-30	1	\N
00000002	01	00000002	\N	D	\N	2013-04-01	1	\N
00000002	01	00000002	\N	D	\N	2013-04-02	1	\N
00000002	01	00000002	\N	D	\N	2013-04-03	1	\N
00000002	01	00000002	\N	D	\N	2013-04-04	1	\N
00000002	01	00000002	\N	F	\N	2013-04-05	1	\N
00000002	01	00000002	\N	F	\N	2013-04-06	1	\N
00000002	01	00000002	\N	D	\N	2013-04-07	1	\N
00000002	01	00000002	\N	D	\N	2013-04-08	1	\N
00000002	01	00000002	\N	D	\N	2013-04-09	1	\N
00000002	01	00000002	\N	D	\N	2013-04-10	1	\N
00000002	01	00000002	\N	D	\N	2013-04-11	1	\N
00000002	01	00000002	\N	F	\N	2013-04-12	1	\N
00000017	02	00000002	\N	D	0000000007	2012-11-13	R	\N
00000009	04	00000002	\N	D	0000000014	2013-01-01	R	\N
00000009	04	00000002	\N	D	0000000014	2013-01-02	R	\N
00000009	04	00000002	\N	D	0000000014	2013-01-03	R	\N
00000009	04	00000002	\N	F	0000000014	2013-01-04	R	\N
00000002	01	00000002	\N	F	\N	2013-04-13	1	\N
00000002	01	00000002	\N	D	\N	2013-04-14	1	\N
00000002	01	00000002	\N	D	\N	2013-04-15	1	\N
00000002	01	00000002	\N	D	\N	2013-04-16	1	\N
00000002	01	00000002	\N	D	\N	2013-04-17	1	\N
00000002	01	00000002	\N	D	\N	2013-04-18	1	\N
00000002	01	00000002	\N	F	\N	2013-04-19	1	\N
00000002	01	00000002	\N	F	\N	2013-04-20	1	\N
00000002	01	00000002	\N	D	\N	2013-04-21	1	\N
00000002	01	00000002	\N	D	\N	2013-04-22	1	\N
00000002	01	00000002	\N	D	\N	2013-04-23	1	\N
00000002	01	00000002	\N	D	\N	2013-04-24	1	\N
00000002	01	00000002	\N	D	\N	2013-04-25	1	\N
00000002	01	00000002	\N	F	\N	2013-04-26	1	\N
00000002	01	00000002	\N	F	\N	2013-04-27	1	\N
00000002	01	00000002	\N	D	\N	2013-04-28	1	\N
00000002	01	00000002	\N	D	\N	2013-04-29	1	\N
00000002	01	00000002	\N	D	\N	2013-04-30	1	\N
00000003	01	00000002	\N	D	\N	2013-04-01	1	\N
00000003	01	00000002	\N	D	\N	2013-04-02	1	\N
00000003	01	00000002	\N	D	\N	2013-04-03	1	\N
00000003	01	00000002	\N	D	\N	2013-04-04	1	\N
00000003	01	00000002	\N	F	\N	2013-04-05	1	\N
00000003	01	00000002	\N	F	\N	2013-04-06	1	\N
00000003	01	00000002	\N	D	\N	2013-04-07	1	\N
00000003	01	00000002	\N	D	\N	2013-04-08	1	\N
00000003	01	00000002	\N	D	\N	2013-04-09	1	\N
00000003	01	00000002	\N	D	\N	2013-04-10	1	\N
00000003	01	00000002	\N	D	\N	2013-04-11	1	\N
00000003	01	00000002	\N	F	\N	2013-04-12	1	\N
00000003	01	00000002	\N	F	\N	2013-04-13	1	\N
00000003	01	00000002	\N	D	\N	2013-04-14	1	\N
00000003	01	00000002	\N	D	\N	2013-04-15	1	\N
00000003	01	00000002	\N	D	\N	2013-04-16	1	\N
00000003	01	00000002	\N	D	\N	2013-04-17	1	\N
00000003	01	00000002	\N	D	\N	2013-04-18	1	\N
00000003	01	00000002	\N	F	\N	2013-04-19	1	\N
00000003	01	00000002	\N	F	\N	2013-04-20	1	\N
00000003	01	00000002	\N	D	\N	2013-04-21	1	\N
00000003	01	00000002	\N	D	\N	2013-04-22	1	\N
00000003	01	00000002	\N	D	\N	2013-04-23	1	\N
00000003	01	00000002	\N	D	\N	2013-04-24	1	\N
00000003	01	00000002	\N	D	\N	2013-04-25	1	\N
00000003	01	00000002	\N	F	\N	2013-04-26	1	\N
00000003	01	00000002	\N	F	\N	2013-04-27	1	\N
00000003	01	00000002	\N	D	\N	2013-04-28	1	\N
00000003	01	00000002	\N	D	\N	2013-04-29	1	\N
00000003	01	00000002	\N	D	\N	2013-04-30	1	\N
00000004	01	00000002	\N	D	\N	2013-04-01	1	\N
00000004	01	00000002	\N	D	\N	2013-04-02	1	\N
00000004	01	00000002	\N	D	\N	2013-04-03	1	\N
00000004	01	00000002	\N	D	\N	2013-04-04	1	\N
00000004	01	00000002	\N	F	\N	2013-04-05	1	\N
00000004	01	00000002	\N	F	\N	2013-04-06	1	\N
00000004	01	00000002	\N	D	\N	2013-04-07	1	\N
00000004	01	00000002	\N	D	\N	2013-04-08	1	\N
00000004	01	00000002	\N	D	\N	2013-04-09	1	\N
00000004	01	00000002	\N	D	\N	2013-04-10	1	\N
00000004	01	00000002	\N	D	\N	2013-04-11	1	\N
00000004	01	00000002	\N	F	\N	2013-04-12	1	\N
00000004	01	00000002	\N	F	\N	2013-04-13	1	\N
00000004	01	00000002	\N	D	\N	2013-04-14	1	\N
00000004	01	00000002	\N	D	\N	2013-04-15	1	\N
00000004	01	00000002	\N	D	\N	2013-04-16	1	\N
00000004	01	00000002	\N	D	\N	2013-04-17	1	\N
00000004	01	00000002	\N	D	\N	2013-04-18	1	\N
00000004	01	00000002	\N	F	\N	2013-04-19	1	\N
00000004	01	00000002	\N	F	\N	2013-04-20	1	\N
00000004	01	00000002	\N	D	\N	2013-04-21	1	\N
00000004	01	00000002	\N	D	\N	2013-04-22	1	\N
00000004	01	00000002	\N	D	\N	2013-04-23	1	\N
00000004	01	00000002	\N	D	\N	2013-04-24	1	\N
00000004	01	00000002	\N	D	\N	2013-04-25	1	\N
00000004	01	00000002	\N	F	\N	2013-04-26	1	\N
00000004	01	00000002	\N	F	\N	2013-04-27	1	\N
00000004	01	00000002	\N	D	\N	2013-04-28	1	\N
00000004	01	00000002	\N	D	\N	2013-04-29	1	\N
00000004	01	00000002	\N	D	\N	2013-04-30	1	\N
00000005	01	00000002	\N	D	\N	2013-04-01	1	\N
00000005	01	00000002	\N	D	\N	2013-04-02	1	\N
00000005	01	00000002	\N	D	\N	2013-04-03	1	\N
00000005	01	00000002	\N	D	\N	2013-04-04	1	\N
00000005	01	00000002	\N	F	\N	2013-04-05	1	\N
00000005	01	00000002	\N	F	\N	2013-04-06	1	\N
00000005	01	00000002	\N	D	\N	2013-04-07	1	\N
00000005	01	00000002	\N	D	\N	2013-04-08	1	\N
00000005	01	00000002	\N	D	\N	2013-04-09	1	\N
00000005	01	00000002	\N	D	\N	2013-04-10	1	\N
00000005	01	00000002	\N	D	\N	2013-04-11	1	\N
00000005	01	00000002	\N	F	\N	2013-04-12	1	\N
00000005	01	00000002	\N	F	\N	2013-04-13	1	\N
00000005	01	00000002	\N	D	\N	2013-04-14	1	\N
00000005	01	00000002	\N	D	\N	2013-04-15	1	\N
00000005	01	00000002	\N	D	\N	2013-04-16	1	\N
00000005	01	00000002	\N	D	\N	2013-04-17	1	\N
00000009	04	00000002	\N	D	0000000040	2013-01-30	R	\N
00000058	05	00000003	\N	D	\N	2012-11-01	1	\N
00000058	05	00000003	\N	F	\N	2012-11-02	1	\N
00000058	05	00000003	\N	F	\N	2012-11-03	1	\N
00000058	05	00000003	\N	D	\N	2012-11-04	1	\N
00000058	05	00000003	\N	D	\N	2012-11-05	1	\N
00000058	05	00000003	\N	D	\N	2012-11-06	1	\N
00000058	05	00000003	\N	D	\N	2012-11-07	1	\N
00000058	05	00000003	\N	D	\N	2012-11-08	1	\N
00000058	05	00000003	\N	F	\N	2012-11-09	1	\N
00000058	05	00000003	\N	F	\N	2012-11-10	1	\N
00000058	05	00000003	\N	D	\N	2012-11-11	1	\N
00000058	05	00000003	\N	D	\N	2012-11-12	1	\N
00000058	05	00000003	\N	D	\N	2012-11-13	1	\N
00000058	05	00000003	\N	D	\N	2012-11-14	1	\N
00000058	05	00000003	\N	D	\N	2012-11-15	1	\N
00000058	05	00000003	\N	F	\N	2012-11-16	1	\N
00000058	05	00000003	\N	F	\N	2012-11-17	1	\N
00000058	05	00000003	\N	D	\N	2012-11-18	1	\N
00000058	05	00000003	\N	D	\N	2012-11-19	1	\N
00000058	05	00000003	\N	D	\N	2012-11-20	1	\N
00000058	05	00000003	\N	D	\N	2012-11-21	1	\N
00000058	05	00000003	\N	D	\N	2012-11-22	1	\N
00000058	05	00000003	\N	F	\N	2012-11-23	1	\N
00000058	05	00000003	\N	F	\N	2012-11-24	1	\N
00000058	05	00000003	\N	D	\N	2012-11-25	1	\N
00000058	05	00000003	\N	D	\N	2012-11-26	1	\N
00000058	05	00000003	\N	D	\N	2012-11-27	1	\N
00000058	05	00000003	\N	D	\N	2012-11-28	1	\N
00000058	05	00000003	\N	D	\N	2012-11-29	1	\N
00000058	05	00000003	\N	F	\N	2012-11-30	1	\N
00000059	05	00000003	\N	D	\N	2012-11-01	1	\N
00000059	05	00000003	\N	F	\N	2012-11-02	1	\N
00000059	05	00000003	\N	F	\N	2012-11-03	1	\N
00000059	05	00000003	\N	D	\N	2012-11-04	1	\N
00000059	05	00000003	\N	D	\N	2012-11-05	1	\N
00000059	05	00000003	\N	D	\N	2012-11-06	1	\N
00000059	05	00000003	\N	D	\N	2012-11-07	1	\N
00000059	05	00000003	\N	D	\N	2012-11-08	1	\N
00000059	05	00000003	\N	F	\N	2012-11-09	1	\N
00000059	05	00000003	\N	F	\N	2012-11-10	1	\N
00000059	05	00000003	\N	D	\N	2012-11-11	1	\N
00000059	05	00000003	\N	D	\N	2012-11-12	1	\N
00000059	05	00000003	\N	D	\N	2012-11-13	1	\N
00000059	05	00000003	\N	D	\N	2012-11-14	1	\N
00000059	05	00000003	\N	D	\N	2012-11-15	1	\N
00000059	05	00000003	\N	F	\N	2012-11-16	1	\N
00000059	05	00000003	\N	F	\N	2012-11-17	1	\N
00000059	05	00000003	\N	D	\N	2012-11-18	1	\N
00000059	05	00000003	\N	D	\N	2012-11-19	1	\N
00000059	05	00000003	\N	D	\N	2012-11-20	1	\N
00000059	05	00000003	\N	D	\N	2012-11-21	1	\N
00000059	05	00000003	\N	D	\N	2012-11-22	1	\N
00000059	05	00000003	\N	F	\N	2012-11-23	1	\N
00000059	05	00000003	\N	F	\N	2012-11-24	1	\N
00000059	05	00000003	\N	D	\N	2012-11-25	1	\N
00000059	05	00000003	\N	D	\N	2012-11-26	1	\N
00000059	05	00000003	\N	D	\N	2012-11-27	1	\N
00000059	05	00000003	\N	D	\N	2012-11-28	1	\N
00000059	05	00000003	\N	D	\N	2012-11-29	1	\N
00000059	05	00000003	\N	F	\N	2012-11-30	1	\N
00000058	05	00000003	\N	F	\N	2012-12-01	1	\N
00000058	05	00000003	\N	D	\N	2012-12-02	1	\N
00000058	05	00000003	\N	D	\N	2012-12-03	1	\N
00000058	05	00000003	\N	D	\N	2012-12-04	1	\N
00000058	05	00000003	\N	D	\N	2012-12-05	1	\N
00000058	05	00000003	\N	D	\N	2012-12-06	1	\N
00000058	05	00000003	\N	F	\N	2012-12-07	1	\N
00000058	05	00000003	\N	F	\N	2012-12-08	1	\N
00000058	05	00000003	\N	D	\N	2012-12-09	1	\N
00000058	05	00000003	\N	D	\N	2012-12-10	1	\N
00000058	05	00000003	\N	D	\N	2012-12-11	1	\N
00000058	05	00000003	\N	D	\N	2012-12-12	1	\N
00000058	05	00000003	\N	D	\N	2012-12-13	1	\N
00000058	05	00000003	\N	F	\N	2012-12-14	1	\N
00000058	05	00000003	\N	F	\N	2012-12-15	1	\N
00000058	05	00000003	\N	D	\N	2012-12-16	1	\N
00000058	05	00000003	\N	D	\N	2012-12-17	1	\N
00000058	05	00000003	\N	D	\N	2012-12-18	1	\N
00000058	05	00000003	\N	D	\N	2012-12-19	1	\N
00000058	05	00000003	\N	D	\N	2012-12-20	1	\N
00000058	05	00000003	\N	F	\N	2012-12-21	1	\N
00000058	05	00000003	\N	F	\N	2012-12-22	1	\N
00000058	05	00000003	\N	D	\N	2012-12-23	1	\N
00000058	05	00000003	\N	D	\N	2012-12-24	1	\N
00000058	05	00000003	\N	D	\N	2012-12-25	1	\N
00000058	05	00000003	\N	D	\N	2012-12-26	1	\N
00000058	05	00000003	\N	D	\N	2012-12-27	1	\N
00000058	05	00000003	\N	F	\N	2012-12-28	1	\N
00000058	05	00000003	\N	F	\N	2012-12-29	1	\N
00000058	05	00000003	\N	D	\N	2012-12-30	1	\N
00000058	05	00000003	\N	D	\N	2012-12-31	1	\N
00000059	05	00000003	\N	F	\N	2012-12-01	1	\N
00000059	05	00000003	\N	D	\N	2012-12-02	1	\N
00000059	05	00000003	\N	D	\N	2012-12-03	1	\N
00000059	05	00000003	\N	D	\N	2012-12-04	1	\N
00000059	05	00000003	\N	D	\N	2012-12-05	1	\N
00000059	05	00000003	\N	D	\N	2012-12-06	1	\N
00000059	05	00000003	\N	F	\N	2012-12-07	1	\N
00000059	05	00000003	\N	F	\N	2012-12-08	1	\N
00000059	05	00000003	\N	D	\N	2012-12-09	1	\N
00000059	05	00000003	\N	D	\N	2012-12-10	1	\N
00000059	05	00000003	\N	D	\N	2012-12-11	1	\N
00000059	05	00000003	\N	D	\N	2012-12-12	1	\N
00000059	05	00000003	\N	D	\N	2012-12-13	1	\N
00000059	05	00000003	\N	F	\N	2012-12-14	1	\N
00000059	05	00000003	\N	F	\N	2012-12-15	1	\N
00000059	05	00000003	\N	D	\N	2012-12-16	1	\N
00000059	05	00000003	\N	D	\N	2012-12-17	1	\N
00000059	05	00000003	\N	D	\N	2012-12-18	1	\N
00000059	05	00000003	\N	D	\N	2012-12-19	1	\N
00000059	05	00000003	\N	D	\N	2012-12-20	1	\N
00000059	05	00000003	\N	F	\N	2012-12-21	1	\N
00000059	05	00000003	\N	F	\N	2012-12-22	1	\N
00000059	05	00000003	\N	D	\N	2012-12-23	1	\N
00000059	05	00000003	\N	D	\N	2012-12-24	1	\N
00000059	05	00000003	\N	D	\N	2012-12-25	1	\N
00000059	05	00000003	\N	D	\N	2012-12-26	1	\N
00000059	05	00000003	\N	D	\N	2012-12-27	1	\N
00000059	05	00000003	\N	F	\N	2012-12-28	1	\N
00000059	05	00000003	\N	F	\N	2012-12-29	1	\N
00000059	05	00000003	\N	D	\N	2012-12-30	1	\N
00000059	05	00000003	\N	D	\N	2012-12-31	1	\N
00000053	04	00000003	\N	D	\N	2012-11-01	1	\N
00000053	04	00000003	\N	F	\N	2012-11-02	1	\N
00000053	04	00000003	\N	F	\N	2012-11-03	1	\N
00000053	04	00000003	\N	D	\N	2012-11-04	1	\N
00000053	04	00000003	\N	D	\N	2012-11-05	1	\N
00000053	04	00000003	\N	D	\N	2012-11-06	1	\N
00000053	04	00000003	\N	D	\N	2012-11-07	1	\N
00000053	04	00000003	\N	D	\N	2012-11-08	1	\N
00000053	04	00000003	\N	F	\N	2012-11-09	1	\N
00000053	04	00000003	\N	F	\N	2012-11-10	1	\N
00000053	04	00000003	\N	D	\N	2012-11-11	1	\N
00000053	04	00000003	\N	D	\N	2012-11-12	1	\N
00000053	04	00000003	\N	D	\N	2012-11-13	1	\N
00000053	04	00000003	\N	D	\N	2012-11-14	1	\N
00000053	04	00000003	\N	D	\N	2012-11-15	1	\N
00000053	04	00000003	\N	F	\N	2012-11-16	1	\N
00000053	04	00000003	\N	F	\N	2012-11-17	1	\N
00000053	04	00000003	\N	D	\N	2012-11-18	1	\N
00000053	04	00000003	\N	D	\N	2012-11-19	1	\N
00000053	04	00000003	\N	D	\N	2012-11-20	1	\N
00000053	04	00000003	\N	D	\N	2012-11-21	1	\N
00000053	04	00000003	\N	D	\N	2012-11-22	1	\N
00000053	04	00000003	\N	F	\N	2012-11-23	1	\N
00000053	04	00000003	\N	F	\N	2012-11-24	1	\N
00000053	04	00000003	\N	D	\N	2012-11-25	1	\N
00000053	04	00000003	\N	D	\N	2012-11-26	1	\N
00000053	04	00000003	\N	D	\N	2012-11-27	1	\N
00000053	04	00000003	\N	D	\N	2012-11-28	1	\N
00000053	04	00000003	\N	D	\N	2012-11-29	1	\N
00000053	04	00000003	\N	F	\N	2012-11-30	1	\N
00000054	04	00000003	\N	D	\N	2012-11-01	1	\N
00000054	04	00000003	\N	F	\N	2012-11-02	1	\N
00000054	04	00000003	\N	F	\N	2012-11-03	1	\N
00000054	04	00000003	\N	D	\N	2012-11-04	1	\N
00000054	04	00000003	\N	D	\N	2012-11-05	1	\N
00000054	04	00000003	\N	D	\N	2012-11-06	1	\N
00000054	04	00000003	\N	D	\N	2012-11-07	1	\N
00000054	04	00000003	\N	D	\N	2012-11-08	1	\N
00000054	04	00000003	\N	F	\N	2012-11-09	1	\N
00000054	04	00000003	\N	F	\N	2012-11-10	1	\N
00000054	04	00000003	\N	D	\N	2012-11-11	1	\N
00000054	04	00000003	\N	D	\N	2012-11-12	1	\N
00000054	04	00000003	\N	D	\N	2012-11-13	1	\N
00000054	04	00000003	\N	D	\N	2012-11-14	1	\N
00000054	04	00000003	\N	D	\N	2012-11-15	1	\N
00000054	04	00000003	\N	F	\N	2012-11-16	1	\N
00000054	04	00000003	\N	F	\N	2012-11-17	1	\N
00000054	04	00000003	\N	D	\N	2012-11-18	1	\N
00000054	04	00000003	\N	D	\N	2012-11-19	1	\N
00000054	04	00000003	\N	D	\N	2012-11-20	1	\N
00000054	04	00000003	\N	D	\N	2012-11-21	1	\N
00000054	04	00000003	\N	D	\N	2012-11-22	1	\N
00000054	04	00000003	\N	F	\N	2012-11-23	1	\N
00000054	04	00000003	\N	F	\N	2012-11-24	1	\N
00000054	04	00000003	\N	D	\N	2012-11-25	1	\N
00000054	04	00000003	\N	D	\N	2012-11-26	1	\N
00000054	04	00000003	\N	D	\N	2012-11-27	1	\N
00000054	04	00000003	\N	D	\N	2012-11-28	1	\N
00000054	04	00000003	\N	D	\N	2012-11-29	1	\N
00000054	04	00000003	\N	F	\N	2012-11-30	1	\N
00000055	04	00000003	\N	D	\N	2012-11-01	1	\N
00000055	04	00000003	\N	F	\N	2012-11-02	1	\N
00000055	04	00000003	\N	F	\N	2012-11-03	1	\N
00000055	04	00000003	\N	D	\N	2012-11-04	1	\N
00000055	04	00000003	\N	D	\N	2012-11-05	1	\N
00000055	04	00000003	\N	D	\N	2012-11-06	1	\N
00000055	04	00000003	\N	D	\N	2012-11-07	1	\N
00000055	04	00000003	\N	D	\N	2012-11-08	1	\N
00000055	04	00000003	\N	F	\N	2012-11-09	1	\N
00000055	04	00000003	\N	F	\N	2012-11-10	1	\N
00000055	04	00000003	\N	D	\N	2012-11-11	1	\N
00000055	04	00000003	\N	D	\N	2012-11-12	1	\N
00000055	04	00000003	\N	D	\N	2012-11-13	1	\N
00000055	04	00000003	\N	D	\N	2012-11-14	1	\N
00000055	04	00000003	\N	D	\N	2012-11-15	1	\N
00000055	04	00000003	\N	F	\N	2012-11-16	1	\N
00000055	04	00000003	\N	F	\N	2012-11-17	1	\N
00000055	04	00000003	\N	D	\N	2012-11-18	1	\N
00000055	04	00000003	\N	D	\N	2012-11-19	1	\N
00000055	04	00000003	\N	D	\N	2012-11-20	1	\N
00000055	04	00000003	\N	D	\N	2012-11-21	1	\N
00000055	04	00000003	\N	D	\N	2012-11-22	1	\N
00000055	04	00000003	\N	F	\N	2012-11-23	1	\N
00000055	04	00000003	\N	F	\N	2012-11-24	1	\N
00000055	04	00000003	\N	D	\N	2012-11-25	1	\N
00000055	04	00000003	\N	D	\N	2012-11-26	1	\N
00000055	04	00000003	\N	D	\N	2012-11-27	1	\N
00000055	04	00000003	\N	D	\N	2012-11-28	1	\N
00000055	04	00000003	\N	D	\N	2012-11-29	1	\N
00000055	04	00000003	\N	F	\N	2012-11-30	1	\N
00000056	04	00000003	\N	D	\N	2012-11-01	1	\N
00000056	04	00000003	\N	F	\N	2012-11-02	1	\N
00000056	04	00000003	\N	F	\N	2012-11-03	1	\N
00000056	04	00000003	\N	D	\N	2012-11-04	1	\N
00000056	04	00000003	\N	D	\N	2012-11-05	1	\N
00000056	04	00000003	\N	D	\N	2012-11-06	1	\N
00000056	04	00000003	\N	D	\N	2012-11-07	1	\N
00000056	04	00000003	\N	D	\N	2012-11-08	1	\N
00000056	04	00000003	\N	F	\N	2012-11-09	1	\N
00000056	04	00000003	\N	F	\N	2012-11-10	1	\N
00000056	04	00000003	\N	D	\N	2012-11-11	1	\N
00000056	04	00000003	\N	D	\N	2012-11-12	1	\N
00000056	04	00000003	\N	D	\N	2012-11-13	1	\N
00000056	04	00000003	\N	D	\N	2012-11-14	1	\N
00000056	04	00000003	\N	D	\N	2012-11-15	1	\N
00000056	04	00000003	\N	F	\N	2012-11-16	1	\N
00000056	04	00000003	\N	F	\N	2012-11-17	1	\N
00000056	04	00000003	\N	D	\N	2012-11-18	1	\N
00000056	04	00000003	\N	D	\N	2012-11-19	1	\N
00000056	04	00000003	\N	D	\N	2012-11-20	1	\N
00000056	04	00000003	\N	D	\N	2012-11-21	1	\N
00000056	04	00000003	\N	D	\N	2012-11-22	1	\N
00000056	04	00000003	\N	F	\N	2012-11-23	1	\N
00000056	04	00000003	\N	F	\N	2012-11-24	1	\N
00000056	04	00000003	\N	D	\N	2012-11-25	1	\N
00000056	04	00000003	\N	D	\N	2012-11-26	1	\N
00000056	04	00000003	\N	D	\N	2012-11-27	1	\N
00000056	04	00000003	\N	D	\N	2012-11-28	1	\N
00000056	04	00000003	\N	D	\N	2012-11-29	1	\N
00000056	04	00000003	\N	F	\N	2012-11-30	1	\N
00000057	04	00000003	\N	D	\N	2012-11-01	1	\N
00000057	04	00000003	\N	F	\N	2012-11-02	1	\N
00000057	04	00000003	\N	F	\N	2012-11-03	1	\N
00000057	04	00000003	\N	D	\N	2012-11-04	1	\N
00000057	04	00000003	\N	D	\N	2012-11-05	1	\N
00000057	04	00000003	\N	D	\N	2012-11-06	1	\N
00000057	04	00000003	\N	D	\N	2012-11-07	1	\N
00000057	04	00000003	\N	D	\N	2012-11-08	1	\N
00000057	04	00000003	\N	F	\N	2012-11-09	1	\N
00000057	04	00000003	\N	F	\N	2012-11-10	1	\N
00000057	04	00000003	\N	D	\N	2012-11-11	1	\N
00000057	04	00000003	\N	D	\N	2012-11-12	1	\N
00000057	04	00000003	\N	D	\N	2012-11-13	1	\N
00000057	04	00000003	\N	D	\N	2012-11-14	1	\N
00000057	04	00000003	\N	D	\N	2012-11-15	1	\N
00000057	04	00000003	\N	F	\N	2012-11-16	1	\N
00000057	04	00000003	\N	F	\N	2012-11-17	1	\N
00000057	04	00000003	\N	D	\N	2012-11-18	1	\N
00000057	04	00000003	\N	D	\N	2012-11-19	1	\N
00000057	04	00000003	\N	D	\N	2012-11-20	1	\N
00000057	04	00000003	\N	D	\N	2012-11-21	1	\N
00000057	04	00000003	\N	D	\N	2012-11-22	1	\N
00000057	04	00000003	\N	F	\N	2012-11-23	1	\N
00000057	04	00000003	\N	F	\N	2012-11-24	1	\N
00000057	04	00000003	\N	D	\N	2012-11-25	1	\N
00000057	04	00000003	\N	D	\N	2012-11-26	1	\N
00000057	04	00000003	\N	D	\N	2012-11-27	1	\N
00000057	04	00000003	\N	D	\N	2012-11-28	1	\N
00000057	04	00000003	\N	D	\N	2012-11-29	1	\N
00000057	04	00000003	\N	F	\N	2012-11-30	1	\N
00000053	04	00000003	\N	F	\N	2012-12-01	1	\N
00000053	04	00000003	\N	D	\N	2012-12-02	1	\N
00000053	04	00000003	\N	D	\N	2012-12-03	1	\N
00000053	04	00000003	\N	D	\N	2012-12-04	1	\N
00000053	04	00000003	\N	D	\N	2012-12-05	1	\N
00000053	04	00000003	\N	D	\N	2012-12-06	1	\N
00000053	04	00000003	\N	F	\N	2012-12-07	1	\N
00000053	04	00000003	\N	F	\N	2012-12-08	1	\N
00000053	04	00000003	\N	D	\N	2012-12-09	1	\N
00000053	04	00000003	\N	D	\N	2012-12-10	1	\N
00000053	04	00000003	\N	D	\N	2012-12-11	1	\N
00000053	04	00000003	\N	D	\N	2012-12-13	1	\N
00000053	04	00000003	\N	F	\N	2012-12-14	1	\N
00000053	04	00000003	\N	F	\N	2012-12-15	1	\N
00000053	04	00000003	\N	D	\N	2012-12-16	1	\N
00000053	04	00000003	\N	D	\N	2012-12-17	1	\N
00000053	04	00000003	\N	D	\N	2012-12-18	1	\N
00000053	04	00000003	\N	D	\N	2012-12-19	1	\N
00000053	04	00000003	\N	D	\N	2012-12-20	1	\N
00000053	04	00000003	\N	F	\N	2012-12-21	1	\N
00000053	04	00000003	\N	F	\N	2012-12-22	1	\N
00000053	04	00000003	\N	D	\N	2012-12-23	1	\N
00000053	04	00000003	\N	D	\N	2012-12-24	1	\N
00000053	04	00000003	\N	D	\N	2012-12-25	1	\N
00000053	04	00000003	\N	D	\N	2012-12-26	1	\N
00000053	04	00000003	\N	D	\N	2012-12-27	1	\N
00000053	04	00000003	\N	F	\N	2012-12-28	1	\N
00000053	04	00000003	\N	F	\N	2012-12-29	1	\N
00000053	04	00000003	\N	D	\N	2012-12-30	1	\N
00000053	04	00000003	\N	D	\N	2012-12-31	1	\N
00000054	04	00000003	\N	F	\N	2012-12-01	1	\N
00000054	04	00000003	\N	D	\N	2012-12-02	1	\N
00000054	04	00000003	\N	D	\N	2012-12-03	1	\N
00000054	04	00000003	\N	D	\N	2012-12-04	1	\N
00000054	04	00000003	\N	D	\N	2012-12-05	1	\N
00000054	04	00000003	\N	D	\N	2012-12-06	1	\N
00000054	04	00000003	\N	F	\N	2012-12-07	1	\N
00000054	04	00000003	\N	F	\N	2012-12-08	1	\N
00000054	04	00000003	\N	D	\N	2012-12-09	1	\N
00000054	04	00000003	\N	D	\N	2012-12-10	1	\N
00000054	04	00000003	\N	D	\N	2012-12-11	1	\N
00000054	04	00000003	\N	D	\N	2012-12-13	1	\N
00000054	04	00000003	\N	F	\N	2012-12-14	1	\N
00000054	04	00000003	\N	F	\N	2012-12-15	1	\N
00000054	04	00000003	\N	D	\N	2012-12-16	1	\N
00000054	04	00000003	\N	D	\N	2012-12-17	1	\N
00000054	04	00000003	\N	D	\N	2012-12-18	1	\N
00000054	04	00000003	\N	D	\N	2012-12-19	1	\N
00000054	04	00000003	\N	D	\N	2012-12-20	1	\N
00000054	04	00000003	\N	F	\N	2012-12-21	1	\N
00000054	04	00000003	\N	F	\N	2012-12-22	1	\N
00000054	04	00000003	\N	D	\N	2012-12-23	1	\N
00000054	04	00000003	\N	D	\N	2012-12-24	1	\N
00000054	04	00000003	\N	D	\N	2012-12-25	1	\N
00000054	04	00000003	\N	D	\N	2012-12-26	1	\N
00000054	04	00000003	\N	D	\N	2012-12-27	1	\N
00000054	04	00000003	\N	F	\N	2012-12-28	1	\N
00000054	04	00000003	\N	F	\N	2012-12-29	1	\N
00000054	04	00000003	\N	D	\N	2012-12-30	1	\N
00000054	04	00000003	\N	D	\N	2012-12-31	1	\N
00000055	04	00000003	\N	F	\N	2012-12-01	1	\N
00000055	04	00000003	\N	D	\N	2012-12-02	1	\N
00000055	04	00000003	\N	D	\N	2012-12-03	1	\N
00000055	04	00000003	\N	D	\N	2012-12-04	1	\N
00000055	04	00000003	\N	D	\N	2012-12-05	1	\N
00000055	04	00000003	\N	D	\N	2012-12-06	1	\N
00000055	04	00000003	\N	F	\N	2012-12-07	1	\N
00000055	04	00000003	\N	F	\N	2012-12-08	1	\N
00000055	04	00000003	\N	D	\N	2012-12-09	1	\N
00000055	04	00000003	\N	D	\N	2012-12-10	1	\N
00000055	04	00000003	\N	D	\N	2012-12-11	1	\N
00000055	04	00000003	\N	D	\N	2012-12-12	1	\N
00000055	04	00000003	\N	D	\N	2012-12-13	1	\N
00000055	04	00000003	\N	F	\N	2012-12-14	1	\N
00000055	04	00000003	\N	F	\N	2012-12-15	1	\N
00000055	04	00000003	\N	D	\N	2012-12-16	1	\N
00000055	04	00000003	\N	D	\N	2012-12-17	1	\N
00000055	04	00000003	\N	D	\N	2012-12-18	1	\N
00000055	04	00000003	\N	D	\N	2012-12-19	1	\N
00000055	04	00000003	\N	D	\N	2012-12-20	1	\N
00000055	04	00000003	\N	F	\N	2012-12-21	1	\N
00000055	04	00000003	\N	F	\N	2012-12-22	1	\N
00000055	04	00000003	\N	D	\N	2012-12-23	1	\N
00000055	04	00000003	\N	D	\N	2012-12-24	1	\N
00000055	04	00000003	\N	D	\N	2012-12-25	1	\N
00000055	04	00000003	\N	D	\N	2012-12-26	1	\N
00000055	04	00000003	\N	D	\N	2012-12-27	1	\N
00000055	04	00000003	\N	F	\N	2012-12-28	1	\N
00000055	04	00000003	\N	F	\N	2012-12-29	1	\N
00000055	04	00000003	\N	D	\N	2012-12-30	1	\N
00000055	04	00000003	\N	D	\N	2012-12-31	1	\N
00000056	04	00000003	\N	F	\N	2012-12-01	1	\N
00000056	04	00000003	\N	D	\N	2012-12-02	1	\N
00000056	04	00000003	\N	D	\N	2012-12-03	1	\N
00000056	04	00000003	\N	D	\N	2012-12-04	1	\N
00000056	04	00000003	\N	D	\N	2012-12-05	1	\N
00000056	04	00000003	\N	D	\N	2012-12-06	1	\N
00000056	04	00000003	\N	F	\N	2012-12-07	1	\N
00000056	04	00000003	\N	F	\N	2012-12-08	1	\N
00000056	04	00000003	\N	D	\N	2012-12-09	1	\N
00000056	04	00000003	\N	D	\N	2012-12-10	1	\N
00000056	04	00000003	\N	D	\N	2012-12-11	1	\N
00000056	04	00000003	\N	D	\N	2012-12-12	1	\N
00000056	04	00000003	\N	D	\N	2012-12-13	1	\N
00000056	04	00000003	\N	F	\N	2012-12-14	1	\N
00000056	04	00000003	\N	F	\N	2012-12-15	1	\N
00000056	04	00000003	\N	D	\N	2012-12-16	1	\N
00000054	04	00000003	\N	D	0000000034	2012-12-12	R	\N
00000056	04	00000003	\N	D	\N	2012-12-17	1	\N
00000056	04	00000003	\N	D	\N	2012-12-18	1	\N
00000056	04	00000003	\N	D	\N	2012-12-19	1	\N
00000056	04	00000003	\N	D	\N	2012-12-20	1	\N
00000056	04	00000003	\N	F	\N	2012-12-21	1	\N
00000056	04	00000003	\N	F	\N	2012-12-22	1	\N
00000056	04	00000003	\N	D	\N	2012-12-23	1	\N
00000056	04	00000003	\N	D	\N	2012-12-24	1	\N
00000056	04	00000003	\N	D	\N	2012-12-25	1	\N
00000056	04	00000003	\N	D	\N	2012-12-26	1	\N
00000056	04	00000003	\N	D	\N	2012-12-27	1	\N
00000056	04	00000003	\N	F	\N	2012-12-28	1	\N
00000056	04	00000003	\N	F	\N	2012-12-29	1	\N
00000056	04	00000003	\N	D	\N	2012-12-30	1	\N
00000056	04	00000003	\N	D	\N	2012-12-31	1	\N
00000057	04	00000003	\N	F	\N	2012-12-01	1	\N
00000057	04	00000003	\N	D	\N	2012-12-02	1	\N
00000057	04	00000003	\N	D	\N	2012-12-03	1	\N
00000057	04	00000003	\N	D	\N	2012-12-04	1	\N
00000057	04	00000003	\N	D	\N	2012-12-05	1	\N
00000057	04	00000003	\N	D	\N	2012-12-06	1	\N
00000057	04	00000003	\N	F	\N	2012-12-07	1	\N
00000057	04	00000003	\N	F	\N	2012-12-08	1	\N
00000057	04	00000003	\N	D	\N	2012-12-09	1	\N
00000057	04	00000003	\N	D	\N	2012-12-10	1	\N
00000057	04	00000003	\N	D	\N	2012-12-11	1	\N
00000057	04	00000003	\N	D	\N	2012-12-12	1	\N
00000057	04	00000003	\N	D	\N	2012-12-13	1	\N
00000057	04	00000003	\N	F	\N	2012-12-14	1	\N
00000057	04	00000003	\N	F	\N	2012-12-15	1	\N
00000057	04	00000003	\N	D	\N	2012-12-16	1	\N
00000057	04	00000003	\N	D	\N	2012-12-17	1	\N
00000057	04	00000003	\N	D	\N	2012-12-18	1	\N
00000057	04	00000003	\N	D	\N	2012-12-19	1	\N
00000057	04	00000003	\N	D	\N	2012-12-20	1	\N
00000057	04	00000003	\N	F	\N	2012-12-21	1	\N
00000057	04	00000003	\N	F	\N	2012-12-22	1	\N
00000057	04	00000003	\N	D	\N	2012-12-23	1	\N
00000057	04	00000003	\N	D	\N	2012-12-24	1	\N
00000057	04	00000003	\N	D	\N	2012-12-25	1	\N
00000057	04	00000003	\N	D	\N	2012-12-26	1	\N
00000057	04	00000003	\N	D	\N	2012-12-27	1	\N
00000057	04	00000003	\N	F	\N	2012-12-28	1	\N
00000057	04	00000003	\N	F	\N	2012-12-29	1	\N
00000057	04	00000003	\N	D	\N	2012-12-30	1	\N
00000057	04	00000003	\N	D	\N	2012-12-31	1	\N
00000052	03	00000003	\N	D	\N	2012-11-01	1	\N
00000052	03	00000003	\N	F	\N	2012-11-02	1	\N
00000052	03	00000003	\N	F	\N	2012-11-03	1	\N
00000052	03	00000003	\N	D	\N	2012-11-04	1	\N
00000052	03	00000003	\N	D	\N	2012-11-05	1	\N
00000052	03	00000003	\N	D	\N	2012-11-06	1	\N
00000052	03	00000003	\N	D	\N	2012-11-07	1	\N
00000052	03	00000003	\N	D	\N	2012-11-08	1	\N
00000052	03	00000003	\N	F	\N	2012-11-09	1	\N
00000052	03	00000003	\N	F	\N	2012-11-10	1	\N
00000052	03	00000003	\N	D	\N	2012-11-11	1	\N
00000052	03	00000003	\N	D	\N	2012-11-12	1	\N
00000052	03	00000003	\N	D	\N	2012-11-13	1	\N
00000052	03	00000003	\N	D	\N	2012-11-14	1	\N
00000052	03	00000003	\N	D	\N	2012-11-15	1	\N
00000052	03	00000003	\N	F	\N	2012-11-16	1	\N
00000052	03	00000003	\N	F	\N	2012-11-17	1	\N
00000052	03	00000003	\N	D	\N	2012-11-18	1	\N
00000052	03	00000003	\N	D	\N	2012-11-19	1	\N
00000052	03	00000003	\N	D	\N	2012-11-20	1	\N
00000052	03	00000003	\N	D	\N	2012-11-21	1	\N
00000052	03	00000003	\N	D	\N	2012-11-22	1	\N
00000052	03	00000003	\N	F	\N	2012-11-23	1	\N
00000052	03	00000003	\N	F	\N	2012-11-24	1	\N
00000052	03	00000003	\N	D	\N	2012-11-25	1	\N
00000052	03	00000003	\N	D	\N	2012-11-26	1	\N
00000052	03	00000003	\N	D	\N	2012-11-27	1	\N
00000052	03	00000003	\N	D	\N	2012-11-28	1	\N
00000052	03	00000003	\N	D	\N	2012-11-29	1	\N
00000052	03	00000003	\N	F	\N	2012-11-30	1	\N
00000052	03	00000003	\N	D	\N	2012-12-05	1	\N
00000052	03	00000003	\N	D	\N	2012-12-06	1	\N
00000052	03	00000003	\N	F	\N	2012-12-07	1	\N
00000052	03	00000003	\N	F	\N	2012-12-08	1	\N
00000052	03	00000003	\N	D	\N	2012-12-09	1	\N
00000052	03	00000003	\N	D	\N	2012-12-10	1	\N
00000052	03	00000003	\N	D	\N	2012-12-11	1	\N
00000052	03	00000003	\N	D	\N	2012-12-12	1	\N
00000052	03	00000003	\N	D	\N	2012-12-13	1	\N
00000052	03	00000003	\N	F	\N	2012-12-14	1	\N
00000052	03	00000003	\N	F	\N	2012-12-15	1	\N
00000052	03	00000003	\N	D	\N	2012-12-16	1	\N
00000052	03	00000003	\N	D	\N	2012-12-17	1	\N
00000052	03	00000003	\N	D	\N	2012-12-18	1	\N
00000052	03	00000003	\N	D	\N	2012-12-19	1	\N
00000052	03	00000003	\N	D	\N	2012-12-20	1	\N
00000052	03	00000003	\N	F	\N	2012-12-21	1	\N
00000052	03	00000003	\N	F	\N	2012-12-22	1	\N
00000052	03	00000003	\N	D	\N	2012-12-23	1	\N
00000052	03	00000003	\N	D	\N	2012-12-24	1	\N
00000052	03	00000003	\N	D	\N	2012-12-25	1	\N
00000052	03	00000003	\N	D	\N	2012-12-26	1	\N
00000052	03	00000003	\N	D	\N	2012-12-27	1	\N
00000052	03	00000003	\N	F	\N	2012-12-28	1	\N
00000052	03	00000003	\N	F	\N	2012-12-29	1	\N
00000052	03	00000003	\N	D	\N	2012-12-30	1	\N
00000052	03	00000003	\N	D	\N	2012-12-31	1	\N
00000060	06	00000003	\N	D	\N	2012-11-01	1	\N
00000060	06	00000003	\N	F	\N	2012-11-02	1	\N
00000060	06	00000003	\N	F	\N	2012-11-03	1	\N
00000060	06	00000003	\N	D	\N	2012-11-04	1	\N
00000060	06	00000003	\N	D	\N	2012-11-05	1	\N
00000060	06	00000003	\N	D	\N	2012-11-06	1	\N
00000060	06	00000003	\N	D	\N	2012-11-07	1	\N
00000060	06	00000003	\N	D	\N	2012-11-08	1	\N
00000060	06	00000003	\N	F	\N	2012-11-09	1	\N
00000060	06	00000003	\N	F	\N	2012-11-10	1	\N
00000060	06	00000003	\N	D	\N	2012-11-11	1	\N
00000060	06	00000003	\N	D	\N	2012-11-12	1	\N
00000060	06	00000003	\N	D	\N	2012-11-13	1	\N
00000060	06	00000003	\N	D	\N	2012-11-14	1	\N
00000060	06	00000003	\N	D	\N	2012-11-15	1	\N
00000060	06	00000003	\N	F	\N	2012-11-16	1	\N
00000060	06	00000003	\N	F	\N	2012-11-17	1	\N
00000060	06	00000003	\N	D	\N	2012-11-18	1	\N
00000060	06	00000003	\N	D	\N	2012-11-19	1	\N
00000060	06	00000003	\N	D	\N	2012-11-20	1	\N
00000060	06	00000003	\N	D	\N	2012-11-21	1	\N
00000060	06	00000003	\N	D	\N	2012-11-22	1	\N
00000060	06	00000003	\N	F	\N	2012-11-23	1	\N
00000060	06	00000003	\N	F	\N	2012-11-24	1	\N
00000060	06	00000003	\N	D	\N	2012-11-25	1	\N
00000060	06	00000003	\N	D	\N	2012-11-26	1	\N
00000060	06	00000003	\N	D	\N	2012-11-27	1	\N
00000060	06	00000003	\N	D	\N	2012-11-28	1	\N
00000060	06	00000003	\N	D	\N	2012-11-29	1	\N
00000060	06	00000003	\N	F	\N	2012-11-30	1	\N
00000060	06	00000003	\N	F	\N	2012-12-01	1	\N
00000060	06	00000003	\N	D	\N	2012-12-02	1	\N
00000060	06	00000003	\N	D	\N	2012-12-03	1	\N
00000060	06	00000003	\N	D	\N	2012-12-04	1	\N
00000060	06	00000003	\N	D	\N	2012-12-05	1	\N
00000060	06	00000003	\N	D	\N	2012-12-06	1	\N
00000060	06	00000003	\N	F	\N	2012-12-07	1	\N
00000060	06	00000003	\N	F	\N	2012-12-08	1	\N
00000060	06	00000003	\N	D	\N	2012-12-09	1	\N
00000060	06	00000003	\N	D	\N	2012-12-10	1	\N
00000060	06	00000003	\N	D	\N	2012-12-11	1	\N
00000060	06	00000003	\N	D	\N	2012-12-12	1	\N
00000060	06	00000003	\N	D	\N	2012-12-13	1	\N
00000060	06	00000003	\N	F	\N	2012-12-14	1	\N
00000060	06	00000003	\N	F	\N	2012-12-15	1	\N
00000060	06	00000003	\N	D	\N	2012-12-16	1	\N
00000060	06	00000003	\N	D	\N	2012-12-17	1	\N
00000060	06	00000003	\N	D	\N	2012-12-18	1	\N
00000060	06	00000003	\N	D	\N	2012-12-19	1	\N
00000060	06	00000003	\N	D	\N	2012-12-20	1	\N
00000060	06	00000003	\N	F	\N	2012-12-21	1	\N
00000060	06	00000003	\N	F	\N	2012-12-22	1	\N
00000060	06	00000003	\N	D	\N	2012-12-23	1	\N
00000060	06	00000003	\N	D	\N	2012-12-24	1	\N
00000060	06	00000003	\N	D	\N	2012-12-25	1	\N
00000060	06	00000003	\N	D	\N	2012-12-26	1	\N
00000060	06	00000003	\N	D	\N	2012-12-27	1	\N
00000060	06	00000003	\N	F	\N	2012-12-28	1	\N
00000060	06	00000003	\N	F	\N	2012-12-29	1	\N
00000060	06	00000003	\N	D	\N	2012-12-30	1	\N
00000060	06	00000003	\N	D	\N	2012-12-31	1	\N
00000042	01	00000003	\N	D	\N	2012-11-01	1	\N
00000042	01	00000003	\N	F	\N	2012-11-02	1	\N
00000042	01	00000003	\N	F	\N	2012-11-03	1	\N
00000042	01	00000003	\N	D	\N	2012-11-04	1	\N
00000042	01	00000003	\N	D	\N	2012-11-05	1	\N
00000042	01	00000003	\N	D	\N	2012-11-06	1	\N
00000042	01	00000003	\N	D	\N	2012-11-07	1	\N
00000042	01	00000003	\N	D	\N	2012-11-08	1	\N
00000042	01	00000003	\N	F	\N	2012-11-09	1	\N
00000042	01	00000003	\N	F	\N	2012-11-10	1	\N
00000042	01	00000003	\N	D	\N	2012-11-11	1	\N
00000042	01	00000003	\N	D	\N	2012-11-12	1	\N
00000042	01	00000003	\N	D	\N	2012-11-13	1	\N
00000042	01	00000003	\N	D	\N	2012-11-14	1	\N
00000042	01	00000003	\N	D	\N	2012-11-15	1	\N
00000042	01	00000003	\N	F	\N	2012-11-16	1	\N
00000042	01	00000003	\N	F	\N	2012-11-17	1	\N
00000042	01	00000003	\N	D	\N	2012-11-18	1	\N
00000042	01	00000003	\N	D	\N	2012-11-19	1	\N
00000042	01	00000003	\N	D	\N	2012-11-20	1	\N
00000042	01	00000003	\N	D	\N	2012-11-21	1	\N
00000042	01	00000003	\N	D	\N	2012-11-22	1	\N
00000042	01	00000003	\N	F	\N	2012-11-23	1	\N
00000042	01	00000003	\N	F	\N	2012-11-24	1	\N
00000042	01	00000003	\N	D	\N	2012-11-25	1	\N
00000042	01	00000003	\N	D	\N	2012-11-26	1	\N
00000042	01	00000003	\N	D	\N	2012-11-29	1	\N
00000042	01	00000003	\N	F	\N	2012-11-30	1	\N
00000043	01	00000003	\N	D	\N	2012-11-01	1	\N
00000043	01	00000003	\N	F	\N	2012-11-02	1	\N
00000043	01	00000003	\N	F	\N	2012-11-03	1	\N
00000043	01	00000003	\N	D	\N	2012-11-04	1	\N
00000043	01	00000003	\N	D	\N	2012-11-05	1	\N
00000043	01	00000003	\N	D	\N	2012-11-06	1	\N
00000043	01	00000003	\N	D	\N	2012-11-07	1	\N
00000043	01	00000003	\N	D	\N	2012-11-08	1	\N
00000043	01	00000003	\N	F	\N	2012-11-09	1	\N
00000043	01	00000003	\N	F	\N	2012-11-10	1	\N
00000043	01	00000003	\N	D	\N	2012-11-11	1	\N
00000043	01	00000003	\N	D	\N	2012-11-12	1	\N
00000043	01	00000003	\N	D	\N	2012-11-13	1	\N
00000043	01	00000003	\N	D	\N	2012-11-14	1	\N
00000043	01	00000003	\N	D	\N	2012-11-15	1	\N
00000043	01	00000003	\N	F	\N	2012-11-16	1	\N
00000043	01	00000003	\N	F	\N	2012-11-17	1	\N
00000043	01	00000003	\N	D	\N	2012-11-18	1	\N
00000043	01	00000003	\N	D	\N	2012-11-19	1	\N
00000043	01	00000003	\N	D	\N	2012-11-20	1	\N
00000043	01	00000003	\N	D	\N	2012-11-21	1	\N
00000043	01	00000003	\N	D	\N	2012-11-22	1	\N
00000043	01	00000003	\N	F	\N	2012-11-23	1	\N
00000043	01	00000003	\N	F	\N	2012-11-24	1	\N
00000043	01	00000003	\N	D	\N	2012-11-25	1	\N
00000043	01	00000003	\N	D	\N	2012-11-26	1	\N
00000043	01	00000003	\N	D	\N	2012-11-27	1	\N
00000043	01	00000003	\N	D	\N	2012-11-28	1	\N
00000043	01	00000003	\N	D	\N	2012-11-29	1	\N
00000043	01	00000003	\N	F	\N	2012-11-30	1	\N
00000044	01	00000003	\N	D	\N	2012-11-01	1	\N
00000044	01	00000003	\N	F	\N	2012-11-02	1	\N
00000044	01	00000003	\N	F	\N	2012-11-03	1	\N
00000044	01	00000003	\N	D	\N	2012-11-04	1	\N
00000044	01	00000003	\N	D	\N	2012-11-05	1	\N
00000044	01	00000003	\N	D	\N	2012-11-06	1	\N
00000044	01	00000003	\N	D	\N	2012-11-07	1	\N
00000044	01	00000003	\N	D	\N	2012-11-08	1	\N
00000044	01	00000003	\N	F	\N	2012-11-09	1	\N
00000044	01	00000003	\N	F	\N	2012-11-10	1	\N
00000044	01	00000003	\N	D	\N	2012-11-11	1	\N
00000044	01	00000003	\N	D	\N	2012-11-12	1	\N
00000044	01	00000003	\N	D	\N	2012-11-13	1	\N
00000044	01	00000003	\N	D	\N	2012-11-14	1	\N
00000044	01	00000003	\N	D	\N	2012-11-15	1	\N
00000044	01	00000003	\N	F	\N	2012-11-16	1	\N
00000044	01	00000003	\N	F	\N	2012-11-17	1	\N
00000044	01	00000003	\N	D	\N	2012-11-18	1	\N
00000044	01	00000003	\N	D	\N	2012-11-19	1	\N
00000044	01	00000003	\N	D	\N	2012-11-20	1	\N
00000044	01	00000003	\N	D	\N	2012-11-21	1	\N
00000044	01	00000003	\N	D	\N	2012-11-22	1	\N
00000044	01	00000003	\N	F	\N	2012-11-23	1	\N
00000044	01	00000003	\N	F	\N	2012-11-24	1	\N
00000044	01	00000003	\N	D	\N	2012-11-25	1	\N
00000044	01	00000003	\N	D	\N	2012-11-26	1	\N
00000044	01	00000003	\N	D	\N	2012-11-27	1	\N
00000044	01	00000003	\N	D	\N	2012-11-28	1	\N
00000044	01	00000003	\N	D	\N	2012-11-29	1	\N
00000044	01	00000003	\N	F	\N	2012-11-30	1	\N
00000045	01	00000003	\N	D	\N	2012-11-01	1	\N
00000045	01	00000003	\N	F	\N	2012-11-02	1	\N
00000045	01	00000003	\N	F	\N	2012-11-03	1	\N
00000045	01	00000003	\N	D	\N	2012-11-04	1	\N
00000045	01	00000003	\N	D	\N	2012-11-05	1	\N
00000045	01	00000003	\N	D	\N	2012-11-06	1	\N
00000045	01	00000003	\N	D	\N	2012-11-07	1	\N
00000045	01	00000003	\N	D	\N	2012-11-08	1	\N
00000045	01	00000003	\N	F	\N	2012-11-09	1	\N
00000045	01	00000003	\N	F	\N	2012-11-10	1	\N
00000045	01	00000003	\N	D	\N	2012-11-11	1	\N
00000045	01	00000003	\N	D	\N	2012-11-12	1	\N
00000045	01	00000003	\N	D	\N	2012-11-13	1	\N
00000045	01	00000003	\N	D	\N	2012-11-14	1	\N
00000045	01	00000003	\N	D	\N	2012-11-15	1	\N
00000045	01	00000003	\N	F	\N	2012-11-16	1	\N
00000045	01	00000003	\N	F	\N	2012-11-17	1	\N
00000045	01	00000003	\N	D	\N	2012-11-18	1	\N
00000045	01	00000003	\N	D	\N	2012-11-19	1	\N
00000045	01	00000003	\N	D	\N	2012-11-20	1	\N
00000045	01	00000003	\N	D	\N	2012-11-21	1	\N
00000045	01	00000003	\N	D	\N	2012-11-22	1	\N
00000045	01	00000003	\N	F	\N	2012-11-23	1	\N
00000045	01	00000003	\N	F	\N	2012-11-24	1	\N
00000045	01	00000003	\N	D	\N	2012-11-25	1	\N
00000045	01	00000003	\N	D	\N	2012-11-26	1	\N
00000045	01	00000003	\N	D	\N	2012-11-27	1	\N
00000045	01	00000003	\N	D	\N	2012-11-28	1	\N
00000045	01	00000003	\N	D	\N	2012-11-29	1	\N
00000045	01	00000003	\N	F	\N	2012-11-30	1	\N
00000046	01	00000003	\N	D	\N	2012-11-01	1	\N
00000046	01	00000003	\N	F	\N	2012-11-02	1	\N
00000046	01	00000003	\N	F	\N	2012-11-03	1	\N
00000046	01	00000003	\N	D	\N	2012-11-04	1	\N
00000046	01	00000003	\N	D	\N	2012-11-05	1	\N
00000046	01	00000003	\N	D	\N	2012-11-06	1	\N
00000046	01	00000003	\N	D	\N	2012-11-07	1	\N
00000046	01	00000003	\N	D	\N	2012-11-08	1	\N
00000046	01	00000003	\N	F	\N	2012-11-09	1	\N
00000046	01	00000003	\N	F	\N	2012-11-10	1	\N
00000046	01	00000003	\N	D	\N	2012-11-11	1	\N
00000046	01	00000003	\N	D	\N	2012-11-12	1	\N
00000046	01	00000003	\N	D	\N	2012-11-13	1	\N
00000046	01	00000003	\N	D	\N	2012-11-14	1	\N
00000046	01	00000003	\N	D	\N	2012-11-15	1	\N
00000046	01	00000003	\N	F	\N	2012-11-16	1	\N
00000046	01	00000003	\N	F	\N	2012-11-17	1	\N
00000046	01	00000003	\N	D	\N	2012-11-18	1	\N
00000046	01	00000003	\N	D	\N	2012-11-19	1	\N
00000046	01	00000003	\N	D	\N	2012-11-20	1	\N
00000046	01	00000003	\N	D	\N	2012-11-21	1	\N
00000046	01	00000003	\N	D	\N	2012-11-22	1	\N
00000046	01	00000003	\N	F	\N	2012-11-23	1	\N
00000046	01	00000003	\N	F	\N	2012-11-24	1	\N
00000046	01	00000003	\N	D	\N	2012-11-25	1	\N
00000046	01	00000003	\N	D	\N	2012-11-26	1	\N
00000046	01	00000003	\N	D	\N	2012-11-27	1	\N
00000046	01	00000003	\N	D	\N	2012-11-28	1	\N
00000046	01	00000003	\N	D	\N	2012-11-29	1	\N
00000046	01	00000003	\N	F	\N	2012-11-30	1	\N
00000047	01	00000003	\N	D	\N	2012-11-01	1	\N
00000047	01	00000003	\N	F	\N	2012-11-02	1	\N
00000047	01	00000003	\N	F	\N	2012-11-03	1	\N
00000047	01	00000003	\N	D	\N	2012-11-04	1	\N
00000047	01	00000003	\N	D	\N	2012-11-05	1	\N
00000047	01	00000003	\N	D	\N	2012-11-06	1	\N
00000047	01	00000003	\N	D	\N	2012-11-07	1	\N
00000047	01	00000003	\N	D	\N	2012-11-08	1	\N
00000047	01	00000003	\N	F	\N	2012-11-09	1	\N
00000047	01	00000003	\N	F	\N	2012-11-10	1	\N
00000047	01	00000003	\N	D	\N	2012-11-11	1	\N
00000047	01	00000003	\N	D	\N	2012-11-12	1	\N
00000047	01	00000003	\N	D	\N	2012-11-13	1	\N
00000047	01	00000003	\N	D	\N	2012-11-14	1	\N
00000047	01	00000003	\N	D	\N	2012-11-15	1	\N
00000047	01	00000003	\N	F	\N	2012-11-16	1	\N
00000047	01	00000003	\N	F	\N	2012-11-17	1	\N
00000047	01	00000003	\N	D	\N	2012-11-18	1	\N
00000047	01	00000003	\N	D	\N	2012-11-19	1	\N
00000047	01	00000003	\N	D	\N	2012-11-20	1	\N
00000047	01	00000003	\N	D	\N	2012-11-21	1	\N
00000047	01	00000003	\N	D	\N	2012-11-22	1	\N
00000047	01	00000003	\N	F	\N	2012-11-23	1	\N
00000047	01	00000003	\N	F	\N	2012-11-24	1	\N
00000047	01	00000003	\N	D	\N	2012-11-25	1	\N
00000047	01	00000003	\N	D	\N	2012-11-26	1	\N
00000047	01	00000003	\N	D	\N	2012-11-27	1	\N
00000047	01	00000003	\N	D	\N	2012-11-28	1	\N
00000047	01	00000003	\N	D	\N	2012-11-29	1	\N
00000047	01	00000003	\N	F	\N	2012-11-30	1	\N
00000048	01	00000003	\N	D	\N	2012-11-01	1	\N
00000048	01	00000003	\N	F	\N	2012-11-02	1	\N
00000048	01	00000003	\N	F	\N	2012-11-03	1	\N
00000048	01	00000003	\N	D	\N	2012-11-04	1	\N
00000048	01	00000003	\N	D	\N	2012-11-05	1	\N
00000048	01	00000003	\N	D	\N	2012-11-06	1	\N
00000048	01	00000003	\N	D	\N	2012-11-07	1	\N
00000048	01	00000003	\N	D	\N	2012-11-08	1	\N
00000048	01	00000003	\N	F	\N	2012-11-09	1	\N
00000048	01	00000003	\N	F	\N	2012-11-10	1	\N
00000048	01	00000003	\N	D	\N	2012-11-11	1	\N
00000048	01	00000003	\N	D	\N	2012-11-12	1	\N
00000048	01	00000003	\N	D	\N	2012-11-13	1	\N
00000048	01	00000003	\N	D	\N	2012-11-14	1	\N
00000048	01	00000003	\N	D	\N	2012-11-15	1	\N
00000048	01	00000003	\N	F	\N	2012-11-16	1	\N
00000048	01	00000003	\N	F	\N	2012-11-17	1	\N
00000048	01	00000003	\N	D	\N	2012-11-18	1	\N
00000048	01	00000003	\N	D	\N	2012-11-19	1	\N
00000048	01	00000003	\N	D	\N	2012-11-20	1	\N
00000048	01	00000003	\N	D	\N	2012-11-21	1	\N
00000048	01	00000003	\N	D	\N	2012-11-22	1	\N
00000048	01	00000003	\N	F	\N	2012-11-23	1	\N
00000048	01	00000003	\N	F	\N	2012-11-24	1	\N
00000048	01	00000003	\N	D	\N	2012-11-25	1	\N
00000048	01	00000003	\N	D	\N	2012-11-26	1	\N
00000048	01	00000003	\N	D	\N	2012-11-27	1	\N
00000048	01	00000003	\N	D	\N	2012-11-28	1	\N
00000048	01	00000003	\N	D	\N	2012-11-29	1	\N
00000048	01	00000003	\N	F	\N	2012-11-30	1	\N
00000042	01	00000003	\N	F	\N	2012-12-01	1	\N
00000042	01	00000003	\N	D	\N	2012-12-03	1	\N
00000042	01	00000003	\N	D	\N	2012-12-04	1	\N
00000042	01	00000003	\N	D	\N	2012-12-05	1	\N
00000042	01	00000003	\N	D	\N	2012-12-06	1	\N
00000042	01	00000003	\N	F	\N	2012-12-07	1	\N
00000042	01	00000003	\N	F	\N	2012-12-08	1	\N
00000042	01	00000003	\N	D	\N	2012-12-09	1	\N
00000042	01	00000003	\N	D	\N	2012-12-10	1	\N
00000042	01	00000003	\N	D	\N	2012-12-11	1	\N
00000042	01	00000003	\N	D	\N	2012-12-12	1	\N
00000042	01	00000003	\N	D	\N	2012-12-13	1	\N
00000042	01	00000003	\N	F	\N	2012-12-14	1	\N
00000042	01	00000003	\N	F	\N	2012-12-15	1	\N
00000042	01	00000003	\N	D	\N	2012-12-17	1	\N
00000042	01	00000003	\N	D	\N	2012-12-18	1	\N
00000042	01	00000003	\N	D	\N	2012-12-19	1	\N
00000042	01	00000003	\N	F	\N	2012-12-21	1	\N
00000042	01	00000003	\N	F	\N	2012-12-22	1	\N
00000042	01	00000003	\N	D	\N	2012-12-23	1	\N
00000042	01	00000003	\N	D	\N	2012-12-24	1	\N
00000042	01	00000003	\N	D	\N	2012-12-25	1	\N
00000042	01	00000003	\N	D	\N	2012-12-26	1	\N
00000042	01	00000003	\N	D	\N	2012-12-27	1	\N
00000042	01	00000003	\N	F	\N	2012-12-28	1	\N
00000042	01	00000003	\N	D	\N	2012-12-30	1	\N
00000042	01	00000003	\N	D	\N	2012-12-31	1	\N
00000043	01	00000003	\N	F	\N	2012-12-01	1	\N
00000043	01	00000003	\N	D	\N	2012-12-02	1	\N
00000043	01	00000003	\N	D	\N	2012-12-03	1	\N
00000043	01	00000003	\N	D	\N	2012-12-04	1	\N
00000043	01	00000003	\N	D	\N	2012-12-05	1	\N
00000043	01	00000003	\N	D	\N	2012-12-06	1	\N
00000043	01	00000003	\N	F	\N	2012-12-07	1	\N
00000043	01	00000003	\N	F	\N	2012-12-08	1	\N
00000043	01	00000003	\N	D	\N	2012-12-09	1	\N
00000043	01	00000003	\N	D	\N	2012-12-10	1	\N
00000043	01	00000003	\N	D	\N	2012-12-11	1	\N
00000043	01	00000003	\N	D	\N	2012-12-12	1	\N
00000043	01	00000003	\N	D	\N	2012-12-13	1	\N
00000043	01	00000003	\N	F	\N	2012-12-14	1	\N
00000043	01	00000003	\N	F	\N	2012-12-15	1	\N
00000043	01	00000003	\N	D	\N	2012-12-16	1	\N
00000043	01	00000003	\N	D	\N	2012-12-17	1	\N
00000043	01	00000003	\N	D	\N	2012-12-18	1	\N
00000043	01	00000003	\N	D	\N	2012-12-19	1	\N
00000043	01	00000003	\N	D	\N	2012-12-20	1	\N
00000043	01	00000003	\N	F	\N	2012-12-21	1	\N
00000043	01	00000003	\N	F	\N	2012-12-22	1	\N
00000043	01	00000003	\N	D	\N	2012-12-23	1	\N
00000043	01	00000003	\N	D	\N	2012-12-24	1	\N
00000043	01	00000003	\N	D	\N	2012-12-25	1	\N
00000043	01	00000003	\N	D	\N	2012-12-26	1	\N
00000043	01	00000003	\N	D	\N	2012-12-27	1	\N
00000043	01	00000003	\N	F	\N	2012-12-28	1	\N
00000043	01	00000003	\N	F	\N	2012-12-29	1	\N
00000043	01	00000003	\N	D	\N	2012-12-30	1	\N
00000043	01	00000003	\N	D	\N	2012-12-31	1	\N
00000044	01	00000003	\N	F	\N	2012-12-01	1	\N
00000044	01	00000003	\N	D	\N	2012-12-02	1	\N
00000044	01	00000003	\N	D	\N	2012-12-03	1	\N
00000044	01	00000003	\N	D	\N	2012-12-04	1	\N
00000044	01	00000003	\N	D	\N	2012-12-05	1	\N
00000044	01	00000003	\N	D	\N	2012-12-06	1	\N
00000044	01	00000003	\N	F	\N	2012-12-07	1	\N
00000044	01	00000003	\N	F	\N	2012-12-08	1	\N
00000044	01	00000003	\N	D	\N	2012-12-09	1	\N
00000044	01	00000003	\N	D	\N	2012-12-10	1	\N
00000044	01	00000003	\N	D	\N	2012-12-11	1	\N
00000044	01	00000003	\N	D	\N	2012-12-12	1	\N
00000044	01	00000003	\N	D	\N	2012-12-13	1	\N
00000044	01	00000003	\N	F	\N	2012-12-14	1	\N
00000044	01	00000003	\N	F	\N	2012-12-15	1	\N
00000044	01	00000003	\N	D	\N	2012-12-16	1	\N
00000044	01	00000003	\N	D	\N	2012-12-17	1	\N
00000044	01	00000003	\N	D	\N	2012-12-18	1	\N
00000044	01	00000003	\N	D	\N	2012-12-19	1	\N
00000044	01	00000003	\N	D	\N	2012-12-20	1	\N
00000044	01	00000003	\N	F	\N	2012-12-21	1	\N
00000044	01	00000003	\N	F	\N	2012-12-22	1	\N
00000044	01	00000003	\N	D	\N	2012-12-23	1	\N
00000044	01	00000003	\N	D	\N	2012-12-24	1	\N
00000044	01	00000003	\N	D	\N	2012-12-25	1	\N
00000044	01	00000003	\N	D	\N	2012-12-26	1	\N
00000044	01	00000003	\N	D	\N	2012-12-27	1	\N
00000044	01	00000003	\N	F	\N	2012-12-28	1	\N
00000044	01	00000003	\N	F	\N	2012-12-29	1	\N
00000044	01	00000003	\N	D	\N	2012-12-30	1	\N
00000044	01	00000003	\N	D	\N	2012-12-31	1	\N
00000045	01	00000003	\N	F	\N	2012-12-01	1	\N
00000045	01	00000003	\N	D	\N	2012-12-02	1	\N
00000045	01	00000003	\N	D	\N	2012-12-03	1	\N
00000045	01	00000003	\N	D	\N	2012-12-04	1	\N
00000045	01	00000003	\N	D	\N	2012-12-05	1	\N
00000045	01	00000003	\N	D	\N	2012-12-06	1	\N
00000045	01	00000003	\N	F	\N	2012-12-07	1	\N
00000045	01	00000003	\N	F	\N	2012-12-08	1	\N
00000045	01	00000003	\N	D	\N	2012-12-09	1	\N
00000045	01	00000003	\N	D	\N	2012-12-10	1	\N
00000045	01	00000003	\N	D	\N	2012-12-11	1	\N
00000045	01	00000003	\N	D	\N	2012-12-12	1	\N
00000045	01	00000003	\N	D	\N	2012-12-13	1	\N
00000045	01	00000003	\N	F	\N	2012-12-14	1	\N
00000045	01	00000003	\N	F	\N	2012-12-15	1	\N
00000045	01	00000003	\N	D	\N	2012-12-16	1	\N
00000045	01	00000003	\N	D	\N	2012-12-17	1	\N
00000045	01	00000003	\N	D	\N	2012-12-18	1	\N
00000045	01	00000003	\N	D	\N	2012-12-19	1	\N
00000045	01	00000003	\N	D	\N	2012-12-20	1	\N
00000045	01	00000003	\N	F	\N	2012-12-21	1	\N
00000045	01	00000003	\N	F	\N	2012-12-22	1	\N
00000045	01	00000003	\N	D	\N	2012-12-23	1	\N
00000045	01	00000003	\N	D	\N	2012-12-24	1	\N
00000045	01	00000003	\N	D	\N	2012-12-25	1	\N
00000045	01	00000003	\N	D	\N	2012-12-26	1	\N
00000045	01	00000003	\N	D	\N	2012-12-27	1	\N
00000045	01	00000003	\N	F	\N	2012-12-28	1	\N
00000045	01	00000003	\N	F	\N	2012-12-29	1	\N
00000045	01	00000003	\N	D	\N	2012-12-30	1	\N
00000045	01	00000003	\N	D	\N	2012-12-31	1	\N
00000046	01	00000003	\N	F	\N	2012-12-01	1	\N
00000046	01	00000003	\N	D	\N	2012-12-02	1	\N
00000046	01	00000003	\N	D	\N	2012-12-03	1	\N
00000046	01	00000003	\N	D	\N	2012-12-04	1	\N
00000046	01	00000003	\N	D	\N	2012-12-05	1	\N
00000046	01	00000003	\N	D	\N	2012-12-06	1	\N
00000042	01	00000003	\N	D	0000000024	2012-12-16	R	\N
00000042	01	00000003	\N	D	0000000038	2012-12-20	R	\N
00000046	01	00000003	\N	F	\N	2012-12-07	1	\N
00000046	01	00000003	\N	F	\N	2012-12-08	1	\N
00000046	01	00000003	\N	D	\N	2012-12-09	1	\N
00000046	01	00000003	\N	D	\N	2012-12-10	1	\N
00000046	01	00000003	\N	D	\N	2012-12-11	1	\N
00000046	01	00000003	\N	D	\N	2012-12-12	1	\N
00000046	01	00000003	\N	D	\N	2012-12-13	1	\N
00000046	01	00000003	\N	F	\N	2012-12-14	1	\N
00000046	01	00000003	\N	F	\N	2012-12-15	1	\N
00000046	01	00000003	\N	D	\N	2012-12-16	1	\N
00000046	01	00000003	\N	D	\N	2012-12-17	1	\N
00000046	01	00000003	\N	D	\N	2012-12-18	1	\N
00000046	01	00000003	\N	D	\N	2012-12-19	1	\N
00000046	01	00000003	\N	D	\N	2012-12-20	1	\N
00000046	01	00000003	\N	F	\N	2012-12-21	1	\N
00000046	01	00000003	\N	F	\N	2012-12-22	1	\N
00000046	01	00000003	\N	D	\N	2012-12-23	1	\N
00000046	01	00000003	\N	D	\N	2012-12-24	1	\N
00000046	01	00000003	\N	D	\N	2012-12-25	1	\N
00000046	01	00000003	\N	D	\N	2012-12-26	1	\N
00000046	01	00000003	\N	D	\N	2012-12-27	1	\N
00000046	01	00000003	\N	F	\N	2012-12-28	1	\N
00000046	01	00000003	\N	F	\N	2012-12-29	1	\N
00000046	01	00000003	\N	D	\N	2012-12-30	1	\N
00000046	01	00000003	\N	D	\N	2012-12-31	1	\N
00000047	01	00000003	\N	F	\N	2012-12-01	1	\N
00000047	01	00000003	\N	D	\N	2012-12-02	1	\N
00000047	01	00000003	\N	D	\N	2012-12-03	1	\N
00000047	01	00000003	\N	D	\N	2012-12-04	1	\N
00000047	01	00000003	\N	D	\N	2012-12-05	1	\N
00000047	01	00000003	\N	D	\N	2012-12-06	1	\N
00000047	01	00000003	\N	F	\N	2012-12-07	1	\N
00000047	01	00000003	\N	F	\N	2012-12-08	1	\N
00000047	01	00000003	\N	D	\N	2012-12-09	1	\N
00000047	01	00000003	\N	D	\N	2012-12-10	1	\N
00000047	01	00000003	\N	D	\N	2012-12-11	1	\N
00000047	01	00000003	\N	D	\N	2012-12-12	1	\N
00000047	01	00000003	\N	D	\N	2012-12-13	1	\N
00000047	01	00000003	\N	F	\N	2012-12-14	1	\N
00000047	01	00000003	\N	F	\N	2012-12-15	1	\N
00000047	01	00000003	\N	D	\N	2012-12-16	1	\N
00000047	01	00000003	\N	D	\N	2012-12-17	1	\N
00000047	01	00000003	\N	D	\N	2012-12-18	1	\N
00000047	01	00000003	\N	D	\N	2012-12-19	1	\N
00000047	01	00000003	\N	D	\N	2012-12-20	1	\N
00000047	01	00000003	\N	F	\N	2012-12-21	1	\N
00000047	01	00000003	\N	F	\N	2012-12-22	1	\N
00000047	01	00000003	\N	D	\N	2012-12-23	1	\N
00000047	01	00000003	\N	D	\N	2012-12-24	1	\N
00000047	01	00000003	\N	D	\N	2012-12-25	1	\N
00000047	01	00000003	\N	D	\N	2012-12-26	1	\N
00000047	01	00000003	\N	D	\N	2012-12-27	1	\N
00000047	01	00000003	\N	F	\N	2012-12-28	1	\N
00000047	01	00000003	\N	F	\N	2012-12-29	1	\N
00000047	01	00000003	\N	D	\N	2012-12-30	1	\N
00000047	01	00000003	\N	D	\N	2012-12-31	1	\N
00000048	01	00000003	\N	F	\N	2012-12-01	1	\N
00000048	01	00000003	\N	D	\N	2012-12-02	1	\N
00000048	01	00000003	\N	D	\N	2012-12-03	1	\N
00000048	01	00000003	\N	D	\N	2012-12-04	1	\N
00000048	01	00000003	\N	D	\N	2012-12-05	1	\N
00000048	01	00000003	\N	D	\N	2012-12-06	1	\N
00000048	01	00000003	\N	F	\N	2012-12-07	1	\N
00000048	01	00000003	\N	F	\N	2012-12-08	1	\N
00000048	01	00000003	\N	D	\N	2012-12-09	1	\N
00000048	01	00000003	\N	D	\N	2012-12-10	1	\N
00000048	01	00000003	\N	D	\N	2012-12-11	1	\N
00000048	01	00000003	\N	D	\N	2012-12-12	1	\N
00000048	01	00000003	\N	D	\N	2012-12-13	1	\N
00000048	01	00000003	\N	F	\N	2012-12-14	1	\N
00000048	01	00000003	\N	F	\N	2012-12-15	1	\N
00000048	01	00000003	\N	D	\N	2012-12-16	1	\N
00000048	01	00000003	\N	D	\N	2012-12-17	1	\N
00000048	01	00000003	\N	D	\N	2012-12-18	1	\N
00000048	01	00000003	\N	D	\N	2012-12-19	1	\N
00000048	01	00000003	\N	D	\N	2012-12-20	1	\N
00000048	01	00000003	\N	F	\N	2012-12-21	1	\N
00000048	01	00000003	\N	F	\N	2012-12-22	1	\N
00000048	01	00000003	\N	D	\N	2012-12-23	1	\N
00000048	01	00000003	\N	D	\N	2012-12-24	1	\N
00000048	01	00000003	\N	D	\N	2012-12-25	1	\N
00000048	01	00000003	\N	D	\N	2012-12-26	1	\N
00000048	01	00000003	\N	D	\N	2012-12-27	1	\N
00000048	01	00000003	\N	F	\N	2012-12-28	1	\N
00000048	01	00000003	\N	F	\N	2012-12-29	1	\N
00000048	01	00000003	\N	D	\N	2012-12-30	1	\N
00000048	01	00000003	\N	D	\N	2012-12-31	1	\N
00000049	02	00000003	\N	D	\N	2012-11-01	1	\N
00000049	02	00000003	\N	F	\N	2012-11-02	1	\N
00000049	02	00000003	\N	F	\N	2012-11-03	1	\N
00000049	02	00000003	\N	D	\N	2012-11-04	1	\N
00000049	02	00000003	\N	D	\N	2012-11-05	1	\N
00000049	02	00000003	\N	D	\N	2012-11-06	1	\N
00000049	02	00000003	\N	D	\N	2012-11-07	1	\N
00000049	02	00000003	\N	D	\N	2012-11-08	1	\N
00000049	02	00000003	\N	F	\N	2012-11-09	1	\N
00000049	02	00000003	\N	F	\N	2012-11-10	1	\N
00000049	02	00000003	\N	D	\N	2012-11-11	1	\N
00000049	02	00000003	\N	D	\N	2012-11-12	1	\N
00000049	02	00000003	\N	D	\N	2012-11-13	1	\N
00000049	02	00000003	\N	D	\N	2012-11-14	1	\N
00000049	02	00000003	\N	D	\N	2012-11-15	1	\N
00000049	02	00000003	\N	F	\N	2012-11-16	1	\N
00000049	02	00000003	\N	F	\N	2012-11-17	1	\N
00000049	02	00000003	\N	D	\N	2012-11-18	1	\N
00000049	02	00000003	\N	D	\N	2012-11-19	1	\N
00000049	02	00000003	\N	D	\N	2012-11-20	1	\N
00000049	02	00000003	\N	D	\N	2012-11-21	1	\N
00000049	02	00000003	\N	D	\N	2012-11-22	1	\N
00000049	02	00000003	\N	F	\N	2012-11-23	1	\N
00000049	02	00000003	\N	F	\N	2012-11-24	1	\N
00000049	02	00000003	\N	D	\N	2012-11-25	1	\N
00000049	02	00000003	\N	D	\N	2012-11-26	1	\N
00000049	02	00000003	\N	D	\N	2012-11-27	1	\N
00000049	02	00000003	\N	D	\N	2012-11-28	1	\N
00000049	02	00000003	\N	D	\N	2012-11-29	1	\N
00000049	02	00000003	\N	F	\N	2012-11-30	1	\N
00000050	02	00000003	\N	D	\N	2012-11-01	1	\N
00000050	02	00000003	\N	F	\N	2012-11-02	1	\N
00000050	02	00000003	\N	F	\N	2012-11-03	1	\N
00000050	02	00000003	\N	D	\N	2012-11-04	1	\N
00000050	02	00000003	\N	D	\N	2012-11-05	1	\N
00000050	02	00000003	\N	D	\N	2012-11-06	1	\N
00000050	02	00000003	\N	D	\N	2012-11-07	1	\N
00000050	02	00000003	\N	D	\N	2012-11-08	1	\N
00000050	02	00000003	\N	F	\N	2012-11-09	1	\N
00000050	02	00000003	\N	F	\N	2012-11-10	1	\N
00000050	02	00000003	\N	D	\N	2012-11-11	1	\N
00000050	02	00000003	\N	D	\N	2012-11-12	1	\N
00000050	02	00000003	\N	D	\N	2012-11-13	1	\N
00000050	02	00000003	\N	D	\N	2012-11-14	1	\N
00000050	02	00000003	\N	D	\N	2012-11-15	1	\N
00000050	02	00000003	\N	F	\N	2012-11-16	1	\N
00000050	02	00000003	\N	F	\N	2012-11-17	1	\N
00000050	02	00000003	\N	D	\N	2012-11-18	1	\N
00000050	02	00000003	\N	D	\N	2012-11-19	1	\N
00000050	02	00000003	\N	D	\N	2012-11-20	1	\N
00000050	02	00000003	\N	D	\N	2012-11-21	1	\N
00000050	02	00000003	\N	D	\N	2012-11-22	1	\N
00000050	02	00000003	\N	F	\N	2012-11-23	1	\N
00000050	02	00000003	\N	F	\N	2012-11-24	1	\N
00000050	02	00000003	\N	D	\N	2012-11-25	1	\N
00000050	02	00000003	\N	D	\N	2012-11-26	1	\N
00000050	02	00000003	\N	D	\N	2012-11-27	1	\N
00000050	02	00000003	\N	D	\N	2012-11-28	1	\N
00000050	02	00000003	\N	D	\N	2012-11-29	1	\N
00000050	02	00000003	\N	F	\N	2012-11-30	1	\N
00000051	02	00000003	\N	D	\N	2012-11-01	1	\N
00000051	02	00000003	\N	F	\N	2012-11-02	1	\N
00000051	02	00000003	\N	F	\N	2012-11-03	1	\N
00000051	02	00000003	\N	D	\N	2012-11-04	1	\N
00000051	02	00000003	\N	D	\N	2012-11-05	1	\N
00000051	02	00000003	\N	D	\N	2012-11-06	1	\N
00000051	02	00000003	\N	D	\N	2012-11-07	1	\N
00000051	02	00000003	\N	D	\N	2012-11-08	1	\N
00000051	02	00000003	\N	F	\N	2012-11-09	1	\N
00000051	02	00000003	\N	F	\N	2012-11-10	1	\N
00000051	02	00000003	\N	D	\N	2012-11-11	1	\N
00000051	02	00000003	\N	D	\N	2012-11-12	1	\N
00000051	02	00000003	\N	D	\N	2012-11-13	1	\N
00000051	02	00000003	\N	D	\N	2012-11-14	1	\N
00000051	02	00000003	\N	D	\N	2012-11-15	1	\N
00000051	02	00000003	\N	F	\N	2012-11-16	1	\N
00000051	02	00000003	\N	F	\N	2012-11-17	1	\N
00000051	02	00000003	\N	D	\N	2012-11-18	1	\N
00000051	02	00000003	\N	D	\N	2012-11-19	1	\N
00000051	02	00000003	\N	D	\N	2012-11-20	1	\N
00000051	02	00000003	\N	D	\N	2012-11-21	1	\N
00000051	02	00000003	\N	D	\N	2012-11-22	1	\N
00000051	02	00000003	\N	F	\N	2012-11-23	1	\N
00000051	02	00000003	\N	F	\N	2012-11-24	1	\N
00000051	02	00000003	\N	D	\N	2012-11-25	1	\N
00000051	02	00000003	\N	D	\N	2012-11-26	1	\N
00000051	02	00000003	\N	D	\N	2012-11-27	1	\N
00000051	02	00000003	\N	D	\N	2012-11-28	1	\N
00000051	02	00000003	\N	D	\N	2012-11-29	1	\N
00000051	02	00000003	\N	F	\N	2012-11-30	1	\N
00000049	02	00000003	\N	F	\N	2012-12-01	1	\N
00000049	02	00000003	\N	D	\N	2012-12-02	1	\N
00000049	02	00000003	\N	D	\N	2012-12-03	1	\N
00000049	02	00000003	\N	D	\N	2012-12-04	1	\N
00000049	02	00000003	\N	D	\N	2012-12-05	1	\N
00000049	02	00000003	\N	D	\N	2012-12-06	1	\N
00000049	02	00000003	\N	F	\N	2012-12-07	1	\N
00000049	02	00000003	\N	F	\N	2012-12-08	1	\N
00000049	02	00000003	\N	D	\N	2012-12-09	1	\N
00000049	02	00000003	\N	D	\N	2012-12-10	1	\N
00000049	02	00000003	\N	D	\N	2012-12-11	1	\N
00000049	02	00000003	\N	D	\N	2012-12-12	1	\N
00000049	02	00000003	\N	D	\N	2012-12-13	1	\N
00000049	02	00000003	\N	F	\N	2012-12-14	1	\N
00000049	02	00000003	\N	F	\N	2012-12-15	1	\N
00000049	02	00000003	\N	D	\N	2012-12-16	1	\N
00000049	02	00000003	\N	D	\N	2012-12-17	1	\N
00000049	02	00000003	\N	D	\N	2012-12-18	1	\N
00000049	02	00000003	\N	D	\N	2012-12-19	1	\N
00000049	02	00000003	\N	D	\N	2012-12-20	1	\N
00000049	02	00000003	\N	F	\N	2012-12-21	1	\N
00000049	02	00000003	\N	F	\N	2012-12-22	1	\N
00000049	02	00000003	\N	D	\N	2012-12-23	1	\N
00000049	02	00000003	\N	D	\N	2012-12-24	1	\N
00000049	02	00000003	\N	D	\N	2012-12-25	1	\N
00000049	02	00000003	\N	D	\N	2012-12-26	1	\N
00000049	02	00000003	\N	D	\N	2012-12-27	1	\N
00000049	02	00000003	\N	F	\N	2012-12-28	1	\N
00000049	02	00000003	\N	F	\N	2012-12-29	1	\N
00000049	02	00000003	\N	D	\N	2012-12-30	1	\N
00000049	02	00000003	\N	D	\N	2012-12-31	1	\N
00000050	02	00000003	\N	F	\N	2012-12-01	1	\N
00000050	02	00000003	\N	D	\N	2012-12-02	1	\N
00000050	02	00000003	\N	D	\N	2012-12-03	1	\N
00000050	02	00000003	\N	D	\N	2012-12-04	1	\N
00000050	02	00000003	\N	D	\N	2012-12-05	1	\N
00000050	02	00000003	\N	D	\N	2012-12-06	1	\N
00000050	02	00000003	\N	F	\N	2012-12-07	1	\N
00000050	02	00000003	\N	F	\N	2012-12-08	1	\N
00000050	02	00000003	\N	D	\N	2012-12-09	1	\N
00000050	02	00000003	\N	D	\N	2012-12-10	1	\N
00000050	02	00000003	\N	D	\N	2012-12-11	1	\N
00000050	02	00000003	\N	D	\N	2012-12-12	1	\N
00000050	02	00000003	\N	D	\N	2012-12-13	1	\N
00000050	02	00000003	\N	F	\N	2012-12-14	1	\N
00000050	02	00000003	\N	F	\N	2012-12-15	1	\N
00000050	02	00000003	\N	D	\N	2012-12-16	1	\N
00000050	02	00000003	\N	D	\N	2012-12-17	1	\N
00000050	02	00000003	\N	D	\N	2012-12-18	1	\N
00000050	02	00000003	\N	D	\N	2012-12-19	1	\N
00000050	02	00000003	\N	D	\N	2012-12-20	1	\N
00000050	02	00000003	\N	F	\N	2012-12-21	1	\N
00000050	02	00000003	\N	F	\N	2012-12-22	1	\N
00000050	02	00000003	\N	D	\N	2012-12-23	1	\N
00000050	02	00000003	\N	D	\N	2012-12-24	1	\N
00000050	02	00000003	\N	D	\N	2012-12-25	1	\N
00000050	02	00000003	\N	D	\N	2012-12-26	1	\N
00000050	02	00000003	\N	D	\N	2012-12-27	1	\N
00000050	02	00000003	\N	F	\N	2012-12-28	1	\N
00000050	02	00000003	\N	F	\N	2012-12-29	1	\N
00000050	02	00000003	\N	D	\N	2012-12-30	1	\N
00000050	02	00000003	\N	D	\N	2012-12-31	1	\N
00000051	02	00000003	\N	F	\N	2012-12-01	1	\N
00000051	02	00000003	\N	D	\N	2012-12-02	1	\N
00000051	02	00000003	\N	D	\N	2012-12-03	1	\N
00000051	02	00000003	\N	D	\N	2012-12-04	1	\N
00000051	02	00000003	\N	D	\N	2012-12-05	1	\N
00000051	02	00000003	\N	D	\N	2012-12-06	1	\N
00000051	02	00000003	\N	F	\N	2012-12-07	1	\N
00000051	02	00000003	\N	F	\N	2012-12-08	1	\N
00000051	02	00000003	\N	D	\N	2012-12-09	1	\N
00000051	02	00000003	\N	D	\N	2012-12-10	1	\N
00000051	02	00000003	\N	D	\N	2012-12-11	1	\N
00000051	02	00000003	\N	D	\N	2012-12-12	1	\N
00000051	02	00000003	\N	D	\N	2012-12-13	1	\N
00000051	02	00000003	\N	F	\N	2012-12-14	1	\N
00000051	02	00000003	\N	F	\N	2012-12-15	1	\N
00000051	02	00000003	\N	D	\N	2012-12-16	1	\N
00000051	02	00000003	\N	D	\N	2012-12-17	1	\N
00000051	02	00000003	\N	D	\N	2012-12-18	1	\N
00000051	02	00000003	\N	D	\N	2012-12-19	1	\N
00000051	02	00000003	\N	D	\N	2012-12-20	1	\N
00000051	02	00000003	\N	F	\N	2012-12-21	1	\N
00000051	02	00000003	\N	F	\N	2012-12-22	1	\N
00000051	02	00000003	\N	D	\N	2012-12-23	1	\N
00000051	02	00000003	\N	D	\N	2012-12-24	1	\N
00000051	02	00000003	\N	D	\N	2012-12-25	1	\N
00000051	02	00000003	\N	D	\N	2012-12-26	1	\N
00000051	02	00000003	\N	D	\N	2012-12-27	1	\N
00000051	02	00000003	\N	F	\N	2012-12-28	1	\N
00000051	02	00000003	\N	F	\N	2012-12-29	1	\N
00000051	02	00000003	\N	D	\N	2012-12-30	1	\N
00000051	02	00000003	\N	D	\N	2012-12-31	1	\N
00000005	01	00000002	\N	D	\N	2013-04-18	1	\N
00000005	01	00000002	\N	F	\N	2013-04-19	1	\N
00000005	01	00000002	\N	F	\N	2013-04-20	1	\N
00000005	01	00000002	\N	D	\N	2013-04-21	1	\N
00000005	01	00000002	\N	D	\N	2013-04-22	1	\N
00000005	01	00000002	\N	D	\N	2013-04-23	1	\N
00000005	01	00000002	\N	D	\N	2013-04-24	1	\N
00000005	01	00000002	\N	D	\N	2013-04-25	1	\N
00000005	01	00000002	\N	F	\N	2013-04-26	1	\N
00000005	01	00000002	\N	F	\N	2013-04-27	1	\N
00000005	01	00000002	\N	D	\N	2013-04-28	1	\N
00000005	01	00000002	\N	D	\N	2013-04-29	1	\N
00000005	01	00000002	\N	D	\N	2013-04-30	1	\N
00000006	01	00000002	\N	D	\N	2013-04-01	1	\N
00000006	01	00000002	\N	D	\N	2013-04-02	1	\N
00000006	01	00000002	\N	D	\N	2013-04-03	1	\N
00000006	01	00000002	\N	D	\N	2013-04-04	1	\N
00000006	01	00000002	\N	F	\N	2013-04-05	1	\N
00000006	01	00000002	\N	F	\N	2013-04-06	1	\N
00000006	01	00000002	\N	D	\N	2013-04-07	1	\N
00000006	01	00000002	\N	D	\N	2013-04-08	1	\N
00000006	01	00000002	\N	D	\N	2013-04-09	1	\N
00000006	01	00000002	\N	D	\N	2013-04-10	1	\N
00000006	01	00000002	\N	D	\N	2013-04-11	1	\N
00000006	01	00000002	\N	F	\N	2013-04-12	1	\N
00000006	01	00000002	\N	F	\N	2013-04-13	1	\N
00000006	01	00000002	\N	D	\N	2013-04-14	1	\N
00000006	01	00000002	\N	D	\N	2013-04-15	1	\N
00000006	01	00000002	\N	D	\N	2013-04-16	1	\N
00000006	01	00000002	\N	D	\N	2013-04-17	1	\N
00000006	01	00000002	\N	D	\N	2013-04-18	1	\N
00000006	01	00000002	\N	F	\N	2013-04-19	1	\N
00000006	01	00000002	\N	F	\N	2013-04-20	1	\N
00000006	01	00000002	\N	D	\N	2013-04-21	1	\N
00000006	01	00000002	\N	D	\N	2013-04-22	1	\N
00000006	01	00000002	\N	D	\N	2013-04-23	1	\N
00000006	01	00000002	\N	D	\N	2013-04-24	1	\N
00000006	01	00000002	\N	D	\N	2013-04-25	1	\N
00000006	01	00000002	\N	F	\N	2013-04-26	1	\N
00000006	01	00000002	\N	F	\N	2013-04-27	1	\N
00000006	01	00000002	\N	D	\N	2013-04-28	1	\N
00000006	01	00000002	\N	D	\N	2013-04-29	1	\N
00000006	01	00000002	\N	D	\N	2013-04-30	1	\N
00000007	01	00000002	\N	D	\N	2013-04-01	1	\N
00000007	01	00000002	\N	D	\N	2013-04-02	1	\N
00000007	01	00000002	\N	D	\N	2013-04-03	1	\N
00000007	01	00000002	\N	D	\N	2013-04-04	1	\N
00000007	01	00000002	\N	F	\N	2013-04-05	1	\N
00000007	01	00000002	\N	F	\N	2013-04-06	1	\N
00000007	01	00000002	\N	D	\N	2013-04-07	1	\N
00000007	01	00000002	\N	D	\N	2013-04-08	1	\N
00000007	01	00000002	\N	D	\N	2013-04-09	1	\N
00000007	01	00000002	\N	D	\N	2013-04-10	1	\N
00000007	01	00000002	\N	D	\N	2013-04-11	1	\N
00000007	01	00000002	\N	F	\N	2013-04-12	1	\N
00000007	01	00000002	\N	F	\N	2013-04-13	1	\N
00000007	01	00000002	\N	D	\N	2013-04-14	1	\N
00000007	01	00000002	\N	D	\N	2013-04-15	1	\N
00000007	01	00000002	\N	D	\N	2013-04-16	1	\N
00000007	01	00000002	\N	D	\N	2013-04-17	1	\N
00000007	01	00000002	\N	D	\N	2013-04-18	1	\N
00000007	01	00000002	\N	F	\N	2013-04-19	1	\N
00000007	01	00000002	\N	F	\N	2013-04-20	1	\N
00000007	01	00000002	\N	D	\N	2013-04-21	1	\N
00000007	01	00000002	\N	D	\N	2013-04-22	1	\N
00000007	01	00000002	\N	D	\N	2013-04-23	1	\N
00000007	01	00000002	\N	D	\N	2013-04-24	1	\N
00000007	01	00000002	\N	D	\N	2013-04-25	1	\N
00000007	01	00000002	\N	F	\N	2013-04-26	1	\N
00000007	01	00000002	\N	F	\N	2013-04-27	1	\N
00000007	01	00000002	\N	D	\N	2013-04-28	1	\N
00000007	01	00000002	\N	D	\N	2013-04-29	1	\N
00000007	01	00000002	\N	D	\N	2013-04-30	1	\N
00000076	01	00000002	\N	D	\N	2013-04-01	1	\N
00000076	01	00000002	\N	D	\N	2013-04-02	1	\N
00000076	01	00000002	\N	D	\N	2013-04-03	1	\N
00000076	01	00000002	\N	D	\N	2013-04-04	1	\N
00000076	01	00000002	\N	F	\N	2013-04-05	1	\N
00000076	01	00000002	\N	F	\N	2013-04-06	1	\N
00000076	01	00000002	\N	D	\N	2013-04-07	1	\N
00000076	01	00000002	\N	D	\N	2013-04-08	1	\N
00000076	01	00000002	\N	D	\N	2013-04-09	1	\N
00000076	01	00000002	\N	D	\N	2013-04-10	1	\N
00000076	01	00000002	\N	D	\N	2013-04-11	1	\N
00000076	01	00000002	\N	F	\N	2013-04-12	1	\N
00000076	01	00000002	\N	F	\N	2013-04-13	1	\N
00000076	01	00000002	\N	D	\N	2013-04-14	1	\N
00000076	01	00000002	\N	D	\N	2013-04-15	1	\N
00000076	01	00000002	\N	D	\N	2013-04-16	1	\N
00000076	01	00000002	\N	D	\N	2013-04-17	1	\N
00000076	01	00000002	\N	D	\N	2013-04-18	1	\N
00000076	01	00000002	\N	F	\N	2013-04-19	1	\N
00000076	01	00000002	\N	F	\N	2013-04-20	1	\N
00000076	01	00000002	\N	D	\N	2013-04-21	1	\N
00000076	01	00000002	\N	D	\N	2013-04-22	1	\N
00000076	01	00000002	\N	D	\N	2013-04-23	1	\N
00000076	01	00000002	\N	D	\N	2013-04-24	1	\N
00000076	01	00000002	\N	D	\N	2013-04-25	1	\N
00000076	01	00000002	\N	F	\N	2013-04-26	1	\N
00000076	01	00000002	\N	F	\N	2013-04-27	1	\N
00000076	01	00000002	\N	D	\N	2013-04-28	1	\N
00000076	01	00000002	\N	D	\N	2013-04-29	1	\N
00000076	01	00000002	\N	D	\N	2013-04-30	1	\N
00000012	03	00000002	\N	D	\N	2013-01-07	1	\N
00000012	03	00000002	\N	D	\N	2013-01-08	1	\N
00000012	03	00000002	\N	D	\N	2013-01-09	1	\N
00000012	03	00000002	\N	D	\N	2013-01-10	1	\N
00000012	03	00000002	\N	F	\N	2013-01-11	1	\N
00000012	03	00000002	\N	F	\N	2013-01-12	1	\N
00000012	03	00000002	\N	D	\N	2013-01-13	1	\N
00000012	03	00000002	\N	D	\N	2013-01-14	1	\N
00000001	01	00000002	\N	D	\N	2012-11-07	1	\N
00000002	01	00000002	\N	D	\N	2012-11-07	1	\N
00000003	01	00000002	\N	D	\N	2012-11-07	1	\N
00000004	01	00000002	\N	D	\N	2012-11-07	1	\N
00000005	01	00000002	\N	D	\N	2012-11-07	1	\N
00000006	01	00000002	\N	D	\N	2012-11-07	1	\N
00000007	01	00000002	\N	D	\N	2012-11-07	1	\N
00000010	03	00000002	\N	D	\N	2013-01-01	1	\N
00000010	03	00000002	\N	D	\N	2013-01-02	1	\N
00000010	03	00000002	\N	D	\N	2013-01-03	1	\N
00000010	03	00000002	\N	F	\N	2013-01-04	1	\N
00000010	03	00000002	\N	F	\N	2013-01-05	1	\N
00000010	03	00000002	\N	D	\N	2013-01-06	1	\N
00000010	03	00000002	\N	D	\N	2013-01-07	1	\N
00000010	03	00000002	\N	D	\N	2013-01-09	1	\N
00000010	03	00000002	\N	D	\N	2013-01-10	1	\N
00000010	03	00000002	\N	F	\N	2013-01-11	1	\N
00000010	03	00000002	\N	F	\N	2013-01-12	1	\N
00000010	03	00000002	\N	D	\N	2013-01-13	1	\N
00000010	03	00000002	\N	D	\N	2013-01-14	1	\N
00000010	03	00000002	\N	D	\N	2013-01-15	1	\N
00000010	03	00000002	\N	D	\N	2013-01-16	1	\N
00000010	03	00000002	\N	D	\N	2013-01-17	1	\N
00000010	03	00000002	\N	F	\N	2013-01-18	1	\N
00000010	03	00000002	\N	F	\N	2013-01-19	1	\N
00000010	03	00000002	\N	D	\N	2013-01-20	1	\N
00000010	03	00000002	\N	D	\N	2013-01-21	1	\N
00000010	03	00000002	\N	D	\N	2013-01-22	1	\N
00000010	03	00000002	\N	D	\N	2013-01-23	1	\N
00000010	03	00000002	\N	D	\N	2013-01-24	1	\N
00000010	03	00000002	\N	F	\N	2013-01-25	1	\N
00000010	03	00000002	\N	F	\N	2013-01-26	1	\N
00000010	03	00000002	\N	D	\N	2013-01-27	1	\N
00000010	03	00000002	\N	D	\N	2013-01-28	1	\N
00000010	03	00000002	\N	D	\N	2013-01-29	1	\N
00000010	03	00000002	\N	D	\N	2013-01-30	1	\N
00000010	03	00000002	\N	D	\N	2013-01-31	1	\N
00000011	03	00000002	\N	D	\N	2013-01-01	1	\N
00000011	03	00000002	\N	D	\N	2013-01-02	1	\N
00000011	03	00000002	\N	D	\N	2013-01-03	1	\N
00000011	03	00000002	\N	F	\N	2013-01-04	1	\N
00000011	03	00000002	\N	F	\N	2013-01-05	1	\N
00000011	03	00000002	\N	D	\N	2013-01-06	1	\N
00000011	03	00000002	\N	D	\N	2013-01-07	1	\N
00000011	03	00000002	\N	D	\N	2013-01-08	1	\N
00000011	03	00000002	\N	D	\N	2013-01-09	1	\N
00000011	03	00000002	\N	D	\N	2013-01-10	1	\N
00000011	03	00000002	\N	F	\N	2013-01-11	1	\N
00000011	03	00000002	\N	F	\N	2013-01-12	1	\N
00000011	03	00000002	\N	D	\N	2013-01-13	1	\N
00000011	03	00000002	\N	D	\N	2013-01-14	1	\N
00000011	03	00000002	\N	D	\N	2013-01-15	1	\N
00000011	03	00000002	\N	D	\N	2013-01-16	1	\N
00000011	03	00000002	\N	D	\N	2013-01-17	1	\N
00000011	03	00000002	\N	F	\N	2013-01-18	1	\N
00000011	03	00000002	\N	F	\N	2013-01-19	1	\N
00000011	03	00000002	\N	D	\N	2013-01-20	1	\N
00000011	03	00000002	\N	D	\N	2013-01-21	1	\N
00000011	03	00000002	\N	D	\N	2013-01-22	1	\N
00000011	03	00000002	\N	D	\N	2013-01-23	1	\N
00000011	03	00000002	\N	D	\N	2013-01-24	1	\N
00000011	03	00000002	\N	F	\N	2013-01-25	1	\N
00000011	03	00000002	\N	F	\N	2013-01-26	1	\N
00000011	03	00000002	\N	D	\N	2013-01-27	1	\N
00000011	03	00000002	\N	D	\N	2013-01-28	1	\N
00000011	03	00000002	\N	D	\N	2013-01-29	1	\N
00000011	03	00000002	\N	D	\N	2013-01-30	1	\N
00000011	03	00000002	\N	D	\N	2013-01-31	1	\N
00000012	03	00000002	\N	D	\N	2013-01-01	1	\N
00000012	03	00000002	\N	D	\N	2013-01-02	1	\N
00000012	03	00000002	\N	D	\N	2013-01-03	1	\N
00000012	03	00000002	\N	F	\N	2013-01-04	1	\N
00000012	03	00000002	\N	F	\N	2013-01-05	1	\N
00000012	03	00000002	\N	D	\N	2013-01-06	1	\N
00000001	01	00000002	\N	F	\N	2012-09-28	0	\N
00000001	01	00000002	\N	F	\N	2012-09-29	0	\N
00000001	01	00000002	\N	D	\N	2012-09-30	0	\N
00000002	01	00000002	\N	F	\N	2012-09-28	0	\N
00000002	01	00000002	\N	F	\N	2012-09-29	0	\N
00000002	01	00000002	\N	D	\N	2012-09-30	0	\N
00000003	01	00000002	\N	F	\N	2012-09-28	0	\N
00000003	01	00000002	\N	F	\N	2012-09-29	0	\N
00000003	01	00000002	\N	D	\N	2012-09-30	0	\N
00000004	01	00000002	\N	F	\N	2012-09-28	0	\N
00000004	01	00000002	\N	F	\N	2012-09-29	0	\N
00000004	01	00000002	\N	D	\N	2012-09-30	0	\N
00000005	01	00000002	\N	F	\N	2012-09-28	0	\N
00000005	01	00000002	\N	F	\N	2012-09-29	0	\N
00000005	01	00000002	\N	D	\N	2012-09-30	0	\N
00000006	01	00000002	\N	F	\N	2012-09-28	0	\N
00000006	01	00000002	\N	F	\N	2012-09-29	0	\N
00000006	01	00000002	\N	D	\N	2012-09-30	0	\N
00000007	01	00000002	\N	F	\N	2012-09-28	0	\N
00000007	01	00000002	\N	F	\N	2012-09-29	0	\N
00000007	01	00000002	\N	D	\N	2012-09-30	0	\N
00000035	02	00000002	\N	D	\N	2012-09-24	0	\N
00000035	02	00000002	\N	D	\N	2012-09-25	0	\N
00000035	02	00000002	\N	D	\N	2012-09-26	0	\N
00000042	01	00000003	\N	F	\N	2012-09-01	1	\N
00000042	01	00000003	\N	D	\N	2012-09-02	1	\N
00000042	01	00000003	\N	D	\N	2012-09-03	1	\N
00000042	01	00000003	\N	D	\N	2012-09-04	1	\N
00000042	01	00000003	\N	D	\N	2012-09-05	1	\N
00000042	01	00000003	\N	D	\N	2012-09-06	1	\N
00000042	01	00000003	\N	F	\N	2012-09-07	1	\N
00000042	01	00000003	\N	F	\N	2012-09-08	1	\N
00000042	01	00000003	\N	D	\N	2012-09-09	1	\N
00000042	01	00000003	\N	D	\N	2012-09-10	1	\N
00000042	01	00000003	\N	D	\N	2012-09-11	1	\N
00000042	01	00000003	\N	D	\N	2012-09-12	1	\N
00000042	01	00000003	\N	D	\N	2012-09-13	1	\N
00000042	01	00000003	\N	F	\N	2012-09-14	1	\N
00000042	01	00000003	\N	F	\N	2012-09-15	1	\N
00000042	01	00000003	\N	D	\N	2012-09-16	1	\N
00000042	01	00000003	\N	D	\N	2012-09-17	1	\N
00000042	01	00000003	\N	D	\N	2012-09-18	1	\N
00000042	01	00000003	\N	D	\N	2012-09-19	1	\N
00000042	01	00000003	\N	D	\N	2012-09-20	1	\N
00000042	01	00000003	\N	F	\N	2012-09-21	1	\N
00000042	01	00000003	\N	F	\N	2012-09-22	1	\N
00000042	01	00000003	\N	D	\N	2012-09-23	1	\N
00000042	01	00000003	\N	D	\N	2012-09-24	1	\N
00000042	01	00000003	\N	D	\N	2012-09-25	1	\N
00000042	01	00000003	\N	D	\N	2012-09-26	1	\N
00000042	01	00000003	\N	D	\N	2012-09-27	1	\N
00000042	01	00000003	\N	F	\N	2012-09-28	1	\N
00000042	01	00000003	\N	F	\N	2012-09-29	1	\N
00000042	01	00000003	\N	D	\N	2012-09-30	1	\N
00000043	01	00000003	\N	F	\N	2012-09-01	1	\N
00000043	01	00000003	\N	D	\N	2012-09-02	1	\N
00000043	01	00000003	\N	D	\N	2012-09-03	1	\N
00000043	01	00000003	\N	D	\N	2012-09-04	1	\N
00000043	01	00000003	\N	D	\N	2012-09-05	1	\N
00000043	01	00000003	\N	D	\N	2012-09-06	1	\N
00000043	01	00000003	\N	F	\N	2012-09-07	1	\N
00000043	01	00000003	\N	F	\N	2012-09-08	1	\N
00000043	01	00000003	\N	D	\N	2012-09-09	1	\N
00000043	01	00000003	\N	D	\N	2012-09-10	1	\N
00000043	01	00000003	\N	D	\N	2012-09-11	1	\N
00000043	01	00000003	\N	D	\N	2012-09-12	1	\N
00000043	01	00000003	\N	D	\N	2012-09-13	1	\N
00000043	01	00000003	\N	F	\N	2012-09-14	1	\N
00000043	01	00000003	\N	F	\N	2012-09-15	1	\N
00000043	01	00000003	\N	D	\N	2012-09-16	1	\N
00000043	01	00000003	\N	D	\N	2012-09-17	1	\N
00000043	01	00000003	\N	D	\N	2012-09-18	1	\N
00000043	01	00000003	\N	D	\N	2012-09-19	1	\N
00000043	01	00000003	\N	D	\N	2012-09-20	1	\N
00000043	01	00000003	\N	F	\N	2012-09-21	1	\N
00000043	01	00000003	\N	F	\N	2012-09-22	1	\N
00000043	01	00000003	\N	D	\N	2012-09-23	1	\N
00000043	01	00000003	\N	D	\N	2012-09-24	1	\N
00000043	01	00000003	\N	D	\N	2012-09-25	1	\N
00000043	01	00000003	\N	D	\N	2012-09-26	1	\N
00000043	01	00000003	\N	D	\N	2012-09-27	1	\N
00000043	01	00000003	\N	F	\N	2012-09-28	1	\N
00000043	01	00000003	\N	F	\N	2012-09-29	1	\N
00000043	01	00000003	\N	D	\N	2012-09-30	1	\N
00000044	01	00000003	\N	F	\N	2012-09-01	1	\N
00000044	01	00000003	\N	D	\N	2012-09-02	1	\N
00000044	01	00000003	\N	D	\N	2012-09-03	1	\N
00000044	01	00000003	\N	D	\N	2012-09-04	1	\N
00000044	01	00000003	\N	D	\N	2012-09-05	1	\N
00000044	01	00000003	\N	D	\N	2012-09-06	1	\N
00000044	01	00000003	\N	F	\N	2012-09-07	1	\N
00000044	01	00000003	\N	F	\N	2012-09-08	1	\N
00000044	01	00000003	\N	D	\N	2012-09-09	1	\N
00000044	01	00000003	\N	D	\N	2012-09-10	1	\N
00000044	01	00000003	\N	D	\N	2012-09-11	1	\N
00000044	01	00000003	\N	D	\N	2012-09-12	1	\N
00000044	01	00000003	\N	D	\N	2012-09-13	1	\N
00000044	01	00000003	\N	F	\N	2012-09-14	1	\N
00000044	01	00000003	\N	F	\N	2012-09-15	1	\N
00000044	01	00000003	\N	D	\N	2012-09-16	1	\N
00000044	01	00000003	\N	D	\N	2012-09-17	1	\N
00000044	01	00000003	\N	D	\N	2012-09-18	1	\N
00000044	01	00000003	\N	D	\N	2012-09-19	1	\N
00000044	01	00000003	\N	D	\N	2012-09-20	1	\N
00000044	01	00000003	\N	F	\N	2012-09-21	1	\N
00000044	01	00000003	\N	F	\N	2012-09-22	1	\N
00000044	01	00000003	\N	D	\N	2012-09-23	1	\N
00000044	01	00000003	\N	D	\N	2012-09-24	1	\N
00000044	01	00000003	\N	D	\N	2012-09-25	1	\N
00000044	01	00000003	\N	D	\N	2012-09-26	1	\N
00000044	01	00000003	\N	D	\N	2012-09-27	1	\N
00000044	01	00000003	\N	F	\N	2012-09-28	1	\N
00000044	01	00000003	\N	F	\N	2012-09-29	1	\N
00000044	01	00000003	\N	D	\N	2012-09-30	1	\N
00000045	01	00000003	\N	F	\N	2012-09-01	1	\N
00000045	01	00000003	\N	D	\N	2012-09-02	1	\N
00000045	01	00000003	\N	D	\N	2012-09-03	1	\N
00000045	01	00000003	\N	D	\N	2012-09-04	1	\N
00000045	01	00000003	\N	D	\N	2012-09-05	1	\N
00000045	01	00000003	\N	D	\N	2012-09-06	1	\N
00000045	01	00000003	\N	F	\N	2012-09-07	1	\N
00000045	01	00000003	\N	F	\N	2012-09-08	1	\N
00000045	01	00000003	\N	D	\N	2012-09-09	1	\N
00000045	01	00000003	\N	D	\N	2012-09-10	1	\N
00000045	01	00000003	\N	D	\N	2012-09-11	1	\N
00000045	01	00000003	\N	D	\N	2012-09-12	1	\N
00000045	01	00000003	\N	D	\N	2012-09-13	1	\N
00000045	01	00000003	\N	F	\N	2012-09-14	1	\N
00000045	01	00000003	\N	F	\N	2012-09-15	1	\N
00000045	01	00000003	\N	D	\N	2012-09-16	1	\N
00000045	01	00000003	\N	D	\N	2012-09-17	1	\N
00000045	01	00000003	\N	D	\N	2012-09-18	1	\N
00000045	01	00000003	\N	D	\N	2012-09-19	1	\N
00000045	01	00000003	\N	D	\N	2012-09-20	1	\N
00000045	01	00000003	\N	F	\N	2012-09-21	1	\N
00000045	01	00000003	\N	F	\N	2012-09-22	1	\N
00000045	01	00000003	\N	D	\N	2012-09-23	1	\N
00000045	01	00000003	\N	D	\N	2012-09-24	1	\N
00000045	01	00000003	\N	D	\N	2012-09-25	1	\N
00000045	01	00000003	\N	D	\N	2012-09-26	1	\N
00000045	01	00000003	\N	D	\N	2012-09-27	1	\N
00000045	01	00000003	\N	F	\N	2012-09-28	1	\N
00000045	01	00000003	\N	F	\N	2012-09-29	1	\N
00000045	01	00000003	\N	D	\N	2012-09-30	1	\N
00000046	01	00000003	\N	F	\N	2012-09-01	1	\N
00000046	01	00000003	\N	D	\N	2012-09-02	1	\N
00000046	01	00000003	\N	D	\N	2012-09-03	1	\N
00000046	01	00000003	\N	D	\N	2012-09-04	1	\N
00000046	01	00000003	\N	D	\N	2012-09-05	1	\N
00000046	01	00000003	\N	D	\N	2012-09-06	1	\N
00000046	01	00000003	\N	F	\N	2012-09-07	1	\N
00000046	01	00000003	\N	F	\N	2012-09-08	1	\N
00000046	01	00000003	\N	D	\N	2012-09-09	1	\N
00000046	01	00000003	\N	D	\N	2012-09-10	1	\N
00000046	01	00000003	\N	D	\N	2012-09-11	1	\N
00000046	01	00000003	\N	D	\N	2012-09-12	1	\N
00000046	01	00000003	\N	D	\N	2012-09-13	1	\N
00000046	01	00000003	\N	F	\N	2012-09-14	1	\N
00000046	01	00000003	\N	F	\N	2012-09-15	1	\N
00000046	01	00000003	\N	D	\N	2012-09-16	1	\N
00000046	01	00000003	\N	D	\N	2012-09-17	1	\N
00000046	01	00000003	\N	D	\N	2012-09-18	1	\N
00000046	01	00000003	\N	D	\N	2012-09-19	1	\N
00000046	01	00000003	\N	D	\N	2012-09-20	1	\N
00000046	01	00000003	\N	F	\N	2012-09-21	1	\N
00000046	01	00000003	\N	F	\N	2012-09-22	1	\N
00000046	01	00000003	\N	D	\N	2012-09-23	1	\N
00000046	01	00000003	\N	D	\N	2012-09-24	1	\N
00000046	01	00000003	\N	D	\N	2012-09-25	1	\N
00000046	01	00000003	\N	D	\N	2012-09-26	1	\N
00000046	01	00000003	\N	D	\N	2012-09-27	1	\N
00000046	01	00000003	\N	F	\N	2012-09-28	1	\N
00000046	01	00000003	\N	F	\N	2012-09-29	1	\N
00000046	01	00000003	\N	D	\N	2012-09-30	1	\N
00000047	01	00000003	\N	F	\N	2012-09-01	1	\N
00000047	01	00000003	\N	D	\N	2012-09-02	1	\N
00000047	01	00000003	\N	D	\N	2012-09-03	1	\N
00000047	01	00000003	\N	D	\N	2012-09-04	1	\N
00000047	01	00000003	\N	D	\N	2012-09-05	1	\N
00000047	01	00000003	\N	D	\N	2012-09-06	1	\N
00000047	01	00000003	\N	F	\N	2012-09-07	1	\N
00000047	01	00000003	\N	F	\N	2012-09-08	1	\N
00000047	01	00000003	\N	D	\N	2012-09-09	1	\N
00000047	01	00000003	\N	D	\N	2012-09-10	1	\N
00000047	01	00000003	\N	D	\N	2012-09-11	1	\N
00000047	01	00000003	\N	D	\N	2012-09-12	1	\N
00000047	01	00000003	\N	D	\N	2012-09-13	1	\N
00000047	01	00000003	\N	F	\N	2012-09-14	1	\N
00000047	01	00000003	\N	F	\N	2012-09-15	1	\N
00000047	01	00000003	\N	D	\N	2012-09-16	1	\N
00000047	01	00000003	\N	D	\N	2012-09-17	1	\N
00000047	01	00000003	\N	D	\N	2012-09-18	1	\N
00000047	01	00000003	\N	D	\N	2012-09-19	1	\N
00000047	01	00000003	\N	D	\N	2012-09-20	1	\N
00000047	01	00000003	\N	F	\N	2012-09-21	1	\N
00000047	01	00000003	\N	F	\N	2012-09-22	1	\N
00000047	01	00000003	\N	D	\N	2012-09-23	1	\N
00000047	01	00000003	\N	D	\N	2012-09-24	1	\N
00000047	01	00000003	\N	D	\N	2012-09-25	1	\N
00000047	01	00000003	\N	D	\N	2012-09-26	1	\N
00000047	01	00000003	\N	D	\N	2012-09-27	1	\N
00000047	01	00000003	\N	F	\N	2012-09-28	1	\N
00000047	01	00000003	\N	F	\N	2012-09-29	1	\N
00000047	01	00000003	\N	D	\N	2012-09-30	1	\N
00000048	01	00000003	\N	F	\N	2012-09-01	1	\N
00000048	01	00000003	\N	D	\N	2012-09-02	1	\N
00000048	01	00000003	\N	D	\N	2012-09-03	1	\N
00000048	01	00000003	\N	D	\N	2012-09-04	1	\N
00000048	01	00000003	\N	D	\N	2012-09-05	1	\N
00000048	01	00000003	\N	D	\N	2012-09-06	1	\N
00000048	01	00000003	\N	F	\N	2012-09-07	1	\N
00000048	01	00000003	\N	F	\N	2012-09-08	1	\N
00000048	01	00000003	\N	D	\N	2012-09-09	1	\N
00000048	01	00000003	\N	D	\N	2012-09-10	1	\N
00000048	01	00000003	\N	D	\N	2012-09-11	1	\N
00000048	01	00000003	\N	D	\N	2012-09-12	1	\N
00000048	01	00000003	\N	D	\N	2012-09-13	1	\N
00000048	01	00000003	\N	F	\N	2012-09-14	1	\N
00000048	01	00000003	\N	F	\N	2012-09-15	1	\N
00000048	01	00000003	\N	D	\N	2012-09-16	1	\N
00000048	01	00000003	\N	D	\N	2012-09-17	1	\N
00000048	01	00000003	\N	D	\N	2012-09-18	1	\N
00000048	01	00000003	\N	D	\N	2012-09-19	1	\N
00000048	01	00000003	\N	D	\N	2012-09-20	1	\N
00000048	01	00000003	\N	F	\N	2012-09-21	1	\N
00000048	01	00000003	\N	F	\N	2012-09-22	1	\N
00000048	01	00000003	\N	D	\N	2012-09-23	1	\N
00000048	01	00000003	\N	D	\N	2012-09-24	1	\N
00000048	01	00000003	\N	D	\N	2012-09-25	1	\N
00000048	01	00000003	\N	D	\N	2012-09-26	1	\N
00000048	01	00000003	\N	D	\N	2012-09-27	1	\N
00000048	01	00000003	\N	F	\N	2012-09-28	1	\N
00000048	01	00000003	\N	F	\N	2012-09-29	1	\N
00000048	01	00000003	\N	D	\N	2012-09-30	1	\N
00000049	02	00000003	\N	F	\N	2012-09-01	1	\N
00000049	02	00000003	\N	D	\N	2012-09-02	1	\N
00000049	02	00000003	\N	D	\N	2012-09-03	1	\N
00000049	02	00000003	\N	D	\N	2012-09-04	1	\N
00000049	02	00000003	\N	D	\N	2012-09-05	1	\N
00000049	02	00000003	\N	D	\N	2012-09-06	1	\N
00000049	02	00000003	\N	F	\N	2012-09-07	1	\N
00000049	02	00000003	\N	F	\N	2012-09-08	1	\N
00000049	02	00000003	\N	D	\N	2012-09-09	1	\N
00000049	02	00000003	\N	D	\N	2012-09-10	1	\N
00000049	02	00000003	\N	D	\N	2012-09-11	1	\N
00000049	02	00000003	\N	D	\N	2012-09-12	1	\N
00000049	02	00000003	\N	D	\N	2012-09-13	1	\N
00000049	02	00000003	\N	F	\N	2012-09-14	1	\N
00000049	02	00000003	\N	F	\N	2012-09-15	1	\N
00000049	02	00000003	\N	D	\N	2012-09-16	1	\N
00000049	02	00000003	\N	D	\N	2012-09-17	1	\N
00000049	02	00000003	\N	D	\N	2012-09-18	1	\N
00000049	02	00000003	\N	D	\N	2012-09-19	1	\N
00000049	02	00000003	\N	D	\N	2012-09-20	1	\N
00000049	02	00000003	\N	F	\N	2012-09-21	1	\N
00000049	02	00000003	\N	F	\N	2012-09-22	1	\N
00000049	02	00000003	\N	D	\N	2012-09-23	1	\N
00000049	02	00000003	\N	D	\N	2012-09-24	1	\N
00000049	02	00000003	\N	D	\N	2012-09-25	1	\N
00000049	02	00000003	\N	D	\N	2012-09-26	1	\N
00000049	02	00000003	\N	D	\N	2012-09-27	1	\N
00000049	02	00000003	\N	F	\N	2012-09-28	1	\N
00000049	02	00000003	\N	F	\N	2012-09-29	1	\N
00000049	02	00000003	\N	D	\N	2012-09-30	1	\N
00000050	02	00000003	\N	F	\N	2012-09-01	1	\N
00000050	02	00000003	\N	D	\N	2012-09-02	1	\N
00000050	02	00000003	\N	D	\N	2012-09-03	1	\N
00000050	02	00000003	\N	D	\N	2012-09-04	1	\N
00000050	02	00000003	\N	D	\N	2012-09-05	1	\N
00000050	02	00000003	\N	D	\N	2012-09-06	1	\N
00000050	02	00000003	\N	F	\N	2012-09-07	1	\N
00000050	02	00000003	\N	F	\N	2012-09-08	1	\N
00000050	02	00000003	\N	D	\N	2012-09-09	1	\N
00000050	02	00000003	\N	D	\N	2012-09-10	1	\N
00000050	02	00000003	\N	D	\N	2012-09-11	1	\N
00000050	02	00000003	\N	D	\N	2012-09-12	1	\N
00000050	02	00000003	\N	D	\N	2012-09-13	1	\N
00000050	02	00000003	\N	F	\N	2012-09-14	1	\N
00000050	02	00000003	\N	F	\N	2012-09-15	1	\N
00000050	02	00000003	\N	D	\N	2012-09-16	1	\N
00000050	02	00000003	\N	D	\N	2012-09-17	1	\N
00000050	02	00000003	\N	D	\N	2012-09-18	1	\N
00000050	02	00000003	\N	D	\N	2012-09-19	1	\N
00000050	02	00000003	\N	D	\N	2012-09-20	1	\N
00000050	02	00000003	\N	F	\N	2012-09-21	1	\N
00000050	02	00000003	\N	F	\N	2012-09-22	1	\N
00000050	02	00000003	\N	D	\N	2012-09-23	1	\N
00000050	02	00000003	\N	D	\N	2012-09-24	1	\N
00000050	02	00000003	\N	D	\N	2012-09-25	1	\N
00000050	02	00000003	\N	D	\N	2012-09-26	1	\N
00000050	02	00000003	\N	D	\N	2012-09-27	1	\N
00000050	02	00000003	\N	F	\N	2012-09-28	1	\N
00000050	02	00000003	\N	F	\N	2012-09-29	1	\N
00000050	02	00000003	\N	D	\N	2012-09-30	1	\N
00000051	02	00000003	\N	F	\N	2012-09-01	1	\N
00000051	02	00000003	\N	D	\N	2012-09-02	1	\N
00000051	02	00000003	\N	D	\N	2012-09-03	1	\N
00000051	02	00000003	\N	D	\N	2012-09-04	1	\N
00000051	02	00000003	\N	D	\N	2012-09-05	1	\N
00000051	02	00000003	\N	D	\N	2012-09-06	1	\N
00000051	02	00000003	\N	F	\N	2012-09-07	1	\N
00000051	02	00000003	\N	F	\N	2012-09-08	1	\N
00000051	02	00000003	\N	D	\N	2012-09-09	1	\N
00000051	02	00000003	\N	D	\N	2012-09-10	1	\N
00000051	02	00000003	\N	D	\N	2012-09-11	1	\N
00000051	02	00000003	\N	D	\N	2012-09-12	1	\N
00000051	02	00000003	\N	D	\N	2012-09-13	1	\N
00000051	02	00000003	\N	F	\N	2012-09-14	1	\N
00000051	02	00000003	\N	F	\N	2012-09-15	1	\N
00000051	02	00000003	\N	D	\N	2012-09-16	1	\N
00000051	02	00000003	\N	D	\N	2012-09-17	1	\N
00000051	02	00000003	\N	D	\N	2012-09-18	1	\N
00000051	02	00000003	\N	D	\N	2012-09-19	1	\N
00000051	02	00000003	\N	D	\N	2012-09-20	1	\N
00000051	02	00000003	\N	F	\N	2012-09-21	1	\N
00000051	02	00000003	\N	F	\N	2012-09-22	1	\N
00000051	02	00000003	\N	D	\N	2012-09-23	1	\N
00000051	02	00000003	\N	D	\N	2012-09-24	1	\N
00000051	02	00000003	\N	D	\N	2012-09-25	1	\N
00000051	02	00000003	\N	D	\N	2012-09-26	1	\N
00000051	02	00000003	\N	D	\N	2012-09-27	1	\N
00000051	02	00000003	\N	F	\N	2012-09-28	1	\N
00000051	02	00000003	\N	F	\N	2012-09-29	1	\N
00000051	02	00000003	\N	D	\N	2012-09-30	1	\N
00000052	03	00000003	\N	F	\N	2012-09-01	1	\N
00000052	03	00000003	\N	D	\N	2012-09-02	1	\N
00000052	03	00000003	\N	D	\N	2012-09-03	1	\N
00000052	03	00000003	\N	D	\N	2012-09-04	1	\N
00000052	03	00000003	\N	D	\N	2012-09-05	1	\N
00000052	03	00000003	\N	D	\N	2012-09-06	1	\N
00000052	03	00000003	\N	F	\N	2012-09-07	1	\N
00000052	03	00000003	\N	F	\N	2012-09-08	1	\N
00000052	03	00000003	\N	D	\N	2012-09-09	1	\N
00000052	03	00000003	\N	D	\N	2012-09-10	1	\N
00000052	03	00000003	\N	D	\N	2012-09-11	1	\N
00000052	03	00000003	\N	D	\N	2012-09-12	1	\N
00000052	03	00000003	\N	D	\N	2012-09-13	1	\N
00000052	03	00000003	\N	F	\N	2012-09-14	1	\N
00000052	03	00000003	\N	F	\N	2012-09-15	1	\N
00000052	03	00000003	\N	D	\N	2012-09-16	1	\N
00000052	03	00000003	\N	D	\N	2012-09-17	1	\N
00000052	03	00000003	\N	D	\N	2012-09-18	1	\N
00000052	03	00000003	\N	D	\N	2012-09-19	1	\N
00000052	03	00000003	\N	D	\N	2012-09-20	1	\N
00000052	03	00000003	\N	F	\N	2012-09-21	1	\N
00000052	03	00000003	\N	F	\N	2012-09-22	1	\N
00000052	03	00000003	\N	D	\N	2012-09-23	1	\N
00000052	03	00000003	\N	D	\N	2012-09-24	1	\N
00000052	03	00000003	\N	D	\N	2012-09-25	1	\N
00000052	03	00000003	\N	D	\N	2012-09-26	1	\N
00000052	03	00000003	\N	D	\N	2012-09-27	1	\N
00000052	03	00000003	\N	F	\N	2012-09-28	1	\N
00000052	03	00000003	\N	F	\N	2012-09-29	1	\N
00000052	03	00000003	\N	D	\N	2012-09-30	1	\N
00000053	04	00000003	\N	F	\N	2012-09-01	1	\N
00000053	04	00000003	\N	D	\N	2012-09-02	1	\N
00000053	04	00000003	\N	D	\N	2012-09-03	1	\N
00000053	04	00000003	\N	D	\N	2012-09-04	1	\N
00000053	04	00000003	\N	D	\N	2012-09-05	1	\N
00000053	04	00000003	\N	D	\N	2012-09-06	1	\N
00000053	04	00000003	\N	F	\N	2012-09-07	1	\N
00000053	04	00000003	\N	F	\N	2012-09-08	1	\N
00000053	04	00000003	\N	D	\N	2012-09-09	1	\N
00000053	04	00000003	\N	D	\N	2012-09-10	1	\N
00000053	04	00000003	\N	D	\N	2012-09-11	1	\N
00000053	04	00000003	\N	D	\N	2012-09-12	1	\N
00000053	04	00000003	\N	D	\N	2012-09-13	1	\N
00000053	04	00000003	\N	F	\N	2012-09-14	1	\N
00000053	04	00000003	\N	F	\N	2012-09-15	1	\N
00000053	04	00000003	\N	D	\N	2012-09-16	1	\N
00000053	04	00000003	\N	D	\N	2012-09-17	1	\N
00000053	04	00000003	\N	D	\N	2012-09-18	1	\N
00000053	04	00000003	\N	D	\N	2012-09-19	1	\N
00000053	04	00000003	\N	D	\N	2012-09-20	1	\N
00000053	04	00000003	\N	F	\N	2012-09-21	1	\N
00000053	04	00000003	\N	F	\N	2012-09-22	1	\N
00000053	04	00000003	\N	D	\N	2012-09-23	1	\N
00000053	04	00000003	\N	D	\N	2012-09-24	1	\N
00000053	04	00000003	\N	D	\N	2012-09-25	1	\N
00000053	04	00000003	\N	D	\N	2012-09-26	1	\N
00000053	04	00000003	\N	D	\N	2012-09-27	1	\N
00000053	04	00000003	\N	F	\N	2012-09-28	1	\N
00000053	04	00000003	\N	F	\N	2012-09-29	1	\N
00000053	04	00000003	\N	D	\N	2012-09-30	1	\N
00000054	04	00000003	\N	F	\N	2012-09-01	1	\N
00000054	04	00000003	\N	D	\N	2012-09-02	1	\N
00000054	04	00000003	\N	D	\N	2012-09-03	1	\N
00000054	04	00000003	\N	D	\N	2012-09-04	1	\N
00000054	04	00000003	\N	D	\N	2012-09-05	1	\N
00000054	04	00000003	\N	D	\N	2012-09-06	1	\N
00000054	04	00000003	\N	F	\N	2012-09-07	1	\N
00000054	04	00000003	\N	F	\N	2012-09-08	1	\N
00000054	04	00000003	\N	D	\N	2012-09-09	1	\N
00000054	04	00000003	\N	D	\N	2012-09-10	1	\N
00000054	04	00000003	\N	D	\N	2012-09-11	1	\N
00000054	04	00000003	\N	D	\N	2012-09-12	1	\N
00000054	04	00000003	\N	D	\N	2012-09-13	1	\N
00000054	04	00000003	\N	F	\N	2012-09-14	1	\N
00000054	04	00000003	\N	F	\N	2012-09-15	1	\N
00000054	04	00000003	\N	D	\N	2012-09-16	1	\N
00000054	04	00000003	\N	D	\N	2012-09-17	1	\N
00000054	04	00000003	\N	D	\N	2012-09-18	1	\N
00000054	04	00000003	\N	D	\N	2012-09-19	1	\N
00000054	04	00000003	\N	D	\N	2012-09-20	1	\N
00000054	04	00000003	\N	F	\N	2012-09-21	1	\N
00000054	04	00000003	\N	F	\N	2012-09-22	1	\N
00000054	04	00000003	\N	D	\N	2012-09-23	1	\N
00000054	04	00000003	\N	D	\N	2012-09-24	1	\N
00000054	04	00000003	\N	D	\N	2012-09-25	1	\N
00000054	04	00000003	\N	D	\N	2012-09-26	1	\N
00000054	04	00000003	\N	D	\N	2012-09-27	1	\N
00000054	04	00000003	\N	F	\N	2012-09-28	1	\N
00000054	04	00000003	\N	F	\N	2012-09-29	1	\N
00000054	04	00000003	\N	D	\N	2012-09-30	1	\N
00000055	04	00000003	\N	F	\N	2012-09-01	1	\N
00000055	04	00000003	\N	D	\N	2012-09-02	1	\N
00000055	04	00000003	\N	D	\N	2012-09-03	1	\N
00000055	04	00000003	\N	D	\N	2012-09-04	1	\N
00000055	04	00000003	\N	D	\N	2012-09-05	1	\N
00000055	04	00000003	\N	D	\N	2012-09-06	1	\N
00000055	04	00000003	\N	F	\N	2012-09-07	1	\N
00000055	04	00000003	\N	F	\N	2012-09-08	1	\N
00000055	04	00000003	\N	D	\N	2012-09-09	1	\N
00000055	04	00000003	\N	D	\N	2012-09-10	1	\N
00000055	04	00000003	\N	D	\N	2012-09-11	1	\N
00000055	04	00000003	\N	D	\N	2012-09-12	1	\N
00000055	04	00000003	\N	D	\N	2012-09-13	1	\N
00000055	04	00000003	\N	F	\N	2012-09-14	1	\N
00000055	04	00000003	\N	F	\N	2012-09-15	1	\N
00000055	04	00000003	\N	D	\N	2012-09-16	1	\N
00000055	04	00000003	\N	D	\N	2012-09-17	1	\N
00000055	04	00000003	\N	D	\N	2012-09-18	1	\N
00000055	04	00000003	\N	D	\N	2012-09-19	1	\N
00000055	04	00000003	\N	D	\N	2012-09-20	1	\N
00000055	04	00000003	\N	F	\N	2012-09-21	1	\N
00000055	04	00000003	\N	F	\N	2012-09-22	1	\N
00000055	04	00000003	\N	D	\N	2012-09-23	1	\N
00000055	04	00000003	\N	D	\N	2012-09-24	1	\N
00000055	04	00000003	\N	D	\N	2012-09-25	1	\N
00000055	04	00000003	\N	D	\N	2012-09-26	1	\N
00000055	04	00000003	\N	D	\N	2012-09-27	1	\N
00000055	04	00000003	\N	F	\N	2012-09-28	1	\N
00000055	04	00000003	\N	F	\N	2012-09-29	1	\N
00000055	04	00000003	\N	D	\N	2012-09-30	1	\N
00000056	04	00000003	\N	F	\N	2012-09-01	1	\N
00000056	04	00000003	\N	D	\N	2012-09-02	1	\N
00000056	04	00000003	\N	D	\N	2012-09-03	1	\N
00000056	04	00000003	\N	D	\N	2012-09-04	1	\N
00000056	04	00000003	\N	D	\N	2012-09-05	1	\N
00000056	04	00000003	\N	D	\N	2012-09-06	1	\N
00000056	04	00000003	\N	F	\N	2012-09-07	1	\N
00000056	04	00000003	\N	F	\N	2012-09-08	1	\N
00000056	04	00000003	\N	D	\N	2012-09-09	1	\N
00000056	04	00000003	\N	D	\N	2012-09-10	1	\N
00000056	04	00000003	\N	D	\N	2012-09-11	1	\N
00000056	04	00000003	\N	D	\N	2012-09-12	1	\N
00000056	04	00000003	\N	D	\N	2012-09-13	1	\N
00000056	04	00000003	\N	F	\N	2012-09-14	1	\N
00000056	04	00000003	\N	F	\N	2012-09-15	1	\N
00000056	04	00000003	\N	D	\N	2012-09-16	1	\N
00000056	04	00000003	\N	D	\N	2012-09-17	1	\N
00000056	04	00000003	\N	D	\N	2012-09-18	1	\N
00000056	04	00000003	\N	D	\N	2012-09-19	1	\N
00000056	04	00000003	\N	D	\N	2012-09-20	1	\N
00000056	04	00000003	\N	F	\N	2012-09-21	1	\N
00000056	04	00000003	\N	F	\N	2012-09-22	1	\N
00000056	04	00000003	\N	D	\N	2012-09-23	1	\N
00000056	04	00000003	\N	D	\N	2012-09-24	1	\N
00000056	04	00000003	\N	D	\N	2012-09-25	1	\N
00000056	04	00000003	\N	D	\N	2012-09-26	1	\N
00000056	04	00000003	\N	D	\N	2012-09-27	1	\N
00000056	04	00000003	\N	F	\N	2012-09-28	1	\N
00000056	04	00000003	\N	F	\N	2012-09-29	1	\N
00000056	04	00000003	\N	D	\N	2012-09-30	1	\N
00000057	04	00000003	\N	F	\N	2012-09-01	1	\N
00000057	04	00000003	\N	D	\N	2012-09-02	1	\N
00000057	04	00000003	\N	D	\N	2012-09-03	1	\N
00000057	04	00000003	\N	D	\N	2012-09-04	1	\N
00000057	04	00000003	\N	D	\N	2012-09-05	1	\N
00000057	04	00000003	\N	D	\N	2012-09-06	1	\N
00000057	04	00000003	\N	F	\N	2012-09-07	1	\N
00000057	04	00000003	\N	F	\N	2012-09-08	1	\N
00000057	04	00000003	\N	D	\N	2012-09-09	1	\N
00000057	04	00000003	\N	D	\N	2012-09-10	1	\N
00000057	04	00000003	\N	D	\N	2012-09-11	1	\N
00000057	04	00000003	\N	D	\N	2012-09-12	1	\N
00000057	04	00000003	\N	D	\N	2012-09-13	1	\N
00000057	04	00000003	\N	F	\N	2012-09-14	1	\N
00000057	04	00000003	\N	F	\N	2012-09-15	1	\N
00000057	04	00000003	\N	D	\N	2012-09-16	1	\N
00000057	04	00000003	\N	D	\N	2012-09-17	1	\N
00000057	04	00000003	\N	D	\N	2012-09-18	1	\N
00000057	04	00000003	\N	D	\N	2012-09-19	1	\N
00000057	04	00000003	\N	D	\N	2012-09-20	1	\N
00000057	04	00000003	\N	F	\N	2012-09-21	1	\N
00000057	04	00000003	\N	F	\N	2012-09-22	1	\N
00000057	04	00000003	\N	D	\N	2012-09-23	1	\N
00000057	04	00000003	\N	D	\N	2012-09-24	1	\N
00000057	04	00000003	\N	D	\N	2012-09-25	1	\N
00000057	04	00000003	\N	D	\N	2012-09-26	1	\N
00000057	04	00000003	\N	D	\N	2012-09-27	1	\N
00000057	04	00000003	\N	F	\N	2012-09-28	1	\N
00000057	04	00000003	\N	F	\N	2012-09-29	1	\N
00000057	04	00000003	\N	D	\N	2012-09-30	1	\N
00000058	05	00000003	\N	F	\N	2012-09-01	1	\N
00000058	05	00000003	\N	D	\N	2012-09-02	1	\N
00000058	05	00000003	\N	D	\N	2012-09-03	1	\N
00000058	05	00000003	\N	D	\N	2012-09-04	1	\N
00000058	05	00000003	\N	D	\N	2012-09-05	1	\N
00000058	05	00000003	\N	D	\N	2012-09-06	1	\N
00000058	05	00000003	\N	F	\N	2012-09-07	1	\N
00000058	05	00000003	\N	F	\N	2012-09-08	1	\N
00000058	05	00000003	\N	D	\N	2012-09-09	1	\N
00000058	05	00000003	\N	D	\N	2012-09-10	1	\N
00000058	05	00000003	\N	D	\N	2012-09-11	1	\N
00000058	05	00000003	\N	D	\N	2012-09-12	1	\N
00000058	05	00000003	\N	D	\N	2012-09-13	1	\N
00000058	05	00000003	\N	F	\N	2012-09-14	1	\N
00000058	05	00000003	\N	F	\N	2012-09-15	1	\N
00000058	05	00000003	\N	D	\N	2012-09-16	1	\N
00000058	05	00000003	\N	D	\N	2012-09-17	1	\N
00000058	05	00000003	\N	D	\N	2012-09-18	1	\N
00000058	05	00000003	\N	D	\N	2012-09-19	1	\N
00000058	05	00000003	\N	D	\N	2012-09-20	1	\N
00000058	05	00000003	\N	F	\N	2012-09-21	1	\N
00000058	05	00000003	\N	F	\N	2012-09-22	1	\N
00000058	05	00000003	\N	D	\N	2012-09-23	1	\N
00000058	05	00000003	\N	D	\N	2012-09-24	1	\N
00000058	05	00000003	\N	D	\N	2012-09-25	1	\N
00000058	05	00000003	\N	D	\N	2012-09-26	1	\N
00000058	05	00000003	\N	D	\N	2012-09-27	1	\N
00000058	05	00000003	\N	F	\N	2012-09-28	1	\N
00000058	05	00000003	\N	F	\N	2012-09-29	1	\N
00000058	05	00000003	\N	D	\N	2012-09-30	1	\N
00000059	05	00000003	\N	F	\N	2012-09-01	1	\N
00000059	05	00000003	\N	D	\N	2012-09-02	1	\N
00000059	05	00000003	\N	D	\N	2012-09-03	1	\N
00000059	05	00000003	\N	D	\N	2012-09-04	1	\N
00000059	05	00000003	\N	D	\N	2012-09-05	1	\N
00000059	05	00000003	\N	D	\N	2012-09-06	1	\N
00000059	05	00000003	\N	F	\N	2012-09-07	1	\N
00000059	05	00000003	\N	F	\N	2012-09-08	1	\N
00000059	05	00000003	\N	D	\N	2012-09-09	1	\N
00000059	05	00000003	\N	D	\N	2012-09-10	1	\N
00000059	05	00000003	\N	D	\N	2012-09-11	1	\N
00000059	05	00000003	\N	D	\N	2012-09-12	1	\N
00000059	05	00000003	\N	D	\N	2012-09-13	1	\N
00000059	05	00000003	\N	F	\N	2012-09-14	1	\N
00000059	05	00000003	\N	F	\N	2012-09-15	1	\N
00000059	05	00000003	\N	D	\N	2012-09-16	1	\N
00000059	05	00000003	\N	D	\N	2012-09-17	1	\N
00000059	05	00000003	\N	D	\N	2012-09-18	1	\N
00000059	05	00000003	\N	D	\N	2012-09-19	1	\N
00000059	05	00000003	\N	D	\N	2012-09-20	1	\N
00000059	05	00000003	\N	F	\N	2012-09-21	1	\N
00000059	05	00000003	\N	F	\N	2012-09-22	1	\N
00000059	05	00000003	\N	D	\N	2012-09-23	1	\N
00000059	05	00000003	\N	D	\N	2012-09-24	1	\N
00000059	05	00000003	\N	D	\N	2012-09-25	1	\N
00000059	05	00000003	\N	D	\N	2012-09-26	1	\N
00000059	05	00000003	\N	D	\N	2012-09-27	1	\N
00000059	05	00000003	\N	F	\N	2012-09-28	1	\N
00000059	05	00000003	\N	F	\N	2012-09-29	1	\N
00000059	05	00000003	\N	D	\N	2012-09-30	1	\N
00000060	06	00000003	\N	F	\N	2012-09-01	1	\N
00000060	06	00000003	\N	D	\N	2012-09-02	1	\N
00000060	06	00000003	\N	D	\N	2012-09-03	1	\N
00000060	06	00000003	\N	D	\N	2012-09-04	1	\N
00000060	06	00000003	\N	D	\N	2012-09-05	1	\N
00000060	06	00000003	\N	D	\N	2012-09-06	1	\N
00000060	06	00000003	\N	F	\N	2012-09-07	1	\N
00000060	06	00000003	\N	F	\N	2012-09-08	1	\N
00000060	06	00000003	\N	D	\N	2012-09-09	1	\N
00000060	06	00000003	\N	D	\N	2012-09-10	1	\N
00000060	06	00000003	\N	D	\N	2012-09-11	1	\N
00000060	06	00000003	\N	D	\N	2012-09-12	1	\N
00000060	06	00000003	\N	D	\N	2012-09-13	1	\N
00000060	06	00000003	\N	F	\N	2012-09-14	1	\N
00000060	06	00000003	\N	F	\N	2012-09-15	1	\N
00000060	06	00000003	\N	D	\N	2012-09-16	1	\N
00000060	06	00000003	\N	D	\N	2012-09-17	1	\N
00000060	06	00000003	\N	D	\N	2012-09-18	1	\N
00000060	06	00000003	\N	D	\N	2012-09-19	1	\N
00000060	06	00000003	\N	D	\N	2012-09-20	1	\N
00000060	06	00000003	\N	F	\N	2012-09-21	1	\N
00000060	06	00000003	\N	F	\N	2012-09-22	1	\N
00000060	06	00000003	\N	D	\N	2012-09-23	1	\N
00000060	06	00000003	\N	D	\N	2012-09-24	1	\N
00000060	06	00000003	\N	D	\N	2012-09-25	1	\N
00000060	06	00000003	\N	D	\N	2012-09-26	1	\N
00000060	06	00000003	\N	D	\N	2012-09-27	1	\N
00000060	06	00000003	\N	F	\N	2012-09-28	1	\N
00000060	06	00000003	\N	F	\N	2012-09-29	1	\N
00000060	06	00000003	\N	D	\N	2012-09-30	1	\N
00000001	01	00000002	\N	D	\N	2012-09-12	0	\N
00000001	01	00000002	\N	D	\N	2012-09-13	0	\N
00000001	01	00000002	\N	F	\N	2012-09-14	0	\N
00000001	01	00000002	\N	F	\N	2012-09-15	0	\N
00000001	01	00000002	\N	D	\N	2012-09-16	0	\N
00000001	01	00000002	\N	D	\N	2012-09-17	0	\N
00000001	01	00000002	\N	D	\N	2012-09-18	0	\N
00000001	01	00000002	\N	D	\N	2012-09-19	0	\N
00000001	01	00000002	\N	D	\N	2012-09-20	0	\N
00000001	01	00000002	\N	F	\N	2012-09-21	0	\N
00000001	01	00000002	\N	F	\N	2012-09-22	0	\N
00000001	01	00000002	\N	D	\N	2012-09-23	0	\N
00000001	01	00000002	\N	D	\N	2012-09-24	0	\N
00000001	01	00000002	\N	D	\N	2012-09-25	0	\N
00000001	01	00000002	\N	D	\N	2012-09-26	0	\N
00000001	01	00000002	\N	D	\N	2012-09-27	0	\N
00000002	01	00000002	\N	F	\N	2012-09-01	0	\N
00000002	01	00000002	\N	D	\N	2012-09-02	0	\N
00000002	01	00000002	\N	D	\N	2012-09-03	0	\N
00000002	01	00000002	\N	D	\N	2012-09-04	0	\N
00000002	01	00000002	\N	D	\N	2012-09-05	0	\N
00000002	01	00000002	\N	D	\N	2012-09-06	0	\N
00000002	01	00000002	\N	F	\N	2012-09-07	0	\N
00000002	01	00000002	\N	F	\N	2012-09-08	0	\N
00000002	01	00000002	\N	D	\N	2012-09-09	0	\N
00000002	01	00000002	\N	D	\N	2012-09-10	0	\N
00000002	01	00000002	\N	D	\N	2012-09-11	0	\N
00000002	01	00000002	\N	D	\N	2012-09-12	0	\N
00000002	01	00000002	\N	D	\N	2012-09-13	0	\N
00000002	01	00000002	\N	F	\N	2012-09-14	0	\N
00000002	01	00000002	\N	F	\N	2012-09-15	0	\N
00000002	01	00000002	\N	D	\N	2012-09-16	0	\N
00000002	01	00000002	\N	D	\N	2012-09-17	0	\N
00000002	01	00000002	\N	D	\N	2012-09-18	0	\N
00000002	01	00000002	\N	D	\N	2012-09-19	0	\N
00000002	01	00000002	\N	D	\N	2012-09-20	0	\N
00000002	01	00000002	\N	F	\N	2012-09-21	0	\N
00000002	01	00000002	\N	F	\N	2012-09-22	0	\N
00000002	01	00000002	\N	D	\N	2012-09-23	0	\N
00000002	01	00000002	\N	D	\N	2012-09-24	0	\N
00000002	01	00000002	\N	D	\N	2012-09-25	0	\N
00000002	01	00000002	\N	D	\N	2012-09-26	0	\N
00000002	01	00000002	\N	D	\N	2012-09-27	0	\N
00000003	01	00000002	\N	F	\N	2012-09-01	0	\N
00000003	01	00000002	\N	D	\N	2012-09-02	0	\N
00000003	01	00000002	\N	D	\N	2012-09-03	0	\N
00000003	01	00000002	\N	D	\N	2012-09-04	0	\N
00000003	01	00000002	\N	D	\N	2012-09-05	0	\N
00000003	01	00000002	\N	D	\N	2012-09-06	0	\N
00000003	01	00000002	\N	F	\N	2012-09-07	0	\N
00000003	01	00000002	\N	F	\N	2012-09-08	0	\N
00000003	01	00000002	\N	D	\N	2012-09-09	0	\N
00000003	01	00000002	\N	D	\N	2012-09-10	0	\N
00000003	01	00000002	\N	D	\N	2012-09-11	0	\N
00000003	01	00000002	\N	D	\N	2012-09-12	0	\N
00000003	01	00000002	\N	D	\N	2012-09-13	0	\N
00000003	01	00000002	\N	F	\N	2012-09-14	0	\N
00000003	01	00000002	\N	F	\N	2012-09-15	0	\N
00000003	01	00000002	\N	D	\N	2012-09-16	0	\N
00000003	01	00000002	\N	D	\N	2012-09-17	0	\N
00000003	01	00000002	\N	D	\N	2012-09-18	0	\N
00000003	01	00000002	\N	D	\N	2012-09-19	0	\N
00000003	01	00000002	\N	D	\N	2012-09-20	0	\N
00000003	01	00000002	\N	F	\N	2012-09-21	0	\N
00000003	01	00000002	\N	F	\N	2012-09-22	0	\N
00000003	01	00000002	\N	D	\N	2012-09-23	0	\N
00000003	01	00000002	\N	D	\N	2012-09-24	0	\N
00000003	01	00000002	\N	D	\N	2012-09-25	0	\N
00000003	01	00000002	\N	D	\N	2012-09-26	0	\N
00000003	01	00000002	\N	D	\N	2012-09-27	0	\N
00000004	01	00000002	\N	F	\N	2012-09-01	0	\N
00000004	01	00000002	\N	D	\N	2012-09-02	0	\N
00000004	01	00000002	\N	D	\N	2012-09-03	0	\N
00000004	01	00000002	\N	D	\N	2012-09-04	0	\N
00000004	01	00000002	\N	D	\N	2012-09-05	0	\N
00000004	01	00000002	\N	D	\N	2012-09-06	0	\N
00000004	01	00000002	\N	F	\N	2012-09-07	0	\N
00000004	01	00000002	\N	F	\N	2012-09-08	0	\N
00000004	01	00000002	\N	D	\N	2012-09-09	0	\N
00000004	01	00000002	\N	D	\N	2012-09-10	0	\N
00000004	01	00000002	\N	D	\N	2012-09-11	0	\N
00000004	01	00000002	\N	D	\N	2012-09-12	0	\N
00000004	01	00000002	\N	D	\N	2012-09-13	0	\N
00000004	01	00000002	\N	F	\N	2012-09-14	0	\N
00000004	01	00000002	\N	F	\N	2012-09-15	0	\N
00000004	01	00000002	\N	D	\N	2012-09-16	0	\N
00000004	01	00000002	\N	D	\N	2012-09-17	0	\N
00000004	01	00000002	\N	D	\N	2012-09-18	0	\N
00000004	01	00000002	\N	D	\N	2012-09-19	0	\N
00000004	01	00000002	\N	D	\N	2012-09-20	0	\N
00000004	01	00000002	\N	F	\N	2012-09-21	0	\N
00000004	01	00000002	\N	F	\N	2012-09-22	0	\N
00000004	01	00000002	\N	D	\N	2012-09-23	0	\N
00000004	01	00000002	\N	D	\N	2012-09-24	0	\N
00000004	01	00000002	\N	D	\N	2012-09-25	0	\N
00000004	01	00000002	\N	D	\N	2012-09-26	0	\N
00000004	01	00000002	\N	D	\N	2012-09-27	0	\N
00000005	01	00000002	\N	F	\N	2012-09-01	0	\N
00000005	01	00000002	\N	D	\N	2012-09-02	0	\N
00000005	01	00000002	\N	D	\N	2012-09-03	0	\N
00000005	01	00000002	\N	D	\N	2012-09-04	0	\N
00000005	01	00000002	\N	D	\N	2012-09-05	0	\N
00000005	01	00000002	\N	D	\N	2012-09-06	0	\N
00000005	01	00000002	\N	F	\N	2012-09-07	0	\N
00000005	01	00000002	\N	D	\N	2012-09-19	0	\N
00000005	01	00000002	\N	D	\N	2012-09-20	0	\N
00000005	01	00000002	\N	F	\N	2012-09-21	0	\N
00000005	01	00000002	\N	F	\N	2012-09-22	0	\N
00000005	01	00000002	\N	D	\N	2012-09-23	0	\N
00000005	01	00000002	\N	D	\N	2012-09-24	0	\N
00000005	01	00000002	\N	D	\N	2012-09-25	0	\N
00000005	01	00000002	\N	D	\N	2012-09-26	0	\N
00000005	01	00000002	\N	D	\N	2012-09-27	0	\N
00000006	01	00000002	\N	F	\N	2012-09-01	0	\N
00000006	01	00000002	\N	D	\N	2012-09-02	0	\N
00000006	01	00000002	\N	D	\N	2012-09-03	0	\N
00000006	01	00000002	\N	D	\N	2012-09-04	0	\N
00000006	01	00000002	\N	D	\N	2012-09-05	0	\N
00000006	01	00000002	\N	D	\N	2012-09-06	0	\N
00000006	01	00000002	\N	F	\N	2012-09-07	0	\N
00000006	01	00000002	\N	F	\N	2012-09-08	0	\N
00000006	01	00000002	\N	D	\N	2012-09-09	0	\N
00000006	01	00000002	\N	D	\N	2012-09-10	0	\N
00000006	01	00000002	\N	D	\N	2012-09-11	0	\N
00000006	01	00000002	\N	D	\N	2012-09-12	0	\N
00000006	01	00000002	\N	D	\N	2012-09-13	0	\N
00000006	01	00000002	\N	F	\N	2012-09-14	0	\N
00000006	01	00000002	\N	F	\N	2012-09-15	0	\N
00000006	01	00000002	\N	D	\N	2012-09-16	0	\N
00000006	01	00000002	\N	D	\N	2012-09-17	0	\N
00000006	01	00000002	\N	D	\N	2012-09-18	0	\N
00000006	01	00000002	\N	D	\N	2012-09-19	0	\N
00000006	01	00000002	\N	D	\N	2012-09-20	0	\N
00000006	01	00000002	\N	F	\N	2012-09-21	0	\N
00000006	01	00000002	\N	F	\N	2012-09-22	0	\N
00000006	01	00000002	\N	D	\N	2012-09-23	0	\N
00000006	01	00000002	\N	D	\N	2012-09-24	0	\N
00000006	01	00000002	\N	D	\N	2012-09-25	0	\N
00000006	01	00000002	\N	D	\N	2012-09-26	0	\N
00000006	01	00000002	\N	D	\N	2012-09-27	0	\N
00000007	01	00000002	\N	F	\N	2012-09-01	0	\N
00000007	01	00000002	\N	D	\N	2012-09-02	0	\N
00000007	01	00000002	\N	D	\N	2012-09-03	0	\N
00000007	01	00000002	\N	D	\N	2012-09-04	0	\N
00000007	01	00000002	\N	D	\N	2012-09-05	0	\N
00000007	01	00000002	\N	D	\N	2012-09-06	0	\N
00000007	01	00000002	\N	F	\N	2012-09-07	0	\N
00000007	01	00000002	\N	F	\N	2012-09-08	0	\N
00000007	01	00000002	\N	D	\N	2012-09-09	0	\N
00000007	01	00000002	\N	D	\N	2012-09-10	0	\N
00000007	01	00000002	\N	D	\N	2012-09-11	0	\N
00000007	01	00000002	\N	D	\N	2012-09-12	0	\N
00000007	01	00000002	\N	D	\N	2012-09-13	0	\N
00000007	01	00000002	\N	F	\N	2012-09-14	0	\N
00000007	01	00000002	\N	F	\N	2012-09-15	0	\N
00000007	01	00000002	\N	D	\N	2012-09-16	0	\N
00000007	01	00000002	\N	D	\N	2012-09-17	0	\N
00000007	01	00000002	\N	D	\N	2012-09-18	0	\N
00000007	01	00000002	\N	D	\N	2012-09-19	0	\N
00000007	01	00000002	\N	D	\N	2012-09-20	0	\N
00000007	01	00000002	\N	F	\N	2012-09-21	0	\N
00000007	01	00000002	\N	F	\N	2012-09-22	0	\N
00000007	01	00000002	\N	D	\N	2012-09-23	0	\N
00000007	01	00000002	\N	D	\N	2012-09-24	0	\N
00000007	01	00000002	\N	D	\N	2012-09-25	0	\N
00000007	01	00000002	\N	D	\N	2012-09-26	0	\N
00000007	01	00000002	\N	D	\N	2012-09-27	0	\N
00000035	02	00000002	\N	D	\N	2012-09-27	0	\N
00000035	02	00000002	\N	F	\N	2012-09-28	0	\N
00000035	02	00000002	\N	F	\N	2012-09-29	0	\N
00000035	02	00000002	\N	D	\N	2012-09-30	0	\N
00000036	02	00000002	\N	F	\N	2012-09-01	0	\N
00000036	02	00000002	\N	D	\N	2012-09-02	0	\N
00000036	02	00000002	\N	D	\N	2012-09-03	0	\N
00000036	02	00000002	\N	D	\N	2012-09-04	0	\N
00000036	02	00000002	\N	D	\N	2012-09-05	0	\N
00000036	02	00000002	\N	D	\N	2012-09-06	0	\N
00000036	02	00000002	\N	F	\N	2012-09-07	0	\N
00000036	02	00000002	\N	F	\N	2012-09-08	0	\N
00000036	02	00000002	\N	D	\N	2012-09-09	0	\N
00000036	02	00000002	\N	D	\N	2012-09-10	0	\N
00000036	02	00000002	\N	D	\N	2012-09-11	0	\N
00000036	02	00000002	\N	D	\N	2012-09-12	0	\N
00000036	02	00000002	\N	D	\N	2012-09-13	0	\N
00000036	02	00000002	\N	F	\N	2012-09-14	0	\N
00000036	02	00000002	\N	F	\N	2012-09-15	0	\N
00000036	02	00000002	\N	D	\N	2012-09-16	0	\N
00000036	02	00000002	\N	D	\N	2012-09-17	0	\N
00000036	02	00000002	\N	D	\N	2012-09-18	0	\N
00000036	02	00000002	\N	D	\N	2012-09-19	0	\N
00000036	02	00000002	\N	D	\N	2012-09-20	0	\N
00000036	02	00000002	\N	F	\N	2012-09-21	0	\N
00000036	02	00000002	\N	F	\N	2012-09-22	0	\N
00000036	02	00000002	\N	D	\N	2012-09-23	0	\N
00000014	05	00000002	\N	D	\N	2012-10-31	1	\N
00000001	01	00000002	\N	D	\N	2012-11-06	1	\N
00000001	01	00000002	\N	D	\N	2012-11-01	1	\N
00000002	01	00000002	\N	D	\N	2012-11-06	1	\N
00000003	01	00000002	\N	D	\N	2012-11-06	1	\N
00000004	01	00000002	\N	D	\N	2012-11-06	1	\N
00000005	01	00000002	\N	D	\N	2012-11-06	1	\N
00000001	01	00000002	\N	D	\N	2012-11-26	1	\N
00000001	01	00000002	\N	D	\N	2012-11-27	1	\N
00000001	01	00000002	\N	D	\N	2012-11-28	1	\N
00000001	01	00000002	\N	D	\N	2012-11-29	1	\N
00000036	02	00000002	\N	D	\N	2012-09-24	0	\N
00000036	02	00000002	\N	D	\N	2012-09-25	0	\N
00000036	02	00000002	\N	D	\N	2012-09-26	0	\N
00000036	02	00000002	\N	D	\N	2012-09-27	0	\N
00000036	02	00000002	\N	F	\N	2012-09-28	0	\N
00000036	02	00000002	\N	F	\N	2012-09-29	0	\N
00000036	02	00000002	\N	D	\N	2012-09-30	0	\N
00000037	02	00000002	\N	F	\N	2012-09-01	0	\N
00000037	02	00000002	\N	D	\N	2012-09-02	0	\N
00000037	02	00000002	\N	D	\N	2012-09-03	0	\N
00000037	02	00000002	\N	D	\N	2012-09-04	0	\N
00000037	02	00000002	\N	D	\N	2012-09-05	0	\N
00000037	02	00000002	\N	D	\N	2012-09-06	0	\N
00000037	02	00000002	\N	F	\N	2012-09-07	0	\N
00000037	02	00000002	\N	F	\N	2012-09-08	0	\N
00000037	02	00000002	\N	D	\N	2012-09-09	0	\N
00000037	02	00000002	\N	D	\N	2012-09-10	0	\N
00000037	02	00000002	\N	D	\N	2012-09-11	0	\N
00000037	02	00000002	\N	D	\N	2012-09-12	0	\N
00000037	02	00000002	\N	D	\N	2012-09-13	0	\N
00000037	02	00000002	\N	F	\N	2012-09-14	0	\N
00000037	02	00000002	\N	F	\N	2012-09-15	0	\N
00000037	02	00000002	\N	D	\N	2012-09-16	0	\N
00000037	02	00000002	\N	D	\N	2012-09-17	0	\N
00000037	02	00000002	\N	D	\N	2012-09-18	0	\N
00000037	02	00000002	\N	D	\N	2012-09-19	0	\N
00000037	02	00000002	\N	D	\N	2012-09-20	0	\N
00000037	02	00000002	\N	F	\N	2012-09-21	0	\N
00000037	02	00000002	\N	F	\N	2012-09-22	0	\N
00000037	02	00000002	\N	D	\N	2012-09-23	0	\N
00000006	01	00000002	\N	D	\N	2012-11-06	1	\N
00000007	01	00000002	\N	D	\N	2012-11-06	1	\N
00000006	01	00000002	\N	F	\N	2012-11-03	1	\N
00000006	01	00000002	\N	D	\N	2012-11-04	1	\N
00000037	02	00000002	\N	D	\N	2012-09-24	0	\N
00000037	02	00000002	\N	D	\N	2012-09-25	0	\N
00000037	02	00000002	\N	D	\N	2012-09-26	0	\N
00000037	02	00000002	\N	D	\N	2012-09-27	0	\N
00000037	02	00000002	\N	F	\N	2012-09-28	0	\N
00000037	02	00000002	\N	F	\N	2012-09-29	0	\N
00000037	02	00000002	\N	D	\N	2012-09-30	0	\N
00000038	02	00000002	\N	F	\N	2012-09-01	0	\N
00000038	02	00000002	\N	D	\N	2012-09-02	0	\N
00000038	02	00000002	\N	D	\N	2012-09-03	0	\N
00000038	02	00000002	\N	D	\N	2012-09-04	0	\N
00000038	02	00000002	\N	D	\N	2012-09-05	0	\N
00000038	02	00000002	\N	D	\N	2012-09-06	0	\N
00000038	02	00000002	\N	F	\N	2012-09-07	0	\N
00000038	02	00000002	\N	F	\N	2012-09-08	0	\N
00000038	02	00000002	\N	D	\N	2012-09-09	0	\N
00000038	02	00000002	\N	D	\N	2012-09-10	0	\N
00000038	02	00000002	\N	D	\N	2012-09-11	0	\N
00000038	02	00000002	\N	D	\N	2012-09-12	0	\N
00000038	02	00000002	\N	D	\N	2012-09-13	0	\N
00000038	02	00000002	\N	F	\N	2012-09-14	0	\N
00000038	02	00000002	\N	F	\N	2012-09-15	0	\N
00000038	02	00000002	\N	D	\N	2012-09-16	0	\N
00000038	02	00000002	\N	D	\N	2012-09-17	0	\N
00000038	02	00000002	\N	D	\N	2012-09-18	0	\N
00000038	02	00000002	\N	D	\N	2012-09-19	0	\N
00000038	02	00000002	\N	D	\N	2012-09-20	0	\N
00000038	02	00000002	\N	F	\N	2012-09-21	0	\N
00000038	02	00000002	\N	F	\N	2012-09-22	0	\N
00000038	02	00000002	\N	D	\N	2012-09-23	0	\N
00000038	02	00000002	\N	D	\N	2012-09-24	0	\N
00000015	05	00000002	\N	D	\N	2012-10-01	1	\N
00000015	05	00000002	\N	D	\N	2012-10-02	1	\N
00000015	05	00000002	\N	D	\N	2012-10-03	1	\N
00000015	05	00000002	\N	D	\N	2012-10-04	1	\N
00000015	05	00000002	\N	F	\N	2012-10-05	1	\N
00000015	05	00000002	\N	F	\N	2012-10-06	1	\N
00000015	05	00000002	\N	D	\N	2012-10-07	1	\N
00000015	05	00000002	\N	D	\N	2012-10-08	1	\N
00000015	05	00000002	\N	D	\N	2012-10-09	1	\N
00000015	05	00000002	\N	D	\N	2012-10-10	1	\N
00000015	05	00000002	\N	D	\N	2012-10-11	1	\N
00000015	05	00000002	\N	F	\N	2012-10-12	1	\N
00000015	05	00000002	\N	F	\N	2012-10-13	1	\N
00000015	05	00000002	\N	D	\N	2012-10-14	1	\N
00000015	05	00000002	\N	D	\N	2012-10-15	1	\N
00000015	05	00000002	\N	D	\N	2012-10-16	1	\N
00000015	05	00000002	\N	D	\N	2012-10-17	1	\N
00000015	05	00000002	\N	D	\N	2012-10-18	1	\N
00000015	05	00000002	\N	F	\N	2012-10-19	1	\N
00000015	05	00000002	\N	F	\N	2012-10-20	1	\N
00000015	05	00000002	\N	D	\N	2012-10-21	1	\N
00000015	05	00000002	\N	D	\N	2012-10-22	1	\N
00000015	05	00000002	\N	D	\N	2012-10-23	1	\N
00000015	05	00000002	\N	D	\N	2012-10-24	1	\N
00000015	05	00000002	\N	D	\N	2012-10-25	1	\N
00000015	05	00000002	\N	F	\N	2012-10-26	1	\N
00000015	05	00000002	\N	F	\N	2012-10-27	1	\N
00000015	05	00000002	\N	D	\N	2012-10-28	1	\N
00000015	05	00000002	\N	D	\N	2012-10-29	1	\N
00000015	05	00000002	\N	D	\N	2012-10-30	1	\N
00000015	05	00000002	\N	D	\N	2012-10-31	1	\N
00000016	05	00000002	\N	D	\N	2012-10-01	1	\N
00000016	05	00000002	\N	D	\N	2012-10-02	1	\N
00000016	05	00000002	\N	D	\N	2012-10-03	1	\N
00000016	05	00000002	\N	D	\N	2012-10-04	1	\N
00000016	05	00000002	\N	F	\N	2012-10-05	1	\N
00000016	05	00000002	\N	F	\N	2012-10-06	1	\N
00000016	05	00000002	\N	D	\N	2012-10-07	1	\N
00000016	05	00000002	\N	D	\N	2012-10-08	1	\N
00000016	05	00000002	\N	D	\N	2012-10-09	1	\N
00000016	05	00000002	\N	D	\N	2012-10-10	1	\N
00000016	05	00000002	\N	D	\N	2012-10-11	1	\N
00000016	05	00000002	\N	F	\N	2012-10-12	1	\N
00000016	05	00000002	\N	F	\N	2012-10-13	1	\N
00000016	05	00000002	\N	D	\N	2012-10-14	1	\N
00000016	05	00000002	\N	D	\N	2012-10-15	1	\N
00000016	05	00000002	\N	D	\N	2012-10-16	1	\N
00000016	05	00000002	\N	D	\N	2012-10-17	1	\N
00000016	05	00000002	\N	D	\N	2012-10-18	1	\N
00000016	05	00000002	\N	F	\N	2012-10-19	1	\N
00000016	05	00000002	\N	F	\N	2012-10-20	1	\N
00000016	05	00000002	\N	D	\N	2012-10-21	1	\N
00000016	05	00000002	\N	D	\N	2012-10-22	1	\N
00000016	05	00000002	\N	D	\N	2012-10-23	1	\N
00000016	05	00000002	\N	D	\N	2012-10-24	1	\N
00000016	05	00000002	\N	D	\N	2012-10-25	1	\N
00000016	05	00000002	\N	F	\N	2012-10-26	1	\N
00000016	05	00000002	\N	F	\N	2012-10-27	1	\N
00000016	05	00000002	\N	D	\N	2012-10-28	1	\N
00000016	05	00000002	\N	D	\N	2012-10-29	1	\N
00000016	05	00000002	\N	D	\N	2012-10-30	1	\N
00000016	05	00000002	\N	D	\N	2012-10-31	1	\N
00000013	05	00000002	\N	D	\N	2012-11-01	1	\N
00000013	05	00000002	\N	F	\N	2012-11-02	1	\N
00000013	05	00000002	\N	F	\N	2012-11-03	1	\N
00000013	05	00000002	\N	D	\N	2012-11-04	1	\N
00000013	05	00000002	\N	D	\N	2012-11-06	1	\N
00000013	05	00000002	\N	D	\N	2012-11-07	1	\N
00000013	05	00000002	\N	D	\N	2012-11-08	1	\N
00000013	05	00000002	\N	F	\N	2012-11-09	1	\N
00000013	05	00000002	\N	F	\N	2012-11-10	1	\N
00000013	05	00000002	\N	D	\N	2012-11-11	1	\N
00000013	05	00000002	\N	D	\N	2012-11-12	1	\N
00000013	05	00000002	\N	D	\N	2012-11-13	1	\N
00000013	05	00000002	\N	D	0000000015	2012-11-21	R	\N
00000013	05	00000002	\N	D	\N	2012-11-05	1	\N
00000038	02	00000002	\N	D	\N	2012-09-25	0	\N
00000013	05	00000002	\N	F	\N	2012-11-17	1	\N
00000013	05	00000002	\N	D	\N	2012-11-18	1	\N
00000013	05	00000002	\N	D	\N	2012-11-19	1	\N
00000013	05	00000002	\N	D	\N	2012-11-20	1	\N
00000013	05	00000002	\N	D	\N	2012-11-22	1	\N
00000013	05	00000002	\N	F	\N	2012-11-23	1	\N
00000013	05	00000002	\N	F	\N	2012-11-24	1	\N
00000013	05	00000002	\N	D	\N	2012-11-25	1	\N
00000013	05	00000002	\N	D	\N	2012-11-26	1	\N
00000013	05	00000002	\N	D	\N	2012-11-27	1	\N
00000013	05	00000002	\N	D	\N	2012-11-28	1	\N
00000013	05	00000002	\N	D	\N	2012-11-29	1	\N
00000013	05	00000002	\N	F	\N	2012-11-30	1	\N
00000014	05	00000002	\N	D	\N	2012-11-01	1	\N
00000014	05	00000002	\N	F	\N	2012-11-02	1	\N
00000038	02	00000002	\N	D	\N	2012-09-26	0	\N
00000038	02	00000002	\N	D	\N	2012-09-27	0	\N
00000038	02	00000002	\N	F	\N	2012-09-28	0	\N
00000038	02	00000002	\N	F	\N	2012-09-29	0	\N
00000038	02	00000002	\N	D	\N	2012-09-30	0	\N
00000039	02	00000002	\N	F	\N	2012-09-01	0	\N
00000039	02	00000002	\N	D	\N	2012-09-02	0	\N
00000039	02	00000002	\N	D	\N	2012-09-03	0	\N
00000039	02	00000002	\N	D	\N	2012-09-04	0	\N
00000039	02	00000002	\N	D	\N	2012-09-05	0	\N
00000039	02	00000002	\N	D	\N	2012-09-06	0	\N
00000039	02	00000002	\N	F	\N	2012-09-07	0	\N
00000039	02	00000002	\N	F	\N	2012-09-08	0	\N
00000039	02	00000002	\N	D	\N	2012-09-09	0	\N
00000039	02	00000002	\N	D	\N	2012-09-10	0	\N
00000039	02	00000002	\N	D	\N	2012-09-11	0	\N
00000039	02	00000002	\N	D	\N	2012-09-12	0	\N
00000039	02	00000002	\N	D	\N	2012-09-13	0	\N
00000039	02	00000002	\N	F	\N	2012-09-14	0	\N
00000039	02	00000002	\N	F	\N	2012-09-15	0	\N
00000039	02	00000002	\N	D	\N	2012-09-16	0	\N
00000039	02	00000002	\N	D	\N	2012-09-17	0	\N
00000039	02	00000002	\N	D	\N	2012-09-18	0	\N
00000039	02	00000002	\N	D	\N	2012-09-19	0	\N
00000039	02	00000002	\N	D	\N	2012-09-20	0	\N
00000015	05	00000002	\N	D	\N	2012-11-01	1	\N
00000015	05	00000002	\N	F	\N	2012-11-02	1	\N
00000039	02	00000002	\N	F	\N	2012-09-21	0	\N
00000039	02	00000002	\N	F	\N	2012-09-22	0	\N
00000039	02	00000002	\N	D	\N	2012-09-23	0	\N
00000039	02	00000002	\N	D	\N	2012-09-24	0	\N
00000039	02	00000002	\N	D	\N	2012-09-25	0	\N
00000039	02	00000002	\N	D	\N	2012-09-26	0	\N
00000039	02	00000002	\N	D	\N	2012-09-27	0	\N
00000039	02	00000002	\N	F	\N	2012-09-28	0	\N
00000039	02	00000002	\N	F	\N	2012-09-29	0	\N
00000039	02	00000002	\N	D	\N	2012-09-30	0	\N
00000040	02	00000002	\N	F	\N	2012-09-01	0	\N
00000040	02	00000002	\N	D	\N	2012-09-02	0	\N
00000040	02	00000002	\N	D	\N	2012-09-03	0	\N
00000040	02	00000002	\N	D	\N	2012-09-04	0	\N
00000040	02	00000002	\N	D	\N	2012-09-05	0	\N
00000040	02	00000002	\N	D	\N	2012-09-06	0	\N
00000040	02	00000002	\N	F	\N	2012-09-07	0	\N
00000040	02	00000002	\N	F	\N	2012-09-08	0	\N
00000040	02	00000002	\N	D	\N	2012-09-09	0	\N
00000040	02	00000002	\N	D	\N	2012-09-10	0	\N
00000040	02	00000002	\N	D	\N	2012-09-11	0	\N
00000040	02	00000002	\N	D	\N	2012-09-12	0	\N
00000040	02	00000002	\N	D	\N	2012-09-13	0	\N
00000040	02	00000002	\N	F	\N	2012-09-14	0	\N
00000040	02	00000002	\N	F	\N	2012-09-15	0	\N
00000040	02	00000002	\N	D	\N	2012-09-16	0	\N
00000040	02	00000002	\N	D	\N	2012-09-17	0	\N
00000040	02	00000002	\N	D	\N	2012-09-18	0	\N
00000016	05	00000002	\N	D	\N	2012-11-01	1	\N
00000016	05	00000002	\N	F	\N	2012-11-02	1	\N
00000040	02	00000002	\N	D	\N	2012-09-19	0	\N
00000040	02	00000002	\N	D	\N	2012-09-20	0	\N
00000040	02	00000002	\N	F	\N	2012-09-21	0	\N
00000040	02	00000002	\N	F	\N	2012-09-22	0	\N
00000040	02	00000002	\N	D	\N	2012-09-23	0	\N
00000040	02	00000002	\N	D	\N	2012-09-24	0	\N
00000040	02	00000002	\N	D	\N	2012-09-25	0	\N
00000040	02	00000002	\N	D	\N	2012-09-26	0	\N
00000040	02	00000002	\N	D	\N	2012-09-27	0	\N
00000040	02	00000002	\N	F	\N	2012-09-28	0	\N
00000040	02	00000002	\N	F	\N	2012-09-29	0	\N
00000040	02	00000002	\N	D	\N	2012-09-30	0	\N
00000041	02	00000002	\N	F	\N	2012-09-01	0	\N
00000041	02	00000002	\N	D	\N	2012-09-02	0	\N
00000041	02	00000002	\N	D	\N	2012-09-03	0	\N
00000041	02	00000002	\N	D	\N	2012-09-04	0	\N
00000041	02	00000002	\N	D	\N	2012-09-05	0	\N
00000041	02	00000002	\N	D	\N	2012-09-06	0	\N
00000001	01	00000002	\N	F	\N	2013-02-01	1	\N
00000001	01	00000002	\N	F	\N	2013-02-02	1	\N
00000014	05	00000002	\N	F	\N	2012-11-03	1	\N
00000014	05	00000002	\N	D	\N	2012-11-04	1	\N
00000014	05	00000002	\N	D	\N	2012-11-05	1	\N
00000013	05	00000002	\N	F	\N	2012-12-21	1	\N
00000013	05	00000002	\N	F	\N	2012-12-22	1	\N
00000013	05	00000002	\N	D	\N	2012-12-23	1	\N
00000013	05	00000002	\N	D	\N	2012-12-24	1	\N
00000013	05	00000002	\N	D	\N	2012-12-25	1	\N
00000013	05	00000002	\N	D	\N	2012-12-26	1	\N
00000013	05	00000002	\N	D	\N	2012-12-27	1	\N
00000013	05	00000002	\N	F	\N	2012-12-28	1	\N
00000013	05	00000002	\N	F	\N	2012-12-29	1	\N
00000013	05	00000002	\N	D	\N	2012-12-30	1	\N
00000013	05	00000002	\N	D	\N	2012-12-31	1	\N
00000014	05	00000002	\N	F	\N	2012-12-01	1	\N
00000014	05	00000002	\N	D	\N	2012-12-02	1	\N
00000014	05	00000002	\N	D	\N	2012-12-03	1	\N
00000014	05	00000002	\N	D	\N	2012-12-04	1	\N
00000014	05	00000002	\N	D	\N	2012-12-05	1	\N
00000014	05	00000002	\N	D	\N	2012-12-06	1	\N
00000014	05	00000002	\N	F	\N	2012-12-07	1	\N
00000014	05	00000002	\N	F	\N	2012-12-08	1	\N
00000014	05	00000002	\N	D	\N	2012-12-09	1	\N
00000014	05	00000002	\N	D	\N	2012-12-10	1	\N
00000014	05	00000002	\N	D	\N	2012-12-11	1	\N
00000014	05	00000002	\N	D	\N	2012-12-12	1	\N
00000014	05	00000002	\N	D	\N	2012-12-13	1	\N
00000014	05	00000002	\N	F	\N	2012-12-14	1	\N
00000014	05	00000002	\N	F	\N	2012-12-15	1	\N
00000014	05	00000002	\N	D	\N	2012-12-16	1	\N
00000014	05	00000002	\N	D	\N	2012-12-17	1	\N
00000014	05	00000002	\N	D	\N	2012-12-18	1	\N
00000014	05	00000002	\N	D	\N	2012-12-19	1	\N
00000014	05	00000002	\N	D	\N	2012-12-20	1	\N
00000014	05	00000002	\N	F	\N	2012-12-21	1	\N
00000014	05	00000002	\N	F	\N	2012-12-22	1	\N
00000014	05	00000002	\N	D	\N	2012-12-23	1	\N
00000014	05	00000002	\N	D	\N	2012-12-24	1	\N
00000014	05	00000002	\N	D	\N	2012-12-25	1	\N
00000014	05	00000002	\N	D	\N	2012-12-26	1	\N
00000014	05	00000002	\N	D	\N	2012-12-27	1	\N
00000014	05	00000002	\N	F	\N	2012-12-28	1	\N
00000014	05	00000002	\N	F	\N	2012-12-29	1	\N
00000014	05	00000002	\N	D	\N	2012-12-30	1	\N
00000014	05	00000002	\N	D	\N	2012-12-31	1	\N
00000015	05	00000002	\N	F	\N	2012-12-01	1	\N
00000015	05	00000002	\N	D	\N	2012-12-02	1	\N
00000015	05	00000002	\N	D	\N	2012-12-03	1	\N
00000015	05	00000002	\N	D	\N	2012-12-04	1	\N
00000041	02	00000002	\N	F	\N	2012-09-07	0	\N
00000041	02	00000002	\N	F	\N	2012-09-08	0	\N
00000041	02	00000002	\N	D	\N	2012-09-09	0	\N
00000041	02	00000002	\N	D	\N	2012-09-10	0	\N
00000041	02	00000002	\N	D	\N	2012-09-11	0	\N
00000041	02	00000002	\N	D	\N	2012-09-12	0	\N
00000041	02	00000002	\N	D	\N	2012-09-13	0	\N
00000041	02	00000002	\N	F	\N	2012-09-14	0	\N
00000041	02	00000002	\N	F	\N	2012-09-15	0	\N
00000041	02	00000002	\N	D	\N	2012-09-16	0	\N
00000016	05	00000002	\N	D	\N	2012-11-21	1	\N
00000016	05	00000002	\N	D	\N	2012-11-22	1	\N
00000016	05	00000002	\N	F	\N	2012-11-23	1	\N
00000016	05	00000002	\N	F	\N	2012-11-24	1	\N
00000016	05	00000002	\N	D	\N	2012-11-25	1	\N
00000016	05	00000002	\N	D	\N	2012-11-26	1	\N
00000015	05	00000002	\N	D	\N	2012-12-05	1	\N
00000015	05	00000002	\N	D	\N	2012-12-06	1	\N
00000015	05	00000002	\N	F	\N	2012-12-07	1	\N
00000015	05	00000002	\N	F	\N	2012-12-08	1	\N
00000015	05	00000002	\N	D	\N	2012-12-09	1	\N
00000015	05	00000002	\N	D	\N	2012-12-10	1	\N
00000015	05	00000002	\N	D	\N	2012-12-11	1	\N
00000015	05	00000002	\N	D	\N	2012-12-12	1	\N
00000015	05	00000002	\N	D	\N	2012-12-13	1	\N
00000015	05	00000002	\N	F	\N	2012-12-14	1	\N
00000015	05	00000002	\N	F	\N	2012-12-15	1	\N
00000015	05	00000002	\N	D	\N	2012-12-16	1	\N
00000015	05	00000002	\N	D	\N	2012-12-17	1	\N
00000015	05	00000002	\N	D	\N	2012-12-18	1	\N
00000041	02	00000002	\N	D	\N	2012-09-17	0	\N
00000041	02	00000002	\N	D	\N	2012-09-18	0	\N
00000041	02	00000002	\N	D	\N	2012-09-19	0	\N
00000015	05	00000002	\N	D	\N	2012-12-19	1	\N
00000041	02	00000002	\N	D	\N	2012-09-20	0	\N
00000041	02	00000002	\N	F	\N	2012-09-21	0	\N
00000041	02	00000002	\N	F	\N	2012-09-22	0	\N
00000041	02	00000002	\N	D	\N	2012-09-23	0	\N
00000041	02	00000002	\N	D	\N	2012-09-24	0	\N
00000015	05	00000002	\N	D	\N	2012-12-20	1	\N
00000015	05	00000002	\N	F	\N	2012-12-21	1	\N
00000016	05	00000002	\N	D	\N	2012-11-27	1	\N
00000016	05	00000002	\N	D	\N	2012-11-28	1	\N
00000013	05	00000002	\N	F	\N	2012-12-01	1	\N
00000013	05	00000002	\N	D	\N	2012-12-02	1	\N
00000013	05	00000002	\N	D	\N	2012-12-03	1	\N
00000013	05	00000002	\N	D	\N	2012-12-04	1	\N
00000013	05	00000002	\N	D	\N	2012-12-06	1	\N
00000013	05	00000002	\N	F	\N	2012-12-07	1	\N
00000013	05	00000002	\N	F	\N	2012-12-08	1	\N
00000013	05	00000002	\N	D	\N	2012-12-09	1	\N
00000013	05	00000002	\N	D	\N	2012-12-10	1	\N
00000013	05	00000002	\N	D	\N	2012-12-11	1	\N
00000013	05	00000002	\N	D	\N	2012-12-12	1	\N
00000013	05	00000002	\N	D	\N	2012-12-13	1	\N
00000013	05	00000002	\N	F	\N	2012-12-14	1	\N
00000013	05	00000002	\N	F	\N	2012-12-15	1	\N
00000013	05	00000002	\N	D	\N	2012-12-16	1	\N
00000013	05	00000002	\N	D	\N	2012-12-17	1	\N
00000013	05	00000002	\N	D	\N	2012-12-18	1	\N
00000013	05	00000002	\N	D	\N	2012-12-19	1	\N
00000013	05	00000002	\N	D	\N	2012-12-20	1	\N
00000015	05	00000002	\N	F	\N	2012-12-22	1	\N
00000015	05	00000002	\N	D	\N	2012-12-23	1	\N
00000015	05	00000002	\N	D	\N	2012-12-24	1	\N
00000015	05	00000002	\N	D	\N	2012-12-25	1	\N
00000015	05	00000002	\N	D	\N	2012-12-26	1	\N
00000015	05	00000002	\N	D	\N	2012-12-27	1	\N
00000015	05	00000002	\N	F	\N	2012-12-28	1	\N
00000015	05	00000002	\N	F	\N	2012-12-29	1	\N
00000015	05	00000002	\N	D	\N	2012-12-30	1	\N
00000015	05	00000002	\N	D	\N	2012-12-31	1	\N
00000016	05	00000002	\N	F	\N	2012-12-01	1	\N
00000016	05	00000002	\N	D	\N	2012-12-02	1	\N
00000016	05	00000002	\N	D	\N	2012-12-03	1	\N
00000016	05	00000002	\N	D	\N	2012-11-29	1	\N
00000016	05	00000002	\N	F	\N	2012-11-30	1	\N
00000016	05	00000002	\N	D	\N	2012-12-04	1	\N
00000016	05	00000002	\N	D	\N	2012-12-05	1	\N
00000016	05	00000002	\N	D	\N	2012-12-06	1	\N
00000016	05	00000002	\N	F	\N	2012-12-07	1	\N
00000016	05	00000002	\N	F	\N	2012-12-08	1	\N
00000016	05	00000002	\N	D	\N	2012-12-09	1	\N
00000016	05	00000002	\N	D	\N	2012-12-10	1	\N
00000016	05	00000002	\N	D	\N	2012-12-11	1	\N
00000016	05	00000002	\N	D	\N	2012-12-12	1	\N
00000016	05	00000002	\N	D	\N	2012-12-13	1	\N
00000016	05	00000002	\N	F	\N	2012-12-14	1	\N
00000016	05	00000002	\N	F	\N	2012-12-15	1	\N
00000016	05	00000002	\N	D	\N	2012-12-16	1	\N
00000016	05	00000002	\N	D	\N	2012-12-17	1	\N
00000016	05	00000002	\N	D	\N	2012-12-18	1	\N
00000016	05	00000002	\N	D	\N	2012-12-19	1	\N
00000016	05	00000002	\N	D	\N	2012-12-20	1	\N
00000016	05	00000002	\N	F	\N	2012-12-21	1	\N
00000016	05	00000002	\N	F	\N	2012-12-22	1	\N
00000016	05	00000002	\N	D	\N	2012-12-23	1	\N
00000016	05	00000002	\N	D	\N	2012-12-24	1	\N
00000016	05	00000002	\N	D	\N	2012-12-25	1	\N
00000016	05	00000002	\N	D	\N	2012-12-26	1	\N
00000016	05	00000002	\N	D	\N	2012-12-27	1	\N
00000016	05	00000002	\N	F	\N	2012-12-28	1	\N
00000016	05	00000002	\N	F	\N	2012-12-29	1	\N
00000016	05	00000002	\N	D	\N	2012-12-30	1	\N
00000016	05	00000002	\N	D	\N	2012-12-31	1	\N
00000041	02	00000002	\N	D	\N	2012-09-25	0	\N
00000041	02	00000002	\N	D	\N	2012-09-26	0	\N
00000010	03	00000002	\N	D	\N	2012-10-01	1	\N
00000010	03	00000002	\N	D	\N	2012-10-02	1	\N
00000010	03	00000002	\N	D	\N	2012-10-03	1	\N
00000010	03	00000002	\N	D	\N	2012-10-04	1	\N
00000010	03	00000002	\N	F	\N	2012-10-05	1	\N
00000010	03	00000002	\N	F	\N	2012-10-06	1	\N
00000010	03	00000002	\N	D	\N	2012-10-07	1	\N
00000011	03	00000002	\N	D	\N	2012-10-21	1	\N
00000011	03	00000002	\N	D	\N	2012-10-22	1	\N
00000011	03	00000002	\N	D	\N	2012-10-23	1	\N
00000011	03	00000002	\N	D	\N	2012-10-24	1	\N
00000011	03	00000002	\N	D	\N	2012-10-25	1	\N
00000011	03	00000002	\N	F	\N	2012-10-26	1	\N
00000011	03	00000002	\N	F	\N	2012-10-27	1	\N
00000011	03	00000002	\N	D	\N	2012-10-28	1	\N
00000011	03	00000002	\N	D	\N	2012-10-29	1	\N
00000011	03	00000002	\N	D	\N	2012-10-30	1	\N
00000011	03	00000002	\N	D	\N	2012-10-31	1	\N
00000012	03	00000002	\N	D	\N	2012-10-01	1	\N
00000012	03	00000002	\N	D	\N	2012-10-02	1	\N
00000012	03	00000002	\N	D	\N	2012-10-03	1	\N
00000012	03	00000002	\N	D	\N	2012-10-04	1	\N
00000012	03	00000002	\N	F	\N	2012-10-05	1	\N
00000012	03	00000002	\N	F	\N	2012-10-06	1	\N
00000012	03	00000002	\N	D	\N	2012-10-07	1	\N
00000012	03	00000002	\N	D	\N	2012-10-08	1	\N
00000012	03	00000002	\N	D	\N	2012-10-09	1	\N
00000012	03	00000002	\N	D	\N	2012-10-10	1	\N
00000012	03	00000002	\N	D	\N	2012-10-11	1	\N
00000012	03	00000002	\N	F	\N	2012-10-12	1	\N
00000012	03	00000002	\N	F	\N	2012-10-13	1	\N
00000012	03	00000002	\N	D	\N	2012-10-14	1	\N
00000012	03	00000002	\N	D	\N	2012-10-15	1	\N
00000012	03	00000002	\N	D	\N	2012-10-16	1	\N
00000012	03	00000002	\N	D	\N	2012-10-17	1	\N
00000012	03	00000002	\N	D	\N	2012-10-18	1	\N
00000012	03	00000002	\N	F	\N	2012-10-19	1	\N
00000012	03	00000002	\N	F	\N	2012-10-20	1	\N
00000012	03	00000002	\N	D	\N	2012-10-21	1	\N
00000012	03	00000002	\N	D	\N	2012-10-22	1	\N
00000012	03	00000002	\N	D	\N	2012-10-23	1	\N
00000012	03	00000002	\N	D	\N	2012-10-24	1	\N
00000012	03	00000002	\N	D	\N	2012-10-25	1	\N
00000010	03	00000002	\N	D	0000000003	2012-11-06	R	\N
00000010	03	00000002	\N	D	0000000003	2012-11-07	R	\N
00000010	03	00000002	\N	D	0000000006	2012-11-21	R	\N
00000041	02	00000002	\N	D	\N	2012-09-27	0	\N
00000041	02	00000002	\N	F	\N	2012-09-28	0	\N
00000041	02	00000002	\N	F	\N	2012-09-29	0	\N
00000041	02	00000002	\N	D	\N	2012-09-30	0	\N
00000017	02	00000002	\N	D	\N	2012-10-01	0	\N
00000017	02	00000002	\N	D	\N	2012-10-02	0	\N
00000017	02	00000002	\N	D	\N	2012-10-03	0	\N
00000017	02	00000002	\N	D	\N	2012-10-04	0	\N
00000017	02	00000002	\N	F	\N	2012-10-05	0	\N
00000017	02	00000002	\N	F	\N	2012-10-06	0	\N
00000017	02	00000002	\N	D	\N	2012-10-07	0	\N
00000017	02	00000002	\N	D	\N	2012-10-08	0	\N
00000017	02	00000002	\N	D	\N	2012-10-09	0	\N
00000017	02	00000002	\N	D	\N	2012-10-10	0	\N
00000010	03	00000002	\N	D	\N	2012-10-08	1	\N
00000010	03	00000002	\N	D	\N	2012-10-09	1	\N
00000010	03	00000002	\N	D	\N	2012-10-10	1	\N
00000010	03	00000002	\N	D	\N	2012-10-11	1	\N
00000010	03	00000002	\N	F	\N	2012-10-12	1	\N
00000010	03	00000002	\N	F	\N	2012-10-13	1	\N
00000012	03	00000002	\N	F	\N	2012-10-26	1	\N
00000012	03	00000002	\N	F	\N	2012-10-27	1	\N
00000012	03	00000002	\N	D	\N	2012-10-28	1	\N
00000012	03	00000002	\N	D	\N	2012-10-29	1	\N
00000012	03	00000002	\N	D	\N	2012-10-30	1	\N
00000012	03	00000002	\N	D	\N	2012-10-31	1	\N
00000010	03	00000002	\N	D	\N	2012-11-01	1	\N
00000010	03	00000002	\N	F	\N	2012-11-02	1	\N
00000010	03	00000002	\N	F	\N	2012-11-03	1	\N
00000010	03	00000002	\N	D	\N	2012-11-04	1	\N
00000010	03	00000002	\N	D	\N	2012-11-05	1	\N
00000017	02	00000002	\N	D	\N	2012-10-11	0	\N
00000017	02	00000002	\N	F	\N	2012-10-12	0	\N
00000010	03	00000002	\N	D	\N	2012-11-08	1	\N
00000010	03	00000002	\N	F	\N	2012-11-09	1	\N
00000010	03	00000002	\N	D	\N	2012-10-14	1	\N
00000010	03	00000002	\N	D	\N	2012-10-15	1	\N
00000010	03	00000002	\N	D	\N	2012-10-16	1	\N
00000010	03	00000002	\N	D	\N	2012-10-17	1	\N
00000010	03	00000002	\N	D	\N	2012-10-18	1	\N
00000010	03	00000002	\N	F	\N	2012-10-19	1	\N
00000010	03	00000002	\N	F	\N	2012-10-20	1	\N
00000010	03	00000002	\N	D	\N	2012-10-21	1	\N
00000010	03	00000002	\N	D	\N	2012-10-22	1	\N
00000010	03	00000002	\N	D	\N	2012-10-23	1	\N
00000010	03	00000002	\N	D	\N	2012-10-24	1	\N
00000010	03	00000002	\N	D	\N	2012-10-25	1	\N
00000010	03	00000002	\N	F	\N	2012-10-26	1	\N
00000010	03	00000002	\N	F	\N	2012-10-27	1	\N
00000010	03	00000002	\N	D	\N	2012-10-28	1	\N
00000010	03	00000002	\N	D	\N	2012-10-29	1	\N
00000010	03	00000002	\N	D	\N	2012-10-30	1	\N
00000010	03	00000002	\N	D	\N	2012-10-31	1	\N
00000011	03	00000002	\N	D	\N	2012-10-01	1	\N
00000011	03	00000002	\N	D	\N	2012-10-02	1	\N
00000011	03	00000002	\N	D	\N	2012-10-03	1	\N
00000011	03	00000002	\N	D	\N	2012-10-04	1	\N
00000011	03	00000002	\N	F	\N	2012-10-05	1	\N
00000011	03	00000002	\N	F	\N	2012-10-06	1	\N
00000011	03	00000002	\N	D	\N	2012-10-07	1	\N
00000011	03	00000002	\N	D	\N	2012-10-08	1	\N
00000011	03	00000002	\N	D	\N	2012-10-09	1	\N
00000011	03	00000002	\N	D	\N	2012-10-10	1	\N
00000011	03	00000002	\N	D	\N	2012-10-11	1	\N
00000011	03	00000002	\N	F	\N	2012-10-12	1	\N
00000011	03	00000002	\N	F	\N	2012-10-13	1	\N
00000011	03	00000002	\N	D	\N	2012-10-14	1	\N
00000011	03	00000002	\N	D	\N	2012-10-15	1	\N
00000011	03	00000002	\N	D	\N	2012-10-16	1	\N
00000011	03	00000002	\N	D	\N	2012-10-17	1	\N
00000011	03	00000002	\N	D	\N	2012-10-18	1	\N
00000011	03	00000002	\N	F	\N	2012-10-19	1	\N
00000011	03	00000002	\N	F	\N	2012-10-20	1	\N
00000010	03	00000002	\N	F	\N	2012-11-10	1	\N
00000010	03	00000002	\N	D	\N	2012-11-11	1	\N
00000010	03	00000002	\N	D	\N	2012-11-12	1	\N
00000010	03	00000002	\N	D	\N	2012-11-13	1	\N
00000010	03	00000002	\N	D	\N	2012-11-14	1	\N
00000010	03	00000002	\N	D	\N	2012-11-15	1	\N
00000010	03	00000002	\N	F	\N	2012-11-16	1	\N
00000010	03	00000002	\N	F	\N	2012-11-17	1	\N
00000010	03	00000002	\N	D	\N	2012-11-18	1	\N
00000010	03	00000002	\N	D	\N	2012-11-19	1	\N
00000010	03	00000002	\N	D	\N	2012-11-20	1	\N
00000017	02	00000002	\N	F	\N	2012-10-13	0	\N
00000017	02	00000002	\N	D	\N	2012-10-14	0	\N
00000017	02	00000002	\N	D	\N	2012-10-15	0	\N
00000017	02	00000002	\N	D	\N	2012-10-16	0	\N
00000017	02	00000002	\N	D	\N	2012-10-17	0	\N
00000017	02	00000002	\N	D	\N	2012-10-18	0	\N
00000017	02	00000002	\N	F	\N	2012-10-19	0	\N
00000017	02	00000002	\N	F	\N	2012-10-20	0	\N
00000017	02	00000002	\N	D	\N	2012-10-21	0	\N
00000017	02	00000002	\N	D	\N	2012-10-22	0	\N
00000017	02	00000002	\N	D	\N	2012-10-23	0	\N
00000017	02	00000002	\N	D	\N	2012-10-24	0	\N
00000017	02	00000002	\N	D	\N	2012-10-25	0	\N
00000017	02	00000002	\N	F	\N	2012-10-26	0	\N
00000017	02	00000002	\N	F	\N	2012-10-27	0	\N
00000017	02	00000002	\N	D	\N	2012-10-28	0	\N
00000017	02	00000002	\N	D	\N	2012-10-29	0	\N
00000017	02	00000002	\N	D	\N	2012-10-30	0	\N
00000017	02	00000002	\N	D	\N	2012-10-31	0	\N
00000018	02	00000002	\N	D	\N	2012-10-01	0	\N
00000018	02	00000002	\N	D	\N	2012-10-02	0	\N
00000018	02	00000002	\N	D	\N	2012-10-03	0	\N
00000018	02	00000002	\N	D	\N	2012-10-04	0	\N
00000018	02	00000002	\N	F	\N	2012-10-05	0	\N
00000018	02	00000002	\N	F	\N	2012-10-06	0	\N
00000018	02	00000002	\N	D	\N	2012-10-07	0	\N
00000018	02	00000002	\N	D	\N	2012-10-08	0	\N
00000018	02	00000002	\N	D	\N	2012-10-09	0	\N
00000018	02	00000002	\N	D	\N	2012-10-10	0	\N
00000018	02	00000002	\N	D	\N	2012-10-11	0	\N
00000018	02	00000002	\N	F	\N	2012-10-12	0	\N
00000018	02	00000002	\N	F	\N	2012-10-13	0	\N
00000018	02	00000002	\N	D	\N	2012-10-14	0	\N
00000018	02	00000002	\N	D	\N	2012-10-15	0	\N
00000018	02	00000002	\N	D	\N	2012-10-16	0	\N
00000018	02	00000002	\N	D	\N	2012-10-17	0	\N
00000018	02	00000002	\N	D	\N	2012-10-18	0	\N
00000018	02	00000002	\N	F	\N	2012-10-19	0	\N
00000018	02	00000002	\N	F	\N	2012-10-20	0	\N
00000018	02	00000002	\N	D	\N	2012-10-21	0	\N
00000018	02	00000002	\N	D	\N	2012-10-22	0	\N
00000018	02	00000002	\N	D	\N	2012-10-23	0	\N
00000018	02	00000002	\N	D	\N	2012-10-24	0	\N
00000018	02	00000002	\N	D	\N	2012-10-25	0	\N
00000018	02	00000002	\N	F	\N	2012-10-26	0	\N
00000018	02	00000002	\N	F	\N	2012-10-27	0	\N
00000018	02	00000002	\N	D	\N	2012-10-28	0	\N
00000018	02	00000002	\N	D	\N	2012-10-29	0	\N
00000018	02	00000002	\N	D	\N	2012-10-30	0	\N
00000018	02	00000002	\N	D	\N	2012-10-31	0	\N
00000019	02	00000002	\N	D	\N	2012-10-01	0	\N
00000019	02	00000002	\N	D	\N	2012-10-02	0	\N
00000019	02	00000002	\N	D	\N	2012-10-03	0	\N
00000019	02	00000002	\N	D	\N	2012-10-04	0	\N
00000019	02	00000002	\N	F	\N	2012-10-05	0	\N
00000019	02	00000002	\N	F	\N	2012-10-06	0	\N
00000019	02	00000002	\N	D	\N	2012-10-07	0	\N
00000019	02	00000002	\N	D	\N	2012-10-08	0	\N
00000019	02	00000002	\N	D	\N	2012-10-09	0	\N
00000019	02	00000002	\N	D	\N	2012-10-10	0	\N
00000019	02	00000002	\N	D	\N	2012-10-11	0	\N
00000019	02	00000002	\N	F	\N	2012-10-12	0	\N
00000019	02	00000002	\N	F	\N	2012-10-13	0	\N
00000019	02	00000002	\N	D	\N	2012-10-14	0	\N
00000019	02	00000002	\N	D	\N	2012-10-15	0	\N
00000019	02	00000002	\N	D	\N	2012-10-16	0	\N
00000019	02	00000002	\N	D	\N	2012-10-17	0	\N
00000019	02	00000002	\N	D	\N	2012-10-18	0	\N
00000019	02	00000002	\N	F	\N	2012-10-19	0	\N
00000019	02	00000002	\N	F	\N	2012-10-20	0	\N
00000019	02	00000002	\N	D	\N	2012-10-21	0	\N
00000019	02	00000002	\N	D	\N	2012-10-22	0	\N
00000019	02	00000002	\N	D	\N	2012-10-23	0	\N
00000019	02	00000002	\N	D	\N	2012-10-24	0	\N
00000019	02	00000002	\N	D	\N	2012-10-25	0	\N
00000019	02	00000002	\N	F	\N	2012-10-26	0	\N
00000019	02	00000002	\N	F	\N	2012-10-27	0	\N
00000019	02	00000002	\N	D	\N	2012-10-28	0	\N
00000019	02	00000002	\N	D	\N	2012-10-29	0	\N
00000019	02	00000002	\N	D	\N	2012-10-30	0	\N
00000019	02	00000002	\N	D	\N	2012-10-31	0	\N
00000020	02	00000002	\N	D	\N	2012-10-01	0	\N
00000020	02	00000002	\N	D	\N	2012-10-02	0	\N
00000020	02	00000002	\N	D	\N	2012-10-03	0	\N
00000020	02	00000002	\N	D	\N	2012-10-04	0	\N
00000020	02	00000002	\N	F	\N	2012-10-05	0	\N
00000020	02	00000002	\N	F	\N	2012-10-06	0	\N
00000020	02	00000002	\N	D	\N	2012-10-07	0	\N
00000020	02	00000002	\N	D	\N	2012-10-08	0	\N
00000020	02	00000002	\N	D	\N	2012-10-09	0	\N
00000020	02	00000002	\N	D	\N	2012-10-10	0	\N
00000020	02	00000002	\N	D	\N	2012-10-11	0	\N
00000020	02	00000002	\N	F	\N	2012-10-12	0	\N
00000020	02	00000002	\N	F	\N	2012-10-13	0	\N
00000020	02	00000002	\N	D	\N	2012-10-14	0	\N
00000020	02	00000002	\N	D	\N	2012-10-15	0	\N
00000020	02	00000002	\N	D	\N	2012-10-16	0	\N
00000020	02	00000002	\N	D	\N	2012-10-17	0	\N
00000020	02	00000002	\N	D	\N	2012-10-18	0	\N
00000020	02	00000002	\N	F	\N	2012-10-19	0	\N
00000020	02	00000002	\N	F	\N	2012-10-20	0	\N
00000020	02	00000002	\N	D	\N	2012-10-21	0	\N
00000020	02	00000002	\N	D	\N	2012-10-22	0	\N
00000020	02	00000002	\N	D	\N	2012-10-23	0	\N
00000020	02	00000002	\N	D	\N	2012-10-24	0	\N
00000020	02	00000002	\N	D	\N	2012-10-25	0	\N
00000020	02	00000002	\N	F	\N	2012-10-26	0	\N
00000020	02	00000002	\N	F	\N	2012-10-27	0	\N
00000020	02	00000002	\N	D	\N	2012-10-28	0	\N
00000020	02	00000002	\N	D	\N	2012-10-29	0	\N
00000020	02	00000002	\N	D	\N	2012-10-30	0	\N
00000020	02	00000002	\N	D	\N	2012-10-31	0	\N
00000021	02	00000002	\N	D	\N	2012-10-01	0	\N
00000021	02	00000002	\N	D	\N	2012-10-02	0	\N
00000021	02	00000002	\N	D	\N	2012-10-03	0	\N
00000021	02	00000002	\N	D	\N	2012-10-04	0	\N
00000021	02	00000002	\N	F	\N	2012-10-05	0	\N
00000021	02	00000002	\N	F	\N	2012-10-06	0	\N
00000021	02	00000002	\N	D	\N	2012-10-07	0	\N
00000021	02	00000002	\N	D	\N	2012-10-08	0	\N
00000021	02	00000002	\N	D	\N	2012-10-09	0	\N
00000004	01	00000002	\N	D	\N	2012-10-12	1	\N
00000004	01	00000002	\N	D	\N	2012-10-15	1	\N
00000004	01	00000002	\N	D	\N	2012-10-13	1	\N
00000004	01	00000002	\N	D	\N	2012-10-14	1	\N
00000021	02	00000002	\N	D	\N	2012-10-10	0	\N
00000021	02	00000002	\N	D	\N	2012-10-11	0	\N
00000021	02	00000002	\N	F	\N	2012-10-12	0	\N
00000021	02	00000002	\N	F	\N	2012-10-13	0	\N
00000021	02	00000002	\N	D	\N	2012-10-14	0	\N
00000021	02	00000002	\N	D	\N	2012-10-15	0	\N
00000021	02	00000002	\N	D	\N	2012-10-16	0	\N
00000021	02	00000002	\N	D	\N	2012-10-17	0	\N
00000021	02	00000002	\N	D	\N	2012-10-18	0	\N
00000021	02	00000002	\N	F	\N	2012-10-19	0	\N
00000021	02	00000002	\N	F	\N	2012-10-20	0	\N
00000021	02	00000002	\N	D	\N	2012-10-21	0	\N
00000021	02	00000002	\N	D	\N	2012-10-22	0	\N
00000021	02	00000002	\N	D	\N	2012-10-23	0	\N
00000021	02	00000002	\N	D	\N	2012-10-24	0	\N
00000021	02	00000002	\N	D	\N	2012-10-25	0	\N
00000021	02	00000002	\N	F	\N	2012-10-26	0	\N
00000011	03	00000002	\N	D	\N	2012-11-05	1	\N
00000011	03	00000002	\N	D	\N	2012-11-06	1	\N
00000011	03	00000002	\N	D	\N	2012-11-07	1	\N
00000011	03	00000002	\N	D	\N	2012-11-08	1	\N
00000011	03	00000002	\N	F	\N	2012-11-09	1	\N
00000011	03	00000002	\N	F	\N	2012-11-10	1	\N
00000011	03	00000002	\N	D	\N	2012-11-11	1	\N
00000011	03	00000002	\N	D	\N	2012-11-12	1	\N
00000011	03	00000002	\N	D	\N	2012-11-13	1	\N
00000011	03	00000002	\N	D	\N	2012-11-14	1	\N
00000011	03	00000002	\N	D	\N	2012-11-15	1	\N
00000011	03	00000002	\N	F	\N	2012-11-16	1	\N
00000011	03	00000002	\N	F	\N	2012-11-17	1	\N
00000011	03	00000002	\N	D	\N	2012-11-18	1	\N
00000011	03	00000002	\N	D	\N	2012-11-19	1	\N
00000011	03	00000002	\N	D	\N	2012-11-20	1	\N
00000011	03	00000002	\N	D	\N	2012-11-21	1	\N
00000011	03	00000002	\N	D	\N	2012-11-22	1	\N
00000011	03	00000002	\N	F	\N	2012-11-23	1	\N
00000011	03	00000002	\N	F	\N	2012-11-24	1	\N
00000011	03	00000002	\N	D	\N	2012-11-25	1	\N
00000011	03	00000002	\N	D	\N	2012-11-26	1	\N
00000011	03	00000002	\N	D	\N	2012-11-27	1	\N
00000011	03	00000002	\N	D	\N	2012-11-28	1	\N
00000011	03	00000002	\N	D	\N	2012-11-29	1	\N
00000011	03	00000002	\N	F	\N	2012-11-30	1	\N
00000012	03	00000002	\N	D	\N	2012-11-01	1	\N
00000012	03	00000002	\N	F	\N	2012-11-02	1	\N
00000012	03	00000002	\N	F	\N	2012-11-03	1	\N
00000012	03	00000002	\N	D	\N	2012-11-04	1	\N
00000012	03	00000002	\N	D	\N	2012-11-05	1	\N
00000012	03	00000002	\N	D	\N	2012-11-06	1	\N
00000012	03	00000002	\N	D	\N	2012-11-07	1	\N
00000012	03	00000002	\N	D	\N	2012-11-08	1	\N
00000012	03	00000002	\N	F	\N	2012-11-09	1	\N
00000012	03	00000002	\N	F	\N	2012-11-10	1	\N
00000012	03	00000002	\N	D	\N	2012-11-11	1	\N
00000012	03	00000002	\N	D	\N	2012-11-12	1	\N
00000012	03	00000002	\N	D	\N	2012-11-13	1	\N
00000012	03	00000002	\N	D	\N	2012-11-14	1	\N
00000012	03	00000002	\N	D	\N	2012-11-15	1	\N
00000012	03	00000002	\N	F	\N	2012-11-16	1	\N
00000012	03	00000002	\N	F	\N	2012-11-17	1	\N
00000012	03	00000002	\N	D	\N	2012-11-18	1	\N
00000012	03	00000002	\N	D	\N	2012-11-19	1	\N
00000012	03	00000002	\N	D	\N	2012-11-20	1	\N
00000012	03	00000002	\N	D	\N	2012-11-21	1	\N
00000012	03	00000002	\N	D	\N	2012-11-22	1	\N
00000012	03	00000002	\N	F	\N	2012-11-23	1	\N
00000012	03	00000002	\N	F	\N	2012-11-24	1	\N
00000012	03	00000002	\N	D	\N	2012-11-25	1	\N
00000012	03	00000002	\N	D	\N	2012-11-26	1	\N
00000012	03	00000002	\N	D	\N	2012-11-27	1	\N
00000012	03	00000002	\N	D	\N	2012-11-28	1	\N
00000012	03	00000002	\N	D	\N	2012-11-29	1	\N
00000012	03	00000002	\N	F	\N	2012-11-30	1	\N
00000010	03	00000002	\N	F	\N	2012-12-01	1	\N
00000010	03	00000002	\N	D	\N	2012-12-02	1	\N
00000010	03	00000002	\N	D	\N	2012-12-03	1	\N
00000010	03	00000002	\N	D	\N	2012-12-04	1	\N
00000010	03	00000002	\N	D	\N	2012-12-05	1	\N
00000010	03	00000002	\N	D	\N	2012-12-06	1	\N
00000010	03	00000002	\N	F	\N	2012-12-07	1	\N
00000010	03	00000002	\N	F	\N	2012-12-08	1	\N
00000010	03	00000002	\N	D	\N	2012-12-09	1	\N
00000010	03	00000002	\N	D	\N	2012-12-10	1	\N
00000010	03	00000002	\N	D	\N	2012-12-11	1	\N
00000010	03	00000002	\N	D	\N	2012-12-12	1	\N
00000010	03	00000002	\N	F	0000000017	2012-11-23	R	\N
00000010	03	00000002	\N	D	\N	2012-11-22	1	\N
00000010	03	00000002	\N	F	\N	2012-11-24	1	\N
00000010	03	00000002	\N	D	\N	2012-11-25	1	\N
00000010	03	00000002	\N	D	\N	2012-11-26	1	\N
00000010	03	00000002	\N	D	\N	2012-11-27	1	\N
00000010	03	00000002	\N	D	\N	2012-11-28	1	\N
00000010	03	00000002	\N	D	\N	2012-11-29	1	\N
00000011	03	00000002	\N	D	\N	2012-11-01	1	\N
00000011	03	00000002	\N	F	\N	2012-11-02	1	\N
00000011	03	00000002	\N	F	\N	2012-11-03	1	\N
00000011	03	00000002	\N	D	\N	2012-11-04	1	\N
00000010	03	00000002	\N	D	\N	2012-12-13	1	\N
00000010	03	00000002	\N	F	\N	2012-12-14	1	\N
00000010	03	00000002	\N	F	\N	2012-12-15	1	\N
00000010	03	00000002	\N	D	\N	2012-12-16	1	\N
00000010	03	00000002	\N	D	\N	2012-12-17	1	\N
00000010	03	00000002	\N	D	\N	2012-12-18	1	\N
00000010	03	00000002	\N	D	\N	2012-12-19	1	\N
00000010	03	00000002	\N	D	\N	2012-12-20	1	\N
00000010	03	00000002	\N	F	\N	2012-12-21	1	\N
00000010	03	00000002	\N	F	\N	2012-12-22	1	\N
00000010	03	00000002	\N	D	\N	2012-12-23	1	\N
00000010	03	00000002	\N	D	\N	2012-12-24	1	\N
00000010	03	00000002	\N	D	\N	2012-12-25	1	\N
00000010	03	00000002	\N	D	\N	2012-12-26	1	\N
00000010	03	00000002	\N	D	\N	2012-12-27	1	\N
00000010	03	00000002	\N	F	\N	2012-12-28	1	\N
00000010	03	00000002	\N	F	\N	2012-12-29	1	\N
00000010	03	00000002	\N	D	\N	2012-12-30	1	\N
00000010	03	00000002	\N	D	\N	2012-12-31	1	\N
00000011	03	00000002	\N	F	\N	2012-12-01	1	\N
00000011	03	00000002	\N	D	\N	2012-12-02	1	\N
00000011	03	00000002	\N	D	\N	2012-12-03	1	\N
00000011	03	00000002	\N	D	\N	2012-12-04	1	\N
00000011	03	00000002	\N	D	\N	2012-12-05	1	\N
00000011	03	00000002	\N	D	\N	2012-12-06	1	\N
00000011	03	00000002	\N	F	\N	2012-12-07	1	\N
00000011	03	00000002	\N	F	\N	2012-12-08	1	\N
00000011	03	00000002	\N	D	\N	2012-12-09	1	\N
00000021	02	00000002	\N	F	\N	2012-10-27	0	\N
00000021	02	00000002	\N	D	\N	2012-10-28	0	\N
00000021	02	00000002	\N	D	\N	2012-10-29	0	\N
00000021	02	00000002	\N	D	\N	2012-10-30	0	\N
00000021	02	00000002	\N	D	\N	2012-10-31	0	\N
00000022	02	00000002	\N	D	\N	2012-10-01	0	\N
00000022	02	00000002	\N	D	\N	2012-10-02	0	\N
00000022	02	00000002	\N	D	\N	2012-10-03	0	\N
00000022	02	00000002	\N	D	\N	2012-10-04	0	\N
00000022	02	00000002	\N	F	\N	2012-10-05	0	\N
00000022	02	00000002	\N	F	\N	2012-10-06	0	\N
00000022	02	00000002	\N	D	\N	2012-10-07	0	\N
00000022	02	00000002	\N	D	\N	2012-10-08	0	\N
00000022	02	00000002	\N	D	\N	2012-10-09	0	\N
00000022	02	00000002	\N	D	\N	2012-10-10	0	\N
00000022	02	00000002	\N	D	\N	2012-10-11	0	\N
00000022	02	00000002	\N	F	\N	2012-10-12	0	\N
00000022	02	00000002	\N	F	\N	2012-10-13	0	\N
00000022	02	00000002	\N	D	\N	2012-10-14	0	\N
00000022	02	00000002	\N	D	\N	2012-10-15	0	\N
00000022	02	00000002	\N	D	\N	2012-10-16	0	\N
00000022	02	00000002	\N	D	\N	2012-10-17	0	\N
00000022	02	00000002	\N	D	\N	2012-10-18	0	\N
00000022	02	00000002	\N	F	\N	2012-10-19	0	\N
00000022	02	00000002	\N	F	\N	2012-10-20	0	\N
00000022	02	00000002	\N	D	\N	2012-10-21	0	\N
00000022	02	00000002	\N	D	\N	2012-10-22	0	\N
00000022	02	00000002	\N	D	\N	2012-10-23	0	\N
00000022	02	00000002	\N	D	\N	2012-10-24	0	\N
00000022	02	00000002	\N	D	\N	2012-10-25	0	\N
00000022	02	00000002	\N	F	\N	2012-10-26	0	\N
00000022	02	00000002	\N	F	\N	2012-10-27	0	\N
00000022	02	00000002	\N	D	\N	2012-10-28	0	\N
00000022	02	00000002	\N	D	\N	2012-10-29	0	\N
00000022	02	00000002	\N	D	\N	2012-10-30	0	\N
00000022	02	00000002	\N	D	\N	2012-10-31	0	\N
00000023	02	00000002	\N	D	\N	2012-10-01	0	\N
00000023	02	00000002	\N	D	\N	2012-10-02	0	\N
00000023	02	00000002	\N	D	\N	2012-10-03	0	\N
00000023	02	00000002	\N	D	\N	2012-10-04	0	\N
00000023	02	00000002	\N	F	\N	2012-10-05	0	\N
00000023	02	00000002	\N	F	\N	2012-10-06	0	\N
00000023	02	00000002	\N	D	\N	2012-10-07	0	\N
00000023	02	00000002	\N	D	\N	2012-10-08	0	\N
00000023	02	00000002	\N	D	\N	2012-10-09	0	\N
00000023	02	00000002	\N	D	\N	2012-10-10	0	\N
00000023	02	00000002	\N	D	\N	2012-10-11	0	\N
00000023	02	00000002	\N	F	\N	2012-10-12	0	\N
00000023	02	00000002	\N	F	\N	2012-10-13	0	\N
00000023	02	00000002	\N	D	\N	2012-10-14	0	\N
00000023	02	00000002	\N	D	\N	2012-10-15	0	\N
00000023	02	00000002	\N	D	\N	2012-10-16	0	\N
00000023	02	00000002	\N	D	\N	2012-10-17	0	\N
00000023	02	00000002	\N	D	\N	2012-10-18	0	\N
00000023	02	00000002	\N	F	\N	2012-10-19	0	\N
00000023	02	00000002	\N	F	\N	2012-10-20	0	\N
00000023	02	00000002	\N	D	\N	2012-10-21	0	\N
00000023	02	00000002	\N	D	\N	2012-10-22	0	\N
00000023	02	00000002	\N	D	\N	2012-10-23	0	\N
00000023	02	00000002	\N	D	\N	2012-10-24	0	\N
00000023	02	00000002	\N	D	\N	2012-10-25	0	\N
00000023	02	00000002	\N	F	\N	2012-10-26	0	\N
00000023	02	00000002	\N	F	\N	2012-10-27	0	\N
00000023	02	00000002	\N	D	\N	2012-10-28	0	\N
00000023	02	00000002	\N	D	\N	2012-10-29	0	\N
00000023	02	00000002	\N	D	\N	2012-10-30	0	\N
00000023	02	00000002	\N	D	\N	2012-10-31	0	\N
00000024	02	00000002	\N	D	\N	2012-10-01	0	\N
00000024	02	00000002	\N	D	\N	2012-10-02	0	\N
00000024	02	00000002	\N	D	\N	2012-10-03	0	\N
00000024	02	00000002	\N	D	\N	2012-10-04	0	\N
00000024	02	00000002	\N	F	\N	2012-10-05	0	\N
00000024	02	00000002	\N	F	\N	2012-10-06	0	\N
00000024	02	00000002	\N	D	\N	2012-10-07	0	\N
00000024	02	00000002	\N	D	\N	2012-10-08	0	\N
00000024	02	00000002	\N	D	\N	2012-10-09	0	\N
00000024	02	00000002	\N	D	\N	2012-10-10	0	\N
00000024	02	00000002	\N	D	\N	2012-10-11	0	\N
00000024	02	00000002	\N	F	\N	2012-10-12	0	\N
00000024	02	00000002	\N	F	\N	2012-10-13	0	\N
00000024	02	00000002	\N	D	\N	2012-10-14	0	\N
00000024	02	00000002	\N	D	\N	2012-10-15	0	\N
00000024	02	00000002	\N	D	\N	2012-10-16	0	\N
00000024	02	00000002	\N	D	\N	2012-10-17	0	\N
00000024	02	00000002	\N	D	\N	2012-10-18	0	\N
00000024	02	00000002	\N	F	\N	2012-10-19	0	\N
00000024	02	00000002	\N	F	\N	2012-10-20	0	\N
00000024	02	00000002	\N	D	\N	2012-10-21	0	\N
00000024	02	00000002	\N	D	\N	2012-10-22	0	\N
00000024	02	00000002	\N	D	\N	2012-10-23	0	\N
00000024	02	00000002	\N	D	\N	2012-10-24	0	\N
00000024	02	00000002	\N	D	\N	2012-10-25	0	\N
00000024	02	00000002	\N	F	\N	2012-10-26	0	\N
00000024	02	00000002	\N	F	\N	2012-10-27	0	\N
00000024	02	00000002	\N	D	\N	2012-10-28	0	\N
00000024	02	00000002	\N	D	\N	2012-10-29	0	\N
00000024	02	00000002	\N	D	\N	2012-10-30	0	\N
00000024	02	00000002	\N	D	\N	2012-10-31	0	\N
00000025	02	00000002	\N	D	\N	2012-10-01	0	\N
00000025	02	00000002	\N	D	\N	2012-10-02	0	\N
00000025	02	00000002	\N	D	\N	2012-10-03	0	\N
00000025	02	00000002	\N	D	\N	2012-10-04	0	\N
00000025	02	00000002	\N	F	\N	2012-10-05	0	\N
00000025	02	00000002	\N	F	\N	2012-10-06	0	\N
00000025	02	00000002	\N	D	\N	2012-10-07	0	\N
00000025	02	00000002	\N	D	\N	2012-10-08	0	\N
00000025	02	00000002	\N	D	\N	2012-10-09	0	\N
00000025	02	00000002	\N	D	\N	2012-10-10	0	\N
00000025	02	00000002	\N	D	\N	2012-10-11	0	\N
00000025	02	00000002	\N	F	\N	2012-10-12	0	\N
00000025	02	00000002	\N	F	\N	2012-10-13	0	\N
00000025	02	00000002	\N	D	\N	2012-10-14	0	\N
00000025	02	00000002	\N	D	\N	2012-10-15	0	\N
00000025	02	00000002	\N	D	\N	2012-10-16	0	\N
00000025	02	00000002	\N	D	\N	2012-10-17	0	\N
00000025	02	00000002	\N	D	\N	2012-10-18	0	\N
00000025	02	00000002	\N	F	\N	2012-10-19	0	\N
00000025	02	00000002	\N	F	\N	2012-10-20	0	\N
00000025	02	00000002	\N	D	\N	2012-10-21	0	\N
00000025	02	00000002	\N	D	\N	2012-10-22	0	\N
00000025	02	00000002	\N	D	\N	2012-10-23	0	\N
00000025	02	00000002	\N	D	\N	2012-10-24	0	\N
00000025	02	00000002	\N	D	\N	2012-10-25	0	\N
00000025	02	00000002	\N	F	\N	2012-10-26	0	\N
00000025	02	00000002	\N	F	\N	2012-10-27	0	\N
00000025	02	00000002	\N	D	\N	2012-10-28	0	\N
00000025	02	00000002	\N	D	\N	2012-10-29	0	\N
00000025	02	00000002	\N	D	\N	2012-10-30	0	\N
00000025	02	00000002	\N	D	\N	2012-10-31	0	\N
00000026	02	00000002	\N	D	\N	2012-10-01	0	\N
00000026	02	00000002	\N	D	\N	2012-10-02	0	\N
00000026	02	00000002	\N	D	\N	2012-10-03	0	\N
00000026	02	00000002	\N	D	\N	2012-10-04	0	\N
00000026	02	00000002	\N	F	\N	2012-10-05	0	\N
00000026	02	00000002	\N	F	\N	2012-10-06	0	\N
00000026	02	00000002	\N	D	\N	2012-10-07	0	\N
00000026	02	00000002	\N	D	\N	2012-10-08	0	\N
00000026	02	00000002	\N	D	\N	2012-10-09	0	\N
00000026	02	00000002	\N	D	\N	2012-10-10	0	\N
00000026	02	00000002	\N	D	\N	2012-10-11	0	\N
00000026	02	00000002	\N	F	\N	2012-10-12	0	\N
00000026	02	00000002	\N	F	\N	2012-10-13	0	\N
00000026	02	00000002	\N	D	\N	2012-10-14	0	\N
00000026	02	00000002	\N	D	\N	2012-10-15	0	\N
00000009	04	00000002	\N	D	0000000005	2012-11-05	R	\N
00000009	04	00000002	\N	D	0000000005	2012-11-06	R	\N
00000026	02	00000002	\N	D	\N	2012-10-16	0	\N
00000026	02	00000002	\N	D	\N	2012-10-17	0	\N
00000026	02	00000002	\N	D	\N	2012-10-18	0	\N
00000026	02	00000002	\N	F	\N	2012-10-19	0	\N
00000026	02	00000002	\N	F	\N	2012-10-20	0	\N
00000026	02	00000002	\N	D	\N	2012-10-21	0	\N
00000026	02	00000002	\N	D	\N	2012-10-22	0	\N
00000026	02	00000002	\N	D	\N	2012-10-23	0	\N
00000026	02	00000002	\N	D	\N	2012-10-24	0	\N
00000026	02	00000002	\N	D	\N	2012-10-25	0	\N
00000026	02	00000002	\N	F	\N	2012-10-26	0	\N
00000026	02	00000002	\N	F	\N	2012-10-27	0	\N
00000026	02	00000002	\N	D	\N	2012-10-28	0	\N
00000026	02	00000002	\N	D	\N	2012-10-29	0	\N
00000026	02	00000002	\N	D	\N	2012-10-30	0	\N
00000026	02	00000002	\N	D	\N	2012-10-31	0	\N
00000027	02	00000002	\N	D	\N	2012-10-01	0	\N
00000027	02	00000002	\N	D	\N	2012-10-02	0	\N
00000027	02	00000002	\N	D	\N	2012-10-03	0	\N
00000027	02	00000002	\N	D	\N	2012-10-04	0	\N
00000027	02	00000002	\N	F	\N	2012-10-05	0	\N
00000027	02	00000002	\N	F	\N	2012-10-06	0	\N
00000027	02	00000002	\N	D	\N	2012-10-07	0	\N
00000027	02	00000002	\N	D	\N	2012-10-08	0	\N
00000027	02	00000002	\N	D	\N	2012-10-09	0	\N
00000027	02	00000002	\N	D	\N	2012-10-10	0	\N
00000027	02	00000002	\N	D	\N	2012-10-11	0	\N
00000027	02	00000002	\N	F	\N	2012-10-12	0	\N
00000027	02	00000002	\N	F	\N	2012-10-13	0	\N
00000027	02	00000002	\N	D	\N	2012-10-14	0	\N
00000027	02	00000002	\N	D	\N	2012-10-15	0	\N
00000027	02	00000002	\N	D	\N	2012-10-16	0	\N
00000027	02	00000002	\N	D	\N	2012-10-17	0	\N
00000027	02	00000002	\N	D	\N	2012-10-18	0	\N
00000027	02	00000002	\N	F	\N	2012-10-19	0	\N
00000027	02	00000002	\N	F	\N	2012-10-20	0	\N
00000027	02	00000002	\N	D	\N	2012-10-21	0	\N
00000027	02	00000002	\N	D	\N	2012-10-22	0	\N
00000027	02	00000002	\N	D	\N	2012-10-23	0	\N
00000027	02	00000002	\N	D	\N	2012-10-24	0	\N
00000027	02	00000002	\N	D	\N	2012-10-25	0	\N
00000027	02	00000002	\N	F	\N	2012-10-26	0	\N
00000027	02	00000002	\N	F	\N	2012-10-27	0	\N
00000027	02	00000002	\N	D	\N	2012-10-28	0	\N
00000027	02	00000002	\N	D	\N	2012-10-29	0	\N
00000027	02	00000002	\N	D	\N	2012-10-30	0	\N
00000027	02	00000002	\N	D	\N	2012-10-31	0	\N
00000028	02	00000002	\N	D	\N	2012-10-01	0	\N
00000028	02	00000002	\N	D	\N	2012-10-02	0	\N
00000028	02	00000002	\N	D	\N	2012-10-03	0	\N
00000028	02	00000002	\N	D	\N	2012-10-04	0	\N
00000028	02	00000002	\N	F	\N	2012-10-05	0	\N
00000028	02	00000002	\N	F	\N	2012-10-06	0	\N
00000028	02	00000002	\N	D	\N	2012-10-07	0	\N
00000028	02	00000002	\N	D	\N	2012-10-08	0	\N
00000028	02	00000002	\N	D	\N	2012-10-09	0	\N
00000028	02	00000002	\N	D	\N	2012-10-10	0	\N
00000028	02	00000002	\N	D	\N	2012-10-11	0	\N
00000028	02	00000002	\N	F	\N	2012-10-12	0	\N
00000028	02	00000002	\N	F	\N	2012-10-13	0	\N
00000028	02	00000002	\N	D	\N	2012-10-14	0	\N
00000028	02	00000002	\N	D	\N	2012-10-15	0	\N
00000028	02	00000002	\N	D	\N	2012-10-16	0	\N
00000028	02	00000002	\N	D	\N	2012-10-17	0	\N
00000028	02	00000002	\N	D	\N	2012-10-18	0	\N
00000028	02	00000002	\N	F	\N	2012-10-19	0	\N
00000028	02	00000002	\N	F	\N	2012-10-20	0	\N
00000028	02	00000002	\N	D	\N	2012-10-21	0	\N
00000028	02	00000002	\N	D	\N	2012-10-22	0	\N
00000028	02	00000002	\N	D	\N	2012-10-23	0	\N
00000028	02	00000002	\N	D	\N	2012-10-24	0	\N
00000028	02	00000002	\N	D	\N	2012-10-25	0	\N
00000028	02	00000002	\N	F	\N	2012-10-26	0	\N
00000028	02	00000002	\N	F	\N	2012-10-27	0	\N
00000028	02	00000002	\N	D	\N	2012-10-28	0	\N
00000028	02	00000002	\N	D	\N	2012-10-29	0	\N
00000028	02	00000002	\N	D	\N	2012-10-30	0	\N
00000028	02	00000002	\N	D	\N	2012-10-31	0	\N
00000029	02	00000002	\N	D	\N	2012-10-01	0	\N
00000029	02	00000002	\N	D	\N	2012-10-02	0	\N
00000029	02	00000002	\N	D	\N	2012-10-03	0	\N
00000029	02	00000002	\N	D	\N	2012-10-04	0	\N
00000029	02	00000002	\N	F	\N	2012-10-05	0	\N
00000029	02	00000002	\N	F	\N	2012-10-06	0	\N
00000029	02	00000002	\N	D	\N	2012-10-07	0	\N
00000029	02	00000002	\N	D	\N	2012-10-08	0	\N
00000029	02	00000002	\N	D	\N	2012-10-09	0	\N
00000029	02	00000002	\N	D	\N	2012-10-10	0	\N
00000029	02	00000002	\N	D	\N	2012-10-11	0	\N
00000029	02	00000002	\N	F	\N	2012-10-12	0	\N
00000029	02	00000002	\N	F	\N	2012-10-13	0	\N
00000029	02	00000002	\N	D	\N	2012-10-14	0	\N
00000029	02	00000002	\N	D	\N	2012-10-15	0	\N
00000029	02	00000002	\N	D	\N	2012-10-16	0	\N
00000029	02	00000002	\N	D	\N	2012-10-17	0	\N
00000029	02	00000002	\N	D	\N	2012-10-18	0	\N
00000029	02	00000002	\N	F	\N	2012-10-19	0	\N
00000029	02	00000002	\N	F	\N	2012-10-20	0	\N
00000029	02	00000002	\N	D	\N	2012-10-21	0	\N
00000029	02	00000002	\N	D	\N	2012-10-22	0	\N
00000029	02	00000002	\N	D	\N	2012-10-23	0	\N
00000029	02	00000002	\N	D	\N	2012-10-24	0	\N
00000029	02	00000002	\N	D	\N	2012-10-25	0	\N
00000029	02	00000002	\N	F	\N	2012-10-26	0	\N
00000029	02	00000002	\N	F	\N	2012-10-27	0	\N
00000029	02	00000002	\N	D	\N	2012-10-28	0	\N
00000029	02	00000002	\N	D	\N	2012-10-29	0	\N
00000029	02	00000002	\N	D	\N	2012-10-30	0	\N
00000029	02	00000002	\N	D	\N	2012-10-31	0	\N
00000030	02	00000002	\N	D	\N	2012-10-01	0	\N
00000030	02	00000002	\N	D	\N	2012-10-02	0	\N
00000030	02	00000002	\N	D	\N	2012-10-03	0	\N
00000030	02	00000002	\N	D	\N	2012-10-04	0	\N
00000030	02	00000002	\N	F	\N	2012-10-05	0	\N
00000030	02	00000002	\N	F	\N	2012-10-06	0	\N
00000030	02	00000002	\N	D	\N	2012-10-07	0	\N
00000030	02	00000002	\N	D	\N	2012-10-08	0	\N
00000030	02	00000002	\N	D	\N	2012-10-09	0	\N
00000030	02	00000002	\N	D	\N	2012-10-10	0	\N
00000030	02	00000002	\N	D	\N	2012-10-11	0	\N
00000030	02	00000002	\N	F	\N	2012-10-12	0	\N
00000030	02	00000002	\N	F	\N	2012-10-13	0	\N
00000030	02	00000002	\N	D	\N	2012-10-14	0	\N
00000030	02	00000002	\N	D	\N	2012-10-15	0	\N
00000030	02	00000002	\N	D	\N	2012-10-16	0	\N
00000030	02	00000002	\N	D	\N	2012-10-17	0	\N
00000030	02	00000002	\N	D	\N	2012-10-18	0	\N
00000030	02	00000002	\N	F	\N	2012-10-19	0	\N
00000030	02	00000002	\N	F	\N	2012-10-20	0	\N
00000030	02	00000002	\N	D	\N	2012-10-21	0	\N
00000030	02	00000002	\N	D	\N	2012-10-22	0	\N
00000030	02	00000002	\N	D	\N	2012-10-23	0	\N
00000030	02	00000002	\N	D	\N	2012-10-24	0	\N
00000030	02	00000002	\N	D	\N	2012-10-25	0	\N
00000030	02	00000002	\N	F	\N	2012-10-26	0	\N
00000030	02	00000002	\N	F	\N	2012-10-27	0	\N
00000030	02	00000002	\N	D	\N	2012-10-28	0	\N
00000030	02	00000002	\N	D	\N	2012-10-29	0	\N
00000030	02	00000002	\N	D	\N	2012-10-30	0	\N
00000030	02	00000002	\N	D	\N	2012-10-31	0	\N
00000031	02	00000002	\N	D	\N	2012-10-01	0	\N
00000031	02	00000002	\N	D	\N	2012-10-02	0	\N
00000031	02	00000002	\N	D	\N	2012-10-03	0	\N
00000031	02	00000002	\N	D	\N	2012-10-04	0	\N
00000031	02	00000002	\N	F	\N	2012-10-05	0	\N
00000031	02	00000002	\N	F	\N	2012-10-06	0	\N
00000031	02	00000002	\N	D	\N	2012-10-07	0	\N
00000031	02	00000002	\N	D	\N	2012-10-08	0	\N
00000031	02	00000002	\N	D	\N	2012-10-09	0	\N
00000031	02	00000002	\N	D	\N	2012-10-10	0	\N
00000013	05	00000002	\N	D	\N	2013-01-01	1	\N
00000013	05	00000002	\N	D	\N	2013-01-02	1	\N
00000013	05	00000002	\N	D	\N	2013-01-03	1	\N
00000013	05	00000002	\N	F	\N	2013-01-04	1	\N
00000013	05	00000002	\N	F	\N	2013-01-05	1	\N
00000013	05	00000002	\N	D	\N	2013-01-06	1	\N
00000013	05	00000002	\N	D	\N	2013-01-07	1	\N
00000013	05	00000002	\N	D	\N	2013-01-08	1	\N
00000013	05	00000002	\N	D	\N	2013-01-09	1	\N
00000013	05	00000002	\N	D	\N	2013-01-10	1	\N
00000013	05	00000002	\N	F	\N	2013-01-11	1	\N
00000013	05	00000002	\N	F	\N	2013-01-12	1	\N
00000013	05	00000002	\N	D	\N	2013-01-13	1	\N
00000013	05	00000002	\N	D	\N	2013-01-14	1	\N
00000013	05	00000002	\N	D	\N	2013-01-15	1	\N
00000013	05	00000002	\N	D	\N	2013-01-16	1	\N
00000013	05	00000002	\N	D	\N	2013-01-17	1	\N
00000013	05	00000002	\N	F	\N	2013-01-18	1	\N
00000013	05	00000002	\N	F	\N	2013-01-19	1	\N
00000013	05	00000002	\N	D	\N	2013-01-20	1	\N
00000013	05	00000002	\N	D	\N	2013-01-21	1	\N
00000013	05	00000002	\N	D	\N	2013-01-22	1	\N
00000013	05	00000002	\N	D	\N	2013-01-23	1	\N
00000013	05	00000002	\N	D	\N	2013-01-24	1	\N
00000013	05	00000002	\N	F	\N	2013-01-25	1	\N
00000013	05	00000002	\N	F	\N	2013-01-26	1	\N
00000013	05	00000002	\N	D	\N	2013-01-27	1	\N
00000013	05	00000002	\N	D	\N	2013-01-28	1	\N
00000013	05	00000002	\N	D	\N	2013-01-29	1	\N
00000013	05	00000002	\N	D	\N	2013-01-30	1	\N
00000013	05	00000002	\N	D	\N	2013-01-31	1	\N
00000014	05	00000002	\N	D	\N	2013-01-01	1	\N
00000014	05	00000002	\N	D	\N	2013-01-02	1	\N
00000014	05	00000002	\N	D	\N	2013-01-03	1	\N
00000014	05	00000002	\N	F	\N	2013-01-04	1	\N
00000014	05	00000002	\N	F	\N	2013-01-05	1	\N
00000014	05	00000002	\N	D	\N	2013-01-06	1	\N
00000014	05	00000002	\N	D	\N	2013-01-07	1	\N
00000014	05	00000002	\N	D	\N	2013-01-08	1	\N
00000014	05	00000002	\N	D	\N	2013-01-09	1	\N
00000014	05	00000002	\N	D	\N	2013-01-10	1	\N
00000014	05	00000002	\N	F	\N	2013-01-11	1	\N
00000014	05	00000002	\N	F	\N	2013-01-12	1	\N
00000014	05	00000002	\N	D	\N	2013-01-13	1	\N
00000014	05	00000002	\N	D	\N	2013-01-14	1	\N
00000014	05	00000002	\N	D	\N	2013-01-15	1	\N
00000014	05	00000002	\N	D	\N	2013-01-16	1	\N
00000014	05	00000002	\N	D	\N	2013-01-17	1	\N
00000014	05	00000002	\N	F	\N	2013-01-18	1	\N
00000014	05	00000002	\N	F	\N	2013-01-19	1	\N
00000014	05	00000002	\N	D	\N	2013-01-20	1	\N
00000014	05	00000002	\N	D	\N	2013-01-21	1	\N
00000014	05	00000002	\N	D	\N	2013-01-22	1	\N
00000014	05	00000002	\N	D	\N	2013-01-23	1	\N
00000014	05	00000002	\N	D	\N	2013-01-24	1	\N
00000014	05	00000002	\N	F	\N	2013-01-25	1	\N
00000014	05	00000002	\N	F	\N	2013-01-26	1	\N
00000014	05	00000002	\N	D	\N	2013-01-27	1	\N
00000014	05	00000002	\N	D	\N	2013-01-28	1	\N
00000014	05	00000002	\N	D	\N	2013-01-29	1	\N
00000014	05	00000002	\N	D	\N	2013-01-30	1	\N
00000014	05	00000002	\N	D	\N	2013-01-31	1	\N
00000015	05	00000002	\N	D	\N	2013-01-01	1	\N
00000015	05	00000002	\N	D	\N	2013-01-02	1	\N
00000015	05	00000002	\N	D	\N	2013-01-03	1	\N
00000015	05	00000002	\N	F	\N	2013-01-04	1	\N
00000015	05	00000002	\N	F	\N	2013-01-05	1	\N
00000015	05	00000002	\N	D	\N	2013-01-06	1	\N
00000015	05	00000002	\N	D	\N	2013-01-07	1	\N
00000015	05	00000002	\N	D	\N	2013-01-08	1	\N
00000015	05	00000002	\N	D	\N	2013-01-09	1	\N
00000015	05	00000002	\N	D	\N	2013-01-10	1	\N
00000015	05	00000002	\N	F	\N	2013-01-11	1	\N
00000015	05	00000002	\N	F	\N	2013-01-12	1	\N
00000015	05	00000002	\N	D	\N	2013-01-13	1	\N
00000015	05	00000002	\N	D	\N	2013-01-14	1	\N
00000015	05	00000002	\N	D	\N	2013-01-15	1	\N
00000015	05	00000002	\N	D	\N	2013-01-16	1	\N
00000015	05	00000002	\N	D	\N	2013-01-17	1	\N
00000015	05	00000002	\N	F	\N	2013-01-18	1	\N
00000015	05	00000002	\N	F	\N	2013-01-19	1	\N
00000015	05	00000002	\N	D	\N	2013-01-20	1	\N
00000015	05	00000002	\N	D	\N	2013-01-21	1	\N
00000015	05	00000002	\N	D	\N	2013-01-22	1	\N
00000015	05	00000002	\N	D	\N	2013-01-23	1	\N
00000015	05	00000002	\N	D	\N	2013-01-24	1	\N
00000015	05	00000002	\N	F	\N	2013-01-25	1	\N
00000015	05	00000002	\N	F	\N	2013-01-26	1	\N
00000015	05	00000002	\N	D	\N	2013-01-27	1	\N
00000015	05	00000002	\N	D	\N	2013-01-28	1	\N
00000015	05	00000002	\N	D	\N	2013-01-29	1	\N
00000015	05	00000002	\N	D	\N	2013-01-30	1	\N
00000015	05	00000002	\N	D	\N	2013-01-31	1	\N
00000016	05	00000002	\N	D	\N	2013-01-01	1	\N
00000016	05	00000002	\N	D	\N	2013-01-02	1	\N
00000016	05	00000002	\N	D	\N	2013-01-03	1	\N
00000016	05	00000002	\N	F	\N	2013-01-04	1	\N
00000016	05	00000002	\N	F	\N	2013-01-05	1	\N
00000016	05	00000002	\N	D	\N	2013-01-06	1	\N
00000016	05	00000002	\N	D	\N	2013-01-07	1	\N
00000016	05	00000002	\N	D	\N	2013-01-08	1	\N
00000016	05	00000002	\N	D	\N	2013-01-09	1	\N
00000016	05	00000002	\N	D	\N	2013-01-10	1	\N
00000016	05	00000002	\N	F	\N	2013-01-11	1	\N
00000016	05	00000002	\N	F	\N	2013-01-12	1	\N
00000016	05	00000002	\N	D	\N	2013-01-13	1	\N
00000016	05	00000002	\N	D	\N	2013-01-14	1	\N
00000016	05	00000002	\N	D	\N	2013-01-15	1	\N
00000016	05	00000002	\N	D	\N	2013-01-16	1	\N
00000016	05	00000002	\N	D	\N	2013-01-17	1	\N
00000016	05	00000002	\N	F	\N	2013-01-18	1	\N
00000016	05	00000002	\N	F	\N	2013-01-19	1	\N
00000016	05	00000002	\N	D	\N	2013-01-20	1	\N
00000016	05	00000002	\N	D	\N	2013-01-21	1	\N
00000016	05	00000002	\N	D	\N	2013-01-22	1	\N
00000016	05	00000002	\N	D	\N	2013-01-23	1	\N
00000016	05	00000002	\N	D	\N	2013-01-24	1	\N
00000016	05	00000002	\N	F	\N	2013-01-25	1	\N
00000016	05	00000002	\N	F	\N	2013-01-26	1	\N
00000016	05	00000002	\N	D	\N	2013-01-27	1	\N
00000016	05	00000002	\N	D	\N	2013-01-28	1	\N
00000016	05	00000002	\N	D	\N	2013-01-29	1	\N
00000016	05	00000002	\N	D	\N	2013-01-30	1	\N
00000016	05	00000002	\N	D	\N	2013-01-31	1	\N
00000031	02	00000002	\N	D	\N	2012-10-11	0	\N
00000031	02	00000002	\N	F	\N	2012-10-12	0	\N
00000031	02	00000002	\N	F	\N	2012-10-13	0	\N
00000031	02	00000002	\N	D	\N	2012-10-14	0	\N
00000031	02	00000002	\N	D	\N	2012-10-15	0	\N
00000031	02	00000002	\N	D	\N	2012-10-16	0	\N
00000031	02	00000002	\N	D	\N	2012-10-17	0	\N
00000031	02	00000002	\N	D	\N	2012-10-18	0	\N
00000031	02	00000002	\N	F	\N	2012-10-19	0	\N
00000031	02	00000002	\N	F	\N	2012-10-20	0	\N
00000031	02	00000002	\N	D	\N	2012-10-21	0	\N
00000031	02	00000002	\N	D	\N	2012-10-22	0	\N
00000031	02	00000002	\N	D	\N	2012-10-23	0	\N
00000031	02	00000002	\N	D	\N	2012-10-24	0	\N
00000031	02	00000002	\N	D	\N	2012-10-25	0	\N
00000031	02	00000002	\N	F	\N	2012-10-26	0	\N
00000031	02	00000002	\N	F	\N	2012-10-27	0	\N
00000031	02	00000002	\N	D	\N	2012-10-28	0	\N
00000031	02	00000002	\N	D	\N	2012-10-29	0	\N
00000031	02	00000002	\N	D	\N	2012-10-30	0	\N
00000031	02	00000002	\N	D	\N	2012-10-31	0	\N
00000032	02	00000002	\N	D	\N	2012-10-01	0	\N
00000032	02	00000002	\N	D	\N	2012-10-02	0	\N
00000032	02	00000002	\N	D	\N	2012-10-03	0	\N
00000032	02	00000002	\N	D	\N	2012-10-04	0	\N
00000032	02	00000002	\N	F	\N	2012-10-05	0	\N
00000032	02	00000002	\N	F	\N	2012-10-06	0	\N
00000032	02	00000002	\N	D	\N	2012-10-07	0	\N
00000032	02	00000002	\N	D	\N	2012-10-08	0	\N
00000032	02	00000002	\N	D	\N	2012-10-09	0	\N
00000032	02	00000002	\N	D	\N	2012-10-10	0	\N
00000032	02	00000002	\N	D	\N	2012-10-11	0	\N
00000032	02	00000002	\N	F	\N	2012-10-12	0	\N
00000032	02	00000002	\N	F	\N	2012-10-13	0	\N
00000032	02	00000002	\N	D	\N	2012-10-14	0	\N
00000032	02	00000002	\N	D	\N	2012-10-15	0	\N
00000032	02	00000002	\N	D	\N	2012-10-16	0	\N
00000032	02	00000002	\N	D	\N	2012-10-17	0	\N
00000032	02	00000002	\N	D	\N	2012-10-18	0	\N
00000032	02	00000002	\N	F	\N	2012-10-19	0	\N
00000032	02	00000002	\N	F	\N	2012-10-20	0	\N
00000032	02	00000002	\N	D	\N	2012-10-21	0	\N
00000032	02	00000002	\N	D	\N	2012-10-22	0	\N
00000032	02	00000002	\N	D	\N	2012-10-23	0	\N
00000032	02	00000002	\N	D	\N	2012-10-24	0	\N
00000032	02	00000002	\N	D	\N	2012-10-25	0	\N
00000032	02	00000002	\N	F	\N	2012-10-26	0	\N
00000032	02	00000002	\N	F	\N	2012-10-27	0	\N
00000032	02	00000002	\N	D	\N	2012-10-28	0	\N
00000032	02	00000002	\N	D	\N	2012-10-29	0	\N
00000032	02	00000002	\N	D	\N	2012-10-30	0	\N
00000032	02	00000002	\N	D	\N	2012-10-31	0	\N
00000033	02	00000002	\N	D	\N	2012-10-01	0	\N
00000033	02	00000002	\N	D	\N	2012-10-02	0	\N
00000033	02	00000002	\N	D	\N	2012-10-03	0	\N
00000033	02	00000002	\N	D	\N	2012-10-04	0	\N
00000033	02	00000002	\N	F	\N	2012-10-05	0	\N
00000033	02	00000002	\N	F	\N	2012-10-06	0	\N
00000033	02	00000002	\N	D	\N	2012-10-07	0	\N
00000033	02	00000002	\N	D	\N	2012-10-08	0	\N
00000033	02	00000002	\N	D	\N	2012-10-09	0	\N
00000033	02	00000002	\N	D	\N	2012-10-10	0	\N
00000033	02	00000002	\N	D	\N	2012-10-11	0	\N
00000033	02	00000002	\N	F	\N	2012-10-12	0	\N
00000033	02	00000002	\N	F	\N	2012-10-13	0	\N
00000033	02	00000002	\N	D	\N	2012-10-14	0	\N
00000033	02	00000002	\N	D	\N	2012-10-15	0	\N
00000033	02	00000002	\N	D	\N	2012-10-16	0	\N
00000033	02	00000002	\N	D	\N	2012-10-17	0	\N
00000033	02	00000002	\N	D	\N	2012-10-18	0	\N
00000033	02	00000002	\N	F	\N	2012-10-19	0	\N
00000033	02	00000002	\N	F	\N	2012-10-20	0	\N
00000033	02	00000002	\N	D	\N	2012-10-21	0	\N
00000033	02	00000002	\N	D	\N	2012-10-22	0	\N
00000033	02	00000002	\N	D	\N	2012-10-23	0	\N
00000033	02	00000002	\N	D	\N	2012-10-24	0	\N
00000033	02	00000002	\N	D	\N	2012-10-25	0	\N
00000033	02	00000002	\N	F	\N	2012-10-26	0	\N
00000033	02	00000002	\N	F	\N	2012-10-27	0	\N
00000033	02	00000002	\N	D	\N	2012-10-28	0	\N
00000033	02	00000002	\N	D	\N	2012-10-29	0	\N
00000033	02	00000002	\N	D	\N	2012-10-30	0	\N
00000033	02	00000002	\N	D	\N	2012-10-31	0	\N
00000034	02	00000002	\N	D	\N	2012-10-01	0	\N
00000034	02	00000002	\N	D	\N	2012-10-02	0	\N
00000034	02	00000002	\N	D	\N	2012-10-03	0	\N
00000034	02	00000002	\N	D	\N	2012-10-04	0	\N
00000034	02	00000002	\N	F	\N	2012-10-05	0	\N
00000034	02	00000002	\N	F	\N	2012-10-06	0	\N
00000034	02	00000002	\N	D	\N	2012-10-07	0	\N
00000034	02	00000002	\N	D	\N	2012-10-08	0	\N
00000034	02	00000002	\N	D	\N	2012-10-09	0	\N
00000034	02	00000002	\N	D	\N	2012-10-10	0	\N
00000034	02	00000002	\N	D	\N	2012-10-11	0	\N
00000034	02	00000002	\N	F	\N	2012-10-12	0	\N
00000034	02	00000002	\N	F	\N	2012-10-13	0	\N
00000034	02	00000002	\N	D	\N	2012-10-14	0	\N
00000034	02	00000002	\N	D	\N	2012-10-15	0	\N
00000034	02	00000002	\N	D	\N	2012-10-16	0	\N
00000034	02	00000002	\N	D	\N	2012-10-17	0	\N
00000034	02	00000002	\N	D	\N	2012-10-18	0	\N
00000034	02	00000002	\N	F	\N	2012-10-19	0	\N
00000034	02	00000002	\N	F	\N	2012-10-20	0	\N
00000034	02	00000002	\N	D	\N	2012-10-21	0	\N
00000034	02	00000002	\N	D	\N	2012-10-22	0	\N
00000034	02	00000002	\N	D	\N	2012-10-23	0	\N
00000034	02	00000002	\N	D	\N	2012-10-24	0	\N
00000034	02	00000002	\N	D	\N	2012-10-25	0	\N
00000034	02	00000002	\N	F	\N	2012-10-26	0	\N
00000034	02	00000002	\N	F	\N	2012-10-27	0	\N
00000034	02	00000002	\N	D	\N	2012-10-28	0	\N
00000034	02	00000002	\N	D	\N	2012-10-29	0	\N
00000034	02	00000002	\N	D	\N	2012-10-30	0	\N
00000034	02	00000002	\N	D	\N	2012-10-31	0	\N
00000035	02	00000002	\N	D	\N	2012-10-01	0	\N
00000035	02	00000002	\N	D	\N	2012-10-02	0	\N
00000035	02	00000002	\N	D	\N	2012-10-03	0	\N
00000035	02	00000002	\N	D	\N	2012-10-04	0	\N
00000035	02	00000002	\N	F	\N	2012-10-05	0	\N
00000035	02	00000002	\N	F	\N	2012-10-06	0	\N
00000035	02	00000002	\N	D	\N	2012-10-07	0	\N
00000035	02	00000002	\N	D	\N	2012-10-08	0	\N
00000035	02	00000002	\N	D	\N	2012-10-09	0	\N
00000035	02	00000002	\N	D	\N	2012-10-10	0	\N
00000035	02	00000002	\N	D	\N	2012-10-11	0	\N
00000035	02	00000002	\N	F	\N	2012-10-12	0	\N
00000035	02	00000002	\N	F	\N	2012-10-13	0	\N
00000035	02	00000002	\N	D	\N	2012-10-14	0	\N
00000035	02	00000002	\N	D	\N	2012-10-15	0	\N
00000035	02	00000002	\N	D	\N	2012-10-16	0	\N
00000035	02	00000002	\N	D	\N	2012-10-17	0	\N
00000035	02	00000002	\N	D	\N	2012-10-18	0	\N
00000035	02	00000002	\N	F	\N	2012-10-19	0	\N
00000035	02	00000002	\N	F	\N	2012-10-20	0	\N
00000035	02	00000002	\N	D	\N	2012-10-21	0	\N
00000035	02	00000002	\N	D	\N	2012-10-22	0	\N
00000035	02	00000002	\N	D	\N	2012-10-23	0	\N
00000035	02	00000002	\N	D	\N	2012-10-24	0	\N
00000035	02	00000002	\N	D	\N	2012-10-25	0	\N
00000035	02	00000002	\N	F	\N	2012-10-26	0	\N
00000035	02	00000002	\N	F	\N	2012-10-27	0	\N
00000035	02	00000002	\N	D	\N	2012-10-28	0	\N
00000035	02	00000002	\N	D	\N	2012-10-29	0	\N
00000035	02	00000002	\N	D	\N	2012-10-30	0	\N
00000035	02	00000002	\N	D	\N	2012-10-31	0	\N
00000036	02	00000002	\N	D	\N	2012-10-01	0	\N
00000036	02	00000002	\N	D	\N	2012-10-02	0	\N
00000036	02	00000002	\N	D	\N	2012-10-03	0	\N
00000036	02	00000002	\N	D	\N	2012-10-04	0	\N
00000036	02	00000002	\N	F	\N	2012-10-05	0	\N
00000036	02	00000002	\N	F	\N	2012-10-06	0	\N
00000036	02	00000002	\N	D	\N	2012-10-07	0	\N
00000036	02	00000002	\N	D	\N	2012-10-08	0	\N
00000036	02	00000002	\N	D	\N	2012-10-09	0	\N
00000036	02	00000002	\N	D	\N	2012-10-10	0	\N
00000013	05	00000002	\N	F	\N	2013-02-01	1	\N
00000013	05	00000002	\N	F	\N	2013-02-02	1	\N
00000013	05	00000002	\N	D	\N	2013-02-03	1	\N
00000013	05	00000002	\N	D	\N	2013-02-04	1	\N
00000013	05	00000002	\N	D	\N	2013-02-05	1	\N
00000013	05	00000002	\N	D	\N	2013-02-06	1	\N
00000013	05	00000002	\N	D	\N	2013-02-07	1	\N
00000013	05	00000002	\N	F	\N	2013-02-08	1	\N
00000013	05	00000002	\N	F	\N	2013-02-09	1	\N
00000013	05	00000002	\N	D	\N	2013-02-10	1	\N
00000013	05	00000002	\N	D	\N	2013-02-11	1	\N
00000013	05	00000002	\N	D	\N	2013-02-12	1	\N
00000013	05	00000002	\N	D	\N	2013-02-13	1	\N
00000013	05	00000002	\N	D	\N	2013-02-14	1	\N
00000013	05	00000002	\N	F	\N	2013-02-15	1	\N
00000013	05	00000002	\N	F	\N	2013-02-16	1	\N
00000013	05	00000002	\N	D	\N	2013-02-17	1	\N
00000013	05	00000002	\N	D	\N	2013-02-18	1	\N
00000013	05	00000002	\N	D	\N	2013-02-19	1	\N
00000013	05	00000002	\N	D	\N	2013-02-20	1	\N
00000013	05	00000002	\N	D	\N	2013-02-21	1	\N
00000013	05	00000002	\N	F	\N	2013-02-22	1	\N
00000013	05	00000002	\N	F	\N	2013-02-23	1	\N
00000013	05	00000002	\N	D	\N	2013-02-24	1	\N
00000013	05	00000002	\N	D	\N	2013-02-25	1	\N
00000013	05	00000002	\N	D	\N	2013-02-26	1	\N
00000013	05	00000002	\N	D	\N	2013-02-27	1	\N
00000013	05	00000002	\N	D	\N	2013-02-28	1	\N
00000014	05	00000002	\N	F	\N	2013-02-01	1	\N
00000014	05	00000002	\N	F	\N	2013-02-02	1	\N
00000014	05	00000002	\N	D	\N	2013-02-03	1	\N
00000014	05	00000002	\N	D	\N	2013-02-04	1	\N
00000014	05	00000002	\N	D	\N	2013-02-05	1	\N
00000014	05	00000002	\N	D	\N	2013-02-06	1	\N
00000014	05	00000002	\N	D	\N	2013-02-07	1	\N
00000014	05	00000002	\N	F	\N	2013-02-08	1	\N
00000014	05	00000002	\N	F	\N	2013-02-09	1	\N
00000014	05	00000002	\N	D	\N	2013-02-10	1	\N
00000014	05	00000002	\N	D	\N	2013-02-11	1	\N
00000014	05	00000002	\N	D	\N	2013-02-12	1	\N
00000014	05	00000002	\N	D	\N	2013-02-13	1	\N
00000014	05	00000002	\N	D	\N	2013-02-14	1	\N
00000014	05	00000002	\N	F	\N	2013-02-15	1	\N
00000014	05	00000002	\N	F	\N	2013-02-16	1	\N
00000014	05	00000002	\N	D	\N	2013-02-17	1	\N
00000014	05	00000002	\N	D	\N	2013-02-18	1	\N
00000014	05	00000002	\N	D	\N	2013-02-19	1	\N
00000014	05	00000002	\N	D	\N	2013-02-20	1	\N
00000014	05	00000002	\N	D	\N	2013-02-21	1	\N
00000014	05	00000002	\N	F	\N	2013-02-22	1	\N
00000014	05	00000002	\N	F	\N	2013-02-23	1	\N
00000014	05	00000002	\N	D	\N	2013-02-24	1	\N
00000014	05	00000002	\N	D	\N	2013-02-25	1	\N
00000014	05	00000002	\N	D	\N	2013-02-26	1	\N
00000014	05	00000002	\N	D	\N	2013-02-27	1	\N
00000014	05	00000002	\N	D	\N	2013-02-28	1	\N
00000015	05	00000002	\N	F	\N	2013-02-01	1	\N
00000015	05	00000002	\N	F	\N	2013-02-02	1	\N
00000015	05	00000002	\N	D	\N	2013-02-03	1	\N
00000015	05	00000002	\N	D	\N	2013-02-04	1	\N
00000015	05	00000002	\N	D	\N	2013-02-05	1	\N
00000015	05	00000002	\N	D	\N	2013-02-06	1	\N
00000015	05	00000002	\N	D	\N	2013-02-07	1	\N
00000015	05	00000002	\N	F	\N	2013-02-08	1	\N
00000015	05	00000002	\N	F	\N	2013-02-09	1	\N
00000015	05	00000002	\N	D	\N	2013-02-10	1	\N
00000015	05	00000002	\N	D	\N	2013-02-11	1	\N
00000015	05	00000002	\N	D	\N	2013-02-12	1	\N
00000015	05	00000002	\N	D	\N	2013-02-13	1	\N
00000015	05	00000002	\N	D	\N	2013-02-14	1	\N
00000015	05	00000002	\N	F	\N	2013-02-15	1	\N
00000015	05	00000002	\N	F	\N	2013-02-16	1	\N
00000015	05	00000002	\N	D	\N	2013-02-17	1	\N
00000015	05	00000002	\N	D	\N	2013-02-18	1	\N
00000015	05	00000002	\N	D	\N	2013-02-19	1	\N
00000015	05	00000002	\N	D	\N	2013-02-20	1	\N
00000015	05	00000002	\N	D	\N	2013-02-21	1	\N
00000015	05	00000002	\N	F	\N	2013-02-22	1	\N
00000015	05	00000002	\N	F	\N	2013-02-23	1	\N
00000015	05	00000002	\N	D	\N	2013-02-24	1	\N
00000015	05	00000002	\N	D	\N	2013-02-25	1	\N
00000015	05	00000002	\N	D	\N	2013-02-26	1	\N
00000015	05	00000002	\N	D	\N	2013-02-27	1	\N
00000015	05	00000002	\N	D	\N	2013-02-28	1	\N
00000016	05	00000002	\N	F	\N	2013-02-01	1	\N
00000016	05	00000002	\N	F	\N	2013-02-02	1	\N
00000016	05	00000002	\N	D	\N	2013-02-03	1	\N
00000016	05	00000002	\N	D	\N	2013-02-04	1	\N
00000016	05	00000002	\N	D	\N	2013-02-05	1	\N
00000016	05	00000002	\N	D	\N	2013-02-06	1	\N
00000016	05	00000002	\N	D	\N	2013-02-07	1	\N
00000016	05	00000002	\N	F	\N	2013-02-08	1	\N
00000016	05	00000002	\N	F	\N	2013-02-09	1	\N
00000016	05	00000002	\N	D	\N	2013-02-10	1	\N
00000016	05	00000002	\N	D	\N	2013-02-11	1	\N
00000016	05	00000002	\N	D	\N	2013-02-12	1	\N
00000016	05	00000002	\N	D	\N	2013-02-13	1	\N
00000016	05	00000002	\N	D	\N	2013-02-14	1	\N
00000016	05	00000002	\N	F	\N	2013-02-15	1	\N
00000016	05	00000002	\N	F	\N	2013-02-16	1	\N
00000016	05	00000002	\N	D	\N	2013-02-17	1	\N
00000016	05	00000002	\N	D	\N	2013-02-18	1	\N
00000016	05	00000002	\N	D	\N	2013-02-19	1	\N
00000016	05	00000002	\N	D	\N	2013-02-20	1	\N
00000016	05	00000002	\N	D	\N	2013-02-21	1	\N
00000016	05	00000002	\N	F	\N	2013-02-22	1	\N
00000016	05	00000002	\N	F	\N	2013-02-23	1	\N
00000016	05	00000002	\N	D	\N	2013-02-24	1	\N
00000016	05	00000002	\N	D	\N	2013-02-25	1	\N
00000016	05	00000002	\N	D	\N	2013-02-26	1	\N
00000016	05	00000002	\N	D	\N	2013-02-27	1	\N
00000016	05	00000002	\N	D	\N	2013-02-28	1	\N
00000036	02	00000002	\N	D	\N	2012-10-11	0	\N
00000036	02	00000002	\N	F	\N	2012-10-12	0	\N
00000036	02	00000002	\N	F	\N	2012-10-13	0	\N
00000036	02	00000002	\N	D	\N	2012-10-14	0	\N
00000036	02	00000002	\N	D	\N	2012-10-15	0	\N
00000036	02	00000002	\N	D	\N	2012-10-16	0	\N
00000036	02	00000002	\N	D	\N	2012-10-17	0	\N
00000036	02	00000002	\N	D	\N	2012-10-18	0	\N
00000036	02	00000002	\N	F	\N	2012-10-19	0	\N
00000036	02	00000002	\N	F	\N	2012-10-20	0	\N
00000036	02	00000002	\N	D	\N	2012-10-21	0	\N
00000036	02	00000002	\N	D	\N	2012-10-22	0	\N
00000036	02	00000002	\N	D	\N	2012-10-23	0	\N
00000036	02	00000002	\N	D	\N	2012-10-24	0	\N
00000036	02	00000002	\N	D	\N	2012-10-25	0	\N
00000036	02	00000002	\N	F	\N	2012-10-26	0	\N
00000036	02	00000002	\N	F	\N	2012-10-27	0	\N
00000036	02	00000002	\N	D	\N	2012-10-28	0	\N
00000036	02	00000002	\N	D	\N	2012-10-29	0	\N
00000036	02	00000002	\N	D	\N	2012-10-30	0	\N
00000036	02	00000002	\N	D	\N	2012-10-31	0	\N
00000037	02	00000002	\N	D	\N	2012-10-01	0	\N
00000037	02	00000002	\N	D	\N	2012-10-02	0	\N
00000037	02	00000002	\N	D	\N	2012-10-03	0	\N
00000037	02	00000002	\N	D	\N	2012-10-04	0	\N
00000037	02	00000002	\N	F	\N	2012-10-05	0	\N
00000037	02	00000002	\N	F	\N	2012-10-06	0	\N
00000037	02	00000002	\N	D	\N	2012-10-07	0	\N
00000037	02	00000002	\N	D	\N	2012-10-08	0	\N
00000037	02	00000002	\N	D	\N	2012-10-09	0	\N
00000037	02	00000002	\N	D	\N	2012-10-10	0	\N
00000037	02	00000002	\N	D	\N	2012-10-11	0	\N
00000037	02	00000002	\N	F	\N	2012-10-12	0	\N
00000037	02	00000002	\N	F	\N	2012-10-13	0	\N
00000037	02	00000002	\N	D	\N	2012-10-14	0	\N
00000037	02	00000002	\N	D	\N	2012-10-15	0	\N
00000037	02	00000002	\N	D	\N	2012-10-16	0	\N
00000037	02	00000002	\N	D	\N	2012-10-17	0	\N
00000037	02	00000002	\N	D	\N	2012-10-18	0	\N
00000037	02	00000002	\N	F	\N	2012-10-19	0	\N
00000037	02	00000002	\N	F	\N	2012-10-20	0	\N
00000037	02	00000002	\N	D	\N	2012-10-21	0	\N
00000037	02	00000002	\N	D	\N	2012-10-22	0	\N
00000037	02	00000002	\N	D	\N	2012-10-23	0	\N
00000037	02	00000002	\N	D	\N	2012-10-24	0	\N
00000037	02	00000002	\N	D	\N	2012-10-25	0	\N
00000037	02	00000002	\N	F	\N	2012-10-26	0	\N
00000037	02	00000002	\N	F	\N	2012-10-27	0	\N
00000037	02	00000002	\N	D	\N	2012-10-28	0	\N
00000037	02	00000002	\N	D	\N	2012-10-29	0	\N
00000037	02	00000002	\N	D	\N	2012-10-30	0	\N
00000037	02	00000002	\N	D	\N	2012-10-31	0	\N
00000038	02	00000002	\N	D	\N	2012-10-01	0	\N
00000038	02	00000002	\N	D	\N	2012-10-02	0	\N
00000038	02	00000002	\N	D	\N	2012-10-03	0	\N
00000038	02	00000002	\N	D	\N	2012-10-04	0	\N
00000038	02	00000002	\N	F	\N	2012-10-05	0	\N
00000038	02	00000002	\N	F	\N	2012-10-06	0	\N
00000038	02	00000002	\N	D	\N	2012-10-07	0	\N
00000038	02	00000002	\N	D	\N	2012-10-08	0	\N
00000038	02	00000002	\N	D	\N	2012-10-09	0	\N
00000038	02	00000002	\N	D	\N	2012-10-10	0	\N
00000038	02	00000002	\N	D	\N	2012-10-11	0	\N
00000038	02	00000002	\N	F	\N	2012-10-12	0	\N
00000038	02	00000002	\N	F	\N	2012-10-13	0	\N
00000038	02	00000002	\N	D	\N	2012-10-14	0	\N
00000038	02	00000002	\N	D	\N	2012-10-15	0	\N
00000038	02	00000002	\N	D	\N	2012-10-16	0	\N
00000038	02	00000002	\N	D	\N	2012-10-17	0	\N
00000038	02	00000002	\N	D	\N	2012-10-18	0	\N
00000038	02	00000002	\N	F	\N	2012-10-19	0	\N
00000038	02	00000002	\N	F	\N	2012-10-20	0	\N
00000038	02	00000002	\N	D	\N	2012-10-21	0	\N
00000038	02	00000002	\N	D	\N	2012-10-22	0	\N
00000038	02	00000002	\N	D	\N	2012-10-23	0	\N
00000038	02	00000002	\N	D	\N	2012-10-24	0	\N
00000038	02	00000002	\N	D	\N	2012-10-25	0	\N
00000038	02	00000002	\N	F	\N	2012-10-26	0	\N
00000038	02	00000002	\N	F	\N	2012-10-27	0	\N
00000038	02	00000002	\N	D	\N	2012-10-28	0	\N
00000038	02	00000002	\N	D	\N	2012-10-29	0	\N
00000038	02	00000002	\N	D	\N	2012-10-30	0	\N
00000038	02	00000002	\N	D	\N	2012-10-31	0	\N
00000039	02	00000002	\N	D	\N	2012-10-01	0	\N
00000039	02	00000002	\N	D	\N	2012-10-02	0	\N
00000039	02	00000002	\N	D	\N	2012-10-03	0	\N
00000039	02	00000002	\N	D	\N	2012-10-04	0	\N
00000039	02	00000002	\N	F	\N	2012-10-05	0	\N
00000039	02	00000002	\N	F	\N	2012-10-06	0	\N
00000039	02	00000002	\N	D	\N	2012-10-07	0	\N
00000039	02	00000002	\N	D	\N	2012-10-08	0	\N
00000039	02	00000002	\N	D	\N	2012-10-09	0	\N
00000039	02	00000002	\N	D	\N	2012-10-10	0	\N
00000039	02	00000002	\N	D	\N	2012-10-11	0	\N
00000039	02	00000002	\N	F	\N	2012-10-12	0	\N
00000039	02	00000002	\N	F	\N	2012-10-13	0	\N
00000039	02	00000002	\N	D	\N	2012-10-14	0	\N
00000039	02	00000002	\N	D	\N	2012-10-15	0	\N
00000039	02	00000002	\N	D	\N	2012-10-16	0	\N
00000039	02	00000002	\N	D	\N	2012-10-17	0	\N
00000039	02	00000002	\N	D	\N	2012-10-18	0	\N
00000039	02	00000002	\N	F	\N	2012-10-19	0	\N
00000039	02	00000002	\N	F	\N	2012-10-20	0	\N
00000039	02	00000002	\N	D	\N	2012-10-21	0	\N
00000039	02	00000002	\N	D	\N	2012-10-22	0	\N
00000039	02	00000002	\N	D	\N	2012-10-23	0	\N
00000039	02	00000002	\N	D	\N	2012-10-24	0	\N
00000039	02	00000002	\N	D	\N	2012-10-25	0	\N
00000039	02	00000002	\N	F	\N	2012-10-26	0	\N
00000039	02	00000002	\N	F	\N	2012-10-27	0	\N
00000039	02	00000002	\N	D	\N	2012-10-28	0	\N
00000039	02	00000002	\N	D	\N	2012-10-29	0	\N
00000039	02	00000002	\N	D	\N	2012-10-30	0	\N
00000039	02	00000002	\N	D	\N	2012-10-31	0	\N
00000040	02	00000002	\N	D	\N	2012-10-01	0	\N
00000040	02	00000002	\N	D	\N	2012-10-02	0	\N
00000040	02	00000002	\N	D	\N	2012-10-03	0	\N
00000040	02	00000002	\N	D	\N	2012-10-04	0	\N
00000040	02	00000002	\N	F	\N	2012-10-05	0	\N
00000040	02	00000002	\N	F	\N	2012-10-06	0	\N
00000040	02	00000002	\N	D	\N	2012-10-07	0	\N
00000040	02	00000002	\N	D	\N	2012-10-08	0	\N
00000040	02	00000002	\N	D	\N	2012-10-09	0	\N
00000040	02	00000002	\N	D	\N	2012-10-10	0	\N
00000040	02	00000002	\N	D	\N	2012-10-11	0	\N
00000040	02	00000002	\N	F	\N	2012-10-12	0	\N
00000040	02	00000002	\N	F	\N	2012-10-13	0	\N
00000040	02	00000002	\N	D	\N	2012-10-14	0	\N
00000040	02	00000002	\N	D	\N	2012-10-15	0	\N
00000040	02	00000002	\N	D	\N	2012-10-16	0	\N
00000040	02	00000002	\N	D	\N	2012-10-17	0	\N
00000040	02	00000002	\N	D	\N	2012-10-18	0	\N
00000040	02	00000002	\N	F	\N	2012-10-19	0	\N
00000040	02	00000002	\N	F	\N	2012-10-20	0	\N
00000040	02	00000002	\N	D	\N	2012-10-21	0	\N
00000040	02	00000002	\N	D	\N	2012-10-22	0	\N
00000040	02	00000002	\N	D	\N	2012-10-23	0	\N
00000040	02	00000002	\N	D	\N	2012-10-24	0	\N
00000040	02	00000002	\N	D	\N	2012-10-25	0	\N
00000040	02	00000002	\N	F	\N	2012-10-26	0	\N
00000013	05	00000002	\N	F	\N	2013-03-01	1	\N
00000013	05	00000002	\N	F	\N	2013-03-02	1	\N
00000013	05	00000002	\N	D	\N	2013-03-03	1	\N
00000013	05	00000002	\N	D	\N	2013-03-04	1	\N
00000013	05	00000002	\N	D	\N	2013-03-05	1	\N
00000013	05	00000002	\N	D	\N	2013-03-06	1	\N
00000013	05	00000002	\N	D	\N	2013-03-07	1	\N
00000013	05	00000002	\N	F	\N	2013-03-08	1	\N
00000013	05	00000002	\N	F	\N	2013-03-09	1	\N
00000013	05	00000002	\N	D	\N	2013-03-10	1	\N
00000013	05	00000002	\N	D	\N	2013-03-11	1	\N
00000013	05	00000002	\N	D	\N	2013-03-12	1	\N
00000013	05	00000002	\N	D	\N	2013-03-13	1	\N
00000013	05	00000002	\N	D	\N	2013-03-14	1	\N
00000013	05	00000002	\N	F	\N	2013-03-15	1	\N
00000013	05	00000002	\N	F	\N	2013-03-16	1	\N
00000013	05	00000002	\N	D	\N	2013-03-17	1	\N
00000013	05	00000002	\N	D	\N	2013-03-18	1	\N
00000013	05	00000002	\N	D	\N	2013-03-19	1	\N
00000013	05	00000002	\N	D	\N	2013-03-20	1	\N
00000013	05	00000002	\N	D	\N	2013-03-21	1	\N
00000013	05	00000002	\N	F	\N	2013-03-22	1	\N
00000013	05	00000002	\N	F	\N	2013-03-23	1	\N
00000013	05	00000002	\N	D	\N	2013-03-24	1	\N
00000013	05	00000002	\N	D	\N	2013-03-25	1	\N
00000013	05	00000002	\N	D	\N	2013-03-26	1	\N
00000013	05	00000002	\N	D	\N	2013-03-27	1	\N
00000013	05	00000002	\N	D	\N	2013-03-28	1	\N
00000013	05	00000002	\N	F	\N	2013-03-29	1	\N
00000013	05	00000002	\N	F	\N	2013-03-30	1	\N
00000013	05	00000002	\N	D	\N	2013-03-31	1	\N
00000014	05	00000002	\N	F	\N	2013-03-01	1	\N
00000014	05	00000002	\N	F	\N	2013-03-02	1	\N
00000014	05	00000002	\N	D	\N	2013-03-03	1	\N
00000014	05	00000002	\N	D	\N	2013-03-04	1	\N
00000014	05	00000002	\N	D	\N	2013-03-05	1	\N
00000014	05	00000002	\N	D	\N	2013-03-06	1	\N
00000014	05	00000002	\N	D	\N	2013-03-07	1	\N
00000014	05	00000002	\N	F	\N	2013-03-08	1	\N
00000014	05	00000002	\N	F	\N	2013-03-09	1	\N
00000014	05	00000002	\N	D	\N	2013-03-10	1	\N
00000014	05	00000002	\N	D	\N	2013-03-11	1	\N
00000014	05	00000002	\N	D	\N	2013-03-12	1	\N
00000014	05	00000002	\N	D	\N	2013-03-13	1	\N
00000014	05	00000002	\N	D	\N	2013-03-14	1	\N
00000014	05	00000002	\N	F	\N	2013-03-15	1	\N
00000014	05	00000002	\N	F	\N	2013-03-16	1	\N
00000014	05	00000002	\N	D	\N	2013-03-17	1	\N
00000014	05	00000002	\N	D	\N	2013-03-18	1	\N
00000014	05	00000002	\N	D	\N	2013-03-19	1	\N
00000014	05	00000002	\N	D	\N	2013-03-20	1	\N
00000014	05	00000002	\N	D	\N	2013-03-21	1	\N
00000014	05	00000002	\N	F	\N	2013-03-22	1	\N
00000014	05	00000002	\N	F	\N	2013-03-23	1	\N
00000014	05	00000002	\N	D	\N	2013-03-24	1	\N
00000014	05	00000002	\N	D	\N	2013-03-25	1	\N
00000014	05	00000002	\N	D	\N	2013-03-26	1	\N
00000014	05	00000002	\N	D	\N	2013-03-27	1	\N
00000014	05	00000002	\N	D	\N	2013-03-28	1	\N
00000014	05	00000002	\N	F	\N	2013-03-29	1	\N
00000014	05	00000002	\N	F	\N	2013-03-30	1	\N
00000014	05	00000002	\N	D	\N	2013-03-31	1	\N
00000015	05	00000002	\N	F	\N	2013-03-01	1	\N
00000015	05	00000002	\N	F	\N	2013-03-02	1	\N
00000015	05	00000002	\N	D	\N	2013-03-03	1	\N
00000015	05	00000002	\N	D	\N	2013-03-04	1	\N
00000015	05	00000002	\N	D	\N	2013-03-05	1	\N
00000015	05	00000002	\N	D	\N	2013-03-06	1	\N
00000015	05	00000002	\N	D	\N	2013-03-07	1	\N
00000015	05	00000002	\N	F	\N	2013-03-08	1	\N
00000015	05	00000002	\N	F	\N	2013-03-09	1	\N
00000015	05	00000002	\N	D	\N	2013-03-10	1	\N
00000015	05	00000002	\N	D	\N	2013-03-11	1	\N
00000015	05	00000002	\N	D	\N	2013-03-12	1	\N
00000015	05	00000002	\N	D	\N	2013-03-13	1	\N
00000015	05	00000002	\N	D	\N	2013-03-14	1	\N
00000015	05	00000002	\N	F	\N	2013-03-15	1	\N
00000015	05	00000002	\N	F	\N	2013-03-16	1	\N
00000015	05	00000002	\N	D	\N	2013-03-17	1	\N
00000015	05	00000002	\N	D	\N	2013-03-18	1	\N
00000015	05	00000002	\N	D	\N	2013-03-19	1	\N
00000015	05	00000002	\N	D	\N	2013-03-20	1	\N
00000015	05	00000002	\N	D	\N	2013-03-21	1	\N
00000015	05	00000002	\N	F	\N	2013-03-22	1	\N
00000015	05	00000002	\N	F	\N	2013-03-23	1	\N
00000015	05	00000002	\N	D	\N	2013-03-24	1	\N
00000015	05	00000002	\N	D	\N	2013-03-25	1	\N
00000015	05	00000002	\N	D	\N	2013-03-26	1	\N
00000015	05	00000002	\N	D	\N	2013-03-27	1	\N
00000015	05	00000002	\N	D	\N	2013-03-28	1	\N
00000015	05	00000002	\N	F	\N	2013-03-29	1	\N
00000015	05	00000002	\N	F	\N	2013-03-30	1	\N
00000015	05	00000002	\N	D	\N	2013-03-31	1	\N
00000016	05	00000002	\N	F	\N	2013-03-01	1	\N
00000016	05	00000002	\N	F	\N	2013-03-02	1	\N
00000016	05	00000002	\N	D	\N	2013-03-03	1	\N
00000016	05	00000002	\N	D	\N	2013-03-04	1	\N
00000016	05	00000002	\N	D	\N	2013-03-05	1	\N
00000016	05	00000002	\N	D	\N	2013-03-06	1	\N
00000016	05	00000002	\N	D	\N	2013-03-07	1	\N
00000016	05	00000002	\N	F	\N	2013-03-08	1	\N
00000016	05	00000002	\N	F	\N	2013-03-09	1	\N
00000016	05	00000002	\N	D	\N	2013-03-10	1	\N
00000016	05	00000002	\N	D	\N	2013-03-11	1	\N
00000016	05	00000002	\N	D	\N	2013-03-12	1	\N
00000016	05	00000002	\N	D	\N	2013-03-13	1	\N
00000016	05	00000002	\N	D	\N	2013-03-14	1	\N
00000016	05	00000002	\N	F	\N	2013-03-15	1	\N
00000016	05	00000002	\N	F	\N	2013-03-16	1	\N
00000016	05	00000002	\N	D	\N	2013-03-17	1	\N
00000016	05	00000002	\N	D	\N	2013-03-18	1	\N
00000016	05	00000002	\N	D	\N	2013-03-19	1	\N
00000016	05	00000002	\N	D	\N	2013-03-20	1	\N
00000016	05	00000002	\N	D	\N	2013-03-21	1	\N
00000016	05	00000002	\N	F	\N	2013-03-22	1	\N
00000016	05	00000002	\N	F	\N	2013-03-23	1	\N
00000016	05	00000002	\N	D	\N	2013-03-24	1	\N
00000016	05	00000002	\N	D	\N	2013-03-25	1	\N
00000016	05	00000002	\N	D	\N	2013-03-26	1	\N
00000016	05	00000002	\N	D	\N	2013-03-27	1	\N
00000016	05	00000002	\N	D	\N	2013-03-28	1	\N
00000016	05	00000002	\N	F	\N	2013-03-29	1	\N
00000016	05	00000002	\N	F	\N	2013-03-30	1	\N
00000016	05	00000002	\N	D	\N	2013-03-31	1	\N
00000040	02	00000002	\N	F	\N	2012-10-27	0	\N
00000040	02	00000002	\N	D	\N	2012-10-28	0	\N
00000040	02	00000002	\N	D	\N	2012-10-29	0	\N
00000040	02	00000002	\N	D	\N	2012-10-30	0	\N
00000040	02	00000002	\N	D	\N	2012-10-31	0	\N
00000041	02	00000002	\N	D	\N	2012-10-01	0	\N
00000041	02	00000002	\N	D	\N	2012-10-02	0	\N
00000041	02	00000002	\N	D	\N	2012-10-03	0	\N
00000041	02	00000002	\N	D	\N	2012-10-04	0	\N
00000041	02	00000002	\N	F	\N	2012-10-05	0	\N
00000041	02	00000002	\N	F	\N	2012-10-06	0	\N
00000041	02	00000002	\N	D	\N	2012-10-07	0	\N
00000041	02	00000002	\N	D	\N	2012-10-08	0	\N
00000041	02	00000002	\N	D	\N	2012-10-09	0	\N
00000041	02	00000002	\N	D	\N	2012-10-10	0	\N
00000041	02	00000002	\N	D	\N	2012-10-11	0	\N
00000041	02	00000002	\N	F	\N	2012-10-12	0	\N
00000041	02	00000002	\N	F	\N	2012-10-13	0	\N
00000041	02	00000002	\N	D	\N	2012-10-14	0	\N
00000041	02	00000002	\N	D	\N	2012-10-15	0	\N
00000041	02	00000002	\N	D	\N	2012-10-16	0	\N
00000041	02	00000002	\N	D	\N	2012-10-17	0	\N
00000041	02	00000002	\N	D	\N	2012-10-18	0	\N
00000041	02	00000002	\N	F	\N	2012-10-19	0	\N
00000041	02	00000002	\N	F	\N	2012-10-20	0	\N
00000041	02	00000002	\N	D	\N	2012-10-21	0	\N
00000041	02	00000002	\N	D	\N	2012-10-22	0	\N
00000041	02	00000002	\N	D	\N	2012-10-23	0	\N
00000041	02	00000002	\N	D	\N	2012-10-24	0	\N
00000041	02	00000002	\N	D	\N	2012-10-25	0	\N
00000041	02	00000002	\N	F	\N	2012-10-26	0	\N
00000041	02	00000002	\N	F	\N	2012-10-27	0	\N
00000041	02	00000002	\N	D	\N	2012-10-28	0	\N
00000041	02	00000002	\N	D	\N	2012-10-29	0	\N
00000041	02	00000002	\N	D	\N	2012-10-30	0	\N
00000041	02	00000002	\N	D	\N	2012-10-31	0	\N
00000017	02	00000002	\N	D	\N	2012-11-01	1	\N
00000017	02	00000002	\N	D	\N	2012-11-08	1	\N
00000018	02	00000002	\N	D	\N	2012-11-07	1	\N
00000018	02	00000002	\N	D	\N	2012-11-08	1	\N
00000018	02	00000002	\N	F	\N	2012-11-09	1	\N
00000018	02	00000002	\N	F	\N	2012-11-10	1	\N
00000018	02	00000002	\N	D	\N	2012-11-11	1	\N
00000018	02	00000002	\N	D	\N	2012-11-12	1	\N
00000018	02	00000002	\N	D	\N	2012-11-13	1	\N
00000018	02	00000002	\N	D	\N	2012-11-14	1	\N
00000076	01	00000002	\N	D	\N	2012-11-01	1	\N
00000076	01	00000002	\N	F	\N	2012-11-02	1	\N
00000076	01	00000002	\N	F	\N	2012-11-03	1	\N
00000076	01	00000002	\N	D	\N	2012-11-04	1	\N
00000076	01	00000002	\N	D	\N	2012-11-06	1	\N
00000076	01	00000002	\N	D	\N	2012-11-08	1	\N
00000076	01	00000002	\N	F	\N	2012-11-09	1	\N
00000076	01	00000002	\N	D	0000000011	2012-11-05	R	\N
00000076	01	00000002	\N	D	0000000012	2012-11-07	R	\N
00000022	02	00000002	\N	D	\N	2012-11-14	1	\N
00000022	02	00000002	\N	F	\N	2012-11-17	1	\N
00000022	02	00000002	\N	D	\N	2012-11-18	1	\N
00000022	02	00000002	\N	D	\N	2012-11-19	1	\N
00000022	02	00000002	\N	D	\N	2012-11-20	1	\N
00000022	02	00000002	\N	D	\N	2012-11-21	1	\N
00000022	02	00000002	\N	D	\N	2012-11-22	1	\N
00000076	01	00000002	\N	F	\N	2012-11-10	1	\N
00000076	01	00000002	\N	D	\N	2012-11-11	1	\N
00000076	01	00000002	\N	D	\N	2012-11-12	1	\N
00000076	01	00000002	\N	D	\N	2012-11-13	1	\N
00000076	01	00000002	\N	F	\N	2012-11-16	1	\N
00000076	01	00000002	\N	F	\N	2012-11-17	1	\N
00000076	01	00000002	\N	D	\N	2012-11-18	1	\N
00000076	01	00000002	\N	D	\N	2012-11-19	1	\N
00000076	01	00000002	\N	D	\N	2012-11-20	1	\N
00000076	01	00000002	\N	D	\N	2012-11-21	1	\N
00000076	01	00000002	\N	D	\N	2012-11-22	1	\N
00000076	01	00000002	\N	F	\N	2012-11-23	1	\N
00000076	01	00000002	\N	F	\N	2012-11-24	1	\N
00000076	01	00000002	\N	D	\N	2012-11-25	1	\N
00000076	01	00000002	\N	D	\N	2012-11-26	1	\N
00000076	01	00000002	\N	D	\N	2012-11-27	1	\N
00000076	01	00000002	\N	D	\N	2012-11-28	1	\N
00000076	01	00000002	\N	D	\N	2012-11-29	1	\N
00000076	01	00000002	\N	F	\N	2012-11-30	1	\N
00000026	02	00000002	\N	D	\N	2012-11-12	1	\N
00000026	02	00000002	\N	D	\N	2012-11-13	1	\N
00000026	02	00000002	\N	D	\N	2012-11-14	1	\N
00000026	02	00000002	\N	F	\N	2012-11-17	1	\N
00000026	02	00000002	\N	D	\N	2012-11-18	1	\N
00000026	02	00000002	\N	D	\N	2012-11-19	1	\N
00000026	02	00000002	\N	D	\N	2012-11-20	1	\N
00000026	02	00000002	\N	D	\N	2012-11-21	1	\N
00000076	01	00000002	\N	F	\N	2012-12-01	1	\N
00000076	01	00000002	\N	D	\N	2012-12-02	1	\N
00000076	01	00000002	\N	D	\N	2012-12-03	1	\N
00000076	01	00000002	\N	D	\N	2012-12-04	1	\N
00000076	01	00000002	\N	D	\N	2012-12-05	1	\N
00000076	01	00000002	\N	D	\N	2012-12-06	1	\N
00000076	01	00000002	\N	F	\N	2012-12-07	1	\N
00000076	01	00000002	\N	F	\N	2012-12-08	1	\N
00000076	01	00000002	\N	D	\N	2012-12-09	1	\N
00000076	01	00000002	\N	D	\N	2012-12-10	1	\N
00000076	01	00000002	\N	D	\N	2012-12-11	1	\N
00000076	01	00000002	\N	D	\N	2012-12-12	1	\N
00000076	01	00000002	\N	D	\N	2012-12-13	1	\N
00000076	01	00000002	\N	F	\N	2012-12-14	1	\N
00000076	01	00000002	\N	F	\N	2012-12-15	1	\N
00000076	01	00000002	\N	D	\N	2012-12-16	1	\N
00000076	01	00000002	\N	D	\N	2012-12-17	1	\N
00000076	01	00000002	\N	D	\N	2012-12-18	1	\N
00000076	01	00000002	\N	D	\N	2012-12-19	1	\N
00000076	01	00000002	\N	D	\N	2012-12-20	1	\N
00000076	01	00000002	\N	F	\N	2012-12-21	1	\N
00000076	01	00000002	\N	F	\N	2012-12-22	1	\N
00000076	01	00000002	\N	D	\N	2012-12-23	1	\N
00000076	01	00000002	\N	D	\N	2012-12-24	1	\N
00000076	01	00000002	\N	D	\N	2012-12-25	1	\N
00000076	01	00000002	\N	D	\N	2012-12-26	1	\N
00000076	01	00000002	\N	D	\N	2012-12-27	1	\N
00000076	01	00000002	\N	F	\N	2012-12-28	1	\N
00000076	01	00000002	\N	F	\N	2012-12-29	1	\N
00000076	01	00000002	\N	D	\N	2012-12-30	1	\N
00000029	02	00000002	\N	D	\N	2012-11-29	1	\N
00000029	02	00000002	\N	F	\N	2012-11-30	1	\N
00000030	02	00000002	\N	D	\N	2012-11-01	1	\N
00000030	02	00000002	\N	F	\N	2012-11-02	1	\N
00000030	02	00000002	\N	F	\N	2012-11-03	1	\N
00000030	02	00000002	\N	D	\N	2012-11-04	1	\N
00000076	01	00000002	\N	D	\N	2012-12-31	1	\N
00000013	05	00000002	\N	D	0000000008	2012-11-14	R	\N
00000033	02	00000002	\N	D	\N	2012-11-05	1	\N
00000033	02	00000002	\N	D	\N	2012-11-06	1	\N
00000033	02	00000002	\N	D	\N	2012-11-07	1	\N
00000033	02	00000002	\N	D	\N	2012-11-08	1	\N
00000033	02	00000002	\N	F	\N	2012-11-09	1	\N
00000033	02	00000002	\N	F	\N	2012-11-10	1	\N
00000033	02	00000002	\N	D	\N	2012-11-11	1	\N
00000037	02	00000002	\N	F	\N	2012-11-09	1	\N
00000037	02	00000002	\N	F	\N	2012-11-10	1	\N
00000037	02	00000002	\N	D	\N	2012-11-11	1	\N
00000037	02	00000002	\N	D	\N	2012-11-12	1	\N
00000037	02	00000002	\N	D	\N	2012-11-13	1	\N
00000037	02	00000002	\N	D	\N	2012-11-14	1	\N
00000037	02	00000002	\N	F	\N	2012-11-17	1	\N
00000037	02	00000002	\N	D	\N	2012-11-18	1	\N
00000037	02	00000002	\N	D	\N	2012-11-19	1	\N
00000017	02	00000002	\N	F	\N	2012-12-01	1	\N
00000017	02	00000002	\N	D	\N	2012-12-02	1	\N
00000017	02	00000002	\N	D	\N	2012-12-03	1	\N
00000017	02	00000002	\N	D	\N	2012-12-04	1	\N
00000017	02	00000002	\N	D	\N	2012-12-05	1	\N
00000017	02	00000002	\N	D	\N	2012-12-06	1	\N
00000017	02	00000002	\N	F	\N	2012-12-07	1	\N
00000017	02	00000002	\N	F	\N	2012-12-08	1	\N
00000017	02	00000002	\N	D	\N	2012-12-09	1	\N
00000017	02	00000002	\N	D	\N	2012-12-10	1	\N
00000017	02	00000002	\N	D	\N	2012-12-11	1	\N
00000017	02	00000002	\N	D	\N	2012-12-12	1	\N
00000017	02	00000002	\N	D	\N	2012-12-13	1	\N
00000017	02	00000002	\N	F	\N	2012-12-14	1	\N
00000017	02	00000002	\N	F	\N	2012-12-15	1	\N
00000017	02	00000002	\N	D	\N	2012-12-16	1	\N
00000017	02	00000002	\N	D	\N	2012-12-17	1	\N
00000017	02	00000002	\N	D	\N	2012-12-18	1	\N
00000017	02	00000002	\N	F	\N	2012-12-22	1	\N
00000017	02	00000002	\N	D	\N	2012-12-23	1	\N
00000017	02	00000002	\N	D	\N	2012-12-24	1	\N
00000017	02	00000002	\N	D	\N	2012-12-25	1	\N
00000017	02	00000002	\N	D	\N	2012-12-26	1	\N
00000017	02	00000002	\N	D	\N	2012-12-27	1	\N
00000017	02	00000002	\N	F	\N	2012-12-28	1	\N
00000017	02	00000002	\N	F	\N	2012-12-29	1	\N
00000017	02	00000002	\N	D	\N	2012-12-30	1	\N
00000018	02	00000002	\N	F	\N	2012-12-01	1	\N
00000018	02	00000002	\N	D	\N	2012-12-02	1	\N
00000018	02	00000002	\N	D	\N	2012-12-03	1	\N
00000018	02	00000002	\N	D	\N	2012-12-04	1	\N
00000018	02	00000002	\N	D	\N	2012-12-05	1	\N
00000018	02	00000002	\N	D	\N	2012-12-06	1	\N
00000018	02	00000002	\N	F	\N	2012-12-07	1	\N
00000018	02	00000002	\N	F	\N	2012-12-08	1	\N
00000018	02	00000002	\N	D	\N	2012-12-09	1	\N
00000018	02	00000002	\N	D	\N	2012-12-10	1	\N
00000018	02	00000002	\N	D	\N	2012-12-11	1	\N
00000018	02	00000002	\N	D	\N	2012-12-12	1	\N
00000018	02	00000002	\N	D	\N	2012-12-13	1	\N
00000018	02	00000002	\N	F	\N	2012-12-14	1	\N
00000014	05	00000002	\N	D	\N	2012-11-06	1	\N
00000014	05	00000002	\N	D	\N	2012-11-07	1	\N
00000014	05	00000002	\N	D	\N	2012-11-08	1	\N
00000014	05	00000002	\N	F	\N	2012-11-09	1	\N
00000014	05	00000002	\N	F	\N	2012-11-10	1	\N
00000014	05	00000002	\N	D	\N	2012-11-11	1	\N
00000014	05	00000002	\N	D	\N	2012-11-12	1	\N
00000014	05	00000002	\N	D	\N	2012-11-13	1	\N
00000014	05	00000002	\N	D	\N	2012-11-14	1	\N
00000014	05	00000002	\N	F	\N	2012-11-17	1	\N
00000014	05	00000002	\N	D	\N	2012-11-18	1	\N
00000014	05	00000002	\N	D	\N	2012-11-19	1	\N
00000014	05	00000002	\N	D	\N	2012-11-20	1	\N
00000014	05	00000002	\N	D	\N	2012-11-21	1	\N
00000014	05	00000002	\N	D	\N	2012-11-22	1	\N
00000014	05	00000002	\N	F	\N	2012-11-23	1	\N
00000014	05	00000002	\N	F	\N	2012-11-24	1	\N
00000014	05	00000002	\N	D	\N	2012-11-25	1	\N
00000014	05	00000002	\N	D	\N	2012-11-26	1	\N
00000014	05	00000002	\N	D	\N	2012-11-27	1	\N
00000014	05	00000002	\N	D	\N	2012-11-28	1	\N
00000014	05	00000002	\N	D	\N	2012-11-29	1	\N
00000014	05	00000002	\N	F	\N	2012-11-30	1	\N
00000015	05	00000002	\N	F	\N	2012-11-03	1	\N
00000015	05	00000002	\N	D	\N	2012-11-04	1	\N
00000015	05	00000002	\N	D	\N	2012-11-05	1	\N
00000015	05	00000002	\N	D	\N	2012-11-06	1	\N
00000015	05	00000002	\N	D	\N	2012-11-07	1	\N
00000015	05	00000002	\N	D	\N	2012-11-08	1	\N
00000015	05	00000002	\N	F	\N	2012-11-09	1	\N
00000015	05	00000002	\N	F	\N	2012-11-10	1	\N
00000015	05	00000002	\N	D	\N	2012-11-11	1	\N
00000015	05	00000002	\N	D	\N	2012-11-12	1	\N
00000015	05	00000002	\N	D	\N	2012-11-13	1	\N
00000015	05	00000002	\N	D	\N	2012-11-14	1	\N
00000015	05	00000002	\N	F	\N	2012-11-17	1	\N
00000015	05	00000002	\N	D	\N	2012-11-18	1	\N
00000015	05	00000002	\N	D	\N	2012-11-19	1	\N
00000015	05	00000002	\N	D	\N	2012-11-20	1	\N
00000015	05	00000002	\N	D	\N	2012-11-21	1	\N
00000015	05	00000002	\N	D	\N	2012-11-22	1	\N
00000015	05	00000002	\N	F	\N	2012-11-23	1	\N
00000015	05	00000002	\N	F	\N	2012-11-24	1	\N
00000015	05	00000002	\N	D	\N	2012-11-25	1	\N
00000015	05	00000002	\N	D	\N	2012-11-26	1	\N
00000015	05	00000002	\N	D	\N	2012-11-27	1	\N
00000015	05	00000002	\N	D	\N	2012-11-28	1	\N
00000015	05	00000002	\N	D	\N	2012-11-29	1	\N
00000015	05	00000002	\N	F	\N	2012-11-30	1	\N
00000016	05	00000002	\N	F	\N	2012-11-03	1	\N
00000016	05	00000002	\N	D	\N	2012-11-04	1	\N
00000016	05	00000002	\N	D	\N	2012-11-05	1	\N
00000016	05	00000002	\N	D	\N	2012-11-06	1	\N
00000016	05	00000002	\N	D	\N	2012-11-07	1	\N
00000016	05	00000002	\N	D	\N	2012-11-08	1	\N
00000016	05	00000002	\N	F	\N	2012-11-09	1	\N
00000016	05	00000002	\N	F	\N	2012-11-10	1	\N
00000016	05	00000002	\N	D	\N	2012-11-11	1	\N
00000016	05	00000002	\N	D	\N	2012-11-12	1	\N
00000016	05	00000002	\N	D	\N	2012-11-13	1	\N
00000041	02	00000002	\N	F	\N	2012-11-17	1	\N
00000041	02	00000002	\N	D	\N	2012-11-18	1	\N
00000041	02	00000002	\N	D	\N	2012-11-19	1	\N
00000041	02	00000002	\N	D	\N	2012-11-20	1	\N
00000017	02	00000002	\N	D	0000000036	2012-12-31	R	\N
00000017	02	00000002	\N	D	\N	2012-12-21	1	\N
00000017	02	00000002	\N	D	0000000035	2012-12-19	R	\N
00000017	02	00000002	\N	D	0000000035	2012-12-20	R	\N
00000018	02	00000002	\N	F	\N	2012-12-15	1	\N
00000018	02	00000002	\N	D	\N	2012-12-16	1	\N
00000018	02	00000002	\N	D	\N	2012-12-17	1	\N
00000016	05	00000002	\N	D	\N	2012-11-14	1	\N
00000016	05	00000002	\N	F	\N	2012-11-17	1	\N
00000016	05	00000002	\N	D	\N	2012-11-18	1	\N
00000016	05	00000002	\N	D	\N	2012-11-19	1	\N
00000016	05	00000002	\N	D	\N	2012-11-20	1	\N
00000018	02	00000002	\N	F	\N	2012-12-22	1	\N
00000018	02	00000002	\N	D	\N	2012-12-23	1	\N
00000018	02	00000002	\N	D	\N	2012-12-24	1	\N
00000018	02	00000002	\N	D	\N	2012-12-25	1	\N
00000018	02	00000002	\N	D	\N	2012-12-26	1	\N
00000018	02	00000002	\N	D	\N	2012-12-27	1	\N
00000018	02	00000002	\N	F	\N	2012-12-28	1	\N
00000018	02	00000002	\N	F	\N	2012-12-29	1	\N
00000018	02	00000002	\N	D	\N	2012-12-30	1	\N
00000018	02	00000002	\N	D	\N	2012-12-31	1	\N
00000019	02	00000002	\N	F	\N	2012-12-01	1	\N
00000019	02	00000002	\N	D	\N	2012-12-02	1	\N
00000019	02	00000002	\N	D	\N	2012-12-03	1	\N
00000019	02	00000002	\N	D	\N	2012-12-04	1	\N
00000019	02	00000002	\N	D	\N	2012-12-05	1	\N
00000019	02	00000002	\N	D	\N	2012-12-06	1	\N
00000019	02	00000002	\N	F	\N	2012-12-07	1	\N
00000019	02	00000002	\N	F	\N	2012-12-08	1	\N
00000019	02	00000002	\N	D	\N	2012-12-09	1	\N
00000019	02	00000002	\N	D	\N	2012-12-10	1	\N
00000019	02	00000002	\N	D	\N	2012-12-11	1	\N
00000019	02	00000002	\N	D	\N	2012-12-12	1	\N
00000019	02	00000002	\N	D	\N	2012-12-13	1	\N
00000019	02	00000002	\N	F	\N	2012-12-14	1	\N
00000019	02	00000002	\N	F	\N	2012-12-15	1	\N
00000019	02	00000002	\N	D	\N	2012-12-16	1	\N
00000019	02	00000002	\N	D	\N	2012-12-17	1	\N
00000019	02	00000002	\N	D	\N	2012-12-18	1	\N
00000019	02	00000002	\N	F	\N	2012-12-22	1	\N
00000019	02	00000002	\N	D	\N	2012-12-23	1	\N
00000019	02	00000002	\N	D	\N	2012-12-24	1	\N
00000019	02	00000002	\N	D	\N	2012-12-25	1	\N
00000019	02	00000002	\N	D	\N	2012-12-26	1	\N
00000019	02	00000002	\N	D	\N	2012-12-27	1	\N
00000019	02	00000002	\N	F	\N	2012-12-28	1	\N
00000019	02	00000002	\N	F	\N	2012-12-29	1	\N
00000019	02	00000002	\N	D	\N	2012-12-30	1	\N
00000019	02	00000002	\N	D	\N	2012-12-31	1	\N
00000020	02	00000002	\N	F	\N	2012-12-01	1	\N
00000020	02	00000002	\N	D	\N	2012-12-02	1	\N
00000020	02	00000002	\N	D	\N	2012-12-03	1	\N
00000020	02	00000002	\N	D	\N	2012-12-04	1	\N
00000020	02	00000002	\N	D	\N	2012-12-05	1	\N
00000020	02	00000002	\N	D	\N	2012-12-06	1	\N
00000020	02	00000002	\N	F	\N	2012-12-07	1	\N
00000020	02	00000002	\N	F	\N	2012-12-08	1	\N
00000020	02	00000002	\N	D	\N	2012-12-09	1	\N
00000020	02	00000002	\N	D	\N	2012-12-10	1	\N
00000020	02	00000002	\N	D	\N	2012-12-11	1	\N
00000020	02	00000002	\N	D	\N	2012-12-12	1	\N
00000020	02	00000002	\N	D	\N	2012-12-13	1	\N
00000020	02	00000002	\N	F	\N	2012-12-14	1	\N
00000020	02	00000002	\N	F	\N	2012-12-15	1	\N
00000020	02	00000002	\N	D	\N	2012-12-16	1	\N
00000020	02	00000002	\N	D	\N	2012-12-17	1	\N
00000020	02	00000002	\N	D	\N	2012-12-18	1	\N
00000020	02	00000002	\N	F	\N	2012-12-22	1	\N
00000020	02	00000002	\N	D	\N	2012-12-23	1	\N
00000020	02	00000002	\N	D	\N	2012-12-24	1	\N
00000020	02	00000002	\N	D	\N	2012-12-25	1	\N
00000020	02	00000002	\N	D	\N	2012-12-26	1	\N
00000020	02	00000002	\N	D	\N	2012-12-27	1	\N
00000020	02	00000002	\N	F	\N	2012-12-28	1	\N
00000020	02	00000002	\N	F	\N	2012-12-29	1	\N
00000020	02	00000002	\N	D	\N	2012-12-30	1	\N
00000020	02	00000002	\N	D	\N	2012-12-31	1	\N
00000021	02	00000002	\N	F	\N	2012-12-01	1	\N
00000021	02	00000002	\N	D	\N	2012-12-02	1	\N
00000021	02	00000002	\N	D	\N	2012-12-03	1	\N
00000021	02	00000002	\N	D	\N	2012-12-04	1	\N
00000021	02	00000002	\N	D	\N	2012-12-05	1	\N
00000021	02	00000002	\N	D	\N	2012-12-06	1	\N
00000021	02	00000002	\N	F	\N	2012-12-07	1	\N
00000021	02	00000002	\N	F	\N	2012-12-08	1	\N
00000021	02	00000002	\N	D	\N	2012-12-09	1	\N
00000021	02	00000002	\N	D	\N	2012-12-10	1	\N
00000021	02	00000002	\N	D	\N	2012-12-11	1	\N
00000021	02	00000002	\N	D	\N	2012-12-12	1	\N
00000021	02	00000002	\N	D	\N	2012-12-13	1	\N
00000021	02	00000002	\N	F	\N	2012-12-14	1	\N
00000021	02	00000002	\N	F	\N	2012-12-15	1	\N
00000021	02	00000002	\N	D	\N	2012-12-16	1	\N
00000021	02	00000002	\N	D	\N	2012-12-17	1	\N
00000021	02	00000002	\N	D	\N	2012-12-18	1	\N
00000021	02	00000002	\N	F	\N	2012-12-22	1	\N
00000021	02	00000002	\N	D	\N	2012-12-23	1	\N
00000021	02	00000002	\N	D	\N	2012-12-24	1	\N
00000021	02	00000002	\N	D	\N	2012-12-25	1	\N
00000021	02	00000002	\N	D	\N	2012-12-26	1	\N
00000021	02	00000002	\N	D	\N	2012-12-27	1	\N
00000021	02	00000002	\N	F	\N	2012-12-28	1	\N
00000021	02	00000002	\N	F	\N	2012-12-29	1	\N
00000021	02	00000002	\N	D	\N	2012-12-30	1	\N
00000021	02	00000002	\N	D	\N	2012-12-31	1	\N
00000022	02	00000002	\N	F	\N	2012-12-01	1	\N
00000022	02	00000002	\N	D	\N	2012-12-02	1	\N
00000022	02	00000002	\N	D	\N	2012-12-03	1	\N
00000022	02	00000002	\N	D	\N	2012-12-04	1	\N
00000022	02	00000002	\N	D	\N	2012-12-05	1	\N
00000022	02	00000002	\N	D	\N	2012-12-06	1	\N
00000022	02	00000002	\N	F	\N	2012-12-07	1	\N
00000022	02	00000002	\N	F	\N	2012-12-08	1	\N
00000022	02	00000002	\N	D	\N	2012-12-09	1	\N
00000022	02	00000002	\N	D	\N	2012-12-10	1	\N
00000022	02	00000002	\N	D	\N	2012-12-11	1	\N
00000022	02	00000002	\N	D	\N	2012-12-12	1	\N
00000022	02	00000002	\N	D	\N	2012-12-13	1	\N
00000019	02	00000002	\N	D	\N	2012-12-19	1	\N
00000019	02	00000002	\N	D	\N	2012-12-20	1	\N
00000018	02	00000002	\N	D	\N	2012-12-18	1	\N
00000019	02	00000002	\N	D	\N	2012-12-21	1	\N
00000020	02	00000002	\N	D	\N	2012-12-19	1	\N
00000022	02	00000002	\N	F	\N	2012-12-14	1	\N
00000022	02	00000002	\N	F	\N	2012-12-15	1	\N
00000022	02	00000002	\N	D	\N	2012-12-16	1	\N
00000022	02	00000002	\N	D	\N	2012-12-17	1	\N
00000022	02	00000002	\N	D	\N	2012-12-18	1	\N
00000022	02	00000002	\N	F	\N	2012-12-22	1	\N
00000022	02	00000002	\N	D	\N	2012-12-23	1	\N
00000022	02	00000002	\N	D	\N	2012-12-24	1	\N
00000022	02	00000002	\N	D	\N	2012-12-25	1	\N
00000022	02	00000002	\N	D	\N	2012-12-26	1	\N
00000022	02	00000002	\N	D	\N	2012-12-27	1	\N
00000022	02	00000002	\N	F	\N	2012-12-28	1	\N
00000022	02	00000002	\N	F	\N	2012-12-29	1	\N
00000022	02	00000002	\N	D	\N	2012-12-30	1	\N
00000022	02	00000002	\N	D	\N	2012-12-31	1	\N
00000023	02	00000002	\N	F	\N	2012-12-01	1	\N
00000023	02	00000002	\N	D	\N	2012-12-02	1	\N
00000076	01	00000002	\N	D	0000000009	2012-11-14	R	\N
00000023	02	00000002	\N	D	\N	2012-12-03	1	\N
00000023	02	00000002	\N	D	\N	2012-12-04	1	\N
00000023	02	00000002	\N	D	\N	2012-12-05	1	\N
00000023	02	00000002	\N	D	\N	2012-12-06	1	\N
00000023	02	00000002	\N	F	\N	2012-12-07	1	\N
00000023	02	00000002	\N	F	\N	2012-12-08	1	\N
00000023	02	00000002	\N	D	\N	2012-12-09	1	\N
00000023	02	00000002	\N	D	\N	2012-12-10	1	\N
00000023	02	00000002	\N	D	\N	2012-12-11	1	\N
00000023	02	00000002	\N	D	\N	2012-12-12	1	\N
00000023	02	00000002	\N	D	\N	2012-12-13	1	\N
00000023	02	00000002	\N	F	\N	2012-12-14	1	\N
00000023	02	00000002	\N	F	\N	2012-12-15	1	\N
00000023	02	00000002	\N	D	\N	2012-12-16	1	\N
00000023	02	00000002	\N	D	\N	2012-12-17	1	\N
00000023	02	00000002	\N	D	\N	2012-12-18	1	\N
00000023	02	00000002	\N	F	\N	2012-12-22	1	\N
00000023	02	00000002	\N	D	\N	2012-12-23	1	\N
00000023	02	00000002	\N	D	\N	2012-12-24	1	\N
00000023	02	00000002	\N	D	\N	2012-12-25	1	\N
00000023	02	00000002	\N	D	\N	2012-12-26	1	\N
00000023	02	00000002	\N	D	\N	2012-12-27	1	\N
00000023	02	00000002	\N	F	\N	2012-12-28	1	\N
00000023	02	00000002	\N	F	\N	2012-12-29	1	\N
00000023	02	00000002	\N	D	\N	2012-12-30	1	\N
00000023	02	00000002	\N	D	\N	2012-12-31	1	\N
00000024	02	00000002	\N	F	\N	2012-12-01	1	\N
00000024	02	00000002	\N	D	\N	2012-12-02	1	\N
00000024	02	00000002	\N	D	\N	2012-12-03	1	\N
00000024	02	00000002	\N	D	\N	2012-12-04	1	\N
00000024	02	00000002	\N	D	\N	2012-12-05	1	\N
00000024	02	00000002	\N	D	\N	2012-12-06	1	\N
00000024	02	00000002	\N	F	\N	2012-12-07	1	\N
00000024	02	00000002	\N	F	\N	2012-12-08	1	\N
00000024	02	00000002	\N	D	\N	2012-12-09	1	\N
00000024	02	00000002	\N	D	\N	2012-12-10	1	\N
00000024	02	00000002	\N	D	\N	2012-12-11	1	\N
00000024	02	00000002	\N	D	\N	2012-12-12	1	\N
00000024	02	00000002	\N	D	\N	2012-12-13	1	\N
00000024	02	00000002	\N	F	\N	2012-12-14	1	\N
00000024	02	00000002	\N	F	\N	2012-12-15	1	\N
00000024	02	00000002	\N	D	\N	2012-12-16	1	\N
00000024	02	00000002	\N	D	\N	2012-12-17	1	\N
00000024	02	00000002	\N	D	\N	2012-12-18	1	\N
00000024	02	00000002	\N	F	\N	2012-12-22	1	\N
00000024	02	00000002	\N	D	\N	2012-12-23	1	\N
00000024	02	00000002	\N	D	\N	2012-12-24	1	\N
00000024	02	00000002	\N	D	\N	2012-12-25	1	\N
00000024	02	00000002	\N	D	\N	2012-12-26	1	\N
00000024	02	00000002	\N	D	\N	2012-12-27	1	\N
00000024	02	00000002	\N	F	\N	2012-12-28	1	\N
00000024	02	00000002	\N	F	\N	2012-12-29	1	\N
00000024	02	00000002	\N	D	\N	2012-12-30	1	\N
00000024	02	00000002	\N	D	\N	2012-12-31	1	\N
00000025	02	00000002	\N	F	\N	2012-12-01	1	\N
00000025	02	00000002	\N	D	\N	2012-12-02	1	\N
00000025	02	00000002	\N	D	\N	2012-12-03	1	\N
00000025	02	00000002	\N	D	\N	2012-12-04	1	\N
00000025	02	00000002	\N	D	\N	2012-12-05	1	\N
00000025	02	00000002	\N	D	\N	2012-12-06	1	\N
00000025	02	00000002	\N	F	\N	2012-12-07	1	\N
00000025	02	00000002	\N	F	\N	2012-12-08	1	\N
00000025	02	00000002	\N	D	\N	2012-12-09	1	\N
00000025	02	00000002	\N	D	\N	2012-12-10	1	\N
00000025	02	00000002	\N	D	\N	2012-12-11	1	\N
00000025	02	00000002	\N	D	\N	2012-12-12	1	\N
00000025	02	00000002	\N	D	\N	2012-12-13	1	\N
00000025	02	00000002	\N	F	\N	2012-12-14	1	\N
00000025	02	00000002	\N	F	\N	2012-12-15	1	\N
00000025	02	00000002	\N	D	\N	2012-12-16	1	\N
00000025	02	00000002	\N	D	\N	2012-12-17	1	\N
00000025	02	00000002	\N	D	\N	2012-12-18	1	\N
00000025	02	00000002	\N	F	\N	2012-12-22	1	\N
00000025	02	00000002	\N	D	\N	2012-12-23	1	\N
00000025	02	00000002	\N	D	\N	2012-12-24	1	\N
00000025	02	00000002	\N	D	\N	2012-12-25	1	\N
00000025	02	00000002	\N	D	\N	2012-12-26	1	\N
00000025	02	00000002	\N	D	\N	2012-12-27	1	\N
00000025	02	00000002	\N	F	\N	2012-12-28	1	\N
00000025	02	00000002	\N	F	\N	2012-12-29	1	\N
00000025	02	00000002	\N	D	\N	2012-12-30	1	\N
00000025	02	00000002	\N	D	\N	2012-12-31	1	\N
00000026	02	00000002	\N	F	\N	2012-12-01	1	\N
00000026	02	00000002	\N	D	\N	2012-12-02	1	\N
00000026	02	00000002	\N	D	\N	2012-12-03	1	\N
00000026	02	00000002	\N	D	\N	2012-12-04	1	\N
00000026	02	00000002	\N	D	\N	2012-12-05	1	\N
00000026	02	00000002	\N	D	\N	2012-12-06	1	\N
00000026	02	00000002	\N	F	\N	2012-12-07	1	\N
00000026	02	00000002	\N	F	\N	2012-12-08	1	\N
00000026	02	00000002	\N	D	\N	2012-12-09	1	\N
00000026	02	00000002	\N	D	\N	2012-12-10	1	\N
00000026	02	00000002	\N	D	\N	2012-12-11	1	\N
00000026	02	00000002	\N	D	\N	2012-12-12	1	\N
00000026	02	00000002	\N	D	\N	2012-12-13	1	\N
00000026	02	00000002	\N	F	\N	2012-12-14	1	\N
00000026	02	00000002	\N	F	\N	2012-12-15	1	\N
00000026	02	00000002	\N	D	\N	2012-12-16	1	\N
00000026	02	00000002	\N	D	\N	2012-12-17	1	\N
00000026	02	00000002	\N	D	\N	2012-12-18	1	\N
00000026	02	00000002	\N	F	\N	2012-12-22	1	\N
00000026	02	00000002	\N	D	\N	2012-12-23	1	\N
00000026	02	00000002	\N	D	\N	2012-12-24	1	\N
00000026	02	00000002	\N	D	\N	2012-12-25	1	\N
00000026	02	00000002	\N	D	\N	2012-12-26	1	\N
00000026	02	00000002	\N	D	\N	2012-12-27	1	\N
00000026	02	00000002	\N	F	\N	2012-12-28	1	\N
00000026	02	00000002	\N	F	\N	2012-12-29	1	\N
00000026	02	00000002	\N	D	\N	2012-12-30	1	\N
00000026	02	00000002	\N	D	\N	2012-12-31	1	\N
00000027	02	00000002	\N	F	\N	2012-12-01	1	\N
00000027	02	00000002	\N	D	\N	2012-12-02	1	\N
00000027	02	00000002	\N	D	\N	2012-12-03	1	\N
00000027	02	00000002	\N	D	\N	2012-12-04	1	\N
00000027	02	00000002	\N	D	\N	2012-12-05	1	\N
00000027	02	00000002	\N	D	\N	2012-12-06	1	\N
00000027	02	00000002	\N	F	\N	2012-12-07	1	\N
00000027	02	00000002	\N	F	\N	2012-12-08	1	\N
00000027	02	00000002	\N	D	\N	2012-12-09	1	\N
00000027	02	00000002	\N	D	\N	2012-12-10	1	\N
00000027	02	00000002	\N	D	\N	2012-12-11	1	\N
00000027	02	00000002	\N	D	\N	2012-12-12	1	\N
00000027	02	00000002	\N	D	\N	2012-12-13	1	\N
00000027	02	00000002	\N	F	\N	2012-12-14	1	\N
00000027	02	00000002	\N	F	\N	2012-12-15	1	\N
00000027	02	00000002	\N	D	\N	2012-12-16	1	\N
00000027	02	00000002	\N	D	\N	2012-12-17	1	\N
00000027	02	00000002	\N	D	\N	2012-12-18	1	\N
00000027	02	00000002	\N	F	\N	2012-12-22	1	\N
00000027	02	00000002	\N	D	\N	2012-12-23	1	\N
00000027	02	00000002	\N	D	\N	2012-12-24	1	\N
00000027	02	00000002	\N	D	\N	2012-12-25	1	\N
00000027	02	00000002	\N	D	\N	2012-12-26	1	\N
00000027	02	00000002	\N	D	\N	2012-12-27	1	\N
00000027	02	00000002	\N	F	\N	2012-12-28	1	\N
00000027	02	00000002	\N	F	\N	2012-12-29	1	\N
00000027	02	00000002	\N	D	\N	2012-12-30	1	\N
00000027	02	00000002	\N	D	\N	2012-12-31	1	\N
00000028	02	00000002	\N	F	\N	2012-12-01	1	\N
00000028	02	00000002	\N	D	\N	2012-12-02	1	\N
00000028	02	00000002	\N	D	\N	2012-12-03	1	\N
00000028	02	00000002	\N	D	\N	2012-12-04	1	\N
00000028	02	00000002	\N	D	\N	2012-12-05	1	\N
00000028	02	00000002	\N	D	\N	2012-12-06	1	\N
00000028	02	00000002	\N	F	\N	2012-12-07	1	\N
00000028	02	00000002	\N	F	\N	2012-12-08	1	\N
00000028	02	00000002	\N	D	\N	2012-12-09	1	\N
00000028	02	00000002	\N	D	\N	2012-12-10	1	\N
00000028	02	00000002	\N	D	\N	2012-12-11	1	\N
00000028	02	00000002	\N	D	\N	2012-12-12	1	\N
00000028	02	00000002	\N	D	\N	2012-12-13	1	\N
00000028	02	00000002	\N	F	\N	2012-12-14	1	\N
00000028	02	00000002	\N	F	\N	2012-12-15	1	\N
00000028	02	00000002	\N	D	\N	2012-12-16	1	\N
00000028	02	00000002	\N	D	\N	2012-12-17	1	\N
00000028	02	00000002	\N	D	\N	2012-12-18	1	\N
00000028	02	00000002	\N	F	\N	2012-12-22	1	\N
00000028	02	00000002	\N	D	\N	2012-12-23	1	\N
00000028	02	00000002	\N	D	\N	2012-12-24	1	\N
00000028	02	00000002	\N	D	\N	2012-12-25	1	\N
00000028	02	00000002	\N	D	\N	2012-12-26	1	\N
00000028	02	00000002	\N	D	\N	2012-12-27	1	\N
00000028	02	00000002	\N	F	\N	2012-12-28	1	\N
00000028	02	00000002	\N	F	\N	2012-12-29	1	\N
00000028	02	00000002	\N	D	\N	2012-12-30	1	\N
00000028	02	00000002	\N	D	\N	2012-12-31	1	\N
00000029	02	00000002	\N	F	\N	2012-12-01	1	\N
00000029	02	00000002	\N	D	\N	2012-12-02	1	\N
00000029	02	00000002	\N	D	\N	2012-12-03	1	\N
00000029	02	00000002	\N	D	\N	2012-12-04	1	\N
00000029	02	00000002	\N	D	\N	2012-12-05	1	\N
00000029	02	00000002	\N	D	\N	2012-12-06	1	\N
00000029	02	00000002	\N	F	\N	2012-12-07	1	\N
00000029	02	00000002	\N	F	\N	2012-12-08	1	\N
00000029	02	00000002	\N	D	\N	2012-12-09	1	\N
00000029	02	00000002	\N	D	\N	2012-12-10	1	\N
00000029	02	00000002	\N	D	\N	2012-12-11	1	\N
00000029	02	00000002	\N	D	\N	2012-12-12	1	\N
00000029	02	00000002	\N	D	\N	2012-12-13	1	\N
00000029	02	00000002	\N	F	\N	2012-12-14	1	\N
00000029	02	00000002	\N	F	\N	2012-12-15	1	\N
00000029	02	00000002	\N	D	\N	2012-12-16	1	\N
00000029	02	00000002	\N	D	\N	2012-12-17	1	\N
00000029	02	00000002	\N	D	\N	2012-12-18	1	\N
00000029	02	00000002	\N	F	\N	2012-12-22	1	\N
00000029	02	00000002	\N	D	\N	2012-12-23	1	\N
00000029	02	00000002	\N	D	\N	2012-12-24	1	\N
00000029	02	00000002	\N	D	\N	2012-12-25	1	\N
00000029	02	00000002	\N	D	\N	2012-12-26	1	\N
00000029	02	00000002	\N	D	\N	2012-12-27	1	\N
00000029	02	00000002	\N	F	\N	2012-12-28	1	\N
00000029	02	00000002	\N	F	\N	2012-12-29	1	\N
00000029	02	00000002	\N	D	\N	2012-12-30	1	\N
00000029	02	00000002	\N	D	\N	2012-12-31	1	\N
00000030	02	00000002	\N	F	\N	2012-12-01	1	\N
00000030	02	00000002	\N	D	\N	2012-12-02	1	\N
00000030	02	00000002	\N	D	\N	2012-12-03	1	\N
00000030	02	00000002	\N	D	\N	2012-12-04	1	\N
00000030	02	00000002	\N	D	\N	2012-12-05	1	\N
00000030	02	00000002	\N	D	\N	2012-12-06	1	\N
00000030	02	00000002	\N	F	\N	2012-12-07	1	\N
00000030	02	00000002	\N	F	\N	2012-12-08	1	\N
00000030	02	00000002	\N	D	\N	2012-12-09	1	\N
00000030	02	00000002	\N	D	\N	2012-12-10	1	\N
00000030	02	00000002	\N	D	\N	2012-12-11	1	\N
00000030	02	00000002	\N	D	\N	2012-12-12	1	\N
00000030	02	00000002	\N	D	\N	2012-12-13	1	\N
00000030	02	00000002	\N	F	\N	2012-12-14	1	\N
00000030	02	00000002	\N	F	\N	2012-12-15	1	\N
00000030	02	00000002	\N	D	\N	2012-12-16	1	\N
00000030	02	00000002	\N	D	\N	2012-12-17	1	\N
00000030	02	00000002	\N	D	\N	2012-12-18	1	\N
00000027	02	00000002	\N	D	\N	2012-12-19	1	\N
00000027	02	00000002	\N	D	\N	2012-12-20	1	\N
00000027	02	00000002	\N	D	\N	2012-12-21	1	\N
00000030	02	00000002	\N	F	\N	2012-12-22	1	\N
00000030	02	00000002	\N	D	\N	2012-12-23	1	\N
00000030	02	00000002	\N	D	\N	2012-12-24	1	\N
00000030	02	00000002	\N	D	\N	2012-12-25	1	\N
00000030	02	00000002	\N	D	\N	2012-12-26	1	\N
00000030	02	00000002	\N	D	\N	2012-12-27	1	\N
00000030	02	00000002	\N	F	\N	2012-12-28	1	\N
00000030	02	00000002	\N	F	\N	2012-12-29	1	\N
00000030	02	00000002	\N	D	\N	2012-12-30	1	\N
00000030	02	00000002	\N	D	\N	2012-12-31	1	\N
00000031	02	00000002	\N	F	\N	2012-12-01	1	\N
00000031	02	00000002	\N	D	\N	2012-12-02	1	\N
00000031	02	00000002	\N	D	\N	2012-12-03	1	\N
00000031	02	00000002	\N	D	\N	2012-12-04	1	\N
00000031	02	00000002	\N	D	\N	2012-12-05	1	\N
00000031	02	00000002	\N	D	\N	2012-12-06	1	\N
00000031	02	00000002	\N	F	\N	2012-12-07	1	\N
00000031	02	00000002	\N	F	\N	2012-12-08	1	\N
00000031	02	00000002	\N	D	\N	2012-12-09	1	\N
00000031	02	00000002	\N	D	\N	2012-12-10	1	\N
00000031	02	00000002	\N	D	\N	2012-12-11	1	\N
00000031	02	00000002	\N	D	\N	2012-12-12	1	\N
00000031	02	00000002	\N	D	\N	2012-12-13	1	\N
00000031	02	00000002	\N	F	\N	2012-12-14	1	\N
00000031	02	00000002	\N	F	\N	2012-12-15	1	\N
00000031	02	00000002	\N	D	\N	2012-12-16	1	\N
00000031	02	00000002	\N	D	\N	2012-12-17	1	\N
00000031	02	00000002	\N	D	\N	2012-12-18	1	\N
00000031	02	00000002	\N	F	\N	2012-12-22	1	\N
00000031	02	00000002	\N	D	\N	2012-12-23	1	\N
00000031	02	00000002	\N	D	\N	2012-12-24	1	\N
00000031	02	00000002	\N	D	\N	2012-12-25	1	\N
00000031	02	00000002	\N	D	\N	2012-12-26	1	\N
00000031	02	00000002	\N	D	\N	2012-12-27	1	\N
00000031	02	00000002	\N	F	\N	2012-12-28	1	\N
00000031	02	00000002	\N	F	\N	2012-12-29	1	\N
00000031	02	00000002	\N	D	\N	2012-12-30	1	\N
00000031	02	00000002	\N	D	\N	2012-12-31	1	\N
00000032	02	00000002	\N	F	\N	2012-12-01	1	\N
00000032	02	00000002	\N	D	\N	2012-12-02	1	\N
00000032	02	00000002	\N	D	\N	2012-12-03	1	\N
00000032	02	00000002	\N	D	\N	2012-12-04	1	\N
00000032	02	00000002	\N	D	\N	2012-12-05	1	\N
00000032	02	00000002	\N	D	\N	2012-12-06	1	\N
00000032	02	00000002	\N	F	\N	2012-12-07	1	\N
00000032	02	00000002	\N	F	\N	2012-12-08	1	\N
00000032	02	00000002	\N	D	\N	2012-12-09	1	\N
00000032	02	00000002	\N	D	\N	2012-12-10	1	\N
00000032	02	00000002	\N	D	\N	2012-12-11	1	\N
00000032	02	00000002	\N	D	\N	2012-12-12	1	\N
00000032	02	00000002	\N	D	\N	2012-12-13	1	\N
00000032	02	00000002	\N	F	\N	2012-12-14	1	\N
00000032	02	00000002	\N	F	\N	2012-12-15	1	\N
00000032	02	00000002	\N	D	\N	2012-12-16	1	\N
00000032	02	00000002	\N	D	\N	2012-12-17	1	\N
00000032	02	00000002	\N	D	\N	2012-12-18	1	\N
00000032	02	00000002	\N	F	\N	2012-12-22	1	\N
00000032	02	00000002	\N	D	\N	2012-12-23	1	\N
00000032	02	00000002	\N	D	\N	2012-12-24	1	\N
00000032	02	00000002	\N	D	\N	2012-12-25	1	\N
00000032	02	00000002	\N	D	\N	2012-12-26	1	\N
00000032	02	00000002	\N	D	\N	2012-12-27	1	\N
00000032	02	00000002	\N	F	\N	2012-12-28	1	\N
00000032	02	00000002	\N	F	\N	2012-12-29	1	\N
00000032	02	00000002	\N	D	\N	2012-12-30	1	\N
00000032	02	00000002	\N	D	\N	2012-12-31	1	\N
00000033	02	00000002	\N	F	\N	2012-12-01	1	\N
00000033	02	00000002	\N	D	\N	2012-12-02	1	\N
00000033	02	00000002	\N	D	\N	2012-12-03	1	\N
00000033	02	00000002	\N	D	\N	2012-12-04	1	\N
00000033	02	00000002	\N	D	\N	2012-12-05	1	\N
00000033	02	00000002	\N	D	\N	2012-12-06	1	\N
00000033	02	00000002	\N	F	\N	2012-12-07	1	\N
00000033	02	00000002	\N	F	\N	2012-12-08	1	\N
00000033	02	00000002	\N	D	\N	2012-12-09	1	\N
00000033	02	00000002	\N	D	\N	2012-12-10	1	\N
00000033	02	00000002	\N	D	\N	2012-12-11	1	\N
00000033	02	00000002	\N	D	\N	2012-12-12	1	\N
00000033	02	00000002	\N	D	\N	2012-12-13	1	\N
00000033	02	00000002	\N	F	\N	2012-12-14	1	\N
00000033	02	00000002	\N	F	\N	2012-12-15	1	\N
00000033	02	00000002	\N	D	\N	2012-12-16	1	\N
00000033	02	00000002	\N	D	\N	2012-12-17	1	\N
00000033	02	00000002	\N	D	\N	2012-12-18	1	\N
00000033	02	00000002	\N	F	\N	2012-12-22	1	\N
00000033	02	00000002	\N	D	\N	2012-12-23	1	\N
00000033	02	00000002	\N	D	\N	2012-12-24	1	\N
00000033	02	00000002	\N	D	\N	2012-12-25	1	\N
00000033	02	00000002	\N	D	\N	2012-12-26	1	\N
00000033	02	00000002	\N	D	\N	2012-12-27	1	\N
00000033	02	00000002	\N	F	\N	2012-12-28	1	\N
00000033	02	00000002	\N	F	\N	2012-12-29	1	\N
00000033	02	00000002	\N	D	\N	2012-12-30	1	\N
00000033	02	00000002	\N	D	\N	2012-12-31	1	\N
00000034	02	00000002	\N	F	\N	2012-12-01	1	\N
00000034	02	00000002	\N	D	\N	2012-12-02	1	\N
00000034	02	00000002	\N	D	\N	2012-12-03	1	\N
00000034	02	00000002	\N	D	\N	2012-12-04	1	\N
00000034	02	00000002	\N	D	\N	2012-12-05	1	\N
00000034	02	00000002	\N	D	\N	2012-12-06	1	\N
00000034	02	00000002	\N	F	\N	2012-12-07	1	\N
00000034	02	00000002	\N	F	\N	2012-12-08	1	\N
00000034	02	00000002	\N	D	\N	2012-12-09	1	\N
00000034	02	00000002	\N	D	\N	2012-12-10	1	\N
00000034	02	00000002	\N	D	\N	2012-12-11	1	\N
00000034	02	00000002	\N	D	\N	2012-12-12	1	\N
00000034	02	00000002	\N	D	\N	2012-12-13	1	\N
00000034	02	00000002	\N	F	\N	2012-12-14	1	\N
00000034	02	00000002	\N	F	\N	2012-12-15	1	\N
00000034	02	00000002	\N	D	\N	2012-12-16	1	\N
00000034	02	00000002	\N	D	\N	2012-12-17	1	\N
00000034	02	00000002	\N	D	\N	2012-12-18	1	\N
00000034	02	00000002	\N	F	\N	2012-12-22	1	\N
00000034	02	00000002	\N	D	\N	2012-12-23	1	\N
00000034	02	00000002	\N	D	\N	2012-12-24	1	\N
00000034	02	00000002	\N	D	\N	2012-12-25	1	\N
00000034	02	00000002	\N	D	\N	2012-12-26	1	\N
00000034	02	00000002	\N	D	\N	2012-12-27	1	\N
00000034	02	00000002	\N	F	\N	2012-12-28	1	\N
00000034	02	00000002	\N	F	\N	2012-12-29	1	\N
00000034	02	00000002	\N	D	\N	2012-12-30	1	\N
00000034	02	00000002	\N	D	\N	2012-12-31	1	\N
00000035	02	00000002	\N	F	\N	2012-12-01	1	\N
00000035	02	00000002	\N	D	\N	2012-12-02	1	\N
00000035	02	00000002	\N	D	\N	2012-12-03	1	\N
00000035	02	00000002	\N	D	\N	2012-12-04	1	\N
00000035	02	00000002	\N	D	\N	2012-12-05	1	\N
00000035	02	00000002	\N	D	\N	2012-12-06	1	\N
00000035	02	00000002	\N	F	\N	2012-12-07	1	\N
00000035	02	00000002	\N	F	\N	2012-12-08	1	\N
00000035	02	00000002	\N	D	\N	2012-12-09	1	\N
00000035	02	00000002	\N	D	\N	2012-12-10	1	\N
00000035	02	00000002	\N	D	\N	2012-12-11	1	\N
00000035	02	00000002	\N	D	\N	2012-12-12	1	\N
00000035	02	00000002	\N	D	\N	2012-12-13	1	\N
00000035	02	00000002	\N	F	\N	2012-12-14	1	\N
00000035	02	00000002	\N	F	\N	2012-12-15	1	\N
00000035	02	00000002	\N	D	\N	2012-12-16	1	\N
00000035	02	00000002	\N	D	\N	2012-12-17	1	\N
00000035	02	00000002	\N	D	\N	2012-12-18	1	\N
00000035	02	00000002	\N	F	\N	2012-12-22	1	\N
00000035	02	00000002	\N	D	\N	2012-12-23	1	\N
00000035	02	00000002	\N	D	\N	2012-12-24	1	\N
00000035	02	00000002	\N	D	\N	2012-12-25	1	\N
00000035	02	00000002	\N	D	\N	2012-12-26	1	\N
00000035	02	00000002	\N	D	\N	2012-12-27	1	\N
00000035	02	00000002	\N	F	\N	2012-12-28	1	\N
00000035	02	00000002	\N	F	\N	2012-12-29	1	\N
00000035	02	00000002	\N	D	\N	2012-12-30	1	\N
00000035	02	00000002	\N	D	\N	2012-12-31	1	\N
00000036	02	00000002	\N	F	\N	2012-12-01	1	\N
00000036	02	00000002	\N	D	\N	2012-12-02	1	\N
00000036	02	00000002	\N	D	\N	2012-12-03	1	\N
00000036	02	00000002	\N	D	\N	2012-12-04	1	\N
00000036	02	00000002	\N	D	\N	2012-12-05	1	\N
00000036	02	00000002	\N	D	\N	2012-12-06	1	\N
00000036	02	00000002	\N	F	\N	2012-12-07	1	\N
00000036	02	00000002	\N	F	\N	2012-12-08	1	\N
00000036	02	00000002	\N	D	\N	2012-12-09	1	\N
00000036	02	00000002	\N	D	\N	2012-12-10	1	\N
00000036	02	00000002	\N	D	\N	2012-12-11	1	\N
00000036	02	00000002	\N	D	\N	2012-12-12	1	\N
00000036	02	00000002	\N	D	\N	2012-12-13	1	\N
00000036	02	00000002	\N	F	\N	2012-12-14	1	\N
00000036	02	00000002	\N	F	\N	2012-12-15	1	\N
00000036	02	00000002	\N	D	\N	2012-12-16	1	\N
00000036	02	00000002	\N	D	\N	2012-12-17	1	\N
00000036	02	00000002	\N	D	\N	2012-12-18	1	\N
00000036	02	00000002	\N	F	\N	2012-12-22	1	\N
00000036	02	00000002	\N	D	\N	2012-12-23	1	\N
00000036	02	00000002	\N	D	\N	2012-12-24	1	\N
00000036	02	00000002	\N	D	\N	2012-12-25	1	\N
00000036	02	00000002	\N	D	\N	2012-12-26	1	\N
00000036	02	00000002	\N	D	\N	2012-12-27	1	\N
00000036	02	00000002	\N	F	\N	2012-12-28	1	\N
00000036	02	00000002	\N	F	\N	2012-12-29	1	\N
00000036	02	00000002	\N	D	\N	2012-12-30	1	\N
00000036	02	00000002	\N	D	\N	2012-12-31	1	\N
00000037	02	00000002	\N	F	\N	2012-12-01	1	\N
00000037	02	00000002	\N	D	\N	2012-12-02	1	\N
00000037	02	00000002	\N	D	\N	2012-12-03	1	\N
00000037	02	00000002	\N	D	\N	2012-12-04	1	\N
00000037	02	00000002	\N	D	\N	2012-12-05	1	\N
00000037	02	00000002	\N	D	\N	2012-12-06	1	\N
00000037	02	00000002	\N	F	\N	2012-12-07	1	\N
00000037	02	00000002	\N	F	\N	2012-12-08	1	\N
00000037	02	00000002	\N	D	\N	2012-12-09	1	\N
00000037	02	00000002	\N	D	\N	2012-12-10	1	\N
00000037	02	00000002	\N	D	\N	2012-12-11	1	\N
00000037	02	00000002	\N	D	\N	2012-12-12	1	\N
00000037	02	00000002	\N	D	\N	2012-12-13	1	\N
00000037	02	00000002	\N	F	\N	2012-12-14	1	\N
00000037	02	00000002	\N	F	\N	2012-12-15	1	\N
00000037	02	00000002	\N	D	\N	2012-12-16	1	\N
00000037	02	00000002	\N	D	\N	2012-12-17	1	\N
00000037	02	00000002	\N	D	\N	2012-12-18	1	\N
00000037	02	00000002	\N	F	\N	2012-12-22	1	\N
00000037	02	00000002	\N	D	\N	2012-12-23	1	\N
00000037	02	00000002	\N	D	\N	2012-12-24	1	\N
00000037	02	00000002	\N	D	\N	2012-12-25	1	\N
00000037	02	00000002	\N	D	\N	2012-12-26	1	\N
00000037	02	00000002	\N	D	\N	2012-12-27	1	\N
00000037	02	00000002	\N	F	\N	2012-12-28	1	\N
00000037	02	00000002	\N	F	\N	2012-12-29	1	\N
00000037	02	00000002	\N	D	\N	2012-12-30	1	\N
00000037	02	00000002	\N	D	\N	2012-12-31	1	\N
00000038	02	00000002	\N	F	\N	2012-12-01	1	\N
00000038	02	00000002	\N	D	\N	2012-12-02	1	\N
00000038	02	00000002	\N	D	\N	2012-12-03	1	\N
00000038	02	00000002	\N	D	\N	2012-12-04	1	\N
00000038	02	00000002	\N	D	\N	2012-12-05	1	\N
00000038	02	00000002	\N	D	\N	2012-12-06	1	\N
00000038	02	00000002	\N	F	\N	2012-12-07	1	\N
00000038	02	00000002	\N	F	\N	2012-12-08	1	\N
00000038	02	00000002	\N	D	\N	2012-12-09	1	\N
00000038	02	00000002	\N	D	\N	2012-12-10	1	\N
00000038	02	00000002	\N	D	\N	2012-12-11	1	\N
00000038	02	00000002	\N	D	\N	2012-12-12	1	\N
00000038	02	00000002	\N	D	\N	2012-12-13	1	\N
00000038	02	00000002	\N	F	\N	2012-12-14	1	\N
00000038	02	00000002	\N	F	\N	2012-12-15	1	\N
00000038	02	00000002	\N	D	\N	2012-12-16	1	\N
00000038	02	00000002	\N	D	\N	2012-12-17	1	\N
00000038	02	00000002	\N	D	\N	2012-12-18	1	\N
00000038	02	00000002	\N	F	\N	2012-12-22	1	\N
00000038	02	00000002	\N	D	\N	2012-12-23	1	\N
00000038	02	00000002	\N	D	\N	2012-12-24	1	\N
00000038	02	00000002	\N	D	\N	2012-12-25	1	\N
00000038	02	00000002	\N	D	\N	2012-12-26	1	\N
00000038	02	00000002	\N	D	\N	2012-12-27	1	\N
00000038	02	00000002	\N	F	\N	2012-12-28	1	\N
00000038	02	00000002	\N	F	\N	2012-12-29	1	\N
00000038	02	00000002	\N	D	\N	2012-12-30	1	\N
00000038	02	00000002	\N	D	\N	2012-12-31	1	\N
00000039	02	00000002	\N	F	\N	2012-12-01	1	\N
00000039	02	00000002	\N	D	\N	2012-12-02	1	\N
00000039	02	00000002	\N	D	\N	2012-12-03	1	\N
00000039	02	00000002	\N	D	\N	2012-12-04	1	\N
00000039	02	00000002	\N	D	\N	2012-12-05	1	\N
00000039	02	00000002	\N	D	\N	2012-12-06	1	\N
00000039	02	00000002	\N	F	\N	2012-12-07	1	\N
00000039	02	00000002	\N	F	\N	2012-12-08	1	\N
00000039	02	00000002	\N	D	\N	2012-12-09	1	\N
00000039	02	00000002	\N	D	\N	2012-12-10	1	\N
00000039	02	00000002	\N	D	\N	2012-12-11	1	\N
00000039	02	00000002	\N	D	\N	2012-12-12	1	\N
00000039	02	00000002	\N	D	\N	2012-12-13	1	\N
00000039	02	00000002	\N	F	\N	2012-12-14	1	\N
00000039	02	00000002	\N	F	\N	2012-12-15	1	\N
00000039	02	00000002	\N	D	\N	2012-12-16	1	\N
00000039	02	00000002	\N	D	\N	2012-12-17	1	\N
00000039	02	00000002	\N	D	\N	2012-12-18	1	\N
00000039	02	00000002	\N	F	\N	2012-12-22	1	\N
00000039	02	00000002	\N	D	\N	2012-12-23	1	\N
00000039	02	00000002	\N	D	\N	2012-12-24	1	\N
00000039	02	00000002	\N	D	\N	2012-12-25	1	\N
00000039	02	00000002	\N	D	\N	2012-12-26	1	\N
00000039	02	00000002	\N	D	\N	2012-12-27	1	\N
00000039	02	00000002	\N	F	\N	2012-12-28	1	\N
00000039	02	00000002	\N	F	\N	2012-12-29	1	\N
00000039	02	00000002	\N	D	\N	2012-12-30	1	\N
00000039	02	00000002	\N	D	\N	2012-12-31	1	\N
00000040	02	00000002	\N	F	\N	2012-12-01	1	\N
00000040	02	00000002	\N	D	\N	2012-12-02	1	\N
00000040	02	00000002	\N	D	\N	2012-12-03	1	\N
00000040	02	00000002	\N	D	\N	2012-12-04	1	\N
00000040	02	00000002	\N	D	\N	2012-12-05	1	\N
00000040	02	00000002	\N	D	\N	2012-12-06	1	\N
00000040	02	00000002	\N	F	\N	2012-12-07	1	\N
00000040	02	00000002	\N	F	\N	2012-12-08	1	\N
00000040	02	00000002	\N	D	\N	2012-12-09	1	\N
00000040	02	00000002	\N	D	\N	2012-12-10	1	\N
00000040	02	00000002	\N	D	\N	2012-12-11	1	\N
00000040	02	00000002	\N	D	\N	2012-12-12	1	\N
00000040	02	00000002	\N	D	\N	2012-12-13	1	\N
00000040	02	00000002	\N	F	\N	2012-12-14	1	\N
00000040	02	00000002	\N	F	\N	2012-12-15	1	\N
00000040	02	00000002	\N	D	\N	2012-12-16	1	\N
00000040	02	00000002	\N	D	\N	2012-12-17	1	\N
00000040	02	00000002	\N	D	\N	2012-12-18	1	\N
00000040	02	00000002	\N	F	\N	2012-12-22	1	\N
00000040	02	00000002	\N	D	\N	2012-12-23	1	\N
00000040	02	00000002	\N	D	\N	2012-12-24	1	\N
00000040	02	00000002	\N	D	\N	2012-12-25	1	\N
00000040	02	00000002	\N	D	\N	2012-12-26	1	\N
00000040	02	00000002	\N	D	\N	2012-12-27	1	\N
00000040	02	00000002	\N	F	\N	2012-12-28	1	\N
00000040	02	00000002	\N	F	\N	2012-12-29	1	\N
00000040	02	00000002	\N	D	\N	2012-12-30	1	\N
00000040	02	00000002	\N	D	\N	2012-12-31	1	\N
00000041	02	00000002	\N	F	\N	2012-12-01	1	\N
00000041	02	00000002	\N	D	\N	2012-12-02	1	\N
00000041	02	00000002	\N	D	\N	2012-12-03	1	\N
00000041	02	00000002	\N	D	\N	2012-12-04	1	\N
00000041	02	00000002	\N	D	\N	2012-12-05	1	\N
00000041	02	00000002	\N	D	\N	2012-12-06	1	\N
00000041	02	00000002	\N	F	\N	2012-12-07	1	\N
00000041	02	00000002	\N	F	\N	2012-12-08	1	\N
00000041	02	00000002	\N	D	\N	2012-12-09	1	\N
00000041	02	00000002	\N	D	\N	2012-12-10	1	\N
00000041	02	00000002	\N	D	\N	2012-12-11	1	\N
00000041	02	00000002	\N	D	\N	2012-12-12	1	\N
00000041	02	00000002	\N	D	\N	2012-12-13	1	\N
00000041	02	00000002	\N	F	\N	2012-12-14	1	\N
00000041	02	00000002	\N	F	\N	2012-12-15	1	\N
00000041	02	00000002	\N	D	\N	2012-12-16	1	\N
00000041	02	00000002	\N	D	\N	2012-12-17	1	\N
00000041	02	00000002	\N	D	\N	2012-12-18	1	\N
00000041	02	00000002	\N	F	\N	2012-12-22	1	\N
00000041	02	00000002	\N	D	\N	2012-12-23	1	\N
00000041	02	00000002	\N	D	\N	2012-12-24	1	\N
00000041	02	00000002	\N	D	\N	2012-12-25	1	\N
00000041	02	00000002	\N	D	\N	2012-12-26	1	\N
00000041	02	00000002	\N	D	\N	2012-12-27	1	\N
00000041	02	00000002	\N	F	\N	2012-12-28	1	\N
00000041	02	00000002	\N	F	\N	2012-12-29	1	\N
00000041	02	00000002	\N	D	\N	2012-12-30	1	\N
00000041	02	00000002	\N	D	\N	2012-12-31	1	\N
00000001	01	00000002	\N	D	\N	2013-02-03	1	\N
00000001	01	00000002	\N	D	\N	2013-02-04	1	\N
00000001	01	00000002	\N	D	\N	2013-02-05	1	\N
00000001	01	00000002	\N	D	\N	2013-02-06	1	\N
00000001	01	00000002	\N	D	\N	2013-02-07	1	\N
00000001	01	00000002	\N	F	\N	2013-02-08	1	\N
00000001	01	00000002	\N	F	\N	2013-02-09	1	\N
00000001	01	00000002	\N	D	\N	2013-02-10	1	\N
00000001	01	00000002	\N	D	\N	2013-02-11	1	\N
00000001	01	00000002	\N	D	\N	2013-02-12	1	\N
00000001	01	00000002	\N	D	\N	2013-02-13	1	\N
00000001	01	00000002	\N	D	\N	2013-02-14	1	\N
00000001	01	00000002	\N	F	\N	2013-02-15	1	\N
00000001	01	00000002	\N	F	\N	2013-02-16	1	\N
00000001	01	00000002	\N	D	\N	2013-02-17	1	\N
00000001	01	00000002	\N	D	\N	2013-02-18	1	\N
00000001	01	00000002	\N	D	\N	2013-02-19	1	\N
00000001	01	00000002	\N	D	\N	2013-02-20	1	\N
00000001	01	00000002	\N	D	\N	2013-02-21	1	\N
00000001	01	00000002	\N	F	\N	2013-02-22	1	\N
00000001	01	00000002	\N	F	\N	2013-02-23	1	\N
00000001	01	00000002	\N	D	\N	2013-02-24	1	\N
00000001	01	00000002	\N	D	\N	2013-02-25	1	\N
00000001	01	00000002	\N	D	\N	2013-02-26	1	\N
00000001	01	00000002	\N	D	\N	2013-02-27	1	\N
00000001	01	00000002	\N	D	\N	2013-02-28	1	\N
00000002	01	00000002	\N	F	\N	2013-02-01	1	\N
00000012	03	00000002	\N	D	\N	2013-01-15	1	\N
00000012	03	00000002	\N	D	\N	2013-01-16	1	\N
00000012	03	00000002	\N	D	\N	2013-01-17	1	\N
00000012	03	00000002	\N	F	\N	2013-01-18	1	\N
00000012	03	00000002	\N	F	\N	2013-01-19	1	\N
00000012	03	00000002	\N	D	\N	2013-01-20	1	\N
00000012	03	00000002	\N	D	\N	2013-01-21	1	\N
00000012	03	00000002	\N	D	\N	2013-01-22	1	\N
00000012	03	00000002	\N	D	\N	2013-01-23	1	\N
00000012	03	00000002	\N	D	\N	2013-01-24	1	\N
00000012	03	00000002	\N	F	\N	2013-01-25	1	\N
00000012	03	00000002	\N	F	\N	2013-01-26	1	\N
00000012	03	00000002	\N	D	\N	2013-01-27	1	\N
00000012	03	00000002	\N	D	\N	2013-01-28	1	\N
00000012	03	00000002	\N	D	\N	2013-01-29	1	\N
00000012	03	00000002	\N	D	\N	2013-01-30	1	\N
00000012	03	00000002	\N	D	\N	2013-01-31	1	\N
00000010	03	00000002	\N	F	\N	2013-02-01	1	\N
00000010	03	00000002	\N	F	\N	2013-02-02	1	\N
00000010	03	00000002	\N	D	\N	2013-02-03	1	\N
00000010	03	00000002	\N	D	\N	2013-02-04	1	\N
00000010	03	00000002	\N	D	\N	2013-02-05	1	\N
00000010	03	00000002	\N	D	\N	2013-02-06	1	\N
00000010	03	00000002	\N	D	\N	2013-02-07	1	\N
00000010	03	00000002	\N	F	\N	2013-02-08	1	\N
00000010	03	00000002	\N	F	\N	2013-02-09	1	\N
00000010	03	00000002	\N	D	\N	2013-02-10	1	\N
00000010	03	00000002	\N	D	\N	2013-02-11	1	\N
00000010	03	00000002	\N	D	\N	2013-02-12	1	\N
00000010	03	00000002	\N	D	\N	2013-02-13	1	\N
00000010	03	00000002	\N	D	\N	2013-02-14	1	\N
00000010	03	00000002	\N	F	\N	2013-02-15	1	\N
00000010	03	00000002	\N	F	\N	2013-02-16	1	\N
00000010	03	00000002	\N	D	\N	2013-02-17	1	\N
00000010	03	00000002	\N	D	\N	2013-02-18	1	\N
00000010	03	00000002	\N	D	\N	2013-02-19	1	\N
00000010	03	00000002	\N	D	\N	2013-02-20	1	\N
00000010	03	00000002	\N	D	\N	2013-02-21	1	\N
00000010	03	00000002	\N	F	\N	2013-02-22	1	\N
00000010	03	00000002	\N	F	\N	2013-02-23	1	\N
00000010	03	00000002	\N	D	\N	2013-02-24	1	\N
00000010	03	00000002	\N	D	\N	2013-02-25	1	\N
00000010	03	00000002	\N	D	\N	2013-02-26	1	\N
00000010	03	00000002	\N	D	\N	2013-02-27	1	\N
00000010	03	00000002	\N	D	\N	2013-02-28	1	\N
00000011	03	00000002	\N	F	\N	2013-02-01	1	\N
00000011	03	00000002	\N	F	\N	2013-02-02	1	\N
00000011	03	00000002	\N	D	\N	2013-02-03	1	\N
00000011	03	00000002	\N	D	\N	2013-02-04	1	\N
00000011	03	00000002	\N	D	\N	2013-02-05	1	\N
00000011	03	00000002	\N	D	\N	2013-02-06	1	\N
00000011	03	00000002	\N	D	\N	2013-02-07	1	\N
00000011	03	00000002	\N	F	\N	2013-02-08	1	\N
00000011	03	00000002	\N	F	\N	2013-02-09	1	\N
00000011	03	00000002	\N	D	\N	2013-02-10	1	\N
00000011	03	00000002	\N	D	\N	2013-02-11	1	\N
00000011	03	00000002	\N	D	\N	2013-02-12	1	\N
00000011	03	00000002	\N	D	\N	2013-02-13	1	\N
00000011	03	00000002	\N	D	\N	2013-02-14	1	\N
00000011	03	00000002	\N	F	\N	2013-02-15	1	\N
00000011	03	00000002	\N	F	\N	2013-02-16	1	\N
00000011	03	00000002	\N	D	\N	2013-02-17	1	\N
00000011	03	00000002	\N	D	\N	2013-02-18	1	\N
00000011	03	00000002	\N	D	\N	2013-02-19	1	\N
00000011	03	00000002	\N	D	\N	2013-02-20	1	\N
00000011	03	00000002	\N	D	\N	2013-02-21	1	\N
00000011	03	00000002	\N	F	\N	2013-02-22	1	\N
00000011	03	00000002	\N	F	\N	2013-02-23	1	\N
00000011	03	00000002	\N	D	\N	2013-02-24	1	\N
00000011	03	00000002	\N	D	\N	2013-02-25	1	\N
00000011	03	00000002	\N	D	\N	2013-02-26	1	\N
00000011	03	00000002	\N	D	\N	2013-02-27	1	\N
00000011	03	00000002	\N	D	\N	2013-02-28	1	\N
00000012	03	00000002	\N	F	\N	2013-02-01	1	\N
00000012	03	00000002	\N	F	\N	2013-02-02	1	\N
00000012	03	00000002	\N	D	\N	2013-02-03	1	\N
00000012	03	00000002	\N	D	\N	2013-02-04	1	\N
00000012	03	00000002	\N	D	\N	2013-02-05	1	\N
00000012	03	00000002	\N	D	\N	2013-02-06	1	\N
00000012	03	00000002	\N	D	\N	2013-02-07	1	\N
00000012	03	00000002	\N	F	\N	2013-02-08	1	\N
00000012	03	00000002	\N	F	\N	2013-02-09	1	\N
00000012	03	00000002	\N	D	\N	2013-02-10	1	\N
00000012	03	00000002	\N	D	\N	2013-02-11	1	\N
00000012	03	00000002	\N	D	\N	2013-02-12	1	\N
00000012	03	00000002	\N	D	\N	2013-02-13	1	\N
00000012	03	00000002	\N	D	\N	2013-02-14	1	\N
00000012	03	00000002	\N	F	\N	2013-02-15	1	\N
00000012	03	00000002	\N	F	\N	2013-02-16	1	\N
00000012	03	00000002	\N	D	\N	2013-02-17	1	\N
00000012	03	00000002	\N	D	\N	2013-02-18	1	\N
00000012	03	00000002	\N	D	\N	2013-02-19	1	\N
00000012	03	00000002	\N	D	\N	2013-02-20	1	\N
00000012	03	00000002	\N	D	\N	2013-02-21	1	\N
00000012	03	00000002	\N	F	\N	2013-02-22	1	\N
00000012	03	00000002	\N	F	\N	2013-02-23	1	\N
00000012	03	00000002	\N	D	\N	2013-02-24	1	\N
00000012	03	00000002	\N	D	\N	2013-02-25	1	\N
00000012	03	00000002	\N	D	\N	2013-02-26	1	\N
00000012	03	00000002	\N	D	\N	2013-02-27	1	\N
00000012	03	00000002	\N	D	\N	2013-02-28	1	\N
00000010	03	00000002	\N	D	0000000016	2013-01-08	R	\N
00000017	02	00000002	\N	F	\N	2013-02-01	1	\N
00000017	02	00000002	\N	F	\N	2013-02-02	1	\N
00000017	02	00000002	\N	D	\N	2013-02-03	1	\N
00000017	02	00000002	\N	D	\N	2013-02-04	1	\N
00000017	02	00000002	\N	D	\N	2013-02-05	1	\N
00000017	02	00000002	\N	D	\N	2013-02-06	1	\N
00000017	02	00000002	\N	D	\N	2013-02-07	1	\N
00000017	02	00000002	\N	F	\N	2013-02-08	1	\N
00000017	02	00000002	\N	F	\N	2013-02-09	1	\N
00000017	02	00000002	\N	D	\N	2013-02-10	1	\N
00000017	02	00000002	\N	D	\N	2013-02-11	1	\N
00000017	02	00000002	\N	D	\N	2013-02-12	1	\N
00000017	02	00000002	\N	D	\N	2013-02-13	1	\N
00000017	02	00000002	\N	D	\N	2013-02-14	1	\N
00000017	02	00000002	\N	F	\N	2013-02-15	1	\N
00000017	02	00000002	\N	F	\N	2013-02-16	1	\N
00000017	02	00000002	\N	D	\N	2013-02-17	1	\N
00000017	02	00000002	\N	D	\N	2013-02-18	1	\N
00000017	02	00000002	\N	D	\N	2013-02-19	1	\N
00000017	02	00000002	\N	D	\N	2013-02-25	1	\N
00000017	02	00000002	\N	D	\N	2013-02-26	1	\N
00000017	02	00000002	\N	D	\N	2013-02-27	1	\N
00000017	02	00000002	\N	D	\N	2013-02-28	1	\N
00000018	02	00000002	\N	F	\N	2013-02-01	1	\N
00000018	02	00000002	\N	F	\N	2013-02-02	1	\N
00000018	02	00000002	\N	D	\N	2013-02-03	1	\N
00000018	02	00000002	\N	D	\N	2013-02-04	1	\N
00000018	02	00000002	\N	D	\N	2013-02-05	1	\N
00000018	02	00000002	\N	D	\N	2013-02-06	1	\N
00000018	02	00000002	\N	D	\N	2013-02-07	1	\N
00000018	02	00000002	\N	F	\N	2013-02-08	1	\N
00000018	02	00000002	\N	F	\N	2013-02-09	1	\N
00000018	02	00000002	\N	D	\N	2013-02-10	1	\N
00000018	02	00000002	\N	D	\N	2013-02-11	1	\N
00000018	02	00000002	\N	D	\N	2013-02-12	1	\N
00000018	02	00000002	\N	D	\N	2013-02-13	1	\N
00000018	02	00000002	\N	D	\N	2013-02-14	1	\N
00000018	02	00000002	\N	F	\N	2013-02-15	1	\N
00000018	02	00000002	\N	F	\N	2013-02-16	1	\N
00000018	02	00000002	\N	D	\N	2013-02-17	1	\N
00000018	02	00000002	\N	D	\N	2013-02-18	1	\N
00000018	02	00000002	\N	D	\N	2013-02-19	1	\N
00000018	02	00000002	\N	D	\N	2013-02-20	1	\N
00000018	02	00000002	\N	D	\N	2013-02-21	1	\N
00000018	02	00000002	\N	F	\N	2013-02-22	1	\N
00000018	02	00000002	\N	F	\N	2013-02-23	1	\N
00000018	02	00000002	\N	D	\N	2013-02-24	1	\N
00000018	02	00000002	\N	D	\N	2013-02-25	1	\N
00000018	02	00000002	\N	D	\N	2013-02-26	1	\N
00000018	02	00000002	\N	D	\N	2013-02-27	1	\N
00000018	02	00000002	\N	D	\N	2013-02-28	1	\N
00000019	02	00000002	\N	F	\N	2013-02-01	1	\N
00000019	02	00000002	\N	F	\N	2013-02-02	1	\N
00000019	02	00000002	\N	D	\N	2013-02-03	1	\N
00000019	02	00000002	\N	D	\N	2013-02-04	1	\N
00000019	02	00000002	\N	D	\N	2013-02-05	1	\N
00000019	02	00000002	\N	D	\N	2013-02-06	1	\N
00000019	02	00000002	\N	D	\N	2013-02-07	1	\N
00000019	02	00000002	\N	F	\N	2013-02-08	1	\N
00000019	02	00000002	\N	F	\N	2013-02-09	1	\N
00000019	02	00000002	\N	D	\N	2013-02-10	1	\N
00000019	02	00000002	\N	D	\N	2013-02-11	1	\N
00000019	02	00000002	\N	D	\N	2013-02-12	1	\N
00000019	02	00000002	\N	D	\N	2013-02-13	1	\N
00000019	02	00000002	\N	D	\N	2013-02-14	1	\N
00000019	02	00000002	\N	F	\N	2013-02-15	1	\N
00000019	02	00000002	\N	F	\N	2013-02-16	1	\N
00000019	02	00000002	\N	D	\N	2013-02-17	1	\N
00000019	02	00000002	\N	D	\N	2013-02-18	1	\N
00000019	02	00000002	\N	D	\N	2013-02-19	1	\N
00000019	02	00000002	\N	D	\N	2013-02-20	1	\N
00000019	02	00000002	\N	D	\N	2013-02-21	1	\N
00000019	02	00000002	\N	F	\N	2013-02-22	1	\N
00000019	02	00000002	\N	F	\N	2013-02-23	1	\N
00000019	02	00000002	\N	D	\N	2013-02-24	1	\N
00000019	02	00000002	\N	D	\N	2013-02-25	1	\N
00000019	02	00000002	\N	D	\N	2013-02-26	1	\N
00000019	02	00000002	\N	D	\N	2013-02-27	1	\N
00000019	02	00000002	\N	D	\N	2013-02-28	1	\N
00000020	02	00000002	\N	F	\N	2013-02-01	1	\N
00000020	02	00000002	\N	F	\N	2013-02-02	1	\N
00000020	02	00000002	\N	D	\N	2013-02-03	1	\N
00000020	02	00000002	\N	D	\N	2013-02-04	1	\N
00000020	02	00000002	\N	D	\N	2013-02-05	1	\N
00000020	02	00000002	\N	D	\N	2013-02-06	1	\N
00000020	02	00000002	\N	D	\N	2013-02-07	1	\N
00000020	02	00000002	\N	F	\N	2013-02-08	1	\N
00000020	02	00000002	\N	F	\N	2013-02-09	1	\N
00000020	02	00000002	\N	D	\N	2013-02-10	1	\N
00000020	02	00000002	\N	D	\N	2013-02-11	1	\N
00000020	02	00000002	\N	D	\N	2013-02-12	1	\N
00000020	02	00000002	\N	D	\N	2013-02-13	1	\N
00000020	02	00000002	\N	D	\N	2013-02-14	1	\N
00000020	02	00000002	\N	F	\N	2013-02-15	1	\N
00000020	02	00000002	\N	F	\N	2013-02-16	1	\N
00000020	02	00000002	\N	D	\N	2013-02-17	1	\N
00000020	02	00000002	\N	D	\N	2013-02-18	1	\N
00000020	02	00000002	\N	D	\N	2013-02-19	1	\N
00000020	02	00000002	\N	D	\N	2013-02-20	1	\N
00000020	02	00000002	\N	D	\N	2013-02-21	1	\N
00000020	02	00000002	\N	F	\N	2013-02-22	1	\N
00000020	02	00000002	\N	F	\N	2013-02-23	1	\N
00000020	02	00000002	\N	D	\N	2013-02-24	1	\N
00000020	02	00000002	\N	D	\N	2013-02-25	1	\N
00000020	02	00000002	\N	D	\N	2013-02-26	1	\N
00000020	02	00000002	\N	D	\N	2013-02-27	1	\N
00000020	02	00000002	\N	D	\N	2013-02-28	1	\N
00000021	02	00000002	\N	F	\N	2013-02-01	1	\N
00000021	02	00000002	\N	F	\N	2013-02-02	1	\N
00000021	02	00000002	\N	D	\N	2013-02-03	1	\N
00000021	02	00000002	\N	D	\N	2013-02-04	1	\N
00000021	02	00000002	\N	D	\N	2013-02-05	1	\N
00000021	02	00000002	\N	D	\N	2013-02-06	1	\N
00000021	02	00000002	\N	D	\N	2013-02-07	1	\N
00000021	02	00000002	\N	F	\N	2013-02-08	1	\N
00000021	02	00000002	\N	F	\N	2013-02-09	1	\N
00000021	02	00000002	\N	D	\N	2013-02-10	1	\N
00000021	02	00000002	\N	D	\N	2013-02-11	1	\N
00000021	02	00000002	\N	D	\N	2013-02-12	1	\N
00000021	02	00000002	\N	D	\N	2013-02-13	1	\N
00000021	02	00000002	\N	D	\N	2013-02-14	1	\N
00000021	02	00000002	\N	F	\N	2013-02-15	1	\N
00000021	02	00000002	\N	F	\N	2013-02-16	1	\N
00000021	02	00000002	\N	D	\N	2013-02-17	1	\N
00000021	02	00000002	\N	D	\N	2013-02-18	1	\N
00000021	02	00000002	\N	D	\N	2013-02-19	1	\N
00000021	02	00000002	\N	D	\N	2013-02-20	1	\N
00000021	02	00000002	\N	D	\N	2013-02-21	1	\N
00000021	02	00000002	\N	F	\N	2013-02-22	1	\N
00000021	02	00000002	\N	F	\N	2013-02-23	1	\N
00000021	02	00000002	\N	D	\N	2013-02-24	1	\N
00000021	02	00000002	\N	D	\N	2013-02-25	1	\N
00000021	02	00000002	\N	D	\N	2013-02-26	1	\N
00000021	02	00000002	\N	D	\N	2013-02-27	1	\N
00000021	02	00000002	\N	D	\N	2013-02-28	1	\N
00000022	02	00000002	\N	F	\N	2013-02-01	1	\N
00000022	02	00000002	\N	F	\N	2013-02-02	1	\N
00000022	02	00000002	\N	D	\N	2013-02-03	1	\N
00000022	02	00000002	\N	D	\N	2013-02-04	1	\N
00000022	02	00000002	\N	D	\N	2013-02-05	1	\N
00000022	02	00000002	\N	D	\N	2013-02-06	1	\N
00000022	02	00000002	\N	D	\N	2013-02-07	1	\N
00000022	02	00000002	\N	F	\N	2013-02-08	1	\N
00000022	02	00000002	\N	F	\N	2013-02-09	1	\N
00000022	02	00000002	\N	D	\N	2013-02-10	1	\N
00000022	02	00000002	\N	D	\N	2013-02-11	1	\N
00000022	02	00000002	\N	D	\N	2013-02-12	1	\N
00000022	02	00000002	\N	D	\N	2013-02-13	1	\N
00000022	02	00000002	\N	D	\N	2013-02-14	1	\N
00000022	02	00000002	\N	F	\N	2013-02-15	1	\N
00000022	02	00000002	\N	F	\N	2013-02-16	1	\N
00000022	02	00000002	\N	D	\N	2013-02-17	1	\N
00000022	02	00000002	\N	D	\N	2013-02-18	1	\N
00000022	02	00000002	\N	D	\N	2013-02-19	1	\N
00000022	02	00000002	\N	D	\N	2013-02-20	1	\N
00000022	02	00000002	\N	D	\N	2013-02-21	1	\N
00000022	02	00000002	\N	F	\N	2013-02-22	1	\N
00000022	02	00000002	\N	F	\N	2013-02-23	1	\N
00000022	02	00000002	\N	D	\N	2013-02-24	1	\N
00000022	02	00000002	\N	D	\N	2013-02-25	1	\N
00000022	02	00000002	\N	D	\N	2013-02-26	1	\N
00000022	02	00000002	\N	D	\N	2013-02-27	1	\N
00000022	02	00000002	\N	D	\N	2013-02-28	1	\N
00000023	02	00000002	\N	F	\N	2013-02-01	1	\N
00000023	02	00000002	\N	F	\N	2013-02-02	1	\N
00000023	02	00000002	\N	D	\N	2013-02-03	1	\N
00000023	02	00000002	\N	D	\N	2013-02-04	1	\N
00000023	02	00000002	\N	D	\N	2013-02-05	1	\N
00000023	02	00000002	\N	D	\N	2013-02-06	1	\N
00000023	02	00000002	\N	D	\N	2013-02-07	1	\N
00000023	02	00000002	\N	F	\N	2013-02-08	1	\N
00000023	02	00000002	\N	F	\N	2013-02-09	1	\N
00000023	02	00000002	\N	D	\N	2013-02-10	1	\N
00000023	02	00000002	\N	D	\N	2013-02-11	1	\N
00000023	02	00000002	\N	D	\N	2013-02-12	1	\N
00000023	02	00000002	\N	D	\N	2013-02-13	1	\N
00000023	02	00000002	\N	D	\N	2013-02-14	1	\N
00000023	02	00000002	\N	F	\N	2013-02-15	1	\N
00000023	02	00000002	\N	F	\N	2013-02-16	1	\N
00000023	02	00000002	\N	D	\N	2013-02-17	1	\N
00000023	02	00000002	\N	D	\N	2013-02-18	1	\N
00000023	02	00000002	\N	D	\N	2013-02-19	1	\N
00000023	02	00000002	\N	D	\N	2013-02-20	1	\N
00000023	02	00000002	\N	D	\N	2013-02-21	1	\N
00000023	02	00000002	\N	F	\N	2013-02-22	1	\N
00000023	02	00000002	\N	F	\N	2013-02-23	1	\N
00000023	02	00000002	\N	D	\N	2013-02-24	1	\N
00000023	02	00000002	\N	D	\N	2013-02-25	1	\N
00000023	02	00000002	\N	D	\N	2013-02-26	1	\N
00000023	02	00000002	\N	D	\N	2013-02-27	1	\N
00000023	02	00000002	\N	D	\N	2013-02-28	1	\N
00000024	02	00000002	\N	F	\N	2013-02-01	1	\N
00000024	02	00000002	\N	F	\N	2013-02-02	1	\N
00000024	02	00000002	\N	D	\N	2013-02-03	1	\N
00000024	02	00000002	\N	D	\N	2013-02-04	1	\N
00000024	02	00000002	\N	D	\N	2013-02-05	1	\N
00000024	02	00000002	\N	D	\N	2013-02-06	1	\N
00000024	02	00000002	\N	D	\N	2013-02-07	1	\N
00000024	02	00000002	\N	F	\N	2013-02-08	1	\N
00000024	02	00000002	\N	F	\N	2013-02-09	1	\N
00000024	02	00000002	\N	D	\N	2013-02-10	1	\N
00000024	02	00000002	\N	D	\N	2013-02-11	1	\N
00000024	02	00000002	\N	D	\N	2013-02-12	1	\N
00000024	02	00000002	\N	D	\N	2013-02-13	1	\N
00000024	02	00000002	\N	D	\N	2013-02-14	1	\N
00000024	02	00000002	\N	F	\N	2013-02-15	1	\N
00000024	02	00000002	\N	F	\N	2013-02-16	1	\N
00000024	02	00000002	\N	D	\N	2013-02-17	1	\N
00000024	02	00000002	\N	D	\N	2013-02-18	1	\N
00000024	02	00000002	\N	D	\N	2013-02-19	1	\N
00000024	02	00000002	\N	D	\N	2013-02-20	1	\N
00000024	02	00000002	\N	D	\N	2013-02-21	1	\N
00000024	02	00000002	\N	F	\N	2013-02-22	1	\N
00000024	02	00000002	\N	F	\N	2013-02-23	1	\N
00000024	02	00000002	\N	D	\N	2013-02-24	1	\N
00000024	02	00000002	\N	D	\N	2013-02-25	1	\N
00000024	02	00000002	\N	D	\N	2013-02-26	1	\N
00000024	02	00000002	\N	D	\N	2013-02-27	1	\N
00000024	02	00000002	\N	D	\N	2013-02-28	1	\N
00000025	02	00000002	\N	F	\N	2013-02-01	1	\N
00000025	02	00000002	\N	F	\N	2013-02-02	1	\N
00000025	02	00000002	\N	D	\N	2013-02-03	1	\N
00000025	02	00000002	\N	D	\N	2013-02-04	1	\N
00000025	02	00000002	\N	D	\N	2013-02-05	1	\N
00000025	02	00000002	\N	D	\N	2013-02-06	1	\N
00000025	02	00000002	\N	D	\N	2013-02-07	1	\N
00000025	02	00000002	\N	F	\N	2013-02-08	1	\N
00000025	02	00000002	\N	F	\N	2013-02-09	1	\N
00000025	02	00000002	\N	D	\N	2013-02-10	1	\N
00000025	02	00000002	\N	D	\N	2013-02-11	1	\N
00000025	02	00000002	\N	D	\N	2013-02-12	1	\N
00000025	02	00000002	\N	D	\N	2013-02-13	1	\N
00000025	02	00000002	\N	D	\N	2013-02-14	1	\N
00000025	02	00000002	\N	F	\N	2013-02-15	1	\N
00000025	02	00000002	\N	F	\N	2013-02-16	1	\N
00000025	02	00000002	\N	D	\N	2013-02-17	1	\N
00000025	02	00000002	\N	D	\N	2013-02-18	1	\N
00000025	02	00000002	\N	D	\N	2013-02-19	1	\N
00000025	02	00000002	\N	D	\N	2013-02-20	1	\N
00000025	02	00000002	\N	D	\N	2013-02-21	1	\N
00000025	02	00000002	\N	F	\N	2013-02-22	1	\N
00000025	02	00000002	\N	F	\N	2013-02-23	1	\N
00000025	02	00000002	\N	D	\N	2013-02-24	1	\N
00000025	02	00000002	\N	D	\N	2013-02-25	1	\N
00000025	02	00000002	\N	D	\N	2013-02-26	1	\N
00000025	02	00000002	\N	D	\N	2013-02-27	1	\N
00000025	02	00000002	\N	D	\N	2013-02-28	1	\N
00000026	02	00000002	\N	F	\N	2013-02-01	1	\N
00000026	02	00000002	\N	F	\N	2013-02-02	1	\N
00000026	02	00000002	\N	D	\N	2013-02-03	1	\N
00000026	02	00000002	\N	D	\N	2013-02-04	1	\N
00000026	02	00000002	\N	D	\N	2013-02-05	1	\N
00000026	02	00000002	\N	D	\N	2013-02-06	1	\N
00000026	02	00000002	\N	D	\N	2013-02-07	1	\N
00000026	02	00000002	\N	F	\N	2013-02-08	1	\N
00000026	02	00000002	\N	F	\N	2013-02-09	1	\N
00000026	02	00000002	\N	D	\N	2013-02-10	1	\N
00000026	02	00000002	\N	D	\N	2013-02-11	1	\N
00000026	02	00000002	\N	D	\N	2013-02-12	1	\N
00000026	02	00000002	\N	D	\N	2013-02-13	1	\N
00000026	02	00000002	\N	D	\N	2013-02-14	1	\N
00000026	02	00000002	\N	F	\N	2013-02-15	1	\N
00000026	02	00000002	\N	F	\N	2013-02-16	1	\N
00000026	02	00000002	\N	D	\N	2013-02-17	1	\N
00000026	02	00000002	\N	D	\N	2013-02-18	1	\N
00000026	02	00000002	\N	D	\N	2013-02-19	1	\N
00000026	02	00000002	\N	D	\N	2013-02-20	1	\N
00000026	02	00000002	\N	D	\N	2013-02-21	1	\N
00000026	02	00000002	\N	F	\N	2013-02-22	1	\N
00000026	02	00000002	\N	F	\N	2013-02-23	1	\N
00000026	02	00000002	\N	D	\N	2013-02-24	1	\N
00000026	02	00000002	\N	D	\N	2013-02-25	1	\N
00000026	02	00000002	\N	D	\N	2013-02-26	1	\N
00000026	02	00000002	\N	D	\N	2013-02-27	1	\N
00000026	02	00000002	\N	D	\N	2013-02-28	1	\N
00000027	02	00000002	\N	F	\N	2013-02-01	1	\N
00000027	02	00000002	\N	F	\N	2013-02-02	1	\N
00000027	02	00000002	\N	D	\N	2013-02-03	1	\N
00000027	02	00000002	\N	D	\N	2013-02-04	1	\N
00000027	02	00000002	\N	D	\N	2013-02-05	1	\N
00000027	02	00000002	\N	D	\N	2013-02-06	1	\N
00000027	02	00000002	\N	D	\N	2013-02-07	1	\N
00000027	02	00000002	\N	F	\N	2013-02-08	1	\N
00000027	02	00000002	\N	F	\N	2013-02-09	1	\N
00000027	02	00000002	\N	D	\N	2013-02-10	1	\N
00000027	02	00000002	\N	D	\N	2013-02-11	1	\N
00000027	02	00000002	\N	D	\N	2013-02-12	1	\N
00000027	02	00000002	\N	D	\N	2013-02-13	1	\N
00000027	02	00000002	\N	D	\N	2013-02-14	1	\N
00000027	02	00000002	\N	F	\N	2013-02-15	1	\N
00000027	02	00000002	\N	F	\N	2013-02-16	1	\N
00000027	02	00000002	\N	D	\N	2013-02-17	1	\N
00000027	02	00000002	\N	D	\N	2013-02-18	1	\N
00000027	02	00000002	\N	D	\N	2013-02-19	1	\N
00000027	02	00000002	\N	D	\N	2013-02-20	1	\N
00000027	02	00000002	\N	D	\N	2013-02-21	1	\N
00000027	02	00000002	\N	F	\N	2013-02-22	1	\N
00000027	02	00000002	\N	F	\N	2013-02-23	1	\N
00000027	02	00000002	\N	D	\N	2013-02-24	1	\N
00000027	02	00000002	\N	D	\N	2013-02-25	1	\N
00000027	02	00000002	\N	D	\N	2013-02-26	1	\N
00000027	02	00000002	\N	D	\N	2013-02-27	1	\N
00000027	02	00000002	\N	D	\N	2013-02-28	1	\N
00000028	02	00000002	\N	F	\N	2013-02-01	1	\N
00000028	02	00000002	\N	F	\N	2013-02-02	1	\N
00000028	02	00000002	\N	D	\N	2013-02-03	1	\N
00000028	02	00000002	\N	D	\N	2013-02-04	1	\N
00000028	02	00000002	\N	D	\N	2013-02-05	1	\N
00000028	02	00000002	\N	D	\N	2013-02-06	1	\N
00000028	02	00000002	\N	D	\N	2013-02-07	1	\N
00000028	02	00000002	\N	F	\N	2013-02-08	1	\N
00000028	02	00000002	\N	F	\N	2013-02-09	1	\N
00000028	02	00000002	\N	D	\N	2013-02-10	1	\N
00000028	02	00000002	\N	D	\N	2013-02-11	1	\N
00000028	02	00000002	\N	D	\N	2013-02-12	1	\N
00000028	02	00000002	\N	D	\N	2013-02-13	1	\N
00000028	02	00000002	\N	D	\N	2013-02-14	1	\N
00000028	02	00000002	\N	F	\N	2013-02-15	1	\N
00000028	02	00000002	\N	F	\N	2013-02-16	1	\N
00000028	02	00000002	\N	D	\N	2013-02-17	1	\N
00000028	02	00000002	\N	D	\N	2013-02-18	1	\N
00000028	02	00000002	\N	D	\N	2013-02-19	1	\N
00000028	02	00000002	\N	D	\N	2013-02-20	1	\N
00000028	02	00000002	\N	D	\N	2013-02-21	1	\N
00000028	02	00000002	\N	F	\N	2013-02-22	1	\N
00000028	02	00000002	\N	F	\N	2013-02-23	1	\N
00000028	02	00000002	\N	D	\N	2013-02-24	1	\N
00000028	02	00000002	\N	D	\N	2013-02-25	1	\N
00000028	02	00000002	\N	D	\N	2013-02-26	1	\N
00000028	02	00000002	\N	D	\N	2013-02-27	1	\N
00000028	02	00000002	\N	D	\N	2013-02-28	1	\N
00000029	02	00000002	\N	F	\N	2013-02-01	1	\N
00000029	02	00000002	\N	F	\N	2013-02-02	1	\N
00000029	02	00000002	\N	D	\N	2013-02-03	1	\N
00000029	02	00000002	\N	D	\N	2013-02-04	1	\N
00000029	02	00000002	\N	D	\N	2013-02-05	1	\N
00000029	02	00000002	\N	D	\N	2013-02-06	1	\N
00000029	02	00000002	\N	D	\N	2013-02-07	1	\N
00000029	02	00000002	\N	F	\N	2013-02-08	1	\N
00000029	02	00000002	\N	F	\N	2013-02-09	1	\N
00000029	02	00000002	\N	D	\N	2013-02-10	1	\N
00000029	02	00000002	\N	D	\N	2013-02-11	1	\N
00000029	02	00000002	\N	D	\N	2013-02-12	1	\N
00000029	02	00000002	\N	D	\N	2013-02-13	1	\N
00000029	02	00000002	\N	D	\N	2013-02-14	1	\N
00000029	02	00000002	\N	F	\N	2013-02-15	1	\N
00000029	02	00000002	\N	F	\N	2013-02-16	1	\N
00000029	02	00000002	\N	D	\N	2013-02-17	1	\N
00000029	02	00000002	\N	D	\N	2013-02-18	1	\N
00000029	02	00000002	\N	D	\N	2013-02-19	1	\N
00000029	02	00000002	\N	D	\N	2013-02-20	1	\N
00000029	02	00000002	\N	D	\N	2013-02-21	1	\N
00000029	02	00000002	\N	F	\N	2013-02-22	1	\N
00000029	02	00000002	\N	F	\N	2013-02-23	1	\N
00000029	02	00000002	\N	D	\N	2013-02-24	1	\N
00000029	02	00000002	\N	D	\N	2013-02-25	1	\N
00000029	02	00000002	\N	D	\N	2013-02-26	1	\N
00000029	02	00000002	\N	D	\N	2013-02-27	1	\N
00000029	02	00000002	\N	D	\N	2013-02-28	1	\N
00000030	02	00000002	\N	F	\N	2013-02-01	1	\N
00000030	02	00000002	\N	F	\N	2013-02-02	1	\N
00000030	02	00000002	\N	D	\N	2013-02-03	1	\N
00000030	02	00000002	\N	D	\N	2013-02-04	1	\N
00000030	02	00000002	\N	D	\N	2013-02-05	1	\N
00000030	02	00000002	\N	D	\N	2013-02-06	1	\N
00000030	02	00000002	\N	D	\N	2013-02-07	1	\N
00000030	02	00000002	\N	F	\N	2013-02-08	1	\N
00000030	02	00000002	\N	F	\N	2013-02-09	1	\N
00000030	02	00000002	\N	D	\N	2013-02-10	1	\N
00000030	02	00000002	\N	D	\N	2013-02-11	1	\N
00000030	02	00000002	\N	D	\N	2013-02-12	1	\N
00000030	02	00000002	\N	D	\N	2013-02-13	1	\N
00000030	02	00000002	\N	D	\N	2013-02-14	1	\N
00000030	02	00000002	\N	F	\N	2013-02-15	1	\N
00000030	02	00000002	\N	F	\N	2013-02-16	1	\N
00000030	02	00000002	\N	D	\N	2013-02-17	1	\N
00000030	02	00000002	\N	D	\N	2013-02-18	1	\N
00000030	02	00000002	\N	D	\N	2013-02-19	1	\N
00000030	02	00000002	\N	D	\N	2013-02-20	1	\N
00000030	02	00000002	\N	D	\N	2013-02-21	1	\N
00000030	02	00000002	\N	F	\N	2013-02-22	1	\N
00000030	02	00000002	\N	F	\N	2013-02-23	1	\N
00000030	02	00000002	\N	D	\N	2013-02-24	1	\N
00000030	02	00000002	\N	D	\N	2013-02-25	1	\N
00000030	02	00000002	\N	D	\N	2013-02-26	1	\N
00000030	02	00000002	\N	D	\N	2013-02-27	1	\N
00000030	02	00000002	\N	D	\N	2013-02-28	1	\N
00000031	02	00000002	\N	F	\N	2013-02-01	1	\N
00000031	02	00000002	\N	F	\N	2013-02-02	1	\N
00000031	02	00000002	\N	D	\N	2013-02-03	1	\N
00000031	02	00000002	\N	D	\N	2013-02-04	1	\N
00000031	02	00000002	\N	D	\N	2013-02-05	1	\N
00000031	02	00000002	\N	D	\N	2013-02-06	1	\N
00000031	02	00000002	\N	D	\N	2013-02-07	1	\N
00000031	02	00000002	\N	F	\N	2013-02-08	1	\N
00000031	02	00000002	\N	F	\N	2013-02-09	1	\N
00000031	02	00000002	\N	D	\N	2013-02-10	1	\N
00000031	02	00000002	\N	D	\N	2013-02-11	1	\N
00000031	02	00000002	\N	D	\N	2013-02-12	1	\N
00000031	02	00000002	\N	D	\N	2013-02-13	1	\N
00000031	02	00000002	\N	D	\N	2013-02-14	1	\N
00000031	02	00000002	\N	F	\N	2013-02-15	1	\N
00000031	02	00000002	\N	F	\N	2013-02-16	1	\N
00000031	02	00000002	\N	D	\N	2013-02-17	1	\N
00000031	02	00000002	\N	D	\N	2013-02-18	1	\N
00000031	02	00000002	\N	D	\N	2013-02-19	1	\N
00000031	02	00000002	\N	D	\N	2013-02-20	1	\N
00000031	02	00000002	\N	D	\N	2013-02-21	1	\N
00000031	02	00000002	\N	F	\N	2013-02-22	1	\N
00000031	02	00000002	\N	F	\N	2013-02-23	1	\N
00000031	02	00000002	\N	D	\N	2013-02-24	1	\N
00000031	02	00000002	\N	D	\N	2013-02-25	1	\N
00000031	02	00000002	\N	D	\N	2013-02-26	1	\N
00000031	02	00000002	\N	D	\N	2013-02-27	1	\N
00000031	02	00000002	\N	D	\N	2013-02-28	1	\N
00000032	02	00000002	\N	F	\N	2013-02-01	1	\N
00000032	02	00000002	\N	F	\N	2013-02-02	1	\N
00000032	02	00000002	\N	D	\N	2013-02-03	1	\N
00000032	02	00000002	\N	D	\N	2013-02-04	1	\N
00000032	02	00000002	\N	D	\N	2013-02-05	1	\N
00000032	02	00000002	\N	D	\N	2013-02-06	1	\N
00000032	02	00000002	\N	D	\N	2013-02-07	1	\N
00000032	02	00000002	\N	F	\N	2013-02-08	1	\N
00000032	02	00000002	\N	F	\N	2013-02-09	1	\N
00000032	02	00000002	\N	D	\N	2013-02-10	1	\N
00000032	02	00000002	\N	D	\N	2013-02-11	1	\N
00000032	02	00000002	\N	D	\N	2013-02-12	1	\N
00000032	02	00000002	\N	D	\N	2013-02-13	1	\N
00000032	02	00000002	\N	D	\N	2013-02-14	1	\N
00000032	02	00000002	\N	F	\N	2013-02-15	1	\N
00000032	02	00000002	\N	F	\N	2013-02-16	1	\N
00000032	02	00000002	\N	D	\N	2013-02-17	1	\N
00000032	02	00000002	\N	D	\N	2013-02-18	1	\N
00000032	02	00000002	\N	D	\N	2013-02-19	1	\N
00000032	02	00000002	\N	D	\N	2013-02-20	1	\N
00000032	02	00000002	\N	D	\N	2013-02-21	1	\N
00000032	02	00000002	\N	F	\N	2013-02-22	1	\N
00000032	02	00000002	\N	F	\N	2013-02-23	1	\N
00000032	02	00000002	\N	D	\N	2013-02-24	1	\N
00000032	02	00000002	\N	D	\N	2013-02-25	1	\N
00000032	02	00000002	\N	D	\N	2013-02-26	1	\N
00000032	02	00000002	\N	D	\N	2013-02-27	1	\N
00000032	02	00000002	\N	D	\N	2013-02-28	1	\N
00000033	02	00000002	\N	F	\N	2013-02-01	1	\N
00000033	02	00000002	\N	F	\N	2013-02-02	1	\N
00000033	02	00000002	\N	D	\N	2013-02-03	1	\N
00000033	02	00000002	\N	D	\N	2013-02-04	1	\N
00000033	02	00000002	\N	D	\N	2013-02-05	1	\N
00000033	02	00000002	\N	D	\N	2013-02-06	1	\N
00000033	02	00000002	\N	D	\N	2013-02-07	1	\N
00000033	02	00000002	\N	F	\N	2013-02-08	1	\N
00000033	02	00000002	\N	F	\N	2013-02-09	1	\N
00000033	02	00000002	\N	D	\N	2013-02-10	1	\N
00000033	02	00000002	\N	D	\N	2013-02-11	1	\N
00000033	02	00000002	\N	D	\N	2013-02-12	1	\N
00000033	02	00000002	\N	D	\N	2013-02-13	1	\N
00000033	02	00000002	\N	D	\N	2013-02-14	1	\N
00000033	02	00000002	\N	F	\N	2013-02-15	1	\N
00000033	02	00000002	\N	F	\N	2013-02-16	1	\N
00000033	02	00000002	\N	D	\N	2013-02-17	1	\N
00000033	02	00000002	\N	D	\N	2013-02-18	1	\N
00000033	02	00000002	\N	D	\N	2013-02-19	1	\N
00000033	02	00000002	\N	D	\N	2013-02-20	1	\N
00000033	02	00000002	\N	D	\N	2013-02-21	1	\N
00000033	02	00000002	\N	F	\N	2013-02-22	1	\N
00000033	02	00000002	\N	F	\N	2013-02-23	1	\N
00000033	02	00000002	\N	D	\N	2013-02-24	1	\N
00000033	02	00000002	\N	D	\N	2013-02-25	1	\N
00000033	02	00000002	\N	D	\N	2013-02-26	1	\N
00000033	02	00000002	\N	D	\N	2013-02-27	1	\N
00000033	02	00000002	\N	D	\N	2013-02-28	1	\N
00000034	02	00000002	\N	F	\N	2013-02-01	1	\N
00000034	02	00000002	\N	F	\N	2013-02-02	1	\N
00000034	02	00000002	\N	D	\N	2013-02-03	1	\N
00000034	02	00000002	\N	D	\N	2013-02-04	1	\N
00000034	02	00000002	\N	D	\N	2013-02-05	1	\N
00000034	02	00000002	\N	D	\N	2013-02-06	1	\N
00000034	02	00000002	\N	D	\N	2013-02-07	1	\N
00000034	02	00000002	\N	F	\N	2013-02-08	1	\N
00000034	02	00000002	\N	F	\N	2013-02-09	1	\N
00000034	02	00000002	\N	D	\N	2013-02-10	1	\N
00000034	02	00000002	\N	D	\N	2013-02-11	1	\N
00000034	02	00000002	\N	D	\N	2013-02-12	1	\N
00000034	02	00000002	\N	D	\N	2013-02-13	1	\N
00000034	02	00000002	\N	D	\N	2013-02-14	1	\N
00000034	02	00000002	\N	F	\N	2013-02-15	1	\N
00000034	02	00000002	\N	F	\N	2013-02-16	1	\N
00000034	02	00000002	\N	D	\N	2013-02-17	1	\N
00000034	02	00000002	\N	D	\N	2013-02-18	1	\N
00000034	02	00000002	\N	D	\N	2013-02-19	1	\N
00000034	02	00000002	\N	D	\N	2013-02-20	1	\N
00000034	02	00000002	\N	D	\N	2013-02-21	1	\N
00000034	02	00000002	\N	F	\N	2013-02-22	1	\N
00000034	02	00000002	\N	F	\N	2013-02-23	1	\N
00000034	02	00000002	\N	D	\N	2013-02-24	1	\N
00000034	02	00000002	\N	D	\N	2013-02-25	1	\N
00000034	02	00000002	\N	D	\N	2013-02-26	1	\N
00000034	02	00000002	\N	D	\N	2013-02-27	1	\N
00000034	02	00000002	\N	D	\N	2013-02-28	1	\N
00000035	02	00000002	\N	F	\N	2013-02-01	1	\N
00000035	02	00000002	\N	F	\N	2013-02-02	1	\N
00000035	02	00000002	\N	D	\N	2013-02-03	1	\N
00000035	02	00000002	\N	D	\N	2013-02-04	1	\N
00000035	02	00000002	\N	D	\N	2013-02-05	1	\N
00000035	02	00000002	\N	D	\N	2013-02-06	1	\N
00000035	02	00000002	\N	D	\N	2013-02-07	1	\N
00000035	02	00000002	\N	F	\N	2013-02-08	1	\N
00000035	02	00000002	\N	F	\N	2013-02-09	1	\N
00000035	02	00000002	\N	D	\N	2013-02-10	1	\N
00000035	02	00000002	\N	D	\N	2013-02-11	1	\N
00000035	02	00000002	\N	D	\N	2013-02-12	1	\N
00000035	02	00000002	\N	D	\N	2013-02-13	1	\N
00000035	02	00000002	\N	D	\N	2013-02-14	1	\N
00000035	02	00000002	\N	F	\N	2013-02-15	1	\N
00000035	02	00000002	\N	F	\N	2013-02-16	1	\N
00000035	02	00000002	\N	D	\N	2013-02-17	1	\N
00000035	02	00000002	\N	D	\N	2013-02-18	1	\N
00000035	02	00000002	\N	D	\N	2013-02-19	1	\N
00000035	02	00000002	\N	D	\N	2013-02-20	1	\N
00000035	02	00000002	\N	D	\N	2013-02-21	1	\N
00000035	02	00000002	\N	F	\N	2013-02-22	1	\N
00000035	02	00000002	\N	F	\N	2013-02-23	1	\N
00000035	02	00000002	\N	D	\N	2013-02-24	1	\N
00000035	02	00000002	\N	D	\N	2013-02-25	1	\N
00000035	02	00000002	\N	D	\N	2013-02-26	1	\N
00000035	02	00000002	\N	D	\N	2013-02-27	1	\N
00000035	02	00000002	\N	D	\N	2013-02-28	1	\N
00000036	02	00000002	\N	F	\N	2013-02-01	1	\N
00000036	02	00000002	\N	F	\N	2013-02-02	1	\N
00000036	02	00000002	\N	D	\N	2013-02-03	1	\N
00000036	02	00000002	\N	D	\N	2013-02-04	1	\N
00000036	02	00000002	\N	D	\N	2013-02-05	1	\N
00000036	02	00000002	\N	D	\N	2013-02-06	1	\N
00000036	02	00000002	\N	D	\N	2013-02-07	1	\N
00000036	02	00000002	\N	F	\N	2013-02-08	1	\N
00000036	02	00000002	\N	F	\N	2013-02-09	1	\N
00000036	02	00000002	\N	D	\N	2013-02-10	1	\N
00000036	02	00000002	\N	D	\N	2013-02-11	1	\N
00000036	02	00000002	\N	D	\N	2013-02-12	1	\N
00000036	02	00000002	\N	D	\N	2013-02-13	1	\N
00000036	02	00000002	\N	D	\N	2013-02-14	1	\N
00000036	02	00000002	\N	F	\N	2013-02-15	1	\N
00000036	02	00000002	\N	F	\N	2013-02-16	1	\N
00000036	02	00000002	\N	D	\N	2013-02-17	1	\N
00000036	02	00000002	\N	D	\N	2013-02-18	1	\N
00000036	02	00000002	\N	D	\N	2013-02-19	1	\N
00000036	02	00000002	\N	D	\N	2013-02-20	1	\N
00000036	02	00000002	\N	D	\N	2013-02-21	1	\N
00000036	02	00000002	\N	F	\N	2013-02-22	1	\N
00000036	02	00000002	\N	F	\N	2013-02-23	1	\N
00000036	02	00000002	\N	D	\N	2013-02-24	1	\N
00000036	02	00000002	\N	D	\N	2013-02-25	1	\N
00000036	02	00000002	\N	D	\N	2013-02-26	1	\N
00000036	02	00000002	\N	D	\N	2013-02-27	1	\N
00000036	02	00000002	\N	D	\N	2013-02-28	1	\N
00000037	02	00000002	\N	F	\N	2013-02-01	1	\N
00000037	02	00000002	\N	F	\N	2013-02-02	1	\N
00000037	02	00000002	\N	D	\N	2013-02-03	1	\N
00000037	02	00000002	\N	D	\N	2013-02-04	1	\N
00000037	02	00000002	\N	D	\N	2013-02-05	1	\N
00000037	02	00000002	\N	D	\N	2013-02-06	1	\N
00000037	02	00000002	\N	D	\N	2013-02-07	1	\N
00000037	02	00000002	\N	F	\N	2013-02-08	1	\N
00000037	02	00000002	\N	F	\N	2013-02-09	1	\N
00000037	02	00000002	\N	D	\N	2013-02-10	1	\N
00000037	02	00000002	\N	D	\N	2013-02-11	1	\N
00000037	02	00000002	\N	D	\N	2013-02-12	1	\N
00000037	02	00000002	\N	D	\N	2013-02-13	1	\N
00000037	02	00000002	\N	D	\N	2013-02-14	1	\N
00000037	02	00000002	\N	F	\N	2013-02-15	1	\N
00000037	02	00000002	\N	F	\N	2013-02-16	1	\N
00000037	02	00000002	\N	D	\N	2013-02-17	1	\N
00000037	02	00000002	\N	D	\N	2013-02-18	1	\N
00000037	02	00000002	\N	D	\N	2013-02-19	1	\N
00000037	02	00000002	\N	D	\N	2013-02-20	1	\N
00000037	02	00000002	\N	D	\N	2013-02-21	1	\N
00000037	02	00000002	\N	F	\N	2013-02-22	1	\N
00000037	02	00000002	\N	F	\N	2013-02-23	1	\N
00000037	02	00000002	\N	D	\N	2013-02-24	1	\N
00000037	02	00000002	\N	D	\N	2013-02-25	1	\N
00000037	02	00000002	\N	D	\N	2013-02-26	1	\N
00000037	02	00000002	\N	D	\N	2013-02-27	1	\N
00000037	02	00000002	\N	D	\N	2013-02-28	1	\N
00000038	02	00000002	\N	F	\N	2013-02-01	1	\N
00000038	02	00000002	\N	F	\N	2013-02-02	1	\N
00000038	02	00000002	\N	D	\N	2013-02-03	1	\N
00000038	02	00000002	\N	D	\N	2013-02-04	1	\N
00000038	02	00000002	\N	D	\N	2013-02-05	1	\N
00000038	02	00000002	\N	D	\N	2013-02-06	1	\N
00000038	02	00000002	\N	D	\N	2013-02-07	1	\N
00000038	02	00000002	\N	F	\N	2013-02-08	1	\N
00000038	02	00000002	\N	F	\N	2013-02-09	1	\N
00000038	02	00000002	\N	D	\N	2013-02-10	1	\N
00000038	02	00000002	\N	D	\N	2013-02-11	1	\N
00000038	02	00000002	\N	D	\N	2013-02-12	1	\N
00000038	02	00000002	\N	D	\N	2013-02-13	1	\N
00000038	02	00000002	\N	D	\N	2013-02-14	1	\N
00000038	02	00000002	\N	F	\N	2013-02-15	1	\N
00000038	02	00000002	\N	F	\N	2013-02-16	1	\N
00000038	02	00000002	\N	D	\N	2013-02-17	1	\N
00000038	02	00000002	\N	D	\N	2013-02-18	1	\N
00000038	02	00000002	\N	D	\N	2013-02-19	1	\N
00000038	02	00000002	\N	D	\N	2013-02-20	1	\N
00000038	02	00000002	\N	D	\N	2013-02-21	1	\N
00000038	02	00000002	\N	F	\N	2013-02-22	1	\N
00000038	02	00000002	\N	F	\N	2013-02-23	1	\N
00000038	02	00000002	\N	D	\N	2013-02-24	1	\N
00000038	02	00000002	\N	D	\N	2013-02-25	1	\N
00000038	02	00000002	\N	D	\N	2013-02-26	1	\N
00000038	02	00000002	\N	D	\N	2013-02-27	1	\N
00000038	02	00000002	\N	D	\N	2013-02-28	1	\N
00000039	02	00000002	\N	F	\N	2013-02-01	1	\N
00000039	02	00000002	\N	F	\N	2013-02-02	1	\N
00000039	02	00000002	\N	D	\N	2013-02-03	1	\N
00000039	02	00000002	\N	D	\N	2013-02-04	1	\N
00000039	02	00000002	\N	D	\N	2013-02-05	1	\N
00000039	02	00000002	\N	D	\N	2013-02-06	1	\N
00000039	02	00000002	\N	D	\N	2013-02-07	1	\N
00000039	02	00000002	\N	F	\N	2013-02-08	1	\N
00000039	02	00000002	\N	F	\N	2013-02-09	1	\N
00000039	02	00000002	\N	D	\N	2013-02-10	1	\N
00000039	02	00000002	\N	D	\N	2013-02-11	1	\N
00000039	02	00000002	\N	D	\N	2013-02-12	1	\N
00000039	02	00000002	\N	D	\N	2013-02-13	1	\N
00000039	02	00000002	\N	D	\N	2013-02-14	1	\N
00000039	02	00000002	\N	F	\N	2013-02-15	1	\N
00000039	02	00000002	\N	F	\N	2013-02-16	1	\N
00000039	02	00000002	\N	D	\N	2013-02-17	1	\N
00000039	02	00000002	\N	D	\N	2013-02-18	1	\N
00000039	02	00000002	\N	D	\N	2013-02-19	1	\N
00000039	02	00000002	\N	D	\N	2013-02-20	1	\N
00000039	02	00000002	\N	D	\N	2013-02-21	1	\N
00000039	02	00000002	\N	F	\N	2013-02-22	1	\N
00000039	02	00000002	\N	F	\N	2013-02-23	1	\N
00000039	02	00000002	\N	D	\N	2013-02-24	1	\N
00000039	02	00000002	\N	D	\N	2013-02-25	1	\N
00000039	02	00000002	\N	D	\N	2013-02-26	1	\N
00000039	02	00000002	\N	D	\N	2013-02-27	1	\N
00000039	02	00000002	\N	D	\N	2013-02-28	1	\N
00000040	02	00000002	\N	F	\N	2013-02-01	1	\N
00000040	02	00000002	\N	F	\N	2013-02-02	1	\N
00000040	02	00000002	\N	D	\N	2013-02-03	1	\N
00000040	02	00000002	\N	D	\N	2013-02-04	1	\N
00000040	02	00000002	\N	D	\N	2013-02-05	1	\N
00000040	02	00000002	\N	D	\N	2013-02-06	1	\N
00000040	02	00000002	\N	D	\N	2013-02-07	1	\N
00000040	02	00000002	\N	F	\N	2013-02-08	1	\N
00000040	02	00000002	\N	F	\N	2013-02-09	1	\N
00000040	02	00000002	\N	D	\N	2013-02-10	1	\N
00000040	02	00000002	\N	D	\N	2013-02-11	1	\N
00000040	02	00000002	\N	D	\N	2013-02-12	1	\N
00000040	02	00000002	\N	D	\N	2013-02-13	1	\N
00000040	02	00000002	\N	D	\N	2013-02-14	1	\N
00000040	02	00000002	\N	F	\N	2013-02-15	1	\N
00000040	02	00000002	\N	F	\N	2013-02-16	1	\N
00000040	02	00000002	\N	D	\N	2013-02-17	1	\N
00000040	02	00000002	\N	D	\N	2013-02-18	1	\N
00000040	02	00000002	\N	D	\N	2013-02-19	1	\N
00000040	02	00000002	\N	D	\N	2013-02-20	1	\N
00000040	02	00000002	\N	D	\N	2013-02-21	1	\N
00000040	02	00000002	\N	F	\N	2013-02-22	1	\N
00000040	02	00000002	\N	F	\N	2013-02-23	1	\N
00000040	02	00000002	\N	D	\N	2013-02-24	1	\N
00000040	02	00000002	\N	D	\N	2013-02-25	1	\N
00000040	02	00000002	\N	D	\N	2013-02-26	1	\N
00000040	02	00000002	\N	D	\N	2013-02-27	1	\N
00000040	02	00000002	\N	D	\N	2013-02-28	1	\N
00000041	02	00000002	\N	F	\N	2013-02-01	1	\N
00000041	02	00000002	\N	F	\N	2013-02-02	1	\N
00000041	02	00000002	\N	D	\N	2013-02-03	1	\N
00000041	02	00000002	\N	D	\N	2013-02-04	1	\N
00000041	02	00000002	\N	D	\N	2013-02-05	1	\N
00000041	02	00000002	\N	D	\N	2013-02-06	1	\N
00000041	02	00000002	\N	D	\N	2013-02-07	1	\N
00000041	02	00000002	\N	F	\N	2013-02-08	1	\N
00000041	02	00000002	\N	F	\N	2013-02-09	1	\N
00000041	02	00000002	\N	D	\N	2013-02-10	1	\N
00000041	02	00000002	\N	D	\N	2013-02-11	1	\N
00000041	02	00000002	\N	D	\N	2013-02-12	1	\N
00000041	02	00000002	\N	D	\N	2013-02-13	1	\N
00000041	02	00000002	\N	D	\N	2013-02-14	1	\N
00000041	02	00000002	\N	F	\N	2013-02-15	1	\N
00000041	02	00000002	\N	F	\N	2013-02-16	1	\N
00000041	02	00000002	\N	D	\N	2013-02-17	1	\N
00000041	02	00000002	\N	D	\N	2013-02-18	1	\N
00000041	02	00000002	\N	D	\N	2013-02-19	1	\N
00000041	02	00000002	\N	D	\N	2013-02-20	1	\N
00000041	02	00000002	\N	D	\N	2013-02-21	1	\N
00000041	02	00000002	\N	F	\N	2013-02-22	1	\N
00000041	02	00000002	\N	F	\N	2013-02-23	1	\N
00000041	02	00000002	\N	D	\N	2013-02-24	1	\N
00000041	02	00000002	\N	D	\N	2013-02-25	1	\N
00000041	02	00000002	\N	D	\N	2013-02-26	1	\N
00000041	02	00000002	\N	D	\N	2013-02-27	1	\N
00000041	02	00000002	\N	D	\N	2013-02-28	1	\N
00000017	02	00000002	\N	F	\N	2013-03-01	1	\N
00000017	02	00000002	\N	F	\N	2013-03-02	1	\N
00000017	02	00000002	\N	D	\N	2013-03-03	1	\N
00000017	02	00000002	\N	D	\N	2013-03-04	1	\N
00000017	02	00000002	\N	D	\N	2013-03-05	1	\N
00000017	02	00000002	\N	D	\N	2013-03-06	1	\N
00000017	02	00000002	\N	D	\N	2013-03-07	1	\N
00000017	02	00000002	\N	F	\N	2013-03-08	1	\N
00000017	02	00000002	\N	F	\N	2013-03-09	1	\N
00000017	02	00000002	\N	D	\N	2013-03-10	1	\N
00000017	02	00000002	\N	D	\N	2013-03-11	1	\N
00000017	02	00000002	\N	D	\N	2013-03-12	1	\N
00000017	02	00000002	\N	D	\N	2013-03-13	1	\N
00000017	02	00000002	\N	D	\N	2013-03-14	1	\N
00000017	02	00000002	\N	F	\N	2013-03-15	1	\N
00000017	02	00000002	\N	F	\N	2013-03-16	1	\N
00000017	02	00000002	\N	D	\N	2013-03-17	1	\N
00000017	02	00000002	\N	D	\N	2013-03-18	1	\N
00000017	02	00000002	\N	D	\N	2013-03-19	1	\N
00000017	02	00000002	\N	D	\N	2013-03-20	1	\N
00000017	02	00000002	\N	D	\N	2013-03-21	1	\N
00000017	02	00000002	\N	F	\N	2013-03-22	1	\N
00000017	02	00000002	\N	F	\N	2013-03-23	1	\N
00000017	02	00000002	\N	D	\N	2013-03-24	1	\N
00000017	02	00000002	\N	D	\N	2013-03-25	1	\N
00000017	02	00000002	\N	D	\N	2013-03-26	1	\N
00000017	02	00000002	\N	D	\N	2013-03-27	1	\N
00000017	02	00000002	\N	D	\N	2013-03-28	1	\N
00000017	02	00000002	\N	F	\N	2013-03-29	1	\N
00000017	02	00000002	\N	F	\N	2013-03-30	1	\N
00000017	02	00000002	\N	D	\N	2013-03-31	1	\N
00000018	02	00000002	\N	F	\N	2013-03-01	1	\N
00000018	02	00000002	\N	F	\N	2013-03-02	1	\N
00000018	02	00000002	\N	D	\N	2013-03-03	1	\N
00000018	02	00000002	\N	D	\N	2013-03-04	1	\N
00000018	02	00000002	\N	D	\N	2013-03-05	1	\N
00000018	02	00000002	\N	D	\N	2013-03-06	1	\N
00000018	02	00000002	\N	D	\N	2013-03-07	1	\N
00000018	02	00000002	\N	F	\N	2013-03-08	1	\N
00000018	02	00000002	\N	F	\N	2013-03-09	1	\N
00000018	02	00000002	\N	D	\N	2013-03-10	1	\N
00000018	02	00000002	\N	D	\N	2013-03-11	1	\N
00000018	02	00000002	\N	D	\N	2013-03-12	1	\N
00000018	02	00000002	\N	D	\N	2013-03-13	1	\N
00000018	02	00000002	\N	D	\N	2013-03-14	1	\N
00000018	02	00000002	\N	F	\N	2013-03-15	1	\N
00000018	02	00000002	\N	F	\N	2013-03-16	1	\N
00000018	02	00000002	\N	D	\N	2013-03-17	1	\N
00000018	02	00000002	\N	D	\N	2013-03-18	1	\N
00000018	02	00000002	\N	D	\N	2013-03-19	1	\N
00000018	02	00000002	\N	D	\N	2013-03-20	1	\N
00000018	02	00000002	\N	D	\N	2013-03-21	1	\N
00000018	02	00000002	\N	F	\N	2013-03-22	1	\N
00000018	02	00000002	\N	F	\N	2013-03-23	1	\N
00000018	02	00000002	\N	D	\N	2013-03-24	1	\N
00000018	02	00000002	\N	D	\N	2013-03-25	1	\N
00000018	02	00000002	\N	D	\N	2013-03-26	1	\N
00000018	02	00000002	\N	D	\N	2013-03-27	1	\N
00000018	02	00000002	\N	D	\N	2013-03-28	1	\N
00000018	02	00000002	\N	F	\N	2013-03-29	1	\N
00000018	02	00000002	\N	F	\N	2013-03-30	1	\N
00000018	02	00000002	\N	D	\N	2013-03-31	1	\N
00000019	02	00000002	\N	F	\N	2013-03-01	1	\N
00000019	02	00000002	\N	F	\N	2013-03-02	1	\N
00000019	02	00000002	\N	D	\N	2013-03-03	1	\N
00000019	02	00000002	\N	D	\N	2013-03-04	1	\N
00000019	02	00000002	\N	D	\N	2013-03-05	1	\N
00000019	02	00000002	\N	D	\N	2013-03-06	1	\N
00000019	02	00000002	\N	D	\N	2013-03-07	1	\N
00000019	02	00000002	\N	F	\N	2013-03-08	1	\N
00000019	02	00000002	\N	F	\N	2013-03-09	1	\N
00000019	02	00000002	\N	D	\N	2013-03-10	1	\N
00000019	02	00000002	\N	D	\N	2013-03-11	1	\N
00000019	02	00000002	\N	D	\N	2013-03-12	1	\N
00000019	02	00000002	\N	D	\N	2013-03-13	1	\N
00000019	02	00000002	\N	D	\N	2013-03-14	1	\N
00000019	02	00000002	\N	F	\N	2013-03-15	1	\N
00000019	02	00000002	\N	F	\N	2013-03-16	1	\N
00000019	02	00000002	\N	D	\N	2013-03-17	1	\N
00000019	02	00000002	\N	D	\N	2013-03-18	1	\N
00000019	02	00000002	\N	D	\N	2013-03-19	1	\N
00000019	02	00000002	\N	D	\N	2013-03-20	1	\N
00000019	02	00000002	\N	D	\N	2013-03-21	1	\N
00000019	02	00000002	\N	F	\N	2013-03-22	1	\N
00000019	02	00000002	\N	F	\N	2013-03-23	1	\N
00000019	02	00000002	\N	D	\N	2013-03-24	1	\N
00000019	02	00000002	\N	D	\N	2013-03-25	1	\N
00000019	02	00000002	\N	D	\N	2013-03-26	1	\N
00000019	02	00000002	\N	D	\N	2013-03-27	1	\N
00000019	02	00000002	\N	D	\N	2013-03-28	1	\N
00000019	02	00000002	\N	F	\N	2013-03-29	1	\N
00000019	02	00000002	\N	F	\N	2013-03-30	1	\N
00000019	02	00000002	\N	D	\N	2013-03-31	1	\N
00000020	02	00000002	\N	F	\N	2013-03-01	1	\N
00000020	02	00000002	\N	F	\N	2013-03-02	1	\N
00000020	02	00000002	\N	D	\N	2013-03-03	1	\N
00000020	02	00000002	\N	D	\N	2013-03-04	1	\N
00000020	02	00000002	\N	D	\N	2013-03-05	1	\N
00000020	02	00000002	\N	D	\N	2013-03-06	1	\N
00000020	02	00000002	\N	D	\N	2013-03-07	1	\N
00000020	02	00000002	\N	F	\N	2013-03-08	1	\N
00000020	02	00000002	\N	F	\N	2013-03-09	1	\N
00000020	02	00000002	\N	D	\N	2013-03-10	1	\N
00000020	02	00000002	\N	D	\N	2013-03-11	1	\N
00000020	02	00000002	\N	D	\N	2013-03-12	1	\N
00000020	02	00000002	\N	D	\N	2013-03-13	1	\N
00000020	02	00000002	\N	D	\N	2013-03-14	1	\N
00000020	02	00000002	\N	F	\N	2013-03-15	1	\N
00000020	02	00000002	\N	F	\N	2013-03-16	1	\N
00000020	02	00000002	\N	D	\N	2013-03-17	1	\N
00000020	02	00000002	\N	D	\N	2013-03-18	1	\N
00000020	02	00000002	\N	D	\N	2013-03-19	1	\N
00000020	02	00000002	\N	D	\N	2013-03-20	1	\N
00000020	02	00000002	\N	D	\N	2013-03-21	1	\N
00000020	02	00000002	\N	F	\N	2013-03-22	1	\N
00000020	02	00000002	\N	F	\N	2013-03-23	1	\N
00000020	02	00000002	\N	D	\N	2013-03-24	1	\N
00000020	02	00000002	\N	D	\N	2013-03-25	1	\N
00000020	02	00000002	\N	D	\N	2013-03-26	1	\N
00000020	02	00000002	\N	D	\N	2013-03-27	1	\N
00000020	02	00000002	\N	D	\N	2013-03-28	1	\N
00000020	02	00000002	\N	F	\N	2013-03-29	1	\N
00000020	02	00000002	\N	F	\N	2013-03-30	1	\N
00000020	02	00000002	\N	D	\N	2013-03-31	1	\N
00000021	02	00000002	\N	F	\N	2013-03-01	1	\N
00000021	02	00000002	\N	F	\N	2013-03-02	1	\N
00000021	02	00000002	\N	D	\N	2013-03-03	1	\N
00000021	02	00000002	\N	D	\N	2013-03-04	1	\N
00000021	02	00000002	\N	D	\N	2013-03-05	1	\N
00000021	02	00000002	\N	D	\N	2013-03-06	1	\N
00000021	02	00000002	\N	D	\N	2013-03-07	1	\N
00000021	02	00000002	\N	F	\N	2013-03-08	1	\N
00000021	02	00000002	\N	F	\N	2013-03-09	1	\N
00000021	02	00000002	\N	D	\N	2013-03-10	1	\N
00000021	02	00000002	\N	D	\N	2013-03-11	1	\N
00000021	02	00000002	\N	D	\N	2013-03-12	1	\N
00000021	02	00000002	\N	D	\N	2013-03-13	1	\N
00000021	02	00000002	\N	D	\N	2013-03-14	1	\N
00000021	02	00000002	\N	F	\N	2013-03-15	1	\N
00000021	02	00000002	\N	F	\N	2013-03-16	1	\N
00000021	02	00000002	\N	D	\N	2013-03-17	1	\N
00000021	02	00000002	\N	D	\N	2013-03-18	1	\N
00000021	02	00000002	\N	D	\N	2013-03-19	1	\N
00000021	02	00000002	\N	D	\N	2013-03-20	1	\N
00000021	02	00000002	\N	D	\N	2013-03-21	1	\N
00000021	02	00000002	\N	F	\N	2013-03-22	1	\N
00000021	02	00000002	\N	F	\N	2013-03-23	1	\N
00000021	02	00000002	\N	D	\N	2013-03-24	1	\N
00000021	02	00000002	\N	D	\N	2013-03-25	1	\N
00000021	02	00000002	\N	D	\N	2013-03-26	1	\N
00000021	02	00000002	\N	D	\N	2013-03-27	1	\N
00000021	02	00000002	\N	D	\N	2013-03-28	1	\N
00000021	02	00000002	\N	F	\N	2013-03-29	1	\N
00000021	02	00000002	\N	F	\N	2013-03-30	1	\N
00000021	02	00000002	\N	D	\N	2013-03-31	1	\N
00000022	02	00000002	\N	F	\N	2013-03-01	1	\N
00000022	02	00000002	\N	F	\N	2013-03-02	1	\N
00000022	02	00000002	\N	D	\N	2013-03-03	1	\N
00000022	02	00000002	\N	D	\N	2013-03-04	1	\N
00000022	02	00000002	\N	D	\N	2013-03-05	1	\N
00000022	02	00000002	\N	D	\N	2013-03-06	1	\N
00000022	02	00000002	\N	D	\N	2013-03-07	1	\N
00000022	02	00000002	\N	F	\N	2013-03-08	1	\N
00000022	02	00000002	\N	F	\N	2013-03-09	1	\N
00000022	02	00000002	\N	D	\N	2013-03-10	1	\N
00000022	02	00000002	\N	D	\N	2013-03-11	1	\N
00000022	02	00000002	\N	D	\N	2013-03-12	1	\N
00000022	02	00000002	\N	D	\N	2013-03-13	1	\N
00000022	02	00000002	\N	D	\N	2013-03-14	1	\N
00000022	02	00000002	\N	F	\N	2013-03-15	1	\N
00000022	02	00000002	\N	F	\N	2013-03-16	1	\N
00000022	02	00000002	\N	D	\N	2013-03-17	1	\N
00000022	02	00000002	\N	D	\N	2013-03-18	1	\N
00000022	02	00000002	\N	D	\N	2013-03-19	1	\N
00000022	02	00000002	\N	D	\N	2013-03-20	1	\N
00000022	02	00000002	\N	D	\N	2013-03-21	1	\N
00000022	02	00000002	\N	F	\N	2013-03-22	1	\N
00000022	02	00000002	\N	F	\N	2013-03-23	1	\N
00000022	02	00000002	\N	D	\N	2013-03-24	1	\N
00000022	02	00000002	\N	D	\N	2013-03-25	1	\N
00000022	02	00000002	\N	D	\N	2013-03-26	1	\N
00000022	02	00000002	\N	D	\N	2013-03-27	1	\N
00000022	02	00000002	\N	D	\N	2013-03-28	1	\N
00000022	02	00000002	\N	F	\N	2013-03-29	1	\N
00000022	02	00000002	\N	F	\N	2013-03-30	1	\N
00000022	02	00000002	\N	D	\N	2013-03-31	1	\N
00000023	02	00000002	\N	F	\N	2013-03-01	1	\N
00000023	02	00000002	\N	F	\N	2013-03-02	1	\N
00000023	02	00000002	\N	D	\N	2013-03-03	1	\N
00000023	02	00000002	\N	D	\N	2013-03-04	1	\N
00000023	02	00000002	\N	D	\N	2013-03-05	1	\N
00000023	02	00000002	\N	D	\N	2013-03-06	1	\N
00000023	02	00000002	\N	D	\N	2013-03-07	1	\N
00000023	02	00000002	\N	F	\N	2013-03-08	1	\N
00000023	02	00000002	\N	F	\N	2013-03-09	1	\N
00000023	02	00000002	\N	D	\N	2013-03-10	1	\N
00000023	02	00000002	\N	D	\N	2013-03-11	1	\N
00000023	02	00000002	\N	D	\N	2013-03-12	1	\N
00000023	02	00000002	\N	D	\N	2013-03-13	1	\N
00000023	02	00000002	\N	D	\N	2013-03-14	1	\N
00000023	02	00000002	\N	F	\N	2013-03-15	1	\N
00000023	02	00000002	\N	F	\N	2013-03-16	1	\N
00000023	02	00000002	\N	D	\N	2013-03-17	1	\N
00000023	02	00000002	\N	D	\N	2013-03-18	1	\N
00000023	02	00000002	\N	D	\N	2013-03-19	1	\N
00000023	02	00000002	\N	D	\N	2013-03-20	1	\N
00000023	02	00000002	\N	D	\N	2013-03-21	1	\N
00000023	02	00000002	\N	F	\N	2013-03-22	1	\N
00000023	02	00000002	\N	F	\N	2013-03-23	1	\N
00000023	02	00000002	\N	D	\N	2013-03-24	1	\N
00000023	02	00000002	\N	D	\N	2013-03-25	1	\N
00000023	02	00000002	\N	D	\N	2013-03-26	1	\N
00000023	02	00000002	\N	D	\N	2013-03-27	1	\N
00000023	02	00000002	\N	D	\N	2013-03-28	1	\N
00000023	02	00000002	\N	F	\N	2013-03-29	1	\N
00000023	02	00000002	\N	F	\N	2013-03-30	1	\N
00000023	02	00000002	\N	D	\N	2013-03-31	1	\N
00000024	02	00000002	\N	F	\N	2013-03-01	1	\N
00000024	02	00000002	\N	F	\N	2013-03-02	1	\N
00000024	02	00000002	\N	D	\N	2013-03-03	1	\N
00000024	02	00000002	\N	D	\N	2013-03-04	1	\N
00000024	02	00000002	\N	D	\N	2013-03-05	1	\N
00000024	02	00000002	\N	D	\N	2013-03-06	1	\N
00000024	02	00000002	\N	D	\N	2013-03-07	1	\N
00000024	02	00000002	\N	F	\N	2013-03-08	1	\N
00000024	02	00000002	\N	F	\N	2013-03-09	1	\N
00000024	02	00000002	\N	D	\N	2013-03-10	1	\N
00000024	02	00000002	\N	D	\N	2013-03-11	1	\N
00000024	02	00000002	\N	D	\N	2013-03-12	1	\N
00000024	02	00000002	\N	D	\N	2013-03-13	1	\N
00000024	02	00000002	\N	D	\N	2013-03-14	1	\N
00000024	02	00000002	\N	F	\N	2013-03-15	1	\N
00000024	02	00000002	\N	F	\N	2013-03-16	1	\N
00000024	02	00000002	\N	D	\N	2013-03-17	1	\N
00000024	02	00000002	\N	D	\N	2013-03-18	1	\N
00000024	02	00000002	\N	D	\N	2013-03-19	1	\N
00000024	02	00000002	\N	D	\N	2013-03-20	1	\N
00000024	02	00000002	\N	D	\N	2013-03-21	1	\N
00000024	02	00000002	\N	F	\N	2013-03-22	1	\N
00000024	02	00000002	\N	F	\N	2013-03-23	1	\N
00000024	02	00000002	\N	D	\N	2013-03-24	1	\N
00000024	02	00000002	\N	D	\N	2013-03-25	1	\N
00000024	02	00000002	\N	D	\N	2013-03-26	1	\N
00000024	02	00000002	\N	D	\N	2013-03-27	1	\N
00000024	02	00000002	\N	D	\N	2013-03-28	1	\N
00000024	02	00000002	\N	F	\N	2013-03-29	1	\N
00000024	02	00000002	\N	F	\N	2013-03-30	1	\N
00000024	02	00000002	\N	D	\N	2013-03-31	1	\N
00000025	02	00000002	\N	F	\N	2013-03-01	1	\N
00000025	02	00000002	\N	F	\N	2013-03-02	1	\N
00000025	02	00000002	\N	D	\N	2013-03-03	1	\N
00000025	02	00000002	\N	D	\N	2013-03-04	1	\N
00000025	02	00000002	\N	D	\N	2013-03-05	1	\N
00000025	02	00000002	\N	D	\N	2013-03-06	1	\N
00000025	02	00000002	\N	D	\N	2013-03-07	1	\N
00000025	02	00000002	\N	F	\N	2013-03-08	1	\N
00000025	02	00000002	\N	F	\N	2013-03-09	1	\N
00000025	02	00000002	\N	D	\N	2013-03-10	1	\N
00000025	02	00000002	\N	D	\N	2013-03-11	1	\N
00000025	02	00000002	\N	D	\N	2013-03-12	1	\N
00000025	02	00000002	\N	D	\N	2013-03-13	1	\N
00000025	02	00000002	\N	D	\N	2013-03-14	1	\N
00000025	02	00000002	\N	F	\N	2013-03-15	1	\N
00000025	02	00000002	\N	F	\N	2013-03-16	1	\N
00000025	02	00000002	\N	D	\N	2013-03-17	1	\N
00000025	02	00000002	\N	D	\N	2013-03-18	1	\N
00000025	02	00000002	\N	D	\N	2013-03-19	1	\N
00000025	02	00000002	\N	D	\N	2013-03-20	1	\N
00000025	02	00000002	\N	D	\N	2013-03-21	1	\N
00000025	02	00000002	\N	F	\N	2013-03-22	1	\N
00000025	02	00000002	\N	F	\N	2013-03-23	1	\N
00000025	02	00000002	\N	D	\N	2013-03-24	1	\N
00000025	02	00000002	\N	D	\N	2013-03-25	1	\N
00000025	02	00000002	\N	D	\N	2013-03-26	1	\N
00000025	02	00000002	\N	D	\N	2013-03-27	1	\N
00000025	02	00000002	\N	D	\N	2013-03-28	1	\N
00000025	02	00000002	\N	F	\N	2013-03-29	1	\N
00000025	02	00000002	\N	F	\N	2013-03-30	1	\N
00000025	02	00000002	\N	D	\N	2013-03-31	1	\N
00000026	02	00000002	\N	F	\N	2013-03-01	1	\N
00000026	02	00000002	\N	F	\N	2013-03-02	1	\N
00000026	02	00000002	\N	D	\N	2013-03-03	1	\N
00000026	02	00000002	\N	D	\N	2013-03-04	1	\N
00000026	02	00000002	\N	D	\N	2013-03-05	1	\N
00000026	02	00000002	\N	D	\N	2013-03-06	1	\N
00000026	02	00000002	\N	D	\N	2013-03-07	1	\N
00000026	02	00000002	\N	F	\N	2013-03-08	1	\N
00000026	02	00000002	\N	F	\N	2013-03-09	1	\N
00000026	02	00000002	\N	D	\N	2013-03-10	1	\N
00000026	02	00000002	\N	D	\N	2013-03-11	1	\N
00000026	02	00000002	\N	D	\N	2013-03-12	1	\N
00000026	02	00000002	\N	D	\N	2013-03-13	1	\N
00000026	02	00000002	\N	D	\N	2013-03-14	1	\N
00000026	02	00000002	\N	F	\N	2013-03-15	1	\N
00000026	02	00000002	\N	F	\N	2013-03-16	1	\N
00000026	02	00000002	\N	D	\N	2013-03-17	1	\N
00000026	02	00000002	\N	D	\N	2013-03-18	1	\N
00000026	02	00000002	\N	D	\N	2013-03-19	1	\N
00000026	02	00000002	\N	D	\N	2013-03-20	1	\N
00000026	02	00000002	\N	D	\N	2013-03-21	1	\N
00000026	02	00000002	\N	F	\N	2013-03-22	1	\N
00000026	02	00000002	\N	F	\N	2013-03-23	1	\N
00000026	02	00000002	\N	D	\N	2013-03-24	1	\N
00000026	02	00000002	\N	D	\N	2013-03-25	1	\N
00000026	02	00000002	\N	D	\N	2013-03-26	1	\N
00000026	02	00000002	\N	D	\N	2013-03-27	1	\N
00000026	02	00000002	\N	D	\N	2013-03-28	1	\N
00000026	02	00000002	\N	F	\N	2013-03-29	1	\N
00000026	02	00000002	\N	F	\N	2013-03-30	1	\N
00000026	02	00000002	\N	D	\N	2013-03-31	1	\N
00000027	02	00000002	\N	F	\N	2013-03-01	1	\N
00000027	02	00000002	\N	F	\N	2013-03-02	1	\N
00000027	02	00000002	\N	D	\N	2013-03-03	1	\N
00000027	02	00000002	\N	D	\N	2013-03-04	1	\N
00000027	02	00000002	\N	D	\N	2013-03-05	1	\N
00000027	02	00000002	\N	D	\N	2013-03-06	1	\N
00000027	02	00000002	\N	D	\N	2013-03-07	1	\N
00000027	02	00000002	\N	F	\N	2013-03-08	1	\N
00000027	02	00000002	\N	F	\N	2013-03-09	1	\N
00000027	02	00000002	\N	D	\N	2013-03-10	1	\N
00000027	02	00000002	\N	D	\N	2013-03-11	1	\N
00000027	02	00000002	\N	D	\N	2013-03-12	1	\N
00000027	02	00000002	\N	D	\N	2013-03-13	1	\N
00000027	02	00000002	\N	D	\N	2013-03-14	1	\N
00000027	02	00000002	\N	F	\N	2013-03-15	1	\N
00000027	02	00000002	\N	F	\N	2013-03-16	1	\N
00000027	02	00000002	\N	D	\N	2013-03-17	1	\N
00000027	02	00000002	\N	D	\N	2013-03-18	1	\N
00000027	02	00000002	\N	D	\N	2013-03-19	1	\N
00000027	02	00000002	\N	D	\N	2013-03-20	1	\N
00000027	02	00000002	\N	D	\N	2013-03-21	1	\N
00000027	02	00000002	\N	F	\N	2013-03-22	1	\N
00000027	02	00000002	\N	F	\N	2013-03-23	1	\N
00000027	02	00000002	\N	D	\N	2013-03-24	1	\N
00000027	02	00000002	\N	D	\N	2013-03-25	1	\N
00000027	02	00000002	\N	D	\N	2013-03-26	1	\N
00000027	02	00000002	\N	D	\N	2013-03-27	1	\N
00000027	02	00000002	\N	D	\N	2013-03-28	1	\N
00000027	02	00000002	\N	F	\N	2013-03-29	1	\N
00000027	02	00000002	\N	F	\N	2013-03-30	1	\N
00000027	02	00000002	\N	D	\N	2013-03-31	1	\N
00000028	02	00000002	\N	F	\N	2013-03-01	1	\N
00000028	02	00000002	\N	F	\N	2013-03-02	1	\N
00000028	02	00000002	\N	D	\N	2013-03-03	1	\N
00000028	02	00000002	\N	D	\N	2013-03-04	1	\N
00000028	02	00000002	\N	D	\N	2013-03-05	1	\N
00000028	02	00000002	\N	D	\N	2013-03-06	1	\N
00000028	02	00000002	\N	D	\N	2013-03-07	1	\N
00000028	02	00000002	\N	F	\N	2013-03-08	1	\N
00000028	02	00000002	\N	F	\N	2013-03-09	1	\N
00000028	02	00000002	\N	D	\N	2013-03-10	1	\N
00000028	02	00000002	\N	D	\N	2013-03-11	1	\N
00000028	02	00000002	\N	D	\N	2013-03-12	1	\N
00000028	02	00000002	\N	D	\N	2013-03-13	1	\N
00000028	02	00000002	\N	D	\N	2013-03-14	1	\N
00000028	02	00000002	\N	F	\N	2013-03-15	1	\N
00000028	02	00000002	\N	F	\N	2013-03-16	1	\N
00000028	02	00000002	\N	D	\N	2013-03-17	1	\N
00000028	02	00000002	\N	D	\N	2013-03-18	1	\N
00000028	02	00000002	\N	D	\N	2013-03-19	1	\N
00000028	02	00000002	\N	D	\N	2013-03-20	1	\N
00000028	02	00000002	\N	D	\N	2013-03-21	1	\N
00000028	02	00000002	\N	F	\N	2013-03-22	1	\N
00000028	02	00000002	\N	F	\N	2013-03-23	1	\N
00000028	02	00000002	\N	D	\N	2013-03-24	1	\N
00000028	02	00000002	\N	D	\N	2013-03-25	1	\N
00000028	02	00000002	\N	D	\N	2013-03-26	1	\N
00000028	02	00000002	\N	D	\N	2013-03-27	1	\N
00000028	02	00000002	\N	D	\N	2013-03-28	1	\N
00000028	02	00000002	\N	F	\N	2013-03-29	1	\N
00000028	02	00000002	\N	F	\N	2013-03-30	1	\N
00000028	02	00000002	\N	D	\N	2013-03-31	1	\N
00000029	02	00000002	\N	F	\N	2013-03-01	1	\N
00000029	02	00000002	\N	F	\N	2013-03-02	1	\N
00000029	02	00000002	\N	D	\N	2013-03-03	1	\N
00000029	02	00000002	\N	D	\N	2013-03-04	1	\N
00000029	02	00000002	\N	D	\N	2013-03-05	1	\N
00000029	02	00000002	\N	D	\N	2013-03-06	1	\N
00000029	02	00000002	\N	D	\N	2013-03-07	1	\N
00000029	02	00000002	\N	F	\N	2013-03-08	1	\N
00000029	02	00000002	\N	F	\N	2013-03-09	1	\N
00000029	02	00000002	\N	D	\N	2013-03-10	1	\N
00000029	02	00000002	\N	D	\N	2013-03-11	1	\N
00000029	02	00000002	\N	D	\N	2013-03-12	1	\N
00000029	02	00000002	\N	D	\N	2013-03-13	1	\N
00000029	02	00000002	\N	D	\N	2013-03-14	1	\N
00000029	02	00000002	\N	F	\N	2013-03-15	1	\N
00000029	02	00000002	\N	F	\N	2013-03-16	1	\N
00000029	02	00000002	\N	D	\N	2013-03-17	1	\N
00000029	02	00000002	\N	D	\N	2013-03-18	1	\N
00000029	02	00000002	\N	D	\N	2013-03-19	1	\N
00000029	02	00000002	\N	D	\N	2013-03-20	1	\N
00000029	02	00000002	\N	D	\N	2013-03-21	1	\N
00000029	02	00000002	\N	F	\N	2013-03-22	1	\N
00000029	02	00000002	\N	F	\N	2013-03-23	1	\N
00000029	02	00000002	\N	D	\N	2013-03-24	1	\N
00000029	02	00000002	\N	D	\N	2013-03-25	1	\N
00000029	02	00000002	\N	D	\N	2013-03-26	1	\N
00000029	02	00000002	\N	D	\N	2013-03-27	1	\N
00000029	02	00000002	\N	D	\N	2013-03-28	1	\N
00000029	02	00000002	\N	F	\N	2013-03-29	1	\N
00000029	02	00000002	\N	F	\N	2013-03-30	1	\N
00000029	02	00000002	\N	D	\N	2013-03-31	1	\N
00000030	02	00000002	\N	F	\N	2013-03-01	1	\N
00000030	02	00000002	\N	F	\N	2013-03-02	1	\N
00000030	02	00000002	\N	D	\N	2013-03-03	1	\N
00000030	02	00000002	\N	D	\N	2013-03-04	1	\N
00000030	02	00000002	\N	D	\N	2013-03-05	1	\N
00000030	02	00000002	\N	D	\N	2013-03-06	1	\N
00000030	02	00000002	\N	D	\N	2013-03-07	1	\N
00000030	02	00000002	\N	F	\N	2013-03-08	1	\N
00000030	02	00000002	\N	F	\N	2013-03-09	1	\N
00000030	02	00000002	\N	D	\N	2013-03-10	1	\N
00000030	02	00000002	\N	D	\N	2013-03-11	1	\N
00000030	02	00000002	\N	D	\N	2013-03-12	1	\N
00000030	02	00000002	\N	D	\N	2013-03-13	1	\N
00000030	02	00000002	\N	D	\N	2013-03-14	1	\N
00000030	02	00000002	\N	F	\N	2013-03-15	1	\N
00000030	02	00000002	\N	F	\N	2013-03-16	1	\N
00000030	02	00000002	\N	D	\N	2013-03-17	1	\N
00000030	02	00000002	\N	D	\N	2013-03-18	1	\N
00000030	02	00000002	\N	D	\N	2013-03-19	1	\N
00000030	02	00000002	\N	D	\N	2013-03-20	1	\N
00000030	02	00000002	\N	D	\N	2013-03-21	1	\N
00000030	02	00000002	\N	F	\N	2013-03-22	1	\N
00000030	02	00000002	\N	F	\N	2013-03-23	1	\N
00000030	02	00000002	\N	D	\N	2013-03-24	1	\N
00000030	02	00000002	\N	D	\N	2013-03-25	1	\N
00000030	02	00000002	\N	D	\N	2013-03-26	1	\N
00000030	02	00000002	\N	D	\N	2013-03-27	1	\N
00000030	02	00000002	\N	D	\N	2013-03-28	1	\N
00000030	02	00000002	\N	F	\N	2013-03-29	1	\N
00000030	02	00000002	\N	F	\N	2013-03-30	1	\N
00000030	02	00000002	\N	D	\N	2013-03-31	1	\N
00000031	02	00000002	\N	F	\N	2013-03-01	1	\N
00000031	02	00000002	\N	F	\N	2013-03-02	1	\N
00000031	02	00000002	\N	D	\N	2013-03-03	1	\N
00000031	02	00000002	\N	D	\N	2013-03-04	1	\N
00000031	02	00000002	\N	D	\N	2013-03-05	1	\N
00000031	02	00000002	\N	D	\N	2013-03-06	1	\N
00000031	02	00000002	\N	D	\N	2013-03-07	1	\N
00000031	02	00000002	\N	F	\N	2013-03-08	1	\N
00000031	02	00000002	\N	F	\N	2013-03-09	1	\N
00000031	02	00000002	\N	D	\N	2013-03-10	1	\N
00000031	02	00000002	\N	D	\N	2013-03-11	1	\N
00000031	02	00000002	\N	D	\N	2013-03-12	1	\N
00000031	02	00000002	\N	D	\N	2013-03-13	1	\N
00000031	02	00000002	\N	D	\N	2013-03-14	1	\N
00000031	02	00000002	\N	F	\N	2013-03-15	1	\N
00000031	02	00000002	\N	F	\N	2013-03-16	1	\N
00000031	02	00000002	\N	D	\N	2013-03-17	1	\N
00000031	02	00000002	\N	D	\N	2013-03-18	1	\N
00000031	02	00000002	\N	D	\N	2013-03-19	1	\N
00000031	02	00000002	\N	D	\N	2013-03-20	1	\N
00000031	02	00000002	\N	D	\N	2013-03-21	1	\N
00000031	02	00000002	\N	F	\N	2013-03-22	1	\N
00000031	02	00000002	\N	F	\N	2013-03-23	1	\N
00000031	02	00000002	\N	D	\N	2013-03-24	1	\N
00000031	02	00000002	\N	D	\N	2013-03-25	1	\N
00000031	02	00000002	\N	D	\N	2013-03-26	1	\N
00000031	02	00000002	\N	D	\N	2013-03-27	1	\N
00000031	02	00000002	\N	D	\N	2013-03-28	1	\N
00000031	02	00000002	\N	F	\N	2013-03-29	1	\N
00000031	02	00000002	\N	F	\N	2013-03-30	1	\N
00000031	02	00000002	\N	D	\N	2013-03-31	1	\N
00000032	02	00000002	\N	F	\N	2013-03-01	1	\N
00000032	02	00000002	\N	F	\N	2013-03-02	1	\N
00000032	02	00000002	\N	D	\N	2013-03-03	1	\N
00000032	02	00000002	\N	D	\N	2013-03-04	1	\N
00000032	02	00000002	\N	D	\N	2013-03-05	1	\N
00000032	02	00000002	\N	D	\N	2013-03-06	1	\N
00000032	02	00000002	\N	D	\N	2013-03-07	1	\N
00000032	02	00000002	\N	F	\N	2013-03-08	1	\N
00000032	02	00000002	\N	F	\N	2013-03-09	1	\N
00000032	02	00000002	\N	D	\N	2013-03-10	1	\N
00000032	02	00000002	\N	D	\N	2013-03-11	1	\N
00000032	02	00000002	\N	D	\N	2013-03-12	1	\N
00000032	02	00000002	\N	D	\N	2013-03-13	1	\N
00000032	02	00000002	\N	D	\N	2013-03-14	1	\N
00000032	02	00000002	\N	F	\N	2013-03-15	1	\N
00000032	02	00000002	\N	F	\N	2013-03-16	1	\N
00000032	02	00000002	\N	D	\N	2013-03-17	1	\N
00000032	02	00000002	\N	D	\N	2013-03-18	1	\N
00000032	02	00000002	\N	D	\N	2013-03-19	1	\N
00000032	02	00000002	\N	D	\N	2013-03-20	1	\N
00000032	02	00000002	\N	D	\N	2013-03-21	1	\N
00000032	02	00000002	\N	F	\N	2013-03-22	1	\N
00000032	02	00000002	\N	F	\N	2013-03-23	1	\N
00000032	02	00000002	\N	D	\N	2013-03-24	1	\N
00000032	02	00000002	\N	D	\N	2013-03-25	1	\N
00000032	02	00000002	\N	D	\N	2013-03-26	1	\N
00000032	02	00000002	\N	D	\N	2013-03-27	1	\N
00000032	02	00000002	\N	D	\N	2013-03-28	1	\N
00000032	02	00000002	\N	F	\N	2013-03-29	1	\N
00000032	02	00000002	\N	F	\N	2013-03-30	1	\N
00000032	02	00000002	\N	D	\N	2013-03-31	1	\N
00000033	02	00000002	\N	F	\N	2013-03-01	1	\N
00000033	02	00000002	\N	F	\N	2013-03-02	1	\N
00000033	02	00000002	\N	D	\N	2013-03-03	1	\N
00000033	02	00000002	\N	D	\N	2013-03-04	1	\N
00000033	02	00000002	\N	D	\N	2013-03-05	1	\N
00000033	02	00000002	\N	D	\N	2013-03-06	1	\N
00000033	02	00000002	\N	D	\N	2013-03-07	1	\N
00000033	02	00000002	\N	F	\N	2013-03-08	1	\N
00000033	02	00000002	\N	F	\N	2013-03-09	1	\N
00000033	02	00000002	\N	D	\N	2013-03-10	1	\N
00000033	02	00000002	\N	D	\N	2013-03-11	1	\N
00000033	02	00000002	\N	D	\N	2013-03-12	1	\N
00000033	02	00000002	\N	D	\N	2013-03-13	1	\N
00000033	02	00000002	\N	D	\N	2013-03-14	1	\N
00000033	02	00000002	\N	F	\N	2013-03-15	1	\N
00000033	02	00000002	\N	F	\N	2013-03-16	1	\N
00000033	02	00000002	\N	D	\N	2013-03-17	1	\N
00000033	02	00000002	\N	D	\N	2013-03-18	1	\N
00000033	02	00000002	\N	D	\N	2013-03-19	1	\N
00000033	02	00000002	\N	D	\N	2013-03-20	1	\N
00000033	02	00000002	\N	D	\N	2013-03-21	1	\N
00000033	02	00000002	\N	F	\N	2013-03-22	1	\N
00000033	02	00000002	\N	F	\N	2013-03-23	1	\N
00000033	02	00000002	\N	D	\N	2013-03-24	1	\N
00000033	02	00000002	\N	D	\N	2013-03-25	1	\N
00000033	02	00000002	\N	D	\N	2013-03-26	1	\N
00000033	02	00000002	\N	D	\N	2013-03-27	1	\N
00000033	02	00000002	\N	D	\N	2013-03-28	1	\N
00000033	02	00000002	\N	F	\N	2013-03-29	1	\N
00000033	02	00000002	\N	F	\N	2013-03-30	1	\N
00000033	02	00000002	\N	D	\N	2013-03-31	1	\N
00000034	02	00000002	\N	F	\N	2013-03-01	1	\N
00000034	02	00000002	\N	F	\N	2013-03-02	1	\N
00000034	02	00000002	\N	D	\N	2013-03-03	1	\N
00000034	02	00000002	\N	D	\N	2013-03-04	1	\N
00000034	02	00000002	\N	D	\N	2013-03-05	1	\N
00000034	02	00000002	\N	D	\N	2013-03-06	1	\N
00000034	02	00000002	\N	D	\N	2013-03-07	1	\N
00000034	02	00000002	\N	F	\N	2013-03-08	1	\N
00000034	02	00000002	\N	F	\N	2013-03-09	1	\N
00000034	02	00000002	\N	D	\N	2013-03-10	1	\N
00000034	02	00000002	\N	D	\N	2013-03-11	1	\N
00000034	02	00000002	\N	D	\N	2013-03-12	1	\N
00000034	02	00000002	\N	D	\N	2013-03-13	1	\N
00000034	02	00000002	\N	D	\N	2013-03-14	1	\N
00000034	02	00000002	\N	F	\N	2013-03-15	1	\N
00000034	02	00000002	\N	F	\N	2013-03-16	1	\N
00000034	02	00000002	\N	D	\N	2013-03-17	1	\N
00000034	02	00000002	\N	D	\N	2013-03-18	1	\N
00000034	02	00000002	\N	D	\N	2013-03-19	1	\N
00000034	02	00000002	\N	D	\N	2013-03-20	1	\N
00000034	02	00000002	\N	D	\N	2013-03-21	1	\N
00000034	02	00000002	\N	F	\N	2013-03-22	1	\N
00000034	02	00000002	\N	F	\N	2013-03-23	1	\N
00000034	02	00000002	\N	D	\N	2013-03-24	1	\N
00000034	02	00000002	\N	D	\N	2013-03-25	1	\N
00000034	02	00000002	\N	D	\N	2013-03-26	1	\N
00000034	02	00000002	\N	D	\N	2013-03-27	1	\N
00000034	02	00000002	\N	D	\N	2013-03-28	1	\N
00000034	02	00000002	\N	F	\N	2013-03-29	1	\N
00000034	02	00000002	\N	F	\N	2013-03-30	1	\N
00000034	02	00000002	\N	D	\N	2013-03-31	1	\N
00000035	02	00000002	\N	F	\N	2013-03-01	1	\N
00000035	02	00000002	\N	F	\N	2013-03-02	1	\N
00000035	02	00000002	\N	D	\N	2013-03-03	1	\N
00000035	02	00000002	\N	D	\N	2013-03-04	1	\N
00000035	02	00000002	\N	D	\N	2013-03-05	1	\N
00000035	02	00000002	\N	D	\N	2013-03-06	1	\N
00000035	02	00000002	\N	D	\N	2013-03-07	1	\N
00000035	02	00000002	\N	F	\N	2013-03-08	1	\N
00000035	02	00000002	\N	F	\N	2013-03-09	1	\N
00000035	02	00000002	\N	D	\N	2013-03-10	1	\N
00000035	02	00000002	\N	D	\N	2013-03-11	1	\N
00000035	02	00000002	\N	D	\N	2013-03-12	1	\N
00000035	02	00000002	\N	D	\N	2013-03-13	1	\N
00000035	02	00000002	\N	D	\N	2013-03-14	1	\N
00000035	02	00000002	\N	F	\N	2013-03-15	1	\N
00000035	02	00000002	\N	F	\N	2013-03-16	1	\N
00000035	02	00000002	\N	D	\N	2013-03-17	1	\N
00000035	02	00000002	\N	D	\N	2013-03-18	1	\N
00000035	02	00000002	\N	D	\N	2013-03-19	1	\N
00000035	02	00000002	\N	D	\N	2013-03-20	1	\N
00000035	02	00000002	\N	D	\N	2013-03-21	1	\N
00000035	02	00000002	\N	F	\N	2013-03-22	1	\N
00000035	02	00000002	\N	F	\N	2013-03-23	1	\N
00000035	02	00000002	\N	D	\N	2013-03-24	1	\N
00000035	02	00000002	\N	D	\N	2013-03-25	1	\N
00000035	02	00000002	\N	D	\N	2013-03-26	1	\N
00000035	02	00000002	\N	D	\N	2013-03-27	1	\N
00000035	02	00000002	\N	D	\N	2013-03-28	1	\N
00000035	02	00000002	\N	F	\N	2013-03-29	1	\N
00000035	02	00000002	\N	F	\N	2013-03-30	1	\N
00000035	02	00000002	\N	D	\N	2013-03-31	1	\N
00000036	02	00000002	\N	F	\N	2013-03-01	1	\N
00000036	02	00000002	\N	F	\N	2013-03-02	1	\N
00000036	02	00000002	\N	D	\N	2013-03-03	1	\N
00000036	02	00000002	\N	D	\N	2013-03-04	1	\N
00000036	02	00000002	\N	D	\N	2013-03-05	1	\N
00000036	02	00000002	\N	D	\N	2013-03-06	1	\N
00000036	02	00000002	\N	D	\N	2013-03-07	1	\N
00000036	02	00000002	\N	F	\N	2013-03-08	1	\N
00000036	02	00000002	\N	F	\N	2013-03-09	1	\N
00000036	02	00000002	\N	D	\N	2013-03-10	1	\N
00000036	02	00000002	\N	D	\N	2013-03-11	1	\N
00000036	02	00000002	\N	D	\N	2013-03-12	1	\N
00000036	02	00000002	\N	D	\N	2013-03-13	1	\N
00000036	02	00000002	\N	D	\N	2013-03-14	1	\N
00000036	02	00000002	\N	F	\N	2013-03-15	1	\N
00000036	02	00000002	\N	F	\N	2013-03-16	1	\N
00000036	02	00000002	\N	D	\N	2013-03-17	1	\N
00000036	02	00000002	\N	D	\N	2013-03-18	1	\N
00000036	02	00000002	\N	D	\N	2013-03-19	1	\N
00000036	02	00000002	\N	D	\N	2013-03-20	1	\N
00000036	02	00000002	\N	D	\N	2013-03-21	1	\N
00000036	02	00000002	\N	F	\N	2013-03-22	1	\N
00000036	02	00000002	\N	F	\N	2013-03-23	1	\N
00000036	02	00000002	\N	D	\N	2013-03-24	1	\N
00000036	02	00000002	\N	D	\N	2013-03-25	1	\N
00000036	02	00000002	\N	D	\N	2013-03-26	1	\N
00000036	02	00000002	\N	D	\N	2013-03-27	1	\N
00000036	02	00000002	\N	D	\N	2013-03-28	1	\N
00000036	02	00000002	\N	F	\N	2013-03-29	1	\N
00000036	02	00000002	\N	F	\N	2013-03-30	1	\N
00000036	02	00000002	\N	D	\N	2013-03-31	1	\N
00000037	02	00000002	\N	F	\N	2013-03-01	1	\N
00000037	02	00000002	\N	F	\N	2013-03-02	1	\N
00000037	02	00000002	\N	D	\N	2013-03-03	1	\N
00000037	02	00000002	\N	D	\N	2013-03-04	1	\N
00000037	02	00000002	\N	D	\N	2013-03-05	1	\N
00000037	02	00000002	\N	D	\N	2013-03-06	1	\N
00000037	02	00000002	\N	D	\N	2013-03-07	1	\N
00000037	02	00000002	\N	F	\N	2013-03-08	1	\N
00000037	02	00000002	\N	F	\N	2013-03-09	1	\N
00000037	02	00000002	\N	D	\N	2013-03-10	1	\N
00000037	02	00000002	\N	D	\N	2013-03-11	1	\N
00000037	02	00000002	\N	D	\N	2013-03-12	1	\N
00000037	02	00000002	\N	D	\N	2013-03-13	1	\N
00000037	02	00000002	\N	D	\N	2013-03-14	1	\N
00000037	02	00000002	\N	F	\N	2013-03-15	1	\N
00000037	02	00000002	\N	F	\N	2013-03-16	1	\N
00000037	02	00000002	\N	D	\N	2013-03-17	1	\N
00000037	02	00000002	\N	D	\N	2013-03-18	1	\N
00000037	02	00000002	\N	D	\N	2013-03-19	1	\N
00000037	02	00000002	\N	D	\N	2013-03-20	1	\N
00000037	02	00000002	\N	D	\N	2013-03-21	1	\N
00000037	02	00000002	\N	F	\N	2013-03-22	1	\N
00000037	02	00000002	\N	F	\N	2013-03-23	1	\N
00000037	02	00000002	\N	D	\N	2013-03-24	1	\N
00000037	02	00000002	\N	D	\N	2013-03-25	1	\N
00000037	02	00000002	\N	D	\N	2013-03-26	1	\N
00000037	02	00000002	\N	D	\N	2013-03-27	1	\N
00000037	02	00000002	\N	D	\N	2013-03-28	1	\N
00000037	02	00000002	\N	F	\N	2013-03-29	1	\N
00000037	02	00000002	\N	F	\N	2013-03-30	1	\N
00000037	02	00000002	\N	D	\N	2013-03-31	1	\N
00000038	02	00000002	\N	F	\N	2013-03-01	1	\N
00000038	02	00000002	\N	F	\N	2013-03-02	1	\N
00000038	02	00000002	\N	D	\N	2013-03-03	1	\N
00000038	02	00000002	\N	D	\N	2013-03-04	1	\N
00000038	02	00000002	\N	D	\N	2013-03-05	1	\N
00000038	02	00000002	\N	D	\N	2013-03-06	1	\N
00000038	02	00000002	\N	D	\N	2013-03-07	1	\N
00000038	02	00000002	\N	F	\N	2013-03-08	1	\N
00000038	02	00000002	\N	F	\N	2013-03-09	1	\N
00000038	02	00000002	\N	D	\N	2013-03-10	1	\N
00000038	02	00000002	\N	D	\N	2013-03-11	1	\N
00000038	02	00000002	\N	D	\N	2013-03-12	1	\N
00000038	02	00000002	\N	D	\N	2013-03-13	1	\N
00000038	02	00000002	\N	D	\N	2013-03-14	1	\N
00000038	02	00000002	\N	F	\N	2013-03-15	1	\N
00000038	02	00000002	\N	F	\N	2013-03-16	1	\N
00000038	02	00000002	\N	D	\N	2013-03-17	1	\N
00000038	02	00000002	\N	D	\N	2013-03-18	1	\N
00000038	02	00000002	\N	D	\N	2013-03-19	1	\N
00000038	02	00000002	\N	D	\N	2013-03-20	1	\N
00000038	02	00000002	\N	D	\N	2013-03-21	1	\N
00000038	02	00000002	\N	F	\N	2013-03-22	1	\N
00000038	02	00000002	\N	F	\N	2013-03-23	1	\N
00000038	02	00000002	\N	D	\N	2013-03-24	1	\N
00000038	02	00000002	\N	D	\N	2013-03-25	1	\N
00000038	02	00000002	\N	D	\N	2013-03-26	1	\N
00000038	02	00000002	\N	D	\N	2013-03-27	1	\N
00000038	02	00000002	\N	D	\N	2013-03-28	1	\N
00000038	02	00000002	\N	F	\N	2013-03-29	1	\N
00000038	02	00000002	\N	F	\N	2013-03-30	1	\N
00000038	02	00000002	\N	D	\N	2013-03-31	1	\N
00000039	02	00000002	\N	F	\N	2013-03-01	1	\N
00000039	02	00000002	\N	F	\N	2013-03-02	1	\N
00000039	02	00000002	\N	D	\N	2013-03-03	1	\N
00000039	02	00000002	\N	D	\N	2013-03-04	1	\N
00000039	02	00000002	\N	D	\N	2013-03-05	1	\N
00000039	02	00000002	\N	D	\N	2013-03-06	1	\N
00000039	02	00000002	\N	D	\N	2013-03-07	1	\N
00000039	02	00000002	\N	F	\N	2013-03-08	1	\N
00000039	02	00000002	\N	F	\N	2013-03-09	1	\N
00000039	02	00000002	\N	D	\N	2013-03-10	1	\N
00000039	02	00000002	\N	D	\N	2013-03-11	1	\N
00000039	02	00000002	\N	D	\N	2013-03-12	1	\N
00000039	02	00000002	\N	D	\N	2013-03-13	1	\N
00000039	02	00000002	\N	D	\N	2013-03-14	1	\N
00000039	02	00000002	\N	F	\N	2013-03-15	1	\N
00000039	02	00000002	\N	F	\N	2013-03-16	1	\N
00000039	02	00000002	\N	D	\N	2013-03-17	1	\N
00000039	02	00000002	\N	D	\N	2013-03-18	1	\N
00000039	02	00000002	\N	D	\N	2013-03-19	1	\N
00000039	02	00000002	\N	D	\N	2013-03-20	1	\N
00000039	02	00000002	\N	D	\N	2013-03-21	1	\N
00000039	02	00000002	\N	F	\N	2013-03-22	1	\N
00000039	02	00000002	\N	F	\N	2013-03-23	1	\N
00000039	02	00000002	\N	D	\N	2013-03-24	1	\N
00000039	02	00000002	\N	D	\N	2013-03-25	1	\N
00000039	02	00000002	\N	D	\N	2013-03-26	1	\N
00000039	02	00000002	\N	D	\N	2013-03-27	1	\N
00000039	02	00000002	\N	D	\N	2013-03-28	1	\N
00000039	02	00000002	\N	F	\N	2013-03-29	1	\N
00000039	02	00000002	\N	F	\N	2013-03-30	1	\N
00000039	02	00000002	\N	D	\N	2013-03-31	1	\N
00000040	02	00000002	\N	F	\N	2013-03-01	1	\N
00000040	02	00000002	\N	F	\N	2013-03-02	1	\N
00000040	02	00000002	\N	D	\N	2013-03-03	1	\N
00000040	02	00000002	\N	D	\N	2013-03-04	1	\N
00000040	02	00000002	\N	D	\N	2013-03-05	1	\N
00000040	02	00000002	\N	D	\N	2013-03-06	1	\N
00000040	02	00000002	\N	D	\N	2013-03-07	1	\N
00000040	02	00000002	\N	F	\N	2013-03-08	1	\N
00000040	02	00000002	\N	F	\N	2013-03-09	1	\N
00000040	02	00000002	\N	D	\N	2013-03-10	1	\N
00000040	02	00000002	\N	D	\N	2013-03-11	1	\N
00000040	02	00000002	\N	D	\N	2013-03-12	1	\N
00000040	02	00000002	\N	D	\N	2013-03-13	1	\N
00000040	02	00000002	\N	D	\N	2013-03-14	1	\N
00000040	02	00000002	\N	F	\N	2013-03-15	1	\N
00000040	02	00000002	\N	F	\N	2013-03-16	1	\N
00000040	02	00000002	\N	D	\N	2013-03-17	1	\N
00000040	02	00000002	\N	D	\N	2013-03-18	1	\N
00000040	02	00000002	\N	D	\N	2013-03-19	1	\N
00000040	02	00000002	\N	D	\N	2013-03-20	1	\N
00000040	02	00000002	\N	D	\N	2013-03-21	1	\N
00000040	02	00000002	\N	F	\N	2013-03-22	1	\N
00000040	02	00000002	\N	F	\N	2013-03-23	1	\N
00000040	02	00000002	\N	D	\N	2013-03-24	1	\N
00000040	02	00000002	\N	D	\N	2013-03-25	1	\N
00000040	02	00000002	\N	D	\N	2013-03-26	1	\N
00000040	02	00000002	\N	D	\N	2013-03-27	1	\N
00000040	02	00000002	\N	D	\N	2013-03-28	1	\N
00000040	02	00000002	\N	F	\N	2013-03-29	1	\N
00000040	02	00000002	\N	F	\N	2013-03-30	1	\N
00000040	02	00000002	\N	D	\N	2013-03-31	1	\N
00000041	02	00000002	\N	F	\N	2013-03-01	1	\N
00000041	02	00000002	\N	F	\N	2013-03-02	1	\N
00000041	02	00000002	\N	D	\N	2013-03-03	1	\N
00000041	02	00000002	\N	D	\N	2013-03-04	1	\N
00000041	02	00000002	\N	D	\N	2013-03-05	1	\N
00000041	02	00000002	\N	D	\N	2013-03-06	1	\N
00000041	02	00000002	\N	D	\N	2013-03-07	1	\N
00000041	02	00000002	\N	F	\N	2013-03-08	1	\N
00000041	02	00000002	\N	F	\N	2013-03-09	1	\N
00000041	02	00000002	\N	D	\N	2013-03-10	1	\N
00000041	02	00000002	\N	D	\N	2013-03-11	1	\N
00000041	02	00000002	\N	D	\N	2013-03-12	1	\N
00000041	02	00000002	\N	D	\N	2013-03-13	1	\N
00000041	02	00000002	\N	D	\N	2013-03-14	1	\N
00000041	02	00000002	\N	F	\N	2013-03-15	1	\N
00000041	02	00000002	\N	F	\N	2013-03-16	1	\N
00000041	02	00000002	\N	D	\N	2013-03-17	1	\N
00000041	02	00000002	\N	D	\N	2013-03-18	1	\N
00000041	02	00000002	\N	D	\N	2013-03-19	1	\N
00000041	02	00000002	\N	D	\N	2013-03-20	1	\N
00000041	02	00000002	\N	D	\N	2013-03-21	1	\N
00000041	02	00000002	\N	F	\N	2013-03-22	1	\N
00000041	02	00000002	\N	F	\N	2013-03-23	1	\N
00000041	02	00000002	\N	D	\N	2013-03-24	1	\N
00000041	02	00000002	\N	D	\N	2013-03-25	1	\N
00000041	02	00000002	\N	D	\N	2013-03-26	1	\N
00000041	02	00000002	\N	D	\N	2013-03-27	1	\N
00000041	02	00000002	\N	D	\N	2013-03-28	1	\N
00000041	02	00000002	\N	F	\N	2013-03-29	1	\N
00000041	02	00000002	\N	F	\N	2013-03-30	1	\N
00000041	02	00000002	\N	D	\N	2013-03-31	1	\N
00000017	02	00000002	\N	D	\N	2012-11-15	1	\N
00000017	02	00000002	\N	D	\N	2012-11-16	1	\N
00000018	02	00000002	\N	D	\N	2012-11-15	1	\N
00000018	02	00000002	\N	D	\N	2012-11-16	1	\N
00000019	02	00000002	\N	D	\N	2012-11-15	1	\N
00000019	02	00000002	\N	D	\N	2012-11-16	1	\N
00000020	02	00000002	\N	D	\N	2012-11-15	1	\N
00000020	02	00000002	\N	D	\N	2012-11-16	1	\N
00000021	02	00000002	\N	D	\N	2012-11-15	1	\N
00000021	02	00000002	\N	D	\N	2012-11-16	1	\N
00000022	02	00000002	\N	D	\N	2012-11-15	1	\N
00000022	02	00000002	\N	D	\N	2012-11-16	1	\N
00000023	02	00000002	\N	D	\N	2012-11-15	1	\N
00000023	02	00000002	\N	D	\N	2012-11-16	1	\N
00000024	02	00000002	\N	D	\N	2012-11-15	1	\N
00000024	02	00000002	\N	D	\N	2012-11-16	1	\N
00000025	02	00000002	\N	D	\N	2012-11-15	1	\N
00000025	02	00000002	\N	D	\N	2012-11-16	1	\N
00000026	02	00000002	\N	D	\N	2012-11-16	1	\N
00000027	02	00000002	\N	D	\N	2012-11-15	1	\N
00000027	02	00000002	\N	D	\N	2012-11-16	1	\N
00000028	02	00000002	\N	D	\N	2012-11-15	1	\N
00000028	02	00000002	\N	D	\N	2012-11-16	1	\N
00000029	02	00000002	\N	D	\N	2012-11-15	1	\N
00000029	02	00000002	\N	D	\N	2012-11-16	1	\N
00000030	02	00000002	\N	D	\N	2012-11-15	1	\N
00000030	02	00000002	\N	D	\N	2012-11-16	1	\N
00000031	02	00000002	\N	D	\N	2012-11-15	1	\N
00000031	02	00000002	\N	D	\N	2012-11-16	1	\N
00000032	02	00000002	\N	D	\N	2012-11-15	1	\N
00000032	02	00000002	\N	D	\N	2012-11-16	1	\N
00000034	02	00000002	\N	D	\N	2012-11-15	1	\N
00000034	02	00000002	\N	D	\N	2012-11-16	1	\N
00000035	02	00000002	\N	D	\N	2012-11-15	1	\N
00000035	02	00000002	\N	D	\N	2012-11-16	1	\N
00000036	02	00000002	\N	D	\N	2012-11-15	1	\N
00000036	02	00000002	\N	D	\N	2012-11-16	1	\N
00000037	02	00000002	\N	D	\N	2012-11-15	1	\N
00000037	02	00000002	\N	D	\N	2012-11-16	1	\N
00000038	02	00000002	\N	D	\N	2012-11-15	1	\N
00000038	02	00000002	\N	D	\N	2012-11-16	1	\N
00000039	02	00000002	\N	D	\N	2012-11-15	1	\N
00000039	02	00000002	\N	D	\N	2012-11-16	1	\N
00000040	02	00000002	\N	D	\N	2012-11-15	1	\N
00000040	02	00000002	\N	D	\N	2012-11-16	1	\N
00000041	02	00000002	\N	D	\N	2012-11-15	1	\N
00000001	01	00000002	\N	F	\N	2012-11-02	1	\N
00000001	01	00000002	\N	F	\N	2012-11-03	1	\N
00000001	01	00000002	\N	D	\N	2012-11-04	1	\N
00000001	01	00000002	\N	D	\N	2012-11-05	1	\N
00000001	01	00000002	\N	D	\N	2012-11-08	1	\N
00000001	01	00000002	\N	F	\N	2012-11-09	1	\N
00000001	01	00000002	\N	F	\N	2012-11-10	1	\N
00000001	01	00000002	\N	D	\N	2012-11-11	1	\N
00000001	01	00000002	\N	D	\N	2012-11-12	1	\N
00000001	01	00000002	\N	D	\N	2012-11-13	1	\N
00000001	01	00000002	\N	D	\N	2012-11-14	1	\N
00000001	01	00000002	\N	D	\N	2012-11-15	1	\N
00000001	01	00000002	\N	F	\N	2012-11-16	1	\N
00000001	01	00000002	\N	F	\N	2012-11-17	1	\N
00000001	01	00000002	\N	D	\N	2012-11-18	1	\N
00000001	01	00000002	\N	D	\N	2012-11-19	1	\N
00000001	01	00000002	\N	D	\N	2012-11-20	1	\N
00000001	01	00000002	\N	D	\N	2012-11-21	1	\N
00000001	01	00000002	\N	D	\N	2012-11-22	1	\N
00000001	01	00000002	\N	F	\N	2012-11-23	1	\N
00000001	01	00000002	\N	F	\N	2012-11-24	1	\N
00000001	01	00000002	\N	D	\N	2012-11-25	1	\N
00000002	01	00000002	\N	D	\N	2012-11-01	1	\N
00000002	01	00000002	\N	F	\N	2012-11-02	1	\N
00000002	01	00000002	\N	F	\N	2012-11-03	1	\N
00000002	01	00000002	\N	D	\N	2012-11-04	1	\N
00000002	01	00000002	\N	D	\N	2012-11-05	1	\N
00000002	01	00000002	\N	D	\N	2012-11-08	1	\N
00000002	01	00000002	\N	F	\N	2012-11-09	1	\N
00000002	01	00000002	\N	F	\N	2012-11-10	1	\N
00000002	01	00000002	\N	D	\N	2012-11-11	1	\N
00000002	01	00000002	\N	D	\N	2012-11-12	1	\N
00000002	01	00000002	\N	D	\N	2012-11-13	1	\N
00000002	01	00000002	\N	D	\N	2012-11-14	1	\N
00000002	01	00000002	\N	D	\N	2012-11-15	1	\N
00000002	01	00000002	\N	F	\N	2012-11-16	1	\N
00000002	01	00000002	\N	F	\N	2012-11-17	1	\N
00000002	01	00000002	\N	D	\N	2012-11-18	1	\N
00000002	01	00000002	\N	D	\N	2012-11-19	1	\N
00000002	01	00000002	\N	D	\N	2012-11-20	1	\N
00000002	01	00000002	\N	D	\N	2012-11-21	1	\N
00000002	01	00000002	\N	D	\N	2012-11-22	1	\N
00000002	01	00000002	\N	F	\N	2012-11-23	1	\N
00000002	01	00000002	\N	F	\N	2012-11-24	1	\N
00000002	01	00000002	\N	D	\N	2012-11-25	1	\N
00000002	01	00000002	\N	D	\N	2012-11-26	1	\N
00000002	01	00000002	\N	D	\N	2012-11-27	1	\N
00000002	01	00000002	\N	D	\N	2012-11-28	1	\N
00000002	01	00000002	\N	D	\N	2012-11-29	1	\N
00000002	01	00000002	\N	F	\N	2012-11-30	1	\N
00000003	01	00000002	\N	D	\N	2012-11-01	1	\N
00000003	01	00000002	\N	F	\N	2012-11-02	1	\N
00000003	01	00000002	\N	F	\N	2012-11-03	1	\N
00000003	01	00000002	\N	D	\N	2012-11-04	1	\N
00000003	01	00000002	\N	D	\N	2012-11-05	1	\N
00000003	01	00000002	\N	D	\N	2012-11-08	1	\N
00000003	01	00000002	\N	F	\N	2012-11-09	1	\N
00000003	01	00000002	\N	F	\N	2012-11-10	1	\N
00000003	01	00000002	\N	D	\N	2012-11-11	1	\N
00000003	01	00000002	\N	D	\N	2012-11-12	1	\N
00000003	01	00000002	\N	D	\N	2012-11-13	1	\N
00000003	01	00000002	\N	D	\N	2012-11-14	1	\N
00000003	01	00000002	\N	D	\N	2012-11-15	1	\N
00000003	01	00000002	\N	F	\N	2012-11-16	1	\N
00000003	01	00000002	\N	F	\N	2012-11-17	1	\N
00000003	01	00000002	\N	D	\N	2012-11-18	1	\N
00000003	01	00000002	\N	D	\N	2012-11-19	1	\N
00000003	01	00000002	\N	D	\N	2012-11-20	1	\N
00000003	01	00000002	\N	D	\N	2012-11-21	1	\N
00000003	01	00000002	\N	D	\N	2012-11-22	1	\N
00000003	01	00000002	\N	F	\N	2012-11-23	1	\N
00000003	01	00000002	\N	F	\N	2012-11-24	1	\N
00000003	01	00000002	\N	D	\N	2012-11-25	1	\N
00000003	01	00000002	\N	D	\N	2012-11-26	1	\N
00000003	01	00000002	\N	D	\N	2012-11-27	1	\N
00000003	01	00000002	\N	D	\N	2012-11-28	1	\N
00000003	01	00000002	\N	D	\N	2012-11-29	1	\N
00000003	01	00000002	\N	F	\N	2012-11-30	1	\N
00000004	01	00000002	\N	D	\N	2012-11-01	1	\N
00000004	01	00000002	\N	F	\N	2012-11-02	1	\N
00000004	01	00000002	\N	F	\N	2012-11-03	1	\N
00000004	01	00000002	\N	D	\N	2012-11-04	1	\N
00000004	01	00000002	\N	D	\N	2012-11-05	1	\N
00000004	01	00000002	\N	D	\N	2012-11-08	1	\N
00000004	01	00000002	\N	F	\N	2012-11-09	1	\N
00000004	01	00000002	\N	F	\N	2012-11-10	1	\N
00000004	01	00000002	\N	D	\N	2012-11-11	1	\N
00000004	01	00000002	\N	D	\N	2012-11-12	1	\N
00000004	01	00000002	\N	D	\N	2012-11-13	1	\N
00000004	01	00000002	\N	D	\N	2012-11-14	1	\N
00000004	01	00000002	\N	D	\N	2012-11-15	1	\N
00000004	01	00000002	\N	F	\N	2012-11-16	1	\N
00000004	01	00000002	\N	F	\N	2012-11-17	1	\N
00000004	01	00000002	\N	D	\N	2012-11-18	1	\N
00000004	01	00000002	\N	D	\N	2012-11-19	1	\N
00000004	01	00000002	\N	D	\N	2012-11-20	1	\N
00000004	01	00000002	\N	D	\N	2012-11-21	1	\N
00000004	01	00000002	\N	D	\N	2012-11-22	1	\N
00000004	01	00000002	\N	F	\N	2012-11-23	1	\N
00000004	01	00000002	\N	F	\N	2012-11-24	1	\N
00000004	01	00000002	\N	D	\N	2012-11-25	1	\N
00000004	01	00000002	\N	D	\N	2012-11-26	1	\N
00000004	01	00000002	\N	D	\N	2012-11-27	1	\N
00000004	01	00000002	\N	D	\N	2012-11-28	1	\N
00000004	01	00000002	\N	D	\N	2012-11-29	1	\N
00000004	01	00000002	\N	F	\N	2012-11-30	1	\N
00000005	01	00000002	\N	D	\N	2012-11-01	1	\N
00000005	01	00000002	\N	F	\N	2012-11-02	1	\N
00000005	01	00000002	\N	F	\N	2012-11-03	1	\N
00000005	01	00000002	\N	D	\N	2012-11-04	1	\N
00000005	01	00000002	\N	D	\N	2012-11-05	1	\N
00000005	01	00000002	\N	D	\N	2012-11-08	1	\N
00000005	01	00000002	\N	F	\N	2012-11-09	1	\N
00000005	01	00000002	\N	F	\N	2012-11-10	1	\N
00000005	01	00000002	\N	D	\N	2012-11-11	1	\N
00000005	01	00000002	\N	D	\N	2012-11-12	1	\N
00000005	01	00000002	\N	D	\N	2012-11-13	1	\N
00000005	01	00000002	\N	D	\N	2012-11-14	1	\N
00000005	01	00000002	\N	D	\N	2012-11-15	1	\N
00000005	01	00000002	\N	F	\N	2012-11-16	1	\N
00000005	01	00000002	\N	F	\N	2012-11-17	1	\N
00000005	01	00000002	\N	D	\N	2012-11-18	1	\N
00000005	01	00000002	\N	D	\N	2012-11-19	1	\N
00000005	01	00000002	\N	D	\N	2012-11-20	1	\N
00000005	01	00000002	\N	D	\N	2012-11-21	1	\N
00000005	01	00000002	\N	D	\N	2012-11-22	1	\N
00000005	01	00000002	\N	F	\N	2012-11-23	1	\N
00000005	01	00000002	\N	F	\N	2012-11-24	1	\N
00000005	01	00000002	\N	D	\N	2012-11-25	1	\N
00000005	01	00000002	\N	D	\N	2012-11-26	1	\N
00000005	01	00000002	\N	D	\N	2012-11-27	1	\N
00000005	01	00000002	\N	D	\N	2012-11-28	1	\N
00000005	01	00000002	\N	D	\N	2012-11-29	1	\N
00000005	01	00000002	\N	F	\N	2012-11-30	1	\N
00000006	01	00000002	\N	D	\N	2012-11-01	1	\N
00000006	01	00000002	\N	F	\N	2012-11-02	1	\N
00000006	01	00000002	\N	D	\N	2012-11-05	1	\N
00000006	01	00000002	\N	D	\N	2012-11-08	1	\N
00000006	01	00000002	\N	F	\N	2012-11-09	1	\N
00000006	01	00000002	\N	F	\N	2012-11-10	1	\N
00000006	01	00000002	\N	D	\N	2012-11-11	1	\N
00000006	01	00000002	\N	D	\N	2012-11-12	1	\N
00000006	01	00000002	\N	D	\N	2012-11-13	1	\N
00000006	01	00000002	\N	D	\N	2012-11-14	1	\N
00000006	01	00000002	\N	D	\N	2012-11-15	1	\N
00000006	01	00000002	\N	F	\N	2012-11-16	1	\N
00000006	01	00000002	\N	F	\N	2012-11-17	1	\N
00000006	01	00000002	\N	D	\N	2012-11-18	1	\N
00000006	01	00000002	\N	D	\N	2012-11-19	1	\N
00000006	01	00000002	\N	D	\N	2012-11-20	1	\N
00000006	01	00000002	\N	D	\N	2012-11-21	1	\N
00000006	01	00000002	\N	D	\N	2012-11-22	1	\N
00000006	01	00000002	\N	F	\N	2012-11-23	1	\N
00000006	01	00000002	\N	F	\N	2012-11-24	1	\N
00000006	01	00000002	\N	D	\N	2012-11-25	1	\N
00000006	01	00000002	\N	D	\N	2012-11-26	1	\N
00000006	01	00000002	\N	D	\N	2012-11-27	1	\N
00000006	01	00000002	\N	D	\N	2012-11-28	1	\N
00000006	01	00000002	\N	D	\N	2012-11-29	1	\N
00000006	01	00000002	\N	F	\N	2012-11-30	1	\N
00000007	01	00000002	\N	D	\N	2012-11-01	1	\N
00000007	01	00000002	\N	F	\N	2012-11-02	1	\N
00000007	01	00000002	\N	F	\N	2012-11-03	1	\N
00000007	01	00000002	\N	D	\N	2012-11-04	1	\N
00000007	01	00000002	\N	D	\N	2012-11-05	1	\N
00000007	01	00000002	\N	D	\N	2012-11-08	1	\N
00000007	01	00000002	\N	F	\N	2012-11-09	1	\N
00000007	01	00000002	\N	F	\N	2012-11-10	1	\N
00000007	01	00000002	\N	D	\N	2012-11-11	1	\N
00000007	01	00000002	\N	D	\N	2012-11-12	1	\N
00000007	01	00000002	\N	D	\N	2012-11-13	1	\N
00000007	01	00000002	\N	D	\N	2012-11-14	1	\N
00000007	01	00000002	\N	D	\N	2012-11-15	1	\N
00000007	01	00000002	\N	F	\N	2012-11-16	1	\N
00000007	01	00000002	\N	F	\N	2012-11-17	1	\N
00000007	01	00000002	\N	D	\N	2012-11-18	1	\N
00000007	01	00000002	\N	D	\N	2012-11-19	1	\N
00000007	01	00000002	\N	D	\N	2012-11-20	1	\N
00000007	01	00000002	\N	D	\N	2012-11-21	1	\N
00000007	01	00000002	\N	D	\N	2012-11-22	1	\N
00000007	01	00000002	\N	F	\N	2012-11-23	1	\N
00000007	01	00000002	\N	F	\N	2012-11-24	1	\N
00000007	01	00000002	\N	D	\N	2012-11-25	1	\N
00000007	01	00000002	\N	D	\N	2012-11-26	1	\N
00000007	01	00000002	\N	D	\N	2012-11-27	1	\N
00000007	01	00000002	\N	D	\N	2012-11-28	1	\N
00000007	01	00000002	\N	D	\N	2012-11-29	1	\N
00000007	01	00000002	\N	F	\N	2012-11-30	1	\N
00000001	01	00000002	\N	F	\N	2012-12-01	1	\N
00000001	01	00000002	\N	D	\N	2012-12-02	1	\N
00000001	01	00000002	\N	D	\N	2012-12-03	1	\N
00000001	01	00000002	\N	D	\N	2012-12-04	1	\N
00000001	01	00000002	\N	D	\N	2012-12-05	1	\N
00000001	01	00000002	\N	F	\N	2012-12-07	1	\N
00000001	01	00000002	\N	F	\N	2012-12-08	1	\N
00000001	01	00000002	\N	D	\N	2012-12-09	1	\N
00000001	01	00000002	\N	D	\N	2012-12-10	1	\N
00000001	01	00000002	\N	D	\N	2012-12-11	1	\N
00000001	01	00000002	\N	D	\N	2012-12-12	1	\N
00000001	01	00000002	\N	D	\N	2012-12-13	1	\N
00000001	01	00000002	\N	F	\N	2012-12-14	1	\N
00000001	01	00000002	\N	F	\N	2012-12-15	1	\N
00000001	01	00000002	\N	D	\N	2012-12-16	1	\N
00000001	01	00000002	\N	D	\N	2012-12-17	1	\N
00000001	01	00000002	\N	D	\N	2012-12-18	1	\N
00000001	01	00000002	\N	F	\N	2012-12-21	1	\N
00000001	01	00000002	\N	F	\N	2012-12-22	1	\N
00000001	01	00000002	\N	D	\N	2012-12-23	1	\N
00000001	01	00000002	\N	D	\N	2012-12-24	1	\N
00000001	01	00000002	\N	D	\N	2012-12-25	1	\N
00000001	01	00000002	\N	D	\N	2012-12-26	1	\N
00000001	01	00000002	\N	F	\N	2012-12-29	1	\N
00000001	01	00000002	\N	D	\N	2012-12-30	1	\N
00000002	01	00000002	\N	F	\N	2012-12-01	1	\N
00000002	01	00000002	\N	D	\N	2012-12-02	1	\N
00000002	01	00000002	\N	D	\N	2012-12-03	1	\N
00000002	01	00000002	\N	D	\N	2012-12-04	1	\N
00000002	01	00000002	\N	D	\N	2012-12-05	1	\N
00000002	01	00000002	\N	D	\N	2012-12-06	1	\N
00000002	01	00000002	\N	F	\N	2012-12-07	1	\N
00000002	01	00000002	\N	F	\N	2012-12-08	1	\N
00000002	01	00000002	\N	D	\N	2012-12-09	1	\N
00000002	01	00000002	\N	D	\N	2012-12-10	1	\N
00000002	01	00000002	\N	D	\N	2012-12-11	1	\N
00000002	01	00000002	\N	D	\N	2012-12-12	1	\N
00000002	01	00000002	\N	D	\N	2012-12-13	1	\N
00000002	01	00000002	\N	F	\N	2012-12-14	1	\N
00000002	01	00000002	\N	F	\N	2012-12-15	1	\N
00000002	01	00000002	\N	D	\N	2012-12-16	1	\N
00000002	01	00000002	\N	D	\N	2012-12-17	1	\N
00000002	01	00000002	\N	D	\N	2012-12-18	1	\N
00000002	01	00000002	\N	D	\N	2012-12-19	1	\N
00000002	01	00000002	\N	D	\N	2012-12-20	1	\N
00000002	01	00000002	\N	F	\N	2012-12-21	1	\N
00000002	01	00000002	\N	F	\N	2012-12-22	1	\N
00000002	01	00000002	\N	D	\N	2012-12-23	1	\N
00000002	01	00000002	\N	D	\N	2012-12-24	1	\N
00000002	01	00000002	\N	D	\N	2012-12-25	1	\N
00000002	01	00000002	\N	D	\N	2012-12-26	1	\N
00000002	01	00000002	\N	D	\N	2012-12-27	1	\N
00000002	01	00000002	\N	F	\N	2012-12-28	1	\N
00000002	01	00000002	\N	F	\N	2012-12-29	1	\N
00000002	01	00000002	\N	D	\N	2012-12-30	1	\N
00000003	01	00000002	\N	F	\N	2012-12-01	1	\N
00000003	01	00000002	\N	D	\N	2012-12-02	1	\N
00000003	01	00000002	\N	D	\N	2012-12-03	1	\N
00000003	01	00000002	\N	D	\N	2012-12-04	1	\N
00000003	01	00000002	\N	D	\N	2012-12-05	1	\N
00000003	01	00000002	\N	D	\N	2012-12-06	1	\N
00000003	01	00000002	\N	F	\N	2012-12-07	1	\N
00000003	01	00000002	\N	F	\N	2012-12-08	1	\N
00000003	01	00000002	\N	D	\N	2012-12-09	1	\N
00000003	01	00000002	\N	D	\N	2012-12-10	1	\N
00000003	01	00000002	\N	D	\N	2012-12-11	1	\N
00000003	01	00000002	\N	D	\N	2012-12-12	1	\N
00000003	01	00000002	\N	D	\N	2012-12-13	1	\N
00000003	01	00000002	\N	F	\N	2012-12-14	1	\N
00000003	01	00000002	\N	F	\N	2012-12-15	1	\N
00000003	01	00000002	\N	D	\N	2012-12-16	1	\N
00000003	01	00000002	\N	D	\N	2012-12-17	1	\N
00000003	01	00000002	\N	D	\N	2012-12-18	1	\N
00000003	01	00000002	\N	D	\N	2012-12-19	1	\N
00000003	01	00000002	\N	D	\N	2012-12-20	1	\N
00000003	01	00000002	\N	F	\N	2012-12-21	1	\N
00000003	01	00000002	\N	F	\N	2012-12-22	1	\N
00000003	01	00000002	\N	D	\N	2012-12-23	1	\N
00000003	01	00000002	\N	D	\N	2012-12-24	1	\N
00000003	01	00000002	\N	D	\N	2012-12-25	1	\N
00000003	01	00000002	\N	D	\N	2012-12-26	1	\N
00000003	01	00000002	\N	D	\N	2012-12-27	1	\N
00000003	01	00000002	\N	F	\N	2012-12-28	1	\N
00000003	01	00000002	\N	F	\N	2012-12-29	1	\N
00000003	01	00000002	\N	D	\N	2012-12-30	1	\N
00000001	01	00000002	\N	D	0000000029	2012-12-19	R	\N
00000001	01	00000002	\N	D	\N	2012-12-31	1	\N
00000002	01	00000002	\N	D	\N	2012-12-31	1	\N
00000001	01	00000002	\N	D	0000000041	2012-12-27	R	\N
00000001	01	00000002	\N	F	0000000041	2012-12-28	R	\N
00000004	01	00000002	\N	F	\N	2012-12-01	1	\N
00000004	01	00000002	\N	D	\N	2012-12-02	1	\N
00000004	01	00000002	\N	D	\N	2012-12-03	1	\N
00000004	01	00000002	\N	D	\N	2012-12-04	1	\N
00000004	01	00000002	\N	D	\N	2012-12-05	1	\N
00000004	01	00000002	\N	D	\N	2012-12-06	1	\N
00000004	01	00000002	\N	F	\N	2012-12-07	1	\N
00000004	01	00000002	\N	F	\N	2012-12-08	1	\N
00000004	01	00000002	\N	D	\N	2012-12-09	1	\N
00000004	01	00000002	\N	D	\N	2012-12-10	1	\N
00000004	01	00000002	\N	D	\N	2012-12-11	1	\N
00000004	01	00000002	\N	D	\N	2012-12-12	1	\N
00000004	01	00000002	\N	D	\N	2012-12-13	1	\N
00000004	01	00000002	\N	F	\N	2012-12-14	1	\N
00000004	01	00000002	\N	F	\N	2012-12-15	1	\N
00000004	01	00000002	\N	D	\N	2012-12-16	1	\N
00000004	01	00000002	\N	D	\N	2012-12-17	1	\N
00000004	01	00000002	\N	D	\N	2012-12-18	1	\N
00000004	01	00000002	\N	D	\N	2012-12-19	1	\N
00000004	01	00000002	\N	D	\N	2012-12-20	1	\N
00000004	01	00000002	\N	F	\N	2012-12-21	1	\N
00000004	01	00000002	\N	F	\N	2012-12-22	1	\N
00000004	01	00000002	\N	D	\N	2012-12-23	1	\N
00000004	01	00000002	\N	D	\N	2012-12-24	1	\N
00000004	01	00000002	\N	D	\N	2012-12-25	1	\N
00000004	01	00000002	\N	D	\N	2012-12-26	1	\N
00000004	01	00000002	\N	D	\N	2012-12-27	1	\N
00000004	01	00000002	\N	F	\N	2012-12-28	1	\N
00000004	01	00000002	\N	F	\N	2012-12-29	1	\N
00000004	01	00000002	\N	D	\N	2012-12-30	1	\N
00000005	01	00000002	\N	F	\N	2012-12-01	1	\N
00000005	01	00000002	\N	D	\N	2012-12-02	1	\N
00000005	01	00000002	\N	D	\N	2012-12-03	1	\N
00000005	01	00000002	\N	D	\N	2012-12-04	1	\N
00000005	01	00000002	\N	D	\N	2012-12-05	1	\N
00000005	01	00000002	\N	D	\N	2012-12-06	1	\N
00000005	01	00000002	\N	F	\N	2012-12-07	1	\N
00000005	01	00000002	\N	F	\N	2012-12-08	1	\N
00000005	01	00000002	\N	D	\N	2012-12-09	1	\N
00000005	01	00000002	\N	D	\N	2012-12-10	1	\N
00000005	01	00000002	\N	D	\N	2012-12-11	1	\N
00000005	01	00000002	\N	D	\N	2012-12-12	1	\N
00000005	01	00000002	\N	D	\N	2012-12-13	1	\N
00000005	01	00000002	\N	F	\N	2012-12-14	1	\N
00000005	01	00000002	\N	F	\N	2012-12-15	1	\N
00000005	01	00000002	\N	D	\N	2012-12-16	1	\N
00000005	01	00000002	\N	D	\N	2012-12-17	1	\N
00000005	01	00000002	\N	D	\N	2012-12-18	1	\N
00000005	01	00000002	\N	D	\N	2012-12-19	1	\N
00000005	01	00000002	\N	D	\N	2012-12-20	1	\N
00000005	01	00000002	\N	F	\N	2012-12-21	1	\N
00000005	01	00000002	\N	F	\N	2012-12-22	1	\N
00000005	01	00000002	\N	D	\N	2012-12-23	1	\N
00000005	01	00000002	\N	D	\N	2012-12-24	1	\N
00000005	01	00000002	\N	D	\N	2012-12-25	1	\N
00000005	01	00000002	\N	D	\N	2012-12-26	1	\N
00000005	01	00000002	\N	D	\N	2012-12-27	1	\N
00000005	01	00000002	\N	F	\N	2012-12-28	1	\N
00000005	01	00000002	\N	F	\N	2012-12-29	1	\N
00000005	01	00000002	\N	D	\N	2012-12-30	1	\N
00000006	01	00000002	\N	F	\N	2012-12-01	1	\N
00000006	01	00000002	\N	D	\N	2012-12-02	1	\N
00000006	01	00000002	\N	D	\N	2012-12-03	1	\N
00000006	01	00000002	\N	D	\N	2012-12-04	1	\N
00000006	01	00000002	\N	D	\N	2012-12-05	1	\N
00000006	01	00000002	\N	D	\N	2012-12-06	1	\N
00000006	01	00000002	\N	F	\N	2012-12-07	1	\N
00000006	01	00000002	\N	F	\N	2012-12-08	1	\N
00000006	01	00000002	\N	D	\N	2012-12-09	1	\N
00000006	01	00000002	\N	D	\N	2012-12-10	1	\N
00000006	01	00000002	\N	D	\N	2012-12-11	1	\N
00000006	01	00000002	\N	D	\N	2012-12-12	1	\N
00000006	01	00000002	\N	D	\N	2012-12-13	1	\N
00000006	01	00000002	\N	F	\N	2012-12-14	1	\N
00000006	01	00000002	\N	F	\N	2012-12-15	1	\N
00000006	01	00000002	\N	D	\N	2012-12-16	1	\N
00000006	01	00000002	\N	D	\N	2012-12-17	1	\N
00000006	01	00000002	\N	D	\N	2012-12-18	1	\N
00000006	01	00000002	\N	D	\N	2012-12-19	1	\N
00000006	01	00000002	\N	D	\N	2012-12-20	1	\N
00000006	01	00000002	\N	F	\N	2012-12-21	1	\N
00000006	01	00000002	\N	F	\N	2012-12-22	1	\N
00000006	01	00000002	\N	D	\N	2012-12-23	1	\N
00000006	01	00000002	\N	D	\N	2012-12-24	1	\N
00000006	01	00000002	\N	D	\N	2012-12-25	1	\N
00000006	01	00000002	\N	D	\N	2012-12-26	1	\N
00000006	01	00000002	\N	D	\N	2012-12-27	1	\N
00000006	01	00000002	\N	F	\N	2012-12-28	1	\N
00000006	01	00000002	\N	F	\N	2012-12-29	1	\N
00000006	01	00000002	\N	D	\N	2012-12-30	1	\N
00000007	01	00000002	\N	F	\N	2012-12-01	1	\N
00000007	01	00000002	\N	D	\N	2012-12-02	1	\N
00000007	01	00000002	\N	D	\N	2012-12-03	1	\N
00000007	01	00000002	\N	D	\N	2012-12-04	1	\N
00000007	01	00000002	\N	D	\N	2012-12-05	1	\N
00000007	01	00000002	\N	D	\N	2012-12-06	1	\N
00000007	01	00000002	\N	F	\N	2012-12-07	1	\N
00000007	01	00000002	\N	F	\N	2012-12-08	1	\N
00000007	01	00000002	\N	D	\N	2012-12-09	1	\N
00000007	01	00000002	\N	D	\N	2012-12-10	1	\N
00000007	01	00000002	\N	D	\N	2012-12-11	1	\N
00000007	01	00000002	\N	D	\N	2012-12-12	1	\N
00000007	01	00000002	\N	D	\N	2012-12-13	1	\N
00000007	01	00000002	\N	F	\N	2012-12-14	1	\N
00000007	01	00000002	\N	F	\N	2012-12-15	1	\N
00000007	01	00000002	\N	D	\N	2012-12-16	1	\N
00000007	01	00000002	\N	D	\N	2012-12-17	1	\N
00000007	01	00000002	\N	D	\N	2012-12-18	1	\N
00000007	01	00000002	\N	D	\N	2012-12-19	1	\N
00000007	01	00000002	\N	D	\N	2012-12-20	1	\N
00000007	01	00000002	\N	F	\N	2012-12-21	1	\N
00000007	01	00000002	\N	F	\N	2012-12-22	1	\N
00000007	01	00000002	\N	D	\N	2012-12-23	1	\N
00000007	01	00000002	\N	D	\N	2012-12-24	1	\N
00000007	01	00000002	\N	D	\N	2012-12-25	1	\N
00000007	01	00000002	\N	D	\N	2012-12-26	1	\N
00000007	01	00000002	\N	D	\N	2012-12-27	1	\N
00000007	01	00000002	\N	F	\N	2012-12-28	1	\N
00000007	01	00000002	\N	F	\N	2012-12-29	1	\N
00000007	01	00000002	\N	D	\N	2012-12-30	1	\N
00000017	02	00000002	\N	F	\N	2012-11-09	1	\N
00000017	02	00000002	\N	F	\N	2012-11-10	1	\N
00000017	02	00000002	\N	D	\N	2012-11-11	1	\N
00000017	02	00000002	\N	D	\N	2012-11-12	1	\N
00000017	02	00000002	\N	D	\N	2012-11-14	1	\N
00000017	02	00000002	\N	F	\N	2012-11-17	1	\N
00000017	02	00000002	\N	D	\N	2012-11-18	1	\N
00000017	02	00000002	\N	D	\N	2012-11-19	1	\N
00000017	02	00000002	\N	D	\N	2012-11-20	1	\N
00000017	02	00000002	\N	D	\N	2012-11-21	1	\N
00000017	02	00000002	\N	D	\N	2012-11-22	1	\N
00000017	02	00000002	\N	F	\N	2012-11-23	1	\N
00000017	02	00000002	\N	F	\N	2012-11-24	1	\N
00000017	02	00000002	\N	D	\N	2012-11-25	1	\N
00000017	02	00000002	\N	D	\N	2012-11-26	1	\N
00000017	02	00000002	\N	D	\N	2012-11-27	1	\N
00000017	02	00000002	\N	D	\N	2012-11-28	1	\N
00000017	02	00000002	\N	D	\N	2012-11-29	1	\N
00000017	02	00000002	\N	F	\N	2012-11-30	1	\N
00000018	02	00000002	\N	D	\N	2012-11-01	1	\N
00000018	02	00000002	\N	F	\N	2012-11-02	1	\N
00000018	02	00000002	\N	F	\N	2012-11-03	1	\N
00000018	02	00000002	\N	D	\N	2012-11-04	1	\N
00000018	02	00000002	\N	D	\N	2012-11-05	1	\N
00000018	02	00000002	\N	D	\N	2012-11-06	1	\N
00000018	02	00000002	\N	F	\N	2012-11-17	1	\N
00000018	02	00000002	\N	D	\N	2012-11-18	1	\N
00000018	02	00000002	\N	D	\N	2012-11-19	1	\N
00000018	02	00000002	\N	D	\N	2012-11-20	1	\N
00000018	02	00000002	\N	D	\N	2012-11-21	1	\N
00000018	02	00000002	\N	D	\N	2012-11-22	1	\N
00000018	02	00000002	\N	F	\N	2012-11-23	1	\N
00000018	02	00000002	\N	F	\N	2012-11-24	1	\N
00000018	02	00000002	\N	D	\N	2012-11-25	1	\N
00000018	02	00000002	\N	D	\N	2012-11-26	1	\N
00000018	02	00000002	\N	D	\N	2012-11-27	1	\N
00000018	02	00000002	\N	D	\N	2012-11-28	1	\N
00000018	02	00000002	\N	D	\N	2012-11-29	1	\N
00000018	02	00000002	\N	F	\N	2012-11-30	1	\N
00000019	02	00000002	\N	D	\N	2012-11-01	1	\N
00000019	02	00000002	\N	F	\N	2012-11-02	1	\N
00000019	02	00000002	\N	F	\N	2012-11-03	1	\N
00000019	02	00000002	\N	D	\N	2012-11-04	1	\N
00000019	02	00000002	\N	D	\N	2012-11-05	1	\N
00000019	02	00000002	\N	D	\N	2012-11-06	1	\N
00000019	02	00000002	\N	D	\N	2012-11-07	1	\N
00000019	02	00000002	\N	D	\N	2012-11-08	1	\N
00000019	02	00000002	\N	F	\N	2012-11-09	1	\N
00000019	02	00000002	\N	F	\N	2012-11-10	1	\N
00000019	02	00000002	\N	D	\N	2012-11-11	1	\N
00000019	02	00000002	\N	D	\N	2012-11-12	1	\N
00000019	02	00000002	\N	D	\N	2012-11-13	1	\N
00000019	02	00000002	\N	D	\N	2012-11-14	1	\N
00000019	02	00000002	\N	F	\N	2012-11-17	1	\N
00000019	02	00000002	\N	D	\N	2012-11-18	1	\N
00000019	02	00000002	\N	D	\N	2012-11-19	1	\N
00000019	02	00000002	\N	D	\N	2012-11-20	1	\N
00000019	02	00000002	\N	D	\N	2012-11-21	1	\N
00000019	02	00000002	\N	D	\N	2012-11-22	1	\N
00000019	02	00000002	\N	F	\N	2012-11-23	1	\N
00000019	02	00000002	\N	F	\N	2012-11-24	1	\N
00000019	02	00000002	\N	D	\N	2012-11-25	1	\N
00000019	02	00000002	\N	D	\N	2012-11-26	1	\N
00000019	02	00000002	\N	D	\N	2012-11-27	1	\N
00000019	02	00000002	\N	D	\N	2012-11-28	1	\N
00000019	02	00000002	\N	D	\N	2012-11-29	1	\N
00000019	02	00000002	\N	F	\N	2012-11-30	1	\N
00000020	02	00000002	\N	D	\N	2012-11-01	1	\N
00000020	02	00000002	\N	F	\N	2012-11-02	1	\N
00000020	02	00000002	\N	F	\N	2012-11-03	1	\N
00000020	02	00000002	\N	D	\N	2012-11-04	1	\N
00000020	02	00000002	\N	D	\N	2012-11-05	1	\N
00000020	02	00000002	\N	D	\N	2012-11-06	1	\N
00000020	02	00000002	\N	D	\N	2012-11-07	1	\N
00000020	02	00000002	\N	D	\N	2012-11-08	1	\N
00000020	02	00000002	\N	F	\N	2012-11-09	1	\N
00000020	02	00000002	\N	F	\N	2012-11-10	1	\N
00000020	02	00000002	\N	D	\N	2012-11-11	1	\N
00000020	02	00000002	\N	D	\N	2012-11-12	1	\N
00000020	02	00000002	\N	D	\N	2012-11-13	1	\N
00000020	02	00000002	\N	D	\N	2012-11-14	1	\N
00000020	02	00000002	\N	F	\N	2012-11-17	1	\N
00000020	02	00000002	\N	D	\N	2012-11-18	1	\N
00000020	02	00000002	\N	D	\N	2012-11-19	1	\N
00000020	02	00000002	\N	D	\N	2012-11-20	1	\N
00000020	02	00000002	\N	D	\N	2012-11-21	1	\N
00000020	02	00000002	\N	D	\N	2012-11-22	1	\N
00000020	02	00000002	\N	F	\N	2012-11-23	1	\N
00000020	02	00000002	\N	F	\N	2012-11-24	1	\N
00000020	02	00000002	\N	D	\N	2012-11-25	1	\N
00000020	02	00000002	\N	D	\N	2012-11-26	1	\N
00000020	02	00000002	\N	D	\N	2012-11-27	1	\N
00000020	02	00000002	\N	D	\N	2012-11-28	1	\N
00000020	02	00000002	\N	D	\N	2012-11-29	1	\N
00000020	02	00000002	\N	F	\N	2012-11-30	1	\N
00000021	02	00000002	\N	D	\N	2012-11-01	1	\N
00000021	02	00000002	\N	F	\N	2012-11-02	1	\N
00000021	02	00000002	\N	F	\N	2012-11-03	1	\N
00000021	02	00000002	\N	D	\N	2012-11-04	1	\N
00000021	02	00000002	\N	D	\N	2012-11-05	1	\N
00000021	02	00000002	\N	D	\N	2012-11-06	1	\N
00000021	02	00000002	\N	D	\N	2012-11-07	1	\N
00000021	02	00000002	\N	D	\N	2012-11-08	1	\N
00000021	02	00000002	\N	F	\N	2012-11-09	1	\N
00000021	02	00000002	\N	F	\N	2012-11-10	1	\N
00000021	02	00000002	\N	D	\N	2012-11-11	1	\N
00000021	02	00000002	\N	D	\N	2012-11-12	1	\N
00000021	02	00000002	\N	D	\N	2012-11-13	1	\N
00000021	02	00000002	\N	D	\N	2012-11-14	1	\N
00000021	02	00000002	\N	F	\N	2012-11-17	1	\N
00000021	02	00000002	\N	D	\N	2012-11-18	1	\N
00000021	02	00000002	\N	D	\N	2012-11-19	1	\N
00000021	02	00000002	\N	D	\N	2012-11-20	1	\N
00000021	02	00000002	\N	D	\N	2012-11-21	1	\N
00000021	02	00000002	\N	D	\N	2012-11-22	1	\N
00000021	02	00000002	\N	F	\N	2012-11-23	1	\N
00000021	02	00000002	\N	F	\N	2012-11-24	1	\N
00000021	02	00000002	\N	D	\N	2012-11-25	1	\N
00000021	02	00000002	\N	D	\N	2012-11-26	1	\N
00000021	02	00000002	\N	D	\N	2012-11-27	1	\N
00000021	02	00000002	\N	D	\N	2012-11-28	1	\N
00000021	02	00000002	\N	D	\N	2012-11-29	1	\N
00000021	02	00000002	\N	F	\N	2012-11-30	1	\N
00000022	02	00000002	\N	D	\N	2012-11-01	1	\N
00000022	02	00000002	\N	F	\N	2012-11-02	1	\N
00000022	02	00000002	\N	F	\N	2012-11-03	1	\N
00000022	02	00000002	\N	D	\N	2012-11-04	1	\N
00000022	02	00000002	\N	D	\N	2012-11-05	1	\N
00000022	02	00000002	\N	D	\N	2012-11-06	1	\N
00000022	02	00000002	\N	D	\N	2012-11-07	1	\N
00000022	02	00000002	\N	D	\N	2012-11-08	1	\N
00000022	02	00000002	\N	F	\N	2012-11-09	1	\N
00000022	02	00000002	\N	F	\N	2012-11-10	1	\N
00000022	02	00000002	\N	D	\N	2012-11-11	1	\N
00000022	02	00000002	\N	D	\N	2012-11-12	1	\N
00000022	02	00000002	\N	D	\N	2012-11-13	1	\N
00000022	02	00000002	\N	F	\N	2012-11-23	1	\N
00000022	02	00000002	\N	F	\N	2012-11-24	1	\N
00000022	02	00000002	\N	D	\N	2012-11-25	1	\N
00000022	02	00000002	\N	D	\N	2012-11-26	1	\N
00000022	02	00000002	\N	D	\N	2012-11-27	1	\N
00000022	02	00000002	\N	D	\N	2012-11-28	1	\N
00000022	02	00000002	\N	D	\N	2012-11-29	1	\N
00000022	02	00000002	\N	F	\N	2012-11-30	1	\N
00000023	02	00000002	\N	D	\N	2012-11-01	1	\N
00000023	02	00000002	\N	F	\N	2012-11-02	1	\N
00000023	02	00000002	\N	F	\N	2012-11-03	1	\N
00000023	02	00000002	\N	D	\N	2012-11-04	1	\N
00000023	02	00000002	\N	D	\N	2012-11-05	1	\N
00000023	02	00000002	\N	D	\N	2012-11-06	1	\N
00000023	02	00000002	\N	D	\N	2012-11-07	1	\N
00000023	02	00000002	\N	D	\N	2012-11-08	1	\N
00000023	02	00000002	\N	F	\N	2012-11-09	1	\N
00000023	02	00000002	\N	F	\N	2012-11-10	1	\N
00000023	02	00000002	\N	D	\N	2012-11-11	1	\N
00000023	02	00000002	\N	D	\N	2012-11-12	1	\N
00000023	02	00000002	\N	D	\N	2012-11-13	1	\N
00000023	02	00000002	\N	D	\N	2012-11-14	1	\N
00000023	02	00000002	\N	F	\N	2012-11-17	1	\N
00000023	02	00000002	\N	D	\N	2012-11-18	1	\N
00000023	02	00000002	\N	D	\N	2012-11-19	1	\N
00000023	02	00000002	\N	D	\N	2012-11-20	1	\N
00000023	02	00000002	\N	D	\N	2012-11-21	1	\N
00000023	02	00000002	\N	D	\N	2012-11-22	1	\N
00000023	02	00000002	\N	F	\N	2012-11-23	1	\N
00000023	02	00000002	\N	F	\N	2012-11-24	1	\N
00000023	02	00000002	\N	D	\N	2012-11-25	1	\N
00000023	02	00000002	\N	D	\N	2012-11-26	1	\N
00000023	02	00000002	\N	D	\N	2012-11-27	1	\N
00000023	02	00000002	\N	D	\N	2012-11-28	1	\N
00000023	02	00000002	\N	D	\N	2012-11-29	1	\N
00000023	02	00000002	\N	F	\N	2012-11-30	1	\N
00000024	02	00000002	\N	D	\N	2012-11-01	1	\N
00000024	02	00000002	\N	F	\N	2012-11-02	1	\N
00000024	02	00000002	\N	F	\N	2012-11-03	1	\N
00000024	02	00000002	\N	D	\N	2012-11-04	1	\N
00000024	02	00000002	\N	D	\N	2012-11-05	1	\N
00000024	02	00000002	\N	D	\N	2012-11-06	1	\N
00000024	02	00000002	\N	D	\N	2012-11-07	1	\N
00000024	02	00000002	\N	D	\N	2012-11-08	1	\N
00000024	02	00000002	\N	F	\N	2012-11-09	1	\N
00000024	02	00000002	\N	F	\N	2012-11-10	1	\N
00000024	02	00000002	\N	D	\N	2012-11-11	1	\N
00000024	02	00000002	\N	D	\N	2012-11-12	1	\N
00000024	02	00000002	\N	D	\N	2012-11-13	1	\N
00000024	02	00000002	\N	D	\N	2012-11-14	1	\N
00000024	02	00000002	\N	F	\N	2012-11-17	1	\N
00000024	02	00000002	\N	D	\N	2012-11-18	1	\N
00000024	02	00000002	\N	D	\N	2012-11-19	1	\N
00000024	02	00000002	\N	D	\N	2012-11-20	1	\N
00000024	02	00000002	\N	D	\N	2012-11-21	1	\N
00000024	02	00000002	\N	D	\N	2012-11-22	1	\N
00000024	02	00000002	\N	F	\N	2012-11-23	1	\N
00000024	02	00000002	\N	F	\N	2012-11-24	1	\N
00000024	02	00000002	\N	D	\N	2012-11-25	1	\N
00000024	02	00000002	\N	D	\N	2012-11-26	1	\N
00000024	02	00000002	\N	D	\N	2012-11-27	1	\N
00000024	02	00000002	\N	D	\N	2012-11-28	1	\N
00000024	02	00000002	\N	D	\N	2012-11-29	1	\N
00000024	02	00000002	\N	F	\N	2012-11-30	1	\N
00000025	02	00000002	\N	D	\N	2012-11-01	1	\N
00000025	02	00000002	\N	F	\N	2012-11-02	1	\N
00000025	02	00000002	\N	F	\N	2012-11-03	1	\N
00000025	02	00000002	\N	D	\N	2012-11-04	1	\N
00000025	02	00000002	\N	D	\N	2012-11-05	1	\N
00000025	02	00000002	\N	D	\N	2012-11-06	1	\N
00000025	02	00000002	\N	D	\N	2012-11-07	1	\N
00000025	02	00000002	\N	D	\N	2012-11-08	1	\N
00000025	02	00000002	\N	F	\N	2012-11-09	1	\N
00000025	02	00000002	\N	F	\N	2012-11-10	1	\N
00000025	02	00000002	\N	D	\N	2012-11-11	1	\N
00000025	02	00000002	\N	D	\N	2012-11-12	1	\N
00000025	02	00000002	\N	D	\N	2012-11-13	1	\N
00000025	02	00000002	\N	D	\N	2012-11-14	1	\N
00000025	02	00000002	\N	F	\N	2012-11-17	1	\N
00000025	02	00000002	\N	D	\N	2012-11-18	1	\N
00000025	02	00000002	\N	D	\N	2012-11-19	1	\N
00000025	02	00000002	\N	D	\N	2012-11-20	1	\N
00000025	02	00000002	\N	D	\N	2012-11-21	1	\N
00000025	02	00000002	\N	D	\N	2012-11-22	1	\N
00000025	02	00000002	\N	F	\N	2012-11-23	1	\N
00000025	02	00000002	\N	F	\N	2012-11-24	1	\N
00000025	02	00000002	\N	D	\N	2012-11-25	1	\N
00000025	02	00000002	\N	D	\N	2012-11-26	1	\N
00000025	02	00000002	\N	D	\N	2012-11-27	1	\N
00000025	02	00000002	\N	D	\N	2012-11-28	1	\N
00000025	02	00000002	\N	D	\N	2012-11-29	1	\N
00000025	02	00000002	\N	F	\N	2012-11-30	1	\N
00000026	02	00000002	\N	D	\N	2012-11-01	1	\N
00000026	02	00000002	\N	F	\N	2012-11-02	1	\N
00000026	02	00000002	\N	F	\N	2012-11-03	1	\N
00000026	02	00000002	\N	D	\N	2012-11-04	1	\N
00000026	02	00000002	\N	D	\N	2012-11-05	1	\N
00000026	02	00000002	\N	D	\N	2012-11-06	1	\N
00000026	02	00000002	\N	D	\N	2012-11-07	1	\N
00000026	02	00000002	\N	D	\N	2012-11-08	1	\N
00000026	02	00000002	\N	F	\N	2012-11-09	1	\N
00000026	02	00000002	\N	F	\N	2012-11-10	1	\N
00000026	02	00000002	\N	D	\N	2012-11-11	1	\N
00000026	02	00000002	\N	D	\N	2012-11-22	1	\N
00000026	02	00000002	\N	F	\N	2012-11-23	1	\N
00000026	02	00000002	\N	F	\N	2012-11-24	1	\N
00000026	02	00000002	\N	D	\N	2012-11-25	1	\N
00000026	02	00000002	\N	D	\N	2012-11-26	1	\N
00000026	02	00000002	\N	D	\N	2012-11-27	1	\N
00000026	02	00000002	\N	D	\N	2012-11-28	1	\N
00000026	02	00000002	\N	D	\N	2012-11-29	1	\N
00000026	02	00000002	\N	F	\N	2012-11-30	1	\N
00000027	02	00000002	\N	D	\N	2012-11-01	1	\N
00000027	02	00000002	\N	F	\N	2012-11-02	1	\N
00000027	02	00000002	\N	F	\N	2012-11-03	1	\N
00000027	02	00000002	\N	D	\N	2012-11-04	1	\N
00000027	02	00000002	\N	D	\N	2012-11-05	1	\N
00000027	02	00000002	\N	D	\N	2012-11-06	1	\N
00000027	02	00000002	\N	D	\N	2012-11-07	1	\N
00000027	02	00000002	\N	D	\N	2012-11-08	1	\N
00000027	02	00000002	\N	F	\N	2012-11-09	1	\N
00000027	02	00000002	\N	F	\N	2012-11-10	1	\N
00000027	02	00000002	\N	D	\N	2012-11-11	1	\N
00000027	02	00000002	\N	D	\N	2012-11-12	1	\N
00000027	02	00000002	\N	D	\N	2012-11-13	1	\N
00000027	02	00000002	\N	D	\N	2012-11-14	1	\N
00000027	02	00000002	\N	F	\N	2012-11-17	1	\N
00000027	02	00000002	\N	D	\N	2012-11-18	1	\N
00000027	02	00000002	\N	D	\N	2012-11-19	1	\N
00000027	02	00000002	\N	D	\N	2012-11-20	1	\N
00000027	02	00000002	\N	D	\N	2012-11-21	1	\N
00000027	02	00000002	\N	D	\N	2012-11-22	1	\N
00000027	02	00000002	\N	F	\N	2012-11-23	1	\N
00000027	02	00000002	\N	F	\N	2012-11-24	1	\N
00000027	02	00000002	\N	D	\N	2012-11-25	1	\N
00000027	02	00000002	\N	D	\N	2012-11-26	1	\N
00000027	02	00000002	\N	D	\N	2012-11-27	1	\N
00000027	02	00000002	\N	D	\N	2012-11-28	1	\N
00000027	02	00000002	\N	D	\N	2012-11-29	1	\N
00000027	02	00000002	\N	F	\N	2012-11-30	1	\N
00000028	02	00000002	\N	D	\N	2012-11-01	1	\N
00000028	02	00000002	\N	F	\N	2012-11-02	1	\N
00000028	02	00000002	\N	F	\N	2012-11-03	1	\N
00000028	02	00000002	\N	D	\N	2012-11-04	1	\N
00000028	02	00000002	\N	D	\N	2012-11-05	1	\N
00000028	02	00000002	\N	D	\N	2012-11-06	1	\N
00000028	02	00000002	\N	D	\N	2012-11-07	1	\N
00000028	02	00000002	\N	D	\N	2012-11-08	1	\N
00000028	02	00000002	\N	F	\N	2012-11-09	1	\N
00000028	02	00000002	\N	F	\N	2012-11-10	1	\N
00000028	02	00000002	\N	D	\N	2012-11-11	1	\N
00000028	02	00000002	\N	D	\N	2012-11-12	1	\N
00000028	02	00000002	\N	D	\N	2012-11-13	1	\N
00000028	02	00000002	\N	D	\N	2012-11-14	1	\N
00000028	02	00000002	\N	F	\N	2012-11-17	1	\N
00000028	02	00000002	\N	D	\N	2012-11-18	1	\N
00000028	02	00000002	\N	D	\N	2012-11-19	1	\N
00000028	02	00000002	\N	D	\N	2012-11-20	1	\N
00000028	02	00000002	\N	D	\N	2012-11-21	1	\N
00000028	02	00000002	\N	D	\N	2012-11-22	1	\N
00000028	02	00000002	\N	F	\N	2012-11-23	1	\N
00000028	02	00000002	\N	F	\N	2012-11-24	1	\N
00000028	02	00000002	\N	D	\N	2012-11-25	1	\N
00000028	02	00000002	\N	D	\N	2012-11-26	1	\N
00000028	02	00000002	\N	D	\N	2012-11-27	1	\N
00000028	02	00000002	\N	D	\N	2012-11-28	1	\N
00000028	02	00000002	\N	D	\N	2012-11-29	1	\N
00000028	02	00000002	\N	F	\N	2012-11-30	1	\N
00000029	02	00000002	\N	D	\N	2012-11-01	1	\N
00000029	02	00000002	\N	F	\N	2012-11-02	1	\N
00000029	02	00000002	\N	F	\N	2012-11-03	1	\N
00000029	02	00000002	\N	D	\N	2012-11-04	1	\N
00000029	02	00000002	\N	D	\N	2012-11-05	1	\N
00000029	02	00000002	\N	D	\N	2012-11-06	1	\N
00000029	02	00000002	\N	D	\N	2012-11-07	1	\N
00000029	02	00000002	\N	D	\N	2012-11-08	1	\N
00000029	02	00000002	\N	F	\N	2012-11-09	1	\N
00000029	02	00000002	\N	F	\N	2012-11-10	1	\N
00000029	02	00000002	\N	D	\N	2012-11-11	1	\N
00000029	02	00000002	\N	D	\N	2012-11-12	1	\N
00000029	02	00000002	\N	D	\N	2012-11-13	1	\N
00000029	02	00000002	\N	D	\N	2012-11-14	1	\N
00000029	02	00000002	\N	F	\N	2012-11-17	1	\N
00000029	02	00000002	\N	D	\N	2012-11-18	1	\N
00000029	02	00000002	\N	D	\N	2012-11-19	1	\N
00000029	02	00000002	\N	D	\N	2012-11-20	1	\N
00000029	02	00000002	\N	D	\N	2012-11-21	1	\N
00000029	02	00000002	\N	D	\N	2012-11-22	1	\N
00000029	02	00000002	\N	F	\N	2012-11-23	1	\N
00000029	02	00000002	\N	F	\N	2012-11-24	1	\N
00000029	02	00000002	\N	D	\N	2012-11-25	1	\N
00000029	02	00000002	\N	D	\N	2012-11-26	1	\N
00000029	02	00000002	\N	D	\N	2012-11-27	1	\N
00000029	02	00000002	\N	D	\N	2012-11-28	1	\N
00000026	02	00000002	\N	D	\N	2012-11-15	1	\N
00000030	02	00000002	\N	D	\N	2012-11-05	1	\N
00000030	02	00000002	\N	D	\N	2012-11-06	1	\N
00000030	02	00000002	\N	D	\N	2012-11-07	1	\N
00000030	02	00000002	\N	D	\N	2012-11-08	1	\N
00000030	02	00000002	\N	F	\N	2012-11-09	1	\N
00000030	02	00000002	\N	F	\N	2012-11-10	1	\N
00000030	02	00000002	\N	D	\N	2012-11-11	1	\N
00000030	02	00000002	\N	D	\N	2012-11-12	1	\N
00000030	02	00000002	\N	D	\N	2012-11-13	1	\N
00000030	02	00000002	\N	D	\N	2012-11-14	1	\N
00000030	02	00000002	\N	F	\N	2012-11-17	1	\N
00000030	02	00000002	\N	D	\N	2012-11-18	1	\N
00000030	02	00000002	\N	D	\N	2012-11-19	1	\N
00000030	02	00000002	\N	D	\N	2012-11-20	1	\N
00000030	02	00000002	\N	D	\N	2012-11-21	1	\N
00000030	02	00000002	\N	D	\N	2012-11-22	1	\N
00000030	02	00000002	\N	F	\N	2012-11-23	1	\N
00000030	02	00000002	\N	F	\N	2012-11-24	1	\N
00000030	02	00000002	\N	D	\N	2012-11-25	1	\N
00000030	02	00000002	\N	D	\N	2012-11-26	1	\N
00000030	02	00000002	\N	D	\N	2012-11-27	1	\N
00000030	02	00000002	\N	D	\N	2012-11-28	1	\N
00000030	02	00000002	\N	D	\N	2012-11-29	1	\N
00000030	02	00000002	\N	F	\N	2012-11-30	1	\N
00000031	02	00000002	\N	D	\N	2012-11-01	1	\N
00000031	02	00000002	\N	F	\N	2012-11-02	1	\N
00000031	02	00000002	\N	F	\N	2012-11-03	1	\N
00000031	02	00000002	\N	D	\N	2012-11-04	1	\N
00000031	02	00000002	\N	D	\N	2012-11-05	1	\N
00000031	02	00000002	\N	D	\N	2012-11-06	1	\N
00000031	02	00000002	\N	D	\N	2012-11-07	1	\N
00000031	02	00000002	\N	D	\N	2012-11-08	1	\N
00000031	02	00000002	\N	F	\N	2012-11-09	1	\N
00000031	02	00000002	\N	F	\N	2012-11-10	1	\N
00000031	02	00000002	\N	D	\N	2012-11-11	1	\N
00000031	02	00000002	\N	D	\N	2012-11-12	1	\N
00000031	02	00000002	\N	D	\N	2012-11-13	1	\N
00000031	02	00000002	\N	D	\N	2012-11-14	1	\N
00000031	02	00000002	\N	F	\N	2012-11-17	1	\N
00000031	02	00000002	\N	D	\N	2012-11-18	1	\N
00000031	02	00000002	\N	D	\N	2012-11-19	1	\N
00000031	02	00000002	\N	D	\N	2012-11-20	1	\N
00000031	02	00000002	\N	D	\N	2012-11-21	1	\N
00000031	02	00000002	\N	D	\N	2012-11-22	1	\N
00000031	02	00000002	\N	F	\N	2012-11-23	1	\N
00000031	02	00000002	\N	F	\N	2012-11-24	1	\N
00000031	02	00000002	\N	D	\N	2012-11-25	1	\N
00000031	02	00000002	\N	D	\N	2012-11-26	1	\N
00000031	02	00000002	\N	D	\N	2012-11-27	1	\N
00000031	02	00000002	\N	D	\N	2012-11-28	1	\N
00000031	02	00000002	\N	D	\N	2012-11-29	1	\N
00000031	02	00000002	\N	F	\N	2012-11-30	1	\N
00000032	02	00000002	\N	D	\N	2012-11-01	1	\N
00000032	02	00000002	\N	F	\N	2012-11-02	1	\N
00000032	02	00000002	\N	F	\N	2012-11-03	1	\N
00000032	02	00000002	\N	D	\N	2012-11-04	1	\N
00000032	02	00000002	\N	D	\N	2012-11-05	1	\N
00000032	02	00000002	\N	D	\N	2012-11-06	1	\N
00000032	02	00000002	\N	D	\N	2012-11-07	1	\N
00000032	02	00000002	\N	D	\N	2012-11-08	1	\N
00000032	02	00000002	\N	F	\N	2012-11-09	1	\N
00000032	02	00000002	\N	F	\N	2012-11-10	1	\N
00000032	02	00000002	\N	D	\N	2012-11-11	1	\N
00000032	02	00000002	\N	D	\N	2012-11-12	1	\N
00000032	02	00000002	\N	D	\N	2012-11-13	1	\N
00000032	02	00000002	\N	D	\N	2012-11-14	1	\N
00000032	02	00000002	\N	F	\N	2012-11-17	1	\N
00000032	02	00000002	\N	D	\N	2012-11-18	1	\N
00000032	02	00000002	\N	D	\N	2012-11-19	1	\N
00000032	02	00000002	\N	D	\N	2012-11-20	1	\N
00000032	02	00000002	\N	D	\N	2012-11-21	1	\N
00000032	02	00000002	\N	D	\N	2012-11-22	1	\N
00000032	02	00000002	\N	F	\N	2012-11-23	1	\N
00000032	02	00000002	\N	F	\N	2012-11-24	1	\N
00000032	02	00000002	\N	D	\N	2012-11-25	1	\N
00000032	02	00000002	\N	D	\N	2012-11-26	1	\N
00000032	02	00000002	\N	D	\N	2012-11-27	1	\N
00000032	02	00000002	\N	D	\N	2012-11-28	1	\N
00000032	02	00000002	\N	D	\N	2012-11-29	1	\N
00000032	02	00000002	\N	F	\N	2012-11-30	1	\N
00000033	02	00000002	\N	D	\N	2012-11-01	1	\N
00000033	02	00000002	\N	F	\N	2012-11-02	1	\N
00000033	02	00000002	\N	F	\N	2012-11-03	1	\N
00000033	02	00000002	\N	D	\N	2012-11-04	1	\N
00000033	02	00000002	\N	D	\N	2012-11-12	1	\N
00000033	02	00000002	\N	D	\N	2012-11-13	1	\N
00000033	02	00000002	\N	D	\N	2012-11-14	1	\N
00000033	02	00000002	\N	F	\N	2012-11-17	1	\N
00000033	02	00000002	\N	D	\N	2012-11-18	1	\N
00000033	02	00000002	\N	D	\N	2012-11-19	1	\N
00000033	02	00000002	\N	D	\N	2012-11-20	1	\N
00000033	02	00000002	\N	D	\N	2012-11-21	1	\N
00000033	02	00000002	\N	D	\N	2012-11-22	1	\N
00000033	02	00000002	\N	F	\N	2012-11-23	1	\N
00000033	02	00000002	\N	F	\N	2012-11-24	1	\N
00000033	02	00000002	\N	D	\N	2012-11-25	1	\N
00000033	02	00000002	\N	D	\N	2012-11-26	1	\N
00000033	02	00000002	\N	D	\N	2012-11-27	1	\N
00000033	02	00000002	\N	D	\N	2012-11-28	1	\N
00000033	02	00000002	\N	D	\N	2012-11-29	1	\N
00000033	02	00000002	\N	F	\N	2012-11-30	1	\N
00000034	02	00000002	\N	D	\N	2012-11-01	1	\N
00000034	02	00000002	\N	F	\N	2012-11-02	1	\N
00000034	02	00000002	\N	F	\N	2012-11-03	1	\N
00000034	02	00000002	\N	D	\N	2012-11-04	1	\N
00000034	02	00000002	\N	D	\N	2012-11-05	1	\N
00000034	02	00000002	\N	D	\N	2012-11-06	1	\N
00000034	02	00000002	\N	D	\N	2012-11-07	1	\N
00000034	02	00000002	\N	D	\N	2012-11-08	1	\N
00000034	02	00000002	\N	F	\N	2012-11-09	1	\N
00000034	02	00000002	\N	F	\N	2012-11-10	1	\N
00000034	02	00000002	\N	D	\N	2012-11-11	1	\N
00000034	02	00000002	\N	D	\N	2012-11-12	1	\N
00000034	02	00000002	\N	D	\N	2012-11-13	1	\N
00000034	02	00000002	\N	D	\N	2012-11-14	1	\N
00000034	02	00000002	\N	F	\N	2012-11-17	1	\N
00000034	02	00000002	\N	D	\N	2012-11-18	1	\N
00000034	02	00000002	\N	D	\N	2012-11-19	1	\N
00000034	02	00000002	\N	D	\N	2012-11-20	1	\N
00000034	02	00000002	\N	D	\N	2012-11-21	1	\N
00000034	02	00000002	\N	D	\N	2012-11-22	1	\N
00000034	02	00000002	\N	F	\N	2012-11-23	1	\N
00000034	02	00000002	\N	F	\N	2012-11-24	1	\N
00000034	02	00000002	\N	D	\N	2012-11-25	1	\N
00000034	02	00000002	\N	D	\N	2012-11-26	1	\N
00000034	02	00000002	\N	D	\N	2012-11-27	1	\N
00000034	02	00000002	\N	D	\N	2012-11-28	1	\N
00000034	02	00000002	\N	D	\N	2012-11-29	1	\N
00000034	02	00000002	\N	F	\N	2012-11-30	1	\N
00000035	02	00000002	\N	D	\N	2012-11-01	1	\N
00000035	02	00000002	\N	F	\N	2012-11-02	1	\N
00000035	02	00000002	\N	F	\N	2012-11-03	1	\N
00000035	02	00000002	\N	D	\N	2012-11-04	1	\N
00000035	02	00000002	\N	D	\N	2012-11-05	1	\N
00000035	02	00000002	\N	D	\N	2012-11-06	1	\N
00000035	02	00000002	\N	D	\N	2012-11-07	1	\N
00000035	02	00000002	\N	D	\N	2012-11-08	1	\N
00000035	02	00000002	\N	F	\N	2012-11-09	1	\N
00000035	02	00000002	\N	F	\N	2012-11-10	1	\N
00000035	02	00000002	\N	D	\N	2012-11-11	1	\N
00000035	02	00000002	\N	D	\N	2012-11-12	1	\N
00000035	02	00000002	\N	D	\N	2012-11-13	1	\N
00000035	02	00000002	\N	D	\N	2012-11-14	1	\N
00000035	02	00000002	\N	F	\N	2012-11-17	1	\N
00000035	02	00000002	\N	D	\N	2012-11-18	1	\N
00000035	02	00000002	\N	D	\N	2012-11-19	1	\N
00000035	02	00000002	\N	D	\N	2012-11-20	1	\N
00000035	02	00000002	\N	D	\N	2012-11-21	1	\N
00000035	02	00000002	\N	D	\N	2012-11-22	1	\N
00000035	02	00000002	\N	F	\N	2012-11-23	1	\N
00000035	02	00000002	\N	F	\N	2012-11-24	1	\N
00000035	02	00000002	\N	D	\N	2012-11-25	1	\N
00000035	02	00000002	\N	D	\N	2012-11-26	1	\N
00000035	02	00000002	\N	D	\N	2012-11-27	1	\N
00000035	02	00000002	\N	D	\N	2012-11-28	1	\N
00000035	02	00000002	\N	D	\N	2012-11-29	1	\N
00000035	02	00000002	\N	F	\N	2012-11-30	1	\N
00000036	02	00000002	\N	D	\N	2012-11-01	1	\N
00000036	02	00000002	\N	F	\N	2012-11-02	1	\N
00000036	02	00000002	\N	F	\N	2012-11-03	1	\N
00000036	02	00000002	\N	D	\N	2012-11-04	1	\N
00000036	02	00000002	\N	D	\N	2012-11-05	1	\N
00000036	02	00000002	\N	D	\N	2012-11-06	1	\N
00000036	02	00000002	\N	D	\N	2012-11-07	1	\N
00000036	02	00000002	\N	D	\N	2012-11-08	1	\N
00000036	02	00000002	\N	F	\N	2012-11-09	1	\N
00000036	02	00000002	\N	F	\N	2012-11-10	1	\N
00000036	02	00000002	\N	D	\N	2012-11-11	1	\N
00000036	02	00000002	\N	D	\N	2012-11-12	1	\N
00000036	02	00000002	\N	D	\N	2012-11-13	1	\N
00000036	02	00000002	\N	D	\N	2012-11-14	1	\N
00000036	02	00000002	\N	F	\N	2012-11-17	1	\N
00000036	02	00000002	\N	D	\N	2012-11-18	1	\N
00000036	02	00000002	\N	D	\N	2012-11-19	1	\N
00000036	02	00000002	\N	D	\N	2012-11-20	1	\N
00000036	02	00000002	\N	D	\N	2012-11-21	1	\N
00000036	02	00000002	\N	D	\N	2012-11-22	1	\N
00000036	02	00000002	\N	F	\N	2012-11-23	1	\N
00000036	02	00000002	\N	F	\N	2012-11-24	1	\N
00000036	02	00000002	\N	D	\N	2012-11-25	1	\N
00000036	02	00000002	\N	D	\N	2012-11-26	1	\N
00000036	02	00000002	\N	D	\N	2012-11-27	1	\N
00000036	02	00000002	\N	D	\N	2012-11-28	1	\N
00000036	02	00000002	\N	D	\N	2012-11-29	1	\N
00000036	02	00000002	\N	F	\N	2012-11-30	1	\N
00000037	02	00000002	\N	D	\N	2012-11-01	1	\N
00000037	02	00000002	\N	F	\N	2012-11-02	1	\N
00000037	02	00000002	\N	F	\N	2012-11-03	1	\N
00000037	02	00000002	\N	D	\N	2012-11-04	1	\N
00000037	02	00000002	\N	D	\N	2012-11-05	1	\N
00000037	02	00000002	\N	D	\N	2012-11-06	1	\N
00000037	02	00000002	\N	D	\N	2012-11-07	1	\N
00000037	02	00000002	\N	D	\N	2012-11-08	1	\N
00000033	02	00000002	\N	D	\N	2012-11-15	1	\N
00000033	02	00000002	\N	D	\N	2012-11-16	1	\N
00000037	02	00000002	\N	D	\N	2012-11-20	1	\N
00000037	02	00000002	\N	D	\N	2012-11-21	1	\N
00000037	02	00000002	\N	D	\N	2012-11-22	1	\N
00000037	02	00000002	\N	F	\N	2012-11-23	1	\N
00000037	02	00000002	\N	F	\N	2012-11-24	1	\N
00000037	02	00000002	\N	D	\N	2012-11-25	1	\N
00000037	02	00000002	\N	D	\N	2012-11-26	1	\N
00000037	02	00000002	\N	D	\N	2012-11-27	1	\N
00000037	02	00000002	\N	D	\N	2012-11-28	1	\N
00000037	02	00000002	\N	D	\N	2012-11-29	1	\N
00000037	02	00000002	\N	F	\N	2012-11-30	1	\N
00000038	02	00000002	\N	D	\N	2012-11-01	1	\N
00000038	02	00000002	\N	F	\N	2012-11-02	1	\N
00000038	02	00000002	\N	F	\N	2012-11-03	1	\N
00000038	02	00000002	\N	D	\N	2012-11-04	1	\N
00000038	02	00000002	\N	D	\N	2012-11-05	1	\N
00000038	02	00000002	\N	D	\N	2012-11-06	1	\N
00000038	02	00000002	\N	D	\N	2012-11-07	1	\N
00000038	02	00000002	\N	D	\N	2012-11-08	1	\N
00000038	02	00000002	\N	F	\N	2012-11-09	1	\N
00000038	02	00000002	\N	F	\N	2012-11-10	1	\N
00000038	02	00000002	\N	D	\N	2012-11-11	1	\N
00000038	02	00000002	\N	D	\N	2012-11-12	1	\N
00000038	02	00000002	\N	D	\N	2012-11-13	1	\N
00000038	02	00000002	\N	D	\N	2012-11-14	1	\N
00000038	02	00000002	\N	F	\N	2012-11-17	1	\N
00000038	02	00000002	\N	D	\N	2012-11-18	1	\N
00000038	02	00000002	\N	D	\N	2012-11-19	1	\N
00000038	02	00000002	\N	D	\N	2012-11-20	1	\N
00000038	02	00000002	\N	D	\N	2012-11-21	1	\N
00000038	02	00000002	\N	D	\N	2012-11-22	1	\N
00000038	02	00000002	\N	F	\N	2012-11-23	1	\N
00000038	02	00000002	\N	F	\N	2012-11-24	1	\N
00000038	02	00000002	\N	D	\N	2012-11-25	1	\N
00000038	02	00000002	\N	D	\N	2012-11-26	1	\N
00000038	02	00000002	\N	D	\N	2012-11-27	1	\N
00000038	02	00000002	\N	D	\N	2012-11-28	1	\N
00000038	02	00000002	\N	D	\N	2012-11-29	1	\N
00000038	02	00000002	\N	F	\N	2012-11-30	1	\N
00000039	02	00000002	\N	D	\N	2012-11-01	1	\N
00000039	02	00000002	\N	F	\N	2012-11-02	1	\N
00000039	02	00000002	\N	F	\N	2012-11-03	1	\N
00000039	02	00000002	\N	D	\N	2012-11-04	1	\N
00000039	02	00000002	\N	D	\N	2012-11-05	1	\N
00000039	02	00000002	\N	D	\N	2012-11-06	1	\N
00000039	02	00000002	\N	D	\N	2012-11-07	1	\N
00000039	02	00000002	\N	D	\N	2012-11-08	1	\N
00000039	02	00000002	\N	F	\N	2012-11-09	1	\N
00000039	02	00000002	\N	F	\N	2012-11-10	1	\N
00000039	02	00000002	\N	D	\N	2012-11-11	1	\N
00000039	02	00000002	\N	D	\N	2012-11-12	1	\N
00000039	02	00000002	\N	D	\N	2012-11-13	1	\N
00000039	02	00000002	\N	D	\N	2012-11-14	1	\N
00000039	02	00000002	\N	F	\N	2012-11-17	1	\N
00000039	02	00000002	\N	D	\N	2012-11-18	1	\N
00000039	02	00000002	\N	D	\N	2012-11-19	1	\N
00000039	02	00000002	\N	D	\N	2012-11-20	1	\N
00000039	02	00000002	\N	D	\N	2012-11-21	1	\N
00000039	02	00000002	\N	D	\N	2012-11-22	1	\N
00000039	02	00000002	\N	F	\N	2012-11-23	1	\N
00000039	02	00000002	\N	F	\N	2012-11-24	1	\N
00000039	02	00000002	\N	D	\N	2012-11-25	1	\N
00000039	02	00000002	\N	D	\N	2012-11-26	1	\N
00000039	02	00000002	\N	D	\N	2012-11-27	1	\N
00000039	02	00000002	\N	D	\N	2012-11-28	1	\N
00000039	02	00000002	\N	D	\N	2012-11-29	1	\N
00000039	02	00000002	\N	F	\N	2012-11-30	1	\N
00000040	02	00000002	\N	D	\N	2012-11-01	1	\N
00000040	02	00000002	\N	F	\N	2012-11-02	1	\N
00000040	02	00000002	\N	F	\N	2012-11-03	1	\N
00000040	02	00000002	\N	D	\N	2012-11-04	1	\N
00000040	02	00000002	\N	D	\N	2012-11-05	1	\N
00000040	02	00000002	\N	D	\N	2012-11-06	1	\N
00000040	02	00000002	\N	D	\N	2012-11-07	1	\N
00000040	02	00000002	\N	D	\N	2012-11-08	1	\N
00000040	02	00000002	\N	F	\N	2012-11-09	1	\N
00000040	02	00000002	\N	F	\N	2012-11-10	1	\N
00000040	02	00000002	\N	D	\N	2012-11-11	1	\N
00000040	02	00000002	\N	D	\N	2012-11-12	1	\N
00000040	02	00000002	\N	D	\N	2012-11-13	1	\N
00000040	02	00000002	\N	D	\N	2012-11-14	1	\N
00000040	02	00000002	\N	F	\N	2012-11-17	1	\N
00000040	02	00000002	\N	D	\N	2012-11-18	1	\N
00000040	02	00000002	\N	D	\N	2012-11-19	1	\N
00000040	02	00000002	\N	D	\N	2012-11-20	1	\N
00000040	02	00000002	\N	D	\N	2012-11-21	1	\N
00000040	02	00000002	\N	D	\N	2012-11-22	1	\N
00000040	02	00000002	\N	F	\N	2012-11-23	1	\N
00000040	02	00000002	\N	F	\N	2012-11-24	1	\N
00000040	02	00000002	\N	D	\N	2012-11-25	1	\N
00000040	02	00000002	\N	D	\N	2012-11-26	1	\N
00000040	02	00000002	\N	D	\N	2012-11-27	1	\N
00000040	02	00000002	\N	D	\N	2012-11-28	1	\N
00000040	02	00000002	\N	D	\N	2012-11-29	1	\N
00000040	02	00000002	\N	F	\N	2012-11-30	1	\N
00000041	02	00000002	\N	D	\N	2012-11-01	1	\N
00000041	02	00000002	\N	F	\N	2012-11-02	1	\N
00000041	02	00000002	\N	F	\N	2012-11-03	1	\N
00000041	02	00000002	\N	D	\N	2012-11-04	1	\N
00000041	02	00000002	\N	D	\N	2012-11-05	1	\N
00000041	02	00000002	\N	D	\N	2012-11-06	1	\N
00000041	02	00000002	\N	D	\N	2012-11-07	1	\N
00000041	02	00000002	\N	D	\N	2012-11-08	1	\N
00000041	02	00000002	\N	F	\N	2012-11-09	1	\N
00000041	02	00000002	\N	F	\N	2012-11-10	1	\N
00000041	02	00000002	\N	D	\N	2012-11-11	1	\N
00000041	02	00000002	\N	D	\N	2012-11-12	1	\N
00000041	02	00000002	\N	D	\N	2012-11-13	1	\N
00000041	02	00000002	\N	D	\N	2012-11-14	1	\N
00000041	02	00000002	\N	D	\N	2012-11-21	1	\N
00000041	02	00000002	\N	D	\N	2012-11-22	1	\N
00000041	02	00000002	\N	F	\N	2012-11-23	1	\N
00000041	02	00000002	\N	F	\N	2012-11-24	1	\N
00000041	02	00000002	\N	D	\N	2012-11-25	1	\N
00000041	02	00000002	\N	D	\N	2012-11-26	1	\N
00000041	02	00000002	\N	D	\N	2012-11-27	1	\N
00000041	02	00000002	\N	D	\N	2012-11-28	1	\N
00000041	02	00000002	\N	D	\N	2012-11-29	1	\N
00000041	02	00000002	\N	F	\N	2012-11-30	1	\N
00000041	02	00000002	\N	D	\N	2012-11-16	1	\N
00000001	01	00000002	\N	F	0000000019	2012-11-30	R	\N
00000042	01	00000003	\N	F	0000000020	2012-12-29	R	\N
00000042	01	00000003	\N	D	0000000021	2012-12-02	R	\N
00000013	05	00000002	\N	D	0000000022	2012-12-05	R	\N
00000052	03	00000003	\N	F	0000000023	2012-12-01	R	\N
00000052	03	00000003	\N	D	0000000023	2012-12-02	R	\N
00000052	03	00000003	\N	D	0000000023	2012-12-03	R	\N
00000052	03	00000003	\N	D	0000000023	2012-12-04	R	\N
00000009	04	00000002	\N	D	0000000025	2012-12-26	R	\N
00000042	01	00000003	\N	D	0000000026	2012-11-27	R	\N
00000042	01	00000003	\N	D	0000000026	2012-11-28	R	\N
00000001	01	00000002	\N	D	0000000027	2012-12-06	R	\N
00000001	01	00000002	\N	D	0000000028	2013-01-08	R	\N
00000001	01	00000002	\N	D	0000000028	2013-01-09	R	\N
00000077	06	00000002	\N	F	\N	2012-12-01	1	\N
00000077	06	00000002	\N	D	\N	2012-12-02	1	\N
00000077	06	00000002	\N	D	\N	2012-12-03	1	\N
00000077	06	00000002	\N	D	\N	2012-12-04	1	\N
00000077	06	00000002	\N	D	\N	2012-12-05	1	\N
00000077	06	00000002	\N	D	\N	2012-12-06	1	\N
00000077	06	00000002	\N	F	\N	2012-12-07	1	\N
00000077	06	00000002	\N	F	\N	2012-12-08	1	\N
00000077	06	00000002	\N	D	\N	2012-12-09	1	\N
00000077	06	00000002	\N	D	\N	2012-12-10	1	\N
00000077	06	00000002	\N	D	\N	2012-12-11	1	\N
00000077	06	00000002	\N	D	\N	2012-12-12	1	\N
00000077	06	00000002	\N	D	\N	2012-12-13	1	\N
00000077	06	00000002	\N	F	\N	2012-12-14	1	\N
00000077	06	00000002	\N	F	\N	2012-12-15	1	\N
00000077	06	00000002	\N	D	\N	2012-12-16	1	\N
00000077	06	00000002	\N	D	\N	2012-12-17	1	\N
00000077	06	00000002	\N	D	\N	2012-12-18	1	\N
00000077	06	00000002	\N	D	\N	2012-12-19	1	\N
00000077	06	00000002	\N	D	\N	2012-12-20	1	\N
00000077	06	00000002	\N	F	\N	2012-12-21	1	\N
00000077	06	00000002	\N	F	\N	2012-12-22	1	\N
00000077	06	00000002	\N	D	\N	2012-12-23	1	\N
00000077	06	00000002	\N	D	\N	2012-12-24	1	\N
00000077	06	00000002	\N	D	\N	2012-12-25	1	\N
00000077	06	00000002	\N	D	\N	2012-12-26	1	\N
00000077	06	00000002	\N	D	\N	2012-12-27	1	\N
00000077	06	00000002	\N	F	\N	2012-12-28	1	\N
00000077	06	00000002	\N	F	\N	2012-12-29	1	\N
00000077	06	00000002	\N	D	\N	2012-12-30	1	\N
00000077	06	00000002	\N	D	\N	2012-12-31	1	\N
00000078	06	00000002	\N	F	\N	2012-12-01	1	\N
00000078	06	00000002	\N	D	\N	2012-12-02	1	\N
00000078	06	00000002	\N	D	\N	2012-12-03	1	\N
00000078	06	00000002	\N	D	\N	2012-12-04	1	\N
00000078	06	00000002	\N	D	\N	2012-12-05	1	\N
00000078	06	00000002	\N	D	\N	2012-12-06	1	\N
00000078	06	00000002	\N	F	\N	2012-12-07	1	\N
00000078	06	00000002	\N	F	\N	2012-12-08	1	\N
00000078	06	00000002	\N	D	\N	2012-12-09	1	\N
00000078	06	00000002	\N	D	\N	2012-12-10	1	\N
00000078	06	00000002	\N	D	\N	2012-12-11	1	\N
00000078	06	00000002	\N	D	\N	2012-12-12	1	\N
00000078	06	00000002	\N	D	\N	2012-12-13	1	\N
00000078	06	00000002	\N	F	\N	2012-12-14	1	\N
00000078	06	00000002	\N	F	\N	2012-12-15	1	\N
00000078	06	00000002	\N	D	\N	2012-12-16	1	\N
00000078	06	00000002	\N	D	\N	2012-12-17	1	\N
00000078	06	00000002	\N	D	\N	2012-12-18	1	\N
00000078	06	00000002	\N	D	\N	2012-12-19	1	\N
00000078	06	00000002	\N	D	\N	2012-12-20	1	\N
00000078	06	00000002	\N	F	\N	2012-12-21	1	\N
00000078	06	00000002	\N	F	\N	2012-12-22	1	\N
00000078	06	00000002	\N	D	\N	2012-12-23	1	\N
00000078	06	00000002	\N	D	\N	2012-12-24	1	\N
00000078	06	00000002	\N	D	\N	2012-12-25	1	\N
00000078	06	00000002	\N	D	\N	2012-12-26	1	\N
00000078	06	00000002	\N	D	\N	2012-12-27	1	\N
00000078	06	00000002	\N	F	\N	2012-12-28	1	\N
00000078	06	00000002	\N	F	\N	2012-12-29	1	\N
00000078	06	00000002	\N	D	\N	2012-12-30	1	\N
00000078	06	00000002	\N	D	\N	2012-12-31	1	\N
00000079	06	00000002	\N	F	\N	2012-12-01	1	\N
00000079	06	00000002	\N	D	\N	2012-12-02	1	\N
00000079	06	00000002	\N	D	\N	2012-12-03	1	\N
00000079	06	00000002	\N	D	\N	2012-12-04	1	\N
00000079	06	00000002	\N	D	\N	2012-12-05	1	\N
00000079	06	00000002	\N	D	\N	2012-12-06	1	\N
00000079	06	00000002	\N	F	\N	2012-12-07	1	\N
00000079	06	00000002	\N	F	\N	2012-12-08	1	\N
00000079	06	00000002	\N	D	\N	2012-12-09	1	\N
00000079	06	00000002	\N	D	\N	2012-12-10	1	\N
00000079	06	00000002	\N	D	\N	2012-12-11	1	\N
00000079	06	00000002	\N	D	\N	2012-12-12	1	\N
00000079	06	00000002	\N	D	\N	2012-12-13	1	\N
00000079	06	00000002	\N	F	\N	2012-12-14	1	\N
00000079	06	00000002	\N	F	\N	2012-12-15	1	\N
00000079	06	00000002	\N	D	\N	2012-12-16	1	\N
00000079	06	00000002	\N	D	\N	2012-12-17	1	\N
00000079	06	00000002	\N	D	\N	2012-12-18	1	\N
00000079	06	00000002	\N	D	\N	2012-12-19	1	\N
00000079	06	00000002	\N	D	\N	2012-12-20	1	\N
00000079	06	00000002	\N	F	\N	2012-12-21	1	\N
00000079	06	00000002	\N	F	\N	2012-12-22	1	\N
00000079	06	00000002	\N	D	\N	2012-12-23	1	\N
00000079	06	00000002	\N	D	\N	2012-12-24	1	\N
00000079	06	00000002	\N	D	\N	2012-12-25	1	\N
00000079	06	00000002	\N	D	\N	2012-12-26	1	\N
00000079	06	00000002	\N	D	\N	2012-12-27	1	\N
00000079	06	00000002	\N	F	\N	2012-12-28	1	\N
00000079	06	00000002	\N	F	\N	2012-12-29	1	\N
00000079	06	00000002	\N	D	\N	2012-12-30	1	\N
00000079	06	00000002	\N	D	\N	2012-12-31	1	\N
00000080	06	00000002	\N	F	\N	2012-12-01	1	\N
00000080	06	00000002	\N	D	\N	2012-12-02	1	\N
00000080	06	00000002	\N	D	\N	2012-12-03	1	\N
00000080	06	00000002	\N	D	\N	2012-12-04	1	\N
00000080	06	00000002	\N	D	\N	2012-12-05	1	\N
00000080	06	00000002	\N	D	\N	2012-12-06	1	\N
00000080	06	00000002	\N	F	\N	2012-12-07	1	\N
00000080	06	00000002	\N	F	\N	2012-12-08	1	\N
00000080	06	00000002	\N	D	\N	2012-12-09	1	\N
00000080	06	00000002	\N	D	\N	2012-12-10	1	\N
00000080	06	00000002	\N	D	\N	2012-12-11	1	\N
00000080	06	00000002	\N	D	\N	2012-12-12	1	\N
00000080	06	00000002	\N	D	\N	2012-12-13	1	\N
00000080	06	00000002	\N	F	\N	2012-12-14	1	\N
00000080	06	00000002	\N	F	\N	2012-12-15	1	\N
00000080	06	00000002	\N	D	\N	2012-12-16	1	\N
00000080	06	00000002	\N	D	\N	2012-12-17	1	\N
00000080	06	00000002	\N	D	\N	2012-12-18	1	\N
00000080	06	00000002	\N	D	\N	2012-12-19	1	\N
00000080	06	00000002	\N	D	\N	2012-12-20	1	\N
00000080	06	00000002	\N	F	\N	2012-12-21	1	\N
00000080	06	00000002	\N	F	\N	2012-12-22	1	\N
00000080	06	00000002	\N	D	\N	2012-12-23	1	\N
00000080	06	00000002	\N	D	\N	2012-12-24	1	\N
00000080	06	00000002	\N	D	\N	2012-12-25	1	\N
00000080	06	00000002	\N	D	\N	2012-12-26	1	\N
00000080	06	00000002	\N	D	\N	2012-12-27	1	\N
00000080	06	00000002	\N	F	\N	2012-12-28	1	\N
00000080	06	00000002	\N	F	\N	2012-12-29	1	\N
00000080	06	00000002	\N	D	\N	2012-12-30	1	\N
00000080	06	00000002	\N	D	\N	2012-12-31	1	\N
00000081	06	00000003	\N	F	\N	2012-12-01	1	\N
00000081	06	00000003	\N	D	\N	2012-12-02	1	\N
00000081	06	00000003	\N	D	\N	2012-12-03	1	\N
00000081	06	00000003	\N	D	\N	2012-12-04	1	\N
00000081	06	00000003	\N	D	\N	2012-12-05	1	\N
00000081	06	00000003	\N	D	\N	2012-12-06	1	\N
00000081	06	00000003	\N	F	\N	2012-12-07	1	\N
00000081	06	00000003	\N	F	\N	2012-12-08	1	\N
00000081	06	00000003	\N	D	\N	2012-12-09	1	\N
00000081	06	00000003	\N	D	\N	2012-12-10	1	\N
00000081	06	00000003	\N	D	\N	2012-12-11	1	\N
00000081	06	00000003	\N	D	\N	2012-12-12	1	\N
00000081	06	00000003	\N	D	\N	2012-12-13	1	\N
00000081	06	00000003	\N	F	\N	2012-12-14	1	\N
00000081	06	00000003	\N	F	\N	2012-12-15	1	\N
00000081	06	00000003	\N	D	\N	2012-12-16	1	\N
00000081	06	00000003	\N	D	\N	2012-12-17	1	\N
00000081	06	00000003	\N	D	\N	2012-12-18	1	\N
00000081	06	00000003	\N	D	\N	2012-12-19	1	\N
00000081	06	00000003	\N	D	\N	2012-12-20	1	\N
00000081	06	00000003	\N	F	\N	2012-12-21	1	\N
00000081	06	00000003	\N	F	\N	2012-12-22	1	\N
00000081	06	00000003	\N	D	\N	2012-12-23	1	\N
00000081	06	00000003	\N	D	\N	2012-12-24	1	\N
00000081	06	00000003	\N	D	\N	2012-12-25	1	\N
00000081	06	00000003	\N	D	\N	2012-12-26	1	\N
00000081	06	00000003	\N	D	\N	2012-12-27	1	\N
00000081	06	00000003	\N	F	\N	2012-12-28	1	\N
00000081	06	00000003	\N	F	\N	2012-12-29	1	\N
00000081	06	00000003	\N	D	\N	2012-12-30	1	\N
00000081	06	00000003	\N	D	\N	2012-12-31	1	\N
00000001	01	00000002	\N	D	0000000029	2012-12-20	R	\N
00000053	04	00000003	\N	D	0000000030	2012-12-12	R	\N
00000077	06	00000002	\N	D	\N	2013-01-01	1	\N
00000077	06	00000002	\N	D	\N	2013-01-02	1	\N
00000077	06	00000002	\N	D	\N	2013-01-03	1	\N
00000077	06	00000002	\N	F	\N	2013-01-04	1	\N
00000077	06	00000002	\N	F	\N	2013-01-05	1	\N
00000077	06	00000002	\N	D	\N	2013-01-06	1	\N
00000077	06	00000002	\N	D	\N	2013-01-07	1	\N
00000077	06	00000002	\N	D	\N	2013-01-08	1	\N
00000077	06	00000002	\N	D	\N	2013-01-09	1	\N
00000077	06	00000002	\N	D	\N	2013-01-10	1	\N
00000077	06	00000002	\N	F	\N	2013-01-11	1	\N
00000077	06	00000002	\N	F	\N	2013-01-12	1	\N
00000077	06	00000002	\N	D	\N	2013-01-13	1	\N
00000077	06	00000002	\N	D	\N	2013-01-14	1	\N
00000077	06	00000002	\N	D	\N	2013-01-15	1	\N
00000077	06	00000002	\N	D	\N	2013-01-16	1	\N
00000077	06	00000002	\N	D	\N	2013-01-17	1	\N
00000077	06	00000002	\N	F	\N	2013-01-18	1	\N
00000077	06	00000002	\N	F	\N	2013-01-19	1	\N
00000077	06	00000002	\N	D	\N	2013-01-20	1	\N
00000077	06	00000002	\N	D	\N	2013-01-21	1	\N
00000077	06	00000002	\N	D	\N	2013-01-22	1	\N
00000077	06	00000002	\N	D	\N	2013-01-23	1	\N
00000077	06	00000002	\N	D	\N	2013-01-24	1	\N
00000077	06	00000002	\N	F	\N	2013-01-25	1	\N
00000077	06	00000002	\N	F	\N	2013-01-26	1	\N
00000077	06	00000002	\N	D	\N	2013-01-27	1	\N
00000077	06	00000002	\N	D	\N	2013-01-28	1	\N
00000077	06	00000002	\N	D	\N	2013-01-29	1	\N
00000077	06	00000002	\N	D	\N	2013-01-30	1	\N
00000077	06	00000002	\N	D	\N	2013-01-31	1	\N
00000078	06	00000002	\N	D	\N	2013-01-01	1	\N
00000078	06	00000002	\N	D	\N	2013-01-02	1	\N
00000078	06	00000002	\N	D	\N	2013-01-03	1	\N
00000078	06	00000002	\N	F	\N	2013-01-04	1	\N
00000078	06	00000002	\N	F	\N	2013-01-05	1	\N
00000078	06	00000002	\N	D	\N	2013-01-06	1	\N
00000078	06	00000002	\N	D	\N	2013-01-07	1	\N
00000078	06	00000002	\N	D	\N	2013-01-08	1	\N
00000078	06	00000002	\N	D	\N	2013-01-09	1	\N
00000078	06	00000002	\N	D	\N	2013-01-10	1	\N
00000078	06	00000002	\N	F	\N	2013-01-11	1	\N
00000078	06	00000002	\N	F	\N	2013-01-12	1	\N
00000078	06	00000002	\N	D	\N	2013-01-13	1	\N
00000078	06	00000002	\N	D	\N	2013-01-14	1	\N
00000078	06	00000002	\N	D	\N	2013-01-15	1	\N
00000078	06	00000002	\N	D	\N	2013-01-16	1	\N
00000078	06	00000002	\N	D	\N	2013-01-17	1	\N
00000078	06	00000002	\N	F	\N	2013-01-18	1	\N
00000078	06	00000002	\N	F	\N	2013-01-19	1	\N
00000078	06	00000002	\N	D	\N	2013-01-20	1	\N
00000078	06	00000002	\N	D	\N	2013-01-21	1	\N
00000078	06	00000002	\N	D	\N	2013-01-22	1	\N
00000078	06	00000002	\N	D	\N	2013-01-23	1	\N
00000078	06	00000002	\N	D	\N	2013-01-24	1	\N
00000078	06	00000002	\N	F	\N	2013-01-25	1	\N
00000078	06	00000002	\N	F	\N	2013-01-26	1	\N
00000078	06	00000002	\N	D	\N	2013-01-27	1	\N
00000078	06	00000002	\N	D	\N	2013-01-28	1	\N
00000078	06	00000002	\N	D	\N	2013-01-29	1	\N
00000078	06	00000002	\N	D	\N	2013-01-30	1	\N
00000078	06	00000002	\N	D	\N	2013-01-31	1	\N
00000079	06	00000002	\N	D	\N	2013-01-01	1	\N
00000079	06	00000002	\N	D	\N	2013-01-02	1	\N
00000079	06	00000002	\N	D	\N	2013-01-03	1	\N
00000079	06	00000002	\N	F	\N	2013-01-04	1	\N
00000079	06	00000002	\N	F	\N	2013-01-05	1	\N
00000079	06	00000002	\N	D	\N	2013-01-06	1	\N
00000079	06	00000002	\N	D	\N	2013-01-07	1	\N
00000079	06	00000002	\N	D	\N	2013-01-08	1	\N
00000079	06	00000002	\N	D	\N	2013-01-09	1	\N
00000079	06	00000002	\N	D	\N	2013-01-10	1	\N
00000079	06	00000002	\N	F	\N	2013-01-11	1	\N
00000079	06	00000002	\N	F	\N	2013-01-12	1	\N
00000079	06	00000002	\N	D	\N	2013-01-13	1	\N
00000079	06	00000002	\N	D	\N	2013-01-14	1	\N
00000079	06	00000002	\N	D	\N	2013-01-15	1	\N
00000079	06	00000002	\N	D	\N	2013-01-16	1	\N
00000079	06	00000002	\N	D	\N	2013-01-17	1	\N
00000079	06	00000002	\N	F	\N	2013-01-18	1	\N
00000079	06	00000002	\N	F	\N	2013-01-19	1	\N
00000079	06	00000002	\N	D	\N	2013-01-20	1	\N
00000079	06	00000002	\N	D	\N	2013-01-21	1	\N
00000079	06	00000002	\N	D	\N	2013-01-22	1	\N
00000079	06	00000002	\N	D	\N	2013-01-23	1	\N
00000079	06	00000002	\N	D	\N	2013-01-24	1	\N
00000079	06	00000002	\N	F	\N	2013-01-25	1	\N
00000079	06	00000002	\N	F	\N	2013-01-26	1	\N
00000079	06	00000002	\N	D	\N	2013-01-27	1	\N
00000079	06	00000002	\N	D	\N	2013-01-28	1	\N
00000079	06	00000002	\N	D	\N	2013-01-29	1	\N
00000079	06	00000002	\N	D	\N	2013-01-30	1	\N
00000079	06	00000002	\N	D	\N	2013-01-31	1	\N
00000080	06	00000002	\N	D	\N	2013-01-01	1	\N
00000080	06	00000002	\N	D	\N	2013-01-02	1	\N
00000080	06	00000002	\N	D	\N	2013-01-03	1	\N
00000080	06	00000002	\N	F	\N	2013-01-04	1	\N
00000080	06	00000002	\N	F	\N	2013-01-05	1	\N
00000080	06	00000002	\N	D	\N	2013-01-06	1	\N
00000080	06	00000002	\N	D	\N	2013-01-07	1	\N
00000080	06	00000002	\N	D	\N	2013-01-08	1	\N
00000080	06	00000002	\N	D	\N	2013-01-09	1	\N
00000080	06	00000002	\N	D	\N	2013-01-10	1	\N
00000080	06	00000002	\N	F	\N	2013-01-11	1	\N
00000080	06	00000002	\N	F	\N	2013-01-12	1	\N
00000080	06	00000002	\N	D	\N	2013-01-13	1	\N
00000080	06	00000002	\N	D	\N	2013-01-14	1	\N
00000080	06	00000002	\N	D	\N	2013-01-15	1	\N
00000080	06	00000002	\N	D	\N	2013-01-16	1	\N
00000080	06	00000002	\N	D	\N	2013-01-17	1	\N
00000080	06	00000002	\N	F	\N	2013-01-18	1	\N
00000080	06	00000002	\N	F	\N	2013-01-19	1	\N
00000080	06	00000002	\N	D	\N	2013-01-20	1	\N
00000080	06	00000002	\N	D	\N	2013-01-21	1	\N
00000080	06	00000002	\N	D	\N	2013-01-22	1	\N
00000080	06	00000002	\N	D	\N	2013-01-23	1	\N
00000080	06	00000002	\N	D	\N	2013-01-24	1	\N
00000080	06	00000002	\N	F	\N	2013-01-25	1	\N
00000080	06	00000002	\N	F	\N	2013-01-26	1	\N
00000080	06	00000002	\N	D	\N	2013-01-27	1	\N
00000080	06	00000002	\N	D	\N	2013-01-28	1	\N
00000080	06	00000002	\N	D	\N	2013-01-29	1	\N
00000080	06	00000002	\N	D	\N	2013-01-30	1	\N
00000080	06	00000002	\N	D	\N	2013-01-31	1	\N
00000003	01	00000002	\N	D	\N	2013-01-01	1	\N
00000004	01	00000002	\N	D	\N	2013-01-01	1	\N
00000005	01	00000002	\N	D	\N	2013-01-01	1	\N
00000003	01	00000002	\N	D	\N	2012-12-31	1	\N
00000004	01	00000002	\N	D	\N	2012-12-31	1	\N
00000005	01	00000002	\N	D	\N	2012-12-31	1	\N
00000006	01	00000002	\N	D	\N	2012-12-31	1	\N
00000007	01	00000002	\N	D	\N	2012-12-31	1	\N
00000018	02	00000002	\N	D	\N	2012-12-20	1	\N
00000018	02	00000002	\N	D	\N	2012-12-21	1	\N
00000018	02	00000002	\N	D	\N	2012-12-19	1	\N
00000020	02	00000002	\N	D	\N	2012-12-20	1	\N
00000020	02	00000002	\N	D	\N	2012-12-21	1	\N
00000021	02	00000002	\N	D	\N	2012-12-19	1	\N
00000021	02	00000002	\N	D	\N	2012-12-20	1	\N
00000021	02	00000002	\N	D	\N	2012-12-21	1	\N
00000028	02	00000002	\N	D	\N	2012-12-19	1	\N
00000028	02	00000002	\N	D	\N	2012-12-20	1	\N
00000028	02	00000002	\N	D	\N	2012-12-21	1	\N
00000029	02	00000002	\N	D	\N	2012-12-19	1	\N
00000029	02	00000002	\N	D	\N	2012-12-20	1	\N
00000029	02	00000002	\N	D	\N	2012-12-21	1	\N
00000030	02	00000002	\N	D	\N	2012-12-19	1	\N
00000030	02	00000002	\N	D	\N	2012-12-20	1	\N
00000030	02	00000002	\N	D	\N	2012-12-21	1	\N
00000031	02	00000002	\N	D	\N	2012-12-19	1	\N
00000031	02	00000002	\N	D	\N	2012-12-20	1	\N
00000031	02	00000002	\N	D	\N	2012-12-21	1	\N
00000032	02	00000002	\N	D	\N	2012-12-19	1	\N
00000032	02	00000002	\N	D	\N	2012-12-20	1	\N
00000032	02	00000002	\N	D	\N	2012-12-21	1	\N
00000033	02	00000002	\N	D	\N	2012-12-19	1	\N
00000033	02	00000002	\N	D	\N	2012-12-20	1	\N
00000033	02	00000002	\N	D	\N	2012-12-21	1	\N
00000034	02	00000002	\N	D	\N	2012-12-19	1	\N
00000034	02	00000002	\N	D	\N	2012-12-20	1	\N
00000034	02	00000002	\N	D	\N	2012-12-21	1	\N
00000035	02	00000002	\N	D	\N	2012-12-19	1	\N
00000035	02	00000002	\N	D	\N	2012-12-20	1	\N
00000035	02	00000002	\N	D	\N	2012-12-21	1	\N
00000036	02	00000002	\N	D	\N	2012-12-19	1	\N
00000036	02	00000002	\N	D	\N	2012-12-20	1	\N
00000022	02	00000002	\N	D	\N	2012-12-19	1	\N
00000022	02	00000002	\N	D	\N	2012-12-20	1	\N
00000022	02	00000002	\N	D	\N	2012-12-21	1	\N
00000023	02	00000002	\N	D	\N	2012-12-19	1	\N
00000023	02	00000002	\N	D	\N	2012-12-20	1	\N
00000023	02	00000002	\N	D	\N	2012-12-21	1	\N
00000024	02	00000002	\N	D	\N	2012-12-19	1	\N
00000024	02	00000002	\N	D	\N	2012-12-20	1	\N
00000024	02	00000002	\N	D	\N	2012-12-21	1	\N
00000025	02	00000002	\N	D	\N	2012-12-19	1	\N
00000025	02	00000002	\N	D	\N	2012-12-20	1	\N
00000025	02	00000002	\N	D	\N	2012-12-21	1	\N
00000026	02	00000002	\N	D	\N	2012-12-19	1	\N
00000026	02	00000002	\N	D	\N	2012-12-20	1	\N
00000036	02	00000002	\N	D	\N	2012-12-21	1	\N
00000037	02	00000002	\N	D	\N	2012-12-19	1	\N
00000037	02	00000002	\N	D	\N	2012-12-20	1	\N
00000037	02	00000002	\N	D	\N	2012-12-21	1	\N
00000038	02	00000002	\N	D	\N	2012-12-19	1	\N
00000038	02	00000002	\N	D	\N	2012-12-20	1	\N
00000038	02	00000002	\N	D	\N	2012-12-21	1	\N
00000039	02	00000002	\N	D	\N	2012-12-19	1	\N
00000039	02	00000002	\N	D	\N	2012-12-20	1	\N
00000039	02	00000002	\N	D	\N	2012-12-21	1	\N
00000040	02	00000002	\N	D	\N	2012-12-19	1	\N
00000040	02	00000002	\N	D	\N	2012-12-20	1	\N
00000040	02	00000002	\N	D	\N	2012-12-21	1	\N
00000041	02	00000002	\N	D	\N	2012-12-19	1	\N
00000041	02	00000002	\N	D	\N	2012-12-20	1	\N
00000041	02	00000002	\N	D	\N	2012-12-21	1	\N
00000026	02	00000002	\N	D	\N	2012-12-21	1	\N
00000009	04	00000002	\N	D	0000000037	2013-01-10	R	\N
00000009	04	00000002	\N	F	0000000037	2013-01-11	R	\N
00000017	02	00000002	\N	D	0000000039	2013-01-23	R	\N
00000017	02	00000002	\N	F	0000000043	2013-01-11	R	\N
00000017	02	00000002	\N	F	0000000043	2013-01-12	R	\N
00000017	02	00000002	\N	D	0000000043	2013-01-13	R	\N
00000017	02	00000002	\N	D	0000000043	2013-01-14	R	\N
00000017	02	00000002	\N	D	0000000044	2013-01-06	R	\N
00000017	02	00000002	\N	D	0000000044	2013-01-07	R	\N
00000042	01	00000003	\N	D	\N	2013-01-01	1	\N
00000042	01	00000003	\N	D	\N	2013-01-02	1	\N
00000042	01	00000003	\N	F	\N	2013-01-04	1	\N
00000042	01	00000003	\N	F	\N	2013-01-05	1	\N
00000042	01	00000003	\N	D	\N	2013-01-06	1	\N
00000042	01	00000003	\N	D	\N	2013-01-07	1	\N
00000042	01	00000003	\N	D	\N	2013-01-08	1	\N
00000042	01	00000003	\N	D	\N	2013-01-10	1	\N
00000042	01	00000003	\N	F	\N	2013-01-11	1	\N
00000042	01	00000003	\N	F	\N	2013-01-12	1	\N
00000042	01	00000003	\N	D	\N	2013-01-13	1	\N
00000042	01	00000003	\N	D	\N	2013-01-14	1	\N
00000042	01	00000003	\N	D	\N	2013-01-15	1	\N
00000042	01	00000003	\N	D	\N	2013-01-16	1	\N
00000042	01	00000003	\N	D	\N	2013-01-17	1	\N
00000042	01	00000003	\N	F	\N	2013-01-18	1	\N
00000042	01	00000003	\N	F	\N	2013-01-19	1	\N
00000042	01	00000003	\N	D	\N	2013-01-20	1	\N
00000042	01	00000003	\N	D	\N	2013-01-21	1	\N
00000042	01	00000003	\N	D	\N	2013-01-22	1	\N
00000042	01	00000003	\N	D	\N	2013-01-23	1	\N
00000042	01	00000003	\N	D	\N	2013-01-24	1	\N
00000042	01	00000003	\N	F	\N	2013-01-25	1	\N
00000042	01	00000003	\N	F	\N	2013-01-26	1	\N
00000042	01	00000003	\N	D	\N	2013-01-27	1	\N
00000042	01	00000003	\N	D	\N	2013-01-28	1	\N
00000042	01	00000003	\N	D	\N	2013-01-29	1	\N
00000042	01	00000003	\N	D	\N	2013-01-30	1	\N
00000042	01	00000003	\N	D	\N	2013-01-31	1	\N
00000043	01	00000003	\N	D	\N	2013-01-01	1	\N
00000043	01	00000003	\N	D	\N	2013-01-02	1	\N
00000043	01	00000003	\N	D	\N	2013-01-03	1	\N
00000043	01	00000003	\N	F	\N	2013-01-04	1	\N
00000043	01	00000003	\N	F	\N	2013-01-05	1	\N
00000043	01	00000003	\N	D	\N	2013-01-06	1	\N
00000043	01	00000003	\N	D	\N	2013-01-07	1	\N
00000043	01	00000003	\N	D	\N	2013-01-08	1	\N
00000043	01	00000003	\N	F	\N	2013-01-12	1	\N
00000043	01	00000003	\N	D	\N	2013-01-13	1	\N
00000043	01	00000003	\N	D	\N	2013-01-14	1	\N
00000043	01	00000003	\N	D	\N	2013-01-15	1	\N
00000043	01	00000003	\N	D	\N	2013-01-16	1	\N
00000043	01	00000003	\N	D	\N	2013-01-17	1	\N
00000043	01	00000003	\N	F	\N	2013-01-18	1	\N
00000043	01	00000003	\N	F	\N	2013-01-19	1	\N
00000043	01	00000003	\N	D	\N	2013-01-20	1	\N
00000043	01	00000003	\N	D	\N	2013-01-21	1	\N
00000043	01	00000003	\N	D	\N	2013-01-22	1	\N
00000043	01	00000003	\N	D	\N	2013-01-23	1	\N
00000043	01	00000003	\N	D	\N	2013-01-24	1	\N
00000043	01	00000003	\N	F	\N	2013-01-25	1	\N
00000043	01	00000003	\N	F	\N	2013-01-26	1	\N
00000043	01	00000003	\N	D	\N	2013-01-27	1	\N
00000043	01	00000003	\N	D	\N	2013-01-28	1	\N
00000043	01	00000003	\N	D	\N	2013-01-29	1	\N
00000043	01	00000003	\N	D	\N	2013-01-30	1	\N
00000043	01	00000003	\N	D	\N	2013-01-31	1	\N
00000044	01	00000003	\N	D	\N	2013-01-01	1	\N
00000044	01	00000003	\N	D	\N	2013-01-02	1	\N
00000044	01	00000003	\N	D	\N	2013-01-03	1	\N
00000044	01	00000003	\N	F	\N	2013-01-04	1	\N
00000044	01	00000003	\N	F	\N	2013-01-05	1	\N
00000044	01	00000003	\N	D	\N	2013-01-06	1	\N
00000044	01	00000003	\N	D	\N	2013-01-07	1	\N
00000044	01	00000003	\N	D	\N	2013-01-08	1	\N
00000044	01	00000003	\N	D	\N	2013-01-09	1	\N
00000042	01	00000003	\N	D	0000000048	2013-01-09	R	\N
00000044	01	00000003	\N	D	\N	2013-01-10	1	\N
00000044	01	00000003	\N	F	\N	2013-01-11	1	\N
00000044	01	00000003	\N	F	\N	2013-01-12	1	\N
00000044	01	00000003	\N	D	\N	2013-01-13	1	\N
00000044	01	00000003	\N	D	\N	2013-01-14	1	\N
00000044	01	00000003	\N	D	\N	2013-01-15	1	\N
00000044	01	00000003	\N	D	\N	2013-01-16	1	\N
00000044	01	00000003	\N	D	\N	2013-01-17	1	\N
00000044	01	00000003	\N	F	\N	2013-01-18	1	\N
00000044	01	00000003	\N	F	\N	2013-01-19	1	\N
00000044	01	00000003	\N	D	\N	2013-01-20	1	\N
00000044	01	00000003	\N	D	\N	2013-01-21	1	\N
00000044	01	00000003	\N	D	\N	2013-01-22	1	\N
00000044	01	00000003	\N	D	\N	2013-01-23	1	\N
00000044	01	00000003	\N	D	\N	2013-01-24	1	\N
00000044	01	00000003	\N	F	\N	2013-01-25	1	\N
00000044	01	00000003	\N	F	\N	2013-01-26	1	\N
00000044	01	00000003	\N	D	\N	2013-01-27	1	\N
00000044	01	00000003	\N	D	\N	2013-01-28	1	\N
00000044	01	00000003	\N	D	\N	2013-01-29	1	\N
00000044	01	00000003	\N	D	\N	2013-01-30	1	\N
00000044	01	00000003	\N	D	\N	2013-01-31	1	\N
00000045	01	00000003	\N	D	\N	2013-01-01	1	\N
00000045	01	00000003	\N	D	\N	2013-01-02	1	\N
00000045	01	00000003	\N	D	\N	2013-01-03	1	\N
00000045	01	00000003	\N	F	\N	2013-01-04	1	\N
00000045	01	00000003	\N	F	\N	2013-01-05	1	\N
00000045	01	00000003	\N	D	\N	2013-01-06	1	\N
00000045	01	00000003	\N	D	\N	2013-01-07	1	\N
00000045	01	00000003	\N	D	\N	2013-01-08	1	\N
00000045	01	00000003	\N	D	\N	2013-01-09	1	\N
00000045	01	00000003	\N	D	\N	2013-01-10	1	\N
00000045	01	00000003	\N	F	\N	2013-01-11	1	\N
00000045	01	00000003	\N	F	\N	2013-01-12	1	\N
00000045	01	00000003	\N	D	\N	2013-01-13	1	\N
00000045	01	00000003	\N	D	\N	2013-01-14	1	\N
00000045	01	00000003	\N	D	\N	2013-01-15	1	\N
00000045	01	00000003	\N	D	\N	2013-01-16	1	\N
00000045	01	00000003	\N	D	\N	2013-01-17	1	\N
00000045	01	00000003	\N	F	\N	2013-01-18	1	\N
00000045	01	00000003	\N	F	\N	2013-01-19	1	\N
00000045	01	00000003	\N	D	\N	2013-01-20	1	\N
00000045	01	00000003	\N	D	\N	2013-01-21	1	\N
00000045	01	00000003	\N	D	\N	2013-01-22	1	\N
00000045	01	00000003	\N	D	\N	2013-01-23	1	\N
00000045	01	00000003	\N	D	\N	2013-01-24	1	\N
00000045	01	00000003	\N	F	\N	2013-01-25	1	\N
00000045	01	00000003	\N	F	\N	2013-01-26	1	\N
00000045	01	00000003	\N	D	\N	2013-01-27	1	\N
00000045	01	00000003	\N	D	\N	2013-01-28	1	\N
00000045	01	00000003	\N	D	\N	2013-01-29	1	\N
00000045	01	00000003	\N	D	\N	2013-01-30	1	\N
00000045	01	00000003	\N	D	\N	2013-01-31	1	\N
00000046	01	00000003	\N	D	\N	2013-01-01	1	\N
00000046	01	00000003	\N	D	\N	2013-01-02	1	\N
00000046	01	00000003	\N	D	\N	2013-01-03	1	\N
00000046	01	00000003	\N	F	\N	2013-01-04	1	\N
00000046	01	00000003	\N	F	\N	2013-01-05	1	\N
00000046	01	00000003	\N	D	\N	2013-01-06	1	\N
00000046	01	00000003	\N	D	\N	2013-01-07	1	\N
00000046	01	00000003	\N	D	\N	2013-01-08	1	\N
00000046	01	00000003	\N	D	\N	2013-01-09	1	\N
00000046	01	00000003	\N	D	\N	2013-01-10	1	\N
00000046	01	00000003	\N	F	\N	2013-01-11	1	\N
00000046	01	00000003	\N	F	\N	2013-01-12	1	\N
00000046	01	00000003	\N	D	\N	2013-01-13	1	\N
00000046	01	00000003	\N	D	\N	2013-01-14	1	\N
00000046	01	00000003	\N	D	\N	2013-01-15	1	\N
00000046	01	00000003	\N	D	\N	2013-01-16	1	\N
00000046	01	00000003	\N	D	\N	2013-01-17	1	\N
00000046	01	00000003	\N	F	\N	2013-01-18	1	\N
00000046	01	00000003	\N	F	\N	2013-01-19	1	\N
00000046	01	00000003	\N	D	\N	2013-01-20	1	\N
00000046	01	00000003	\N	D	\N	2013-01-21	1	\N
00000046	01	00000003	\N	D	\N	2013-01-22	1	\N
00000046	01	00000003	\N	D	\N	2013-01-23	1	\N
00000046	01	00000003	\N	D	\N	2013-01-24	1	\N
00000046	01	00000003	\N	F	\N	2013-01-25	1	\N
00000046	01	00000003	\N	F	\N	2013-01-26	1	\N
00000046	01	00000003	\N	D	\N	2013-01-27	1	\N
00000046	01	00000003	\N	D	\N	2013-01-28	1	\N
00000046	01	00000003	\N	D	\N	2013-01-29	1	\N
00000046	01	00000003	\N	D	\N	2013-01-30	1	\N
00000046	01	00000003	\N	D	\N	2013-01-31	1	\N
00000047	01	00000003	\N	D	\N	2013-01-01	1	\N
00000047	01	00000003	\N	D	\N	2013-01-02	1	\N
00000047	01	00000003	\N	D	\N	2013-01-03	1	\N
00000047	01	00000003	\N	F	\N	2013-01-04	1	\N
00000047	01	00000003	\N	F	\N	2013-01-05	1	\N
00000047	01	00000003	\N	D	\N	2013-01-06	1	\N
00000047	01	00000003	\N	D	\N	2013-01-07	1	\N
00000047	01	00000003	\N	D	\N	2013-01-08	1	\N
00000047	01	00000003	\N	D	\N	2013-01-09	1	\N
00000047	01	00000003	\N	D	\N	2013-01-10	1	\N
00000047	01	00000003	\N	F	\N	2013-01-11	1	\N
00000047	01	00000003	\N	F	\N	2013-01-12	1	\N
00000047	01	00000003	\N	D	\N	2013-01-13	1	\N
00000047	01	00000003	\N	D	\N	2013-01-14	1	\N
00000047	01	00000003	\N	D	\N	2013-01-15	1	\N
00000047	01	00000003	\N	D	\N	2013-01-16	1	\N
00000047	01	00000003	\N	D	\N	2013-01-17	1	\N
00000047	01	00000003	\N	F	\N	2013-01-18	1	\N
00000047	01	00000003	\N	F	\N	2013-01-19	1	\N
00000047	01	00000003	\N	D	\N	2013-01-20	1	\N
00000047	01	00000003	\N	D	\N	2013-01-21	1	\N
00000047	01	00000003	\N	D	\N	2013-01-22	1	\N
00000047	01	00000003	\N	D	\N	2013-01-23	1	\N
00000047	01	00000003	\N	D	\N	2013-01-24	1	\N
00000047	01	00000003	\N	F	\N	2013-01-25	1	\N
00000047	01	00000003	\N	F	\N	2013-01-26	1	\N
00000047	01	00000003	\N	D	\N	2013-01-27	1	\N
00000047	01	00000003	\N	D	\N	2013-01-28	1	\N
00000047	01	00000003	\N	D	\N	2013-01-29	1	\N
00000047	01	00000003	\N	D	\N	2013-01-30	1	\N
00000047	01	00000003	\N	D	\N	2013-01-31	1	\N
00000048	01	00000003	\N	D	\N	2013-01-01	1	\N
00000048	01	00000003	\N	D	\N	2013-01-02	1	\N
00000048	01	00000003	\N	D	\N	2013-01-03	1	\N
00000048	01	00000003	\N	F	\N	2013-01-04	1	\N
00000048	01	00000003	\N	F	\N	2013-01-05	1	\N
00000048	01	00000003	\N	D	\N	2013-01-06	1	\N
00000048	01	00000003	\N	D	\N	2013-01-07	1	\N
00000048	01	00000003	\N	D	\N	2013-01-08	1	\N
00000048	01	00000003	\N	D	\N	2013-01-09	1	\N
00000048	01	00000003	\N	D	\N	2013-01-10	1	\N
00000048	01	00000003	\N	F	\N	2013-01-11	1	\N
00000048	01	00000003	\N	F	\N	2013-01-12	1	\N
00000048	01	00000003	\N	D	\N	2013-01-13	1	\N
00000048	01	00000003	\N	D	\N	2013-01-14	1	\N
00000048	01	00000003	\N	D	\N	2013-01-15	1	\N
00000048	01	00000003	\N	D	\N	2013-01-16	1	\N
00000048	01	00000003	\N	D	\N	2013-01-17	1	\N
00000048	01	00000003	\N	F	\N	2013-01-18	1	\N
00000048	01	00000003	\N	F	\N	2013-01-19	1	\N
00000048	01	00000003	\N	D	\N	2013-01-20	1	\N
00000048	01	00000003	\N	D	\N	2013-01-21	1	\N
00000048	01	00000003	\N	D	\N	2013-01-22	1	\N
00000048	01	00000003	\N	D	\N	2013-01-23	1	\N
00000048	01	00000003	\N	D	\N	2013-01-24	1	\N
00000048	01	00000003	\N	F	\N	2013-01-25	1	\N
00000048	01	00000003	\N	F	\N	2013-01-26	1	\N
00000048	01	00000003	\N	D	\N	2013-01-27	1	\N
00000048	01	00000003	\N	D	\N	2013-01-28	1	\N
00000048	01	00000003	\N	D	\N	2013-01-29	1	\N
00000048	01	00000003	\N	D	\N	2013-01-30	1	\N
00000048	01	00000003	\N	D	\N	2013-01-31	1	\N
00000042	01	00000003	\N	F	\N	2013-02-01	1	\N
00000042	01	00000003	\N	F	\N	2013-02-02	1	\N
00000042	01	00000003	\N	D	\N	2013-02-03	1	\N
00000042	01	00000003	\N	D	\N	2013-02-04	1	\N
00000042	01	00000003	\N	D	\N	2013-02-05	1	\N
00000042	01	00000003	\N	D	\N	2013-02-06	1	\N
00000042	01	00000003	\N	D	\N	2013-02-07	1	\N
00000042	01	00000003	\N	F	\N	2013-02-08	1	\N
00000042	01	00000003	\N	F	\N	2013-02-09	1	\N
00000042	01	00000003	\N	D	\N	2013-02-10	1	\N
00000042	01	00000003	\N	D	\N	2013-02-11	1	\N
00000042	01	00000003	\N	D	\N	2013-02-12	1	\N
00000042	01	00000003	\N	D	\N	2013-02-13	1	\N
00000042	01	00000003	\N	D	\N	2013-02-14	1	\N
00000042	01	00000003	\N	F	\N	2013-02-15	1	\N
00000042	01	00000003	\N	F	\N	2013-02-16	1	\N
00000042	01	00000003	\N	D	\N	2013-02-17	1	\N
00000042	01	00000003	\N	D	\N	2013-02-18	1	\N
00000042	01	00000003	\N	D	\N	2013-02-19	1	\N
00000042	01	00000003	\N	D	\N	2013-02-20	1	\N
00000042	01	00000003	\N	D	\N	2013-02-21	1	\N
00000042	01	00000003	\N	F	\N	2013-02-22	1	\N
00000042	01	00000003	\N	F	\N	2013-02-23	1	\N
00000042	01	00000003	\N	D	\N	2013-02-24	1	\N
00000042	01	00000003	\N	D	\N	2013-02-25	1	\N
00000042	01	00000003	\N	D	\N	2013-02-26	1	\N
00000042	01	00000003	\N	D	\N	2013-02-27	1	\N
00000042	01	00000003	\N	D	\N	2013-02-28	1	\N
00000043	01	00000003	\N	F	\N	2013-02-01	1	\N
00000043	01	00000003	\N	F	\N	2013-02-02	1	\N
00000043	01	00000003	\N	D	\N	2013-02-03	1	\N
00000043	01	00000003	\N	D	\N	2013-02-04	1	\N
00000043	01	00000003	\N	D	\N	2013-02-05	1	\N
00000043	01	00000003	\N	D	\N	2013-02-06	1	\N
00000043	01	00000003	\N	D	\N	2013-02-07	1	\N
00000043	01	00000003	\N	F	\N	2013-02-08	1	\N
00000043	01	00000003	\N	F	\N	2013-02-09	1	\N
00000043	01	00000003	\N	D	\N	2013-02-10	1	\N
00000043	01	00000003	\N	D	\N	2013-02-11	1	\N
00000043	01	00000003	\N	D	\N	2013-02-12	1	\N
00000043	01	00000003	\N	D	\N	2013-02-13	1	\N
00000043	01	00000003	\N	D	\N	2013-02-14	1	\N
00000043	01	00000003	\N	F	\N	2013-02-15	1	\N
00000043	01	00000003	\N	F	\N	2013-02-16	1	\N
00000043	01	00000003	\N	D	\N	2013-02-17	1	\N
00000043	01	00000003	\N	D	\N	2013-02-18	1	\N
00000043	01	00000003	\N	D	\N	2013-02-19	1	\N
00000043	01	00000003	\N	D	\N	2013-02-20	1	\N
00000043	01	00000003	\N	D	\N	2013-02-21	1	\N
00000043	01	00000003	\N	F	\N	2013-02-22	1	\N
00000043	01	00000003	\N	F	\N	2013-02-23	1	\N
00000043	01	00000003	\N	D	\N	2013-02-24	1	\N
00000043	01	00000003	\N	D	\N	2013-02-25	1	\N
00000043	01	00000003	\N	D	\N	2013-02-26	1	\N
00000043	01	00000003	\N	D	\N	2013-02-27	1	\N
00000043	01	00000003	\N	D	\N	2013-02-28	1	\N
00000044	01	00000003	\N	F	\N	2013-02-01	1	\N
00000044	01	00000003	\N	F	\N	2013-02-02	1	\N
00000044	01	00000003	\N	D	\N	2013-02-03	1	\N
00000044	01	00000003	\N	D	\N	2013-02-04	1	\N
00000044	01	00000003	\N	D	\N	2013-02-05	1	\N
00000044	01	00000003	\N	D	\N	2013-02-06	1	\N
00000044	01	00000003	\N	D	\N	2013-02-07	1	\N
00000044	01	00000003	\N	F	\N	2013-02-08	1	\N
00000044	01	00000003	\N	F	\N	2013-02-09	1	\N
00000044	01	00000003	\N	D	\N	2013-02-10	1	\N
00000044	01	00000003	\N	D	\N	2013-02-11	1	\N
00000044	01	00000003	\N	D	\N	2013-02-12	1	\N
00000044	01	00000003	\N	D	\N	2013-02-13	1	\N
00000044	01	00000003	\N	D	\N	2013-02-14	1	\N
00000044	01	00000003	\N	F	\N	2013-02-15	1	\N
00000044	01	00000003	\N	F	\N	2013-02-16	1	\N
00000044	01	00000003	\N	D	\N	2013-02-17	1	\N
00000044	01	00000003	\N	D	\N	2013-02-18	1	\N
00000044	01	00000003	\N	D	\N	2013-02-19	1	\N
00000044	01	00000003	\N	D	\N	2013-02-20	1	\N
00000044	01	00000003	\N	D	\N	2013-02-21	1	\N
00000044	01	00000003	\N	F	\N	2013-02-22	1	\N
00000044	01	00000003	\N	F	\N	2013-02-23	1	\N
00000044	01	00000003	\N	D	\N	2013-02-24	1	\N
00000044	01	00000003	\N	D	\N	2013-02-25	1	\N
00000044	01	00000003	\N	D	\N	2013-02-26	1	\N
00000044	01	00000003	\N	D	\N	2013-02-27	1	\N
00000044	01	00000003	\N	D	\N	2013-02-28	1	\N
00000045	01	00000003	\N	F	\N	2013-02-01	1	\N
00000045	01	00000003	\N	F	\N	2013-02-02	1	\N
00000045	01	00000003	\N	D	\N	2013-02-03	1	\N
00000045	01	00000003	\N	D	\N	2013-02-04	1	\N
00000045	01	00000003	\N	D	\N	2013-02-05	1	\N
00000045	01	00000003	\N	D	\N	2013-02-06	1	\N
00000045	01	00000003	\N	D	\N	2013-02-07	1	\N
00000045	01	00000003	\N	F	\N	2013-02-08	1	\N
00000045	01	00000003	\N	F	\N	2013-02-09	1	\N
00000045	01	00000003	\N	D	\N	2013-02-10	1	\N
00000045	01	00000003	\N	D	\N	2013-02-11	1	\N
00000045	01	00000003	\N	D	\N	2013-02-12	1	\N
00000045	01	00000003	\N	D	\N	2013-02-13	1	\N
00000045	01	00000003	\N	D	\N	2013-02-14	1	\N
00000045	01	00000003	\N	F	\N	2013-02-15	1	\N
00000045	01	00000003	\N	F	\N	2013-02-16	1	\N
00000045	01	00000003	\N	D	\N	2013-02-17	1	\N
00000045	01	00000003	\N	D	\N	2013-02-18	1	\N
00000045	01	00000003	\N	D	\N	2013-02-19	1	\N
00000045	01	00000003	\N	D	\N	2013-02-20	1	\N
00000045	01	00000003	\N	D	\N	2013-02-21	1	\N
00000045	01	00000003	\N	F	\N	2013-02-22	1	\N
00000045	01	00000003	\N	F	\N	2013-02-23	1	\N
00000045	01	00000003	\N	D	\N	2013-02-24	1	\N
00000045	01	00000003	\N	D	\N	2013-02-25	1	\N
00000045	01	00000003	\N	D	\N	2013-02-26	1	\N
00000045	01	00000003	\N	D	\N	2013-02-27	1	\N
00000045	01	00000003	\N	D	\N	2013-02-28	1	\N
00000046	01	00000003	\N	F	\N	2013-02-01	1	\N
00000046	01	00000003	\N	F	\N	2013-02-02	1	\N
00000046	01	00000003	\N	D	\N	2013-02-03	1	\N
00000046	01	00000003	\N	D	\N	2013-02-04	1	\N
00000046	01	00000003	\N	D	\N	2013-02-05	1	\N
00000046	01	00000003	\N	D	\N	2013-02-06	1	\N
00000046	01	00000003	\N	D	\N	2013-02-07	1	\N
00000046	01	00000003	\N	F	\N	2013-02-08	1	\N
00000046	01	00000003	\N	F	\N	2013-02-09	1	\N
00000046	01	00000003	\N	D	\N	2013-02-10	1	\N
00000046	01	00000003	\N	D	\N	2013-02-11	1	\N
00000046	01	00000003	\N	D	\N	2013-02-12	1	\N
00000046	01	00000003	\N	D	\N	2013-02-13	1	\N
00000046	01	00000003	\N	D	\N	2013-02-14	1	\N
00000046	01	00000003	\N	F	\N	2013-02-15	1	\N
00000046	01	00000003	\N	F	\N	2013-02-16	1	\N
00000046	01	00000003	\N	D	\N	2013-02-17	1	\N
00000046	01	00000003	\N	D	\N	2013-02-18	1	\N
00000046	01	00000003	\N	D	\N	2013-02-19	1	\N
00000046	01	00000003	\N	D	\N	2013-02-20	1	\N
00000046	01	00000003	\N	D	\N	2013-02-21	1	\N
00000046	01	00000003	\N	F	\N	2013-02-22	1	\N
00000046	01	00000003	\N	F	\N	2013-02-23	1	\N
00000046	01	00000003	\N	D	\N	2013-02-24	1	\N
00000046	01	00000003	\N	D	\N	2013-02-25	1	\N
00000046	01	00000003	\N	D	\N	2013-02-26	1	\N
00000046	01	00000003	\N	D	\N	2013-02-27	1	\N
00000046	01	00000003	\N	D	\N	2013-02-28	1	\N
00000047	01	00000003	\N	F	\N	2013-02-01	1	\N
00000047	01	00000003	\N	F	\N	2013-02-02	1	\N
00000047	01	00000003	\N	D	\N	2013-02-03	1	\N
00000047	01	00000003	\N	D	\N	2013-02-04	1	\N
00000047	01	00000003	\N	D	\N	2013-02-05	1	\N
00000047	01	00000003	\N	D	\N	2013-02-06	1	\N
00000047	01	00000003	\N	D	\N	2013-02-07	1	\N
00000047	01	00000003	\N	F	\N	2013-02-08	1	\N
00000047	01	00000003	\N	F	\N	2013-02-09	1	\N
00000047	01	00000003	\N	D	\N	2013-02-10	1	\N
00000047	01	00000003	\N	D	\N	2013-02-11	1	\N
00000047	01	00000003	\N	D	\N	2013-02-12	1	\N
00000047	01	00000003	\N	D	\N	2013-02-13	1	\N
00000047	01	00000003	\N	D	\N	2013-02-14	1	\N
00000047	01	00000003	\N	F	\N	2013-02-15	1	\N
00000047	01	00000003	\N	F	\N	2013-02-16	1	\N
00000047	01	00000003	\N	D	\N	2013-02-17	1	\N
00000047	01	00000003	\N	D	\N	2013-02-18	1	\N
00000047	01	00000003	\N	D	\N	2013-02-19	1	\N
00000047	01	00000003	\N	D	\N	2013-02-20	1	\N
00000047	01	00000003	\N	D	\N	2013-02-21	1	\N
00000047	01	00000003	\N	F	\N	2013-02-22	1	\N
00000047	01	00000003	\N	F	\N	2013-02-23	1	\N
00000047	01	00000003	\N	D	\N	2013-02-24	1	\N
00000047	01	00000003	\N	D	\N	2013-02-25	1	\N
00000047	01	00000003	\N	D	\N	2013-02-26	1	\N
00000047	01	00000003	\N	D	\N	2013-02-27	1	\N
00000047	01	00000003	\N	D	\N	2013-02-28	1	\N
00000048	01	00000003	\N	F	\N	2013-02-01	1	\N
00000048	01	00000003	\N	F	\N	2013-02-02	1	\N
00000048	01	00000003	\N	D	\N	2013-02-03	1	\N
00000048	01	00000003	\N	D	\N	2013-02-04	1	\N
00000048	01	00000003	\N	D	\N	2013-02-05	1	\N
00000048	01	00000003	\N	D	\N	2013-02-06	1	\N
00000048	01	00000003	\N	D	\N	2013-02-07	1	\N
00000048	01	00000003	\N	F	\N	2013-02-08	1	\N
00000048	01	00000003	\N	F	\N	2013-02-09	1	\N
00000048	01	00000003	\N	D	\N	2013-02-10	1	\N
00000048	01	00000003	\N	D	\N	2013-02-11	1	\N
00000048	01	00000003	\N	D	\N	2013-02-12	1	\N
00000048	01	00000003	\N	D	\N	2013-02-13	1	\N
00000048	01	00000003	\N	D	\N	2013-02-14	1	\N
00000048	01	00000003	\N	F	\N	2013-02-15	1	\N
00000048	01	00000003	\N	F	\N	2013-02-16	1	\N
00000048	01	00000003	\N	D	\N	2013-02-17	1	\N
00000048	01	00000003	\N	D	\N	2013-02-18	1	\N
00000048	01	00000003	\N	D	\N	2013-02-19	1	\N
00000048	01	00000003	\N	D	\N	2013-02-20	1	\N
00000048	01	00000003	\N	D	\N	2013-02-21	1	\N
00000048	01	00000003	\N	F	\N	2013-02-22	1	\N
00000048	01	00000003	\N	F	\N	2013-02-23	1	\N
00000048	01	00000003	\N	D	\N	2013-02-24	1	\N
00000048	01	00000003	\N	D	\N	2013-02-25	1	\N
00000048	01	00000003	\N	D	\N	2013-02-26	1	\N
00000048	01	00000003	\N	D	\N	2013-02-27	1	\N
00000048	01	00000003	\N	D	\N	2013-02-28	1	\N
00000060	06	00000003	\N	D	\N	2013-01-01	1	\N
00000060	06	00000003	\N	D	\N	2013-01-02	1	\N
00000060	06	00000003	\N	D	\N	2013-01-03	1	\N
00000060	06	00000003	\N	F	\N	2013-01-04	1	\N
00000060	06	00000003	\N	F	\N	2013-01-05	1	\N
00000060	06	00000003	\N	D	\N	2013-01-06	1	\N
00000060	06	00000003	\N	D	\N	2013-01-07	1	\N
00000060	06	00000003	\N	D	\N	2013-01-08	1	\N
00000060	06	00000003	\N	D	\N	2013-01-09	1	\N
00000060	06	00000003	\N	D	\N	2013-01-10	1	\N
00000060	06	00000003	\N	F	\N	2013-01-11	1	\N
00000060	06	00000003	\N	F	\N	2013-01-12	1	\N
00000060	06	00000003	\N	D	\N	2013-01-13	1	\N
00000060	06	00000003	\N	D	\N	2013-01-14	1	\N
00000060	06	00000003	\N	D	\N	2013-01-15	1	\N
00000060	06	00000003	\N	D	\N	2013-01-16	1	\N
00000060	06	00000003	\N	D	\N	2013-01-17	1	\N
00000060	06	00000003	\N	F	\N	2013-01-18	1	\N
00000060	06	00000003	\N	F	\N	2013-01-19	1	\N
00000060	06	00000003	\N	D	\N	2013-01-20	1	\N
00000060	06	00000003	\N	D	\N	2013-01-21	1	\N
00000060	06	00000003	\N	D	\N	2013-01-22	1	\N
00000060	06	00000003	\N	D	\N	2013-01-23	1	\N
00000060	06	00000003	\N	D	\N	2013-01-24	1	\N
00000060	06	00000003	\N	F	\N	2013-01-25	1	\N
00000060	06	00000003	\N	F	\N	2013-01-26	1	\N
00000060	06	00000003	\N	D	\N	2013-01-27	1	\N
00000060	06	00000003	\N	D	\N	2013-01-28	1	\N
00000060	06	00000003	\N	D	\N	2013-01-29	1	\N
00000060	06	00000003	\N	D	\N	2013-01-30	1	\N
00000060	06	00000003	\N	D	\N	2013-01-31	1	\N
00000081	06	00000003	\N	D	\N	2013-01-01	1	\N
00000081	06	00000003	\N	D	\N	2013-01-02	1	\N
00000081	06	00000003	\N	D	\N	2013-01-03	1	\N
00000081	06	00000003	\N	F	\N	2013-01-04	1	\N
00000081	06	00000003	\N	F	\N	2013-01-05	1	\N
00000081	06	00000003	\N	D	\N	2013-01-06	1	\N
00000081	06	00000003	\N	D	\N	2013-01-07	1	\N
00000081	06	00000003	\N	D	\N	2013-01-08	1	\N
00000081	06	00000003	\N	D	\N	2013-01-09	1	\N
00000081	06	00000003	\N	D	\N	2013-01-10	1	\N
00000081	06	00000003	\N	F	\N	2013-01-11	1	\N
00000081	06	00000003	\N	F	\N	2013-01-12	1	\N
00000081	06	00000003	\N	D	\N	2013-01-13	1	\N
00000081	06	00000003	\N	D	\N	2013-01-14	1	\N
00000081	06	00000003	\N	D	\N	2013-01-15	1	\N
00000081	06	00000003	\N	D	\N	2013-01-16	1	\N
00000081	06	00000003	\N	D	\N	2013-01-17	1	\N
00000081	06	00000003	\N	F	\N	2013-01-18	1	\N
00000081	06	00000003	\N	F	\N	2013-01-19	1	\N
00000081	06	00000003	\N	D	\N	2013-01-20	1	\N
00000081	06	00000003	\N	D	\N	2013-01-21	1	\N
00000081	06	00000003	\N	D	\N	2013-01-22	1	\N
00000081	06	00000003	\N	D	\N	2013-01-23	1	\N
00000081	06	00000003	\N	D	\N	2013-01-24	1	\N
00000081	06	00000003	\N	F	\N	2013-01-25	1	\N
00000081	06	00000003	\N	F	\N	2013-01-26	1	\N
00000081	06	00000003	\N	D	\N	2013-01-27	1	\N
00000081	06	00000003	\N	D	\N	2013-01-28	1	\N
00000081	06	00000003	\N	D	\N	2013-01-29	1	\N
00000081	06	00000003	\N	D	\N	2013-01-30	1	\N
00000081	06	00000003	\N	D	\N	2013-01-31	1	\N
00000060	06	00000003	\N	F	\N	2013-02-01	1	\N
00000060	06	00000003	\N	F	\N	2013-02-02	1	\N
00000060	06	00000003	\N	D	\N	2013-02-03	1	\N
00000060	06	00000003	\N	D	\N	2013-02-04	1	\N
00000060	06	00000003	\N	D	\N	2013-02-05	1	\N
00000060	06	00000003	\N	D	\N	2013-02-06	1	\N
00000060	06	00000003	\N	D	\N	2013-02-07	1	\N
00000060	06	00000003	\N	F	\N	2013-02-08	1	\N
00000060	06	00000003	\N	F	\N	2013-02-09	1	\N
00000060	06	00000003	\N	D	\N	2013-02-10	1	\N
00000060	06	00000003	\N	D	\N	2013-02-11	1	\N
00000060	06	00000003	\N	D	\N	2013-02-12	1	\N
00000060	06	00000003	\N	D	\N	2013-02-13	1	\N
00000060	06	00000003	\N	D	\N	2013-02-14	1	\N
00000060	06	00000003	\N	F	\N	2013-02-15	1	\N
00000060	06	00000003	\N	F	\N	2013-02-16	1	\N
00000060	06	00000003	\N	D	\N	2013-02-17	1	\N
00000060	06	00000003	\N	D	\N	2013-02-18	1	\N
00000060	06	00000003	\N	D	\N	2013-02-19	1	\N
00000060	06	00000003	\N	D	\N	2013-02-20	1	\N
00000060	06	00000003	\N	D	\N	2013-02-21	1	\N
00000060	06	00000003	\N	F	\N	2013-02-22	1	\N
00000060	06	00000003	\N	F	\N	2013-02-23	1	\N
00000060	06	00000003	\N	D	\N	2013-02-24	1	\N
00000060	06	00000003	\N	D	\N	2013-02-25	1	\N
00000060	06	00000003	\N	D	\N	2013-02-26	1	\N
00000060	06	00000003	\N	D	\N	2013-02-27	1	\N
00000060	06	00000003	\N	D	\N	2013-02-28	1	\N
00000081	06	00000003	\N	F	\N	2013-02-01	1	\N
00000081	06	00000003	\N	F	\N	2013-02-02	1	\N
00000081	06	00000003	\N	D	\N	2013-02-03	1	\N
00000081	06	00000003	\N	D	\N	2013-02-04	1	\N
00000081	06	00000003	\N	D	\N	2013-02-05	1	\N
00000081	06	00000003	\N	D	\N	2013-02-06	1	\N
00000081	06	00000003	\N	D	\N	2013-02-07	1	\N
00000081	06	00000003	\N	F	\N	2013-02-08	1	\N
00000081	06	00000003	\N	F	\N	2013-02-09	1	\N
00000081	06	00000003	\N	D	\N	2013-02-10	1	\N
00000081	06	00000003	\N	D	\N	2013-02-11	1	\N
00000081	06	00000003	\N	D	\N	2013-02-12	1	\N
00000081	06	00000003	\N	D	\N	2013-02-13	1	\N
00000081	06	00000003	\N	D	\N	2013-02-14	1	\N
00000081	06	00000003	\N	F	\N	2013-02-15	1	\N
00000081	06	00000003	\N	F	\N	2013-02-16	1	\N
00000081	06	00000003	\N	D	\N	2013-02-17	1	\N
00000081	06	00000003	\N	D	\N	2013-02-18	1	\N
00000081	06	00000003	\N	D	\N	2013-02-19	1	\N
00000081	06	00000003	\N	D	\N	2013-02-20	1	\N
00000081	06	00000003	\N	D	\N	2013-02-21	1	\N
00000081	06	00000003	\N	F	\N	2013-02-22	1	\N
00000081	06	00000003	\N	F	\N	2013-02-23	1	\N
00000081	06	00000003	\N	D	\N	2013-02-24	1	\N
00000081	06	00000003	\N	D	\N	2013-02-25	1	\N
00000081	06	00000003	\N	D	\N	2013-02-26	1	\N
00000081	06	00000003	\N	D	\N	2013-02-27	1	\N
00000081	06	00000003	\N	D	\N	2013-02-28	1	\N
00000058	05	00000003	\N	D	\N	2013-01-01	1	\N
00000058	05	00000003	\N	D	\N	2013-01-02	1	\N
00000058	05	00000003	\N	D	\N	2013-01-03	1	\N
00000058	05	00000003	\N	F	\N	2013-01-04	1	\N
00000058	05	00000003	\N	F	\N	2013-01-05	1	\N
00000058	05	00000003	\N	D	\N	2013-01-06	1	\N
00000058	05	00000003	\N	D	\N	2013-01-07	1	\N
00000058	05	00000003	\N	D	\N	2013-01-08	1	\N
00000058	05	00000003	\N	D	\N	2013-01-09	1	\N
00000058	05	00000003	\N	D	\N	2013-01-10	1	\N
00000058	05	00000003	\N	F	\N	2013-01-11	1	\N
00000058	05	00000003	\N	F	\N	2013-01-12	1	\N
00000058	05	00000003	\N	D	\N	2013-01-13	1	\N
00000058	05	00000003	\N	D	\N	2013-01-14	1	\N
00000058	05	00000003	\N	D	\N	2013-01-15	1	\N
00000058	05	00000003	\N	D	\N	2013-01-16	1	\N
00000058	05	00000003	\N	D	\N	2013-01-17	1	\N
00000058	05	00000003	\N	F	\N	2013-01-18	1	\N
00000058	05	00000003	\N	F	\N	2013-01-19	1	\N
00000058	05	00000003	\N	D	\N	2013-01-20	1	\N
00000058	05	00000003	\N	D	\N	2013-01-21	1	\N
00000058	05	00000003	\N	D	\N	2013-01-22	1	\N
00000058	05	00000003	\N	D	\N	2013-01-23	1	\N
00000058	05	00000003	\N	D	\N	2013-01-24	1	\N
00000058	05	00000003	\N	F	\N	2013-01-25	1	\N
00000058	05	00000003	\N	F	\N	2013-01-26	1	\N
00000058	05	00000003	\N	D	\N	2013-01-27	1	\N
00000058	05	00000003	\N	D	\N	2013-01-28	1	\N
00000058	05	00000003	\N	D	\N	2013-01-29	1	\N
00000058	05	00000003	\N	D	\N	2013-01-30	1	\N
00000058	05	00000003	\N	D	\N	2013-01-31	1	\N
00000059	05	00000003	\N	D	\N	2013-01-01	1	\N
00000059	05	00000003	\N	D	\N	2013-01-02	1	\N
00000059	05	00000003	\N	D	\N	2013-01-03	1	\N
00000059	05	00000003	\N	F	\N	2013-01-04	1	\N
00000059	05	00000003	\N	F	\N	2013-01-05	1	\N
00000059	05	00000003	\N	D	\N	2013-01-06	1	\N
00000059	05	00000003	\N	D	\N	2013-01-07	1	\N
00000059	05	00000003	\N	D	\N	2013-01-08	1	\N
00000059	05	00000003	\N	D	\N	2013-01-09	1	\N
00000059	05	00000003	\N	D	\N	2013-01-10	1	\N
00000059	05	00000003	\N	F	\N	2013-01-11	1	\N
00000059	05	00000003	\N	F	\N	2013-01-12	1	\N
00000059	05	00000003	\N	D	\N	2013-01-13	1	\N
00000059	05	00000003	\N	D	\N	2013-01-14	1	\N
00000059	05	00000003	\N	D	\N	2013-01-15	1	\N
00000059	05	00000003	\N	D	\N	2013-01-16	1	\N
00000059	05	00000003	\N	D	\N	2013-01-17	1	\N
00000059	05	00000003	\N	F	\N	2013-01-18	1	\N
00000059	05	00000003	\N	F	\N	2013-01-19	1	\N
00000059	05	00000003	\N	D	\N	2013-01-20	1	\N
00000059	05	00000003	\N	D	\N	2013-01-21	1	\N
00000059	05	00000003	\N	D	\N	2013-01-22	1	\N
00000059	05	00000003	\N	D	\N	2013-01-23	1	\N
00000059	05	00000003	\N	D	\N	2013-01-24	1	\N
00000059	05	00000003	\N	F	\N	2013-01-25	1	\N
00000059	05	00000003	\N	F	\N	2013-01-26	1	\N
00000059	05	00000003	\N	D	\N	2013-01-27	1	\N
00000059	05	00000003	\N	D	\N	2013-01-28	1	\N
00000059	05	00000003	\N	D	\N	2013-01-29	1	\N
00000059	05	00000003	\N	D	\N	2013-01-30	1	\N
00000059	05	00000003	\N	D	\N	2013-01-31	1	\N
00000058	05	00000003	\N	F	\N	2013-02-01	1	\N
00000058	05	00000003	\N	F	\N	2013-02-02	1	\N
00000058	05	00000003	\N	D	\N	2013-02-03	1	\N
00000058	05	00000003	\N	D	\N	2013-02-04	1	\N
00000058	05	00000003	\N	D	\N	2013-02-05	1	\N
00000058	05	00000003	\N	D	\N	2013-02-06	1	\N
00000058	05	00000003	\N	D	\N	2013-02-07	1	\N
00000058	05	00000003	\N	F	\N	2013-02-08	1	\N
00000058	05	00000003	\N	F	\N	2013-02-09	1	\N
00000058	05	00000003	\N	D	\N	2013-02-10	1	\N
00000058	05	00000003	\N	D	\N	2013-02-11	1	\N
00000058	05	00000003	\N	D	\N	2013-02-12	1	\N
00000058	05	00000003	\N	D	\N	2013-02-13	1	\N
00000058	05	00000003	\N	D	\N	2013-02-14	1	\N
00000058	05	00000003	\N	F	\N	2013-02-15	1	\N
00000058	05	00000003	\N	F	\N	2013-02-16	1	\N
00000058	05	00000003	\N	D	\N	2013-02-17	1	\N
00000058	05	00000003	\N	D	\N	2013-02-18	1	\N
00000058	05	00000003	\N	D	\N	2013-02-19	1	\N
00000058	05	00000003	\N	D	\N	2013-02-20	1	\N
00000058	05	00000003	\N	D	\N	2013-02-21	1	\N
00000058	05	00000003	\N	F	\N	2013-02-22	1	\N
00000058	05	00000003	\N	F	\N	2013-02-23	1	\N
00000058	05	00000003	\N	D	\N	2013-02-24	1	\N
00000058	05	00000003	\N	D	\N	2013-02-25	1	\N
00000058	05	00000003	\N	D	\N	2013-02-26	1	\N
00000058	05	00000003	\N	D	\N	2013-02-27	1	\N
00000058	05	00000003	\N	D	\N	2013-02-28	1	\N
00000059	05	00000003	\N	F	\N	2013-02-01	1	\N
00000059	05	00000003	\N	F	\N	2013-02-02	1	\N
00000059	05	00000003	\N	D	\N	2013-02-03	1	\N
00000059	05	00000003	\N	D	\N	2013-02-04	1	\N
00000059	05	00000003	\N	D	\N	2013-02-05	1	\N
00000059	05	00000003	\N	D	\N	2013-02-06	1	\N
00000059	05	00000003	\N	D	\N	2013-02-07	1	\N
00000059	05	00000003	\N	F	\N	2013-02-08	1	\N
00000059	05	00000003	\N	F	\N	2013-02-09	1	\N
00000059	05	00000003	\N	D	\N	2013-02-10	1	\N
00000059	05	00000003	\N	D	\N	2013-02-11	1	\N
00000059	05	00000003	\N	D	\N	2013-02-12	1	\N
00000059	05	00000003	\N	D	\N	2013-02-13	1	\N
00000059	05	00000003	\N	D	\N	2013-02-14	1	\N
00000059	05	00000003	\N	F	\N	2013-02-15	1	\N
00000059	05	00000003	\N	F	\N	2013-02-16	1	\N
00000059	05	00000003	\N	D	\N	2013-02-17	1	\N
00000059	05	00000003	\N	D	\N	2013-02-18	1	\N
00000059	05	00000003	\N	D	\N	2013-02-19	1	\N
00000059	05	00000003	\N	D	\N	2013-02-20	1	\N
00000059	05	00000003	\N	D	\N	2013-02-21	1	\N
00000059	05	00000003	\N	F	\N	2013-02-22	1	\N
00000059	05	00000003	\N	F	\N	2013-02-23	1	\N
00000059	05	00000003	\N	D	\N	2013-02-24	1	\N
00000059	05	00000003	\N	D	\N	2013-02-25	1	\N
00000059	05	00000003	\N	D	\N	2013-02-26	1	\N
00000059	05	00000003	\N	D	\N	2013-02-27	1	\N
00000059	05	00000003	\N	D	\N	2013-02-28	1	\N
00000049	02	00000003	\N	D	\N	2013-01-01	1	\N
00000049	02	00000003	\N	D	\N	2013-01-02	1	\N
00000049	02	00000003	\N	D	\N	2013-01-03	1	\N
00000049	02	00000003	\N	F	\N	2013-01-04	1	\N
00000049	02	00000003	\N	F	\N	2013-01-05	1	\N
00000049	02	00000003	\N	D	\N	2013-01-06	1	\N
00000049	02	00000003	\N	D	\N	2013-01-07	1	\N
00000049	02	00000003	\N	D	\N	2013-01-08	1	\N
00000049	02	00000003	\N	D	\N	2013-01-09	1	\N
00000049	02	00000003	\N	D	\N	2013-01-10	1	\N
00000049	02	00000003	\N	D	\N	2013-01-15	1	\N
00000049	02	00000003	\N	D	\N	2013-01-16	1	\N
00000049	02	00000003	\N	D	\N	2013-01-17	1	\N
00000049	02	00000003	\N	F	\N	2013-01-18	1	\N
00000049	02	00000003	\N	F	\N	2013-01-19	1	\N
00000049	02	00000003	\N	D	\N	2013-01-20	1	\N
00000049	02	00000003	\N	D	\N	2013-01-21	1	\N
00000049	02	00000003	\N	D	\N	2013-01-22	1	\N
00000049	02	00000003	\N	D	\N	2013-01-23	1	\N
00000049	02	00000003	\N	D	\N	2013-01-24	1	\N
00000049	02	00000003	\N	F	\N	2013-01-25	1	\N
00000049	02	00000003	\N	F	\N	2013-01-26	1	\N
00000049	02	00000003	\N	D	\N	2013-01-27	1	\N
00000049	02	00000003	\N	D	\N	2013-01-28	1	\N
00000049	02	00000003	\N	D	\N	2013-01-29	1	\N
00000049	02	00000003	\N	D	\N	2013-01-30	1	\N
00000049	02	00000003	\N	D	\N	2013-01-31	1	\N
00000050	02	00000003	\N	D	\N	2013-01-01	1	\N
00000050	02	00000003	\N	D	\N	2013-01-02	1	\N
00000050	02	00000003	\N	D	\N	2013-01-03	1	\N
00000050	02	00000003	\N	F	\N	2013-01-04	1	\N
00000050	02	00000003	\N	F	\N	2013-01-05	1	\N
00000050	02	00000003	\N	D	\N	2013-01-06	1	\N
00000050	02	00000003	\N	D	\N	2013-01-07	1	\N
00000050	02	00000003	\N	D	\N	2013-01-08	1	\N
00000050	02	00000003	\N	D	\N	2013-01-09	1	\N
00000050	02	00000003	\N	D	\N	2013-01-10	1	\N
00000050	02	00000003	\N	F	\N	2013-01-11	1	\N
00000050	02	00000003	\N	F	\N	2013-01-12	1	\N
00000050	02	00000003	\N	D	\N	2013-01-13	1	\N
00000050	02	00000003	\N	D	\N	2013-01-14	1	\N
00000050	02	00000003	\N	D	\N	2013-01-15	1	\N
00000050	02	00000003	\N	D	\N	2013-01-16	1	\N
00000050	02	00000003	\N	D	\N	2013-01-17	1	\N
00000050	02	00000003	\N	F	\N	2013-01-18	1	\N
00000050	02	00000003	\N	F	\N	2013-01-19	1	\N
00000050	02	00000003	\N	D	\N	2013-01-20	1	\N
00000050	02	00000003	\N	D	\N	2013-01-21	1	\N
00000050	02	00000003	\N	D	\N	2013-01-22	1	\N
00000050	02	00000003	\N	D	\N	2013-01-23	1	\N
00000050	02	00000003	\N	D	\N	2013-01-24	1	\N
00000050	02	00000003	\N	F	\N	2013-01-25	1	\N
00000050	02	00000003	\N	F	\N	2013-01-26	1	\N
00000050	02	00000003	\N	D	\N	2013-01-27	1	\N
00000050	02	00000003	\N	D	\N	2013-01-28	1	\N
00000050	02	00000003	\N	D	\N	2013-01-29	1	\N
00000050	02	00000003	\N	D	\N	2013-01-30	1	\N
00000050	02	00000003	\N	D	\N	2013-01-31	1	\N
00000051	02	00000003	\N	D	\N	2013-01-01	1	\N
00000051	02	00000003	\N	D	\N	2013-01-02	1	\N
00000051	02	00000003	\N	D	\N	2013-01-03	1	\N
00000051	02	00000003	\N	F	\N	2013-01-04	1	\N
00000051	02	00000003	\N	F	\N	2013-01-05	1	\N
00000051	02	00000003	\N	D	\N	2013-01-06	1	\N
00000051	02	00000003	\N	D	\N	2013-01-07	1	\N
00000051	02	00000003	\N	D	\N	2013-01-08	1	\N
00000051	02	00000003	\N	D	\N	2013-01-09	1	\N
00000051	02	00000003	\N	D	\N	2013-01-10	1	\N
00000051	02	00000003	\N	F	\N	2013-01-11	1	\N
00000051	02	00000003	\N	F	\N	2013-01-12	1	\N
00000051	02	00000003	\N	D	\N	2013-01-13	1	\N
00000051	02	00000003	\N	D	\N	2013-01-14	1	\N
00000051	02	00000003	\N	D	\N	2013-01-15	1	\N
00000051	02	00000003	\N	D	\N	2013-01-16	1	\N
00000051	02	00000003	\N	D	\N	2013-01-17	1	\N
00000051	02	00000003	\N	F	\N	2013-01-18	1	\N
00000051	02	00000003	\N	F	\N	2013-01-19	1	\N
00000051	02	00000003	\N	D	\N	2013-01-20	1	\N
00000051	02	00000003	\N	D	\N	2013-01-21	1	\N
00000051	02	00000003	\N	D	\N	2013-01-22	1	\N
00000051	02	00000003	\N	D	\N	2013-01-23	1	\N
00000051	02	00000003	\N	D	\N	2013-01-24	1	\N
00000051	02	00000003	\N	F	\N	2013-01-25	1	\N
00000051	02	00000003	\N	F	\N	2013-01-26	1	\N
00000051	02	00000003	\N	D	\N	2013-01-27	1	\N
00000051	02	00000003	\N	D	\N	2013-01-28	1	\N
00000051	02	00000003	\N	D	\N	2013-01-29	1	\N
00000051	02	00000003	\N	D	\N	2013-01-30	1	\N
00000051	02	00000003	\N	D	\N	2013-01-31	1	\N
00000049	02	00000003	\N	F	\N	2013-02-01	1	\N
00000049	02	00000003	\N	F	\N	2013-02-02	1	\N
00000049	02	00000003	\N	D	\N	2013-02-03	1	\N
00000049	02	00000003	\N	D	\N	2013-02-04	1	\N
00000049	02	00000003	\N	D	\N	2013-02-05	1	\N
00000049	02	00000003	\N	D	\N	2013-02-06	1	\N
00000049	02	00000003	\N	D	\N	2013-02-07	1	\N
00000049	02	00000003	\N	F	\N	2013-02-08	1	\N
00000049	02	00000003	\N	F	\N	2013-02-09	1	\N
00000049	02	00000003	\N	D	\N	2013-02-10	1	\N
00000049	02	00000003	\N	D	\N	2013-02-11	1	\N
00000049	02	00000003	\N	D	\N	2013-02-12	1	\N
00000049	02	00000003	\N	D	\N	2013-02-13	1	\N
00000049	02	00000003	\N	D	\N	2013-02-14	1	\N
00000049	02	00000003	\N	F	\N	2013-02-15	1	\N
00000049	02	00000003	\N	F	\N	2013-02-16	1	\N
00000049	02	00000003	\N	D	\N	2013-02-17	1	\N
00000049	02	00000003	\N	D	\N	2013-02-18	1	\N
00000049	02	00000003	\N	D	\N	2013-02-19	1	\N
00000049	02	00000003	\N	D	\N	2013-02-20	1	\N
00000049	02	00000003	\N	D	\N	2013-02-21	1	\N
00000049	02	00000003	\N	F	\N	2013-02-22	1	\N
00000049	02	00000003	\N	F	\N	2013-02-23	1	\N
00000049	02	00000003	\N	D	\N	2013-02-24	1	\N
00000049	02	00000003	\N	D	\N	2013-02-25	1	\N
00000049	02	00000003	\N	D	\N	2013-02-26	1	\N
00000049	02	00000003	\N	D	\N	2013-02-27	1	\N
00000049	02	00000003	\N	D	\N	2013-02-28	1	\N
00000050	02	00000003	\N	F	\N	2013-02-01	1	\N
00000050	02	00000003	\N	F	\N	2013-02-02	1	\N
00000050	02	00000003	\N	D	\N	2013-02-03	1	\N
00000050	02	00000003	\N	D	\N	2013-02-04	1	\N
00000050	02	00000003	\N	D	\N	2013-02-05	1	\N
00000050	02	00000003	\N	D	\N	2013-02-06	1	\N
00000050	02	00000003	\N	D	\N	2013-02-07	1	\N
00000050	02	00000003	\N	F	\N	2013-02-08	1	\N
00000050	02	00000003	\N	F	\N	2013-02-09	1	\N
00000050	02	00000003	\N	D	\N	2013-02-10	1	\N
00000050	02	00000003	\N	D	\N	2013-02-11	1	\N
00000050	02	00000003	\N	D	\N	2013-02-12	1	\N
00000050	02	00000003	\N	D	\N	2013-02-13	1	\N
00000050	02	00000003	\N	D	\N	2013-02-14	1	\N
00000050	02	00000003	\N	F	\N	2013-02-15	1	\N
00000050	02	00000003	\N	F	\N	2013-02-16	1	\N
00000050	02	00000003	\N	D	\N	2013-02-17	1	\N
00000050	02	00000003	\N	D	\N	2013-02-18	1	\N
00000050	02	00000003	\N	D	\N	2013-02-19	1	\N
00000050	02	00000003	\N	D	\N	2013-02-20	1	\N
00000050	02	00000003	\N	D	\N	2013-02-21	1	\N
00000050	02	00000003	\N	F	\N	2013-02-22	1	\N
00000050	02	00000003	\N	F	\N	2013-02-23	1	\N
00000050	02	00000003	\N	D	\N	2013-02-24	1	\N
00000050	02	00000003	\N	D	\N	2013-02-25	1	\N
00000050	02	00000003	\N	D	\N	2013-02-26	1	\N
00000050	02	00000003	\N	D	\N	2013-02-27	1	\N
00000050	02	00000003	\N	D	\N	2013-02-28	1	\N
00000051	02	00000003	\N	F	\N	2013-02-01	1	\N
00000051	02	00000003	\N	F	\N	2013-02-02	1	\N
00000051	02	00000003	\N	D	\N	2013-02-03	1	\N
00000051	02	00000003	\N	D	\N	2013-02-04	1	\N
00000051	02	00000003	\N	D	\N	2013-02-05	1	\N
00000051	02	00000003	\N	D	\N	2013-02-06	1	\N
00000051	02	00000003	\N	D	\N	2013-02-07	1	\N
00000051	02	00000003	\N	F	\N	2013-02-08	1	\N
00000051	02	00000003	\N	F	\N	2013-02-09	1	\N
00000051	02	00000003	\N	D	\N	2013-02-10	1	\N
00000051	02	00000003	\N	D	\N	2013-02-11	1	\N
00000051	02	00000003	\N	D	\N	2013-02-12	1	\N
00000051	02	00000003	\N	D	\N	2013-02-13	1	\N
00000051	02	00000003	\N	D	\N	2013-02-14	1	\N
00000051	02	00000003	\N	F	\N	2013-02-15	1	\N
00000051	02	00000003	\N	F	\N	2013-02-16	1	\N
00000051	02	00000003	\N	D	\N	2013-02-17	1	\N
00000051	02	00000003	\N	D	\N	2013-02-18	1	\N
00000051	02	00000003	\N	D	\N	2013-02-19	1	\N
00000051	02	00000003	\N	D	\N	2013-02-20	1	\N
00000051	02	00000003	\N	D	\N	2013-02-21	1	\N
00000051	02	00000003	\N	F	\N	2013-02-22	1	\N
00000051	02	00000003	\N	F	\N	2013-02-23	1	\N
00000051	02	00000003	\N	D	\N	2013-02-24	1	\N
00000051	02	00000003	\N	D	\N	2013-02-25	1	\N
00000051	02	00000003	\N	D	\N	2013-02-26	1	\N
00000051	02	00000003	\N	D	\N	2013-02-27	1	\N
00000051	02	00000003	\N	D	\N	2013-02-28	1	\N
00000053	04	00000003	\N	D	\N	2013-01-01	1	\N
00000053	04	00000003	\N	D	\N	2013-01-02	1	\N
00000053	04	00000003	\N	D	\N	2013-01-03	1	\N
00000053	04	00000003	\N	F	\N	2013-01-04	1	\N
00000053	04	00000003	\N	F	\N	2013-01-05	1	\N
00000053	04	00000003	\N	D	\N	2013-01-06	1	\N
00000053	04	00000003	\N	D	\N	2013-01-07	1	\N
00000053	04	00000003	\N	D	\N	2013-01-08	1	\N
00000053	04	00000003	\N	D	\N	2013-01-09	1	\N
00000053	04	00000003	\N	D	\N	2013-01-10	1	\N
00000053	04	00000003	\N	F	\N	2013-01-11	1	\N
00000053	04	00000003	\N	F	\N	2013-01-12	1	\N
00000053	04	00000003	\N	D	\N	2013-01-13	1	\N
00000053	04	00000003	\N	D	\N	2013-01-14	1	\N
00000053	04	00000003	\N	D	\N	2013-01-15	1	\N
00000053	04	00000003	\N	D	\N	2013-01-16	1	\N
00000053	04	00000003	\N	D	\N	2013-01-17	1	\N
00000053	04	00000003	\N	F	\N	2013-01-18	1	\N
00000053	04	00000003	\N	F	\N	2013-01-19	1	\N
00000053	04	00000003	\N	D	\N	2013-01-20	1	\N
00000053	04	00000003	\N	D	\N	2013-01-21	1	\N
00000053	04	00000003	\N	D	\N	2013-01-22	1	\N
00000053	04	00000003	\N	D	\N	2013-01-23	1	\N
00000053	04	00000003	\N	D	\N	2013-01-24	1	\N
00000053	04	00000003	\N	F	\N	2013-01-25	1	\N
00000053	04	00000003	\N	F	\N	2013-01-26	1	\N
00000053	04	00000003	\N	D	\N	2013-01-27	1	\N
00000053	04	00000003	\N	D	\N	2013-01-28	1	\N
00000053	04	00000003	\N	D	\N	2013-01-29	1	\N
00000053	04	00000003	\N	D	\N	2013-01-30	1	\N
00000053	04	00000003	\N	D	\N	2013-01-31	1	\N
00000054	04	00000003	\N	D	\N	2013-01-01	1	\N
00000054	04	00000003	\N	D	\N	2013-01-02	1	\N
00000054	04	00000003	\N	D	\N	2013-01-03	1	\N
00000054	04	00000003	\N	F	\N	2013-01-04	1	\N
00000054	04	00000003	\N	F	\N	2013-01-05	1	\N
00000054	04	00000003	\N	D	\N	2013-01-06	1	\N
00000054	04	00000003	\N	D	\N	2013-01-07	1	\N
00000054	04	00000003	\N	D	\N	2013-01-08	1	\N
00000054	04	00000003	\N	D	\N	2013-01-09	1	\N
00000054	04	00000003	\N	D	\N	2013-01-10	1	\N
00000054	04	00000003	\N	F	\N	2013-01-11	1	\N
00000054	04	00000003	\N	F	\N	2013-01-12	1	\N
00000054	04	00000003	\N	D	\N	2013-01-13	1	\N
00000054	04	00000003	\N	D	\N	2013-01-14	1	\N
00000054	04	00000003	\N	D	\N	2013-01-15	1	\N
00000054	04	00000003	\N	D	\N	2013-01-16	1	\N
00000054	04	00000003	\N	D	\N	2013-01-17	1	\N
00000054	04	00000003	\N	F	\N	2013-01-18	1	\N
00000054	04	00000003	\N	F	\N	2013-01-19	1	\N
00000054	04	00000003	\N	D	\N	2013-01-20	1	\N
00000054	04	00000003	\N	D	\N	2013-01-21	1	\N
00000054	04	00000003	\N	D	\N	2013-01-22	1	\N
00000054	04	00000003	\N	D	\N	2013-01-23	1	\N
00000054	04	00000003	\N	D	\N	2013-01-24	1	\N
00000054	04	00000003	\N	F	\N	2013-01-25	1	\N
00000054	04	00000003	\N	F	\N	2013-01-26	1	\N
00000054	04	00000003	\N	D	\N	2013-01-27	1	\N
00000054	04	00000003	\N	D	\N	2013-01-28	1	\N
00000054	04	00000003	\N	D	\N	2013-01-29	1	\N
00000054	04	00000003	\N	D	\N	2013-01-30	1	\N
00000054	04	00000003	\N	D	\N	2013-01-31	1	\N
00000055	04	00000003	\N	D	\N	2013-01-01	1	\N
00000055	04	00000003	\N	D	\N	2013-01-02	1	\N
00000055	04	00000003	\N	D	\N	2013-01-03	1	\N
00000055	04	00000003	\N	F	\N	2013-01-04	1	\N
00000055	04	00000003	\N	F	\N	2013-01-05	1	\N
00000055	04	00000003	\N	D	\N	2013-01-06	1	\N
00000055	04	00000003	\N	D	\N	2013-01-07	1	\N
00000055	04	00000003	\N	D	\N	2013-01-08	1	\N
00000055	04	00000003	\N	D	\N	2013-01-09	1	\N
00000055	04	00000003	\N	D	\N	2013-01-10	1	\N
00000055	04	00000003	\N	F	\N	2013-01-11	1	\N
00000055	04	00000003	\N	F	\N	2013-01-12	1	\N
00000055	04	00000003	\N	D	\N	2013-01-13	1	\N
00000055	04	00000003	\N	D	\N	2013-01-14	1	\N
00000055	04	00000003	\N	D	\N	2013-01-15	1	\N
00000055	04	00000003	\N	D	\N	2013-01-16	1	\N
00000055	04	00000003	\N	D	\N	2013-01-17	1	\N
00000055	04	00000003	\N	F	\N	2013-01-18	1	\N
00000055	04	00000003	\N	F	\N	2013-01-19	1	\N
00000055	04	00000003	\N	D	\N	2013-01-20	1	\N
00000055	04	00000003	\N	D	\N	2013-01-21	1	\N
00000055	04	00000003	\N	D	\N	2013-01-22	1	\N
00000055	04	00000003	\N	D	\N	2013-01-23	1	\N
00000055	04	00000003	\N	D	\N	2013-01-24	1	\N
00000055	04	00000003	\N	F	\N	2013-01-25	1	\N
00000055	04	00000003	\N	F	\N	2013-01-26	1	\N
00000055	04	00000003	\N	D	\N	2013-01-27	1	\N
00000055	04	00000003	\N	D	\N	2013-01-28	1	\N
00000055	04	00000003	\N	D	\N	2013-01-29	1	\N
00000055	04	00000003	\N	D	\N	2013-01-30	1	\N
00000055	04	00000003	\N	D	\N	2013-01-31	1	\N
00000056	04	00000003	\N	D	\N	2013-01-01	1	\N
00000056	04	00000003	\N	D	\N	2013-01-02	1	\N
00000056	04	00000003	\N	D	\N	2013-01-03	1	\N
00000056	04	00000003	\N	F	\N	2013-01-04	1	\N
00000056	04	00000003	\N	F	\N	2013-01-05	1	\N
00000056	04	00000003	\N	D	\N	2013-01-06	1	\N
00000056	04	00000003	\N	D	\N	2013-01-07	1	\N
00000056	04	00000003	\N	D	\N	2013-01-08	1	\N
00000056	04	00000003	\N	D	\N	2013-01-09	1	\N
00000056	04	00000003	\N	D	\N	2013-01-10	1	\N
00000056	04	00000003	\N	F	\N	2013-01-11	1	\N
00000056	04	00000003	\N	F	\N	2013-01-12	1	\N
00000056	04	00000003	\N	D	\N	2013-01-13	1	\N
00000056	04	00000003	\N	D	\N	2013-01-14	1	\N
00000056	04	00000003	\N	D	\N	2013-01-15	1	\N
00000056	04	00000003	\N	D	\N	2013-01-16	1	\N
00000056	04	00000003	\N	D	\N	2013-01-17	1	\N
00000056	04	00000003	\N	F	\N	2013-01-18	1	\N
00000056	04	00000003	\N	F	\N	2013-01-19	1	\N
00000056	04	00000003	\N	D	\N	2013-01-20	1	\N
00000056	04	00000003	\N	D	\N	2013-01-21	1	\N
00000056	04	00000003	\N	D	\N	2013-01-22	1	\N
00000056	04	00000003	\N	D	\N	2013-01-23	1	\N
00000056	04	00000003	\N	D	\N	2013-01-24	1	\N
00000056	04	00000003	\N	F	\N	2013-01-25	1	\N
00000056	04	00000003	\N	F	\N	2013-01-26	1	\N
00000056	04	00000003	\N	D	\N	2013-01-27	1	\N
00000056	04	00000003	\N	D	\N	2013-01-28	1	\N
00000056	04	00000003	\N	D	\N	2013-01-29	1	\N
00000056	04	00000003	\N	D	\N	2013-01-30	1	\N
00000056	04	00000003	\N	D	\N	2013-01-31	1	\N
00000057	04	00000003	\N	D	\N	2013-01-01	1	\N
00000057	04	00000003	\N	D	\N	2013-01-02	1	\N
00000057	04	00000003	\N	D	\N	2013-01-03	1	\N
00000057	04	00000003	\N	F	\N	2013-01-04	1	\N
00000057	04	00000003	\N	F	\N	2013-01-05	1	\N
00000057	04	00000003	\N	D	\N	2013-01-06	1	\N
00000057	04	00000003	\N	D	\N	2013-01-07	1	\N
00000057	04	00000003	\N	D	\N	2013-01-08	1	\N
00000057	04	00000003	\N	D	\N	2013-01-09	1	\N
00000057	04	00000003	\N	D	\N	2013-01-10	1	\N
00000057	04	00000003	\N	F	\N	2013-01-11	1	\N
00000057	04	00000003	\N	F	\N	2013-01-12	1	\N
00000057	04	00000003	\N	D	\N	2013-01-13	1	\N
00000057	04	00000003	\N	D	\N	2013-01-14	1	\N
00000057	04	00000003	\N	D	\N	2013-01-15	1	\N
00000057	04	00000003	\N	D	\N	2013-01-16	1	\N
00000057	04	00000003	\N	D	\N	2013-01-17	1	\N
00000057	04	00000003	\N	F	\N	2013-01-18	1	\N
00000057	04	00000003	\N	F	\N	2013-01-19	1	\N
00000057	04	00000003	\N	D	\N	2013-01-20	1	\N
00000057	04	00000003	\N	D	\N	2013-01-21	1	\N
00000057	04	00000003	\N	D	\N	2013-01-22	1	\N
00000057	04	00000003	\N	D	\N	2013-01-23	1	\N
00000057	04	00000003	\N	D	\N	2013-01-24	1	\N
00000057	04	00000003	\N	F	\N	2013-01-25	1	\N
00000057	04	00000003	\N	F	\N	2013-01-26	1	\N
00000057	04	00000003	\N	D	\N	2013-01-27	1	\N
00000057	04	00000003	\N	D	\N	2013-01-28	1	\N
00000057	04	00000003	\N	D	\N	2013-01-29	1	\N
00000057	04	00000003	\N	D	\N	2013-01-30	1	\N
00000057	04	00000003	\N	D	\N	2013-01-31	1	\N
00000053	04	00000003	\N	F	\N	2013-02-01	1	\N
00000053	04	00000003	\N	F	\N	2013-02-02	1	\N
00000053	04	00000003	\N	D	\N	2013-02-03	1	\N
00000053	04	00000003	\N	D	\N	2013-02-04	1	\N
00000053	04	00000003	\N	D	\N	2013-02-05	1	\N
00000053	04	00000003	\N	D	\N	2013-02-06	1	\N
00000053	04	00000003	\N	D	\N	2013-02-07	1	\N
00000053	04	00000003	\N	F	\N	2013-02-08	1	\N
00000053	04	00000003	\N	F	\N	2013-02-09	1	\N
00000053	04	00000003	\N	D	\N	2013-02-10	1	\N
00000053	04	00000003	\N	D	\N	2013-02-11	1	\N
00000053	04	00000003	\N	D	\N	2013-02-12	1	\N
00000053	04	00000003	\N	D	\N	2013-02-13	1	\N
00000053	04	00000003	\N	D	\N	2013-02-14	1	\N
00000053	04	00000003	\N	F	\N	2013-02-15	1	\N
00000053	04	00000003	\N	F	\N	2013-02-16	1	\N
00000053	04	00000003	\N	D	\N	2013-02-17	1	\N
00000053	04	00000003	\N	D	\N	2013-02-18	1	\N
00000053	04	00000003	\N	D	\N	2013-02-19	1	\N
00000053	04	00000003	\N	D	\N	2013-02-20	1	\N
00000053	04	00000003	\N	D	\N	2013-02-21	1	\N
00000053	04	00000003	\N	F	\N	2013-02-22	1	\N
00000053	04	00000003	\N	F	\N	2013-02-23	1	\N
00000053	04	00000003	\N	D	\N	2013-02-24	1	\N
00000053	04	00000003	\N	D	\N	2013-02-25	1	\N
00000053	04	00000003	\N	D	\N	2013-02-26	1	\N
00000053	04	00000003	\N	D	\N	2013-02-27	1	\N
00000053	04	00000003	\N	D	\N	2013-02-28	1	\N
00000054	04	00000003	\N	F	\N	2013-02-01	1	\N
00000054	04	00000003	\N	F	\N	2013-02-02	1	\N
00000054	04	00000003	\N	D	\N	2013-02-03	1	\N
00000054	04	00000003	\N	D	\N	2013-02-04	1	\N
00000054	04	00000003	\N	D	\N	2013-02-05	1	\N
00000054	04	00000003	\N	D	\N	2013-02-06	1	\N
00000054	04	00000003	\N	D	\N	2013-02-07	1	\N
00000054	04	00000003	\N	F	\N	2013-02-08	1	\N
00000054	04	00000003	\N	F	\N	2013-02-09	1	\N
00000054	04	00000003	\N	D	\N	2013-02-10	1	\N
00000054	04	00000003	\N	D	\N	2013-02-11	1	\N
00000054	04	00000003	\N	D	\N	2013-02-12	1	\N
00000054	04	00000003	\N	D	\N	2013-02-13	1	\N
00000054	04	00000003	\N	D	\N	2013-02-14	1	\N
00000054	04	00000003	\N	F	\N	2013-02-15	1	\N
00000054	04	00000003	\N	F	\N	2013-02-16	1	\N
00000054	04	00000003	\N	D	\N	2013-02-17	1	\N
00000054	04	00000003	\N	D	\N	2013-02-18	1	\N
00000054	04	00000003	\N	D	\N	2013-02-19	1	\N
00000054	04	00000003	\N	D	\N	2013-02-20	1	\N
00000054	04	00000003	\N	D	\N	2013-02-21	1	\N
00000054	04	00000003	\N	F	\N	2013-02-22	1	\N
00000054	04	00000003	\N	F	\N	2013-02-23	1	\N
00000054	04	00000003	\N	D	\N	2013-02-24	1	\N
00000054	04	00000003	\N	D	\N	2013-02-25	1	\N
00000054	04	00000003	\N	D	\N	2013-02-26	1	\N
00000054	04	00000003	\N	D	\N	2013-02-27	1	\N
00000054	04	00000003	\N	D	\N	2013-02-28	1	\N
00000055	04	00000003	\N	F	\N	2013-02-01	1	\N
00000055	04	00000003	\N	F	\N	2013-02-02	1	\N
00000055	04	00000003	\N	D	\N	2013-02-03	1	\N
00000055	04	00000003	\N	D	\N	2013-02-04	1	\N
00000055	04	00000003	\N	D	\N	2013-02-05	1	\N
00000055	04	00000003	\N	D	\N	2013-02-06	1	\N
00000055	04	00000003	\N	D	\N	2013-02-07	1	\N
00000055	04	00000003	\N	F	\N	2013-02-08	1	\N
00000055	04	00000003	\N	F	\N	2013-02-09	1	\N
00000055	04	00000003	\N	D	\N	2013-02-10	1	\N
00000055	04	00000003	\N	D	\N	2013-02-11	1	\N
00000055	04	00000003	\N	D	\N	2013-02-12	1	\N
00000055	04	00000003	\N	D	\N	2013-02-13	1	\N
00000055	04	00000003	\N	D	\N	2013-02-14	1	\N
00000055	04	00000003	\N	F	\N	2013-02-15	1	\N
00000055	04	00000003	\N	F	\N	2013-02-16	1	\N
00000055	04	00000003	\N	D	\N	2013-02-17	1	\N
00000055	04	00000003	\N	D	\N	2013-02-18	1	\N
00000055	04	00000003	\N	D	\N	2013-02-19	1	\N
00000055	04	00000003	\N	D	\N	2013-02-20	1	\N
00000055	04	00000003	\N	D	\N	2013-02-21	1	\N
00000055	04	00000003	\N	F	\N	2013-02-22	1	\N
00000055	04	00000003	\N	F	\N	2013-02-23	1	\N
00000055	04	00000003	\N	D	\N	2013-02-24	1	\N
00000055	04	00000003	\N	D	\N	2013-02-25	1	\N
00000055	04	00000003	\N	D	\N	2013-02-26	1	\N
00000055	04	00000003	\N	D	\N	2013-02-27	1	\N
00000055	04	00000003	\N	D	\N	2013-02-28	1	\N
00000056	04	00000003	\N	F	\N	2013-02-01	1	\N
00000056	04	00000003	\N	F	\N	2013-02-02	1	\N
00000056	04	00000003	\N	D	\N	2013-02-03	1	\N
00000056	04	00000003	\N	D	\N	2013-02-04	1	\N
00000056	04	00000003	\N	D	\N	2013-02-05	1	\N
00000056	04	00000003	\N	D	\N	2013-02-06	1	\N
00000056	04	00000003	\N	D	\N	2013-02-07	1	\N
00000056	04	00000003	\N	F	\N	2013-02-08	1	\N
00000056	04	00000003	\N	F	\N	2013-02-09	1	\N
00000056	04	00000003	\N	D	\N	2013-02-10	1	\N
00000056	04	00000003	\N	D	\N	2013-02-11	1	\N
00000056	04	00000003	\N	D	\N	2013-02-12	1	\N
00000056	04	00000003	\N	D	\N	2013-02-13	1	\N
00000056	04	00000003	\N	D	\N	2013-02-14	1	\N
00000056	04	00000003	\N	F	\N	2013-02-15	1	\N
00000056	04	00000003	\N	F	\N	2013-02-16	1	\N
00000056	04	00000003	\N	D	\N	2013-02-17	1	\N
00000056	04	00000003	\N	D	\N	2013-02-18	1	\N
00000056	04	00000003	\N	D	\N	2013-02-19	1	\N
00000056	04	00000003	\N	D	\N	2013-02-20	1	\N
00000056	04	00000003	\N	D	\N	2013-02-21	1	\N
00000056	04	00000003	\N	F	\N	2013-02-22	1	\N
00000056	04	00000003	\N	F	\N	2013-02-23	1	\N
00000056	04	00000003	\N	D	\N	2013-02-24	1	\N
00000056	04	00000003	\N	D	\N	2013-02-25	1	\N
00000056	04	00000003	\N	D	\N	2013-02-26	1	\N
00000056	04	00000003	\N	D	\N	2013-02-27	1	\N
00000056	04	00000003	\N	D	\N	2013-02-28	1	\N
00000057	04	00000003	\N	F	\N	2013-02-01	1	\N
00000057	04	00000003	\N	F	\N	2013-02-02	1	\N
00000057	04	00000003	\N	D	\N	2013-02-03	1	\N
00000057	04	00000003	\N	D	\N	2013-02-04	1	\N
00000057	04	00000003	\N	D	\N	2013-02-05	1	\N
00000057	04	00000003	\N	D	\N	2013-02-06	1	\N
00000057	04	00000003	\N	D	\N	2013-02-07	1	\N
00000057	04	00000003	\N	F	\N	2013-02-08	1	\N
00000057	04	00000003	\N	F	\N	2013-02-09	1	\N
00000057	04	00000003	\N	D	\N	2013-02-10	1	\N
00000057	04	00000003	\N	D	\N	2013-02-11	1	\N
00000057	04	00000003	\N	D	\N	2013-02-12	1	\N
00000057	04	00000003	\N	D	\N	2013-02-13	1	\N
00000057	04	00000003	\N	D	\N	2013-02-14	1	\N
00000057	04	00000003	\N	F	\N	2013-02-15	1	\N
00000057	04	00000003	\N	F	\N	2013-02-16	1	\N
00000057	04	00000003	\N	D	\N	2013-02-17	1	\N
00000057	04	00000003	\N	D	\N	2013-02-18	1	\N
00000057	04	00000003	\N	D	\N	2013-02-19	1	\N
00000057	04	00000003	\N	D	\N	2013-02-20	1	\N
00000057	04	00000003	\N	D	\N	2013-02-21	1	\N
00000057	04	00000003	\N	F	\N	2013-02-22	1	\N
00000057	04	00000003	\N	F	\N	2013-02-23	1	\N
00000057	04	00000003	\N	D	\N	2013-02-24	1	\N
00000057	04	00000003	\N	D	\N	2013-02-25	1	\N
00000057	04	00000003	\N	D	\N	2013-02-26	1	\N
00000057	04	00000003	\N	D	\N	2013-02-27	1	\N
00000057	04	00000003	\N	D	\N	2013-02-28	1	\N
00000052	03	00000003	\N	D	\N	2013-01-01	1	\N
00000052	03	00000003	\N	D	\N	2013-01-02	1	\N
00000052	03	00000003	\N	D	\N	2013-01-03	1	\N
00000052	03	00000003	\N	F	\N	2013-01-04	1	\N
00000052	03	00000003	\N	F	\N	2013-01-05	1	\N
00000052	03	00000003	\N	D	\N	2013-01-06	1	\N
00000052	03	00000003	\N	D	\N	2013-01-07	1	\N
00000052	03	00000003	\N	D	\N	2013-01-08	1	\N
00000052	03	00000003	\N	D	\N	2013-01-09	1	\N
00000052	03	00000003	\N	D	\N	2013-01-10	1	\N
00000052	03	00000003	\N	F	\N	2013-01-11	1	\N
00000052	03	00000003	\N	F	\N	2013-01-12	1	\N
00000052	03	00000003	\N	D	\N	2013-01-13	1	\N
00000052	03	00000003	\N	D	\N	2013-01-14	1	\N
00000052	03	00000003	\N	D	\N	2013-01-15	1	\N
00000052	03	00000003	\N	D	\N	2013-01-16	1	\N
00000052	03	00000003	\N	D	\N	2013-01-17	1	\N
00000052	03	00000003	\N	F	\N	2013-01-18	1	\N
00000052	03	00000003	\N	F	\N	2013-01-19	1	\N
00000052	03	00000003	\N	D	\N	2013-01-20	1	\N
00000052	03	00000003	\N	D	\N	2013-01-21	1	\N
00000052	03	00000003	\N	D	\N	2013-01-22	1	\N
00000052	03	00000003	\N	D	\N	2013-01-23	1	\N
00000052	03	00000003	\N	D	\N	2013-01-24	1	\N
00000052	03	00000003	\N	F	\N	2013-01-25	1	\N
00000052	03	00000003	\N	F	\N	2013-01-26	1	\N
00000052	03	00000003	\N	D	\N	2013-01-27	1	\N
00000052	03	00000003	\N	D	\N	2013-01-28	1	\N
00000052	03	00000003	\N	D	\N	2013-01-29	1	\N
00000052	03	00000003	\N	D	\N	2013-01-30	1	\N
00000052	03	00000003	\N	D	\N	2013-01-31	1	\N
00000052	03	00000003	\N	F	\N	2013-02-01	1	\N
00000052	03	00000003	\N	F	\N	2013-02-02	1	\N
00000052	03	00000003	\N	D	\N	2013-02-03	1	\N
00000052	03	00000003	\N	D	\N	2013-02-04	1	\N
00000052	03	00000003	\N	D	\N	2013-02-05	1	\N
00000052	03	00000003	\N	D	\N	2013-02-06	1	\N
00000052	03	00000003	\N	D	\N	2013-02-07	1	\N
00000052	03	00000003	\N	F	\N	2013-02-08	1	\N
00000052	03	00000003	\N	F	\N	2013-02-09	1	\N
00000052	03	00000003	\N	D	\N	2013-02-10	1	\N
00000052	03	00000003	\N	D	\N	2013-02-11	1	\N
00000052	03	00000003	\N	D	\N	2013-02-12	1	\N
00000052	03	00000003	\N	D	\N	2013-02-13	1	\N
00000052	03	00000003	\N	D	\N	2013-02-14	1	\N
00000052	03	00000003	\N	F	\N	2013-02-15	1	\N
00000052	03	00000003	\N	F	\N	2013-02-16	1	\N
00000052	03	00000003	\N	D	\N	2013-02-17	1	\N
00000052	03	00000003	\N	D	\N	2013-02-18	1	\N
00000052	03	00000003	\N	D	\N	2013-02-19	1	\N
00000052	03	00000003	\N	D	\N	2013-02-20	1	\N
00000052	03	00000003	\N	D	\N	2013-02-21	1	\N
00000052	03	00000003	\N	F	\N	2013-02-22	1	\N
00000052	03	00000003	\N	F	\N	2013-02-23	1	\N
00000052	03	00000003	\N	D	\N	2013-02-24	1	\N
00000052	03	00000003	\N	D	\N	2013-02-25	1	\N
00000052	03	00000003	\N	D	\N	2013-02-26	1	\N
00000052	03	00000003	\N	D	\N	2013-02-27	1	\N
00000052	03	00000003	\N	D	\N	2013-02-28	1	\N
00000042	01	00000003	\N	D	0000000046	2013-01-03	R	\N
00000043	01	00000003	\N	D	0000000049	2013-01-09	R	\N
00000043	01	00000003	\N	D	0000000049	2013-01-10	R	\N
00000043	01	00000003	\N	F	0000000049	2013-01-11	R	\N
00000001	01	00000002	\N	F	0000000050	2013-01-11	R	\N
00000001	01	00000002	\N	F	0000000050	2013-01-12	R	\N
00000001	01	00000002	\N	D	0000000050	2013-01-13	R	\N
00000001	01	00000002	\N	D	0000000050	2013-01-14	R	\N
00000019	02	00000002	\N	F	0000000051	2013-01-11	R	\N
00000019	02	00000002	\N	F	0000000051	2013-01-12	R	\N
00000019	02	00000002	\N	D	0000000051	2013-01-13	R	\N
00000019	02	00000002	\N	D	0000000051	2013-01-14	R	\N
00000002	01	00000002	\N	F	0000000052	2013-01-12	R	\N
00000002	01	00000002	\N	D	0000000052	2013-01-13	R	\N
00000002	01	00000002	\N	D	0000000052	2013-01-14	R	\N
00000002	01	00000002	\N	D	0000000052	2013-01-15	R	\N
00000049	02	00000003	\N	F	0000000054	2013-01-11	R	\N
00000049	02	00000003	\N	F	0000000054	2013-01-12	R	\N
00000049	02	00000003	\N	D	0000000054	2013-01-13	R	\N
00000049	02	00000003	\N	D	0000000054	2013-01-14	R	\N
00000010	03	00000002	\N	F	\N	2013-03-01	1	\N
00000010	03	00000002	\N	F	\N	2013-03-02	1	\N
00000010	03	00000002	\N	D	\N	2013-03-03	1	\N
00000010	03	00000002	\N	D	\N	2013-03-04	1	\N
00000010	03	00000002	\N	D	\N	2013-03-05	1	\N
00000010	03	00000002	\N	D	\N	2013-03-06	1	\N
00000010	03	00000002	\N	D	\N	2013-03-07	1	\N
00000010	03	00000002	\N	F	\N	2013-03-08	1	\N
00000010	03	00000002	\N	F	\N	2013-03-09	1	\N
00000010	03	00000002	\N	D	\N	2013-03-10	1	\N
00000010	03	00000002	\N	D	\N	2013-03-11	1	\N
00000010	03	00000002	\N	D	\N	2013-03-12	1	\N
00000010	03	00000002	\N	D	\N	2013-03-13	1	\N
00000010	03	00000002	\N	D	\N	2013-03-14	1	\N
00000010	03	00000002	\N	F	\N	2013-03-15	1	\N
00000010	03	00000002	\N	F	\N	2013-03-16	1	\N
00000010	03	00000002	\N	D	\N	2013-03-17	1	\N
00000010	03	00000002	\N	D	\N	2013-03-18	1	\N
00000010	03	00000002	\N	D	\N	2013-03-19	1	\N
00000010	03	00000002	\N	D	\N	2013-03-20	1	\N
00000010	03	00000002	\N	D	\N	2013-03-21	1	\N
00000010	03	00000002	\N	F	\N	2013-03-22	1	\N
00000010	03	00000002	\N	F	\N	2013-03-23	1	\N
00000010	03	00000002	\N	D	\N	2013-03-24	1	\N
00000010	03	00000002	\N	D	\N	2013-03-25	1	\N
00000010	03	00000002	\N	D	\N	2013-03-26	1	\N
00000010	03	00000002	\N	D	\N	2013-03-27	1	\N
00000010	03	00000002	\N	D	\N	2013-03-28	1	\N
00000010	03	00000002	\N	F	\N	2013-03-29	1	\N
00000010	03	00000002	\N	F	\N	2013-03-30	1	\N
00000010	03	00000002	\N	D	\N	2013-03-31	1	\N
00000011	03	00000002	\N	F	\N	2013-03-01	1	\N
00000011	03	00000002	\N	F	\N	2013-03-02	1	\N
00000011	03	00000002	\N	D	\N	2013-03-03	1	\N
00000011	03	00000002	\N	D	\N	2013-03-04	1	\N
00000011	03	00000002	\N	D	\N	2013-03-05	1	\N
00000011	03	00000002	\N	D	\N	2013-03-06	1	\N
00000011	03	00000002	\N	D	\N	2013-03-07	1	\N
00000011	03	00000002	\N	F	\N	2013-03-08	1	\N
00000011	03	00000002	\N	F	\N	2013-03-09	1	\N
00000011	03	00000002	\N	D	\N	2013-03-10	1	\N
00000011	03	00000002	\N	D	\N	2013-03-11	1	\N
00000011	03	00000002	\N	D	\N	2013-03-12	1	\N
00000011	03	00000002	\N	D	\N	2013-03-13	1	\N
00000011	03	00000002	\N	D	\N	2013-03-14	1	\N
00000011	03	00000002	\N	F	\N	2013-03-15	1	\N
00000011	03	00000002	\N	F	\N	2013-03-16	1	\N
00000011	03	00000002	\N	D	\N	2013-03-17	1	\N
00000011	03	00000002	\N	D	\N	2013-03-18	1	\N
00000011	03	00000002	\N	D	\N	2013-03-19	1	\N
00000011	03	00000002	\N	D	\N	2013-03-20	1	\N
00000011	03	00000002	\N	D	\N	2013-03-21	1	\N
00000011	03	00000002	\N	F	\N	2013-03-22	1	\N
00000011	03	00000002	\N	F	\N	2013-03-23	1	\N
00000011	03	00000002	\N	D	\N	2013-03-24	1	\N
00000011	03	00000002	\N	D	\N	2013-03-25	1	\N
00000011	03	00000002	\N	D	\N	2013-03-26	1	\N
00000011	03	00000002	\N	D	\N	2013-03-27	1	\N
00000011	03	00000002	\N	D	\N	2013-03-28	1	\N
00000011	03	00000002	\N	F	\N	2013-03-29	1	\N
00000011	03	00000002	\N	F	\N	2013-03-30	1	\N
00000011	03	00000002	\N	D	\N	2013-03-31	1	\N
00000012	03	00000002	\N	F	\N	2013-03-01	1	\N
00000012	03	00000002	\N	F	\N	2013-03-02	1	\N
00000012	03	00000002	\N	D	\N	2013-03-03	1	\N
00000012	03	00000002	\N	D	\N	2013-03-04	1	\N
00000012	03	00000002	\N	D	\N	2013-03-05	1	\N
00000012	03	00000002	\N	D	\N	2013-03-06	1	\N
00000012	03	00000002	\N	D	\N	2013-03-07	1	\N
00000012	03	00000002	\N	F	\N	2013-03-08	1	\N
00000012	03	00000002	\N	F	\N	2013-03-09	1	\N
00000012	03	00000002	\N	D	\N	2013-03-10	1	\N
00000012	03	00000002	\N	D	\N	2013-03-11	1	\N
00000012	03	00000002	\N	D	\N	2013-03-12	1	\N
00000012	03	00000002	\N	D	\N	2013-03-13	1	\N
00000012	03	00000002	\N	D	\N	2013-03-14	1	\N
00000012	03	00000002	\N	F	\N	2013-03-15	1	\N
00000012	03	00000002	\N	F	\N	2013-03-16	1	\N
00000012	03	00000002	\N	D	\N	2013-03-17	1	\N
00000012	03	00000002	\N	D	\N	2013-03-18	1	\N
00000012	03	00000002	\N	D	\N	2013-03-19	1	\N
00000012	03	00000002	\N	D	\N	2013-03-20	1	\N
00000012	03	00000002	\N	D	\N	2013-03-21	1	\N
00000012	03	00000002	\N	F	\N	2013-03-22	1	\N
00000012	03	00000002	\N	F	\N	2013-03-23	1	\N
00000012	03	00000002	\N	D	\N	2013-03-24	1	\N
00000012	03	00000002	\N	D	\N	2013-03-25	1	\N
00000012	03	00000002	\N	D	\N	2013-03-26	1	\N
00000012	03	00000002	\N	D	\N	2013-03-27	1	\N
00000012	03	00000002	\N	D	\N	2013-03-28	1	\N
00000012	03	00000002	\N	F	\N	2013-03-29	1	\N
00000012	03	00000002	\N	F	\N	2013-03-30	1	\N
00000012	03	00000002	\N	D	\N	2013-03-31	1	\N
00000010	03	00000002	\N	D	\N	2013-04-01	1	\N
00000010	03	00000002	\N	D	\N	2013-04-02	1	\N
00000010	03	00000002	\N	D	\N	2013-04-03	1	\N
00000010	03	00000002	\N	D	\N	2013-04-04	1	\N
00000010	03	00000002	\N	F	\N	2013-04-05	1	\N
00000010	03	00000002	\N	F	\N	2013-04-06	1	\N
00000010	03	00000002	\N	D	\N	2013-04-07	1	\N
00000010	03	00000002	\N	D	\N	2013-04-08	1	\N
00000010	03	00000002	\N	D	\N	2013-04-09	1	\N
00000010	03	00000002	\N	D	\N	2013-04-10	1	\N
00000010	03	00000002	\N	D	\N	2013-04-11	1	\N
00000010	03	00000002	\N	F	\N	2013-04-12	1	\N
00000010	03	00000002	\N	F	\N	2013-04-13	1	\N
00000010	03	00000002	\N	D	\N	2013-04-14	1	\N
00000010	03	00000002	\N	D	\N	2013-04-15	1	\N
00000010	03	00000002	\N	D	\N	2013-04-16	1	\N
00000010	03	00000002	\N	D	\N	2013-04-17	1	\N
00000010	03	00000002	\N	D	\N	2013-04-18	1	\N
00000010	03	00000002	\N	F	\N	2013-04-19	1	\N
00000010	03	00000002	\N	F	\N	2013-04-20	1	\N
00000010	03	00000002	\N	D	\N	2013-04-21	1	\N
00000010	03	00000002	\N	D	\N	2013-04-22	1	\N
00000010	03	00000002	\N	D	\N	2013-04-23	1	\N
00000010	03	00000002	\N	D	\N	2013-04-24	1	\N
00000010	03	00000002	\N	D	\N	2013-04-25	1	\N
00000010	03	00000002	\N	F	\N	2013-04-26	1	\N
00000010	03	00000002	\N	F	\N	2013-04-27	1	\N
00000010	03	00000002	\N	D	\N	2013-04-28	1	\N
00000010	03	00000002	\N	D	\N	2013-04-29	1	\N
00000010	03	00000002	\N	D	\N	2013-04-30	1	\N
00000011	03	00000002	\N	D	\N	2013-04-01	1	\N
00000011	03	00000002	\N	D	\N	2013-04-02	1	\N
00000011	03	00000002	\N	D	\N	2013-04-03	1	\N
00000011	03	00000002	\N	D	\N	2013-04-04	1	\N
00000011	03	00000002	\N	F	\N	2013-04-05	1	\N
00000011	03	00000002	\N	F	\N	2013-04-06	1	\N
00000011	03	00000002	\N	D	\N	2013-04-07	1	\N
00000011	03	00000002	\N	D	\N	2013-04-08	1	\N
00000011	03	00000002	\N	D	\N	2013-04-09	1	\N
00000011	03	00000002	\N	D	\N	2013-04-10	1	\N
00000011	03	00000002	\N	D	\N	2013-04-11	1	\N
00000011	03	00000002	\N	F	\N	2013-04-12	1	\N
00000011	03	00000002	\N	F	\N	2013-04-13	1	\N
00000011	03	00000002	\N	D	\N	2013-04-14	1	\N
00000011	03	00000002	\N	D	\N	2013-04-15	1	\N
00000011	03	00000002	\N	D	\N	2013-04-16	1	\N
00000011	03	00000002	\N	D	\N	2013-04-17	1	\N
00000011	03	00000002	\N	D	\N	2013-04-18	1	\N
00000011	03	00000002	\N	F	\N	2013-04-19	1	\N
00000011	03	00000002	\N	F	\N	2013-04-20	1	\N
00000011	03	00000002	\N	D	\N	2013-04-21	1	\N
00000011	03	00000002	\N	D	\N	2013-04-22	1	\N
00000011	03	00000002	\N	D	\N	2013-04-23	1	\N
00000011	03	00000002	\N	D	\N	2013-04-24	1	\N
00000011	03	00000002	\N	D	\N	2013-04-25	1	\N
00000011	03	00000002	\N	F	\N	2013-04-26	1	\N
00000011	03	00000002	\N	F	\N	2013-04-27	1	\N
00000011	03	00000002	\N	D	\N	2013-04-28	1	\N
00000011	03	00000002	\N	D	\N	2013-04-29	1	\N
00000011	03	00000002	\N	D	\N	2013-04-30	1	\N
00000012	03	00000002	\N	D	\N	2013-04-01	1	\N
00000012	03	00000002	\N	D	\N	2013-04-02	1	\N
00000012	03	00000002	\N	D	\N	2013-04-03	1	\N
00000012	03	00000002	\N	D	\N	2013-04-04	1	\N
00000012	03	00000002	\N	F	\N	2013-04-05	1	\N
00000012	03	00000002	\N	F	\N	2013-04-06	1	\N
00000012	03	00000002	\N	D	\N	2013-04-07	1	\N
00000012	03	00000002	\N	D	\N	2013-04-08	1	\N
00000012	03	00000002	\N	D	\N	2013-04-09	1	\N
00000012	03	00000002	\N	D	\N	2013-04-10	1	\N
00000012	03	00000002	\N	D	\N	2013-04-11	1	\N
00000012	03	00000002	\N	F	\N	2013-04-12	1	\N
00000012	03	00000002	\N	F	\N	2013-04-13	1	\N
00000012	03	00000002	\N	D	\N	2013-04-14	1	\N
00000012	03	00000002	\N	D	\N	2013-04-15	1	\N
00000012	03	00000002	\N	D	\N	2013-04-16	1	\N
00000012	03	00000002	\N	D	\N	2013-04-17	1	\N
00000012	03	00000002	\N	D	\N	2013-04-18	1	\N
00000012	03	00000002	\N	F	\N	2013-04-19	1	\N
00000012	03	00000002	\N	F	\N	2013-04-20	1	\N
00000012	03	00000002	\N	D	\N	2013-04-21	1	\N
00000012	03	00000002	\N	D	\N	2013-04-22	1	\N
00000012	03	00000002	\N	D	\N	2013-04-23	1	\N
00000012	03	00000002	\N	D	\N	2013-04-24	1	\N
00000012	03	00000002	\N	D	\N	2013-04-25	1	\N
00000012	03	00000002	\N	F	\N	2013-04-26	1	\N
00000012	03	00000002	\N	F	\N	2013-04-27	1	\N
00000012	03	00000002	\N	D	\N	2013-04-28	1	\N
00000012	03	00000002	\N	D	\N	2013-04-29	1	\N
00000012	03	00000002	\N	D	\N	2013-04-30	1	\N
00000010	03	00000002	\N	D	\N	2013-05-01	1	\N
00000010	03	00000002	\N	D	\N	2013-05-02	1	\N
00000010	03	00000002	\N	F	\N	2013-05-03	1	\N
00000010	03	00000002	\N	F	\N	2013-05-04	1	\N
00000010	03	00000002	\N	D	\N	2013-05-05	1	\N
00000010	03	00000002	\N	D	\N	2013-05-06	1	\N
00000010	03	00000002	\N	D	\N	2013-05-07	1	\N
00000010	03	00000002	\N	D	\N	2013-05-08	1	\N
00000010	03	00000002	\N	D	\N	2013-05-09	1	\N
00000010	03	00000002	\N	F	\N	2013-05-10	1	\N
00000010	03	00000002	\N	F	\N	2013-05-11	1	\N
00000010	03	00000002	\N	D	\N	2013-05-12	1	\N
00000010	03	00000002	\N	D	\N	2013-05-13	1	\N
00000010	03	00000002	\N	D	\N	2013-05-14	1	\N
00000010	03	00000002	\N	D	\N	2013-05-15	1	\N
00000010	03	00000002	\N	D	\N	2013-05-16	1	\N
00000010	03	00000002	\N	F	\N	2013-05-17	1	\N
00000010	03	00000002	\N	F	\N	2013-05-18	1	\N
00000010	03	00000002	\N	D	\N	2013-05-19	1	\N
00000010	03	00000002	\N	D	\N	2013-05-20	1	\N
00000010	03	00000002	\N	D	\N	2013-05-21	1	\N
00000010	03	00000002	\N	D	\N	2013-05-22	1	\N
00000010	03	00000002	\N	D	\N	2013-05-23	1	\N
00000010	03	00000002	\N	F	\N	2013-05-24	1	\N
00000010	03	00000002	\N	F	\N	2013-05-25	1	\N
00000010	03	00000002	\N	D	\N	2013-05-26	1	\N
00000010	03	00000002	\N	D	\N	2013-05-27	1	\N
00000010	03	00000002	\N	D	\N	2013-05-28	1	\N
00000010	03	00000002	\N	D	\N	2013-05-29	1	\N
00000010	03	00000002	\N	D	\N	2013-05-30	1	\N
00000010	03	00000002	\N	F	\N	2013-05-31	1	\N
00000011	03	00000002	\N	D	\N	2013-05-01	1	\N
00000011	03	00000002	\N	D	\N	2013-05-02	1	\N
00000011	03	00000002	\N	F	\N	2013-05-03	1	\N
00000011	03	00000002	\N	F	\N	2013-05-04	1	\N
00000011	03	00000002	\N	D	\N	2013-05-05	1	\N
00000011	03	00000002	\N	D	\N	2013-05-06	1	\N
00000011	03	00000002	\N	D	\N	2013-05-07	1	\N
00000011	03	00000002	\N	D	\N	2013-05-08	1	\N
00000011	03	00000002	\N	D	\N	2013-05-09	1	\N
00000011	03	00000002	\N	F	\N	2013-05-10	1	\N
00000011	03	00000002	\N	F	\N	2013-05-11	1	\N
00000011	03	00000002	\N	D	\N	2013-05-12	1	\N
00000011	03	00000002	\N	D	\N	2013-05-13	1	\N
00000011	03	00000002	\N	D	\N	2013-05-14	1	\N
00000011	03	00000002	\N	D	\N	2013-05-15	1	\N
00000011	03	00000002	\N	D	\N	2013-05-16	1	\N
00000011	03	00000002	\N	F	\N	2013-05-17	1	\N
00000011	03	00000002	\N	F	\N	2013-05-18	1	\N
00000011	03	00000002	\N	D	\N	2013-05-19	1	\N
00000011	03	00000002	\N	D	\N	2013-05-20	1	\N
00000011	03	00000002	\N	D	\N	2013-05-21	1	\N
00000011	03	00000002	\N	D	\N	2013-05-22	1	\N
00000011	03	00000002	\N	D	\N	2013-05-23	1	\N
00000011	03	00000002	\N	F	\N	2013-05-24	1	\N
00000011	03	00000002	\N	F	\N	2013-05-25	1	\N
00000011	03	00000002	\N	D	\N	2013-05-26	1	\N
00000011	03	00000002	\N	D	\N	2013-05-27	1	\N
00000011	03	00000002	\N	D	\N	2013-05-28	1	\N
00000011	03	00000002	\N	D	\N	2013-05-29	1	\N
00000011	03	00000002	\N	D	\N	2013-05-30	1	\N
00000011	03	00000002	\N	F	\N	2013-05-31	1	\N
00000012	03	00000002	\N	D	\N	2013-05-01	1	\N
00000012	03	00000002	\N	D	\N	2013-05-02	1	\N
00000012	03	00000002	\N	F	\N	2013-05-03	1	\N
00000012	03	00000002	\N	F	\N	2013-05-04	1	\N
00000012	03	00000002	\N	D	\N	2013-05-05	1	\N
00000012	03	00000002	\N	D	\N	2013-05-06	1	\N
00000012	03	00000002	\N	D	\N	2013-05-07	1	\N
00000012	03	00000002	\N	D	\N	2013-05-08	1	\N
00000012	03	00000002	\N	D	\N	2013-05-09	1	\N
00000012	03	00000002	\N	F	\N	2013-05-10	1	\N
00000012	03	00000002	\N	F	\N	2013-05-11	1	\N
00000012	03	00000002	\N	D	\N	2013-05-12	1	\N
00000012	03	00000002	\N	D	\N	2013-05-13	1	\N
00000012	03	00000002	\N	D	\N	2013-05-14	1	\N
00000012	03	00000002	\N	D	\N	2013-05-15	1	\N
00000012	03	00000002	\N	D	\N	2013-05-16	1	\N
00000012	03	00000002	\N	F	\N	2013-05-17	1	\N
00000012	03	00000002	\N	F	\N	2013-05-18	1	\N
00000012	03	00000002	\N	D	\N	2013-05-19	1	\N
00000012	03	00000002	\N	D	\N	2013-05-20	1	\N
00000012	03	00000002	\N	D	\N	2013-05-21	1	\N
00000012	03	00000002	\N	D	\N	2013-05-22	1	\N
00000012	03	00000002	\N	D	\N	2013-05-23	1	\N
00000012	03	00000002	\N	F	\N	2013-05-24	1	\N
00000012	03	00000002	\N	F	\N	2013-05-25	1	\N
00000012	03	00000002	\N	D	\N	2013-05-26	1	\N
00000012	03	00000002	\N	D	\N	2013-05-27	1	\N
00000012	03	00000002	\N	D	\N	2013-05-28	1	\N
00000012	03	00000002	\N	D	\N	2013-05-29	1	\N
00000012	03	00000002	\N	D	\N	2013-05-30	1	\N
00000012	03	00000002	\N	F	\N	2013-05-31	1	\N
00000010	03	00000002	\N	F	\N	2013-06-01	1	\N
00000010	03	00000002	\N	D	\N	2013-06-02	1	\N
00000010	03	00000002	\N	D	\N	2013-06-03	1	\N
00000010	03	00000002	\N	D	\N	2013-06-04	1	\N
00000010	03	00000002	\N	D	\N	2013-06-05	1	\N
00000010	03	00000002	\N	D	\N	2013-06-06	1	\N
00000010	03	00000002	\N	F	\N	2013-06-07	1	\N
00000010	03	00000002	\N	F	\N	2013-06-08	1	\N
00000010	03	00000002	\N	D	\N	2013-06-09	1	\N
00000010	03	00000002	\N	D	\N	2013-06-10	1	\N
00000010	03	00000002	\N	D	\N	2013-06-11	1	\N
00000010	03	00000002	\N	D	\N	2013-06-12	1	\N
00000010	03	00000002	\N	D	\N	2013-06-13	1	\N
00000010	03	00000002	\N	F	\N	2013-06-14	1	\N
00000010	03	00000002	\N	F	\N	2013-06-15	1	\N
00000010	03	00000002	\N	D	\N	2013-06-16	1	\N
00000010	03	00000002	\N	D	\N	2013-06-17	1	\N
00000010	03	00000002	\N	D	\N	2013-06-18	1	\N
00000010	03	00000002	\N	D	\N	2013-06-19	1	\N
00000010	03	00000002	\N	D	\N	2013-06-20	1	\N
00000010	03	00000002	\N	F	\N	2013-06-21	1	\N
00000010	03	00000002	\N	F	\N	2013-06-22	1	\N
00000010	03	00000002	\N	D	\N	2013-06-23	1	\N
00000010	03	00000002	\N	D	\N	2013-06-24	1	\N
00000010	03	00000002	\N	D	\N	2013-06-25	1	\N
00000010	03	00000002	\N	D	\N	2013-06-26	1	\N
00000010	03	00000002	\N	D	\N	2013-06-27	1	\N
00000010	03	00000002	\N	F	\N	2013-06-28	1	\N
00000010	03	00000002	\N	F	\N	2013-06-29	1	\N
00000010	03	00000002	\N	D	\N	2013-06-30	1	\N
00000011	03	00000002	\N	F	\N	2013-06-01	1	\N
00000011	03	00000002	\N	D	\N	2013-06-02	1	\N
00000011	03	00000002	\N	D	\N	2013-06-03	1	\N
00000011	03	00000002	\N	D	\N	2013-06-04	1	\N
00000011	03	00000002	\N	D	\N	2013-06-05	1	\N
00000011	03	00000002	\N	D	\N	2013-06-06	1	\N
00000011	03	00000002	\N	F	\N	2013-06-07	1	\N
00000011	03	00000002	\N	F	\N	2013-06-08	1	\N
00000011	03	00000002	\N	D	\N	2013-06-09	1	\N
00000011	03	00000002	\N	D	\N	2013-06-10	1	\N
00000011	03	00000002	\N	D	\N	2013-06-11	1	\N
00000011	03	00000002	\N	D	\N	2013-06-12	1	\N
00000011	03	00000002	\N	D	\N	2013-06-13	1	\N
00000011	03	00000002	\N	F	\N	2013-06-14	1	\N
00000011	03	00000002	\N	F	\N	2013-06-15	1	\N
00000011	03	00000002	\N	D	\N	2013-06-16	1	\N
00000011	03	00000002	\N	D	\N	2013-06-17	1	\N
00000011	03	00000002	\N	D	\N	2013-06-18	1	\N
00000011	03	00000002	\N	D	\N	2013-06-19	1	\N
00000011	03	00000002	\N	D	\N	2013-06-20	1	\N
00000011	03	00000002	\N	F	\N	2013-06-21	1	\N
00000011	03	00000002	\N	F	\N	2013-06-22	1	\N
00000011	03	00000002	\N	D	\N	2013-06-23	1	\N
00000011	03	00000002	\N	D	\N	2013-06-24	1	\N
00000011	03	00000002	\N	D	\N	2013-06-25	1	\N
00000011	03	00000002	\N	D	\N	2013-06-26	1	\N
00000011	03	00000002	\N	D	\N	2013-06-27	1	\N
00000011	03	00000002	\N	F	\N	2013-06-28	1	\N
00000011	03	00000002	\N	F	\N	2013-06-29	1	\N
00000011	03	00000002	\N	D	\N	2013-06-30	1	\N
00000012	03	00000002	\N	F	\N	2013-06-01	1	\N
00000012	03	00000002	\N	D	\N	2013-06-02	1	\N
00000012	03	00000002	\N	D	\N	2013-06-03	1	\N
00000012	03	00000002	\N	D	\N	2013-06-04	1	\N
00000012	03	00000002	\N	D	\N	2013-06-05	1	\N
00000012	03	00000002	\N	D	\N	2013-06-06	1	\N
00000012	03	00000002	\N	F	\N	2013-06-07	1	\N
00000012	03	00000002	\N	F	\N	2013-06-08	1	\N
00000012	03	00000002	\N	D	\N	2013-06-09	1	\N
00000012	03	00000002	\N	D	\N	2013-06-10	1	\N
00000012	03	00000002	\N	D	\N	2013-06-11	1	\N
00000012	03	00000002	\N	D	\N	2013-06-12	1	\N
00000012	03	00000002	\N	D	\N	2013-06-13	1	\N
00000012	03	00000002	\N	F	\N	2013-06-14	1	\N
00000012	03	00000002	\N	F	\N	2013-06-15	1	\N
00000012	03	00000002	\N	D	\N	2013-06-16	1	\N
00000012	03	00000002	\N	D	\N	2013-06-17	1	\N
00000012	03	00000002	\N	D	\N	2013-06-18	1	\N
00000012	03	00000002	\N	D	\N	2013-06-19	1	\N
00000012	03	00000002	\N	D	\N	2013-06-20	1	\N
00000012	03	00000002	\N	F	\N	2013-06-21	1	\N
00000012	03	00000002	\N	F	\N	2013-06-22	1	\N
00000012	03	00000002	\N	D	\N	2013-06-23	1	\N
00000012	03	00000002	\N	D	\N	2013-06-24	1	\N
00000012	03	00000002	\N	D	\N	2013-06-25	1	\N
00000012	03	00000002	\N	D	\N	2013-06-26	1	\N
00000012	03	00000002	\N	D	\N	2013-06-27	1	\N
00000012	03	00000002	\N	F	\N	2013-06-28	1	\N
00000012	03	00000002	\N	F	\N	2013-06-29	1	\N
00000012	03	00000002	\N	D	\N	2013-06-30	1	\N
00000010	03	00000002	\N	D	\N	2013-07-01	1	\N
00000010	03	00000002	\N	D	\N	2013-07-02	1	\N
00000010	03	00000002	\N	D	\N	2013-07-03	1	\N
00000010	03	00000002	\N	D	\N	2013-07-04	1	\N
00000010	03	00000002	\N	F	\N	2013-07-05	1	\N
00000010	03	00000002	\N	F	\N	2013-07-06	1	\N
00000010	03	00000002	\N	D	\N	2013-07-07	1	\N
00000010	03	00000002	\N	D	\N	2013-07-08	1	\N
00000010	03	00000002	\N	D	\N	2013-07-09	1	\N
00000010	03	00000002	\N	D	\N	2013-07-10	1	\N
00000010	03	00000002	\N	D	\N	2013-07-11	1	\N
00000010	03	00000002	\N	F	\N	2013-07-12	1	\N
00000010	03	00000002	\N	F	\N	2013-07-13	1	\N
00000010	03	00000002	\N	D	\N	2013-07-14	1	\N
00000010	03	00000002	\N	D	\N	2013-07-15	1	\N
00000010	03	00000002	\N	D	\N	2013-07-16	1	\N
00000010	03	00000002	\N	D	\N	2013-07-17	1	\N
00000010	03	00000002	\N	D	\N	2013-07-18	1	\N
00000010	03	00000002	\N	F	\N	2013-07-19	1	\N
00000010	03	00000002	\N	F	\N	2013-07-20	1	\N
00000010	03	00000002	\N	D	\N	2013-07-21	1	\N
00000010	03	00000002	\N	D	\N	2013-07-22	1	\N
00000010	03	00000002	\N	D	\N	2013-07-23	1	\N
00000010	03	00000002	\N	D	\N	2013-07-24	1	\N
00000010	03	00000002	\N	D	\N	2013-07-25	1	\N
00000010	03	00000002	\N	F	\N	2013-07-26	1	\N
00000010	03	00000002	\N	F	\N	2013-07-27	1	\N
00000010	03	00000002	\N	D	\N	2013-07-28	1	\N
00000010	03	00000002	\N	D	\N	2013-07-29	1	\N
00000010	03	00000002	\N	D	\N	2013-07-30	1	\N
00000010	03	00000002	\N	D	\N	2013-07-31	1	\N
00000011	03	00000002	\N	D	\N	2013-07-01	1	\N
00000011	03	00000002	\N	D	\N	2013-07-02	1	\N
00000011	03	00000002	\N	D	\N	2013-07-03	1	\N
00000011	03	00000002	\N	D	\N	2013-07-04	1	\N
00000011	03	00000002	\N	F	\N	2013-07-05	1	\N
00000011	03	00000002	\N	F	\N	2013-07-06	1	\N
00000011	03	00000002	\N	D	\N	2013-07-07	1	\N
00000011	03	00000002	\N	D	\N	2013-07-08	1	\N
00000011	03	00000002	\N	D	\N	2013-07-09	1	\N
00000011	03	00000002	\N	D	\N	2013-07-10	1	\N
00000011	03	00000002	\N	D	\N	2013-07-11	1	\N
00000011	03	00000002	\N	F	\N	2013-07-12	1	\N
00000011	03	00000002	\N	F	\N	2013-07-13	1	\N
00000011	03	00000002	\N	D	\N	2013-07-14	1	\N
00000011	03	00000002	\N	D	\N	2013-07-15	1	\N
00000011	03	00000002	\N	D	\N	2013-07-16	1	\N
00000011	03	00000002	\N	D	\N	2013-07-17	1	\N
00000011	03	00000002	\N	D	\N	2013-07-18	1	\N
00000011	03	00000002	\N	F	\N	2013-07-19	1	\N
00000011	03	00000002	\N	F	\N	2013-07-20	1	\N
00000011	03	00000002	\N	D	\N	2013-07-21	1	\N
00000011	03	00000002	\N	D	\N	2013-07-22	1	\N
00000011	03	00000002	\N	D	\N	2013-07-23	1	\N
00000011	03	00000002	\N	D	\N	2013-07-24	1	\N
00000011	03	00000002	\N	D	\N	2013-07-25	1	\N
00000011	03	00000002	\N	F	\N	2013-07-26	1	\N
00000011	03	00000002	\N	F	\N	2013-07-27	1	\N
00000011	03	00000002	\N	D	\N	2013-07-28	1	\N
00000011	03	00000002	\N	D	\N	2013-07-29	1	\N
00000011	03	00000002	\N	D	\N	2013-07-30	1	\N
00000011	03	00000002	\N	D	\N	2013-07-31	1	\N
00000012	03	00000002	\N	D	\N	2013-07-01	1	\N
00000012	03	00000002	\N	D	\N	2013-07-02	1	\N
00000012	03	00000002	\N	D	\N	2013-07-03	1	\N
00000012	03	00000002	\N	D	\N	2013-07-04	1	\N
00000012	03	00000002	\N	F	\N	2013-07-05	1	\N
00000012	03	00000002	\N	F	\N	2013-07-06	1	\N
00000012	03	00000002	\N	D	\N	2013-07-07	1	\N
00000012	03	00000002	\N	D	\N	2013-07-08	1	\N
00000012	03	00000002	\N	D	\N	2013-07-09	1	\N
00000012	03	00000002	\N	D	\N	2013-07-10	1	\N
00000012	03	00000002	\N	D	\N	2013-07-11	1	\N
00000012	03	00000002	\N	F	\N	2013-07-12	1	\N
00000012	03	00000002	\N	F	\N	2013-07-13	1	\N
00000012	03	00000002	\N	D	\N	2013-07-14	1	\N
00000012	03	00000002	\N	D	\N	2013-07-15	1	\N
00000012	03	00000002	\N	D	\N	2013-07-16	1	\N
00000012	03	00000002	\N	D	\N	2013-07-17	1	\N
00000012	03	00000002	\N	D	\N	2013-07-18	1	\N
00000012	03	00000002	\N	F	\N	2013-07-19	1	\N
00000012	03	00000002	\N	F	\N	2013-07-20	1	\N
00000012	03	00000002	\N	D	\N	2013-07-21	1	\N
00000012	03	00000002	\N	D	\N	2013-07-22	1	\N
00000012	03	00000002	\N	D	\N	2013-07-23	1	\N
00000012	03	00000002	\N	D	\N	2013-07-24	1	\N
00000012	03	00000002	\N	D	\N	2013-07-25	1	\N
00000012	03	00000002	\N	F	\N	2013-07-26	1	\N
00000012	03	00000002	\N	F	\N	2013-07-27	1	\N
00000012	03	00000002	\N	D	\N	2013-07-28	1	\N
00000012	03	00000002	\N	D	\N	2013-07-29	1	\N
00000012	03	00000002	\N	D	\N	2013-07-30	1	\N
00000012	03	00000002	\N	D	\N	2013-07-31	1	\N
00000010	03	00000002	\N	D	\N	2013-08-01	1	\N
00000010	03	00000002	\N	F	\N	2013-08-02	1	\N
00000010	03	00000002	\N	F	\N	2013-08-03	1	\N
00000010	03	00000002	\N	D	\N	2013-08-04	1	\N
00000010	03	00000002	\N	D	\N	2013-08-05	1	\N
00000010	03	00000002	\N	D	\N	2013-08-06	1	\N
00000010	03	00000002	\N	D	\N	2013-08-07	1	\N
00000010	03	00000002	\N	D	\N	2013-08-08	1	\N
00000010	03	00000002	\N	F	\N	2013-08-09	1	\N
00000010	03	00000002	\N	F	\N	2013-08-10	1	\N
00000010	03	00000002	\N	D	\N	2013-08-11	1	\N
00000010	03	00000002	\N	D	\N	2013-08-12	1	\N
00000010	03	00000002	\N	D	\N	2013-08-13	1	\N
00000010	03	00000002	\N	D	\N	2013-08-14	1	\N
00000010	03	00000002	\N	D	\N	2013-08-15	1	\N
00000010	03	00000002	\N	F	\N	2013-08-16	1	\N
00000010	03	00000002	\N	F	\N	2013-08-17	1	\N
00000010	03	00000002	\N	D	\N	2013-08-18	1	\N
00000010	03	00000002	\N	D	\N	2013-08-19	1	\N
00000010	03	00000002	\N	D	\N	2013-08-20	1	\N
00000010	03	00000002	\N	D	\N	2013-08-21	1	\N
00000010	03	00000002	\N	D	\N	2013-08-22	1	\N
00000010	03	00000002	\N	F	\N	2013-08-23	1	\N
00000010	03	00000002	\N	F	\N	2013-08-24	1	\N
00000010	03	00000002	\N	D	\N	2013-08-25	1	\N
00000010	03	00000002	\N	D	\N	2013-08-26	1	\N
00000010	03	00000002	\N	D	\N	2013-08-27	1	\N
00000010	03	00000002	\N	D	\N	2013-08-28	1	\N
00000010	03	00000002	\N	D	\N	2013-08-29	1	\N
00000010	03	00000002	\N	F	\N	2013-08-30	1	\N
00000010	03	00000002	\N	F	\N	2013-08-31	1	\N
00000011	03	00000002	\N	D	\N	2013-08-01	1	\N
00000011	03	00000002	\N	F	\N	2013-08-02	1	\N
00000011	03	00000002	\N	F	\N	2013-08-03	1	\N
00000011	03	00000002	\N	D	\N	2013-08-04	1	\N
00000011	03	00000002	\N	D	\N	2013-08-05	1	\N
00000011	03	00000002	\N	D	\N	2013-08-06	1	\N
00000011	03	00000002	\N	D	\N	2013-08-07	1	\N
00000011	03	00000002	\N	D	\N	2013-08-08	1	\N
00000011	03	00000002	\N	F	\N	2013-08-09	1	\N
00000011	03	00000002	\N	F	\N	2013-08-10	1	\N
00000011	03	00000002	\N	D	\N	2013-08-11	1	\N
00000011	03	00000002	\N	D	\N	2013-08-12	1	\N
00000011	03	00000002	\N	D	\N	2013-08-13	1	\N
00000011	03	00000002	\N	D	\N	2013-08-14	1	\N
00000011	03	00000002	\N	D	\N	2013-08-15	1	\N
00000011	03	00000002	\N	F	\N	2013-08-16	1	\N
00000011	03	00000002	\N	F	\N	2013-08-17	1	\N
00000011	03	00000002	\N	D	\N	2013-08-18	1	\N
00000011	03	00000002	\N	D	\N	2013-08-19	1	\N
00000011	03	00000002	\N	D	\N	2013-08-20	1	\N
00000011	03	00000002	\N	D	\N	2013-08-21	1	\N
00000011	03	00000002	\N	D	\N	2013-08-22	1	\N
00000011	03	00000002	\N	F	\N	2013-08-23	1	\N
00000011	03	00000002	\N	F	\N	2013-08-24	1	\N
00000011	03	00000002	\N	D	\N	2013-08-25	1	\N
00000011	03	00000002	\N	D	\N	2013-08-26	1	\N
00000011	03	00000002	\N	D	\N	2013-08-27	1	\N
00000011	03	00000002	\N	D	\N	2013-08-28	1	\N
00000011	03	00000002	\N	D	\N	2013-08-29	1	\N
00000011	03	00000002	\N	F	\N	2013-08-30	1	\N
00000011	03	00000002	\N	F	\N	2013-08-31	1	\N
00000012	03	00000002	\N	D	\N	2013-08-01	1	\N
00000012	03	00000002	\N	F	\N	2013-08-02	1	\N
00000012	03	00000002	\N	F	\N	2013-08-03	1	\N
00000012	03	00000002	\N	D	\N	2013-08-04	1	\N
00000012	03	00000002	\N	D	\N	2013-08-05	1	\N
00000012	03	00000002	\N	D	\N	2013-08-06	1	\N
00000012	03	00000002	\N	D	\N	2013-08-07	1	\N
00000012	03	00000002	\N	D	\N	2013-08-08	1	\N
00000012	03	00000002	\N	F	\N	2013-08-09	1	\N
00000012	03	00000002	\N	F	\N	2013-08-10	1	\N
00000012	03	00000002	\N	D	\N	2013-08-11	1	\N
00000012	03	00000002	\N	D	\N	2013-08-12	1	\N
00000012	03	00000002	\N	D	\N	2013-08-13	1	\N
00000012	03	00000002	\N	D	\N	2013-08-14	1	\N
00000012	03	00000002	\N	D	\N	2013-08-15	1	\N
00000012	03	00000002	\N	F	\N	2013-08-16	1	\N
00000012	03	00000002	\N	F	\N	2013-08-17	1	\N
00000012	03	00000002	\N	D	\N	2013-08-18	1	\N
00000012	03	00000002	\N	D	\N	2013-08-19	1	\N
00000012	03	00000002	\N	D	\N	2013-08-20	1	\N
00000012	03	00000002	\N	D	\N	2013-08-21	1	\N
00000012	03	00000002	\N	D	\N	2013-08-22	1	\N
00000012	03	00000002	\N	F	\N	2013-08-23	1	\N
00000012	03	00000002	\N	F	\N	2013-08-24	1	\N
00000012	03	00000002	\N	D	\N	2013-08-25	1	\N
00000012	03	00000002	\N	D	\N	2013-08-26	1	\N
00000012	03	00000002	\N	D	\N	2013-08-27	1	\N
00000012	03	00000002	\N	D	\N	2013-08-28	1	\N
00000012	03	00000002	\N	D	\N	2013-08-29	1	\N
00000012	03	00000002	\N	F	\N	2013-08-30	1	\N
00000012	03	00000002	\N	F	\N	2013-08-31	1	\N
00000010	03	00000002	\N	D	\N	2013-09-01	1	\N
00000010	03	00000002	\N	D	\N	2013-09-02	1	\N
00000010	03	00000002	\N	D	\N	2013-09-03	1	\N
00000010	03	00000002	\N	D	\N	2013-09-04	1	\N
00000010	03	00000002	\N	D	\N	2013-09-05	1	\N
00000010	03	00000002	\N	F	\N	2013-09-06	1	\N
00000010	03	00000002	\N	F	\N	2013-09-07	1	\N
00000010	03	00000002	\N	D	\N	2013-09-08	1	\N
00000010	03	00000002	\N	D	\N	2013-09-09	1	\N
00000010	03	00000002	\N	D	\N	2013-09-10	1	\N
00000010	03	00000002	\N	D	\N	2013-09-11	1	\N
00000010	03	00000002	\N	D	\N	2013-09-12	1	\N
00000010	03	00000002	\N	F	\N	2013-09-13	1	\N
00000010	03	00000002	\N	F	\N	2013-09-14	1	\N
00000010	03	00000002	\N	D	\N	2013-09-15	1	\N
00000010	03	00000002	\N	D	\N	2013-09-16	1	\N
00000010	03	00000002	\N	D	\N	2013-09-17	1	\N
00000010	03	00000002	\N	D	\N	2013-09-18	1	\N
00000010	03	00000002	\N	D	\N	2013-09-19	1	\N
00000010	03	00000002	\N	F	\N	2013-09-20	1	\N
00000010	03	00000002	\N	F	\N	2013-09-21	1	\N
00000010	03	00000002	\N	D	\N	2013-09-22	1	\N
00000010	03	00000002	\N	D	\N	2013-09-23	1	\N
00000010	03	00000002	\N	D	\N	2013-09-24	1	\N
00000010	03	00000002	\N	D	\N	2013-09-25	1	\N
00000010	03	00000002	\N	D	\N	2013-09-26	1	\N
00000010	03	00000002	\N	F	\N	2013-09-27	1	\N
00000010	03	00000002	\N	F	\N	2013-09-28	1	\N
00000010	03	00000002	\N	D	\N	2013-09-29	1	\N
00000010	03	00000002	\N	D	\N	2013-09-30	1	\N
00000011	03	00000002	\N	D	\N	2013-09-01	1	\N
00000011	03	00000002	\N	D	\N	2013-09-02	1	\N
00000011	03	00000002	\N	D	\N	2013-09-03	1	\N
00000011	03	00000002	\N	D	\N	2013-09-04	1	\N
00000011	03	00000002	\N	D	\N	2013-09-05	1	\N
00000011	03	00000002	\N	F	\N	2013-09-06	1	\N
00000011	03	00000002	\N	F	\N	2013-09-07	1	\N
00000011	03	00000002	\N	D	\N	2013-09-08	1	\N
00000011	03	00000002	\N	D	\N	2013-09-09	1	\N
00000011	03	00000002	\N	D	\N	2013-09-10	1	\N
00000011	03	00000002	\N	D	\N	2013-09-11	1	\N
00000011	03	00000002	\N	D	\N	2013-09-12	1	\N
00000011	03	00000002	\N	F	\N	2013-09-13	1	\N
00000011	03	00000002	\N	F	\N	2013-09-14	1	\N
00000011	03	00000002	\N	D	\N	2013-09-15	1	\N
00000011	03	00000002	\N	D	\N	2013-09-16	1	\N
00000011	03	00000002	\N	D	\N	2013-09-17	1	\N
00000011	03	00000002	\N	D	\N	2013-09-18	1	\N
00000011	03	00000002	\N	D	\N	2013-09-19	1	\N
00000011	03	00000002	\N	F	\N	2013-09-20	1	\N
00000011	03	00000002	\N	F	\N	2013-09-21	1	\N
00000011	03	00000002	\N	D	\N	2013-09-22	1	\N
00000011	03	00000002	\N	D	\N	2013-09-23	1	\N
00000011	03	00000002	\N	D	\N	2013-09-24	1	\N
00000011	03	00000002	\N	D	\N	2013-09-25	1	\N
00000011	03	00000002	\N	D	\N	2013-09-26	1	\N
00000011	03	00000002	\N	F	\N	2013-09-27	1	\N
00000011	03	00000002	\N	F	\N	2013-09-28	1	\N
00000011	03	00000002	\N	D	\N	2013-09-29	1	\N
00000011	03	00000002	\N	D	\N	2013-09-30	1	\N
00000012	03	00000002	\N	D	\N	2013-09-01	1	\N
00000012	03	00000002	\N	D	\N	2013-09-02	1	\N
00000012	03	00000002	\N	D	\N	2013-09-03	1	\N
00000012	03	00000002	\N	D	\N	2013-09-04	1	\N
00000012	03	00000002	\N	D	\N	2013-09-05	1	\N
00000012	03	00000002	\N	F	\N	2013-09-06	1	\N
00000012	03	00000002	\N	F	\N	2013-09-07	1	\N
00000012	03	00000002	\N	D	\N	2013-09-08	1	\N
00000012	03	00000002	\N	D	\N	2013-09-09	1	\N
00000012	03	00000002	\N	D	\N	2013-09-10	1	\N
00000012	03	00000002	\N	D	\N	2013-09-11	1	\N
00000012	03	00000002	\N	D	\N	2013-09-12	1	\N
00000012	03	00000002	\N	F	\N	2013-09-13	1	\N
00000012	03	00000002	\N	F	\N	2013-09-14	1	\N
00000012	03	00000002	\N	D	\N	2013-09-15	1	\N
00000012	03	00000002	\N	D	\N	2013-09-16	1	\N
00000012	03	00000002	\N	D	\N	2013-09-17	1	\N
00000012	03	00000002	\N	D	\N	2013-09-18	1	\N
00000012	03	00000002	\N	D	\N	2013-09-19	1	\N
00000012	03	00000002	\N	F	\N	2013-09-20	1	\N
00000012	03	00000002	\N	F	\N	2013-09-21	1	\N
00000012	03	00000002	\N	D	\N	2013-09-22	1	\N
00000012	03	00000002	\N	D	\N	2013-09-23	1	\N
00000012	03	00000002	\N	D	\N	2013-09-24	1	\N
00000012	03	00000002	\N	D	\N	2013-09-25	1	\N
00000012	03	00000002	\N	D	\N	2013-09-26	1	\N
00000012	03	00000002	\N	F	\N	2013-09-27	1	\N
00000012	03	00000002	\N	F	\N	2013-09-28	1	\N
00000012	03	00000002	\N	D	\N	2013-09-29	1	\N
00000012	03	00000002	\N	D	\N	2013-09-30	1	\N
00000010	03	00000002	\N	D	\N	2013-10-01	1	\N
00000010	03	00000002	\N	D	\N	2013-10-02	1	\N
00000010	03	00000002	\N	D	\N	2013-10-03	1	\N
00000010	03	00000002	\N	F	\N	2013-10-04	1	\N
00000010	03	00000002	\N	F	\N	2013-10-05	1	\N
00000010	03	00000002	\N	D	\N	2013-10-06	1	\N
00000010	03	00000002	\N	D	\N	2013-10-07	1	\N
00000010	03	00000002	\N	D	\N	2013-10-08	1	\N
00000010	03	00000002	\N	D	\N	2013-10-09	1	\N
00000010	03	00000002	\N	D	\N	2013-10-10	1	\N
00000010	03	00000002	\N	F	\N	2013-10-11	1	\N
00000010	03	00000002	\N	F	\N	2013-10-12	1	\N
00000010	03	00000002	\N	D	\N	2013-10-13	1	\N
00000010	03	00000002	\N	D	\N	2013-10-14	1	\N
00000010	03	00000002	\N	D	\N	2013-10-15	1	\N
00000010	03	00000002	\N	D	\N	2013-10-16	1	\N
00000010	03	00000002	\N	D	\N	2013-10-17	1	\N
00000010	03	00000002	\N	F	\N	2013-10-18	1	\N
00000010	03	00000002	\N	F	\N	2013-10-19	1	\N
00000010	03	00000002	\N	D	\N	2013-10-20	1	\N
00000010	03	00000002	\N	D	\N	2013-10-21	1	\N
00000010	03	00000002	\N	D	\N	2013-10-22	1	\N
00000010	03	00000002	\N	D	\N	2013-10-23	1	\N
00000010	03	00000002	\N	D	\N	2013-10-24	1	\N
00000010	03	00000002	\N	F	\N	2013-10-25	1	\N
00000010	03	00000002	\N	F	\N	2013-10-26	1	\N
00000010	03	00000002	\N	D	\N	2013-10-27	1	\N
00000010	03	00000002	\N	D	\N	2013-10-28	1	\N
00000010	03	00000002	\N	D	\N	2013-10-29	1	\N
00000010	03	00000002	\N	D	\N	2013-10-30	1	\N
00000010	03	00000002	\N	D	\N	2013-10-31	1	\N
00000011	03	00000002	\N	D	\N	2013-10-01	1	\N
00000011	03	00000002	\N	D	\N	2013-10-02	1	\N
00000011	03	00000002	\N	D	\N	2013-10-03	1	\N
00000011	03	00000002	\N	F	\N	2013-10-04	1	\N
00000011	03	00000002	\N	F	\N	2013-10-05	1	\N
00000011	03	00000002	\N	D	\N	2013-10-06	1	\N
00000011	03	00000002	\N	D	\N	2013-10-07	1	\N
00000011	03	00000002	\N	D	\N	2013-10-08	1	\N
00000011	03	00000002	\N	D	\N	2013-10-09	1	\N
00000011	03	00000002	\N	D	\N	2013-10-10	1	\N
00000011	03	00000002	\N	F	\N	2013-10-11	1	\N
00000011	03	00000002	\N	F	\N	2013-10-12	1	\N
00000011	03	00000002	\N	D	\N	2013-10-13	1	\N
00000011	03	00000002	\N	D	\N	2013-10-14	1	\N
00000011	03	00000002	\N	D	\N	2013-10-15	1	\N
00000011	03	00000002	\N	D	\N	2013-10-16	1	\N
00000011	03	00000002	\N	D	\N	2013-10-17	1	\N
00000011	03	00000002	\N	F	\N	2013-10-18	1	\N
00000011	03	00000002	\N	F	\N	2013-10-19	1	\N
00000011	03	00000002	\N	D	\N	2013-10-20	1	\N
00000011	03	00000002	\N	D	\N	2013-10-21	1	\N
00000011	03	00000002	\N	D	\N	2013-10-22	1	\N
00000011	03	00000002	\N	D	\N	2013-10-23	1	\N
00000011	03	00000002	\N	D	\N	2013-10-24	1	\N
00000011	03	00000002	\N	F	\N	2013-10-25	1	\N
00000011	03	00000002	\N	F	\N	2013-10-26	1	\N
00000011	03	00000002	\N	D	\N	2013-10-27	1	\N
00000011	03	00000002	\N	D	\N	2013-10-28	1	\N
00000011	03	00000002	\N	D	\N	2013-10-29	1	\N
00000011	03	00000002	\N	D	\N	2013-10-30	1	\N
00000011	03	00000002	\N	D	\N	2013-10-31	1	\N
00000012	03	00000002	\N	D	\N	2013-10-01	1	\N
00000012	03	00000002	\N	D	\N	2013-10-02	1	\N
00000012	03	00000002	\N	D	\N	2013-10-03	1	\N
00000012	03	00000002	\N	F	\N	2013-10-04	1	\N
00000012	03	00000002	\N	F	\N	2013-10-05	1	\N
00000012	03	00000002	\N	D	\N	2013-10-06	1	\N
00000012	03	00000002	\N	D	\N	2013-10-07	1	\N
00000012	03	00000002	\N	D	\N	2013-10-08	1	\N
00000012	03	00000002	\N	D	\N	2013-10-09	1	\N
00000012	03	00000002	\N	D	\N	2013-10-10	1	\N
00000012	03	00000002	\N	F	\N	2013-10-11	1	\N
00000012	03	00000002	\N	F	\N	2013-10-12	1	\N
00000012	03	00000002	\N	D	\N	2013-10-13	1	\N
00000012	03	00000002	\N	D	\N	2013-10-14	1	\N
00000012	03	00000002	\N	D	\N	2013-10-15	1	\N
00000012	03	00000002	\N	D	\N	2013-10-16	1	\N
00000012	03	00000002	\N	D	\N	2013-10-17	1	\N
00000012	03	00000002	\N	F	\N	2013-10-18	1	\N
00000012	03	00000002	\N	F	\N	2013-10-19	1	\N
00000012	03	00000002	\N	D	\N	2013-10-20	1	\N
00000012	03	00000002	\N	D	\N	2013-10-21	1	\N
00000012	03	00000002	\N	D	\N	2013-10-22	1	\N
00000012	03	00000002	\N	D	\N	2013-10-23	1	\N
00000012	03	00000002	\N	D	\N	2013-10-24	1	\N
00000012	03	00000002	\N	F	\N	2013-10-25	1	\N
00000012	03	00000002	\N	F	\N	2013-10-26	1	\N
00000012	03	00000002	\N	D	\N	2013-10-27	1	\N
00000012	03	00000002	\N	D	\N	2013-10-28	1	\N
00000012	03	00000002	\N	D	\N	2013-10-29	1	\N
00000012	03	00000002	\N	D	\N	2013-10-30	1	\N
00000012	03	00000002	\N	D	\N	2013-10-31	1	\N
00000010	03	00000002	\N	F	\N	2013-11-01	1	\N
00000010	03	00000002	\N	F	\N	2013-11-02	1	\N
00000010	03	00000002	\N	D	\N	2013-11-03	1	\N
00000010	03	00000002	\N	D	\N	2013-11-04	1	\N
00000010	03	00000002	\N	D	\N	2013-11-05	1	\N
00000010	03	00000002	\N	D	\N	2013-11-06	1	\N
00000010	03	00000002	\N	D	\N	2013-11-07	1	\N
00000010	03	00000002	\N	F	\N	2013-11-08	1	\N
00000010	03	00000002	\N	F	\N	2013-11-09	1	\N
00000010	03	00000002	\N	D	\N	2013-11-10	1	\N
00000010	03	00000002	\N	D	\N	2013-11-11	1	\N
00000010	03	00000002	\N	D	\N	2013-11-12	1	\N
00000010	03	00000002	\N	D	\N	2013-11-13	1	\N
00000010	03	00000002	\N	D	\N	2013-11-14	1	\N
00000010	03	00000002	\N	F	\N	2013-11-15	1	\N
00000010	03	00000002	\N	F	\N	2013-11-16	1	\N
00000010	03	00000002	\N	D	\N	2013-11-17	1	\N
00000010	03	00000002	\N	D	\N	2013-11-18	1	\N
00000010	03	00000002	\N	D	\N	2013-11-19	1	\N
00000010	03	00000002	\N	D	\N	2013-11-20	1	\N
00000010	03	00000002	\N	D	\N	2013-11-21	1	\N
00000010	03	00000002	\N	F	\N	2013-11-22	1	\N
00000010	03	00000002	\N	F	\N	2013-11-23	1	\N
00000010	03	00000002	\N	D	\N	2013-11-24	1	\N
00000010	03	00000002	\N	D	\N	2013-11-25	1	\N
00000010	03	00000002	\N	D	\N	2013-11-26	1	\N
00000010	03	00000002	\N	D	\N	2013-11-27	1	\N
00000010	03	00000002	\N	D	\N	2013-11-28	1	\N
00000010	03	00000002	\N	F	\N	2013-11-29	1	\N
00000010	03	00000002	\N	F	\N	2013-11-30	1	\N
00000011	03	00000002	\N	F	\N	2013-11-01	1	\N
00000011	03	00000002	\N	F	\N	2013-11-02	1	\N
00000011	03	00000002	\N	D	\N	2013-11-03	1	\N
00000011	03	00000002	\N	D	\N	2013-11-04	1	\N
00000011	03	00000002	\N	D	\N	2013-11-05	1	\N
00000011	03	00000002	\N	D	\N	2013-11-06	1	\N
00000011	03	00000002	\N	D	\N	2013-11-07	1	\N
00000011	03	00000002	\N	F	\N	2013-11-08	1	\N
00000011	03	00000002	\N	F	\N	2013-11-09	1	\N
00000011	03	00000002	\N	D	\N	2013-11-10	1	\N
00000011	03	00000002	\N	D	\N	2013-11-11	1	\N
00000011	03	00000002	\N	D	\N	2013-11-12	1	\N
00000011	03	00000002	\N	D	\N	2013-11-13	1	\N
00000011	03	00000002	\N	D	\N	2013-11-14	1	\N
00000011	03	00000002	\N	F	\N	2013-11-15	1	\N
00000011	03	00000002	\N	F	\N	2013-11-16	1	\N
00000011	03	00000002	\N	D	\N	2013-11-17	1	\N
00000011	03	00000002	\N	D	\N	2013-11-18	1	\N
00000011	03	00000002	\N	D	\N	2013-11-19	1	\N
00000011	03	00000002	\N	D	\N	2013-11-20	1	\N
00000011	03	00000002	\N	D	\N	2013-11-21	1	\N
00000011	03	00000002	\N	F	\N	2013-11-22	1	\N
00000011	03	00000002	\N	F	\N	2013-11-23	1	\N
00000011	03	00000002	\N	D	\N	2013-11-24	1	\N
00000011	03	00000002	\N	D	\N	2013-11-25	1	\N
00000011	03	00000002	\N	D	\N	2013-11-26	1	\N
00000011	03	00000002	\N	D	\N	2013-11-27	1	\N
00000011	03	00000002	\N	D	\N	2013-11-28	1	\N
00000011	03	00000002	\N	F	\N	2013-11-29	1	\N
00000011	03	00000002	\N	F	\N	2013-11-30	1	\N
00000012	03	00000002	\N	F	\N	2013-11-01	1	\N
00000012	03	00000002	\N	F	\N	2013-11-02	1	\N
00000012	03	00000002	\N	D	\N	2013-11-03	1	\N
00000012	03	00000002	\N	D	\N	2013-11-04	1	\N
00000012	03	00000002	\N	D	\N	2013-11-05	1	\N
00000012	03	00000002	\N	D	\N	2013-11-06	1	\N
00000012	03	00000002	\N	D	\N	2013-11-07	1	\N
00000012	03	00000002	\N	F	\N	2013-11-08	1	\N
00000012	03	00000002	\N	F	\N	2013-11-09	1	\N
00000012	03	00000002	\N	D	\N	2013-11-10	1	\N
00000012	03	00000002	\N	D	\N	2013-11-11	1	\N
00000012	03	00000002	\N	D	\N	2013-11-12	1	\N
00000012	03	00000002	\N	D	\N	2013-11-13	1	\N
00000012	03	00000002	\N	D	\N	2013-11-14	1	\N
00000012	03	00000002	\N	F	\N	2013-11-15	1	\N
00000012	03	00000002	\N	F	\N	2013-11-16	1	\N
00000012	03	00000002	\N	D	\N	2013-11-17	1	\N
00000012	03	00000002	\N	D	\N	2013-11-18	1	\N
00000012	03	00000002	\N	D	\N	2013-11-19	1	\N
00000012	03	00000002	\N	D	\N	2013-11-20	1	\N
00000012	03	00000002	\N	D	\N	2013-11-21	1	\N
00000012	03	00000002	\N	F	\N	2013-11-22	1	\N
00000012	03	00000002	\N	F	\N	2013-11-23	1	\N
00000012	03	00000002	\N	D	\N	2013-11-24	1	\N
00000012	03	00000002	\N	D	\N	2013-11-25	1	\N
00000012	03	00000002	\N	D	\N	2013-11-26	1	\N
00000012	03	00000002	\N	D	\N	2013-11-27	1	\N
00000012	03	00000002	\N	D	\N	2013-11-28	1	\N
00000012	03	00000002	\N	F	\N	2013-11-29	1	\N
00000012	03	00000002	\N	F	\N	2013-11-30	1	\N
00000010	03	00000002	\N	D	\N	2013-12-01	1	\N
00000010	03	00000002	\N	D	\N	2013-12-02	1	\N
00000010	03	00000002	\N	D	\N	2013-12-03	1	\N
00000010	03	00000002	\N	D	\N	2013-12-04	1	\N
00000010	03	00000002	\N	D	\N	2013-12-05	1	\N
00000010	03	00000002	\N	F	\N	2013-12-06	1	\N
00000010	03	00000002	\N	F	\N	2013-12-07	1	\N
00000010	03	00000002	\N	D	\N	2013-12-08	1	\N
00000010	03	00000002	\N	D	\N	2013-12-09	1	\N
00000010	03	00000002	\N	D	\N	2013-12-10	1	\N
00000010	03	00000002	\N	D	\N	2013-12-11	1	\N
00000010	03	00000002	\N	D	\N	2013-12-12	1	\N
00000010	03	00000002	\N	F	\N	2013-12-13	1	\N
00000010	03	00000002	\N	F	\N	2013-12-14	1	\N
00000010	03	00000002	\N	D	\N	2013-12-15	1	\N
00000010	03	00000002	\N	D	\N	2013-12-16	1	\N
00000010	03	00000002	\N	D	\N	2013-12-17	1	\N
00000010	03	00000002	\N	D	\N	2013-12-18	1	\N
00000010	03	00000002	\N	D	\N	2013-12-19	1	\N
00000010	03	00000002	\N	F	\N	2013-12-20	1	\N
00000010	03	00000002	\N	F	\N	2013-12-21	1	\N
00000010	03	00000002	\N	D	\N	2013-12-22	1	\N
00000010	03	00000002	\N	D	\N	2013-12-23	1	\N
00000010	03	00000002	\N	D	\N	2013-12-24	1	\N
00000010	03	00000002	\N	D	\N	2013-12-25	1	\N
00000010	03	00000002	\N	D	\N	2013-12-26	1	\N
00000010	03	00000002	\N	F	\N	2013-12-27	1	\N
00000010	03	00000002	\N	F	\N	2013-12-28	1	\N
00000010	03	00000002	\N	D	\N	2013-12-29	1	\N
00000010	03	00000002	\N	D	\N	2013-12-30	1	\N
00000010	03	00000002	\N	D	\N	2013-12-31	1	\N
00000011	03	00000002	\N	D	\N	2013-12-01	1	\N
00000011	03	00000002	\N	D	\N	2013-12-02	1	\N
00000011	03	00000002	\N	D	\N	2013-12-03	1	\N
00000011	03	00000002	\N	D	\N	2013-12-04	1	\N
00000011	03	00000002	\N	D	\N	2013-12-05	1	\N
00000011	03	00000002	\N	F	\N	2013-12-06	1	\N
00000011	03	00000002	\N	F	\N	2013-12-07	1	\N
00000011	03	00000002	\N	D	\N	2013-12-08	1	\N
00000011	03	00000002	\N	D	\N	2013-12-09	1	\N
00000011	03	00000002	\N	D	\N	2013-12-10	1	\N
00000011	03	00000002	\N	D	\N	2013-12-11	1	\N
00000011	03	00000002	\N	D	\N	2013-12-12	1	\N
00000011	03	00000002	\N	F	\N	2013-12-13	1	\N
00000011	03	00000002	\N	F	\N	2013-12-14	1	\N
00000011	03	00000002	\N	D	\N	2013-12-15	1	\N
00000011	03	00000002	\N	D	\N	2013-12-16	1	\N
00000011	03	00000002	\N	D	\N	2013-12-17	1	\N
00000011	03	00000002	\N	D	\N	2013-12-18	1	\N
00000011	03	00000002	\N	D	\N	2013-12-19	1	\N
00000011	03	00000002	\N	F	\N	2013-12-20	1	\N
00000011	03	00000002	\N	F	\N	2013-12-21	1	\N
00000011	03	00000002	\N	D	\N	2013-12-22	1	\N
00000011	03	00000002	\N	D	\N	2013-12-23	1	\N
00000011	03	00000002	\N	D	\N	2013-12-24	1	\N
00000011	03	00000002	\N	D	\N	2013-12-25	1	\N
00000011	03	00000002	\N	D	\N	2013-12-26	1	\N
00000011	03	00000002	\N	F	\N	2013-12-27	1	\N
00000011	03	00000002	\N	F	\N	2013-12-28	1	\N
00000011	03	00000002	\N	D	\N	2013-12-29	1	\N
00000011	03	00000002	\N	D	\N	2013-12-30	1	\N
00000011	03	00000002	\N	D	\N	2013-12-31	1	\N
00000012	03	00000002	\N	D	\N	2013-12-01	1	\N
00000012	03	00000002	\N	D	\N	2013-12-02	1	\N
00000012	03	00000002	\N	D	\N	2013-12-03	1	\N
00000012	03	00000002	\N	D	\N	2013-12-04	1	\N
00000012	03	00000002	\N	D	\N	2013-12-05	1	\N
00000012	03	00000002	\N	F	\N	2013-12-06	1	\N
00000012	03	00000002	\N	F	\N	2013-12-07	1	\N
00000012	03	00000002	\N	D	\N	2013-12-08	1	\N
00000012	03	00000002	\N	D	\N	2013-12-09	1	\N
00000012	03	00000002	\N	D	\N	2013-12-10	1	\N
00000012	03	00000002	\N	D	\N	2013-12-11	1	\N
00000012	03	00000002	\N	D	\N	2013-12-12	1	\N
00000012	03	00000002	\N	F	\N	2013-12-13	1	\N
00000012	03	00000002	\N	F	\N	2013-12-14	1	\N
00000012	03	00000002	\N	D	\N	2013-12-15	1	\N
00000012	03	00000002	\N	D	\N	2013-12-16	1	\N
00000012	03	00000002	\N	D	\N	2013-12-17	1	\N
00000012	03	00000002	\N	D	\N	2013-12-18	1	\N
00000012	03	00000002	\N	D	\N	2013-12-19	1	\N
00000012	03	00000002	\N	F	\N	2013-12-20	1	\N
00000012	03	00000002	\N	F	\N	2013-12-21	1	\N
00000012	03	00000002	\N	D	\N	2013-12-22	1	\N
00000012	03	00000002	\N	D	\N	2013-12-23	1	\N
00000012	03	00000002	\N	D	\N	2013-12-24	1	\N
00000012	03	00000002	\N	D	\N	2013-12-25	1	\N
00000012	03	00000002	\N	D	\N	2013-12-26	1	\N
00000012	03	00000002	\N	F	\N	2013-12-27	1	\N
00000012	03	00000002	\N	F	\N	2013-12-28	1	\N
00000012	03	00000002	\N	D	\N	2013-12-29	1	\N
00000012	03	00000002	\N	D	\N	2013-12-30	1	\N
00000012	03	00000002	\N	D	\N	2013-12-31	1	\N
00000009	04	00000002	\N	F	\N	2012-09-01	0	\N
00000009	04	00000002	\N	D	\N	2012-09-02	0	\N
00000009	04	00000002	\N	D	\N	2012-09-03	0	\N
00000009	04	00000002	\N	D	\N	2012-09-04	0	\N
00000009	04	00000002	\N	D	\N	2012-09-05	0	\N
00000009	04	00000002	\N	D	\N	2012-09-06	0	\N
00000009	04	00000002	\N	F	\N	2012-09-07	0	\N
00000009	04	00000002	\N	F	\N	2012-09-08	0	\N
00000009	04	00000002	\N	D	\N	2012-09-09	0	\N
00000009	04	00000002	\N	D	\N	2012-09-10	0	\N
00000009	04	00000002	\N	D	\N	2012-09-11	0	\N
00000009	04	00000002	\N	D	\N	2012-09-12	0	\N
00000009	04	00000002	\N	D	\N	2012-09-13	0	\N
00000009	04	00000002	\N	F	\N	2012-09-14	0	\N
00000009	04	00000002	\N	F	\N	2012-09-15	0	\N
00000009	04	00000002	\N	D	\N	2012-09-16	0	\N
00000009	04	00000002	\N	D	\N	2012-09-17	0	\N
00000009	04	00000002	\N	D	\N	2012-09-18	0	\N
00000009	04	00000002	\N	D	\N	2012-09-19	0	\N
00000009	04	00000002	\N	D	\N	2012-09-20	0	\N
00000009	04	00000002	\N	F	\N	2012-09-21	0	\N
00000009	04	00000002	\N	F	\N	2012-09-22	0	\N
00000009	04	00000002	\N	D	\N	2012-09-23	0	\N
00000009	04	00000002	\N	D	\N	2012-09-24	0	\N
00000009	04	00000002	\N	D	\N	2012-09-25	0	\N
00000009	04	00000002	\N	D	\N	2012-09-26	0	\N
00000009	04	00000002	\N	D	\N	2012-09-27	0	\N
00000009	04	00000002	\N	F	\N	2012-09-28	0	\N
00000009	04	00000002	\N	F	\N	2012-09-29	0	\N
00000009	04	00000002	\N	D	\N	2012-09-30	0	\N
00000009	04	00000002	\N	F	\N	2013-01-05	0	\N
00000009	04	00000002	\N	D	\N	2013-01-06	0	\N
00000009	04	00000002	\N	D	\N	2013-01-07	0	\N
00000009	04	00000002	\N	D	\N	2013-01-08	0	\N
00000009	04	00000002	\N	D	\N	2013-01-09	0	\N
00000009	04	00000002	\N	F	\N	2013-01-12	0	\N
00000009	04	00000002	\N	D	\N	2013-01-13	0	\N
00000009	04	00000002	\N	D	\N	2013-01-14	0	\N
00000009	04	00000002	\N	D	\N	2013-01-15	0	\N
00000009	04	00000002	\N	D	\N	2013-01-16	0	\N
00000009	04	00000002	\N	D	\N	2013-01-17	0	\N
00000009	04	00000002	\N	F	\N	2013-01-18	0	\N
00000009	04	00000002	\N	F	\N	2013-01-19	0	\N
00000009	04	00000002	\N	D	\N	2013-01-20	0	\N
00000009	04	00000002	\N	D	\N	2013-01-21	0	\N
00000009	04	00000002	\N	D	\N	2013-01-22	0	\N
00000009	04	00000002	\N	D	\N	2013-01-23	0	\N
00000009	04	00000002	\N	D	\N	2013-01-24	0	\N
00000009	04	00000002	\N	F	\N	2013-01-25	0	\N
00000009	04	00000002	\N	F	\N	2013-01-26	0	\N
00000009	04	00000002	\N	D	\N	2013-01-27	0	\N
00000009	04	00000002	\N	D	\N	2013-01-28	0	\N
00000009	04	00000002	\N	D	\N	2013-01-29	0	\N
00000009	04	00000002	\N	D	\N	2013-01-31	0	\N
00000009	04	00000002	\N	D	\N	2012-10-01	0	\N
00000009	04	00000002	\N	D	\N	2012-10-02	0	\N
00000009	04	00000002	\N	D	\N	2012-10-03	0	\N
00000009	04	00000002	\N	D	\N	2012-10-04	0	\N
00000009	04	00000002	\N	F	\N	2012-10-05	0	\N
00000009	04	00000002	\N	F	\N	2012-10-06	0	\N
00000009	04	00000002	\N	D	\N	2012-10-07	0	\N
00000009	04	00000002	\N	D	\N	2012-10-08	0	\N
00000009	04	00000002	\N	D	\N	2012-10-09	0	\N
00000009	04	00000002	\N	D	\N	2012-10-10	0	\N
00000009	04	00000002	\N	D	\N	2012-10-11	0	\N
00000009	04	00000002	\N	F	\N	2012-10-12	0	\N
00000009	04	00000002	\N	F	\N	2012-10-13	0	\N
00000009	04	00000002	\N	D	\N	2012-10-14	0	\N
00000009	04	00000002	\N	D	\N	2012-10-15	0	\N
00000009	04	00000002	\N	D	\N	2012-10-16	0	\N
00000009	04	00000002	\N	D	\N	2012-10-17	0	\N
00000009	04	00000002	\N	D	\N	2012-10-18	0	\N
00000009	04	00000002	\N	F	\N	2012-10-19	0	\N
00000009	04	00000002	\N	F	\N	2012-10-20	0	\N
00000009	04	00000002	\N	D	\N	2012-10-21	0	\N
00000009	04	00000002	\N	D	\N	2012-10-22	0	\N
00000009	04	00000002	\N	D	\N	2012-10-23	0	\N
00000009	04	00000002	\N	D	\N	2012-10-24	0	\N
00000009	04	00000002	\N	D	\N	2012-10-25	0	\N
00000009	04	00000002	\N	F	\N	2012-10-26	0	\N
00000009	04	00000002	\N	F	\N	2012-10-27	0	\N
00000009	04	00000002	\N	D	\N	2012-10-28	0	\N
00000009	04	00000002	\N	D	\N	2012-10-29	0	\N
00000009	04	00000002	\N	D	\N	2012-10-30	0	\N
00000009	04	00000002	\N	D	\N	2012-10-31	0	\N
00000009	04	00000002	\N	D	\N	2012-11-01	0	\N
00000009	04	00000002	\N	F	\N	2012-11-02	0	\N
00000009	04	00000002	\N	F	\N	2012-11-03	0	\N
00000009	04	00000002	\N	D	\N	2012-11-04	0	\N
00000009	04	00000002	\N	D	\N	2012-11-07	0	\N
00000009	04	00000002	\N	D	\N	2012-11-08	0	\N
00000009	04	00000002	\N	F	\N	2012-11-09	0	\N
00000009	04	00000002	\N	F	\N	2012-11-10	0	\N
00000009	04	00000002	\N	D	\N	2012-11-11	0	\N
00000009	04	00000002	\N	D	\N	2012-11-12	0	\N
00000009	04	00000002	\N	D	\N	2012-11-13	0	\N
00000009	04	00000002	\N	D	\N	2012-11-14	0	\N
00000009	04	00000002	\N	D	\N	2012-11-15	0	\N
00000009	04	00000002	\N	F	\N	2012-11-16	0	\N
00000009	04	00000002	\N	F	\N	2012-11-17	0	\N
00000009	04	00000002	\N	D	\N	2012-11-18	0	\N
00000009	04	00000002	\N	D	\N	2012-11-19	0	\N
00000009	04	00000002	\N	D	\N	2012-11-20	0	\N
00000009	04	00000002	\N	D	\N	2012-11-21	0	\N
00000009	04	00000002	\N	D	\N	2012-11-22	0	\N
00000009	04	00000002	\N	F	\N	2012-11-23	0	\N
00000009	04	00000002	\N	F	\N	2012-11-24	0	\N
00000009	04	00000002	\N	D	\N	2012-11-25	0	\N
00000009	04	00000002	\N	D	\N	2012-11-26	0	\N
00000009	04	00000002	\N	D	\N	2012-11-27	0	\N
00000009	04	00000002	\N	D	\N	2012-11-28	0	\N
00000009	04	00000002	\N	D	\N	2012-11-29	0	\N
00000009	04	00000002	\N	F	\N	2012-11-30	0	\N
00000009	04	00000002	\N	F	\N	2012-12-01	0	\N
00000009	04	00000002	\N	D	\N	2012-12-02	0	\N
00000009	04	00000002	\N	D	\N	2012-12-03	0	\N
00000009	04	00000002	\N	D	\N	2012-12-04	0	\N
00000009	04	00000002	\N	D	\N	2012-12-05	0	\N
00000009	04	00000002	\N	D	\N	2012-12-06	0	\N
00000009	04	00000002	\N	F	\N	2012-12-07	0	\N
00000009	04	00000002	\N	F	\N	2012-12-08	0	\N
00000009	04	00000002	\N	D	\N	2012-12-09	0	\N
00000009	04	00000002	\N	D	\N	2012-12-10	0	\N
00000009	04	00000002	\N	D	\N	2012-12-11	0	\N
00000009	04	00000002	\N	D	\N	2012-12-12	0	\N
00000009	04	00000002	\N	D	\N	2012-12-13	0	\N
00000009	04	00000002	\N	F	\N	2012-12-14	0	\N
00000009	04	00000002	\N	F	\N	2012-12-15	0	\N
00000009	04	00000002	\N	D	\N	2012-12-16	0	\N
00000009	04	00000002	\N	D	\N	2012-12-17	0	\N
00000009	04	00000002	\N	D	\N	2012-12-18	0	\N
00000009	04	00000002	\N	D	\N	2012-12-19	0	\N
00000009	04	00000002	\N	D	\N	2012-12-20	0	\N
00000009	04	00000002	\N	F	\N	2012-12-21	0	\N
00000009	04	00000002	\N	F	\N	2012-12-22	0	\N
00000009	04	00000002	\N	D	\N	2012-12-23	0	\N
00000009	04	00000002	\N	D	\N	2012-12-24	0	\N
00000009	04	00000002	\N	D	\N	2012-12-25	0	\N
00000009	04	00000002	\N	D	\N	2012-12-27	0	\N
00000009	04	00000002	\N	F	\N	2012-12-28	0	\N
00000009	04	00000002	\N	F	\N	2012-12-29	0	\N
00000009	04	00000002	\N	D	\N	2012-12-30	0	\N
00000009	04	00000002	\N	D	\N	2012-12-31	0	\N
00000009	04	00000002	\N	F	\N	2013-02-01	1	\N
00000009	04	00000002	\N	F	\N	2013-02-02	1	\N
00000009	04	00000002	\N	D	\N	2013-02-03	1	\N
00000009	04	00000002	\N	D	\N	2013-02-04	1	\N
00000009	04	00000002	\N	D	\N	2013-02-05	1	\N
00000009	04	00000002	\N	D	\N	2013-02-06	1	\N
00000009	04	00000002	\N	D	\N	2013-02-07	1	\N
00000009	04	00000002	\N	F	\N	2013-02-08	0	\N
00000009	04	00000002	\N	F	\N	2013-02-09	1	\N
00000009	04	00000002	\N	D	\N	2013-02-10	1	\N
00000009	04	00000002	\N	D	\N	2013-02-11	1	\N
00000009	04	00000002	\N	D	\N	2013-02-12	1	\N
00000009	04	00000002	\N	D	\N	2013-02-13	1	\N
00000009	04	00000002	\N	D	\N	2013-02-14	1	\N
00000009	04	00000002	\N	F	\N	2013-02-15	1	\N
00000009	04	00000002	\N	F	\N	2013-02-16	1	\N
00000009	04	00000002	\N	D	\N	2013-02-17	1	\N
00000009	04	00000002	\N	D	\N	2013-02-18	1	\N
00000009	04	00000002	\N	D	\N	2013-02-19	1	\N
00000009	04	00000002	\N	D	\N	2013-02-20	1	\N
00000009	04	00000002	\N	D	\N	2013-02-21	1	\N
00000009	04	00000002	\N	F	\N	2013-02-22	1	\N
00000009	04	00000002	\N	F	\N	2013-02-23	1	\N
00000009	04	00000002	\N	D	\N	2013-02-24	1	\N
00000009	04	00000002	\N	D	\N	2013-02-25	1	\N
00000009	04	00000002	\N	D	\N	2013-02-26	1	\N
00000009	04	00000002	\N	D	\N	2013-02-27	1	\N
00000009	04	00000002	\N	D	\N	2013-02-28	1	\N
00000082	04	00000003	\N	F	\N	2013-02-01	1	\N
00000082	04	00000003	\N	F	\N	2013-02-02	1	\N
00000082	04	00000003	\N	D	\N	2013-02-03	1	\N
00000082	04	00000003	\N	D	\N	2013-02-04	1	\N
00000082	04	00000003	\N	D	\N	2013-02-05	1	\N
00000082	04	00000003	\N	D	\N	2013-02-06	1	\N
00000082	04	00000003	\N	D	\N	2013-02-07	1	\N
00000082	04	00000003	\N	F	\N	2013-02-08	1	\N
00000082	04	00000003	\N	F	\N	2013-02-09	1	\N
00000082	04	00000003	\N	D	\N	2013-02-10	1	\N
00000082	04	00000003	\N	D	\N	2013-02-11	1	\N
00000082	04	00000003	\N	D	\N	2013-02-12	1	\N
00000082	04	00000003	\N	D	\N	2013-02-13	1	\N
00000082	04	00000003	\N	D	\N	2013-02-14	1	\N
00000082	04	00000003	\N	F	\N	2013-02-15	1	\N
00000082	04	00000003	\N	F	\N	2013-02-16	1	\N
00000082	04	00000003	\N	D	\N	2013-02-17	1	\N
00000082	04	00000003	\N	D	\N	2013-02-18	1	\N
00000082	04	00000003	\N	D	\N	2013-02-19	1	\N
00000082	04	00000003	\N	D	\N	2013-02-20	1	\N
00000082	04	00000003	\N	D	\N	2013-02-21	1	\N
00000082	04	00000003	\N	F	\N	2013-02-22	1	\N
00000082	04	00000003	\N	F	\N	2013-02-23	1	\N
00000082	04	00000003	\N	D	\N	2013-02-24	1	\N
00000082	04	00000003	\N	D	\N	2013-02-25	1	\N
00000082	04	00000003	\N	D	\N	2013-02-26	1	\N
00000082	04	00000003	\N	D	\N	2013-02-27	1	\N
00000082	04	00000003	\N	D	\N	2013-02-28	1	\N
00000083	04	00000003	\N	F	\N	2013-02-01	1	\N
00000083	04	00000003	\N	F	\N	2013-02-02	1	\N
00000083	04	00000003	\N	D	\N	2013-02-03	1	\N
00000083	04	00000003	\N	D	\N	2013-02-04	1	\N
00000083	04	00000003	\N	D	\N	2013-02-05	1	\N
00000083	04	00000003	\N	D	\N	2013-02-06	1	\N
00000083	04	00000003	\N	D	\N	2013-02-07	1	\N
00000083	04	00000003	\N	F	\N	2013-02-08	1	\N
00000083	04	00000003	\N	F	\N	2013-02-09	1	\N
00000083	04	00000003	\N	D	\N	2013-02-10	1	\N
00000083	04	00000003	\N	D	\N	2013-02-11	1	\N
00000083	04	00000003	\N	D	\N	2013-02-12	1	\N
00000083	04	00000003	\N	D	\N	2013-02-13	1	\N
00000083	04	00000003	\N	D	\N	2013-02-14	1	\N
00000083	04	00000003	\N	F	\N	2013-02-15	1	\N
00000083	04	00000003	\N	F	\N	2013-02-16	1	\N
00000083	04	00000003	\N	D	\N	2013-02-17	1	\N
00000083	04	00000003	\N	D	\N	2013-02-18	1	\N
00000083	04	00000003	\N	D	\N	2013-02-19	1	\N
00000083	04	00000003	\N	D	\N	2013-02-20	1	\N
00000083	04	00000003	\N	D	\N	2013-02-21	1	\N
00000083	04	00000003	\N	F	\N	2013-02-22	1	\N
00000083	04	00000003	\N	F	\N	2013-02-23	1	\N
00000083	04	00000003	\N	D	\N	2013-02-24	1	\N
00000083	04	00000003	\N	D	\N	2013-02-25	1	\N
00000083	04	00000003	\N	D	\N	2013-02-26	1	\N
00000083	04	00000003	\N	D	\N	2013-02-27	1	\N
00000083	04	00000003	\N	D	\N	2013-02-28	1	\N
00000017	02	00000002	\N	D	0000000055	2013-02-20	R	\N
00000017	02	00000002	\N	D	0000000055	2013-02-21	R	\N
00000017	02	00000002	\N	F	0000000055	2013-02-22	R	\N
00000017	02	00000002	\N	F	0000000055	2013-02-23	R	\N
00000017	02	00000002	\N	D	0000000055	2013-02-24	R	\N
\.


--
-- TOC entry 2594 (class 0 OID 24785)
-- Dependencies: 210
-- Data for Name: ht_res_reserva; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_reserva (res_id, cl_id, res_fecha, su_id, res_total, res_promo, res_tarj_id, res_nomb_tarj, res_numero_tarj, res_tarj_caducidad, res_cod_tarj, res_monto_bonos, res_monto_promo, res_estado, res_persona, id_id, res_codigo) FROM stdin;
0000000002	00000002	2012-11-01	00000002	627	\N	0001	temporal	42156878891	04/2014	321	\N	\N	1	2	\N	\N
0000000001	00000001	\N	\N	0	N               	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N
0000000003	00000003	2012-11-03	00000002	306	\N	0001	temporal	42156878891	04/2014	321	\N	\N	1	1	\N	\N
0000000004	00000003	2012-11-03	00000002	0	\N	0001	temporal	42156878891	09/2020	321	\N	\N	1	1	\N	\N
0000000005	00000003	2012-11-03	00000002	354	\N	0001	temporal	42156878891	11/2019	321	\N	\N	1	1	\N	\N
0000000006	00000004	2012-11-03	00000002	153	\N	0003	temporal	42156878891	10/2019	321	\N	\N	1	1	\N	\N
0000000007	00000004	2012-11-03	00000002	149	\N	0001	temporal	42156878891	01/2016	321	\N	\N	1	3	\N	\N
0000000008	00000003	2012-11-03	00000002	458.4	\N	0001	temporal	42156878891	01/2012	321	\N	\N	1	1	\N	\N
0000000009	00000005	2012-11-05	00000002	106	\N	0001	temporal	42156878891	02/2014	321	\N	\N	1	1	\N	\N
0000000010	00000006	2012-11-05	00000002	106	\N	0001	temporal	42156878891	11/2012	321	\N	\N	1	1	\N	\N
0000000011	00000006	2012-11-05	00000002	106	\N	0001	temporal	42156878891	11/2012	321	\N	\N	1	1	\N	\N
0000000012	00000006	2012-11-05	00000002	74.2	\N	0001	temporal	42156878891	11/2012	321	\N	\N	1	1	\N	\N
0000000013	00000006	2012-11-05	00000002	0	\N	0001	temporal	42156878891	12/2012	321	\N	\N	1	1	\N	\N
0000000014	00000006	2012-11-05	00000002	859	\N	0001	temporal	42156878891	01/2013	321	\N	\N	1	2	\N	\N
0000000015	00000007	2012-11-16	00000002	123.2	\N	0001	temporal	42156878891	10/	321	\N	\N	1	1	\N	\N
0000000016	00000008	2012-11-17	00000002	153	\N	0001	temporal	42156878891	04/2013	321	\N	\N	1	1	\N	\N
0000000017	00000009	2012-11-17	00000002	15	\N	0001	temporal	42156878891	04/2012	321	\N	\N	1	2	\N	\N
0000000018	00000009	2012-11-17	00000002	0	\N	0001	temporal	42156878891	/2014	321	\N	\N	1	2	\N	\N
0000000019	00000010	2012-11-19	00000002	119	\N	0001	temporal	42156878891	01/2012	321	\N	\N	1	1	\N	\N
0000000020	00000011	2012-11-20	00000003	68	\N	0001	temporal	42156878891	07/2014	321	\N	\N	1	2	\N	\N
0000000021	00000012	2012-11-24	00000003	75	\N	0001	temporal	42156878891	10/2013	321	\N	\N	1	2	\N	\N
0000000022	00000013	2012-11-26	00000002	129	\N	0001	temporal	42156878891	11/2014	321	\N	\N	1	1	\N	\N
0000000023	00000013	2012-11-26	00000003	527	\N	0002	temporal	42156878891	02/2014	321	\N	\N	1	1	\N	\N
0000000024	00000013	2012-11-26	00000003	75	\N	0001	temporal	42156878891	08/2013	321	\N	\N	1	2	\N	\N
0000000025	00000013	2012-11-26	00000002	177	\N	0001	temporal	42156878891	11/2014	321	\N	\N	1	1	\N	\N
0000000026	00000014	2012-11-26	00000003	150	\N	0001	temporal	42156878891	09/2014	321	\N	\N	1	1	\N	\N
0000000027	00000015	2012-11-28	00000002	106	\N	0001	temporal	42156878891	09/2018	321	\N	\N	1	2	\N	\N
0000000028	00000016	2012-12-03	00000002	252	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000029	00000017	2012-12-04	00000002	212	\N	0002	temporal	42156878891	10/2014	321	\N	\N	1	1	\N	\N
0000000030	00000017	2012-12-04	00000003	124	\N	0002	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000031	00000017	2012-12-04	00000003	0	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000032	00000017	2012-12-04	00000003	0	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000033	00000017	2012-12-04	00000003	0	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000034	00000017	2012-12-04	00000003	124	\N	0001	temporal	42156878891	04/2014	321	\N	\N	1	1	\N	\N
0000000035	00000018	2012-12-04	00000002	270	\N	0001	temporal	42156878891	12/2013	321	\N	\N	1	1	\N	\N
0000000036	00000019	2012-12-14	00000002	149	\N	0003	temporal	42156878891	03/2014	321	\N	\N	1	3	\N	\N
0000000037	00000020	2012-12-18	00000002	341	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	1	\N	\N
0000000038	00000021	2012-12-18	00000003	75	\N	0001	temporal	42156878891	05/2013	321	\N	\N	1	2	\N	\N
0000000039	00000022	2012-12-22	00000002	141.5	\N	0001	temporal	42156878891	12/2020	321	\N	\N	1	1	\N	\N
0000000040	00000022	2012-12-22	00000002	170.45	\N	0003	temporal	42156878891	01/2012	321	\N	\N	1	1	\N	\N
0000000041	00000023	2012-12-22	00000002	205	\N	0001	temporal	42156878891	03/2014	321	\N	\N	1	1	\N	\N
0000000042	00000003	2012-12-30	00000002	596	\N	0001	temporal	42156878891	04/2014	321	\N	\N	1	1	ES	\N
0000000043	00000003	2012-12-31	00000002	684	\N	0002	temporal	42156878891	03/2014	321	\N	\N	1	1	ES	\N
0000000044	00000024	2013-01-01	00000002	925.5	\N	0001	temporal	42156878891	04/2014	321	\N	\N	1	1	ES	50E3604BC2474
0000000045	00000025	2013-01-02	00000002	106	\N	0001	temporal	42156878891	04/2013	321	\N	\N	0	2	ES	50E467BE79154
0000000046	00000026	2013-01-02	00000003	75	\N	0003	temporal	42156878891	02/2013	321	\N	\N	1	2	ES	50E46B08ECEC3
0000000047	00000027	2013-01-06	00000002	784	\N	0001	temporal	42156878891	03/2015	321	\N	\N	1	1	ES	50E990B08FD0B
0000000048	00000028	2013-01-06	00000003	2262	\N	0002	temporal	42156878891	/	321	\N	\N	1	1	ES	50E9B68165EF1
0000000049	00000027	2013-01-08	00000003	286	\N	0001	temporal	42156878891	09/2017	321	\N	\N	1	1	ES	50EC8055455F0
0000000050	00000027	2013-01-10	00000002	410	\N	0001	Visa	4557880799674621	04/2015	\N	\N	\N	1	1	ES	50EF728706FD2
0000000051	00000027	2013-01-10	00000002	521	\N	0001	Visa	4557880799674621	07/2017	\N	\N	\N	1	1	ES	50EF781B8E852
0000000052	00000027	2013-01-10	00000002	497	\N	0001	Visa	4557880799674621	02/2015	\N	\N	\N	1	1	ES	50EF797BF236F
0000000053	00000027	2013-01-11	00000002	205	\N	0001	Visa	4557880799674621	05/2014	\N	\N	\N	1	1	ES	50F0B31CD5ADF
0000000054	00000027	2013-01-11	00000003	358.4	\N	0001	Visa	4557880799674621	03/2015	\N	\N	\N	1	1	ES	50F0B6073756B
0000000055	00000002	2013-02-16	00000002	562.5	\N	0001	Visa	4557880799674621	04/2015	\N	\N	\N	1	1	ES	51200EE0BD53E
\.


--
-- TOC entry 2595 (class 0 OID 24793)
-- Dependencies: 211
-- Data for Name: ht_res_reserva_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_res_reserva_detalle (res_id, red_item, hb_id, pro_id, an_id, ser_id, red_fecini, red_fecfin, red_precio, red_tipo, ac_id, pro_dias) FROM stdin;
0000000002	1	00000017	\N	\N	\N	2012-11-02	2012-11-08	627.00	R	\N	\N
0000000003	1	00000010	\N	\N	\N	2012-11-06	2012-11-08	306.00	R	\N	\N
0000000005	1	00000009	\N	\N	\N	2012-11-05	2012-11-07	354.00	R	\N	\N
0000000006	1	00000010	\N	\N	\N	2012-11-21	2012-11-22	153.00	R	\N	\N
0000000007	1	00000017	\N	\N	\N	2012-11-13	2012-11-14	149.00	R	\N	\N
0000000008	1	00000013	\N	\N	\N	2012-11-14	2012-11-17	398.40	R	\N	\N
0000000008	2	\N	NBO	2012	\N	2012-11-02	2012-12-01	99.60	P	\N	3
0000000008	3	\N	\N	\N	\N	2012-11-14	2012-11-17	60.00	A	0010	\N
0000000009	1	00000076	\N	\N	\N	2012-11-14	2012-11-15	106.00	R	\N	\N
0000000010	1	00000076	\N	\N	\N	2012-11-15	2012-11-16	106.00	R	\N	\N
0000000011	1	00000076	\N	\N	\N	2012-11-05	2012-11-06	106.00	R	\N	\N
0000000012	1	00000076	\N	\N	\N	2012-11-07	2012-11-08	74.20	R	\N	\N
0000000012	2	\N	24	2012	\N	2012-11-07	2012-11-07	31.80	P	\N	1
0000000013	1	00000010	\N	\N	\N	2012-11-30	2012-12-01	0.00	R	\N	\N
0000000013	2	\N	16	2012	\N	2012-11-05	2012-11-30	141.00	P	\N	1
0000000014	1	00000009	\N	\N	\N	2013-01-01	2013-01-05	859.00	R	\N	\N
0000000015	1	00000013	\N	\N	\N	2012-11-21	2012-11-22	103.20	R	\N	\N
0000000015	2	\N	NBO	2012	\N	2012-11-02	2012-12-01	25.80	P	\N	1
0000000015	3	\N	\N	\N	\N	2012-11-21	2012-11-22	20.00	A	0010	\N
0000000016	1	00000010	\N	\N	\N	2013-01-08	2013-01-09	153.00	R	\N	\N
0000000017	1	00000010	\N	\N	\N	2012-11-23	2012-11-24	0.00	R	\N	\N
0000000017	2	\N	16	2012	\N	2012-11-05	2012-11-30	141.00	P	\N	1
0000000017	3	\N	\N	\N	\N	2012-11-23	2012-11-24	15.00	A	0017	\N
0000000019	1	00000001	\N	\N	\N	2012-11-30	2012-12-01	99.00	R	\N	\N
0000000019	2	\N	\N	\N	\N	2012-11-30	2012-12-01	20.00	A	0010	\N
0000000020	1	00000042	\N	\N	\N	2012-12-29	2012-12-30	68.00	R	\N	\N
0000000021	1	00000042	\N	\N	\N	2012-12-02	2012-12-03	75.00	R	\N	\N
0000000022	1	00000013	\N	\N	\N	2012-12-05	2012-12-06	129.00	R	\N	\N
0000000023	1	00000052	\N	\N	\N	2012-12-01	2012-12-05	527.00	R	\N	\N
0000000024	1	00000042	\N	\N	\N	2012-12-16	2012-12-17	75.00	R	\N	\N
0000000025	1	00000009	\N	\N	\N	2012-12-26	2012-12-27	177.00	R	\N	\N
0000000026	1	00000042	\N	\N	\N	2012-11-27	2012-11-29	150.00	R	\N	\N
0000000027	1	00000001	\N	\N	\N	2012-12-06	2012-12-07	106.00	R	\N	\N
0000000028	1	00000001	\N	\N	\N	2013-01-08	2013-01-10	212.00	R	\N	\N
0000000028	2	\N	\N	\N	\N	2013-01-08	2013-01-10	40.00	A	0010	\N
0000000029	1	00000001	\N	\N	\N	2012-12-19	2012-12-21	212.00	R	\N	\N
0000000030	1	00000053	\N	\N	\N	2012-12-12	2012-12-13	124.00	R	\N	\N
0000000034	1	00000054	\N	\N	\N	2012-12-12	2012-12-13	124.00	R	\N	\N
0000000035	1	00000017	\N	\N	\N	2012-12-19	2012-12-21	270.00	R	\N	\N
0000000036	1	00000017	\N	\N	\N	2012-12-31	2013-01-01	149.00	R	\N	\N
0000000037	1	00000009	\N	\N	\N	2013-01-10	2013-01-12	341.00	R	\N	\N
0000000038	1	00000042	\N	\N	\N	2012-12-20	2012-12-21	75.00	R	\N	\N
0000000039	1	00000017	\N	\N	\N	2013-01-23	2013-01-24	121.50	R	\N	\N
0000000039	2	\N	VISALIM	2012	\N	2013-01-01	2013-01-31	13.50	P	\N	1
0000000039	3	\N	\N	\N	\N	2013-01-23	2013-01-24	20.00	A	0010	\N
0000000040	1	00000009	\N	\N	\N	2013-01-30	2013-01-31	150.45	R	\N	\N
0000000040	2	\N	NBO	2012	\N	2013-01-01	2013-01-31	26.55	P	\N	1
0000000040	3	\N	\N	\N	\N	2013-01-30	2013-01-31	20.00	A	0010	\N
0000000041	1	00000001	\N	\N	\N	2012-12-27	2012-12-29	205.00	R	\N	\N
0000000042	1	00000002	\N	\N	\N	2013-01-08	2013-01-12	516.00	R	\N	\N
0000000042	2	\N	\N	\N	\N	2013-01-08	2013-01-12	80.00	A	0010	\N
0000000043	1	00000017	\N	\N	\N	2013-01-10	2013-01-15	684.00	R	\N	\N
0000000043	2	\N	VISALIM	2012	\N	2013-01-01	2013-01-31	76.00	P	\N	5
0000000044	1	00000017	\N	\N	\N	2013-01-02	2013-01-08	805.50	R	\N	\N
0000000044	2	\N	VISALIM	2012	\N	2013-01-01	2013-01-31	89.50	P	\N	6
0000000044	3	\N	\N	\N	\N	2013-01-02	2013-01-08	120.00	A	0010	\N
0000000045	1	00000001	\N	\N	\N	2013-01-03	2013-01-04	106.00	R	\N	\N
0000000046	1	00000042	\N	\N	\N	2013-01-03	2013-01-04	75.00	R	\N	\N
0000000047	1	00000018	\N	\N	\N	2013-01-10	2013-01-15	684.00	R	\N	\N
0000000047	2	\N	VISALIM	2012	\N	2013-01-01	2013-01-31	76.00	P	\N	5
0000000047	3	\N	\N	\N	\N	2013-01-10	2013-01-15	100.00	A	0010	\N
0000000048	1	00000042	\N	\N	\N	2013-01-09	2013-01-10	2262.00	R	\N	\N
0000000049	1	00000043	\N	\N	\N	2013-01-09	2013-01-12	286.00	R	\N	\N
0000000050	1	00000001	\N	\N	\N	2013-01-11	2013-01-15	410.00	R	\N	\N
0000000051	1	00000019	\N	\N	\N	2013-01-11	2013-01-15	441.00	R	\N	\N
0000000051	2	\N	24	2012	\N	2013-01-01	2013-01-31	49.00	P	\N	4
0000000051	3	\N	\N	\N	\N	2013-01-11	2013-01-15	80.00	A	0010	\N
0000000052	1	00000002	\N	\N	\N	2013-01-12	2013-01-16	417.00	R	\N	\N
0000000052	2	\N	\N	\N	\N	2013-01-12	2013-01-16	80.00	A	0010	\N
0000000053	1	00000003	\N	\N	\N	2013-01-12	2013-01-14	205.00	R	\N	\N
0000000054	1	00000049	\N	\N	\N	2013-01-11	2013-01-15	278.40	R	\N	\N
0000000054	2	\N	16	2012	\N	2013-01-01	2013-01-31	69.60	P	\N	4
0000000054	3	\N	\N	\N	\N	2013-01-11	2013-01-15	80.00	A	0010	\N
0000000055	1	00000017	\N	\N	\N	2013-02-20	2013-02-25	562.50	R	\N	\N
0000000055	2	\N	VISA2	2012	\N	2013-01-01	2013-02-28	62.50	P	\N	5
\.


--
-- TOC entry 2596 (class 0 OID 24797)
-- Dependencies: 212
-- Data for Name: ht_seg_cliente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_cliente (cl_id, pe_id, cl_estado, cl_puntos, us_id, per_id) FROM stdin;
00000002	0000000002	1	0	00000002	0003
00000003	0000000003	1	0	00000003	0003
00000004	0000000004	1	0	00000004	0003
00000005	0000000005	1	0	00000005	0003
00000006	0000000006	1	0	00000006	0003
00000007	0000000007	1	0	00000007	0003
00000008	0000000008	1	0	00000008	0003
00000009	0000000009	1	0	00000009	0003
00000010	0000000010	1	0	00000010	0003
00000011	0000000011	1	0	00000011	0003
00000012	0000000012	1	0	00000012	0003
00000013	0000000013	1	0	00000013	0003
00000014	0000000014	1	0	00000014	0003
00000015	0000000015	1	0	00000015	0003
00000016	0000000016	1	0	00000016	0003
00000017	0000000017	1	0	00000017	0003
00000018	0000000018	1	0	00000018	0003
00000019	0000000019	1	0	00000019	0003
00000020	0000000020	1	0	00000020	0003
00000021	0000000021	1	0	00000021	0003
00000022	0000000022	1	0	00000022	0003
00000023	0000000023	1	0	00000023	0003
00000024	0000000024	1	0	00000024	0003
00000025	0000000025	1	0	00000025	0003
00000026	0000000026	1	0	00000026	0003
00000027	0000000027	1	0	00000027	0003
00000028	0000000028	1	0	00000028	0003
00000001	0000000001	1	0	00000001	\N
\.


--
-- TOC entry 2597 (class 0 OID 24803)
-- Dependencies: 213
-- Data for Name: ht_seg_idioma_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_idioma_menu (id_id, me_id, me_desc) FROM stdin;
EN	00001004	Password
ES	00001001	Usuario
ES	00001002	Perfiles
ES	00001004	Password
ES	00001005	Nivel de Acceso por Usuario
ES	00001006	Nivel de Acceso por Perfil
ES	00002001	Inventario
ES	00002000	Reservas
ES	00002004	Consulta
ES	00003000	Asignación
ES	00003003	Servicio de Sucursal
EN	00001006	Nivel de Acceso por Perfil
ES	00004000	Mantenimiento
ES	00004009	Hotel
ES	00003002	Promociones por Sucursal
ES	00003008	Precios de Habitaciones
EN	00004009	Hotel
EN	00002007	Calendar Generation
EN	00002008	3 Months Inventory
ES	00002008	Inventario 3 Meses
ES	00002009	Bloqueo por Rango
ES	00001000	Seguridad
ES	00004006	Clientes
ES	00004005	Servicios
EN	00002001	Inventory
EN	00002000	Reservations
EN	00001000	Security
EN	00001001	User
EN	00001002	Profiles
EN	00001005	User Access Level
EN	00002004	Consultation
EN	00002006	Management by Type of Room
EN	00002009	Lock Range
EN	00003000	Assignation
EN	00003002	Promotions by Branch
EN	00003003	Services Branch
EN	00003007	Room Rate Management
EN	00003008	Price for
EN	00004000	Maintenance
EN	00004001	Habitation
EN	00004005	Services
EN	00004006	Customers
EN	00004010	Promotion
EN	00004003	Room Amenities
ES	00004003	Room Amenities
EN	00004008	Banners
ES	00004008	Banners
ES	00004002	Gestion SEWE
EN	00004002	Gestion SEWE
ES	00004011	Descripciones
EN	00004011	Description
EN	00004007	Titles
ES	00004012	Eventos y Noticias
EN	00004012	Events & News
ES	00003006	Títulos por Banner y Sucursal
EN	00003006	Titles by Banner and Branch
ES	00003001	Accesorios por Categoría
EN	00003001	Accessories by Category
ES	00003009	SEWE por Sucursal
EN	00003009	SEWE by Branch
ES	00004001	Habitación
ES	00004007	Títulos
ES	00004010	Promoción
ES	00004004	Generales
EN	00004004	General
ES	00002007	Generación de Calendario
ES	00002006	Gestión por tipo de Habitacion
ES	00002003	Gestión de Habitaciones
EN	00002003	Rooms Management
ES	00002005	Inventario de Habitaciones
EN	00002005	Inventory of Rooms
ES	00003007	Tipo Habitacion
EN	00002010	Room availability
ES	00002010	Room availability
\.


--
-- TOC entry 2598 (class 0 OID 24806)
-- Dependencies: 214
-- Data for Name: ht_seg_idioma_perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_idioma_perfil (id_id, per_id, per_desc) FROM stdin;
EN	0002	Administrador Local
ES	0003	Clientes
ES	0004	Administrador Sucursal
ES	0002	Administrador Local
ES	0001	Administrador Gral
EN	0001	General Manager
\.


--
-- TOC entry 2599 (class 0 OID 24809)
-- Dependencies: 215
-- Data for Name: ht_seg_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_menu (me_id, mo_id, ht_me_id, me_link, me_estado, me_orden) FROM stdin;
00003008	00	00003000	/habitacion/asignacion-habitacion-tarifas/form	1	\N
00002005	00	00002000	/reserva/reserva-inventario-tipo-hab/form	1	\N
00002007	00	00002000	/reserva/reserva-proceso-genera/form	1	\N
00004007	00	00004000	/cms/mantenimiento-articulo/form	1	\N
00002008	00	00002000	/reserva/reserva-inventario-meses/form	1	\N
00004009	00	00004000	/cms/mantenimiento-hotel/form	1	\N
00004010	00	00004000	/cms/mantenimiento-promocion/form	1	\N
00001005	00	00001000	/seguridad/seguridad-nivel-acceso-usuario/form	1	\N
00001002	00	00001000	/seguridad/seguridad-perfiles/form	1	\N
00001001	00	00001000	/seguridad/seguridad-usuario/form	1	\N
00002004	00	00002000	/reserva/reserva-consulta/form	1	\N
00002003	00	00002000	/reserva/reserva-gestion/form	1	\N
00002009	00	00002000	/reserva/reserva-bloqueo-rango/form	1	\N
00002001	00	00002000	/reserva/reserva-inventario/form	0	\N
00004001	00	00004000	/habitacion/mantenimiento-habitacion/form	1	\N
00004002	00	00004000	/habitacion/mantenimiento-tipo-habitacion/form	1	\N
00004004	00	00004000	/seguridad/mantenimiento-tablas/form	1	\N
00004006	00	00004000	/seguridad/mantenimiento-cliente/form	1	\N
00004005	00	00004000	/servicios/mantenimiento-servicio/form	1	\N
00004003	00	00004000	/habitacion/mantenimiento-accesorios/form	1	\N
00003001	00	00003000	/habitacion/asignacion-accesorio-tipo-hab/form	1	\N
00001007	00	00001000	/cms/mantenimiento-promocion/form	1	\N
00003003	00	00003000	/servicios/asignacion-servicio-sucursal/form	1	\N
00004008	00	00004000	/cms/mantenimiento-categorias/form	1	\N
00003006	00	00003000	/cms/asignacion-articulo-categoria-sucursal/form	1	\N
00003007	00	00003000	/habitacion/asignacion-tarifa-tipo-hab/form	1	\N
00003002	00	00003000	/cms/asignacion-promocion-sucursal/form	1	\N
00001004	00	00001000	/seguridad/seguridad-password/form	0	\N
00001006	00	00001000	/seguridad/seguridad-nivel-acceso-perfil/form	0	\N
00003009	00	00003000	/cms/asignacion-categoria-tipo-hab/form	1	\N
00002006	00	00002000	/reserva/reserva-gestion-tipo-hab/form	0	\N
00004011	00	00004000	/cms/mantenimiento-descripciones/form	1	\N
00004000	00	\N	\N	1	1
00003000	00	\N	\N	1	2
00002000	00	\N	\N	1	3
00001000	00	\N	\N	1	4
00004012	00	00004000	/cms/mantenimiento-publicaciones/form	1	\N
00002010	00	00002000	/reserva/reserva-room-avail/form	1	\N
\.


--
-- TOC entry 2600 (class 0 OID 24812)
-- Dependencies: 216
-- Data for Name: ht_seg_modulo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_modulo (mo_id, mo_desc, mo_estado) FROM stdin;
00	cms	1
\.


--
-- TOC entry 2601 (class 0 OID 24815)
-- Dependencies: 217
-- Data for Name: ht_seg_perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_perfil (per_id, per_desc, per_estado) FROM stdin;
0001	Administrador Generalsdddd	1
0002	Administrador Localdddddsssss	1
0003	cLIENTES	1
0004	\N	1
\.


--
-- TOC entry 2602 (class 0 OID 24818)
-- Dependencies: 218
-- Data for Name: ht_seg_perfil_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_perfil_menu (me_id, su_id, per_id, pem_permiso, pem_estado) FROM stdin;
\.


--
-- TOC entry 2603 (class 0 OID 24821)
-- Dependencies: 219
-- Data for Name: ht_seg_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_persona (pe_id, ub_id, pe_nombre, pe_paterno, pe_materno, pe_fec_nacimiento, pe_lugar_nacimiento, pe_estado, pe_dni, pe_pais, pe_nacionalidad, pe_nomcomp, pe_sexo, pe_tipdoc, pe_company, pe_direccion, pe_telefono, pe_qphciudad) FROM stdin;
0000000002	\N	Joaquin	TARAZONA	\N	\N	\N	\N	\N	01  	\N	TARAZONA, Joaquin	\N	\N	\N	sdsd	962718800	\N
0000000001	010109	Hotels	Hotels	Hotels	1987-05-04	HUanuco	1	44386298	Peru	0001	Hotels Hotels, Hotels	M	1	\N	\N	\N	\N
0000000003	\N	Joaquin	TARAZONA	\N	\N	\N	\N	\N	01  	\N	TARAZONAA, Joaquin	\N	\N	\N	15432	961718800	\N
0000000004	\N	D	D	\N	\N	\N	\N	\N	01  	\N	D, D	\N	\N	\N	jorge chavez 206		\N
0000000005	\N	Diego	CASTRO	\N	\N	\N	\N	\N	01  	\N	CASTRO, Diego	\N	\N	\N	Jorge Chavez 206		\N
0000000006	\N	Luis	CAMPOS	\N	\N	\N	\N	\N	01  	\N	CAMPOS, Luis	\N	\N	\N	Jr. Francisco de Zela 1020	2655170	\N
0000000007	\N	D	D	\N	\N	\N	\N	\N	15  	\N	D, D	\N	\N	\N	d		\N
0000000008	\N	A	A	\N	\N	\N	\N	\N	01  	\N	A, A	\N	\N	\N	q	343242134	\N
0000000009	\N	A	A	\N	\N	\N	\N	\N	07  	\N	A, A	\N	\N	\N	q		\N
0000000010	\N	D	D	\N	\N	\N	\N	\N	01  	\N	D, D	\N	\N	\N	asas11		\N
0000000011	\N	Alexis 	MONJE	\N	\N	\N	\N	\N	06  	\N	MONJESAN, Alexis 	\N	\N	\N	Victor Garrido 2886	62185137	\N
0000000012	\N	Oscar	GONZALEZ	\N	\N	\N	\N	\N	13  	\N	GONZALEZURUCHURTU, Oscar	\N	\N	\N	Lago de Barbarisa, 14		\N
0000000013	\N	Luis	CAMPOS		\N	\N	\N	\N	01  	\N	CAMPOS , Luis	\N	\N	\N	Jr. Francisco de Zela 1020	2655170	\N
0000000014	\N	Daniele	FONTANINI		\N	\N	\N	\N	04  	\N	FONTANINI , Daniele	\N	\N	\N	Rua Helena		\N
0000000015	\N	D	D		\N	\N	\N	\N	12  	\N	D , D	\N	\N	\N	asas11		\N
0000000016	\N	Carlos	PONCE		\N	\N	\N	\N	03  	\N	PONCE , Carlos	\N	\N	\N	dfadflkad	1231564	\N
0000000017	\N	Luis	CAMPOS		\N	\N	\N	\N	01  	\N	CAMPOS , Luis	\N	\N	\N	dfsadfa	12365484	\N
0000000018	\N	Dasdf	FGDGF		\N	\N	\N	\N	01  	\N	FGDGF , Dasdf	\N	\N	\N	asdfasdf	997371526	\N
0000000019	\N	Jay Z	CAMPOS		\N	\N	\N	\N	01  	\N	CAMPOS , Jay Z	\N	\N	\N	sdf	4655544	\N
0000000020	\N	Luis	DASFADSF		\N	\N	\N	\N	08  	\N	DASFADSF , Luis	\N	\N	\N	fadsf	2655170	\N
0000000021	\N	Efasdfa	ADFAD		\N	\N	\N	\N	23  	\N	ADFAD , Efasdfa	\N	\N	\N	sdfadf	2655170	\N
0000000022	\N	Marcelo	CARRANZA		\N	\N	\N	\N	01  	\N	CARRANZA , Marcelo	\N	\N	\N	lima	015223266	\N
0000000023	\N	Fgsdh	GFGFH		\N	\N	\N	\N	04  	\N	GFGFH , Fgsdh	\N	\N	\N	gfhj		\N
0000000024	\N	Joaquin	TARAZONA		\N	\N	\N	\N	175 	\N	TARAZONA , Joaquin	\N	\N	\N	Jr Huanucp	962718856	\N
0000000025	\N	Horacio 	PARRA		\N	\N	\N	\N	175 	\N	PARRA , Horacio 	\N	\N	\N	Jr. Santa Fe 154	2655170	\N
0000000026	\N	Margarita	GUILLEN		\N	\N	\N	\N	175 	\N	GUILLEN , Margarita	\N	\N	\N	Jr. Arnaldo Marquez 156	44053707	\N
0000000027	\N	Melanio	TARAZONA		\N	\N	\N	\N	175 	\N	TARAZONA , Melanio	\N	\N	\N	Huanuco	042554272	\N
0000000028	\N	Nsn	NJJ		\N	\N	\N	\N	2   	\N	NJJ , Nsn	\N	\N	\N	jkj		\N
\.


--
-- TOC entry 2604 (class 0 OID 24827)
-- Dependencies: 220
-- Data for Name: ht_seg_persona_hotel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_persona_hotel (pe_id, ho_id) FROM stdin;
0000000001	01
0000000002	01
0000000003	01
0000000004	01
0000000005	01
0000000006	01
0000000007	01
0000000008	01
0000000009	01
0000000010	01
0000000011	01
0000000012	01
0000000013	01
0000000014	01
0000000015	01
0000000016	01
0000000017	01
0000000018	01
0000000019	01
0000000020	01
0000000021	01
0000000022	01
0000000023	01
0000000024	01
0000000025	01
0000000026	01
0000000027	01
0000000028	01
\.


--
-- TOC entry 2605 (class 0 OID 24830)
-- Dependencies: 221
-- Data for Name: ht_seg_ubigeo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_ubigeo (ub_id, ub_desc) FROM stdin;
010101	CHACHAPOYAS
010102	ASUNCION
010103	BALSAS
010104	CHETO
010105	CHILIQUIN
010106	CHUQUIBAMBA
010107	GRANADA
010108	HUANCAS
010109	LA JALCA
010110	LEIMEBAMBA
010111	LEVANTO
010112	MAGDALENA
010113	MARISCAL CASTILLA
010114	MOLINOPAMPA
010115	MONTEVIDEO
010116	OLLEROS
010117	QUINJALCA
010118	SAN FRANCISCO DE DAGUAS
010119	SAN ISIDRO DE MAINO
010120	SOLOCO
010121	SONCHE
010200	BAGUA
010201	BAGUA
010202	ARAMANGO
010203	COPALLIN
010204	EL PARCO
010205	IMAZA
010206	LA PECA
010300	BONGARA
010301	JUMBILLA
010302	CHISQUILLA
010303	CHURUJA
010304	COROSHA
010305	CUISPES
010306	FLORIDA
010307	JAZAN
010308	RECTA
010309	SAN CARLOS
010310	SHIPASBAMBA
010311	VALERA
010312	YAMBRASBAMBA
010400	CONDORCANQUI
010401	NIEVA
010402	EL CENEPA
010403	RIO SANTIAGO
010500	LUYA
010501	LAMUD
010502	CAMPORREDONDO
010503	COCABAMBA
010504	COLCAMAR
010505	CONILA
010506	INGUILPATA
010507	LONGUITA
010508	LONYA CHICO
010509	LUYA
010510	LUYA VIEJO
010511	MARIA
010512	OCALLI
010513	OCUMAL
010514	PISUQUIA
010515	PROVIDENCIA
010516	SAN CRISTOBAL
010517	SAN FRANCISCO DEL YESO
010518	SAN JERONIMO
010519	SAN JUAN DE LOPECANCHA
010520	SANTA CATALINA
010521	SANTO TOMAS
010522	TINGO
010523	TRITA
010600	RODRIGUEZ DE MENDOZA
010601	SAN NICOLAS
010602	CHIRIMOTO
010603	COCHAMAL
010604	HUAMBO
010605	LIMABAMBA
010606	LONGAR
010607	MARISCAL BENAVIDES
010608	MILPUC
010609	OMIA
010610	SANTA ROSA
010611	TOTORA
010612	VISTA ALEGRE
010700	UTCUBAMBA
010701	BAGUA GRANDE
010702	CAJARURO
010703	CUMBA
010704	EL MILAGRO
010705	JAMALCA
010706	LONYA GRANDE
010707	YAMON
020000	ANCASH
020100	HUARAZ
020101	HUARAZ
020102	COCHABAMBA
020103	COLCABAMBA
020104	HUANCHAY
020105	INDEPENDENCIA
020106	JANGAS
020107	LA LIBERTAD
020108	OLLEROS
020109	PAMPAS
020110	PARIACOTO
020111	PIRA
020112	TARICA
020200	AIJA
020201	AIJA
020202	CORIS
020203	HUACLLAN
020204	LA MERCED
020205	SUCCHA
020300	ANTONIO RAYMONDI
020301	LLAMELLIN
020302	ACZO
020303	CHACCHO
020304	CHINGAS
020305	MIRGAS
020306	SAN JUAN DE RONTOY
020400	ASUNCION
020401	CHACAS
020402	ACOCHACA
020500	BOLOGNESI
020501	CHIQUIAN
020502	ABELARDO PARDO LEZAMETA
020503	ANTONIO RAYMONDI
020504	AQUIA
020505	CAJACAY
020506	CANIS
020507	COLQUIOC
020508	HUALLANCA
020509	HUASTA
020510	HUAYLLACAYAN
020511	LA PRIMAVERA
020512	MANGAS
020513	PACLLON
020514	SAN MIGUEL DE CORPANQUI
020515	TICLLOS
020600	CARHUAZ
020601	CARHUAZ
020602	ACOPAMPA
020603	AMASHCA
020604	ANTA
020605	ATAQUERO
020606	MARCARA
020607	PARIAHUANCA
020608	SAN MIGUEL DE ACO
020609	SHILLA
020610	TINCO
020611	YUNGAR
020700	CARLOS FERMIN FITZCARRALD
020701	SAN LUIS
020702	SAN NICOLAS
020703	YAUYA
020800	CASMA
020801	CASMA
020802	BUENA VISTA ALTA
010000	AMAZONAS
010100	CHACHAPOYAS
020803	COMANDANTE NOEL
020804	YAUTAN
020900	CORONGO
020901	CORONGO
020902	ACO
020903	BAMBAS
020904	CUSCA
020905	LA PAMPA
020906	YANAC
020907	YUPAN
021000	HUARI
021001	HUARI
021002	ANRA
021003	CAJAY
021004	CHAVIN DE HUANTAR
021005	HUACACHI
021006	HUACCHIS
021007	HUACHIS
021008	HUANTAR
021009	MASIN
021010	PAUCAS
021011	PONTO
021012	RAHUAPAMPA
021013	RAPAYAN
021014	SAN MARCOS
021015	SAN PEDRO DE CHANA
021016	UCO
021100	HUARMEY
021101	HUARMEY
021102	COCHAPETI
021103	CULEBRAS
021104	HUAYAN
021105	MALVAS
021200	HUAYLAS
021201	CARAZ
021202	HUALLANCA
021203	HUATA
021204	HUAYLAS
021205	MATO
021206	PAMPAROMAS
021207	PUEBLO LIBRE
021208	SANTA CRUZ
021209	SANTO TORIBIO
021210	YURACMARCA
021300	MARISCAL LUZURIAGA
021301	PISCOBAMBA
021302	CASCA
021303	ELEAZAR GUZMAN BARRON
021304	FIDEL OLIVAS ESCUDERO
021305	LLAMA
021306	LLUMPA
021307	LUCMA
021308	MUSGA
021400	OCROS
021401	OCROS
021402	ACAS
021403	CAJAMARQUILLA
021404	CARHUAPAMPA
021405	COCHAS
021406	CONGAS
021407	LLIPA
021408	SAN CRISTOBAL DE RAJAN
021409	SAN PEDRO
021410	SANTIAGO DE CHILCAS
021500	PALLASCA
021501	CABANA
021502	BOLOGNESI
021503	CONCHUCOS
021504	HUACASCHUQUE
021505	HUANDOVAL
021506	LACABAMBA
021507	LLAPO
021508	PALLASCA
021509	PAMPAS
021510	SANTA ROSA
021511	TAUCA
021600	POMABAMBA
021601	POMABAMBA
021602	HUAYLLAN
021603	PAROBAMBA
021604	QUINUABAMBA
021700	RECUAY
021701	RECUAY
021702	CATAC
021703	COTAPARACO
021704	HUAYLLAPAMPA
021705	LLACLLIN
021706	MARCA
021707	PAMPAS CHICO
021708	PARARIN
021709	TAPACOCHA
021710	TICAPAMPA
021800	SANTA
021801	CHIMBOTE
021802	CACERES DEL PERU
021803	COISHCO
021804	MACATE
021805	MORO
021806	NEPEÑA
021807	SAMANCO
021808	SANTA
021809	NUEVO CHIMBOTE
021900	SIHUAS
021901	SIHUAS
021902	ACOBAMBA
021903	ALFONSO UGARTE
021904	CASHAPAMPA
021905	CHINGALPO
021906	HUAYLLABAMBA
021907	QUICHES
021908	RAGASH
021909	SAN JUAN
021910	SICSIBAMBA
022000	YUNGAY
022001	YUNGAY
022002	CASCAPARA
022003	MANCOS
022004	MATACOTO
022005	QUILLO
022006	RANRAHIRCA
022007	SHUPLUY
022008	YANAMA
030000	APURIMAC
030100	ABANCAY
030101	ABANCAY
030102	CHACOCHE
030103	CIRCA
030104	CURAHUASI
030105	HUANIPACA
030106	LAMBRAMA
030107	PICHIRHUA
030108	SAN PEDRO DE CACHORA
030109	TAMBURCO
030200	ANDAHUAYLAS
030201	ANDAHUAYLAS
030202	ANDARAPA
030203	CHIARA
030204	HUANCARAMA
030205	HUANCARAY
030206	HUAYANA
030207	KISHUARA
030208	PACOBAMBA
030209	PACUCHA
030210	PAMPACHIRI
030211	POMACOCHA
030212	SAN ANTONIO DE CACHI
030213	SAN JERONIMO
030214	SAN MIGUEL DE CHACCRAMPA
030215	SANTA MARIA DE CHICMO
030216	TALAVERA
030217	TUMAY HUARACA
030218	TURPO
030219	KAQUIABAMBA
030300	ANTABAMBA
030301	ANTABAMBA
030302	EL ORO
030303	HUAQUIRCA
030304	JUAN ESPINOZA MEDRANO
030305	OROPESA
030306	PACHACONAS
030307	SABAINO
030400	AYMARAES
030401	CHALHUANCA
030402	CAPAYA
030403	CARAYBAMBA
030404	CHAPIMARCA
030405	COLCABAMBA
030406	COTARUSE
030407	HUAYLLO
030408	JUSTO APU SAHUARAURA
030409	LUCRE
030410	POCOHUANCA
030411	SAN JUAN DE CHACÑA
030412	SAÑAYCA
030413	SORAYA
030414	TAPAIRIHUA
030415	TINTAY
030416	TORAYA
030417	YANACA
030500	COTABAMBAS
030501	TAMBOBAMBA
030502	COTABAMBAS
030503	COYLLURQUI
030504	HAQUIRA
030505	MARA
030506	CHALLHUAHUACHO
030600	CHINCHEROS
030601	CHINCHEROS
030602	ANCO_HUALLO
030603	COCHARCAS
030604	HUACCANA
030605	OCOBAMBA
030606	ONGOY
030607	URANMARCA
030608	RANRACANCHA
030700	GRAU
030701	CHUQUIBAMBILLA
030702	CURPAHUASI
030703	GAMARRA
030704	HUAYLLATI
030705	MAMARA
030706	MICAELA BASTIDAS
030707	PATAYPAMPA
030708	PROGRESO
030709	SAN ANTONIO
030710	SANTA ROSA
030711	TURPAY
030712	VILCABAMBA
030713	VIRUNDO
030714	CURASCO
040000	AREQUIPA
040100	AREQUIPA
040101	AREQUIPA
040102	ALTO SELVA ALEGRE
040103	CAYMA
040104	CERRO COLORADO
040105	CHARACATO
040106	CHIGUATA
040107	JACOBO HUNTER
040108	LA JOYA
040109	MARIANO MELGAR
040110	MIRAFLORES
040111	MOLLEBAYA
040112	PAUCARPATA
040113	POCSI
040114	POLOBAYA
040115	QUEQUEÑA
040116	SABANDIA
040117	SACHACA
040118	SAN JUAN DE SIGUAS
040119	SAN JUAN DE TARUCANI
040120	SANTA ISABEL DE SIGUAS
040121	SANTA RITA DE SIGUAS
040122	SOCABAYA
040123	TIABAYA
040124	UCHUMAYO
040125	VITOR
040126	YANAHUARA
040127	YARABAMBA
040128	YURA
040129	JOSE LUIS BUSTAMANTE Y RIVERO
040200	CAMANA
040201	CAMANA
040202	JOSE MARIA QUIMPER
040203	MARIANO NICOLAS VALCARCEL
040204	MARISCAL CACERES
040205	NICOLAS DE PIEROLA
040206	OCOÑA
040207	QUILCA
040208	SAMUEL PASTOR
040300	CARAVELI
040301	CARAVELI
040302	ACARI
040303	ATICO
040304	ATIQUIPA
040305	BELLA UNION
040306	CAHUACHO
040307	CHALA
040308	CHAPARRA
040309	HUANUHUANU
040310	JAQUI
040311	LOMAS
040312	QUICACHA
040313	YAUCA
040400	CASTILLA
040401	APLAO
040402	ANDAGUA
040403	AYO
040404	CHACHAS
040405	CHILCAYMARCA
040406	CHOCO
040407	HUANCARQUI
040408	MACHAGUAY
040409	ORCOPAMPA
040410	PAMPACOLCA
040411	TIPAN
040412	UÑON
040413	URACA
040414	VIRACO
040500	CAYLLOMA
040501	CHIVAY
040502	ACHOMA
040503	CABANACONDE
040504	CALLALLI
040505	CAYLLOMA
040506	COPORAQUE
040507	HUAMBO
040508	HUANCA
040509	ICHUPAMPA
040510	LARI
040511	LLUTA
040512	MACA
040513	MADRIGAL
040514	SAN ANTONIO DE CHUCA
040515	SIBAYO
040516	TAPAY
040517	TISCO
040518	TUTI
040519	YANQUE
040520	MAJES
040600	CONDESUYOS
040601	CHUQUIBAMBA
040602	ANDARAY
040603	CAYARANI
040604	CHICHAS
040605	IRAY
040606	RIO GRANDE
040607	SALAMANCA
040608	YANAQUIHUA
040700	ISLAY
040701	MOLLENDO
040702	COCACHACRA
040703	DEAN VALDIVIA
040704	ISLAY
040705	MEJIA
040706	PUNTA DE BOMBON
040800	LA UNION
040801	COTAHUASI
040802	ALCA
040803	CHARCANA
040804	HUAYNACOTAS
040805	PAMPAMARCA
040806	PUYCA
040807	QUECHUALLA
040808	SAYLA
040809	TAURIA
040810	TOMEPAMPA
040811	TORO
050000	AYACUCHO
050100	HUAMANGA
050101	AYACUCHO
050102	ACOCRO
050103	ACOS VINCHOS
050104	CARMEN ALTO
050105	CHIARA
050106	OCROS
050107	PACAYCASA
050108	QUINUA
050109	SAN JOSE DE TICLLAS
050110	SAN JUAN BAUTISTA
050111	SANTIAGO DE PISCHA
050112	SOCOS
050113	TAMBILLO
050114	VINCHOS
050115	JESUS NAZARENO
050200	CANGALLO
050201	CANGALLO
050202	CHUSCHI
050203	LOS MOROCHUCOS
050204	MARIA PARADO DE BELLIDO
050205	PARAS
050206	TOTOS
050300	HUANCA SANCOS
050301	SANCOS
050302	CARAPO
050303	SACSAMARCA
050304	SANTIAGO DE LUCANAMARCA
050400	HUANTA
050401	HUANTA
050402	AYAHUANCO
050403	HUAMANGUILLA
050404	IGUAIN
050405	LURICOCHA
050406	SANTILLANA
050407	SIVIA
050408	LLOCHEGUA
050500	LA MAR
050501	SAN MIGUEL
050502	ANCO
050503	AYNA
050504	CHILCAS
050505	CHUNGUI
050506	LUIS CARRANZA
050507	SANTA ROSA
050508	TAMBO
050600	LUCANAS
050601	PUQUIO
050602	AUCARA
050603	CABANA
050604	CARMEN SALCEDO
050605	CHAVIÑA
050606	CHIPAO
050607	HUAC-HUAS
050608	LARAMATE
050609	LEONCIO PRADO
050610	LLAUTA
050611	LUCANAS
050612	OCAÑA
050613	OTOCA
050614	SAISA
050615	SAN CRISTOBAL
050616	SAN JUAN
050617	SAN PEDRO
050618	SAN PEDRO DE PALCO
050619	SANCOS
050620	SANTA ANA DE HUAYCAHUACHO
050621	SANTA LUCIA
050700	PARINACOCHAS
050701	CORACORA
050702	CHUMPI
050703	CORONEL CASTAÑEDA
050704	PACAPAUSA
050705	PULLO
050706	PUYUSCA
050707	SAN FRANCISCO DE RAVACAYCO
050708	UPAHUACHO
050800	PAUCAR DEL SARA SARA
050801	PAUSA
050802	COLTA
050803	CORCULLA
050804	LAMPA
050805	MARCABAMBA
050806	OYOLO
050807	PARARCA
050808	SAN JAVIER DE ALPABAMBA
050809	SAN JOSE DE USHUA
050810	SARA SARA
050900	SUCRE
050901	QUEROBAMBA
050902	BELEN
050903	CHALCOS
050904	CHILCAYOC
050905	HUACAÑA
050906	MORCOLLA
050907	PAICO
050908	SAN PEDRO DE LARCAY
050909	SAN SALVADOR DE QUIJE
050910	SANTIAGO DE PAUCARAY
050911	SORAS
051000	VICTOR FAJARDO
051001	HUANCAPI
051002	ALCAMENCA
051003	APONGO
051004	ASQUIPATA
051005	CANARIA
051006	CAYARA
051007	COLCA
051008	HUAMANQUIQUIA
051009	HUANCARAYLLA
051010	HUAYA
051011	SARHUA
051012	VILCANCHOS
051100	VILCAS HUAMAN
051101	VILCAS HUAMAN
051102	ACCOMARCA
051103	CARHUANCA
051104	CONCEPCION
051105	HUAMBALPA
051106	INDEPENDENCIA
051107	SAURAMA
051108	VISCHONGO
060000	CAJAMARCA
060100	CAJAMARCA
060101	CAJAMARCA
060102	ASUNCION
060103	CHETILLA
060104	COSPAN
060105	ENCAÑADA
060106	JESUS
060107	LLACANORA
060108	LOS BAÑOS DEL INCA
060109	MAGDALENA
060110	MATARA
060111	NAMORA
060112	SAN JUAN
060200	CAJABAMBA
060201	CAJABAMBA
060202	CACHACHI
060203	CONDEBAMBA
060204	SITACOCHA
060300	CELENDIN
060301	CELENDIN
060302	CHUMUCH
060303	CORTEGANA
060304	HUASMIN
060305	JORGE CHAVEZ
060306	JOSE GALVEZ
060307	MIGUEL IGLESIAS
060308	OXAMARCA
060309	SOROCHUCO
060310	SUCRE
060311	UTCO
060312	LA LIBERTAD DE PALLAN
060400	CHOTA
060401	CHOTA
060402	ANGUIA
060403	CHADIN
060404	CHIGUIRIP
060405	CHIMBAN
060406	CHOROPAMPA
060407	COCHABAMBA
060408	CONCHAN
060409	HUAMBOS
060410	LAJAS
060411	LLAMA
060412	MIRACOSTA
060413	PACCHA
060414	PION
060415	QUEROCOTO
060416	SAN JUAN DE LICUPIS
060417	TACABAMBA
060418	TOCMOCHE
060419	CHALAMARCA
060500	CONTUMAZA
060501	CONTUMAZA
060502	CHILETE
060503	CUPISNIQUE
060504	GUZMANGO
060505	SAN BENITO
060506	SANTA CRUZ DE TOLED
060507	TANTARICA
060508	YONAN
060600	CUTERVO
060601	CUTERVO
060602	CALLAYUC
060603	CHOROS
060604	CUJILLO
060605	LA RAMADA
060606	PIMPINGOS
060607	QUEROCOTILLO
060608	SAN ANDRES DE CUTERVO
060609	SAN JUAN DE CUTERVO
060610	SAN LUIS DE LUCMA
060611	SANTA CRUZ
060612	SANTO DOMINGO DE LA CAPILLA
060613	SANTO TOMAS
060614	SOCOTA
060615	TORIBIO CASANOVA
060700	HUALGAYOC
060701	BAMBAMARCA
060702	CHUGUR
060703	HUALGAYOC
060800	JAEN
060801	JAEN
060802	BELLAVISTA
060803	CHONTALI
060804	COLASAY
060805	HUABAL
060806	LAS PIRIAS
060807	POMAHUACA
060808	PUCARA
060809	SALLIQUE
060810	SAN FELIPE
060811	SAN JOSE DEL ALTO
060812	SANTA ROSA
060900	SAN IGNACIO
060901	SAN IGNACIO
060902	CHIRINOS
060903	HUARANGO
060904	LA COIPA
060905	NAMBALLE
060906	SAN JOSE DE LOURDES
060907	TABACONAS
061000	SAN MARCOS
061001	PEDRO GALVEZ
061002	CHANCAY
061003	EDUARDO VILLANUEVA
061004	GREGORIO PITA
061005	ICHOCAN
061006	JOSE MANUEL QUIROZ
061007	JOSE SABOGAL
061100	SAN MIGUEL
061101	SAN MIGUEL
061102	BOLIVAR
061103	CALQUIS
061104	CATILLUC
061105	EL PRADO
061106	LA FLORIDA
061107	LLAPA
061108	NANCHOC
061109	NIEPOS
061110	SAN GREGORIO
061111	SAN SILVESTRE DE COCHAN
061112	TONGOD
061113	UNION AGUA BLANCA
061200	SAN PABLO
061201	SAN PABLO
061202	SAN BERNARDINO
061203	SAN LUIS
061204	TUMBADEN
061300	SANTA CRUZ
061301	SANTA CRUZ
061302	ANDABAMBA
061303	CATACHE
061304	CHANCAYBAÑOS
061305	LA ESPERANZA
061306	NINABAMBA
061307	PULAN
061308	SAUCEPAMPA
061309	SEXI
061310	UTICYACU
061311	YAUYUCAN
070000	CALLAO
070100	CALLAO
070101	CALLAO
070102	BELLAVISTA
070103	CARMEN DE LA LEGUA REYNOSO
070104	LA PERLA
070105	LA PUNTA
070106	VENTANILLA
080000	CUSCO
080100	CUSCO
080101	CUSCO
080102	CCORCA
080103	POROY
080104	SAN JERONIMO
080105	SAN SEBASTIAN
080106	SANTIAGO
080107	SAYLLA
080108	WANCHAQ
080200	ACOMAYO
080201	ACOMAYO
080202	ACOPIA
080203	ACOS
080204	MOSOC LLACTA
080205	POMACANCHI
080206	RONDOCAN
080207	SANGARARA
080300	ANTA
080301	ANTA
080302	ANCAHUASI
080303	CACHIMAYO
080304	CHINCHAYPUJIO
080305	HUAROCONDO
080306	LIMATAMBO
080307	MOLLEPATA
080308	PUCYURA
080309	ZURITE
080400	CALCA
080401	CALCA
080402	COYA
080403	LAMAY
080404	LARES
080405	PISAC
080406	SAN SALVADOR
080407	TARAY
080408	YANATILE
080500	CANAS
080501	YANAOCA
080502	CHECCA
080503	KUNTURKANKI
080504	LANGUI
080505	LAYO
080506	PAMPAMARCA
080507	QUEHUE
080508	TUPAC AMARU
080600	CANCHIS
080601	SICUANI
080602	CHECACUPE
080603	COMBAPATA
080604	MARANGANI
080605	PITUMARCA
080606	SAN PABLO
080607	SAN PEDRO
080608	TINTA
080700	CHUMBIVILCAS
080701	SANTO TOMAS
080702	CAPACMARCA
080703	CHAMACA
080704	COLQUEMARCA
080705	LIVITACA
080706	LLUSCO
080707	QUIÑOTA
080708	VELILLE
080800	ESPINAR
080801	ESPINAR
080802	CONDOROMA
080803	COPORAQUE
080804	OCORURO
080805	PALLPATA
080806	PICHIGUA
080807	SUYCKUTAMBO
080808	ALTO PICHIGUA
080900	LA CONVENCION
080901	SANTA ANA
080902	ECHARATE
080903	HUAYOPATA
080904	MARANURA
080905	OCOBAMBA
080906	QUELLOUNO
080907	KIMBIRI
080908	SANTA TERESA
080909	VILCABAMBA
080910	PICHARI
081000	PARURO
081001	PARURO
081002	ACCHA
081003	CCAPI
081004	COLCHA
081005	HUANOQUITE
081006	OMACHA
081007	PACCARITAMBO
081008	PILLPINTO
081009	YAURISQUE
081100	PAUCARTAMBO
081101	PAUCARTAMBO
081102	CAICAY
081103	CHALLABAMBA
081104	COLQUEPATA
081105	HUANCARANI
081106	KOSÑIPATA
081200	QUISPICANCHI
081201	URCOS
081202	ANDAHUAYLILLAS
081203	CAMANTI
081204	CCARHUAYO
081205	CCATCA
081206	CUSIPATA
081207	HUARO
081208	LUCRE
081209	MARCAPATA
081210	OCONGATE
081211	OROPESA
081212	QUIQUIJANA
081300	URUBAMBA
081301	URUBAMBA
081302	CHINCHERO
081303	HUAYLLABAMBA
081304	MACHUPICCHU
081305	MARAS
081306	OLLANTAYTAMBO
081307	YUCAY
090000	HUANCAVELICA
090100	HUANCAVELICA
090101	HUANCAVELICA
090102	ACOBAMBILLA
090103	ACORIA
090104	CONAYCA
090105	CUENCA
090106	HUACHOCOLPA
090107	HUAYLLAHUARA
090108	IZCUCHACA
090109	LARIA
090110	MANTA
090111	MARISCAL CACERES
090112	MOYA
090113	NUEVO OCCORO
090114	PALCA
090115	PILCHACA
090116	VILCA
090117	YAULI
090118	ASCENSION
090119	HUANDO
090200	ACOBAMBA
090201	ACOBAMBA
090202	ANDABAMBA
090203	ANTA
090204	CAJA
090205	MARCAS
090206	PAUCARA
090207	POMACOCHA
090208	ROSARIO
090300	ANGARAES
090301	LIRCAY
090302	ANCHONGA
090303	CALLANMARCA
090304	CCOCHACCASA
090305	CHINCHO
090306	CONGALLA
090307	HUANCA-HUANCA
090308	HUAYLLAY GRANDE
090309	JULCAMARCA
090310	SAN ANTONIO DE ANTAPARCO
090311	SANTO TOMAS DE PATA
090312	SECCLLA
090400	CASTROVIRREYNA
090401	CASTROVIRREYNA
090402	ARMA
090403	AURAHUA
090404	CAPILLAS
090405	CHUPAMARCA
090406	COCAS
090407	HUACHOS
090408	HUAMATAMBO
090409	MOLLEPAMPA
090410	SAN JUAN
090411	SANTA ANA
090412	TANTARA
090413	TICRAPO
090500	CHURCAMPA
090501	CHURCAMPA
090502	ANCO
090503	CHINCHIHUASI
090504	EL CARMEN
090505	LA MERCED
090506	LOCROJA
090507	PAUCARBAMBA
090508	SAN MIGUEL DE MAYOCC
090509	SAN PEDRO DE CORIS
090510	PACHAMARCA
090600	HUAYTARA
090601	HUAYTARA
090602	AYAVI
090603	CORDOVA
090604	HUAYACUNDO ARMA
090605	LARAMARCA
090606	OCOYO
090607	PILPICHACA
090608	QUERCO
090609	QUITO-ARMA
090610	SAN ANTONIO DE CUSICANCHA
090611	SAN FRANCISCO DE SANGAYAICO
090612	SAN ISIDRO
090613	SANTIAGO DE CHOCORVOS
090614	SANTIAGO DE QUIRAHUARA
090615	SANTO DOMINGO DE CAPILLAS
090616	TAMBO
090700	TAYACAJA
090701	PAMPAS
090702	ACOSTAMBO
090703	ACRAQUIA
090704	AHUAYCHA
090705	COLCABAMBA
090706	DANIEL HERNANDEZ
090707	HUACHOCOLPA
090709	HUARIBAMBA
090710	ÑAHUIMPUQUIO
090711	PAZOS
090713	QUISHUAR
090714	SALCABAMBA
090715	SALCAHUASI
090716	SAN MARCOS DE ROCCHAC
090717	SURCUBAMBA
090718	TINTAY PUNCU
100000	HUANUCO
100100	HUANUCO
100101	HUANUCO
100102	AMARILIS
100103	CHINCHAO
100104	CHURUBAMBA
100105	MARGOS
100106	QUISQUI
100107	SAN FRANCISCO DE CAYRAN
100108	SAN PEDRO DE CHAULAN
100109	SANTA MARIA DEL VALLE
100110	YARUMAYO
100111	PILLCO MARCA
100200	AMBO
100201	AMBO
100202	CAYNA
100203	COLPAS
100204	CONCHAMARCA
100205	HUACAR
100206	SAN FRANCISCO
100207	SAN RAFAEL
100208	TOMAY KICHWA
100300	DOS DE MAYO
100301	LA UNION
100307	CHUQUIS
100311	MARIAS
100313	PACHAS
100316	QUIVILLA
100317	RIPAN
100321	SHUNQUI
100322	SILLAPATA
100323	YANAS
100400	HUACAYBAMBA
100401	HUACAYBAMBA
100402	CANCHABAMBA
100403	COCHABAMBA
100404	PINRA
100500	HUAMALIES
100501	LLATA
100502	ARANCAY
100503	CHAVIN DE PARIARCA
100504	JACAS GRANDE
100505	JIRCAN
100506	MIRAFLORES
100507	MONZON
100508	PUNCHAO
100509	PUÑOS
100510	SINGA
100511	TANTAMAYO
100600	LEONCIO PRADO
100601	RUPA-RUPA
100602	DANIEL ALOMIAS ROBLES
100603	HERMILIO VALDIZAN
100604	JOSE CRESPO Y CASTILLO
100605	LUYANDO
100606	MARIANO DAMASO BERAUN
100700	MARAÑON
100701	HUACRACHUCO
100702	CHOLON
100703	SAN BUENAVENTURA
100800	PACHITEA
100801	PANAO
100802	CHAGLLA
100803	MOLINO
100804	UMARI
100900	PUERTO INCA
100901	PUERTO INCA
100902	CODO DEL POZUZO
100903	HONORIA
100904	TOURNAVISTA
100905	YUYAPICHIS
101000	LAURICOCHA
101001	JESUS
101002	BAÑOS
101003	JIVIA
101004	QUEROPALCA
101005	RONDOS
101006	SAN FRANCISCO DE ASIS
101007	SAN MIGUEL DE CAURI
101100	YAROWILCA
101101	CHAVINILLO
101102	CAHUAC
101103	CHACABAMBA
101104	APARICIO POMARES
101105	JACAS CHICO
101106	OBAS
101107	PAMPAMARCA
101108	CHORAS
110000	ICA
110100	ICA
110101	ICA
110102	LA TINGUIÑA
110103	LOS AQUIJES
110104	OCUCAJE
110105	PACHACUTEC
110106	PARCONA
110107	PUEBLO NUEVO
110108	SALAS
110109	SAN JOSE DE LOS MOLINOS
110110	SAN JUAN BAUTISTA
110111	SANTIAGO
110112	SUBTANJALLA
110113	TATE
110114	YAUCA DEL ROSARIO
110200	CHINCHA
110201	CHINCHA ALTA
110202	ALTO LARAN
110203	CHAVIN
110204	CHINCHA BAJA
110205	EL CARMEN
110206	GROCIO PRADO
110207	PUEBLO NUEVO
110208	SAN JUAN DE YANAC
110209	SAN PEDRO DE HUACARPANA
110210	SUNAMPE
110211	TAMBO DE MORA
110300	NAZCA
110301	NAZCA
110302	CHANGUILLO
110303	EL INGENIO
110304	MARCONA
110305	VISTA ALEGRE
110400	PALPA
110401	PALPA
110402	LLIPATA
110403	RIO GRANDE
110404	SANTA CRUZ
110405	TIBILLO
110500	PISCO
110501	PISCO
110502	HUANCANO
110503	HUMAY
110504	INDEPENDENCIA
110505	PARACAS
110506	SAN ANDRES
110507	SAN CLEMENTE
110508	TUPAC AMARU INCA
120000	JUNIN
120100	HUANCAYO
120101	HUANCAYO
120104	CARHUACALLANGA
120105	CHACAPAMPA
120106	CHICCHE
120107	CHILCA
120108	CHONGOS ALTO
120111	CHUPURO
120112	COLCA
120113	CULLHUAS
120114	EL TAMBO
120116	HUACRAPUQUIO
120117	HUALHUAS
120119	HUANCAN
120120	HUASICANCHA
120121	HUAYUCACHI
120122	INGENIO
120124	PARIAHUANCA
120125	PILCOMAYO
120126	PUCARA
120127	QUICHUAY
120128	QUILCAS
120129	SAN AGUSTIN
120130	SAN JERONIMO DE TUNAN
120132	SAÑO
120133	SAPALLANGA
120134	SICAYA
120135	SANTO DOMINGO DE ACOBAMBA
120136	VIQUES
120200	CONCEPCION
120201	CONCEPCION
120202	ACO
120203	ANDAMARCA
120204	CHAMBARA
120205	COCHAS
120206	COMAS
120207	HEROINAS TOLEDO
120208	MANZANARES
120209	MARISCAL CASTILLA
120210	MATAHUASI
120211	MITO
120212	NUEVE DE JULIO
120213	ORCOTUNA
120214	SAN JOSE DE QUERO
120215	SANTA ROSA DE OCOPA
120300	CHANCHAMAYO
120301	CHANCHAMAYO
120302	PERENE
120303	PICHANAQUI
120304	SAN LUIS DE SHUARO
120305	SAN RAMON
120306	VITOC
120400	JAUJA
120401	JAUJA
120402	ACOLLA
120403	APATA
120404	ATAURA
120405	CANCHAYLLO
120406	CURICACA
120407	EL MANTARO
120408	HUAMALI
120409	HUARIPAMPA
120410	HUERTAS
120411	JANJAILLO
120412	JULCAN
120413	LEONOR ORDOÑEZ
120414	LLOCLLAPAMPA
120415	MARCO
120416	MASMA
120417	MASMA CHICCHE
120418	MOLINOS
120419	MONOBAMBA
120420	MUQUI
120421	MUQUIYAUYO
120422	PACA
120423	PACCHA
120424	PANCAN
120425	PARCO
120426	POMACANCHA
120427	RICRAN
120428	SAN LORENZO
120429	SAN PEDRO DE CHUNAN
120430	SAUSA
120431	SINCOS
120432	TUNAN MARCA
120433	YAULI
120434	YAUYOS
120500	JUNIN
120501	JUNIN
120502	CARHUAMAYO
120503	ONDORES
120504	ULCUMAYO
120600	SATIPO
120601	SATIPO
120602	COVIRIALI
120603	LLAYLLA
120604	MAZAMARI
120605	PAMPA HERMOSA
120606	PANGOA
120607	RIO NEGRO
120608	RIO TAMBO
120700	TARMA
120701	TARMA
120702	ACOBAMBA
120703	HUARICOLCA
120704	HUASAHUASI
120705	LA UNION
120706	PALCA
120707	PALCAMAYO
120708	SAN PEDRO DE CAJAS
120709	TAPO
120800	YAULI
120801	LA OROYA
120802	CHACAPALPA
120803	HUAY-HUAY
120804	MARCAPOMACOCHA
120805	MOROCOCHA
120806	PACCHA
120807	SANTA BARBARA DE CARHUACAYAN
120808	SANTA ROSA DE SACCO
120809	SUITUCANCHA
120810	YAULI
120900	CHUPACA
120901	CHUPACA
120902	AHUAC
120903	CHONGOS BAJO
120904	HUACHAC
120905	HUAMANCACA CHICO
120906	SAN JUAN DE YSCOS
120907	SAN JUAN DE JARPA
120908	TRES DE DICIEMBRE
120909	YANACANCHA
130000	LA LIBERTAD
130100	TRUJILLO
130101	TRUJILLO
130102	EL PORVENIR
130103	FLORENCIA DE MORA
130104	HUANCHACO
130105	LA ESPERANZA
130106	LAREDO
130107	MOCHE
130108	POROTO
130109	SALAVERRY
130110	SIMBAL
130111	VICTOR LARCO HERRERA
130200	ASCOPE
130201	ASCOPE
130202	CHICAMA
130203	CHOCOPE
130204	MAGDALENA DE CAO
130205	PAIJAN
130206	RAZURI
130207	SANTIAGO DE CAO
130208	CASA GRANDE
130300	BOLIVAR
130301	BOLIVAR
130302	BAMBAMARCA
130303	CONDORMARCA
130304	LONGOTEA
130305	UCHUMARCA
130306	UCUNCHA
130400	CHEPEN
130401	CHEPEN
130402	PACANGA
130403	PUEBLO NUEVO
130500	JULCAN
130501	JULCAN
130502	CALAMARCA
130503	CARABAMBA
130504	HUASO
130600	OTUZCO
130601	OTUZCO
130602	AGALLPAMPA
130604	CHARAT
130605	HUARANCHAL
130606	LA CUESTA
130608	MACHE
130610	PARANDAY
130611	SALPO
130613	SINSICAP
130614	USQUIL
130700	PACASMAYO
130701	SAN PEDRO DE LLOC
130702	GUADALUPE
130703	JEQUETEPEQUE
130704	PACASMAYO
130705	SAN JOSE
130800	PATAZ
130801	TAYABAMBA
130802	BULDIBUYO
130803	CHILLIA
130804	HUANCASPATA
130805	HUAYLILLAS
130806	HUAYO
130807	ONGON
130808	PARCOY
130809	PATAZ
130810	PIAS
130811	SANTIAGO DE CHALLAS
130812	TAURIJA
130813	URPAY
130900	SANCHEZ CARRION
130901	HUAMACHUCO
130902	CHUGAY
130903	COCHORCO
130904	CURGOS
130905	MARCABAL
130906	SANAGORAN
130907	SARIN
130908	SARTIMBAMBA
131000	SANTIAGO DE CHUCO
131001	SANTIAGO DE CHUCO
131002	ANGASMARCA
131003	CACHICADAN
131004	MOLLEBAMBA
131005	MOLLEPATA
131006	QUIRUVILCA
131007	SANTA CRUZ DE CHUCA
131008	SITABAMBA
131100	GRAN CHIMU
131101	CASCAS
131102	LUCMA
131103	COMPIN
131104	SAYAPULLO
131200	VIRU
131201	VIRU
131202	CHAO
131203	GUADALUPITO
140000	LAMBAYEQUE
140100	CHICLAYO
140101	CHICLAYO
140102	CHONGOYAPE
140103	ETEN
140104	ETEN PUERTO
140105	JOSE LEONARDO ORTIZ
140106	LA VICTORIA
140107	LAGUNAS
140108	MONSEFU
140109	NUEVA ARICA
140110	OYOTUN
140111	PICSI
140112	PIMENTEL
140113	REQUE
140114	SANTA ROSA
140115	SAÑA
140116	CAYALTI
140117	PATAPO
140118	POMALCA
140119	PUCALA
140120	TUMAN
140200	FERREÑAFE
140201	FERREÑAFE
140202	CAÑARIS
140203	INCAHUASI
140204	MANUEL ANTONIO MESONES MURO
140205	PITIPO
140206	PUEBLO NUEVO
140300	LAMBAYEQUE
140301	LAMBAYEQUE
140302	CHOCHOPE
140303	ILLIMO
140304	JAYANCA
140305	MOCHUMI
140306	MORROPE
140307	MOTUPE
140308	OLMOS
140309	PACORA
140310	SALAS
140311	SAN JOSE
140312	TUCUME
150102	ANCON
150103	ATE
150104	BARRANCO
150105	BREÑA
150106	CARABAYLLO
150107	CHACLACAYO
150108	CHORRILLOS
150109	CIENEGUILLA
150110	COMAS
150111	EL AGUSTINO
150112	INDEPENDENCIA
150113	JESUS MARIA
150114	LA MOLINA
150115	LA VICTORIA
150116	LINCE
150117	LOS OLIVOS
150118	LURIGANCHO
150119	LURIN
150120	MAGDALENA DEL MAR
150121	MAGDALENA VIEJA
150122	MIRAFLORES
150123	PACHACAMAC
150124	PUCUSANA
150125	PUENTE PIEDRA
150126	PUNTA HERMOSA
150127	PUNTA NEGRA
150128	RIMAC
150129	SAN BARTOLO
150130	SAN BORJA
150131	SAN ISIDRO
150132	SAN JUAN DE LURIGANCHO
150133	SAN JUAN DE MIRAFLORES
150134	SAN LUIS
150135	SAN MARTIN DE PORRES
150136	SAN MIGUEL
150137	SANTA ANITA
150138	SANTA MARIA DEL MAR
150139	SANTA ROSA
150140	SANTIAGO DE SURCO
150141	SURQUILLO
150142	VILLA EL SALVADOR
150143	VILLA MARIA DEL TRIUNFO
150200	BARRANCA
150201	BARRANCA
150202	PARAMONGA
150203	PATIVILCA
150204	SUPE
150205	SUPE PUERTO
150300	CAJATAMBO
150301	CAJATAMBO
150302	COPA
150303	GORGOR
150304	HUANCAPON
150305	MANAS
150400	CANTA
150401	CANTA
150402	ARAHUAY
150403	HUAMANTANGA
150404	HUAROS
150405	LACHAQUI
150406	SAN BUENAVENTURA
150407	SANTA ROSA DE QUIVES
150500	CAÑETE
150501	SAN VICENTE DE CAÑETE
150502	ASIA
150503	CALANGO
150504	CERRO AZUL
150505	CHILCA
150506	COAYLLO
150507	IMPERIAL
150508	LUNAHUANA
150509	MALA
150510	NUEVO IMPERIAL
150511	PACARAN
150512	QUILMANA
150513	SAN ANTONIO
150514	SAN LUIS
150515	SANTA CRUZ DE FLORES
150516	ZUÑIGA
150600	HUARAL
150601	HUARAL
150602	ATAVILLOS ALTO
150603	ATAVILLOS BAJO
150604	AUCALLAMA
150605	CHANCAY
150606	IHUARI
150607	LAMPIAN
150608	PACARAOS
150609	SAN MIGUEL DE ACOS
150610	SANTA CRUZ DE ANDAMARCA
150611	SUMBILCA
150612	VEINTISIETE DE NOVIEMBRE
150700	HUAROCHIRI
150701	MATUCANA
150702	ANTIOQUIA
150703	CALLAHUANCA
150704	CARAMPOMA
150705	CHICLA
150706	CUENCA
150707	HUACHUPAMPA
150708	HUANZA
150709	HUAROCHIRI
150710	LAHUAYTAMBO
150711	LANGA
150712	LARAOS
150713	MARIATANA
150714	RICARDO PALMA
150715	SAN ANDRES DE TUPICOCHA
150716	SAN ANTONIO
150717	SAN BARTOLOME
150718	SAN DAMIAN
150719	SAN JUAN DE IRIS
150720	SAN JUAN DE TANTARANCHE
150721	SAN LORENZO DE QUINTI
150722	SAN MATEO
150723	SAN MATEO DE OTAO
150724	SAN PEDRO DE CASTA
150725	SAN PEDRO DE HUANCAYRE
150726	SANGALLAYA
150727	SANTA CRUZ DE COCACHACRA
150728	SANTA EULALIA
150729	SANTIAGO DE ANCHUCAYA
150730	SANTIAGO DE TUNA
150731	SANTO DOMINGO DE LOS OLLEROS
150732	SURCO
150800	HUAURA
150801	HUACHO
150802	AMBAR
150803	CALETA DE CARQUIN
150804	CHECRAS
150805	HUALMAY
150806	HUAURA
150807	LEONCIO PRADO
150808	PACCHO
150809	SANTA LEONOR
150810	SANTA MARIA
150811	SAYAN
150812	VEGUETA
150900	OYON
150901	OYON
150902	ANDAJES
150903	CAUJUL
150904	COCHAMARCA
150905	NAVAN
150906	PACHANGARA
151000	YAUYOS
151001	YAUYOS
151002	ALIS
151003	AYAUCA
151004	AYAVIRI
151005	AZANGARO
151006	CACRA
151007	CARANIA
151008	CATAHUASI
151009	CHOCOS
151010	COCHAS
151011	COLONIA
151012	HONGOS
151013	HUAMPARA
151014	HUANCAYA
151015	HUANGASCAR
151016	HUANTAN
151017	HUAÑEC
151018	LARAOS
151019	LINCHA
151020	MADEAN
151021	MIRAFLORES
151022	OMAS
151023	PUTINZA
151024	QUINCHES
151025	QUINOCAY
151026	SAN JOAQUIN
151027	SAN PEDRO DE PILAS
151028	TANTA
151029	TAURIPAMPA
151030	TOMAS
151031	TUPE
151032	VIÑAC
151033	VITIS
160000	LORETO
160100	MAYNAS
160101	IQUITOS
160102	ALTO NANAY
160103	FERNANDO LORES
160104	INDIANA
160105	LAS AMAZONAS
160106	MAZAN
160107	NAPO
160108	PUNCHANA
160109	PUTUMAYO
160110	TORRES CAUSANA
160112	BELEN
160113	SAN JUAN BAUTISTA
160114	TENIENTE MANUEL CLAVERO
160200	ALTO AMAZONAS
160201	YURIMAGUAS
160202	BALSAPUERTO
160205	JEBEROS
160206	LAGUNAS
160210	SANTA CRUZ
160211	TENIENTE CESAR LOPEZ ROJAS
160300	LORETO
160301	NAUTA
160302	PARINARI
160303	TIGRE
160304	TROMPETEROS
160305	URARINAS
160400	MARISCAL RAMON CASTILLA
160401	RAMON CASTILLA
160402	PEBAS
160403	YAVARI
160404	SAN PABLO
160500	REQUENA
160501	REQUENA
160502	ALTO TAPICHE
160503	CAPELO
160504	EMILIO SAN MARTIN
160505	MAQUIA
160506	PUINAHUA
160507	SAQUENA
160508	SOPLIN
160509	TAPICHE
160510	JENARO HERRERA
160511	YAQUERANA
160600	UCAYALI
160601	CONTAMANA
160602	INAHUAYA
160603	PADRE MARQUEZ
160604	PAMPA HERMOSA
160605	SARAYACU
160606	VARGAS GUERRA
160700	DATEM DEL MARAÑON
160701	BARRANCA
160702	CAHUAPANAS
160703	MANSERICHE
160704	MORONA
160705	PASTAZA
160706	ANDOAS
170000	MADRE DE DIOS
170100	TAMBOPATA
170101	TAMBOPATA
170102	INAMBARI
170103	LAS PIEDRAS
170104	LABERINTO
170200	MANU
170201	MANU
170202	FITZCARRALD
170203	MADRE DE DIOS
170204	HUEPETUHE
170300	TAHUAMANU
170301	IÑAPARI
170302	IBERIA
170303	TAHUAMANU
180000	MOQUEGUA
180100	MARISCAL NIETO
180101	MOQUEGUA
180102	CARUMAS
180103	CUCHUMBAYA
180104	SAMEGUA
180105	SAN CRISTOBAL
180106	TORATA
180200	GENERAL SANCHEZ CERRO
180201	OMATE
180202	CHOJATA
180203	COALAQUE
180204	ICHUÑA
180205	LA CAPILLA
180206	LLOQUE
180207	MATALAQUE
180208	PUQUINA
180209	QUINISTAQUILLAS
180210	UBINAS
180211	YUNGA
180300	ILO
180301	ILO
180302	EL ALGARROBAL
180303	PACOCHA
190000	PASCO
190100	PASCO
190101	CHAUPIMARCA
190102	HUACHON
190103	HUARIACA
190104	HUAYLLAY
190105	NINACACA
190106	PALLANCHACRA
190107	PAUCARTAMBO
190108	SAN FRANCISCO DE ASIS DE YARUSYACAN
190109	SIMON BOLIVAR
190110	TICLACAYAN
190111	TINYAHUARCO
190112	VICCO
190113	YANACANCHA
190200	DANIEL ALCIDES CARRION
190201	YANAHUANCA
190202	CHACAYAN
190203	GOYLLARISQUIZGA
190204	PAUCAR
190205	SAN PEDRO DE PILLAO
190206	SANTA ANA DE TUSI
190207	TAPUC
190208	VILCABAMBA
190300	OXAPAMPA
190301	OXAPAMPA
190302	CHONTABAMBA
190303	HUANCABAMBA
190304	PALCAZU
190305	POZUZO
190306	PUERTO BERMUDEZ
190307	VILLA RICA
200000	PIURA
200100	PIURA
200101	PIURA
200104	CASTILLA
200105	CATACAOS
200107	CURA MORI
200108	EL TALLAN
200109	LA ARENA
200110	LA UNION
200111	LAS LOMAS
200114	TAMBO GRANDE
200200	AYABACA
200201	AYABACA
200202	FRIAS
200203	JILILI
200204	LAGUNAS
200205	MONTERO
200206	PACAIPAMPA
200207	PAIMAS
200208	SAPILLICA
200209	SICCHEZ
200210	SUYO
200300	HUANCABAMBA
200301	HUANCABAMBA
200302	CANCHAQUE
200303	EL CARMEN DE LA FRONTERA
200304	HUARMACA
200305	LALAQUIZ
200306	SAN MIGUEL DE EL FAIQUE
200307	SONDOR
200308	SONDORILLO
200400	MORROPON
200401	CHULUCANAS
200402	BUENOS AIRES
200403	CHALACO
200404	LA MATANZA
200405	MORROPON
200406	SALITRAL
200407	SAN JUAN DE BIGOTE
200408	SANTA CATALINA DE MOSSA
200409	SANTO DOMINGO
200410	YAMANGO
200500	PAITA
200501	PAITA
200502	AMOTAPE
200503	ARENAL
200504	COLAN
200505	LA HUACA
200506	TAMARINDO
200507	VICHAYAL
200600	SULLANA
200601	SULLANA
200602	BELLAVISTA
200603	IGNACIO ESCUDERO
200604	LANCONES
200605	MARCAVELICA
200606	MIGUEL CHECA
200607	QUERECOTILLO
200608	SALITRAL
200700	TALARA
200701	PARIÑAS
200702	EL ALTO
200703	LA BREA
200704	LOBITOS
200705	LOS ORGANOS
200706	MANCORA
200800	SECHURA
200801	SECHURA
200802	BELLAVISTA DE LA UNION
200803	BERNAL
200804	CRISTO NOS VALGA
200805	VICE
200806	RINCONADA LLICUAR
210000	PUNO
210100	PUNO
210101	PUNO
210102	ACORA
210103	AMANTANI
210104	ATUNCOLLA
210105	CAPACHICA
210106	CHUCUITO
210107	COATA
210108	HUATA
210109	MAÑAZO
210110	PAUCARCOLLA
210111	PICHACANI
210112	PLATERIA
210113	SAN ANTONIO
210114	TIQUILLACA
210115	VILQUE
210200	AZANGARO
210201	AZANGARO
210202	ACHAYA
210203	ARAPA
210204	ASILLO
210205	CAMINACA
210206	CHUPA
210207	JOSE DOMINGO CHOQUEHUANCA
210208	MUÑANI
210209	POTONI
210210	SAMAN
210211	SAN ANTON
210212	SAN JOSE
210213	SAN JUAN DE SALINAS
210214	SANTIAGO DE PUPUJA
210215	TIRAPATA
210300	CARABAYA
210301	MACUSANI
210302	AJOYANI
210303	AYAPATA
210304	COASA
210305	CORANI
210306	CRUCERO
210307	ITUATA
210308	OLLACHEA
210309	SAN GABAN
210310	USICAYOS
210400	CHUCUITO
210401	JULI
210402	DESAGUADERO
210403	HUACULLANI
210404	KELLUYO
210405	PISACOMA
210406	POMATA
210407	ZEPITA
210500	EL COLLAO
210501	ILAVE
210502	CAPAZO
210503	PILCUYO
210504	SANTA ROSA
210505	CONDURIRI
210600	HUANCANE
210601	HUANCANE
210602	COJATA
210603	HUATASANI
210604	INCHUPALLA
210605	PUSI
210606	ROSASPATA
210607	TARACO
210608	VILQUE CHICO
210700	LAMPA
210701	LAMPA
210702	CABANILLA
210703	CALAPUJA
210704	NICASIO
210705	OCUVIRI
210706	PALCA
210707	PARATIA
210708	PUCARA
210709	SANTA LUCIA
210710	VILAVILA
210800	MELGAR
210801	AYAVIRI
210802	ANTAUTA
210803	CUPI
210804	LLALLI
210805	MACARI
210806	NUÑOA
210807	ORURILLO
210808	SANTA ROSA
210809	UMACHIRI
210900	MOHO
210901	MOHO
210902	CONIMA
210903	HUAYRAPATA
210904	TILALI
211000	SAN ANTONIO DE PUTINA
211001	PUTINA
211002	ANANEA
211003	PEDRO VILCA APAZA
211004	QUILCAPUNCU
211005	SINA
211100	SAN ROMAN
211101	JULIACA
211102	CABANA
211103	CABANILLAS
211104	CARACOTO
211200	SANDIA
211201	SANDIA
211202	CUYOCUYO
211203	LIMBANI
211204	PATAMBUCO
211205	PHARA
211206	QUIACA
211207	SAN JUAN DEL ORO
211208	YANAHUAYA
211209	ALTO INAMBARI
211210	SAN PEDRO DE PUTINA PUNCO
211300	YUNGUYO
211301	YUNGUYO
211302	ANAPIA
211303	COPANI
211304	CUTURAPI
211305	OLLARAYA
211306	TINICACHI
211307	UNICACHI
220000	SAN MARTIN
220100	MOYOBAMBA
220101	MOYOBAMBA
220102	CALZADA
220103	HABANA
220104	JEPELACIO
220105	SORITOR
220106	YANTALO
220200	BELLAVISTA
220201	BELLAVISTA
220202	ALTO BIAVO
220203	BAJO BIAVO
220204	HUALLAGA
220205	SAN PABLO
220206	SAN RAFAEL
220300	EL DORADO
220301	SAN JOSE DE SISA
220302	AGUA BLANCA
220303	SAN MARTIN
220304	SANTA ROSA
220305	SHATOJA
220400	HUALLAGA
220401	SAPOSOA
220402	ALTO SAPOSOA
220403	EL ESLABON
220404	PISCOYACU
220405	SACANCHE
220406	TINGO DE SAPOSOA
220500	LAMAS
220501	LAMAS
220502	ALONSO DE ALVARADO
220503	BARRANQUITA
220504	CAYNARACHI
220505	CUÑUMBUQUI
220506	PINTO RECODO
220507	RUMISAPA
220508	SAN ROQUE DE CUMBAZA
220509	SHANAO
220510	TABALOSOS
220511	ZAPATERO
220600	MARISCAL CACERES
220601	JUANJUI
220602	CAMPANILLA
220603	HUICUNGO
220604	PACHIZA
220605	PAJARILLO
220700	PICOTA
220701	PICOTA
220702	BUENOS AIRES
220703	CASPISAPA
220704	PILLUANA
220705	PUCACACA
220706	SAN CRISTOBAL
220707	SAN HILARION
220708	SHAMBOYACU
220709	TINGO DE PONASA
220710	TRES UNIDOS
220800	RIOJA
220801	RIOJA
220802	AWAJUN
220803	ELIAS SOPLIN VARGAS
220804	NUEVA CAJAMARCA
220805	PARDO MIGUEL
220806	POSIC
220807	SAN FERNANDO
220808	YORONGOS
220809	YURACYACU
220900	SAN MARTIN
220901	TARAPOTO
220902	ALBERTO LEVEAU
220903	CACATACHI
220904	CHAZUTA
220905	CHIPURANA
220906	EL PORVENIR
220907	HUIMBAYOC
220908	JUAN GUERRA
220909	LA BANDA DE SHILCAYO
220910	MORALES
220911	PAPAPLAYA
220912	SAN ANTONIO
220913	SAUCE
220914	SHAPAJA
221000	TOCACHE
221001	TOCACHE
221002	NUEVO PROGRESO
221003	POLVORA
221004	SHUNTE
221005	UCHIZA
230000	TACNA
230100	TACNA
230101	TACNA
230102	ALTO DE LA ALIANZA
230103	CALANA
230104	CIUDAD NUEVA
230105	INCLAN
230106	PACHIA
230107	PALCA
230108	POCOLLAY
230109	SAMA
230110	CORONEL GREGORIO ALBARRACIN LANCHIPA
230200	CANDARAVE
230201	CANDARAVE
230202	CAIRANI
230203	CAMILACA
230204	CURIBAYA
230205	HUANUARA
230206	QUILAHUANI
230300	JORGE BASADRE
230301	LOCUMBA
230302	ILABAYA
230303	ITE
230400	TARATA
230401	TARATA
230402	HEROES ALBARRACIN
230403	ESTIQUE
230404	ESTIQUE-PAMPA
230405	SITAJARA
230406	SUSAPAYA
230407	TARUCACHI
230408	TICACO
240000	TUMBES
240100	TUMBES
240101	TUMBES
240102	CORRALES
240103	LA CRUZ
240104	PAMPAS DE HOSPITAL
240105	SAN JACINTO
240106	SAN JUAN DE LA VIRGEN
240200	CONTRALMIRANTE VILLAR
240201	ZORRITOS
240202	CASITAS
240203	CANOAS DE PUNTA SAL
240300	ZARUMILLA
240301	ZARUMILLA
240302	AGUAS VERDES
240303	MATAPALO
240304	PAPAYAL
250000	UCAYALI
250100	CORONEL PORTILLO
250101	CALLERIA
250102	CAMPOVERDE
250103	IPARIA
250104	MASISEA
250105	YARINACOCHA
250106	NUEVA REQUENA
250107	MANANTAY
250200	ATALAYA
250201	RAYMONDI
250202	SEPAHUA
250203	TAHUANIA
250204	YURUA
250300	PADRE ABAD
250301	PADRE ABAD
250302	IRAZOLA
250303	CURIMANA
250400	PURUS
250401	PURUS
150000	Lima
150100	Lima
150101	Lima
\.


--
-- TOC entry 2606 (class 0 OID 24833)
-- Dependencies: 222
-- Data for Name: ht_seg_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_usuario (us_id, per_id, pe_id, ho_id, us_login, us_password, us_puntos, su_id) FROM stdin;
00000001	0001	0000000001	01	admin	21232f297a57a5a743894a0e4a801fc3	\N	\N
00000004	0003	0000000004	01	d@q.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000005	0003	0000000005	01	diego@qp.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000006	0003	0000000006	01	luis_campos@usmp.pe	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000007	0003	0000000007	01	d@q.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000008	0003	0000000008	01	simplemente_jc@hotmail.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000009	0003	0000000009	01	simplemente_jc@hotmail.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000010	0003	0000000010	01	d@q.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000011	0003	0000000011	01	alexismonje@gmail.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000012	0003	0000000012	01	ogonzalez@gamesacorp.com	827ccb0eea8a706c4c34a16891f84e7b	\N	\N
00000013	0003	0000000013	01	luis.cval@gmail.com	6976e3e73031ce3f397e8f9ac1d4bcb1	\N	\N
00000014	0003	0000000014	01	d.fontanini@dca.it	e2959dc01b11393ae74c3a916a3ca7a7	\N	\N
00000015	0003	0000000015	01	d@q.com	0d55a09480578958c9d5ce02ee353856	\N	\N
00000016	0003	0000000016	01	lalala@hotmail.com	4834e58231763992f4229c9697689f02	\N	\N
00000017	0003	0000000017	01	dfadsfad@hotmail.com	0121f917551807a6506113f84757af51	\N	\N
00000018	0003	0000000018	01	luis.cval@gmail.com	cb970510bd0e0b1fcef46b9774d8e257	\N	\N
00000019	0003	0000000019	01	simplemente_jc@hotmail.com	d53b1139b4dd637762ce995294b88ab6	\N	\N
00000020	0003	0000000020	01	simplemente@hotmail.com	1ef63043c23a93f65c3637e9838fe325	\N	\N
00000021	0003	0000000021	01	asdf@hotmail.com	edad198b923943a79bb90816f665bec2	\N	\N
00000022	0003	0000000022	01	marrselo@gmail.com	17c6e61eaba15728c1cc8ed87e5bd745	\N	\N
00000023	0003	0000000023	01	sosmar1@aol.com	2720b3b01a21bba3ec380dcd53113752	\N	\N
00000003	0003	0000000003	01	jmike@gmail.com	3201ab8ebcd07cc77acca54b8dcfe712	\N	\N
00000025	0003	0000000025	01	luis.cval@gmail.com	923fc10317da6d320e84807a549f3880	\N	\N
00000026	0003	0000000026	01	luis.cval@gmail.com	8adca1bbad65ac140fe03b5ea62e519a	\N	\N
00000028	0003	0000000028	01	t2@gmail.com	ff1160c6e9ab8c577380867a64df057b	\N	\N
00000024	0003	0000000024	01	jmike15@gmail.com	fe640be9cb159e28dfea791745011cbb	\N	\N
00000002	0003	0000000002	01	jmike410@gmail.com	3201ab8ebcd07cc77acca54b8dcfe712	\N	\N
00000027	0003	0000000027	01	jmike410@gmail.com	3201ab8ebcd07cc77acca54b8dcfe712	\N	\N
\.


--
-- TOC entry 2607 (class 0 OID 24836)
-- Dependencies: 223
-- Data for Name: ht_seg_usuario_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_seg_usuario_menu (us_id, per_id, pe_id, su_id, me_id, usm_permiso, usm_estado) FROM stdin;
00000001	0001	0000000001	00000001	00004004	3	S
00000001	0001	0000000001	00000001	00004000	3	S
00000001	0001	0000000001	00000001	00004005	3	S
00000001	0001	0000000001	00000001	00004006	3	S
00000001	0001	0000000001	00000001	00004009	3	S
00000001	0001	0000000001	00000001	00004008	3	S
00000001	0001	0000000001	00000001	00004007	3	S
00000001	0001	0000000001	00000001	00004010	3	S
00000001	0001	0000000001	00000001	00004003	3	S
00000001	0001	0000000001	00000001	00004002	3	S
00000001	0001	0000000001	00000001	00004001	3	S
00000001	0001	0000000001	00000001	00003008	3	S
00000001	0001	0000000001	00000001	00002006	3	S
00000001	0001	0000000001	00000002	00002003	3	S
00000001	0001	0000000001	00000002	00004000	3	S
00000001	0001	0000000001	00000002	00004004	3	S
00000001	0001	0000000001	00000002	00004005	3	S
00000001	0001	0000000001	00000002	00004006	3	S
00000001	0001	0000000001	00000002	00004009	3	S
00000001	0001	0000000001	00000002	00004008	3	S
00000001	0001	0000000001	00000002	00004007	3	S
00000001	0001	0000000001	00000002	00004010	3	S
00000001	0001	0000000001	00000002	00004003	3	S
00000001	0001	0000000001	00000002	00004002	3	S
00000001	0001	0000000001	00000002	00004001	3	S
00000001	0001	0000000001	00000002	00002000	3	S
00000001	0001	0000000001	00000002	00002004	3	S
00000001	0001	0000000001	00000002	00002005	3	S
00000001	0001	0000000001	00000002	00002006	3	S
00000001	0001	0000000001	00000002	00002007	3	S
00000001	0001	0000000001	00000002	00002008	3	S
00000001	0001	0000000001	00000002	00003000	3	S
00000001	0001	0000000001	00000002	00003008	3	S
00000001	0001	0000000001	00000002	00003001	3	S
00000001	0001	0000000001	00000002	00003003	3	S
00000001	0001	0000000001	00000002	00003006	3	S
00000001	0001	0000000001	00000002	00003007	3	S
00000001	0001	0000000001	00000002	00003002	3	S
00000001	0001	0000000001	00000002	00001000	3	S
00000001	0001	0000000001	00000002	00001004	3	S
00000001	0001	0000000001	00000002	00001005	3	S
00000001	0001	0000000001	00000002	00001006	3	S
00000001	0001	0000000001	00000002	00001002	3	S
00000001	0001	0000000001	00000002	00001001	3	S
00000001	0001	0000000001	00000001	00002009	3	S
00000001	0001	0000000001	00000002	00002009	3	S
00000001	0001	0000000001	00000003	00002009	3	S
00000001	0001	0000000001	00000002	00003009	3	S
00000001	0001	0000000001	00000002	00004011	3	S
00000001	0001	0000000001	00000002	00004012	3	S
00000001	0001	0000000001	00000003	00002010	3	S
00000001	0001	0000000001	00000002	00002010	3	S
\.


--
-- TOC entry 2608 (class 0 OID 24839)
-- Dependencies: 224
-- Data for Name: ht_ser_idioma_servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_ser_idioma_servicio (ser_id, id_id, ser_titulo, ser_desc) FROM stdin;
\.


--
-- TOC entry 2609 (class 0 OID 24842)
-- Dependencies: 225
-- Data for Name: ht_ser_idioma_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_ser_idioma_tipo (set_id, id_id, set_titulo, set_desc) FROM stdin;
1 	ES	SERVICIOS	\N
1 	EN	SERVICIOS	\N
2 	ES	taxi remisse	fvfvwf
2 	EN	taxi remisse	fvfvwf
\.


--
-- TOC entry 2610 (class 0 OID 24845)
-- Dependencies: 226
-- Data for Name: ht_ser_servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_ser_servicio (ser_id, set_id, ser_razsocial, ser_ruc, ser_direccion, ser_telefono1, ser_telefono2, ser_web, ser_correo, ser_precio, ser_estado, ub_id) FROM stdin;
\.


--
-- TOC entry 2611 (class 0 OID 24851)
-- Dependencies: 227
-- Data for Name: ht_ser_servicio_sucursal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_ser_servicio_sucursal (ser_id, su_id) FROM stdin;
\.


--
-- TOC entry 2612 (class 0 OID 24854)
-- Dependencies: 228
-- Data for Name: ht_ser_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ht_ser_tipo (set_id, set_estado) FROM stdin;
1 	\N
2 	1
\.


--
-- TOC entry 2613 (class 0 OID 24857)
-- Dependencies: 229
-- Data for Name: idioma_tabla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY idioma_tabla (id_id, tba_id, tba_desc) FROM stdin;
ES	001	Nacionalidad
ES	003	Aseguradoras / Clínicas
ES	010	Colegios
ES	011	Documento de Identidad
ES	014	Banco
ES	015	Destino
ES	016	Estado Civil
ES	020	Tipo Personal
ES	021	Religión
ES	022	Pais
ES	023	Medio de Transporte
ES	025	Lengua
ES	026	Nivel de Instrucción
ES	033	Forma de Pago
ES	037	Tipo de Documento Sunat
ES	042	Estado Civil Familia
ES	043	Actividades
ES	002	Distrito
EN	001	Nacionalidad
EN	003	Aseguradoras / Clínicas
EN	010	Colegios
EN	011	Documento de Identidad
EN	014	Banco
EN	015	Destino
EN	016	Estado Civil
EN	020	Tipo Personal
EN	021	Religión
EN	022	Pais
EN	023	Medio de Transporte
EN	025	Lengua
EN	026	Nivel de Instrucción
EN	033	Forma de Pago
EN	037	Tipo de Documento Sunat
EN	042	Estado Civil Familia
EN	043	Actividades
EN	002	Distrito
ES	012	Tarjetas de Crédito
EN	012	Credit Cards
\.


--
-- TOC entry 2614 (class 0 OID 24860)
-- Dependencies: 230
-- Data for Name: idioma_tabla_det; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY idioma_tabla_det (id_id, tad_id, tba_id, tad_desc) FROM stdin;
ES	00	026	Ninguno
ES	00	025	Ninguno
ES	0001	014	Banco Continental
ES	0001	003	Montefiori
ES	0001	016	Soltero(a)
ES	0001	015	Colegio
ES	0001	021	catolica
ES	0001	010	Colegio
ES	0001	020	Directivo
ES	0002	003	Good Hope
ES	0002	014	Banco Scotiabank
ES	0002	010	SALESIANO
ES	0002	020	Profesor
ES	0002	015	Copafa
ES	0002	016	Casado(a)
ES	0002	021	Evamgelico
ES	0003	020	Empleado
ES	0003	001	Italiana
ES	0003	003	Ricardo Palma
ES	0003	016	Viudo(a)
ES	0003	010	C.E. "Lucecito de Magdalena"
ES	0004	020	Obrero
ES	0004	014	Banco Sudamericano
ES	0004	010	CEI SAN MIGUEL
ES	0004	016	Divorciado(a)
ES	0004	003	San Bernardo
ES	0005	016	Separado(a)
ES	0005	001	Colombiana
ES	0005	014	Banco Financiero
ES	0005	003	Las Americas
ES	0005	010	IE LUIS ENRIQUE X
ES	0006	003	Los Andes
ES	0006	014	Banco Interbank
ES	0006	001	Venezolana
ES	0007	001	Inglesa
ES	0007	010	IE NIDO CHICHIZOLA
ES	0007	003	San Jose
ES	0007	014	BIF
ES	0008	010	CEI AUGUSTO PEREZA ARANIBAR
ES	0008	003	San Pablo
ES	0008	001	Cubana
ES	0009	001	China
ES	0009	010	CEI LAS AMERICAS
ES	0009	003	San Pablo (Sede norte)
ES	001	043	Pastoral
ES	001	042	C. Civil
ES	0010	001	Chilena
ES	0010	014	Caja Municipal de Sullana
ES	0010	003	San Pablo (Sede Sur)
ES	0011	001	Alemana
ES	0011	010	IE LEARNING KIDS
ES	0011	003	Santa Monica
ES	0012	010	IE SAGRADO CORAZON
ES	0012	003	Stella Maris
ES	0012	001	Argentina
ES	0013	003	Padre Luis Tezza
ES	0013	001	Guinea
ES	0013	010	GOTITA DE MIEL
ES	0014	001	Japonesa
ES	0014	003	Maison de Sante
ES	0014	010	GOTITAS DE MIEL
ES	0015	001	Brasilera
ES	0015	010	IEP "Madre Enriqueta Vicens"
ES	0016	010	C.E.I. "Nido de Papel"
ES	0016	001	Uruguaya
ES	0017	010	CEI VIRGEN DEL CARMEN
ES	0017	001	Ecuatoriana
ES	0018	010	I.E. "Reina del Carmelo"
ES	0018	001	Moldava
ES	0019	010	TRILCE
ES	0019	001	Noruega
ES	002	043	Jornada
ES	002	042	C. Religioso
ES	0020	010	IEI VIRGEN DE LOURDES
ES	0020	001	Boliviana
ES	0021	010	C.E.P. "Independencia"
ES	0022	001	Francia
ES	0022	010	CEI "Gotitas de Miel"
ES	0023	010	I.E.P."Los Rosales"
ES	0023	001	Polonia
ES	0024	010	I.E.I. "Pininos"
ES	0025	010	IE "ANA MARIA BACH"
ES	0026	010	IE 090
ES	0027	010	CEI SOL DE FATIMA
ES	0029	010	CEIP CARITAS ALEGRES
ES	003	043	Retiro
ES	003	042	C. Civil-Relig.
ES	0030	010	MARIA AUXILIADORA
ES	0031	010	COLEGIO BELEN
ES	0033	010	IEI 098
ES	0035	010	Pininos
ES	0036	010	CLARETIANO
ES	0037	010	CEP SAN JOSE PADRES MARISTAS
ES	0038	010	IE SANTA ROSA
ES	0039	010	Bubles Kids
ES	004	042	Conviviente
ES	004	043	Talleres
ES	0002	001	Norteamericana
ES	0040	010	IE ISABEL FLORES DE OLIVA
ES	0041	010	VIRGEN DEL CARMEN
ES	0042	010	IE "REINA DEL CARMELO"
ES	0044	010	CEI LOS ROSALES
ES	0045	010	C.E. "Alas Peruano Argentino"
ES	0046	010	CEI 091
ES	0047	010	IE JUNIOR CESAR DE LOS RIOS
ES	0048	010	SAN FELIPE
ES	0049	010	IEI 091
ES	005	042	Divorciado
ES	0051	010	CEI SAN FELIPE
ES	0052	010	C.P. "Angelica Recharte"
ES	0053	010	IEI GUILLERMO ALMEYDA IRIGOYEN
ES	0054	010	CEI GUILLERMO ALMENARA
ES	0055	010	I.E.P.I.  "Caritas Alegres"
ES	0056	010	I.E. "Juana Alarco de Dammert"
ES	0058	010	LITTLE SCHOOL
ES	0059	010	C.E.P. "David Ausubel"
ES	006	042	Casados
ES	0060	010	CEI 098
ES	0061	010	SAN MIGUEL
ES	0063	010	CEI 095 MEDALLA MILAGROSA
ES	0064	010	IEPI. "Gotitas del Cielo"
ES	0066	010	CEI 127
ES	0067	010	CEP MARCO ANTONIO SCHENONE OLIVOS
ES	0068	010	C.E.I. "Ana Magdalena Bach"
ES	0069	010	CEI MADRE TERESA RONDON
ES	0071	010	I.E. Preuniversitaria "Friedrich Gauss"
ES	0073	010	CEI FRANCISCO BOLOGNESI
ES	0075	010	SALESIANO ROSENTHAL DE LA PUENTE
ES	0076	010	CEI LITTLE GARDEN
ES	0078	010	IE SANTA MARIA DE LA GRACIA  l
ES	0079	010	IE MARIANO MELGAR
ES	0080	010	CEN CHICHIZOLA
ES	0081	010	CEP Los Querubines
ES	0082	010	IE 098
ES	0083	010	EI 098
ES	0085	010	C.E.I. "Rayito de Luz"
ES	0086	010	IE SANTA MAR5IA DE LA GRACIA
ES	0087	010	C.E. "Pamer"
ES	0088	010	C.E.I. "Juana de Arco"
ES	0090	010	SAN LUCAS
ES	0091	010	CEI VIRGEN MARIA
ES	0092	010	IE ALFONSO UGARTE
ES	0093	010	CE SAN MIGUEL
ES	0095	010	CLEMENTE ALTHAUS
ES	0099	010	IE "GOTITAS DE MIEL"
ES	01	026	Primaria Incompleta
ES	01	011	DNI
ES	01	037	Factura
ES	01	025	Castellano
ES	01	023	A pie
ES	0100	010	IE "Ana Magdalena Bach"
ES	0101	010	IE "SAN AGUSTIN DE IQUITOS"
ES	0102	010	CEI JESUS DIVINO MAESTRO
ES	0103	010	CEP SAGITARIO
ES	0104	010	IEP "Clemente Althaus"
ES	0105	010	I.E.P "Alfredo Deza"
ES	0106	010	CEI SAN IGNACIO DE LOYOLA
ES	0107	010	I.E.P."Santisima Cruz"
ES	0108	010	IE SAN MIGUEL
ES	0110	010	ALDEA DEL PADRES FEDERICO JUNIPERO
ES	0111	010	CEP MARIA INMACULADA
ES	0112	010	IEI PININOS
ES	0113	010	CEI CAYETANITO DE LOS OLIVOS
ES	0114	010	CEI EL CARMELO
ES	0115	010	CEI RAYITOS DE LUZ
ES	0116	010	C.E.I.P "Caritas Alegres"
ES	0117	010	C.E.P. " Mabel  Condemarin"
ES	0120	010	C.E.I. "Bubble Kids"
ES	0121	010	CEP GERTRUDE HANKS
ES	0122	010	C.E.I. " Madre Enriqueta Vicens"
ES	0123	010	CEI SANTA MARIA GRACIA
ES	0126	010	MATER ADMIRABILIS
ES	0127	010	COLEGIO COOPERATIVO SAN FELIPE
ES	0128	010	C.E. "San Felipe"
ES	0129	010	IEI PRIVADO BERTOLT BRECHT
ES	0130	010	CEI. "Rayitos de Luz"
ES	0131	010	SAN LUIS MARISTAS
ES	0135	010	IEP "gotitas de Miel"
ES	0136	010	SANTO DOMINGO APOSTOL
ES	0137	010	CEP MI AMIGO JESUS
ES	0138	010	CEI. "Pininos"
ES	0139	010	C.E. "Cristo Redentor"
ES	0140	010	IEP BOLITA DE CIELO
ES	0145	010	CEI O9O SAN FELIPE
ES	0146	010	IEI LITTLE SCHOOL
ES	0147	010	IE LA SAGRADA FAMILIA
ES	0149	010	CEI INGENIERIA
ES	0150	010	IEP ALFREDO REBAZA COSTA
ES	0152	010	CE SAN LUIS GONZAGA
ES	0153	010	NUESTRA SRA. DE GUADALUPE
ES	0154	010	IE LITLE SCHOOL
ES	0155	010	LITTLE SCHOLL
ES	0156	010	HERMAN - USA
ES	0157	010	SACO OLIVEROS
ES	0159	010	CEP SANTISIMA CRUZ
ES	0160	010	ALFREDO DEZA
ES	0161	010	IE LUCERITO ALFREDO DEZA
ES	0162	010	CEI ALFREDO DEZA
ES	0163	010	CI "KIDDIE CARE"
ES	0164	010	CEI LA SAGRADA FAMILIA
ES	0165	010	MADRE ENRIQUETA VICENS
ES	0167	010	CEI 06 MIRAFLORES
ES	0168	010	I.E.P. "Santa Isabel"
ES	0169	010	CEI VILLANOVA
ES	0172	010	REYNA DE LA PAZ
ES	0173	010	CEI REYNA DE LA PAZ
ES	0174	010	CEI 090
ES	0175	010	CEI DOMINGUITO SABIO
ES	0176	010	IE VICTOR ANDRES BELAUNDE
ES	0178	010	C.P."San Norberto"
ES	0179	010	IE "MIS PASITOS FELICES"
ES	0180	010	IEP SANTISIMA TRINIDAD
ES	0182	010	CEP ISAAC NEWTON
ES	0183	010	CEP MARTIR OLAYA
ES	0185	010	C.P."Santo Domingo, el Apostol"
ES	0186	010	CEI MADRE ANGELICA RECHARTE
ES	0187	010	CEI VIRGEN DE LA MACARENA
ES	0188	010	INMACULADA CORAZON DE MARIA
ES	0189	010	CEI MADRE ENRIQUETA VICENS
ES	0194	010	IEP INTERAMERICANO
ES	0195	010	MELVIN JOHNS
ES	0196	010	IE JACARANDA
ES	07	011	Pasaporte
ES	0198	010	I.E.Parroquial "Virgen de la Esperanza"
ES	0199	010	C.E.I. "La casita de Nany"
ES	02	011	Carnet de Fuerzas Policiales
ES	02	026	Primaria completa
ES	02	037	Recibo  por honorarios
ES	02	025	Quechua
ES	02	023	Vehiculo de Transporte Motorizado
ES	0200	010	COLEGIO LA UNION
ES	0201	010	IEP ANA MAGDALENA BACH
ES	0202	010	IEP SANTA MARIA DE LA GRACIA
ES	0204	010	CEI MEDALLA MILAGROSA 095
ES	0206	010	NIDO DE PAPEL
ES	0207	010	LICEO PATRIA COLOMBIA
ES	0208	010	C.E.P."Clemente Althaus"
ES	0209	010	IEP "Brigham Young"
ES	0210	010	I.E. 1074 "REPUBLICA DE IRLANDA"
ES	0211	010	IEP SANTA ANA
ES	0212	010	CEP SANTA ANA
ES	0213	010	CEI NIDO DE PAPEL
ES	0214	010	C.E. "Reina Marianista"
ES	0215	010	IEP MAYUPAMPA
ES	0216	010	IEP SAN LUIS GONZAGADE PIURA
ES	0217	010	C.E.P "Santa Maria de la Grazia"
ES	0218	010	CEI MARIA DE LOS ANGELES
ES	0219	010	MARIA DE LOS ANGELES
ES	0220	010	IE PERUANO CANADIENSE
ES	0221	010	I.E.P. "Peruano Chino"
ES	0226	010	CEI "SOR ANGELA LECCA"
ES	0227	010	IE "Sor angela Lecca"
ES	0228	010	CEI MARIA AUXILIADORA
ES	0229	010	C.E. "Bertolt Brecht"
ES	0230	010	CEI NIDO DE JESUS
ES	0231	010	IE. LEWIS ELEMENTARY - USA
ES	0232	010	CEI SANTA MARIA DE LA GRACIA
ES	0233	010	IEI "ORFEON"
ES	0234	010	CEP SAN  PIO X
ES	0235	010	SAN PIO X
ES	0236	010	SAN ANTONIO MARISTAS
ES	0237	010	C.E.P. Clemente Athaus"
ES	0238	010	IE RAYITOS DE LUZ
ES	0239	010	CEI MODELO ESSALUD
ES	0240	010	COLEGIO DE CIENCIAS
ES	0241	010	IEP "Colegio de Ciencias"
ES	0242	010	IE MADRE ENRIQUETA VICENS
ES	0244	010	CEI OLIVAR
ES	0246	010	CEP SAN GABRIEL
ES	0247	010	IE MADRE TERESA RONDON
ES	0248	010	CEP SAN FELIPE
ES	0249	010	IEI SAN JUAN RAGGIO CHICCIZOLA
ES	0250	010	COLEGIO INGENIERIA
ES	0251	010	I.E.P. "Clinton-Rodham"
ES	0252	010	CUNA JARDIN LEARNING KIDS
ES	0253	010	CEI LOS PININOS
ES	0254	010	IEIP MARIA AUXILIADORA
ES	0255	010	NEWTON JESUS MARIA
ES	0256	010	IE MARIA INMACULADA
ES	0258	010	CEP REYNA DE LOS ANGELES
ES	0259	010	IE "Camila School"
ES	0261	010	IEI 126 VIRGEN DEL CARMEN
ES	0262	010	CEP EL CARMELO
ES	0263	010	C.E.P."St.Andrew"
ES	0268	010	COLEGIO PANAMERICANO
ES	0269	010	C.E.P. "Santa Rosa"
ES	0271	010	CEI  MADRE ENRIQUETA VICENS
ES	0272	010	COLEGIO EXPERIMENTAL SAN MIGUEL
ES	0273	010	CEI "SANTA ROSA"
ES	0275	010	COLEGIO MATER ADMIRABILIS
ES	0278	010	CEI JUAN RAGGIO CHICCIZOLA
ES	0279	010	IE JUAN RAGGIO CHICCIZOLA
ES	0280	010	RECOLETA
ES	0282	010	PAMER
ES	0283	010	CEI CARITAS ALEGRES
ES	0285	010	CENTRO EDUCATIVO INICIAL 106
ES	0286	010	CEI JESUSCITO
ES	0287	010	CEI AURORA T DE CASTRO IGLESIAS
ES	0290	010	CENTRO EDUCATIVO INICIAL 091 SAN FELIPE
ES	0291	010	IE "NIDO DE PAPEL"
ES	0293	010	CEP. "Santa Ana"
ES	0294	010	CEI SANTA ANA
ES	0295	010	MONTEALTO
ES	0296	010	IEP "ANA MAGDALENA BACH"
ES	0297	010	IE ANA MAGDALENA BACH
ES	0299	010	COLEGIO ALAS
ES	03	026	Secundaria Incompleta
ES	03	037	Boleta de venta
ES	03	025	Aymara
ES	03	023	Vehiculo Particular
ES	03	011	Carnet de Fuerzas Armadas
ES	0301	010	IEP "Manuel Pardo"
ES	0302	010	CEP MANUEL PARDO
ES	0303	010	C.E. "Santa Ana"
ES	0305	010	CEI CIELITO LINDO
ES	0306	010	C.E. "Villa Magadalema"
ES	0308	010	IE "VIRGEN DEL CARMEN"
ES	0309	010	LOS ALAMOS
ES	0310	010	CEP JESUS MAESTRO
ES	0311	010	C.E."Montealto"
ES	0312	010	I.S.N. Cuna Jardin
ES	0314	010	IEP "Salesiano Rosenthal de la Puente"
ES	0315	010	IEI "PININOS"
ES	0316	010	CEI MATER ADMIRABILIS
ES	0317	010	CEP SAN MATILDE
ES	0318	010	CEP SAN ANTONIO DE SAN MIGUEL
ES	0319	010	CEI "MARIA AUXILIADORA"
ES	0320	010	IE "NIDO ALAMITOS"
ES	0321	010	Nuestra Señora del Consuelo
ES	04	026	Secundaria Completa
ES	04	023	Movilidad Escolar
ES	04	025	Ashaninka(Campa)
ES	05	023	Otros vehiculos Motorizados
ES	05	026	Superior No Univ. Incompleta
ES	05	025	Aguaruna
ES	06	026	Superior No Univ. Completa
ES	06	025	Shipiba
ES	06	023	Otros vehiculos No Motorizados
ES	07	023	Transporte Animal
ES	07	026	Superior Univ. Incompleta
ES	07	025	Huitoto
ES	08	025	Cocama-Cocamilla
ES	08	011	Doc. Provisional de Identidad
ES	08	026	Superior Univ. Completa
ES	09	026	Superior Post Graduado
ES	09	025	Machiguenga
ES	10	026	Magister
ES	10	025	Piro
ES	10	011	Autogenerado
ES	11	026	Doctor
ES	11	011	Partida de Nacimiento
ES	11	025	Cashinahua
ES	12	025	Cacataibo
ES	13	025	Yaminahua
ES	13	011	Trab.Menor de Edad / Interdiet
ES	14	025	Culina
ES	15	025	Sharanahua
ES	16	025	Matsanahua
ES	17	025	Anahuaca
ES	18	025	Huambisa
ES	19	025	Achuar
ES	20	025	Bora
ES	20	011	AFP Horizonte
ES	21	025	Chayahuita
ES	21	011	Carnet ESSALUD
ES	22	025	Yanesha
ES	22	011	Libreta Militar
ES	23	011	AFP Integra
ES	90	025	Ingles
ES	91	025	Frances
ES	92	025	Italiano
ES	93	025	Otro extranjero
ES	94	025	Otro nacional
ES	99	023	Otro
ES	A	033	American Express
ES	C001	002	Callao
ES	D	033	Diners
ES	M	033	MasterCard
ES	V	033	Visa
ES	0015	003	Red Uchiza
ES	07	037	Nota de Crédito
ES	08	037	Nota de Débito
ES	0001	001	Peruana
EN	00	026	Ninguno
EN	00	025	Ninguno
EN	0001	014	Banco Continental
EN	0001	003	Montefiori
EN	0001	016	Soltero(a)
EN	0001	015	Colegio
EN	0001	021	catolica
EN	0001	010	Colegio
EN	0001	020	Directivo
EN	0002	003	Good Hope
EN	0002	014	Banco Scotiabank
EN	0002	010	SALESIANO
EN	0002	020	Profesor
EN	0002	015	Copafa
EN	0002	016	Casado(a)
EN	0002	021	Evamgelico
EN	0003	020	Empleado
EN	0003	003	Ricardo Palma
EN	0003	016	Viudo(a)
EN	0003	010	C.E. "Lucecito de Magdalena"
EN	0004	020	Obrero
EN	0004	014	Banco Sudamericano
EN	0004	010	CEI SAN MIGUEL
EN	0004	016	Divorciado(a)
EN	0004	003	San Bernardo
EN	0005	016	Separado(a)
EN	0005	014	Banco Financiero
EN	0005	003	Las Americas
EN	0005	010	IE LUIS ENRIQUE X
EN	0006	003	Los Andes
EN	0006	014	Banco Interbank
EN	0006	001	Venezolana
EN	0007	001	Inglesa
EN	0007	010	IE NIDO CHICHIZOLA
EN	0007	003	San Jose
EN	0007	014	BIF
EN	0008	010	CEI AUGUSTO PEREZA ARANIBAR
EN	0008	003	San Pablo
EN	0008	001	Cubana
EN	0009	001	China
EN	0009	010	CEI LAS AMERICAS
EN	0009	003	San Pablo (Sede norte)
EN	001	043	Pastoral
EN	001	042	C. Civil
EN	0010	001	Chilena
EN	0010	014	Caja Municipal de Sullana
EN	0010	003	San Pablo (Sede Sur)
EN	0011	001	Alemana
EN	0011	010	IE LEARNING KIDS
EN	0011	003	Santa Monica
EN	0012	010	IE SAGRADO CORAZON
EN	0012	003	Stella Maris
EN	0012	001	Argentina
EN	0013	003	Padre Luis Tezza
EN	0013	001	Guinea
EN	0013	010	GOTITA DE MIEL
EN	0014	001	Japonesa
EN	0014	003	Maison de Sante
EN	0014	010	GOTITAS DE MIEL
EN	0015	001	Brasilera
EN	0015	010	IEP "Madre Enriqueta Vicens"
EN	0016	010	C.E.I. "Nido de Papel"
EN	0016	001	Uruguaya
EN	0017	010	CEI VIRGEN DEL CARMEN
EN	0017	001	Ecuatoriana
EN	0018	010	I.E. "Reina del Carmelo"
EN	0018	001	Moldava
EN	0019	010	TRILCE
EN	0019	001	Noruega
EN	002	043	Jornada
EN	002	042	C. Religioso
EN	0005	001	Colombian
EN	0020	010	IEI VIRGEN DE LOURDES
EN	0020	001	Boliviana
EN	0021	010	C.E.P. "Independencia"
EN	0022	001	Francia
EN	0022	010	CEI "Gotitas de Miel"
EN	0023	010	I.E.P."Los Rosales"
EN	0023	001	Polonia
EN	0024	010	I.E.I. "Pininos"
EN	0025	010	IE "ANA MARIA BACH"
EN	0026	010	IE 090
EN	0027	010	CEI SOL DE FATIMA
EN	0029	010	CEIP CARITAS ALEGRES
EN	003	043	Retiro
EN	003	042	C. Civil-Relig.
EN	0030	010	MARIA AUXILIADORA
EN	0031	010	COLEGIO BELEN
EN	0033	010	IEI 098
EN	0035	010	Pininos
EN	0036	010	CLARETIANO
EN	0037	010	CEP SAN JOSE PADRES MARISTAS
EN	0038	010	IE SANTA ROSA
EN	0039	010	Bubles Kids
EN	004	042	Conviviente
EN	004	043	Talleres
EN	0002	001	Norteamericanaa
EN	0040	010	IE ISABEL FLORES DE OLIVA
EN	0041	010	VIRGEN DEL CARMEN
EN	0042	010	IE "REINA DEL CARMELO"
EN	0044	010	CEI LOS ROSALES
EN	0045	010	C.E. "Alas Peruano Argentino"
EN	0046	010	CEI 091
EN	0047	010	IE JUNIOR CESAR DE LOS RIOS
EN	0048	010	SAN FELIPE
EN	0049	010	IEI 091
EN	005	042	Divorciado
EN	0051	010	CEI SAN FELIPE
EN	0052	010	C.P. "Angelica Recharte"
EN	0053	010	IEI GUILLERMO ALMEYDA IRIGOYEN
EN	0054	010	CEI GUILLERMO ALMENARA
EN	0055	010	I.E.P.I.  "Caritas Alegres"
EN	0056	010	I.E. "Juana Alarco de Dammert"
EN	0058	010	LITTLE SCHOOL
EN	0059	010	C.E.P. "David Ausubel"
EN	006	042	Casados
EN	0060	010	CEI 098
EN	0061	010	SAN MIGUEL
EN	0063	010	CEI 095 MEDALLA MILAGROSA
EN	0064	010	IEPI. "Gotitas del Cielo"
EN	0066	010	CEI 127
EN	0067	010	CEP MARCO ANTONIO SCHENONE OLIVOS
EN	0068	010	C.E.I. "Ana Magdalena Bach"
EN	0069	010	CEI MADRE TERESA RONDON
EN	0071	010	I.E. Preuniversitaria "Friedrich Gauss"
EN	0073	010	CEI FRANCISCO BOLOGNESI
EN	0075	010	SALESIANO ROSENTHAL DE LA PUENTE
EN	0076	010	CEI LITTLE GARDEN
EN	0078	010	IE SANTA MARIA DE LA GRACIA  l
EN	0079	010	IE MARIANO MELGAR
EN	0080	010	CEN CHICHIZOLA
EN	0081	010	CEP Los Querubines
EN	0082	010	IE 098
EN	0083	010	EI 098
EN	0085	010	C.E.I. "Rayito de Luz"
EN	0086	010	IE SANTA MAR5IA DE LA GRACIA
EN	0087	010	C.E. "Pamer"
EN	0088	010	C.E.I. "Juana de Arco"
EN	0090	010	SAN LUCAS
EN	0091	010	CEI VIRGEN MARIA
EN	0092	010	IE ALFONSO UGARTE
EN	0093	010	CE SAN MIGUEL
EN	0095	010	CLEMENTE ALTHAUS
EN	0099	010	IE "GOTITAS DE MIEL"
EN	01	026	Primaria Incompleta
EN	01	011	DNI
EN	01	037	Factura
EN	01	025	Castellano
EN	01	023	A pie
EN	0100	010	IE "Ana Magdalena Bach"
EN	0101	010	IE "SAN AGUSTIN DE IQUITOS"
EN	0102	010	CEI JESUS DIVINO MAESTRO
EN	0103	010	CEP SAGITARIO
EN	0104	010	IEP "Clemente Althaus"
EN	0105	010	I.E.P "Alfredo Deza"
EN	0106	010	CEI SAN IGNACIO DE LOYOLA
EN	0107	010	I.E.P."Santisima Cruz"
EN	0108	010	IE SAN MIGUEL
EN	0110	010	ALDEA DEL PADRES FEDERICO JUNIPERO
EN	0111	010	CEP MARIA INMACULADA
EN	0112	010	IEI PININOS
EN	0113	010	CEI CAYETANITO DE LOS OLIVOS
EN	0114	010	CEI EL CARMELO
EN	0115	010	CEI RAYITOS DE LUZ
EN	0116	010	C.E.I.P "Caritas Alegres"
EN	0117	010	C.E.P. " Mabel  Condemarin"
EN	0120	010	C.E.I. "Bubble Kids"
EN	0121	010	CEP GERTRUDE HANKS
EN	0122	010	C.E.I. " Madre Enriqueta Vicens"
EN	0123	010	CEI SANTA MARIA GRACIA
EN	0126	010	MATER ADMIRABILIS
EN	0127	010	COLEGIO COOPERATIVO SAN FELIPE
EN	0128	010	C.E. "San Felipe"
EN	0129	010	IEI PRIVADO BERTOLT BRECHT
EN	0130	010	CEI. "Rayitos de Luz"
EN	0131	010	SAN LUIS MARISTAS
EN	0135	010	IEP "gotitas de Miel"
EN	0136	010	SANTO DOMINGO APOSTOL
EN	0137	010	CEP MI AMIGO JESUS
EN	0138	010	CEI. "Pininos"
EN	0139	010	C.E. "Cristo Redentor"
EN	0140	010	IEP BOLITA DE CIELO
EN	0145	010	CEI O9O SAN FELIPE
EN	0146	010	IEI LITTLE SCHOOL
EN	0147	010	IE LA SAGRADA FAMILIA
EN	0149	010	CEI INGENIERIA
EN	0150	010	IEP ALFREDO REBAZA COSTA
EN	0152	010	CE SAN LUIS GONZAGA
EN	0153	010	NUESTRA SRA. DE GUADALUPE
EN	0154	010	IE LITLE SCHOOL
EN	0155	010	LITTLE SCHOLL
EN	0156	010	HERMAN - USA
EN	0157	010	SACO OLIVEROS
EN	0159	010	CEP SANTISIMA CRUZ
EN	0160	010	ALFREDO DEZA
EN	0161	010	IE LUCERITO ALFREDO DEZA
EN	0162	010	CEI ALFREDO DEZA
EN	0163	010	CI "KIDDIE CARE"
EN	0164	010	CEI LA SAGRADA FAMILIA
EN	0165	010	MADRE ENRIQUETA VICENS
EN	0167	010	CEI 06 MIRAFLORES
EN	0168	010	I.E.P. "Santa Isabel"
EN	0169	010	CEI VILLANOVA
EN	0172	010	REYNA DE LA PAZ
EN	0173	010	CEI REYNA DE LA PAZ
EN	0174	010	CEI 090
EN	0175	010	CEI DOMINGUITO SABIO
EN	0176	010	IE VICTOR ANDRES BELAUNDE
EN	0178	010	C.P."San Norberto"
EN	0179	010	IE "MIS PASITOS FELICES"
EN	0180	010	IEP SANTISIMA TRINIDAD
EN	0182	010	CEP ISAAC NEWTON
EN	0183	010	CEP MARTIR OLAYA
EN	0185	010	C.P."Santo Domingo, el Apostol"
EN	0186	010	CEI MADRE ANGELICA RECHARTE
EN	0187	010	CEI VIRGEN DE LA MACARENA
EN	0188	010	INMACULADA CORAZON DE MARIA
EN	0189	010	CEI MADRE ENRIQUETA VICENS
EN	0194	010	IEP INTERAMERICANO
EN	0195	010	MELVIN JOHNS
EN	0196	010	IE JACARANDA
EN	07	011	Pasaporte
EN	0198	010	I.E.Parroquial "Virgen de la Esperanza"
EN	0199	010	C.E.I. "La casita de Nany"
EN	02	011	Carnet de Fuerzas Policiales
EN	02	026	Primaria completa
EN	02	037	Recibo  por honorarios
EN	02	025	Quechua
EN	02	023	Vehiculo de Transporte Motorizado
EN	0200	010	COLEGIO LA UNION
EN	0201	010	IEP ANA MAGDALENA BACH
EN	0202	010	IEP SANTA MARIA DE LA GRACIA
EN	0204	010	CEI MEDALLA MILAGROSA 095
EN	0206	010	NIDO DE PAPEL
EN	0207	010	LICEO PATRIA COLOMBIA
EN	0208	010	C.E.P."Clemente Althaus"
EN	0209	010	IEP "Brigham Young"
EN	0210	010	I.E. 1074 "REPUBLICA DE IRLANDA"
EN	0211	010	IEP SANTA ANA
EN	0212	010	CEP SANTA ANA
EN	0213	010	CEI NIDO DE PAPEL
EN	0214	010	C.E. "Reina Marianista"
EN	0215	010	IEP MAYUPAMPA
EN	0216	010	IEP SAN LUIS GONZAGADE PIURA
EN	0217	010	C.E.P "Santa Maria de la Grazia"
EN	0218	010	CEI MARIA DE LOS ANGELES
EN	0219	010	MARIA DE LOS ANGELES
EN	0220	010	IE PERUANO CANADIENSE
EN	0221	010	I.E.P. "Peruano Chino"
EN	0226	010	CEI "SOR ANGELA LECCA"
EN	0227	010	IE "Sor angela Lecca"
EN	0228	010	CEI MARIA AUXILIADORA
EN	0229	010	C.E. "Bertolt Brecht"
EN	0230	010	CEI NIDO DE JESUS
EN	0231	010	IE. LEWIS ELEMENTARY - USA
EN	0232	010	CEI SANTA MARIA DE LA GRACIA
EN	0233	010	IEI "ORFEON"
EN	0234	010	CEP SAN  PIO X
EN	0235	010	SAN PIO X
EN	0236	010	SAN ANTONIO MARISTAS
EN	0237	010	C.E.P. Clemente Athaus"
EN	0238	010	IE RAYITOS DE LUZ
EN	0239	010	CEI MODELO ESSALUD
EN	0240	010	COLEGIO DE CIENCIAS
EN	0241	010	IEP "Colegio de Ciencias"
EN	0242	010	IE MADRE ENRIQUETA VICENS
EN	0244	010	CEI OLIVAR
EN	0246	010	CEP SAN GABRIEL
EN	0247	010	IE MADRE TERESA RONDON
EN	0248	010	CEP SAN FELIPE
EN	0249	010	IEI SAN JUAN RAGGIO CHICCIZOLA
EN	0250	010	COLEGIO INGENIERIA
EN	0251	010	I.E.P. "Clinton-Rodham"
EN	0252	010	CUNA JARDIN LEARNING KIDS
EN	0253	010	CEI LOS PININOS
EN	0254	010	IEIP MARIA AUXILIADORA
EN	0255	010	NEWTON JESUS MARIA
EN	0256	010	IE MARIA INMACULADA
EN	0258	010	CEP REYNA DE LOS ANGELES
EN	0259	010	IE "Camila School"
EN	0261	010	IEI 126 VIRGEN DEL CARMEN
EN	0262	010	CEP EL CARMELO
EN	0263	010	C.E.P."St.Andrew"
EN	0268	010	COLEGIO PANAMERICANO
EN	0269	010	C.E.P. "Santa Rosa"
EN	0271	010	CEI  MADRE ENRIQUETA VICENS
EN	0272	010	COLEGIO EXPERIMENTAL SAN MIGUEL
EN	0273	010	CEI "SANTA ROSA"
EN	0275	010	COLEGIO MATER ADMIRABILIS
EN	0278	010	CEI JUAN RAGGIO CHICCIZOLA
EN	0279	010	IE JUAN RAGGIO CHICCIZOLA
EN	0280	010	RECOLETA
EN	0282	010	PAMER
EN	0283	010	CEI CARITAS ALEGRES
EN	0285	010	CENTRO EDUCATIVO INICIAL 106
EN	0286	010	CEI JESUSCITO
EN	0287	010	CEI AURORA T DE CASTRO IGLESIAS
EN	0290	010	CENTRO EDUCATIVO INICIAL 091 SAN FELIPE
EN	0291	010	IE "NIDO DE PAPEL"
EN	0293	010	CEP. "Santa Ana"
EN	0294	010	CEI SANTA ANA
EN	0295	010	MONTEALTO
EN	0296	010	IEP "ANA MAGDALENA BACH"
EN	0297	010	IE ANA MAGDALENA BACH
EN	0299	010	COLEGIO ALAS
EN	03	026	Secundaria Incompleta
EN	03	037	Boleta de venta
EN	03	025	Aymara
EN	03	023	Vehiculo Particular
EN	03	011	Carnet de Fuerzas Armadas
EN	0301	010	IEP "Manuel Pardo"
EN	0302	010	CEP MANUEL PARDO
EN	0303	010	C.E. "Santa Ana"
EN	0305	010	CEI CIELITO LINDO
EN	0306	010	C.E. "Villa Magadalema"
EN	0308	010	IE "VIRGEN DEL CARMEN"
EN	0309	010	LOS ALAMOS
EN	0310	010	CEP JESUS MAESTRO
EN	0311	010	C.E."Montealto"
EN	0312	010	I.S.N. Cuna Jardin
EN	0314	010	IEP "Salesiano Rosenthal de la Puente"
EN	0315	010	IEI "PININOS"
EN	0316	010	CEI MATER ADMIRABILIS
EN	0317	010	CEP SAN MATILDE
EN	0318	010	CEP SAN ANTONIO DE SAN MIGUEL
EN	0319	010	CEI "MARIA AUXILIADORA"
EN	0320	010	IE "NIDO ALAMITOS"
EN	0321	010	Nuestra Señora del Consuelo
EN	04	026	Secundaria Completa
EN	04	023	Movilidad Escolar
EN	04	025	Ashaninka(Campa)
EN	05	023	Otros vehiculos Motorizados
EN	05	026	Superior No Univ. Incompleta
EN	05	025	Aguaruna
EN	06	026	Superior No Univ. Completa
EN	06	025	Shipiba
EN	06	023	Otros vehiculos No Motorizados
EN	07	023	Transporte Animal
EN	07	026	Superior Univ. Incompleta
EN	07	025	Huitoto
EN	08	025	Cocama-Cocamilla
EN	08	011	Doc. Provisional de Identidad
EN	08	026	Superior Univ. Completa
EN	09	026	Superior Post Graduado
EN	09	025	Machiguenga
EN	10	026	Magister
EN	10	025	Piro
EN	10	011	Autogenerado
EN	11	026	Doctor
EN	11	011	Partida de Nacimiento
EN	11	025	Cashinahua
EN	12	025	Cacataibo
EN	13	025	Yaminahua
EN	13	011	Trab.Menor de Edad / Interdiet
EN	14	025	Culina
EN	15	025	Sharanahua
EN	16	025	Matsanahua
EN	17	025	Anahuaca
EN	18	025	Huambisa
EN	19	025	Achuar
EN	20	025	Bora
EN	20	011	AFP Horizonte
EN	21	025	Chayahuita
EN	21	011	Carnet ESSALUD
EN	22	025	Yanesha
EN	22	011	Libreta Militar
EN	23	011	AFP Integra
EN	90	025	Ingles
EN	91	025	Frances
EN	92	025	Italiano
EN	93	025	Otro extranjero
EN	94	025	Otro nacional
EN	99	023	Otro
EN	A	033	American Express
EN	C001	002	Callao
EN	D	033	Diners
EN	M	033	MasterCard
EN	V	033	Visa
EN	0015	003	Red Uchiza
EN	07	037	Nota de Crédito
EN	08	037	Nota de Débito
EN	0001	001	Peruvian
EN	0003	001	Italian
ES	0001	012	Visa
EN	0001	012	Visa
ES	0003	012	MasterCard
EN	0003	012	MasterCard
EN	0002	012	Diners Club
ES	0002	012	Diners Club
ES	2	022	Albania
EN	2	022	Albania
ES	3	022	Alemania
EN	3	022	Alemania
ES	4	022	Andorra
EN	4	022	Andorra
ES	5	022	Angola
EN	5	022	Angola
ES	6	022	Anguila
EN	6	022	Anguila
ES	7	022	Antártida
EN	7	022	Antártida
ES	8	022	Antigua y Barbuda
EN	8	022	Antigua y Barbuda
ES	9	022	Arabia Saudí
EN	9	022	Arabia Saudí
ES	11	022	Argentina
EN	11	022	Argentina
ES	12	022	Armenia
EN	12	022	Armenia
ES	13	022	Aruba
EN	13	022	Aruba
ES	14	022	Atlantic Ocean
EN	14	022	Atlantic Ocean
ES	15	022	Australia
EN	15	022	Australia
ES	16	022	Austria
EN	16	022	Austria
ES	17	022	Azerbaiyán
EN	17	022	Azerbaiyán
ES	10	022	Artic Ocean es
ES	18	022	Bahamas
EN	18	022	Bahamas
ES	19	022	Bahráin
EN	19	022	Bahráin
ES	20	022	Bangladesh
EN	20	022	Bangladesh
ES	21	022	Barbados
EN	21	022	Barbados
ES	22	022	Bélgica
EN	22	022	Bélgica
ES	23	022	Belice
EN	23	022	Belice
ES	24	022	Bermudas
EN	24	022	Bermudas
ES	25	022	Bielorussia
EN	25	022	Bielorussia
ES	26	022	Birmania; Myanmar
EN	26	022	Birmania; Myanmar
ES	27	022	Bolivia
EN	27	022	Bolivia
ES	28	022	Bosnia
EN	28	022	Bosnia
ES	29	022	Botsuana
EN	29	022	Botsuana
ES	30	022	Brasil
EN	30	022	Brasil
ES	31	022	Brunéi
EN	31	022	Brunéi
ES	32	022	Bulgaria
EN	32	022	Bulgaria
ES	33	022	Burundi
EN	33	022	Burundi
ES	34	022	Cabo Verde
EN	34	022	Cabo Verde
ES	35	022	Camboya
EN	35	022	Camboya
ES	36	022	Camerún
EN	36	022	Camerún
ES	37	022	Canadá
EN	37	022	Canadá
ES	38	022	Chad
EN	38	022	Chad
ES	39	022	Chile
EN	39	022	Chile
ES	40	022	China
EN	40	022	China
ES	41	022	Colombia
EN	41	022	Colombia
ES	42	022	Comoras
EN	42	022	Comoras
ES	43	022	Congo
EN	43	022	Congo
ES	44	022	Coral Sea Islands
EN	44	022	Coral Sea Islands
ES	45	022	Corea del Norte
EN	45	022	Corea del Norte
ES	46	022	Corea del Sur
EN	46	022	Corea del Sur
ES	47	022	Costa de Marfil
EN	47	022	Costa de Marfil
ES	48	022	Costa Rica
EN	48	022	Costa Rica
ES	49	022	Croacia
EN	49	022	Croacia
ES	50	022	Cuba
EN	50	022	Cuba
ES	51	022	Dinamarca
EN	51	022	Dinamarca
ES	52	022	Dominica
EN	52	022	Dominica
ES	53	022	Ecuador
EN	53	022	Ecuador
ES	54	022	Egipto
EN	54	022	Egipto
ES	55	022	El Salvador
EN	55	022	El Salvador
ES	56	022	El Vaticano
EN	56	022	El Vaticano
ES	57	022	Emiratos Árabes
EN	57	022	Emiratos Árabes
ES	58	022	Eritrea
EN	58	022	Eritrea
ES	59	022	Eslovaquia
EN	59	022	Eslovaquia
ES	60	022	Eslovenia
EN	60	022	Eslovenia
ES	61	022	España
EN	61	022	España
ES	62	022	Estados Unidos
EN	62	022	Estados Unidos
ES	63	022	Estonia
EN	63	022	Estonia
ES	64	022	Etiopía
EN	64	022	Etiopía
ES	65	022	Filipinas
EN	65	022	Filipinas
ES	66	022	Finlandia
EN	66	022	Finlandia
ES	67	022	Fiyi
EN	67	022	Fiyi
ES	68	022	Francia
EN	68	022	Francia
ES	69	022	Gabón
EN	69	022	Gabón
ES	70	022	Gambia
EN	70	022	Gambia
ES	71	022	Gaza Strip
EN	71	022	Gaza Strip
ES	72	022	Georgia
EN	72	022	Georgia
ES	73	022	Ghana
EN	73	022	Ghana
ES	74	022	Gibraltar
EN	74	022	Gibraltar
ES	75	022	Granada
EN	75	022	Granada
ES	76	022	Grecia
EN	76	022	Grecia
ES	77	022	Groenlandia
EN	77	022	Groenlandia
ES	78	022	Guam
EN	78	022	Guam
ES	79	022	Guatemala
EN	79	022	Guatemala
ES	80	022	Guernsey
EN	80	022	Guernsey
ES	81	022	Guinea
EN	81	022	Guinea
ES	82	022	Guinea Ecuatorial
EN	82	022	Guinea Ecuatorial
ES	83	022	Guinea-Bissau
EN	83	022	Guinea-Bissau
ES	84	022	Guyana
EN	84	022	Guyana
ES	85	022	Haití
EN	85	022	Haití
ES	86	022	Honduras
EN	86	022	Honduras
ES	87	022	Hong Kong
EN	87	022	Hong Kong
ES	88	022	Hungría
EN	88	022	Hungría
ES	89	022	India
EN	89	022	India
ES	90	022	Indian Ocean
EN	90	022	Indian Ocean
ES	91	022	Indonesia
EN	91	022	Indonesia
ES	92	022	Irán
EN	92	022	Irán
ES	93	022	Iraq
EN	93	022	Iraq
ES	94	022	Irlanda
EN	94	022	Irlanda
ES	95	022	Isla Bouvet
EN	95	022	Isla Bouvet
ES	96	022	Isla Christmas
EN	96	022	Isla Christmas
ES	97	022	Isla Norfolk
EN	97	022	Isla Norfolk
ES	98	022	Islandia
EN	98	022	Islandia
ES	99	022	Islas Caimán
EN	99	022	Islas Caimán
ES	100	022	Islas Cocos
EN	100	022	Islas Cocos
ES	101	022	Islas Cook
EN	101	022	Islas Cook
ES	102	022	Islas Feroe
EN	102	022	Islas Feroe
ES	103	022	Islas Georgia del Sur y Sandwich del Sur
EN	103	022	Islas Georgia del Sur y Sandwich del Sur
ES	104	022	Islas Heard y McDonald
EN	104	022	Islas Heard y McDonald
ES	105	022	Islas Malvinas
EN	105	022	Islas Malvinas
ES	106	022	Islas Marianas del Norte
EN	106	022	Islas Marianas del Norte
ES	107	022	Islas Marshall
EN	107	022	Islas Marshall
ES	108	022	Islas Pitcairn
EN	108	022	Islas Pitcairn
ES	109	022	Islas Salomón
EN	109	022	Islas Salomón
ES	110	022	Islas Turcas y Caicos
EN	110	022	Islas Turcas y Caicos
ES	111	022	Islas Vírgenes Americanas
EN	111	022	Islas Vírgenes Americanas
ES	112	022	Islas Vírgenes Británicas
EN	112	022	Islas Vírgenes Británicas
ES	113	022	Israel
EN	113	022	Israel
ES	114	022	Italia
EN	114	022	Italia
ES	115	022	Jamaica
EN	115	022	Jamaica
ES	116	022	Jan Mayen
EN	116	022	Jan Mayen
ES	117	022	Japón
EN	117	022	Japón
ES	118	022	Jersey
EN	118	022	Jersey
ES	119	022	Jordania
EN	119	022	Jordania
ES	120	022	Kazajistán
EN	120	022	Kazajistán
ES	121	022	Kenia
EN	121	022	Kenia
ES	122	022	Kirguizistán
EN	122	022	Kirguizistán
ES	123	022	Kiribati
EN	123	022	Kiribati
ES	124	022	Kuwait
EN	124	022	Kuwait
ES	125	022	Laos
EN	125	022	Laos
ES	126	022	Lesoto
EN	126	022	Lesoto
ES	127	022	Letonia
EN	127	022	Letonia
ES	128	022	Líbano
EN	128	022	Líbano
ES	129	022	Liberia
EN	129	022	Liberia
ES	130	022	Libia
EN	130	022	Libia
ES	131	022	Liechtenstein
EN	131	022	Liechtenstein
ES	132	022	Lituania
EN	132	022	Lituania
ES	133	022	Luxemburgo
EN	133	022	Luxemburgo
ES	134	022	Macao
EN	134	022	Macao
ES	135	022	Macedonia
EN	135	022	Macedonia
ES	136	022	Madagascar
EN	136	022	Madagascar
ES	137	022	Malasia
EN	137	022	Malasia
ES	138	022	Malaui
EN	138	022	Malaui
ES	139	022	Maldivas
EN	139	022	Maldivas
ES	140	022	Malí
EN	140	022	Malí
ES	141	022	Malta
EN	141	022	Malta
ES	142	022	Man, Isle of
EN	142	022	Man, Isle of
ES	143	022	Marruecos
EN	143	022	Marruecos
ES	144	022	Mauricio
EN	144	022	Mauricio
ES	145	022	Mauritania
EN	145	022	Mauritania
ES	146	022	Mayotte
EN	146	022	Mayotte
ES	147	022	México
EN	147	022	México
ES	148	022	Micronesia
EN	148	022	Micronesia
ES	149	022	Moldavia
EN	149	022	Moldavia
ES	150	022	Mónaco
EN	150	022	Mónaco
ES	151	022	Mongolia
EN	151	022	Mongolia
ES	152	022	Montenegro
EN	152	022	Montenegro
ES	153	022	Montserrat
EN	153	022	Montserrat
ES	154	022	Mozambique
EN	154	022	Mozambique
ES	155	022	Mundo
EN	155	022	Mundo
ES	156	022	Namibia
EN	156	022	Namibia
ES	157	022	Nauru
EN	157	022	Nauru
ES	158	022	Navassa Island
EN	158	022	Navassa Island
ES	159	022	Nepal
EN	159	022	Nepal
ES	160	022	Nicaragua
EN	160	022	Nicaragua
ES	161	022	Níger
EN	161	022	Níger
ES	162	022	Nigeria
EN	162	022	Nigeria
ES	163	022	Niue
EN	163	022	Niue
ES	164	022	Noruega
EN	164	022	Noruega
ES	165	022	Nueva Caledonia
EN	165	022	Nueva Caledonia
ES	166	022	Nueva Zelanda
EN	166	022	Nueva Zelanda
ES	167	022	Omán
EN	167	022	Omán
ES	168	022	Pacific Ocean
EN	168	022	Pacific Ocean
ES	169	022	Países Bajos
EN	169	022	Países Bajos
ES	170	022	Pakistán
EN	170	022	Pakistán
ES	171	022	Palaos
EN	171	022	Palaos
ES	172	022	Panamá
EN	172	022	Panamá
ES	173 	022	Papúa Nueva-Guinea
EN	173 	022	Papúa Nueva-Guinea
ES	174	022	Paraguay
EN	174	022	Paraguay
ES	175	022	Perú
EN	175	022	Perú
ES	176	022	Polinesia Francesa
EN	176	022	Polinesia Francesa
ES	177	022	Polonia
EN	177	022	Polonia
ES	178	022	Portugal
EN	178	022	Portugal
ES	179	022	Puerto Rico
EN	179	022	Puerto Rico
ES	180	022	Qatar
EN	180	022	Qatar
ES	181	022	Reino Unido
EN	181	022	Reino Unido
ES	182	022	República Centroafricana
EN	182	022	República Centroafricana
ES	183	022	República Checa
EN	183	022	República Checa
ES	184	022	República Democrática del Congo
EN	184	022	República Democrática del Congo
ES	185	022	República Dominicana
EN	185	022	República Dominicana
ES	186	022	Ruanda
EN	186	022	Ruanda
ES	187	022	Rumania
EN	187	022	Rumania
ES	188	022	Rusia
EN	188	022	Rusia
ES	189	022	Sáhara Occidental
EN	189	022	Sáhara Occidental
ES	190	022	Samoa
EN	190	022	Samoa
ES	191	022	Samoa Americana
EN	191	022	Samoa Americana
ES	192	022	San Cristóbal y Nieves
EN	192	022	San Cristóbal y Nieves
ES	193	022	San Marino
EN	193	022	San Marino
ES	194	022	San Pedro y Miquelón
EN	194	022	San Pedro y Miquelón
ES	195	022	San Vicente y las Granadinas
EN	195	022	San Vicente y las Granadinas
ES	196	022	Santa Helena
EN	196	022	Santa Helena
ES	197	022	Santa Lucía
EN	197	022	Santa Lucía
ES	198	022	Santo Tomé y Príncipe
EN	198	022	Santo Tomé y Príncipe
ES	199	022	Senegal
EN	199	022	Senegal
ES	200	022	Serbia
EN	200	022	Serbia
ES	201	022	Sierra Leona
EN	201	022	Sierra Leona
ES	202	022	Singapur
EN	202	022	Singapur
ES	203	022	Siria
EN	203	022	Siria
ES	204	022	Somalia
EN	204	022	Somalia
ES	205	022	Southern Ocean
EN	205	022	Southern Ocean
ES	206	022	Spratly Islands
EN	206	022	Spratly Islands
ES	207	022	Sri Lanka
EN	207	022	Sri Lanka
ES	208	022	Suazilandia
EN	208	022	Suazilandia
ES	209	022	Sudáfrica
EN	209	022	Sudáfrica
ES	210	022	Sudán
EN	210	022	Sudán
ES	211	022	Suecia
EN	211	022	Suecia
ES	212	022	Suiza
EN	212	022	Suiza
ES	213	022	Surinam
EN	213	022	Surinam
ES	214	022	Tailandia
EN	214	022	Tailandia
ES	215	022	Taiwán
EN	215	022	Taiwán
ES	216	022	Tanzania
EN	216	022	Tanzania
ES	217	022	Tayikistán
EN	217	022	Tayikistán
ES	218	022	Timor Oriental
EN	218	022	Timor Oriental
ES	219	022	Togo
EN	219	022	Togo
ES	220	022	Tonga
EN	220	022	Tonga
ES	221	022	Trinidad y Tobago
EN	221	022	Trinidad y Tobago
ES	222	022	Túnez
EN	222	022	Túnez
ES	223	022	Turkmenistán
EN	223	022	Turkmenistán
ES	224	022	Turquía
EN	224	022	Turquía
ES	225	022	Ucrania
EN	225	022	Ucrania
ES	226	022	Uganda
EN	226	022	Uganda
ES	227	022	Unión Europea
EN	227	022	Unión Europea
ES	228	022	Uruguay
EN	228	022	Uruguay
ES	229	022	Uzbekistán
EN	229	022	Uzbekistán
ES	230	022	Vanuatu
EN	230	022	Vanuatu
ES	231	022	Venezuela
EN	231	022	Venezuela
ES	232	022	Vietnam
EN	232	022	Vietnam
ES	233	022	Wake Island
EN	233	022	Wake Island
ES	234	022	Wallis y Futuna
EN	234	022	Wallis y Futuna
ES	235	022	West Bank
EN	235	022	West Bank
ES	236	022	Yemen
EN	236	022	Yemen
ES	237	022	Zambia
EN	237	022	Zambia
ES	238	022	Zimbabue
EN	238	022	Zimbabue
EN	10	022	Artic Ocean s
EN	1	022	Afganistán s
ES	1	022	Afganistán es
\.


--
-- TOC entry 2615 (class 0 OID 24863)
-- Dependencies: 231
-- Data for Name: idioma_tabla_sis; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY idioma_tabla_sis (tas_id, id_id, tas_desc) FROM stdin;
0010	ES	Tablas de Tablas
0001	ES	Tipo de Servicios
0002	ES	Tarifas
0010	EN	Tablas de Tablas
0001	EN	Tipo de Servicios
0002	EN	Tarifas
0003	EN	SEWE
0003	ES	SEWE
0004	EN	Years
0004	ES	Años
\.


--
-- TOC entry 2616 (class 0 OID 24866)
-- Dependencies: 232
-- Data for Name: menu_sucursal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY menu_sucursal (me_id, su_id) FROM stdin;
00004004	00000001
00004000	00000001
00004005	00000001
00004006	00000001
00004009	00000001
00004008	00000001
00004007	00000001
00004010	00000001
00004003	00000001
00004002	00000001
00004001	00000001
00003008	00000001
00003000	00000001
00002000	00000001
00001000	00000001
00001001	00000001
00004004	00000003
00001002	00000001
00001004	00000001
00001005	00000001
00001006	00000001
00004000	00000003
00004005	00000003
00002001	00000001
00002003	00000001
00002004	00000001
00003001	00000001
00003002	00000001
00003003	00000001
00004006	00000003
00004009	00000003
00004008	00000003
00004007	00000003
00003006	00000001
00003007	00000001
00004010	00000003
00004003	00000003
00004002	00000003
00004001	00000003
00002005	00000001
00002006	00000001
00002007	00000001
00002008	00000001
00003008	00000003
00003000	00000003
00002000	00000003
00004004	00000002
00004000	00000002
00004005	00000002
00004006	00000002
00004009	00000002
00004008	00000002
00004007	00000002
00004010	00000002
00004003	00000002
00004002	00000002
00004001	00000002
00003008	00000002
00003000	00000002
00002000	00000002
00001000	00000002
00001001	00000002
00001002	00000002
00001004	00000002
00001005	00000002
00001006	00000002
00002001	00000002
00002003	00000002
00002004	00000002
00003001	00000002
00003002	00000002
00003003	00000002
00003006	00000002
00003007	00000002
00002005	00000002
00002006	00000002
00002007	00000002
00002008	00000002
00001000	00000003
00001001	00000003
00001002	00000003
00001004	00000003
00001005	00000003
00001006	00000003
00002001	00000003
00002003	00000003
00002004	00000003
00003001	00000003
00003002	00000003
00003003	00000003
00003006	00000003
00003007	00000003
00002005	00000003
00002009	00000001
00002006	00000003
00002007	00000003
00002008	00000003
00002009	00000002
00002009	00000003
00003009	00000001
00003009	00000002
00003009	00000003
00004011	00000001
00004011	00000002
00004011	00000003
00004012	00000001
00004012	00000002
00004012	00000003
00002010	00000001
00002010	00000002
00002010	00000003
\.


--
-- TOC entry 2617 (class 0 OID 24869)
-- Dependencies: 233
-- Data for Name: tabla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tabla (tba_id) FROM stdin;
001
003
010
011
014
015
016
020
021
022
023
025
026
033
037
042
043
002
012
\.


--
-- TOC entry 2618 (class 0 OID 24872)
-- Dependencies: 234
-- Data for Name: tabla_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tabla_detalle (tad_id, tba_id) FROM stdin;
03	025
04	025
05	025
06	025
07	025
08	025
09	025
10	025
11	025
12	025
13	025
14	025
15	025
16	025
17	025
18	025
19	025
20	025
21	025
22	025
90	025
91	025
92	025
93	025
94	025
00	026
01	026
02	026
03	026
04	026
05	026
06	026
07	026
08	026
09	026
0001	010
0006	014
0010	014
0007	014
001	042
002	042
003	042
004	042
005	042
006	042
001	043
002	043
003	043
004	043
V	033
M	033
D	033
A	033
01	037
02	037
03	037
C001	002
0003	010
0004	010
0005	010
0013	010
0079	010
0213	010
0214	010
0215	010
0216	010
0217	010
0218	010
0219	010
0220	010
0221	010
0226	010
0227	010
0228	010
0229	010
0230	010
0231	010
0232	010
0233	010
0234	010
0235	010
0236	010
0237	010
0238	010
0239	010
0240	010
0241	010
0242	010
0244	010
0246	010
0247	010
0248	010
0249	010
0250	010
0251	010
0252	010
0253	010
0003	001
0001	003
0002	003
0003	003
0002	001
0001	001
0005	001
0001	014
0002	014
0002	010
0004	014
0001	015
0002	015
0001	016
0002	016
0003	016
0004	016
0004	003
0005	003
0006	003
0007	003
0008	003
0009	003
0010	003
0011	003
0012	003
0013	003
0014	003
0006	001
0007	001
0008	001
0009	001
0010	001
0011	001
0012	001
0013	001
0001	020
0002	020
0003	020
0004	020
0014	001
0001	021
0002	021
0321	010
01	023
02	023
03	023
04	023
05	023
06	023
07	023
99	023
00	025
01	025
02	025
01	011
02	011
03	011
07	011
08	011
10	011
11	011
13	011
20	011
21	011
22	011
0005	014
0015	001
0016	001
0017	001
0018	001
0019	001
0020	001
0022	001
0023	001
23	011
0005	016
10	026
11	026
0007	010
0008	010
0009	010
0011	010
0012	010
0014	010
0015	010
0016	010
0017	010
0018	010
0019	010
0020	010
0021	010
0022	010
0023	010
0024	010
0025	010
0026	010
0027	010
0029	010
0030	010
0031	010
0033	010
0035	010
0036	010
0037	010
0038	010
0039	010
0040	010
0041	010
0042	010
0044	010
0045	010
0046	010
0047	010
0048	010
0049	010
0051	010
0052	010
0053	010
0054	010
0055	010
0056	010
0058	010
0059	010
0060	010
0061	010
0063	010
0064	010
0066	010
0067	010
0068	010
0069	010
0071	010
0073	010
0075	010
0076	010
0080	010
0081	010
0082	010
0083	010
0085	010
0086	010
0087	010
0088	010
0090	010
0091	010
0092	010
0093	010
0095	010
0099	010
0100	010
0101	010
0102	010
0103	010
0104	010
0105	010
0106	010
0107	010
0108	010
0110	010
0111	010
0112	010
0113	010
0114	010
0115	010
0116	010
0117	010
0120	010
0121	010
0122	010
0123	010
0126	010
0127	010
0128	010
0129	010
0130	010
0131	010
0135	010
0136	010
0137	010
0138	010
0139	010
0140	010
0145	010
0146	010
0147	010
0149	010
0150	010
0152	010
0153	010
0154	010
0155	010
0156	010
0157	010
0159	010
0160	010
0161	010
0162	010
0163	010
0164	010
0165	010
0167	010
0168	010
0169	010
0172	010
0173	010
0174	010
0175	010
0176	010
0178	010
0179	010
0180	010
0182	010
0183	010
0185	010
0186	010
0187	010
0188	010
0189	010
0194	010
0195	010
0196	010
0198	010
0199	010
0200	010
0201	010
0202	010
0204	010
0206	010
0207	010
0208	010
0209	010
0210	010
0211	010
0212	010
0254	010
0255	010
0256	010
0258	010
0259	010
0261	010
0262	010
0263	010
0268	010
0269	010
0271	010
0272	010
0273	010
0275	010
0278	010
0279	010
0280	010
0282	010
0283	010
0285	010
0286	010
0287	010
0290	010
0291	010
0293	010
0294	010
0295	010
0296	010
0297	010
0299	010
0301	010
0302	010
0303	010
0305	010
0306	010
0308	010
0309	010
0310	010
0311	010
0312	010
0314	010
0315	010
0316	010
0317	010
0318	010
0319	010
0320	010
0078	010
0015	003
07	037
08	037
0001	012
0002	012
0003	012
1	022
2	022
3	022
4	022
5	022
6	022
7	022
8	022
9	022
10	022
11	022
12	022
13	022
14	022
15	022
16	022
17	022
18	022
19	022
20	022
21	022
22	022
23	022
24	022
25	022
26	022
27	022
28	022
29	022
30	022
31	022
32	022
33	022
34	022
35	022
36	022
37	022
38	022
39	022
40	022
41	022
42	022
43	022
44	022
45	022
46	022
47	022
48	022
49	022
50	022
51	022
52	022
53	022
54	022
55	022
56	022
57	022
58	022
59	022
60	022
61	022
62	022
63	022
64	022
65	022
66	022
67	022
68	022
69	022
70	022
71	022
72	022
73	022
74	022
75	022
76	022
77	022
78	022
79	022
80	022
81	022
82	022
83	022
84	022
85	022
86	022
87	022
88	022
89	022
90	022
91	022
92	022
93	022
94	022
95	022
96	022
97	022
98	022
99	022
100	022
101	022
102	022
103	022
104	022
105	022
106	022
107	022
108	022
109	022
110	022
111	022
112	022
113	022
114	022
115	022
116	022
117	022
118	022
119	022
120	022
121	022
122	022
123	022
124	022
125	022
126	022
127	022
128	022
129	022
130	022
131	022
132	022
133	022
134	022
135	022
136	022
137	022
138	022
139	022
140	022
141	022
142	022
143	022
144	022
145	022
146	022
147	022
148	022
149	022
150	022
151	022
152	022
153	022
154	022
155	022
156	022
157	022
158	022
159	022
160	022
161	022
162	022
163	022
164	022
165	022
166	022
167	022
168	022
169	022
170	022
171	022
172	022
173 	022
174	022
175	022
176	022
177	022
178	022
179	022
180	022
181	022
182	022
183	022
184	022
185	022
186	022
187	022
188	022
189	022
190	022
191	022
192	022
193	022
194	022
195	022
196	022
197	022
198	022
199	022
200	022
201	022
202	022
203	022
204	022
205	022
206	022
207	022
208	022
209	022
210	022
211	022
212	022
213	022
214	022
215	022
216	022
217	022
218	022
219	022
220	022
221	022
222	022
223	022
224	022
225	022
226	022
227	022
228	022
229	022
230	022
231	022
232	022
233	022
234	022
235	022
236	022
237	022
238	022
\.


--
-- TOC entry 2619 (class 0 OID 24875)
-- Dependencies: 235
-- Data for Name: tabla_sistema; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tabla_sistema (tas_id, tas_orden) FROM stdin;
0001	1
0002	2
0003	3
0010	5
0004	4
\.


--
-- TOC entry 2191 (class 2606 OID 24976)
-- Dependencies: 161 161 161 161
-- Name: pk_cms_categoria_articulo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_categoria_articulo
    ADD CONSTRAINT pk_cms_categoria_articulo PRIMARY KEY (cat_id, art_id, su_id);


--
-- TOC entry 2195 (class 2606 OID 24978)
-- Dependencies: 162 162 162 162 162
-- Name: pk_cms_imagenes_articulo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cms_imagenes_articulo
    ADD CONSTRAINT pk_cms_imagenes_articulo PRIMARY KEY (im_id, art_id, su_id, cat_id);


--
-- TOC entry 2200 (class 2606 OID 24980)
-- Dependencies: 163 163
-- Name: pk_ht_cms_articulo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_articulo
    ADD CONSTRAINT pk_ht_cms_articulo PRIMARY KEY (art_id);


--
-- TOC entry 2203 (class 2606 OID 24982)
-- Dependencies: 164 164
-- Name: pk_ht_cms_categoria; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_categoria
    ADD CONSTRAINT pk_ht_cms_categoria PRIMARY KEY (cat_id);


--
-- TOC entry 2206 (class 2606 OID 24984)
-- Dependencies: 165 165
-- Name: pk_ht_cms_descripciones; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_descripciones
    ADD CONSTRAINT pk_ht_cms_descripciones PRIMARY KEY (des_id);


--
-- TOC entry 2209 (class 2606 OID 24986)
-- Dependencies: 166 166
-- Name: pk_ht_cms_idioma; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma
    ADD CONSTRAINT pk_ht_cms_idioma PRIMARY KEY (id_id);


--
-- TOC entry 2212 (class 2606 OID 24988)
-- Dependencies: 167 167 167 167 167
-- Name: pk_ht_cms_idioma_art_cont; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_art_cont
    ADD CONSTRAINT pk_ht_cms_idioma_art_cont PRIMARY KEY (art_id, id_id, su_id, cat_id);


--
-- TOC entry 2214 (class 2606 OID 24990)
-- Dependencies: 168 168 168
-- Name: pk_ht_cms_idioma_articulo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_articulo
    ADD CONSTRAINT pk_ht_cms_idioma_articulo PRIMARY KEY (id_id, art_id);


--
-- TOC entry 2219 (class 2606 OID 24992)
-- Dependencies: 169 169 169
-- Name: pk_ht_cms_idioma_categoria; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_categoria
    ADD CONSTRAINT pk_ht_cms_idioma_categoria PRIMARY KEY (id_id, cat_id);


--
-- TOC entry 2222 (class 2606 OID 24994)
-- Dependencies: 170 170 170
-- Name: pk_ht_cms_idioma_desc; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_desc
    ADD CONSTRAINT pk_ht_cms_idioma_desc PRIMARY KEY (id_id, des_id);


--
-- TOC entry 2225 (class 2606 OID 24996)
-- Dependencies: 171 171 171
-- Name: pk_ht_cms_idioma_menu_interno; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_menu_interno
    ADD CONSTRAINT pk_ht_cms_idioma_menu_interno PRIMARY KEY (cat_id, id_id);


--
-- TOC entry 2228 (class 2606 OID 24998)
-- Dependencies: 172 172 172
-- Name: pk_ht_cms_idioma_mes; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_mes
    ADD CONSTRAINT pk_ht_cms_idioma_mes PRIMARY KEY (id_id, mes_id);


--
-- TOC entry 2231 (class 2606 OID 25000)
-- Dependencies: 173 173 173 173
-- Name: pk_ht_cms_idioma_promocion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_promocion
    ADD CONSTRAINT pk_ht_cms_idioma_promocion PRIMARY KEY (pro_id, an_id, id_id);


--
-- TOC entry 2234 (class 2606 OID 25002)
-- Dependencies: 174 174 174 174 174 174
-- Name: pk_ht_cms_idioma_promsucursal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_promsucursal
    ADD CONSTRAINT pk_ht_cms_idioma_promsucursal PRIMARY KEY (id_id, pro_id, an_id, tih_id, su_id);


--
-- TOC entry 2237 (class 2606 OID 25004)
-- Dependencies: 175 175 175
-- Name: pk_ht_cms_idioma_public; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_idioma_public
    ADD CONSTRAINT pk_ht_cms_idioma_public PRIMARY KEY (pub_id, id_id);


--
-- TOC entry 2240 (class 2606 OID 25006)
-- Dependencies: 176 176
-- Name: pk_ht_cms_imagenes; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_imagenes
    ADD CONSTRAINT pk_ht_cms_imagenes PRIMARY KEY (im_id);


--
-- TOC entry 2243 (class 2606 OID 25008)
-- Dependencies: 177 177
-- Name: pk_ht_cms_menu_interno; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_menu_interno
    ADD CONSTRAINT pk_ht_cms_menu_interno PRIMARY KEY (cat_id);


--
-- TOC entry 2246 (class 2606 OID 25010)
-- Dependencies: 178 178
-- Name: pk_ht_cms_meses; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_meses
    ADD CONSTRAINT pk_ht_cms_meses PRIMARY KEY (mes_id);


--
-- TOC entry 2249 (class 2606 OID 25012)
-- Dependencies: 179 179 179 179 179 179
-- Name: pk_ht_cms_promo_imagen; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_promo_imagen
    ADD CONSTRAINT pk_ht_cms_promo_imagen PRIMARY KEY (pro_id, an_id, tih_id, su_id, im_id);


--
-- TOC entry 2252 (class 2606 OID 25014)
-- Dependencies: 180 180 180
-- Name: pk_ht_cms_promocion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_promocion
    ADD CONSTRAINT pk_ht_cms_promocion PRIMARY KEY (pro_id, an_id);


--
-- TOC entry 2255 (class 2606 OID 25016)
-- Dependencies: 181 181 181 181 181
-- Name: pk_ht_cms_promocion_sucursal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_promocion_sucursal
    ADD CONSTRAINT pk_ht_cms_promocion_sucursal PRIMARY KEY (pro_id, an_id, tih_id, su_id);


--
-- TOC entry 2258 (class 2606 OID 25018)
-- Dependencies: 182 182 182
-- Name: pk_ht_cms_public_imagen; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_public_imagen
    ADD CONSTRAINT pk_ht_cms_public_imagen PRIMARY KEY (pub_id, im_id);


--
-- TOC entry 2261 (class 2606 OID 25020)
-- Dependencies: 183 183
-- Name: pk_ht_cms_publicacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_publicacion
    ADD CONSTRAINT pk_ht_cms_publicacion PRIMARY KEY (pub_id);


--
-- TOC entry 2263 (class 2606 OID 25022)
-- Dependencies: 184 184 184
-- Name: pk_ht_cms_sucursal_categoria; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_sucursal_categoria
    ADD CONSTRAINT pk_ht_cms_sucursal_categoria PRIMARY KEY (su_id, cat_id);


--
-- TOC entry 2266 (class 2606 OID 25024)
-- Dependencies: 185 185 185 185
-- Name: pk_ht_cms_sucursal_menu_interno; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_cms_sucursal_menu_interno
    ADD CONSTRAINT pk_ht_cms_sucursal_menu_interno PRIMARY KEY (cat_id, su_id, tih_id);


--
-- TOC entry 2272 (class 2606 OID 25026)
-- Dependencies: 187 187
-- Name: pk_ht_hab_accesorios; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_accesorios
    ADD CONSTRAINT pk_ht_hab_accesorios PRIMARY KEY (ac_id);


--
-- TOC entry 2279 (class 2606 OID 25028)
-- Dependencies: 189 189 189 189
-- Name: pk_ht_hab_habitacion_accesorio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_habitacion_accesorios
    ADD CONSTRAINT pk_ht_hab_habitacion_accesorio PRIMARY KEY (hb_id, ac_id, hba_id);


--
-- TOC entry 2283 (class 2606 OID 25030)
-- Dependencies: 190 190 190
-- Name: pk_ht_hab_habitacion_imagen; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_habitacion_imagen
    ADD CONSTRAINT pk_ht_hab_habitacion_imagen PRIMARY KEY (hb_id, im_id);


--
-- TOC entry 2293 (class 2606 OID 25032)
-- Dependencies: 193 193 193
-- Name: pk_ht_hab_idioma_accesorios; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_accesorios
    ADD CONSTRAINT pk_ht_hab_idioma_accesorios PRIMARY KEY (ac_id, id_id);


--
-- TOC entry 2296 (class 2606 OID 25034)
-- Dependencies: 194 194 194
-- Name: pk_ht_hab_idioma_habitacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_habitacion
    ADD CONSTRAINT pk_ht_hab_idioma_habitacion PRIMARY KEY (hb_id, id_id);


--
-- TOC entry 2308 (class 2606 OID 25036)
-- Dependencies: 198 198 198
-- Name: pk_ht_hab_idioma_tipo_habitaci; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_tipo_habitacion
    ADD CONSTRAINT pk_ht_hab_idioma_tipo_habitaci PRIMARY KEY (tih_id, id_id);


--
-- TOC entry 2317 (class 2606 OID 25038)
-- Dependencies: 201 201 201
-- Name: pk_ht_hab_sucursal_tipo_habita; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_sucursal_tipo_habitacion
    ADD CONSTRAINT pk_ht_hab_sucursal_tipo_habita PRIMARY KEY (tih_id, su_id);


--
-- TOC entry 2322 (class 2606 OID 25040)
-- Dependencies: 202 202
-- Name: pk_ht_hab_tarifa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_tarifa
    ADD CONSTRAINT pk_ht_hab_tarifa PRIMARY KEY (ta_id);


--
-- TOC entry 2325 (class 2606 OID 25042)
-- Dependencies: 203 203
-- Name: pk_ht_hab_tipo_habitacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_tipo_habitacion
    ADD CONSTRAINT pk_ht_hab_tipo_habitacion PRIMARY KEY (tih_id);


--
-- TOC entry 2269 (class 2606 OID 25044)
-- Dependencies: 186 186 186 186
-- Name: pk_ht_hb_accesorio_sucursal_ti; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_accesorio_sucursal_tiphab
    ADD CONSTRAINT pk_ht_hb_accesorio_sucursal_ti PRIMARY KEY (ac_id, tih_id, su_id);


--
-- TOC entry 2275 (class 2606 OID 25046)
-- Dependencies: 188 188
-- Name: pk_ht_hb_habitacion; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_habitacion
    ADD CONSTRAINT pk_ht_hb_habitacion PRIMARY KEY (hb_id);


--
-- TOC entry 2288 (class 2606 OID 25048)
-- Dependencies: 191 191 191 191 191
-- Name: pk_ht_hb_habitacion_tarifa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_habitacion_tarifa
    ADD CONSTRAINT pk_ht_hb_habitacion_tarifa PRIMARY KEY (tih_id, su_id, hb_capacidad, ta_id);


--
-- TOC entry 2291 (class 2606 OID 25050)
-- Dependencies: 192 192
-- Name: pk_ht_hb_hotel; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_hotel
    ADD CONSTRAINT pk_ht_hb_hotel PRIMARY KEY (ho_id);


--
-- TOC entry 2300 (class 2606 OID 25052)
-- Dependencies: 195 195 195
-- Name: pk_ht_hb_idioma_piso; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_piso
    ADD CONSTRAINT pk_ht_hb_idioma_piso PRIMARY KEY (pi_id, id_id);


--
-- TOC entry 2303 (class 2606 OID 25054)
-- Dependencies: 196 196 196 196
-- Name: pk_ht_hb_idioma_suc_tiphab; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_suc_tiphab
    ADD CONSTRAINT pk_ht_hb_idioma_suc_tiphab PRIMARY KEY (tih_id, su_id, id_id);


--
-- TOC entry 2306 (class 2606 OID 25056)
-- Dependencies: 197 197 197
-- Name: pk_ht_hb_idioma_tarifa; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_idioma_tarifa
    ADD CONSTRAINT pk_ht_hb_idioma_tarifa PRIMARY KEY (ta_id, id_id);


--
-- TOC entry 2312 (class 2606 OID 25058)
-- Dependencies: 199 199
-- Name: pk_ht_hb_piso; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_piso
    ADD CONSTRAINT pk_ht_hb_piso PRIMARY KEY (pi_id);


--
-- TOC entry 2315 (class 2606 OID 25060)
-- Dependencies: 200 200
-- Name: pk_ht_hb_sucursal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_sucursal
    ADD CONSTRAINT pk_ht_hb_sucursal PRIMARY KEY (su_id);


--
-- TOC entry 2331 (class 2606 OID 25062)
-- Dependencies: 205 205
-- Name: pk_ht_res_anio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_anio
    ADD CONSTRAINT pk_ht_res_anio PRIMARY KEY (an_id);


--
-- TOC entry 2334 (class 2606 OID 25064)
-- Dependencies: 206 206
-- Name: pk_ht_res_bonos; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_bonos
    ADD CONSTRAINT pk_ht_res_bonos PRIMARY KEY (bo_id);


--
-- TOC entry 2337 (class 2606 OID 25066)
-- Dependencies: 207 207
-- Name: pk_ht_res_cambio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_cambio
    ADD CONSTRAINT pk_ht_res_cambio PRIMARY KEY (cam_fecha);


--
-- TOC entry 2340 (class 2606 OID 25068)
-- Dependencies: 208 208 208
-- Name: pk_ht_res_idioma_anio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_idioma_anio
    ADD CONSTRAINT pk_ht_res_idioma_anio PRIMARY KEY (id_id, an_id);


--
-- TOC entry 2342 (class 2606 OID 25070)
-- Dependencies: 209 209 209 209 209
-- Name: pk_ht_res_kardex; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_kardex
    ADD CONSTRAINT pk_ht_res_kardex PRIMARY KEY (hb_id, ka_fecha, su_id, tih_id);


--
-- TOC entry 2345 (class 2606 OID 25072)
-- Dependencies: 210 210
-- Name: pk_ht_res_reserva; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_reserva
    ADD CONSTRAINT pk_ht_res_reserva PRIMARY KEY (res_id);


--
-- TOC entry 2349 (class 2606 OID 25074)
-- Dependencies: 211 211 211
-- Name: pk_ht_res_reserva_detalle; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT pk_ht_res_reserva_detalle PRIMARY KEY (red_item, res_id);


--
-- TOC entry 2352 (class 2606 OID 25076)
-- Dependencies: 212 212
-- Name: pk_ht_seg_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_cliente
    ADD CONSTRAINT pk_ht_seg_cliente PRIMARY KEY (cl_id);


--
-- TOC entry 2354 (class 2606 OID 25078)
-- Dependencies: 213 213 213
-- Name: pk_ht_seg_idioma_menu; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_idioma_menu
    ADD CONSTRAINT pk_ht_seg_idioma_menu PRIMARY KEY (id_id, me_id);


--
-- TOC entry 2359 (class 2606 OID 25080)
-- Dependencies: 214 214 214
-- Name: pk_ht_seg_idioma_perfil; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_idioma_perfil
    ADD CONSTRAINT pk_ht_seg_idioma_perfil PRIMARY KEY (id_id, per_id);


--
-- TOC entry 2362 (class 2606 OID 25082)
-- Dependencies: 215 215
-- Name: pk_ht_seg_menu; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_menu
    ADD CONSTRAINT pk_ht_seg_menu PRIMARY KEY (me_id);


--
-- TOC entry 2366 (class 2606 OID 25084)
-- Dependencies: 216 216
-- Name: pk_ht_seg_modulo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_modulo
    ADD CONSTRAINT pk_ht_seg_modulo PRIMARY KEY (mo_id);


--
-- TOC entry 2369 (class 2606 OID 25086)
-- Dependencies: 217 217
-- Name: pk_ht_seg_perfil; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_perfil
    ADD CONSTRAINT pk_ht_seg_perfil PRIMARY KEY (per_id);


--
-- TOC entry 2372 (class 2606 OID 25088)
-- Dependencies: 218 218 218
-- Name: pk_ht_seg_perfil_menu; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_perfil_menu
    ADD CONSTRAINT pk_ht_seg_perfil_menu PRIMARY KEY (me_id, per_id);


--
-- TOC entry 2375 (class 2606 OID 25090)
-- Dependencies: 219 219
-- Name: pk_ht_seg_persona; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_persona
    ADD CONSTRAINT pk_ht_seg_persona PRIMARY KEY (pe_id);


--
-- TOC entry 2377 (class 2606 OID 25092)
-- Dependencies: 220 220 220
-- Name: pk_ht_seg_persona_hotel; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_persona_hotel
    ADD CONSTRAINT pk_ht_seg_persona_hotel PRIMARY KEY (pe_id, ho_id);


--
-- TOC entry 2379 (class 2606 OID 25094)
-- Dependencies: 221 221
-- Name: pk_ht_seg_ubigeo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_ubigeo
    ADD CONSTRAINT pk_ht_seg_ubigeo PRIMARY KEY (ub_id);


--
-- TOC entry 2383 (class 2606 OID 25096)
-- Dependencies: 222 222 222 222
-- Name: pk_ht_seg_usuario; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_usuario
    ADD CONSTRAINT pk_ht_seg_usuario PRIMARY KEY (us_id, per_id, pe_id);


--
-- TOC entry 2386 (class 2606 OID 25098)
-- Dependencies: 223 223 223 223 223 223
-- Name: pk_ht_seg_usuario_menu; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_seg_usuario_menu
    ADD CONSTRAINT pk_ht_seg_usuario_menu PRIMARY KEY (us_id, per_id, pe_id, su_id, me_id);


--
-- TOC entry 2397 (class 2606 OID 25100)
-- Dependencies: 226 226
-- Name: pk_ht_ser_servicio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_ser_servicio
    ADD CONSTRAINT pk_ht_ser_servicio PRIMARY KEY (ser_id);


--
-- TOC entry 2405 (class 2606 OID 25102)
-- Dependencies: 228 228
-- Name: pk_ht_ser_tipo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_ser_tipo
    ADD CONSTRAINT pk_ht_ser_tipo PRIMARY KEY (set_id);


--
-- TOC entry 2388 (class 2606 OID 25104)
-- Dependencies: 224 224 224
-- Name: pk_ht_srv_idioma_servicio; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_ser_idioma_servicio
    ADD CONSTRAINT pk_ht_srv_idioma_servicio PRIMARY KEY (ser_id, id_id);


--
-- TOC entry 2392 (class 2606 OID 25106)
-- Dependencies: 225 225 225
-- Name: pk_ht_srv_idioma_tipo; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_ser_idioma_tipo
    ADD CONSTRAINT pk_ht_srv_idioma_tipo PRIMARY KEY (set_id, id_id);


--
-- TOC entry 2400 (class 2606 OID 25108)
-- Dependencies: 227 227 227
-- Name: pk_ht_srv_servicio_sucursal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_ser_servicio_sucursal
    ADD CONSTRAINT pk_ht_srv_servicio_sucursal PRIMARY KEY (ser_id, su_id);


--
-- TOC entry 2328 (class 2606 OID 25110)
-- Dependencies: 204 204 204
-- Name: pk_ht_tipo_habitacion_imagen; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ht_hb_tipo_habitacion_imagen
    ADD CONSTRAINT pk_ht_tipo_habitacion_imagen PRIMARY KEY (tih_id, im_id);


--
-- TOC entry 2408 (class 2606 OID 25112)
-- Dependencies: 229 229 229
-- Name: pk_idioma_tabla; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY idioma_tabla
    ADD CONSTRAINT pk_idioma_tabla PRIMARY KEY (id_id, tba_id);


--
-- TOC entry 2411 (class 2606 OID 25114)
-- Dependencies: 230 230 230 230
-- Name: pk_idioma_tabla_det; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY idioma_tabla_det
    ADD CONSTRAINT pk_idioma_tabla_det PRIMARY KEY (id_id, tad_id, tba_id);


--
-- TOC entry 2414 (class 2606 OID 25116)
-- Dependencies: 231 231 231
-- Name: pk_idioma_tabla_sis; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY idioma_tabla_sis
    ADD CONSTRAINT pk_idioma_tabla_sis PRIMARY KEY (tas_id, id_id);


--
-- TOC entry 2416 (class 2606 OID 25118)
-- Dependencies: 232 232 232
-- Name: pk_menu_sucursal; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY menu_sucursal
    ADD CONSTRAINT pk_menu_sucursal PRIMARY KEY (me_id, su_id);


--
-- TOC entry 2418 (class 2606 OID 25120)
-- Dependencies: 233 233
-- Name: pk_tabla; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tabla
    ADD CONSTRAINT pk_tabla PRIMARY KEY (tba_id);


--
-- TOC entry 2422 (class 2606 OID 25122)
-- Dependencies: 234 234 234
-- Name: pk_tabla_detalle; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tabla_detalle
    ADD CONSTRAINT pk_tabla_detalle PRIMARY KEY (tad_id, tba_id);


--
-- TOC entry 2426 (class 2606 OID 25124)
-- Dependencies: 235 235
-- Name: pk_tabla_sistema; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tabla_sistema
    ADD CONSTRAINT pk_tabla_sistema PRIMARY KEY (tas_id);


--
-- TOC entry 2420 (class 1259 OID 25125)
-- Dependencies: 234 234
-- Name: detalle_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX detalle_pk ON tabla_detalle USING btree (tad_id, tba_id);


--
-- TOC entry 2198 (class 1259 OID 25126)
-- Dependencies: 163
-- Name: ht_cms_articulo_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_cms_articulo_pk ON ht_cms_articulo USING btree (art_id);


--
-- TOC entry 2201 (class 1259 OID 25127)
-- Dependencies: 164
-- Name: ht_cms_categoria_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_cms_categoria_pk ON ht_cms_categoria USING btree (cat_id);


--
-- TOC entry 2207 (class 1259 OID 25128)
-- Dependencies: 166
-- Name: ht_cms_idioma_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_cms_idioma_pk ON ht_cms_idioma USING btree (id_id);


--
-- TOC entry 2238 (class 1259 OID 25129)
-- Dependencies: 176
-- Name: ht_cms_imagenes_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_cms_imagenes_pk ON ht_cms_imagenes USING btree (im_id);


--
-- TOC entry 2270 (class 1259 OID 25130)
-- Dependencies: 187
-- Name: ht_hab_accesorios_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hab_accesorios_pk ON ht_hb_accesorios USING btree (ac_id);


--
-- TOC entry 2320 (class 1259 OID 25131)
-- Dependencies: 202
-- Name: ht_hab_tarifa_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hab_tarifa_pk ON ht_hb_tarifa USING btree (ta_id);


--
-- TOC entry 2323 (class 1259 OID 25132)
-- Dependencies: 203
-- Name: ht_hab_tipo_habitacion_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hab_tipo_habitacion_pk ON ht_hb_tipo_habitacion USING btree (tih_id);


--
-- TOC entry 2273 (class 1259 OID 25133)
-- Dependencies: 188
-- Name: ht_hb_habitaciones_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hb_habitaciones_pk ON ht_hb_habitacion USING btree (hb_id);


--
-- TOC entry 2289 (class 1259 OID 25134)
-- Dependencies: 192
-- Name: ht_hb_hotel_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hb_hotel_pk ON ht_hb_hotel USING btree (ho_id);


--
-- TOC entry 2313 (class 1259 OID 25135)
-- Dependencies: 200
-- Name: ht_hb_sucursal_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_hb_sucursal_pk ON ht_hb_sucursal USING btree (su_id);


--
-- TOC entry 2332 (class 1259 OID 25136)
-- Dependencies: 206
-- Name: ht_res_bonos_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_res_bonos_pk ON ht_res_bonos USING btree (bo_id);


--
-- TOC entry 2350 (class 1259 OID 25137)
-- Dependencies: 212
-- Name: ht_seg_cliente_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_cliente_pk ON ht_seg_cliente USING btree (cl_id);


--
-- TOC entry 2360 (class 1259 OID 25138)
-- Dependencies: 215
-- Name: ht_seg_menu_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_menu_pk ON ht_seg_menu USING btree (me_id);


--
-- TOC entry 2364 (class 1259 OID 25139)
-- Dependencies: 216
-- Name: ht_seg_modulo_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_modulo_pk ON ht_seg_modulo USING btree (mo_id);


--
-- TOC entry 2370 (class 1259 OID 25140)
-- Dependencies: 218
-- Name: ht_seg_perfil_menu2_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ht_seg_perfil_menu2_fk ON ht_seg_perfil_menu USING btree (per_id);


--
-- TOC entry 2367 (class 1259 OID 25141)
-- Dependencies: 217
-- Name: ht_seg_perfil_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_perfil_pk ON ht_seg_perfil USING btree (per_id);


--
-- TOC entry 2373 (class 1259 OID 25142)
-- Dependencies: 219
-- Name: ht_seg_persona_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_persona_pk ON ht_seg_persona USING btree (pe_id);


--
-- TOC entry 2384 (class 1259 OID 25143)
-- Dependencies: 223 223 223
-- Name: ht_seg_usuario_menu_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ht_seg_usuario_menu_fk ON ht_seg_usuario_menu USING btree (us_id, per_id, pe_id);


--
-- TOC entry 2380 (class 1259 OID 25144)
-- Dependencies: 222 222 222
-- Name: ht_seg_usuario_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_seg_usuario_pk ON ht_seg_usuario USING btree (us_id, per_id, pe_id);


--
-- TOC entry 2395 (class 1259 OID 25145)
-- Dependencies: 226
-- Name: ht_ser_servicio_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_ser_servicio_pk ON ht_ser_servicio USING btree (ser_id);


--
-- TOC entry 2403 (class 1259 OID 25146)
-- Dependencies: 228
-- Name: ht_ser_tipo_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX ht_ser_tipo_pk ON ht_ser_tipo USING btree (set_id);


--
-- TOC entry 2217 (class 1259 OID 25147)
-- Dependencies: 169
-- Name: index_1; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_1 ON ht_cms_idioma_categoria USING btree (id_id);


--
-- TOC entry 2298 (class 1259 OID 25148)
-- Dependencies: 195
-- Name: index_10; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_10 ON ht_hb_idioma_piso USING btree (pi_id);


--
-- TOC entry 2210 (class 1259 OID 25149)
-- Dependencies: 167
-- Name: index_11; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_11 ON ht_cms_idioma_art_cont USING btree (art_id);


--
-- TOC entry 2250 (class 1259 OID 25150)
-- Dependencies: 180
-- Name: index_13; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_13 ON ht_cms_promocion USING btree (pro_id);


--
-- TOC entry 2267 (class 1259 OID 25151)
-- Dependencies: 186
-- Name: index_14; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_14 ON ht_hb_accesorio_sucursal_tiphab USING btree (ac_id);


--
-- TOC entry 2253 (class 1259 OID 25152)
-- Dependencies: 181
-- Name: index_16; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_16 ON ht_cms_promocion_sucursal USING btree (pro_id);


--
-- TOC entry 2229 (class 1259 OID 25153)
-- Dependencies: 173
-- Name: index_17; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_17 ON ht_cms_idioma_promocion USING btree (pro_id);


--
-- TOC entry 2232 (class 1259 OID 25154)
-- Dependencies: 174
-- Name: index_18; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_18 ON ht_cms_idioma_promsucursal USING btree (id_id);


--
-- TOC entry 2304 (class 1259 OID 25155)
-- Dependencies: 197
-- Name: index_19; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_19 ON ht_hb_idioma_tarifa USING btree (ta_id);


--
-- TOC entry 2286 (class 1259 OID 25156)
-- Dependencies: 191
-- Name: index_2; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_2 ON ht_hb_habitacion_tarifa USING btree (su_id);


--
-- TOC entry 2241 (class 1259 OID 25157)
-- Dependencies: 177
-- Name: index_20; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_20 ON ht_cms_menu_interno USING btree (cat_id);


--
-- TOC entry 2409 (class 1259 OID 25158)
-- Dependencies: 230
-- Name: index_21; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_21 ON idioma_tabla_det USING btree (id_id);


--
-- TOC entry 2406 (class 1259 OID 25159)
-- Dependencies: 229
-- Name: index_22; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_22 ON idioma_tabla USING btree (id_id);


--
-- TOC entry 2223 (class 1259 OID 25160)
-- Dependencies: 171
-- Name: index_23; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_23 ON ht_cms_idioma_menu_interno USING btree (cat_id);


--
-- TOC entry 2247 (class 1259 OID 25161)
-- Dependencies: 179
-- Name: index_24; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_24 ON ht_cms_promo_imagen USING btree (pro_id);


--
-- TOC entry 2264 (class 1259 OID 25162)
-- Dependencies: 185
-- Name: index_25; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_25 ON ht_cms_sucursal_menu_interno USING btree (cat_orden);


--
-- TOC entry 2220 (class 1259 OID 25163)
-- Dependencies: 170
-- Name: index_27; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_27 ON ht_cms_idioma_desc USING btree (id_id);


--
-- TOC entry 2204 (class 1259 OID 25164)
-- Dependencies: 165
-- Name: index_28; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_28 ON ht_cms_descripciones USING btree (des_id);


--
-- TOC entry 2329 (class 1259 OID 25165)
-- Dependencies: 205
-- Name: index_29; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_29 ON ht_res_anio USING btree (an_id);


--
-- TOC entry 2310 (class 1259 OID 25166)
-- Dependencies: 199
-- Name: index_3; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_3 ON ht_hb_piso USING btree (pi_id);


--
-- TOC entry 2338 (class 1259 OID 25167)
-- Dependencies: 208
-- Name: index_30; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_30 ON ht_res_idioma_anio USING btree (id_id);


--
-- TOC entry 2259 (class 1259 OID 25168)
-- Dependencies: 183
-- Name: index_31; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_31 ON ht_cms_publicacion USING btree (pub_id);


--
-- TOC entry 2235 (class 1259 OID 25169)
-- Dependencies: 175
-- Name: index_32; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_32 ON ht_cms_idioma_public USING btree (pub_id);


--
-- TOC entry 2256 (class 1259 OID 25170)
-- Dependencies: 182
-- Name: index_33; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_33 ON ht_cms_public_imagen USING btree (im_id);


--
-- TOC entry 2301 (class 1259 OID 25171)
-- Dependencies: 196
-- Name: index_34; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_34 ON ht_hb_idioma_suc_tiphab USING btree (tih_id);


--
-- TOC entry 2335 (class 1259 OID 25172)
-- Dependencies: 207
-- Name: index_35; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_35 ON ht_res_cambio USING btree (cam_fecha);


--
-- TOC entry 2244 (class 1259 OID 25173)
-- Dependencies: 178
-- Name: index_36; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_36 ON ht_cms_meses USING btree (mes_id);


--
-- TOC entry 2226 (class 1259 OID 25174)
-- Dependencies: 172
-- Name: index_37; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_37 ON ht_cms_idioma_mes USING btree (id_id);


--
-- TOC entry 2357 (class 1259 OID 25175)
-- Dependencies: 214
-- Name: index_4; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_4 ON ht_seg_idioma_perfil USING btree (id_id);


--
-- TOC entry 2424 (class 1259 OID 25176)
-- Dependencies: 235
-- Name: index_8; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_8 ON tabla_sistema USING btree (tas_id);


--
-- TOC entry 2412 (class 1259 OID 25177)
-- Dependencies: 231
-- Name: index_9; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_9 ON idioma_tabla_sis USING btree (tas_id);


--
-- TOC entry 2381 (class 1259 OID 25178)
-- Dependencies: 222
-- Name: perfil_de_usuario_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX perfil_de_usuario_fk ON ht_seg_usuario USING btree (per_id);


--
-- TOC entry 2297 (class 1259 OID 25179)
-- Dependencies: 194
-- Name: relationship_15_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_15_fk ON ht_hb_idioma_habitacion USING btree (hb_id);


--
-- TOC entry 2294 (class 1259 OID 25180)
-- Dependencies: 193
-- Name: relationship_20_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_20_fk ON ht_hb_idioma_accesorios USING btree (ac_id);


--
-- TOC entry 2309 (class 1259 OID 25181)
-- Dependencies: 198
-- Name: relationship_21_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_21_fk ON ht_hb_idioma_tipo_habitacion USING btree (tih_id);


--
-- TOC entry 2346 (class 1259 OID 25182)
-- Dependencies: 210
-- Name: relationship_22_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_22_fk ON ht_res_reserva USING btree (cl_id);


--
-- TOC entry 2355 (class 1259 OID 25183)
-- Dependencies: 213
-- Name: relationship_24_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_24_fk ON ht_seg_idioma_menu USING btree (me_id);


--
-- TOC entry 2356 (class 1259 OID 25184)
-- Dependencies: 213
-- Name: relationship_25_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_25_fk ON ht_seg_idioma_menu USING btree (id_id);


--
-- TOC entry 2284 (class 1259 OID 25185)
-- Dependencies: 190
-- Name: relationship_28_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_28_fk ON ht_hb_habitacion_imagen USING btree (hb_id);


--
-- TOC entry 2285 (class 1259 OID 25186)
-- Dependencies: 190
-- Name: relationship_29_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_29_fk ON ht_hb_habitacion_imagen USING btree (im_id);


--
-- TOC entry 2276 (class 1259 OID 25187)
-- Dependencies: 188
-- Name: relationship_2_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_2_fk ON ht_hb_habitacion USING btree (su_id);


--
-- TOC entry 2215 (class 1259 OID 25188)
-- Dependencies: 168
-- Name: relationship_30_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_30_fk ON ht_cms_idioma_articulo USING btree (art_id);


--
-- TOC entry 2216 (class 1259 OID 25189)
-- Dependencies: 168
-- Name: relationship_31_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_31_fk ON ht_cms_idioma_articulo USING btree (id_id);


--
-- TOC entry 2196 (class 1259 OID 25190)
-- Dependencies: 162
-- Name: relationship_32_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_32_fk ON cms_imagenes_articulo USING btree (im_id);


--
-- TOC entry 2197 (class 1259 OID 25191)
-- Dependencies: 162
-- Name: relationship_33_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_33_fk ON cms_imagenes_articulo USING btree (art_id);


--
-- TOC entry 2192 (class 1259 OID 25192)
-- Dependencies: 161
-- Name: relationship_34_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_34_fk ON cms_categoria_articulo USING btree (cat_id);


--
-- TOC entry 2193 (class 1259 OID 25193)
-- Dependencies: 161
-- Name: relationship_35_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_35_fk ON cms_categoria_articulo USING btree (art_id);


--
-- TOC entry 2389 (class 1259 OID 25194)
-- Dependencies: 224
-- Name: relationship_38_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_38_fk ON ht_ser_idioma_servicio USING btree (id_id);


--
-- TOC entry 2390 (class 1259 OID 25195)
-- Dependencies: 224
-- Name: relationship_39_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_39_fk ON ht_ser_idioma_servicio USING btree (ser_id);


--
-- TOC entry 2277 (class 1259 OID 25196)
-- Dependencies: 188
-- Name: relationship_3_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_3_fk ON ht_hb_habitacion USING btree (tih_id);


--
-- TOC entry 2393 (class 1259 OID 25197)
-- Dependencies: 225
-- Name: relationship_40_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_40_fk ON ht_ser_idioma_tipo USING btree (id_id);


--
-- TOC entry 2398 (class 1259 OID 25198)
-- Dependencies: 226
-- Name: relationship_41_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_41_fk ON ht_ser_servicio USING btree (set_id);


--
-- TOC entry 2394 (class 1259 OID 25199)
-- Dependencies: 225
-- Name: relationship_42_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_42_fk ON ht_ser_idioma_tipo USING btree (set_id);


--
-- TOC entry 2401 (class 1259 OID 25200)
-- Dependencies: 227
-- Name: relationship_43_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_43_fk ON ht_ser_servicio_sucursal USING btree (ser_id);


--
-- TOC entry 2402 (class 1259 OID 25201)
-- Dependencies: 227
-- Name: relationship_44_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_44_fk ON ht_ser_servicio_sucursal USING btree (su_id);


--
-- TOC entry 2280 (class 1259 OID 25202)
-- Dependencies: 189
-- Name: relationship_45_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_45_fk ON ht_hb_habitacion_accesorios USING btree (ac_id);


--
-- TOC entry 2281 (class 1259 OID 25203)
-- Dependencies: 189
-- Name: relationship_46_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_46_fk ON ht_hb_habitacion_accesorios USING btree (hb_id);


--
-- TOC entry 2363 (class 1259 OID 25204)
-- Dependencies: 215
-- Name: relationship_47_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_47_fk ON ht_seg_menu USING btree (mo_id);


--
-- TOC entry 2326 (class 1259 OID 25205)
-- Dependencies: 203
-- Name: relationship_49_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_49_fk ON ht_hb_tipo_habitacion USING btree (ht_tih_id);


--
-- TOC entry 2318 (class 1259 OID 25206)
-- Dependencies: 201
-- Name: relationship_53_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_53_fk ON ht_hb_sucursal_tipo_habitacion USING btree (su_id);


--
-- TOC entry 2319 (class 1259 OID 25207)
-- Dependencies: 201
-- Name: relationship_54_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_54_fk ON ht_hb_sucursal_tipo_habitacion USING btree (tih_id);


--
-- TOC entry 2343 (class 1259 OID 25208)
-- Dependencies: 209
-- Name: relationship_58_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_58_fk ON ht_res_kardex USING btree (hb_id);


--
-- TOC entry 2423 (class 1259 OID 25209)
-- Dependencies: 234
-- Name: relationship_97_fk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX relationship_97_fk ON tabla_detalle USING btree (tba_id);


--
-- TOC entry 2347 (class 1259 OID 25210)
-- Dependencies: 210 210
-- Name: reserva_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX reserva_pk ON ht_res_reserva USING btree (res_id, cl_id);


--
-- TOC entry 2419 (class 1259 OID 25211)
-- Dependencies: 233
-- Name: tabla_pk; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX tabla_pk ON tabla USING btree (tba_id);


--
-- TOC entry 2427 (class 2606 OID 25212)
-- Dependencies: 2314 200 161
-- Name: fk_cms_cate_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_categoria_articulo
    ADD CONSTRAINT fk_cms_cate_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2428 (class 2606 OID 25217)
-- Dependencies: 163 161 2199
-- Name: fk_cms_cate_relations_ht_cms_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_categoria_articulo
    ADD CONSTRAINT fk_cms_cate_relations_ht_cms_a FOREIGN KEY (art_id) REFERENCES ht_cms_articulo(art_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2429 (class 2606 OID 25222)
-- Dependencies: 164 161 2202
-- Name: fk_cms_cate_relations_ht_cms_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_categoria_articulo
    ADD CONSTRAINT fk_cms_cate_relations_ht_cms_c FOREIGN KEY (cat_id) REFERENCES ht_cms_categoria(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2430 (class 2606 OID 25227)
-- Dependencies: 162 2202 164
-- Name: fk_cms_imag_reference_ht_cms_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_imagenes_articulo
    ADD CONSTRAINT fk_cms_imag_reference_ht_cms_c FOREIGN KEY (cat_id) REFERENCES ht_cms_categoria(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2431 (class 2606 OID 25232)
-- Dependencies: 200 2314 162
-- Name: fk_cms_imag_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_imagenes_articulo
    ADD CONSTRAINT fk_cms_imag_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2432 (class 2606 OID 25237)
-- Dependencies: 163 162 2199
-- Name: fk_cms_imag_relations_ht_cms_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_imagenes_articulo
    ADD CONSTRAINT fk_cms_imag_relations_ht_cms_a FOREIGN KEY (art_id) REFERENCES ht_cms_articulo(art_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2433 (class 2606 OID 25242)
-- Dependencies: 176 162 2239
-- Name: fk_cms_imag_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cms_imagenes_articulo
    ADD CONSTRAINT fk_cms_imag_relations_ht_cms_i FOREIGN KEY (im_id) REFERENCES ht_cms_imagenes(im_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2434 (class 2606 OID 25247)
-- Dependencies: 2290 192 164
-- Name: fk_ht_cms_c_reference_ht_hb_ho; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_categoria
    ADD CONSTRAINT fk_ht_cms_c_reference_ht_hb_ho FOREIGN KEY (ho_id) REFERENCES ht_hb_hotel(ho_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2435 (class 2606 OID 25252)
-- Dependencies: 163 167 2199
-- Name: fk_ht_cms_i_reference_ht_cms_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_art_cont
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_a FOREIGN KEY (art_id) REFERENCES ht_cms_articulo(art_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2436 (class 2606 OID 25257)
-- Dependencies: 167 164 2202
-- Name: fk_ht_cms_i_reference_ht_cms_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_art_cont
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_c FOREIGN KEY (cat_id) REFERENCES ht_cms_categoria(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2441 (class 2606 OID 25262)
-- Dependencies: 169 164 2202
-- Name: fk_ht_cms_i_reference_ht_cms_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_categoria
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_c FOREIGN KEY (cat_id) REFERENCES ht_cms_categoria(cat_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2443 (class 2606 OID 25267)
-- Dependencies: 170 165 2205
-- Name: fk_ht_cms_i_reference_ht_cms_d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_desc
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_d FOREIGN KEY (des_id) REFERENCES ht_cms_descripciones(des_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2437 (class 2606 OID 25272)
-- Dependencies: 167 166 2208
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_art_cont
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2442 (class 2606 OID 25277)
-- Dependencies: 2208 169 166
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_categoria
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2445 (class 2606 OID 25282)
-- Dependencies: 171 166 2208
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_menu_interno
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2449 (class 2606 OID 25287)
-- Dependencies: 2208 166 173
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_promocion
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2451 (class 2606 OID 25292)
-- Dependencies: 174 166 2208
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_promsucursal
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2453 (class 2606 OID 25297)
-- Dependencies: 175 166 2208
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_public
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2444 (class 2606 OID 25302)
-- Dependencies: 166 2208 170
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_desc
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2447 (class 2606 OID 25307)
-- Dependencies: 166 172 2208
-- Name: fk_ht_cms_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_mes
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2446 (class 2606 OID 25312)
-- Dependencies: 177 171 2242
-- Name: fk_ht_cms_i_reference_ht_cms_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_menu_interno
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_m FOREIGN KEY (cat_id) REFERENCES ht_cms_menu_interno(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2448 (class 2606 OID 25317)
-- Dependencies: 2245 172 178
-- Name: fk_ht_cms_i_reference_ht_cms_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_mes
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_m FOREIGN KEY (mes_id) REFERENCES ht_cms_meses(mes_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2450 (class 2606 OID 25322)
-- Dependencies: 180 173 173 2251 180
-- Name: fk_ht_cms_i_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_promocion
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_p FOREIGN KEY (pro_id, an_id) REFERENCES ht_cms_promocion(pro_id, an_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2452 (class 2606 OID 25327)
-- Dependencies: 174 181 174 174 181 181 181 2254 174
-- Name: fk_ht_cms_i_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_promsucursal
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_p FOREIGN KEY (pro_id, an_id, tih_id, su_id) REFERENCES ht_cms_promocion_sucursal(pro_id, an_id, tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2454 (class 2606 OID 25332)
-- Dependencies: 183 2260 175
-- Name: fk_ht_cms_i_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_public
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_cms_p FOREIGN KEY (pub_id) REFERENCES ht_cms_publicacion(pub_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2438 (class 2606 OID 25337)
-- Dependencies: 167 2314 200
-- Name: fk_ht_cms_i_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_art_cont
    ADD CONSTRAINT fk_ht_cms_i_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2439 (class 2606 OID 25342)
-- Dependencies: 168 2199 163
-- Name: fk_ht_cms_i_relations_ht_cms_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_articulo
    ADD CONSTRAINT fk_ht_cms_i_relations_ht_cms_a FOREIGN KEY (art_id) REFERENCES ht_cms_articulo(art_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2440 (class 2606 OID 25347)
-- Dependencies: 166 168 2208
-- Name: fk_ht_cms_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_idioma_articulo
    ADD CONSTRAINT fk_ht_cms_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2455 (class 2606 OID 25352)
-- Dependencies: 176 2239 179
-- Name: fk_ht_cms_p_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promo_imagen
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_cms_i FOREIGN KEY (im_id) REFERENCES ht_cms_imagenes(im_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2461 (class 2606 OID 25357)
-- Dependencies: 2239 182 176
-- Name: fk_ht_cms_p_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_public_imagen
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_cms_i FOREIGN KEY (im_id) REFERENCES ht_cms_imagenes(im_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2459 (class 2606 OID 25362)
-- Dependencies: 180 180 181 2251 181
-- Name: fk_ht_cms_p_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promocion_sucursal
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_cms_p FOREIGN KEY (pro_id, an_id) REFERENCES ht_cms_promocion(pro_id, an_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2456 (class 2606 OID 25367)
-- Dependencies: 2254 181 181 179 179 179 181 181 179
-- Name: fk_ht_cms_p_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promo_imagen
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_cms_p FOREIGN KEY (pro_id, an_id, tih_id, su_id) REFERENCES ht_cms_promocion_sucursal(pro_id, an_id, tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2462 (class 2606 OID 25372)
-- Dependencies: 182 2260 183
-- Name: fk_ht_cms_p_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_public_imagen
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_cms_p FOREIGN KEY (pub_id) REFERENCES ht_cms_publicacion(pub_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2457 (class 2606 OID 25377)
-- Dependencies: 180 192 2290
-- Name: fk_ht_cms_p_reference_ht_hb_ho; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promocion
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_hb_ho FOREIGN KEY (ho_id) REFERENCES ht_hb_hotel(ho_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2460 (class 2606 OID 25382)
-- Dependencies: 201 181 181 201 2316
-- Name: fk_ht_cms_p_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promocion_sucursal
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_hb_su FOREIGN KEY (tih_id, su_id) REFERENCES ht_hb_sucursal_tipo_habitacion(tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2458 (class 2606 OID 25387)
-- Dependencies: 2324 180 203
-- Name: fk_ht_cms_p_reference_ht_hb_ti; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_promocion
    ADD CONSTRAINT fk_ht_cms_p_reference_ht_hb_ti FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2463 (class 2606 OID 25392)
-- Dependencies: 2202 164 184
-- Name: fk_ht_cms_s_reference_ht_cms_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_sucursal_categoria
    ADD CONSTRAINT fk_ht_cms_s_reference_ht_cms_c FOREIGN KEY (cat_id) REFERENCES ht_cms_categoria(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2465 (class 2606 OID 25397)
-- Dependencies: 177 185 2242
-- Name: fk_ht_cms_s_reference_ht_cms_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_sucursal_menu_interno
    ADD CONSTRAINT fk_ht_cms_s_reference_ht_cms_m FOREIGN KEY (cat_id) REFERENCES ht_cms_menu_interno(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2464 (class 2606 OID 25402)
-- Dependencies: 184 2314 200
-- Name: fk_ht_cms_s_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_sucursal_categoria
    ADD CONSTRAINT fk_ht_cms_s_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2466 (class 2606 OID 25407)
-- Dependencies: 200 185 2314
-- Name: fk_ht_cms_s_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_sucursal_menu_interno
    ADD CONSTRAINT fk_ht_cms_s_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2467 (class 2606 OID 25412)
-- Dependencies: 2324 185 203
-- Name: fk_ht_cms_s_reference_ht_hb_ti; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_cms_sucursal_menu_interno
    ADD CONSTRAINT fk_ht_cms_s_reference_ht_hb_ti FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2475 (class 2606 OID 25417)
-- Dependencies: 2239 176 190
-- Name: fk_ht_hab_h_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_imagen
    ADD CONSTRAINT fk_ht_hab_h_relations_ht_cms_i FOREIGN KEY (im_id) REFERENCES ht_cms_imagenes(im_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2473 (class 2606 OID 25422)
-- Dependencies: 2271 189 187
-- Name: fk_ht_hab_h_relations_ht_hab_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_accesorios
    ADD CONSTRAINT fk_ht_hab_h_relations_ht_hab_a FOREIGN KEY (ac_id) REFERENCES ht_hb_accesorios(ac_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2474 (class 2606 OID 25427)
-- Dependencies: 2274 188 189
-- Name: fk_ht_hab_h_relations_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_accesorios
    ADD CONSTRAINT fk_ht_hab_h_relations_ht_hb_ha FOREIGN KEY (hb_id) REFERENCES ht_hb_habitacion(hb_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2476 (class 2606 OID 25432)
-- Dependencies: 2274 190 188
-- Name: fk_ht_hab_h_relations_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_imagen
    ADD CONSTRAINT fk_ht_hab_h_relations_ht_hb_ha FOREIGN KEY (hb_id) REFERENCES ht_hb_habitacion(hb_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2479 (class 2606 OID 25437)
-- Dependencies: 2208 193 166
-- Name: fk_ht_hab_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_accesorios
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2481 (class 2606 OID 25442)
-- Dependencies: 166 194 2208
-- Name: fk_ht_hab_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_habitacion
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2489 (class 2606 OID 25447)
-- Dependencies: 2208 166 198
-- Name: fk_ht_hab_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_tipo_habitacion
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2480 (class 2606 OID 25452)
-- Dependencies: 187 2271 193
-- Name: fk_ht_hab_i_relations_ht_hab_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_accesorios
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_hab_a FOREIGN KEY (ac_id) REFERENCES ht_hb_accesorios(ac_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2490 (class 2606 OID 25457)
-- Dependencies: 203 2324 198
-- Name: fk_ht_hab_i_relations_ht_hab_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_tipo_habitacion
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_hab_t FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2482 (class 2606 OID 25462)
-- Dependencies: 194 2274 188
-- Name: fk_ht_hab_i_relations_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_habitacion
    ADD CONSTRAINT fk_ht_hab_i_relations_ht_hb_ha FOREIGN KEY (hb_id) REFERENCES ht_hb_habitacion(hb_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2494 (class 2606 OID 25467)
-- Dependencies: 203 201 2324
-- Name: fk_ht_hab_s_relations_ht_hab_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_sucursal_tipo_habitacion
    ADD CONSTRAINT fk_ht_hab_s_relations_ht_hab_t FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2495 (class 2606 OID 25472)
-- Dependencies: 2314 200 201
-- Name: fk_ht_hab_s_relations_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_sucursal_tipo_habitacion
    ADD CONSTRAINT fk_ht_hab_s_relations_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2496 (class 2606 OID 25477)
-- Dependencies: 2324 203 203
-- Name: fk_ht_hab_t_relations_ht_hab_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_tipo_habitacion
    ADD CONSTRAINT fk_ht_hab_t_relations_ht_hab_t FOREIGN KEY (ht_tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2468 (class 2606 OID 25482)
-- Dependencies: 2271 187 186
-- Name: fk_ht_hb_ac_reference_ht_hb_ac; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_accesorio_sucursal_tiphab
    ADD CONSTRAINT fk_ht_hb_ac_reference_ht_hb_ac FOREIGN KEY (ac_id) REFERENCES ht_hb_accesorios(ac_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2469 (class 2606 OID 25487)
-- Dependencies: 2316 186 186 201 201
-- Name: fk_ht_hb_ac_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_accesorio_sucursal_tiphab
    ADD CONSTRAINT fk_ht_hb_ac_reference_ht_hb_su FOREIGN KEY (tih_id, su_id) REFERENCES ht_hb_sucursal_tipo_habitacion(tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2470 (class 2606 OID 25492)
-- Dependencies: 2311 199 188
-- Name: fk_ht_hb_ha_reference_ht_hb_pi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion
    ADD CONSTRAINT fk_ht_hb_ha_reference_ht_hb_pi FOREIGN KEY (pi_id) REFERENCES ht_hb_piso(pi_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2477 (class 2606 OID 25497)
-- Dependencies: 191 191 201 201 2316
-- Name: fk_ht_hb_ha_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_tarifa
    ADD CONSTRAINT fk_ht_hb_ha_reference_ht_hb_su FOREIGN KEY (tih_id, su_id) REFERENCES ht_hb_sucursal_tipo_habitacion(tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2478 (class 2606 OID 25502)
-- Dependencies: 202 191 2321
-- Name: fk_ht_hb_ha_reference_ht_hb_ta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion_tarifa
    ADD CONSTRAINT fk_ht_hb_ha_reference_ht_hb_ta FOREIGN KEY (ta_id) REFERENCES ht_hb_tarifa(ta_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2471 (class 2606 OID 25507)
-- Dependencies: 2324 203 188
-- Name: fk_ht_hb_ha_relations_ht_hab_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion
    ADD CONSTRAINT fk_ht_hb_ha_relations_ht_hab_t FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2472 (class 2606 OID 25512)
-- Dependencies: 188 2314 200
-- Name: fk_ht_hb_ha_relations_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_habitacion
    ADD CONSTRAINT fk_ht_hb_ha_relations_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2483 (class 2606 OID 25517)
-- Dependencies: 166 2208 195
-- Name: fk_ht_hb_id_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_piso
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2487 (class 2606 OID 25522)
-- Dependencies: 166 197 2208
-- Name: fk_ht_hb_id_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_tarifa
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2485 (class 2606 OID 25527)
-- Dependencies: 196 166 2208
-- Name: fk_ht_hb_id_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_suc_tiphab
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2484 (class 2606 OID 25532)
-- Dependencies: 2311 195 199
-- Name: fk_ht_hb_id_reference_ht_hb_pi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_piso
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_hb_pi FOREIGN KEY (pi_id) REFERENCES ht_hb_piso(pi_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2486 (class 2606 OID 25537)
-- Dependencies: 201 196 196 201 2316
-- Name: fk_ht_hb_id_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_suc_tiphab
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_hb_su FOREIGN KEY (tih_id, su_id) REFERENCES ht_hb_sucursal_tipo_habitacion(tih_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2488 (class 2606 OID 25542)
-- Dependencies: 202 197 2321
-- Name: fk_ht_hb_id_reference_ht_hb_ta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_idioma_tarifa
    ADD CONSTRAINT fk_ht_hb_id_reference_ht_hb_ta FOREIGN KEY (ta_id) REFERENCES ht_hb_tarifa(ta_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2491 (class 2606 OID 25547)
-- Dependencies: 200 199 2314
-- Name: fk_ht_hb_pi_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_piso
    ADD CONSTRAINT fk_ht_hb_pi_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2492 (class 2606 OID 25552)
-- Dependencies: 221 200 2378
-- Name: fk_ht_hb_su_reference_ht_seg_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_sucursal
    ADD CONSTRAINT fk_ht_hb_su_reference_ht_seg_u FOREIGN KEY (ub_id) REFERENCES ht_seg_ubigeo(ub_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2493 (class 2606 OID 25557)
-- Dependencies: 192 200 2290
-- Name: fk_ht_hb_su_sucursale_ht_hb_ho; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_sucursal
    ADD CONSTRAINT fk_ht_hb_su_sucursale_ht_hb_ho FOREIGN KEY (ho_id) REFERENCES ht_hb_hotel(ho_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2497 (class 2606 OID 25562)
-- Dependencies: 177 2242 203
-- Name: fk_ht_hb_ti_reference_ht_cms_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_tipo_habitacion
    ADD CONSTRAINT fk_ht_hb_ti_reference_ht_cms_m FOREIGN KEY (cat_id) REFERENCES ht_cms_menu_interno(cat_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2501 (class 2606 OID 25567)
-- Dependencies: 2208 208 166
-- Name: fk_ht_res_i_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_idioma_anio
    ADD CONSTRAINT fk_ht_res_i_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2502 (class 2606 OID 25572)
-- Dependencies: 208 205 2330
-- Name: fk_ht_res_i_reference_ht_res_a; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_idioma_anio
    ADD CONSTRAINT fk_ht_res_i_reference_ht_res_a FOREIGN KEY (an_id) REFERENCES ht_res_anio(an_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2503 (class 2606 OID 25577)
-- Dependencies: 191 191 2287 191 209 209 209 209 191
-- Name: fk_ht_res_k_reference_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_kardex
    ADD CONSTRAINT fk_ht_res_k_reference_ht_hb_ha FOREIGN KEY (tih_id, su_id, hb_capacidad, ta_id) REFERENCES ht_hb_habitacion_tarifa(tih_id, su_id, hb_capacidad, ta_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2504 (class 2606 OID 25582)
-- Dependencies: 210 209 2344
-- Name: fk_ht_res_k_reference_ht_res_r; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_kardex
    ADD CONSTRAINT fk_ht_res_k_reference_ht_res_r FOREIGN KEY (res_id) REFERENCES ht_res_reserva(res_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2505 (class 2606 OID 25587)
-- Dependencies: 188 209 2274
-- Name: fk_ht_res_k_relations_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_kardex
    ADD CONSTRAINT fk_ht_res_k_relations_ht_hb_ha FOREIGN KEY (hb_id) REFERENCES ht_hb_habitacion(hb_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2506 (class 2606 OID 25592)
-- Dependencies: 2208 210 166
-- Name: fk_ht_res_r_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva
    ADD CONSTRAINT fk_ht_res_r_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2509 (class 2606 OID 25597)
-- Dependencies: 180 211 211 180 2251
-- Name: fk_ht_res_r_reference_ht_cms_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT fk_ht_res_r_reference_ht_cms_p FOREIGN KEY (pro_id, an_id) REFERENCES ht_cms_promocion(pro_id, an_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2510 (class 2606 OID 25602)
-- Dependencies: 211 2271 187
-- Name: fk_ht_res_r_reference_ht_hb_acc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT fk_ht_res_r_reference_ht_hb_acc FOREIGN KEY (ac_id) REFERENCES ht_hb_accesorios(ac_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2507 (class 2606 OID 25607)
-- Dependencies: 200 210 2314
-- Name: fk_ht_res_r_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva
    ADD CONSTRAINT fk_ht_res_r_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2511 (class 2606 OID 25612)
-- Dependencies: 210 211 2344
-- Name: fk_ht_res_r_reference_ht_res_r; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT fk_ht_res_r_reference_ht_res_r FOREIGN KEY (res_id) REFERENCES ht_res_reserva(res_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- TOC entry 2512 (class 2606 OID 25617)
-- Dependencies: 226 211 2396
-- Name: fk_ht_res_r_reference_ht_ser_s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT fk_ht_res_r_reference_ht_ser_s FOREIGN KEY (ser_id) REFERENCES ht_ser_servicio(ser_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2513 (class 2606 OID 25622)
-- Dependencies: 2274 188 211
-- Name: fk_ht_res_r_relations_ht_hb_ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva_detalle
    ADD CONSTRAINT fk_ht_res_r_relations_ht_hb_ha FOREIGN KEY (hb_id) REFERENCES ht_hb_habitacion(hb_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2508 (class 2606 OID 25627)
-- Dependencies: 2351 212 210
-- Name: fk_ht_res_r_relations_ht_seg_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_res_reserva
    ADD CONSTRAINT fk_ht_res_r_relations_ht_seg_c FOREIGN KEY (cl_id) REFERENCES ht_seg_cliente(cl_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2514 (class 2606 OID 25632)
-- Dependencies: 2382 212 212 222 222 222 212
-- Name: fk_ht_seg_c_reference_ht_seg_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_cliente
    ADD CONSTRAINT fk_ht_seg_c_reference_ht_seg_u FOREIGN KEY (us_id, pe_id, per_id) REFERENCES ht_seg_usuario(us_id, pe_id, per_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2515 (class 2606 OID 25637)
-- Dependencies: 213 2208 166
-- Name: fk_ht_seg_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_idioma_menu
    ADD CONSTRAINT fk_ht_seg_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2516 (class 2606 OID 25642)
-- Dependencies: 2361 213 215
-- Name: fk_ht_seg_i_relations_ht_seg_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_idioma_menu
    ADD CONSTRAINT fk_ht_seg_i_relations_ht_seg_m FOREIGN KEY (me_id) REFERENCES ht_seg_menu(me_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2517 (class 2606 OID 25647)
-- Dependencies: 2361 215 215
-- Name: fk_ht_seg_m_reference_ht_seg_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_menu
    ADD CONSTRAINT fk_ht_seg_m_reference_ht_seg_m FOREIGN KEY (ht_me_id) REFERENCES ht_seg_menu(me_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2518 (class 2606 OID 25652)
-- Dependencies: 215 2365 216
-- Name: fk_ht_seg_m_relations_ht_seg_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_menu
    ADD CONSTRAINT fk_ht_seg_m_relations_ht_seg_m FOREIGN KEY (mo_id) REFERENCES ht_seg_modulo(mo_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2519 (class 2606 OID 25657)
-- Dependencies: 218 2368 217
-- Name: fk_ht_seg_p_ht_seg_pe_ht_seg_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_perfil_menu
    ADD CONSTRAINT fk_ht_seg_p_ht_seg_pe_ht_seg_p FOREIGN KEY (per_id) REFERENCES ht_seg_perfil(per_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2522 (class 2606 OID 25662)
-- Dependencies: 220 2290 192
-- Name: fk_ht_seg_p_reference_ht_hb_ho; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_persona_hotel
    ADD CONSTRAINT fk_ht_seg_p_reference_ht_hb_ho FOREIGN KEY (ho_id) REFERENCES ht_hb_hotel(ho_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2523 (class 2606 OID 25667)
-- Dependencies: 220 219 2374
-- Name: fk_ht_seg_p_reference_ht_seg_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_persona_hotel
    ADD CONSTRAINT fk_ht_seg_p_reference_ht_seg_p FOREIGN KEY (pe_id) REFERENCES ht_seg_persona(pe_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2521 (class 2606 OID 25672)
-- Dependencies: 221 2378 219
-- Name: fk_ht_seg_p_reference_ht_seg_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_persona
    ADD CONSTRAINT fk_ht_seg_p_reference_ht_seg_u FOREIGN KEY (ub_id) REFERENCES ht_seg_ubigeo(ub_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2520 (class 2606 OID 25677)
-- Dependencies: 232 232 218 218 2415
-- Name: fk_ht_seg_p_reference_menu_suc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_perfil_menu
    ADD CONSTRAINT fk_ht_seg_p_reference_menu_suc FOREIGN KEY (me_id, su_id) REFERENCES menu_sucursal(me_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2526 (class 2606 OID 25682)
-- Dependencies: 223 223 223 222 222 222 2382
-- Name: fk_ht_seg_u_ht_seg_us_ht_seg_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_usuario_menu
    ADD CONSTRAINT fk_ht_seg_u_ht_seg_us_ht_seg_u FOREIGN KEY (us_id, per_id, pe_id) REFERENCES ht_seg_usuario(us_id, per_id, pe_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2524 (class 2606 OID 25687)
-- Dependencies: 217 222 2368
-- Name: fk_ht_seg_u_perfil_de_ht_seg_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_usuario
    ADD CONSTRAINT fk_ht_seg_u_perfil_de_ht_seg_p FOREIGN KEY (per_id) REFERENCES ht_seg_perfil(per_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2525 (class 2606 OID 25692)
-- Dependencies: 222 222 220 220 2376
-- Name: fk_ht_seg_u_reference_ht_seg_p; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_usuario
    ADD CONSTRAINT fk_ht_seg_u_reference_ht_seg_p FOREIGN KEY (pe_id, ho_id) REFERENCES ht_seg_persona_hotel(pe_id, ho_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2527 (class 2606 OID 25697)
-- Dependencies: 2415 223 223 232 232
-- Name: fk_ht_seg_u_reference_menu_suc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_seg_usuario_menu
    ADD CONSTRAINT fk_ht_seg_u_reference_menu_suc FOREIGN KEY (me_id, su_id) REFERENCES menu_sucursal(me_id, su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2532 (class 2606 OID 25702)
-- Dependencies: 221 2378 226
-- Name: fk_ht_ser_s_reference_ht_seg_u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_servicio
    ADD CONSTRAINT fk_ht_ser_s_reference_ht_seg_u FOREIGN KEY (ub_id) REFERENCES ht_seg_ubigeo(ub_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2533 (class 2606 OID 25707)
-- Dependencies: 2404 228 226
-- Name: fk_ht_ser_s_relations_ht_ser_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_servicio
    ADD CONSTRAINT fk_ht_ser_s_relations_ht_ser_t FOREIGN KEY (set_id) REFERENCES ht_ser_tipo(set_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2528 (class 2606 OID 25712)
-- Dependencies: 2208 166 224
-- Name: fk_ht_srv_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_idioma_servicio
    ADD CONSTRAINT fk_ht_srv_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2530 (class 2606 OID 25717)
-- Dependencies: 2208 225 166
-- Name: fk_ht_srv_i_relations_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_idioma_tipo
    ADD CONSTRAINT fk_ht_srv_i_relations_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2529 (class 2606 OID 25722)
-- Dependencies: 224 226 2396
-- Name: fk_ht_srv_i_relations_ht_ser_s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_idioma_servicio
    ADD CONSTRAINT fk_ht_srv_i_relations_ht_ser_s FOREIGN KEY (ser_id) REFERENCES ht_ser_servicio(ser_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2531 (class 2606 OID 25727)
-- Dependencies: 2404 228 225
-- Name: fk_ht_srv_i_relations_ht_ser_t; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_idioma_tipo
    ADD CONSTRAINT fk_ht_srv_i_relations_ht_ser_t FOREIGN KEY (set_id) REFERENCES ht_ser_tipo(set_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2534 (class 2606 OID 25732)
-- Dependencies: 227 200 2314
-- Name: fk_ht_srv_s_relations_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_servicio_sucursal
    ADD CONSTRAINT fk_ht_srv_s_relations_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2535 (class 2606 OID 25737)
-- Dependencies: 226 227 2396
-- Name: fk_ht_srv_s_relations_ht_ser_s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_ser_servicio_sucursal
    ADD CONSTRAINT fk_ht_srv_s_relations_ht_ser_s FOREIGN KEY (ser_id) REFERENCES ht_ser_servicio(ser_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2498 (class 2606 OID 25742)
-- Dependencies: 2324 204 203
-- Name: fk_ht_tipo__ht_hb_tip_ht_hb_ti; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_tipo_habitacion_imagen
    ADD CONSTRAINT fk_ht_tipo__ht_hb_tip_ht_hb_ti FOREIGN KEY (tih_id) REFERENCES ht_hb_tipo_habitacion(tih_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2499 (class 2606 OID 25747)
-- Dependencies: 2239 204 176
-- Name: fk_ht_tipo__reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_tipo_habitacion_imagen
    ADD CONSTRAINT fk_ht_tipo__reference_ht_cms_i FOREIGN KEY (im_id) REFERENCES ht_cms_imagenes(im_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2500 (class 2606 OID 25752)
-- Dependencies: 204 200 2314
-- Name: fk_ht_tipo_reference_ht_hb_suc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ht_hb_tipo_habitacion_imagen
    ADD CONSTRAINT fk_ht_tipo_reference_ht_hb_suc FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2536 (class 2606 OID 25757)
-- Dependencies: 166 2208 229
-- Name: fk_idioma_t_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla
    ADD CONSTRAINT fk_idioma_t_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2538 (class 2606 OID 25762)
-- Dependencies: 166 2208 230
-- Name: fk_idioma_t_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla_det
    ADD CONSTRAINT fk_idioma_t_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2540 (class 2606 OID 25767)
-- Dependencies: 166 2208 231
-- Name: fk_idioma_t_reference_ht_cms_i; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla_sis
    ADD CONSTRAINT fk_idioma_t_reference_ht_cms_i FOREIGN KEY (id_id) REFERENCES ht_cms_idioma(id_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2537 (class 2606 OID 25772)
-- Dependencies: 2417 233 229
-- Name: fk_idioma_t_reference_tabla; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla
    ADD CONSTRAINT fk_idioma_t_reference_tabla FOREIGN KEY (tba_id) REFERENCES tabla(tba_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2539 (class 2606 OID 25777)
-- Dependencies: 234 2421 230 230 234
-- Name: fk_idioma_t_reference_tabla_de; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla_det
    ADD CONSTRAINT fk_idioma_t_reference_tabla_de FOREIGN KEY (tad_id, tba_id) REFERENCES tabla_detalle(tad_id, tba_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2541 (class 2606 OID 25782)
-- Dependencies: 231 2425 235
-- Name: fk_idioma_t_reference_tabla_si; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY idioma_tabla_sis
    ADD CONSTRAINT fk_idioma_t_reference_tabla_si FOREIGN KEY (tas_id) REFERENCES tabla_sistema(tas_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2542 (class 2606 OID 25787)
-- Dependencies: 232 2314 200
-- Name: fk_menu_suc_reference_ht_hb_su; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menu_sucursal
    ADD CONSTRAINT fk_menu_suc_reference_ht_hb_su FOREIGN KEY (su_id) REFERENCES ht_hb_sucursal(su_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2543 (class 2606 OID 25792)
-- Dependencies: 2361 232 215
-- Name: fk_menu_suc_reference_ht_seg_m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY menu_sucursal
    ADD CONSTRAINT fk_menu_suc_reference_ht_seg_m FOREIGN KEY (me_id) REFERENCES ht_seg_menu(me_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2544 (class 2606 OID 25797)
-- Dependencies: 233 2417 234
-- Name: fk_tabla_de_reference_tabla; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tabla_detalle
    ADD CONSTRAINT fk_tabla_de_reference_tabla FOREIGN KEY (tba_id) REFERENCES tabla(tba_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-03-09 23:27:37

--
-- PostgreSQL database dump complete
--

