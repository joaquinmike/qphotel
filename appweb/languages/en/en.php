<?php

require_once 'Zend/Validate/NotEmpty.php';
require_once 'Zend/Validate/StringLength.php';
require_once 'Zend/Validate/EmailAddress.php';

return array(
Zend_Validate_NotEmpty::IS_EMPTY => 'El campo no puede estar vacío',
Zend_Validate_StringLength::TOO_LONG => 'El campo debe contener por lo menos %min% caracteres',
Zend_Validate_StringLength::TOO_SHORT => 'El campo debe contener un máximo de %max% caracteres',
Zend_Validate_EmailAddress::INVALID => 'La dirección de correo no es válida',
Zend_Validate_EmailAddress::QUOTED_STRING => "'%localPart%' no concuerda con el formato de comillas",
Zend_Validate_EmailAddress::DOT_ATOM => "'%localPart%' no concuerda con el formato de punto",
Zend_Validate_EmailAddress::INVALID_HOSTNAME => "'%hostname%' no es un nombre de dominio válido",
Zend_Validate_EmailAddress::INVALID_LOCAL_PART => "'%localPart%' no es una parte local válida",
Zend_Validate_EmailAddress::INVALID_MX_RECORD => "'%hostname%' no tiene un dominio de correo asignado",
);